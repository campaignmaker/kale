<?php if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.');} ?>

<div class="mp_wrapper mp_login_form">
  <?php if(MeprUtils::is_user_logged_in()): ?>

    <?php if(!isset($_GET['mepr-unauth-page']) && (!isset($_GET['action']) || $_GET['action'] != 'mepr_unauthorized')): ?>
      <?php if(is_page($login_page_id) && isset($redirect_to) && !empty($redirect_to)): ?>
        <script type="text/javascript">
          window.location.href="<?php echo urldecode($redirect_to); ?>";
        </script>
      <?php else: ?>
        <div class="mepr-already-logged-in">
          <?php printf(_x('You\'re already logged in. %1$sLogout.%2$s', 'ui', 'memberpress'), '<a href="'. wp_logout_url($redirect_to) . '">', '</a>'); ?>
        </div>
      <?php endif; ?>
    <?php else: ?>
      <?php echo $message; ?>
    <?php endif; ?>

  <?php else: ?>
    <?php echo $message; ?>
    <!-- mp-login-form-start --> <?php //DON'T GET RID OF THIS HTML COMMENT PLEASE IT'S USEFUL FOR SOME REGEX WE'RE DOING ?>
<div class="form-wrap">
	<div class="holder">
	<h2 class="form-title">Login</h2>
		<form name="mepr_loginform" id="mepr_loginform" class="mepr-form" action="<?php echo $login_url; ?>" method="post">
		  <?php /* nonce not necessary on this form seeing as the user isn't logged in yet */ ?>
		  <div class="mp-form-row mepr_username form-group">
			<div class="mp-form-label">
			  <?php $uname_or_email_str = MeprHooks::apply_filters('mepr-login-uname-or-email-str', _x('Email Address', 'ui', 'memberpress')); ?>
			  <?php $uname_str = MeprHooks::apply_filters('mepr-login-uname-str', _x('Username', 'ui', 'memberpress')); ?>
			  <label for="log"><?php echo ($mepr_options->username_is_email)?$uname_or_email_str:$uname_str; ?></label>
			  <?php /* <span class="cc-error"><?php _ex('Username Required', 'ui', 'memberpress'); ?></span> */ ?>
			</div>
			<input type="text" name="log" class="form-control" id="user_login" value="<?php echo (isset($_REQUEST['log'])?$_REQUEST['log']:''); ?>" />
		  </div>
		  <div class="mp-form-row mepr_password form-group">
			<div class="mp-form-label">
			  <label for="pwd"><?php _ex('Password', 'ui', 'memberpress'); ?></label>
			  <?php /* <span class="cc-error"><?php _ex('Password Required', 'ui', 'memberpress'); ?></span> */ ?>
			</div>
			<input type="password" name="pwd" class="form-control" id="user_pass" value="<?php echo (isset($_REQUEST['pwd'])?$_REQUEST['pwd']:''); ?>" />
		  </div>
		  <?php MeprHooks::do_action('mepr-login-form-before-submit'); ?>
		  <!--<div>
			<label><input name="rememberme" type="checkbox" id="rememberme" value="forever"<?php //checked(isset($_REQUEST['rememberme'])); ?> /> <?php //_ex('Remember Me', 'ui', 'memberpress'); ?></label>
		  </div>-->
		  <div class="mp-spacer">&nbsp;</div>
		  <div class="d-sm-flex">
			<div class="">
				<div class="submit">
					<input type="submit" name="wp-submit" id="wp-submit" class="button-primary mepr-share-button btn btn-primary" value="<?php _ex('Log In', 'ui', 'memberpress'); ?>" />
					
					<?php if (strpos(wp_get_referer(), 'register-ltd') !== false){ ?>
					<input type="hidden" name="redirect_to" value="<?php echo esc_html($redirect_to); ?>?from=ltd" />
					<?php }else{ ?>
					<input type="hidden" name="redirect_to" value="<?php echo esc_html($redirect_to); ?>" />
					<?php } ?>
				
					<input type="hidden" name="mepr_process_login_form" value="true" />
					<input type="hidden" name="mepr_is_login_page" value="<?php echo ($is_login_page)?'true':'false'; ?>" />
				</div>
			</div>
		<!--<div class="col">
				<div class="btn-holder text-center text-sm-right">
				  <a href="https://thecampaignmaker.com/chklogin/fb-authorization.php" class="btn btn-fb"><span class="icon-facebook"></span>LOGIN WITH FB</a>
				</div>
			</div>	-->		
          </div>
		  <div class="mepr-login-actions">
			  <a class="link-s1" href="<?php echo $forgot_password_url; ?>"><?php _ex('Forgot Password', 'ui', 'memberpress'); ?></a>
		  </div>
		</form>
	</div>
	  <div class="panel">
    <div class="panel-holder d-sm-flex justify-content-between align-items-center">
      <div class="link-holder text-center text-sm-left">
        <a href="<?php the_field('register_page_link', 'option'); ?>" class="link-s2">Don’t have an account?</a>
      </div>
      <div class="btn-holder text-center text-sm-right">
        <a href="<?php the_field('register_page_link', 'option'); ?>" class="btn btn-secondary">REGISTER NOW</a>
      </div>
    </div>
  </div>
</div>
    <div class="mp-spacer">&nbsp;</div>
    
    <!-- mp-login-form-end --> <?php //DON'T GET RID OF THIS HTML COMMENT PLEASE IT'S USEFUL FOR SOME REGEX WE'RE DOING ?>

  <?php endif; ?>
</div>

