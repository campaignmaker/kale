<?php
/*
Plugin Name: iDevAffiliate Memberpress Integration
Plugin URI: http://www.idevdirect.com/
Description: Memberpress / iDevAffiliate Integration
Version: 2.0
Author: iDevDirect.com LLC
Author URI: http://www.idevdirect.com/
*/

if ( ! defined( 'WPINC' ) ) {
	die();
}

if ( ! class_exists( 'iDevAffiliate_Memberpress' ) ):

	class iDevAffiliate_Memberpress {
		private static $instance;
		private $dir, $url;
		const DS = DIRECTORY_SEPARATOR;
		public static function getInstance() {
			if(self::$instance==null) {
				self::$instance = new self;
				self::$instance->dir = dirname(__FILE__);
				self::$instance->url = WP_PLUGIN_URL . DIRECTORY_SEPARATOR . basename(self::$instance->dir);
				self::$instance->actions();
			}

			return self::$instance;
		}

		private function __construct() {
			;
		}
		/**
		 * call all actions/filters here
		 */
		private function actions() {

			if( is_admin() ) {

				add_action( 'mepr_display_options_tabs', array( $this, 'display_option_tab' ) );
				add_action( 'mepr_display_options',      array( $this, 'display_option_fields' ) );
				add_action( 'mepr-process-options',      array( $this, 'store_option_fields' ) );

				//add_action( 'init', array( $this, 'checkMemberpress' ) );
				add_action( 'admin_init', array( $this, 'check_memberpress' ) );

			}

			//tracking hooks
			//add_action( 'mepr-txn-store', array( $this, 'process_membership2' ), 10, 1 );
			add_action( 'mepr-event-store', array( $this, 'process_membership2' ), 10, 1 );

		}

		public function process_membership2( $e )
		{
			//wp_mail('zikubd@gmail.com', 'memberpress event initial call: server: ' . site_url(), print_r($e, true));
			if ( $e->event == 'transaction-completed' ) {
				//wp_mail('zikubd@gmail.com', 'memberpress event object: server: ' . site_url(), print_r($e, true));

				//check tracking is enable
				$enable = get_option('mepr_idevaffiliates_enable');
				if($enable != 'enable')
					return;

				//get tracking url
				$tracking_url = get_option('mepr_idevaffiliates_url');

				if( !$this->valid_url( $tracking_url ) )
					return;

				$idv_url = rtrim($tracking_url, '/') . '/sale.php';

				//$e->evt_id = $obj->id;
				//$e->evt_id_type = self::$transactions_str;

				$data['profile'] = 125;
				$data['order_type'] = $e->evt_id_type; //can be recurring or single

				//now get transaction object
				if ( $e->evt_id_type == "transactions" ) {
					//get transaction object
					$obj = new MeprTransaction($e->evt_id);

					if ( $obj->status != 'complete' ) {
						return;
					}

					//order id
					$data['idev_ordernum']  = $obj->trans_num;

					// get amount
					$data['idev_saleamt']   = $obj->amount;


					if ( is_numeric($obj->subscription_id) && $obj->subscription_id > 0 )
					{
						//get subscription object
						$subscription = new MeprSubscription( $obj->subscription_id );

						//get ip address
						$data['ip_address'] = $subscription->ip_addr;

					}
					else
					{
						//get ip address
						$data['ip_address'] = $obj->ip_addr;
					}
				}


				//get coupon
				if (  is_numeric($obj->coupon_id) && $obj->coupon_id > 0 )
				{
					$coupons = new MeprCoupon( $obj->coupon_id );

					if( $coupons->post_title ) {
						$data['coupon_code']    = $coupons->post_title;
					}
				}

				if ( floatval($data['idev_saleamt']) > 0.00 ) {
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $idv_url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					$json = curl_exec($ch);
					curl_close($ch);
				}

				//wp_mail('zikubd@gmail.com', 'Memberpress Pursed Data: Server: ' . site_url(), print_r($data,true) . " URL: $idv_url");

			}
		}

		public function check_memberpress() {
			$plugin = plugin_basename( __FILE__ );
			$plugin_data = get_plugin_data( __FILE__, false );

			if ( !class_exists( 'MeprTransaction' ) ) {
				if( is_plugin_active($plugin) ) {
					deactivate_plugins( $plugin );
				}
			}
		}

		public function display_option_tab() {
			?>
			<a class="nav-tab" id="idevaffiliate" href="#"><?php _e('iDevAffliate', 'memberpress'); ?></a>
			<?php
		}

		public function display_option_fields() {
			$enable = get_option('mepr_idevaffiliates_enable');
			$url    = get_option('mepr_idevaffiliates_url');
		?>
			<div id="idevaffiliate" class="mepr-options-hidden-pane">
				<h3><?php _e('iDevAffiliates Settings', 'memberpress'); ?></h3>
				<table class="mepr-options-pane">
					<tbody>
					<tr valign="top">
						<th scope="row">
							<label for="mepr_idevaffiliates_enable"><?php _e('Enable Tracking', 'memberpress'); ?>: </label>
							<?php MeprAppHelper::info_tooltip('mepr-options-idevaffiliate-enable',
								__( 'Enable iDevAffiliate Tracking', 'memberpress' ),
								__( 'Select Enable to enable tracking and select Disable to disable tracking.', 'memberpress' ) );
							?>
						</th>
						<td>
							<select name="mepr_idevaffiliates_enable" id="mepr_idevaffiliates_enable">
								<option value="enable" <?php echo $enable == 'enable' ? 'selected="selected"': ''; ?>><?php _e('Enable', 'memberpress'); ?></option>
								<option value="disable" <?php echo $enable == 'disable' ? 'selected="selected"': ''; ?> ><?php _e('Disable', 'memberpress'); ?></option>
							</select>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<label for="mepr_idevaffiliates_url"><?php _e('Tracking URL', 'memberpress'); ?>: </label>
							<?php MeprAppHelper::info_tooltip('mepr-options-idevaffiliates-url',
								__('iDevAffiliate Installation URL', 'memberpress'),
								__('Enter iDevaffiliate installation Site URL.<br />Example: http://www.yoursite.com/idevaffiliate/<br />Note: Include the trailing slash (/).', 'memberpress'));
							?>
						</th>
						<td>
							<input type="text" id="mepr_idevaffiliates_url" name="mepr_idevaffiliates_url" value="<?php echo $url; ?>" class="regular-text" />
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		<?php
		}

		public function store_option_fields() {
			if ( isset($_POST['mepr_idevaffiliates_url']) ) {
				$url = esc_url($_POST['mepr_idevaffiliates_url']);
				update_option('mepr_idevaffiliates_url', $url);
			}

			if ( isset($_POST['mepr_idevaffiliates_enable']) ) {
				$enable = $_POST['mepr_idevaffiliates_enable'];

				if(in_array($enable, array('enable', 'disable'))) {
					update_option('mepr_idevaffiliates_enable', $enable);
				}

			}
		}

		public function pr($data) {
			echo "<pre>"; print_r($data); echo "</pre>";
		}

		public function valid_url( $url ) {
			$regex = "((https?|ftp)\:\/\/)?"; // SCHEME
			$regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
			$regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
			$regex .= "(\:[0-9]{2,5})?"; // Port
			$regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
			$regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
			$regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

			if (preg_match("/^$regex$/", $url)) {
				return true;
			} else {
				return false;
			}
		}

	}

	iDevAffiliate_Memberpress::getInstance();

endif;