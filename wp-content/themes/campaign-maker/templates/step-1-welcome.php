<?php
/**
 * Template Name: Step 1 : Welcome
 */
?>
<?php if ( is_user_logged_in() ) {  get_header('steps');
 ?>

<div class="step-bar">
  <div class="holder d-flex flex-wrap">
    <div class="step active">
      <strong class="step-name">Welcome</strong>
    </div>
    <div class="step">
      <strong class="step-name">Ebook</strong>
    </div>
    <div class="step">
      <strong class="step-name">Analyze</strong>
    </div>
    <div class="step">
      <strong class="step-name">Create</strong>
    </div>
    <div class="step">
      <strong class="step-name">Optimize</strong>
    </div>
    <div class="step">
      <strong class="step-name">Report</strong>
    </div>
  </div>
</div>

<div class="main-content">
  <div class="container">
    <div class="center-box">
      <div class="holder">
        <h2 class="screen-heading text-center">Welcome <?php echo  $_SESSION['logged_in']['first_name']; ?></h2>
        <div class="text-holder text-center">
          <p>We at The Campaign Maker wanted to thank you for taking the time to try our suite of tools that eradicate wasteful ads and improve your overall ROAS.</p>
          <p>We are proud to have you as our client and we will make sure to make it the best we can for you.</p>
          <p>Your Facebook account is now 100% integrated with our software, so brace yourself for an epic journey of Facebook ads gold digging.</p>
        </div>
        <div class="btn-holder text-center">
          <a href="<?php the_field('step_2_link', 'option'); ?>" class="btn btn-next">
            <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-next-pointer.svg" alt="pointer"></span>
            Next Step
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer('steps'); ?>  <?php
} else { header('Location: '.home_url().'/login');  } ?>

<!-- FB Lead -->
<script>
  fbq('track', 'Lead');
</script>
<!-- FB Lead end -->