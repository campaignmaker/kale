<?php
/**
 * Template Name: Step 2 : Ebook
 */
?>
<?php if ( is_user_logged_in() ) { get_header('steps'); ?>

<div class="step-bar">
  <div class="holder d-flex flex-wrap">
    <div class="step done">
      <strong class="step-name">Welcome</strong>
    </div>
    <div class="step active">
      <strong class="step-name">Ebook</strong>
    </div>
    <div class="step">
      <strong class="step-name">Analyze</strong>
    </div>
    <div class="step">
      <strong class="step-name">Create</strong>
    </div>
    <div class="step">
      <strong class="step-name">Optimize</strong>
    </div>
    <div class="step">
      <strong class="step-name">Report</strong>
    </div>
  </div>
</div>
<div class="main-content s1">
  <div class="container">
    <div class="center-box s1">
      <div class="holder s1">
        <h2 class="screen-heading text-center">Download Our Ebook!</h2>
        <div class="text-holder text-center">
          <p>To help you get started with Facebook ads I wrote this small ebook explaining the basics of Facebook ads and how it works.</p>
          <p>The ebook is a bit outdated but still holds true to the basics of how it works, but don't worry, I am working on a brand new ebook that will outline methods I genuinely use every day to improve my campaigns (hint: it focuses on the ACOR method).</p>
        </div>
        <div class="btn-holder text-center">
          <a href="https://gallery.mailchimp.com/a9f632539467cbc1aad82bb67/files/Successful_Facebook_Advertising_Methods.02.pdf" target="_blank" class="btn btn-download">
            <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-save.svg" alt="save"></span>
            Download
          </a>
        </div>
        <div class="btn-holder s1 text-center d-flex flex-wrap justify-content-center">
          <div class="btn-col">
            <a href="<?php the_field('step_1_link', 'option'); ?>" class="btn btn-prev">
              <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-prev-pointer.svg" alt="pointer"></span>
              Previous Step
            </a>
          </div>
          <div class="btn-col">
            <a href="<?php the_field('step_3_link', 'option'); ?>" class="btn btn-next">
              <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-pointer.svg" alt="pointer"></span>
              Next Step
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer('steps'); ?>  <?php
} else { header('Location: '.home_url().'/login');  } ?>