<?php
/**
 * Template Name: Step 3 : Analyze
 */
?>
<?php if ( is_user_logged_in() ) { get_header('steps'); ?>

<div class="step-bar">
  <div class="holder d-flex flex-wrap">
    <div class="step done">
      <strong class="step-name">Welcome</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Ebook</strong>
    </div>
    <div class="step active">
      <strong class="step-name">Analyze</strong>
    </div>
    <div class="step">
      <strong class="step-name">Create</strong>
    </div>
    <div class="step">
      <strong class="step-name">Optimize</strong>
    </div>
    <div class="step">
      <strong class="step-name">Report</strong>
    </div>
  </div>
</div>
<div class="main-content s1">
  <div class="container">
    <div class="center-box s2">
      <div class="holder">
        <h2 class="screen-heading text-center">Analyzing Your Past Campaigns</h2>
        <div class="text-holder text-center">
          <p>First and foremost is to analyze your past adsets for golden nuggets of information you can use to create and optimize for your new campaigns.</p>
          <p>The analysis tool helps breakdown your past adsets to see which demographic was giving you the highest return on your ad spend.</p>
          <p>Try it yourself and see how you can use the analysis tool to find them.</p>
        </div>
       <!-- <div class="video-holder">
          <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/video-player.png" alt="video">
        </div> -->
		 <div class="video-holder">
          <img class="img-fluid" src="https://thecampaignmaker.com/wp-content/uploads/2018/05/browser01.png" alt="video">
        </div> 
        <div class="btn-holder s1 text-center d-flex flex-wrap justify-content-center">
          <div class="btn-col">
            <a href="<?php the_field('step_2_link', 'option'); ?>" class="btn btn-prev">
              <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-prev-pointer.svg" alt="pointer"></span>
              Previous Step
            </a>
          </div>
          <div class="btn-col">
            <a href="<?php the_field('step_4_link', 'option'); ?>" class="btn btn-next">
              <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-pointer.svg" alt="pointer"></span>
              Next Step
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer('steps'); ?>  <?php
} else { header('Location: '.home_url().'/login');  } ?>