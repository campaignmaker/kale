<?php
/**
 * Template Name: Step 6 : Report
 */
?>
<?php if ( is_user_logged_in() ) { get_header('steps'); ?>

<div class="step-bar">
  <div class="holder d-flex flex-wrap">
    <div class="step done">
      <strong class="step-name">Welcome</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Ebook</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Analyze</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Create</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Optimize</strong>
    </div>
    <div class="step active">
      <strong class="step-name">Report</strong>
    </div>
  </div>
</div>
<div class="main-content s1">
  <div class="container">
    <div class="center-box s2">
      <div class="holder">
        <h2 class="screen-heading text-center">Reviewing Your Running Campaigns</h2>
        <div class="text-holder text-center">
          <p>Reviewing your campaigns, adsets and ads are now a breeze and simple to find laser targeted data that would otherwise be hidden in your Facebook manager dashboard.</p>
          <p>We created the reporting tool for two scenarios in mind. People who want a quick glance on how their campaigns are doing OR for people who want in depth review of each individual ad is doing.</p>
        </div>
        <!--<div class="video-holder">
          <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/video-player.png" alt="video">
        </div> -->
		<div class="video-holder">
          <img class="img-fluid" src="https://thecampaignmaker.com/wp-content/uploads/2018/06/Browser-2.png" alt="video">
        </div>
        <div class="btn-holder s1 text-center d-flex flex-wrap justify-content-center">
          <div class="btn-col">
            <a href="<?php the_field('step_5_link', 'option'); ?>" class="btn btn-prev">
              <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-prev-pointer.svg" alt="pointer"></span>
              Previous Step
            </a>
          </div>
          <div class="btn-col">
            <a href="<?php the_field('step_7_link', 'option'); ?>" class="btn btn-next">
              <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-pointer.svg" alt="pointer"></span>
              Next Step
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer('steps'); ?>  <?php
} else { header('Location: '.home_url().'/login');  } ?>