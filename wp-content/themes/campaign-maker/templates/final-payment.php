<?php
/**
 * Template Name: Final Payment
 */
?>
<?php
if ( is_user_logged_in() ) {
get_header('basic'); 
$ads = $wpdb->get_results("SELECT sum(amount_spent) as ttl from user_ad_account where user_id IN (select id from users where email ='".$_SESSION['user']->data->user_email."')");
$ad_spend_amount=$ads[0]->ttl;
$user_data = $wpdb->get_results("select * from users where email ='".$_SESSION['user']->data->user_email."'");
if(isset($user_data[0]->accesstoken)){
$url = "https://graph.facebook.com/v3.0/me/adaccounts?fields=id,account_id,amount_spent,insights.date_preset(last_30d)&access_token=" .$user_data[0]->accesstoken;
$ch = curl_init($url);
 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
			$error = $response['error']['message'];
            
			if(!$error){
				$ad_spend_amount=0;
				foreach($response['data'] as $k => $v){
					if(isset($v['insights']['data'][0]['spend'])){
						$ad_spend_amount+=$v['insights']['data'][0]['spend'];
					}
				}
			}
}
?>
<style>
.mepr-loading-gif{
 margin-left: -35px;
}
.form-wrap {
	max-width: 100%;
}
 .form-wrap .holder.s1 {
	border: 0px;
	padding: 20px 15px;
}
.form-wrap #mepr_payment_method .mp-form-row  {
	max-width: 50%;
	float: left;
	
	padding: 20px;
}
#mepr_payment_method .mp-form-row .mepr-payment-method {
	min-height: 110px;
	background:#f2f6f9;
	transition-duration:.35s;
	border-radius:43px;
	padding: 20px 20px 20px 80px;
	position:relative;
	cursor:pointer;
}
<!--#mepr_payment_method .mp-form-row .mepr-payment-method:hover{ box-shadow: 0 0 30px #0073fa;}-->

#mepr_payment_method .mp-form-row .mepr-payment-method::before {
	content: "";
	display: block;
	position: absolute;
	height: 60px;
	width: 60px;
	left: 30px;
	top: 22px;background-size: 100% !important;
}

#mepr_payment_method .mp-checkbox-field::before {
	position: absolute;
	height: 100%;
	width: 100%;
	content: "";
	display: block;
	left: 0;
	top: 0;
}
#mepr_payment_method .mepr-submit.btn.btn-primary {
	font-size: 15px;
}
#mepr_payment_method .mepr-payment-method-label-text {
	display: none;
}
#mepr_payment_method .mp-form-row:first-child .mepr-payment-method::before{background:url('https://thecampaignmaker.com/kale/wp-content/uploads/2018/08/stripe.png')no-repeat;}

#mepr_payment_method .mp-form-row:nth-child(2) .mepr-payment-method::before{background:url('https://thecampaignmaker.com/kale/wp-content/uploads/2018/08/paypal.png')no-repeat;}

@media only screen and (min-width:100px) and (max-width:768px)
{
	#mepr_payment_method .mp-form-row .mepr-payment-method {
	padding: 80px 20px 20px 20px;}
 .form-wrap #mepr_payment_method .mp-form-row {max-width: 100%; margin: 20px auto;}
 .holder.s1 {padding: 0 !important;}
 #mepr_payment_method .mp-form-row .mepr-payment-method::before {left: 0;top: 10px !important;right: 0;margin: 0 auto;}
 .form-wrap #mepr_payment_method .mp-form-row {padding:0px !important;}
#mepr_payment_method .mp-form-row .mepr-payment-method::before {left: 18px;	top: 47px;}
}
.mepr-payment-method-desc-text{ font-size: 14;}
</style>
<div class="center-box s4">
  <div class="holder s1">
    <h2 class="screen-heading text-center">
      Based On Your Adspend You Qualify For The Following Offer
      <span class="sub-heading">You can <strong class="highlight-text">lock in this price</strong> for the full year even if your ad spend goes up!</span>
        <span class="sub-heading">The pricing offered is based on your past 30 day spend with Facebook ads which is <strong class="highlight-text"> $ <?php echo number_format($ad_spend_amount,2); ?></strong></span>
    </h2>
    <div class="plan-holder text-center">
      <div class="plan-row d-flex flex-wrap">
        <div class="plan d-flex flex-wrap">
          <div class="btn-radio-custom s4 d-flex flex-wrap">
            <input type="radio" name="plan" id="monthly" onchange="document.getElementById('monthlypay').style='display:block;';document.getElementById('yearlypay').style='display:none;'">
            <label class="d-flex flex-wrap align-items-center" for="monthly">
              <span class="radio-text-holder">
                <span class="plan-title">Monthly Renewal</span>
                <?php if($ad_spend_amount>=0 && $ad_spend_amount<=999){ ?>
                <strong class="price">$9</strong>
                <span class="paid-title">Paid Monthly</span>
                 <span class="offer">One Month at a Time</span>
                 <?php }else
                  if($ad_spend_amount>=1000 && $ad_spend_amount<=2999){ ?>
                <strong class="price">$29</strong>
                <span class="paid-title">Paid Monthly</span>
                 <span class="offer">One Month at a Time</span>
              
                  <?php }else
                  if($ad_spend_amount>=3000 && $ad_spend_amount<=10000){ ?>
                <strong class="price">$69</strong>
                <span class="paid-title">Paid Monthly</span>
                 <span class="offer">One Month at a Time</span>
               
                  <?php }else
                  if($ad_spend_amount>10000){ ?>
                <strong class="price">$149</strong>
                <span class="paid-title">Paid Monthly</span>
                 <span class="offer">One Month at a Time</span>
                 <?php } ?>
              </span>
            </label>
          </div>
        </div>
        <div class="plan d-flex flex-wrap">
          <div class="btn-radio-custom s4 d-flex flex-wrap">
            <input type="radio" checked="" name="plan" id="yearly" onchange="document.getElementById('monthlypay').style='display:none;';document.getElementById('yearlypay').style='display:block;'">
            <label class="d-flex flex-wrap align-items-center" for="yearly">
              <span class="radio-text-holder">
                <span class="plan-title">Yearly Renewal</span>
                   <?php if($ad_spend_amount>=0 && $ad_spend_amount<=999){ ?>
                <strong class="price">$90</strong>
                <span class="paid-title">Paid Yearly</span>
                 <span class="offer">Get 2 months free + Lock in price</span>
                 <?php }else
                  if($ad_spend_amount>=1000 && $ad_spend_amount<=2999){ ?>
                <strong class="price">$290</strong>
                <span class="paid-title">Paid Yearly</span>
                 <span class="offer">Get 2 months free + Lock in price</span>
              
                  <?php }else
                  if($ad_spend_amount>=3000 && $ad_spend_amount<=10000){ ?>
                <strong class="price">$690</strong>
                <span class="paid-title">Paid Yearly</span>
                 <span class="offer">Get 2 months free + Lock in price</span>
               
                  <?php }else
                  if($ad_spend_amount>10000){ ?>
                <strong class="price">$1490</strong>
                <span class="paid-title">Paid Yearly</span>
                 <span class="offer">Get 2 months free + Lock in price</span>
                 <?php } ?>
              </span>
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="features">
      <h2 class="feature-heading">What’s Included</h2>
      <div class="holder s1">
        <div class="item-holder d-flex flex-wrap">
          <div class="feature-item d-flex flex-wrap">
            <div class="item-wrap d-flex flex-wrap align-items-center">
              <div class="icon-holder">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-crown-rounded.svg" alt="crown">
              </div>
              <div class="text">
                <span>Access to All Tools</span>
              </div>
            </div>
          </div>
          <div class="feature-item d-flex flex-wrap">
            <div class="item-wrap d-flex flex-wrap align-items-center">
              <div class="icon-holder">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-megaphone-rounded.svg" alt="megaphone">
              </div>
              <div class="text">
                <span>Unlimited Ad Accounts</span>
              </div>
            </div>
          </div>
          <div class="feature-item d-flex flex-wrap">
            <div class="item-wrap d-flex flex-wrap align-items-center">
              <div class="icon-holder">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-heart-rounded.svg" alt="heart">
              </div>
              <div class="text">
                <span>Premium Support</span>
              </div>
            </div>
          </div>
          <div class="feature-item d-flex flex-wrap">
            <div class="item-wrap d-flex flex-wrap align-items-center">
              <div class="icon-holder">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-gift-rounded.svg" alt="gift">
              </div>
              <div class="text">
                <span>Free 7 Day Trial</span>
              </div>
            </div>
          </div>
          <div class="feature-item d-flex flex-wrap">
            <div class="item-wrap d-flex flex-wrap align-items-center">
              <div class="icon-holder">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-dimond-rounded.svg" alt="dimond">
              </div>
              <div class="text">
                <span>Cancel Anytime</span>
              </div>
            </div>
          </div>
          <div class="feature-item d-flex flex-wrap">
            <div class="item-wrap d-flex flex-wrap align-items-center">
              <div class="icon-holder">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-trophy-rounded.svg" alt="trophy">
              </div>
              <div class="text">
                <span>30 Day Refund Guarantee</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="credit-card-details">
        

           <?php  if($ad_spend_amount>=0 && $ad_spend_amount<=999){ ?>
               <div id="monthlypay" style="display:none;"><?php echo do_shortcode('[mepr-membership-registration-form id="334"]'); ?></div>
                <div id="yearlypay"><?php echo do_shortcode('[mepr-membership-registration-form id="335"]'); ?></div>
                 <?php }else
                  if($ad_spend_amount>=1000 && $ad_spend_amount<=2999){ ?>
               <div id="monthlypay" style="display:none;"><?php echo do_shortcode('[mepr-membership-registration-form id="338"]'); ?></div>
                <div id="yearlypay"><?php echo do_shortcode('[mepr-membership-registration-form id="339"]'); ?></div>
              
                  <?php }else
                  if($ad_spend_amount>=3000 && $ad_spend_amount<=10000){ ?>
              <div id="monthlypay" style="display:none;"><?php echo do_shortcode('[mepr-membership-registration-form id="336"]'); ?></div>
                <div id="yearlypay"><?php echo do_shortcode('[mepr-membership-registration-form id="337"]'); ?></div>
               
                  <?php }else
                  if($ad_spend_amount>10000){ ?>
              <div id="monthlypay" style="display:none;"><?php echo do_shortcode('[mepr-membership-registration-form id="340"]'); ?></div>
                <div id="yearlypay"><?php echo do_shortcode('[mepr-membership-registration-form id="341"]'); ?></div>
                 <?php }  ?>

    </div>
  </div>
</div>

  </div>
</div>

			</main>
<?php //get_footer('basic'); ?>
<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<nav class="footer-nav">
                <ul id="menu-footer-menu" class="d-flex flex-wrap justify-content-center justify-content-md-start text-center"><li class="nav-item menu-terms-of-service"><a class="nav-link"href="https://thecampaignmaker.com/blog/terms-of-service/">Terms of Service</a></li>
<li class="nav-item menu-privacy-policy"><a class="nav-link"href="https://www.iubenda.com/privacy-policy/95975692">Privacy Policy</a></li>
<li class="nav-item menu-help"><a class="nav-link"href="https://thecampaignmaker.com/help">Help</a></li>
</ul>							</nav>
            </div>
                          <div class="col-md-4">
                <div class="made-with text-center text-md-right">
                  <p>Made with &lt;3 <a href="#">by FUBSZ</a></p>
                </div>
              </div>
            					</div>
				</div>
			</footer>
		</div>
  </div>
 <script src="<?php echo home_url(); ?>/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>
<script src="<?php echo home_url(); ?>/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
<script src="<?php echo home_url(); ?>/wp-content/plugins/memberpress/js/jquery.payment.js?ver=4.9.6"></script>
<script src="<?php echo home_url(); ?>/wp-content/plugins/memberpress/js/validate.js?ver=4.9.6"></script>
<script type='text/javascript'>
/* <![CDATA[ */
var MeprI18n = {"states":{"CA":{"AB":"Alberta","BC":"British Columbia","MB":"Manitoba","NB":"New Brunswick","NL":"Newfoundland","NT":"Northwest Territories","NS":"Nova Scotia","NU":"Nunavut","ON":"Ontario","PE":"Prince Edward Island","QC":"Quebec","SK":"Saskatchewan","YT":"Yukon Territory"},"JP":{"JP01":"Hokkaido","JP02":"Aomori","JP03":"Iwate","JP04":"Miyagi","JP05":"Akita","JP06":"Yamagata","JP07":"Fukushima","JP08":"Ibaraki","JP09":"Tochigi","JP10":"Gunma","JP11":"Saitama","JP12":"Chiba","JP13":"Tokyo","JP14":"Kanagawa","JP15":"Niigata","JP16":"Toyama","JP17":"Ishikawa","JP18":"Fukui","JP19":"Yamanashi","JP20":"Nagano","JP21":"Gifu","JP22":"Shizuoka","JP23":"Aichi","JP24":"Mie","JP25":"Shiga","JP26":"Kyouto","JP27":"Osaka","JP28":"Hyougo","JP29":"Nara","JP30":"Wakayama","JP31":"Tottori","JP32":"Shimane","JP33":"Okayama","JP34":"Hiroshima","JP35":"Yamaguchi","JP36":"Tokushima","JP37":"Kagawa","JP38":"Ehime","JP39":"Kochi","JP40":"Fukuoka","JP41":"Saga","JP42":"Nagasaki","JP43":"Kumamoto","JP44":"Oita","JP45":"Miyazaki","JP46":"Kagoshima","JP47":"Okinawa"},"HK":{"HONG KONG":"Hong Kong Island","KOWLOON":"Kowloon","NEW TERRITORIES":"New Territories"},"US":{"AL":"Alabama","AK":"Alaska","AZ":"Arizona","AR":"Arkansas","CA":"California","CO":"Colorado","CT":"Connecticut","DE":"Delaware","DC":"District Of Columbia","FL":"Florida","GA":"Georgia","HI":"Hawaii","ID":"Idaho","IL":"Illinois","IN":"Indiana","IA":"Iowa","KS":"Kansas","KY":"Kentucky","LA":"Louisiana","ME":"Maine","MD":"Maryland","MA":"Massachusetts","MI":"Michigan","MN":"Minnesota","MS":"Mississippi","MO":"Missouri","MT":"Montana","NE":"Nebraska","NV":"Nevada","NH":"New Hampshire","NJ":"New Jersey","NM":"New Mexico","NY":"New York","NC":"North Carolina","ND":"North Dakota","OH":"Ohio","OK":"Oklahoma","OR":"Oregon","PA":"Pennsylvania","RI":"Rhode Island","SC":"South Carolina","SD":"South Dakota","TN":"Tennessee","TX":"Texas","UT":"Utah","VT":"Vermont","VA":"Virginia","WA":"Washington","WV":"West Virginia","WI":"Wisconsin","WY":"Wyoming","AA":"Armed Forces (AA)","AE":"Armed Forces (AE)","AP":"Armed Forces (AP)","AS":"American Samoa","GU":"Guam","MP":"Northern Mariana Islands","PR":"Puerto Rico","UM":"US Minor Outlying Islands","VI":"US Virgin Islands"},"CN":{"CN1":"Yunnan \/ &#20113;&#21335;","CN2":"Beijing \/ &#21271;&#20140;","CN3":"Tianjin \/ &#22825;&#27941;","CN4":"Hebei \/ &#27827;&#21271;","CN5":"Shanxi \/ &#23665;&#35199;","CN6":"Inner Mongolia \/ &#20839;&#33945;&#21476;","CN7":"Liaoning \/ &#36797;&#23425;","CN8":"Jilin \/ &#21513;&#26519;","CN9":"Heilongjiang \/ &#40657;&#40857;&#27743;","CN10":"Shanghai \/ &#19978;&#28023;","CN11":"Jiangsu \/ &#27743;&#33487;","CN12":"Zhejiang \/ &#27993;&#27743;","CN13":"Anhui \/ &#23433;&#24509;","CN14":"Fujian \/ &#31119;&#24314;","CN15":"Jiangxi \/ &#27743;&#35199;","CN16":"Shandong \/ &#23665;&#19996;","CN17":"Henan \/ &#27827;&#21335;","CN18":"Hubei \/ &#28246;&#21271;","CN19":"Hunan \/ &#28246;&#21335;","CN20":"Guangdong \/ &#24191;&#19996;","CN21":"Guangxi Zhuang \/ &#24191;&#35199;&#22766;&#26063;","CN22":"Hainan \/ &#28023;&#21335;","CN23":"Chongqing \/ &#37325;&#24198;","CN24":"Sichuan \/ &#22235;&#24029;","CN25":"Guizhou \/ &#36149;&#24030;","CN26":"Shaanxi \/ &#38485;&#35199;","CN27":"Gansu \/ &#29976;&#32899;","CN28":"Qinghai \/ &#38738;&#28023;","CN29":"Ningxia Hui \/ &#23425;&#22799;","CN30":"Macau \/ &#28595;&#38376;","CN31":"Tibet \/ &#35199;&#34255;","CN32":"Xinjiang \/ &#26032;&#30086;"},"MY":{"JHR":"Johor","KDH":"Kedah","KTN":"Kelantan","MLK":"Melaka","NSN":"Negeri Sembilan","PHG":"Pahang","PRK":"Perak","PLS":"Perlis","PNG":"Pulau Pinang","SBH":"Sabah","SWK":"Sarawak","SGR":"Selangor","TRG":"Terengganu","KUL":"W.P. Kuala Lumpur","LBN":"W.P. Labuan","PJY":"W.P. Putrajaya"},"PE":{"CAL":"El Callao","LMA":"Municipalidad Metropolitana de Lima","AMA":"Amazonas","ANC":"Ancash","APU":"Apur&iacute;mac","ARE":"Arequipa","AYA":"Ayacucho","CAJ":"Cajamarca","CUS":"Cusco","HUV":"Huancavelica","HUC":"Hu&aacute;nuco","ICA":"Ica","JUN":"Jun&iacute;n","LAL":"La Libertad","LAM":"Lambayeque","LIM":"Lima","LOR":"Loreto","MDD":"Madre de Dios","MOQ":"Moquegua","PAS":"Pasco","PIU":"Piura","PUN":"Puno","SAM":"San Mart&iacute;n","TAC":"Tacna","TUM":"Tumbes","UCA":"Ucayali"},"AU":{"ACT":"Australian Capital Territory","NSW":"New South Wales","NT":"Northern Territory","QLD":"Queensland","SA":"South Australia","TAS":"Tasmania","VIC":"Victoria","WA":"Western Australia"},"NP":{"ILL":"Illam","JHA":"Jhapa","PAN":"Panchthar","TAP":"Taplejung","BHO":"Bhojpur","DKA":"Dhankuta","MOR":"Morang","SUN":"Sunsari","SAN":"Sankhuwa","TER":"Terhathum","KHO":"Khotang","OKH":"Okhaldhunga","SAP":"Saptari","SIR":"Siraha","SOL":"Solukhumbu","UDA":"Udayapur","DHA":"Dhanusa","DLK":"Dolakha","MOH":"Mohottari","RAM":"Ramechha","SAR":"Sarlahi","SIN":"Sindhuli","BHA":"Bhaktapur","DHD":"Dhading","KTM":"Kathmandu","KAV":"Kavrepalanchowk","LAL":"Lalitpur","NUW":"Nuwakot","RAS":"Rasuwa","SPC":"Sindhupalchowk","BAR":"Bara","CHI":"Chitwan","MAK":"Makwanpur","PAR":"Parsa","RAU":"Rautahat","GOR":"Gorkha","KAS":"Kaski","LAM":"Lamjung","MAN":"Manang","SYN":"Syangja","TAN":"Tanahun","BAG":"Baglung","PBT":"Parbat","MUS":"Mustang","MYG":"Myagdi","AGR":"Agrghakanchi","GUL":"Gulmi","KAP":"Kapilbastu","NAW":"Nawalparasi","PAL":"Palpa","RUP":"Rupandehi","DAN":"Dang","PYU":"Pyuthan","ROL":"Rolpa","RUK":"Rukum","SAL":"Salyan","BAN":"Banke","BDA":"Bardiya","DAI":"Dailekh","JAJ":"Jajarkot","SUR":"Surkhet","DOL":"Dolpa","HUM":"Humla","JUM":"Jumla","KAL":"Kalikot","MUG":"Mugu","ACH":"Achham","BJH":"Bajhang","BJU":"Bajura","DOT":"Doti","KAI":"Kailali","BAI":"Baitadi","DAD":"Dadeldhura","DAR":"Darchula","KAN":"Kanchanpur"},"DE":{"BW":"Baden-W\u00fcrttemberg","BY":"Bavaria","BE":"Berlin","BB":"Brandenburg","HB":"Bremen","HH":"Hamburg","HE":"Hesse","NI":"Lower Saxony","MV":"Mecklenburg-Vorpommern","NW":"North Rhine-Westphalia","RP":"Rhineland-Palatinate","SL":"Saarland","SN":"Saxony","ST":"Saxony-Anhalt","SH":"Schleswig-Holstein","TH":"Thuringia"},"BR":{"AC":"Acre","AL":"Alagoas","AP":"Amap&aacute;","AM":"Amazonas","BA":"Bahia","CE":"Cear&aacute;","DF":"Distrito Federal","ES":"Esp&iacute;rito Santo","GO":"Goi&aacute;s","MA":"Maranh&atilde;o","MT":"Mato Grosso","MS":"Mato Grosso do Sul","MG":"Minas Gerais","PA":"Par&aacute;","PB":"Para&iacute;ba","PR":"Paran&aacute;","PE":"Pernambuco","PI":"Piau&iacute;","RJ":"Rio de Janeiro","RN":"Rio Grande do Norte","RS":"Rio Grande do Sul","RO":"Rond&ocirc;nia","RR":"Roraima","SC":"Santa Catarina","SP":"S&atilde;o Paulo","SE":"Sergipe","TO":"Tocantins"},"IR":{"KHZ":"Khuzestan  (\u062e\u0648\u0632\u0633\u062a\u0627\u0646)","THR":"Tehran  (\u062a\u0647\u0631\u0627\u0646)","ILM":"Ilaam (\u0627\u06cc\u0644\u0627\u0645)","BHR":"Bushehr (\u0628\u0648\u0634\u0647\u0631)","ADL":"Ardabil (\u0627\u0631\u062f\u0628\u06cc\u0644)","ESF":"Isfahan (\u0627\u0635\u0641\u0647\u0627\u0646)","YZD":"Yazd (\u06cc\u0632\u062f)","KRH":"Kermanshah (\u06a9\u0631\u0645\u0627\u0646\u0634\u0627\u0647)","KRN":"Kerman (\u06a9\u0631\u0645\u0627\u0646)","HDN":"Hamadan (\u0647\u0645\u062f\u0627\u0646)","GZN":"Ghazvin (\u0642\u0632\u0648\u06cc\u0646)","ZJN":"Zanjan (\u0632\u0646\u062c\u0627\u0646)","LRS":"Luristan (\u0644\u0631\u0633\u062a\u0627\u0646)","ABZ":"Alborz (\u0627\u0644\u0628\u0631\u0632)","EAZ":"East Azarbaijan (\u0622\u0630\u0631\u0628\u0627\u06cc\u062c\u0627\u0646 \u0634\u0631\u0642\u06cc)","WAZ":"West Azarbaijan (\u0622\u0630\u0631\u0628\u0627\u06cc\u062c\u0627\u0646 \u063a\u0631\u0628\u06cc)","CHB":"Chaharmahal and Bakhtiari (\u0686\u0647\u0627\u0631\u0645\u062d\u0627\u0644 \u0648 \u0628\u062e\u062a\u06cc\u0627\u0631\u06cc)","SKH":"South Khorasan (\u062e\u0631\u0627\u0633\u0627\u0646 \u062c\u0646\u0648\u0628\u06cc)","RKH":"Razavi Khorasan (\u062e\u0631\u0627\u0633\u0627\u0646 \u0631\u0636\u0648\u06cc)","NKH":"North Khorasan (\u062e\u0631\u0627\u0633\u0627\u0646 \u062c\u0646\u0648\u0628\u06cc)","SMN":"Semnan (\u0633\u0645\u0646\u0627\u0646)","FRS":"Fars (\u0641\u0627\u0631\u0633)","QHM":"Qom (\u0642\u0645)","KRD":"Kurdistan \/ \u06a9\u0631\u062f\u0633\u062a\u0627\u0646)","KBD":"Kohgiluyeh and BoyerAhmad (\u06a9\u0647\u06af\u06cc\u0644\u0648\u06cc\u06cc\u0647 \u0648 \u0628\u0648\u06cc\u0631\u0627\u062d\u0645\u062f)","GLS":"Golestan (\u06af\u0644\u0633\u062a\u0627\u0646)","GIL":"Gilan (\u06af\u06cc\u0644\u0627\u0646)","MZN":"Mazandaran (\u0645\u0627\u0632\u0646\u062f\u0631\u0627\u0646)","MKZ":"Markazi (\u0645\u0631\u06a9\u0632\u06cc)","HRZ":"Hormozgan (\u0647\u0631\u0645\u0632\u06af\u0627\u0646)","SBN":"Sistan and Baluchestan (\u0633\u06cc\u0633\u062a\u0627\u0646 \u0648 \u0628\u0644\u0648\u0686\u0633\u062a\u0627\u0646)"},"BG":{"BG-01":"Blagoevgrad","BG-02":"Burgas","BG-08":"Dobrich","BG-07":"Gabrovo","BG-26":"Haskovo","BG-09":"Kardzhali","BG-10":"Kyustendil","BG-11":"Lovech","BG-12":"Montana","BG-13":"Pazardzhik","BG-14":"Pernik","BG-15":"Pleven","BG-16":"Plovdiv","BG-17":"Razgrad","BG-18":"Ruse","BG-27":"Shumen","BG-19":"Silistra","BG-20":"Sliven","BG-21":"Smolyan","BG-23":"Sofia","BG-22":"Sofia-Grad","BG-24":"Stara Zagora","BG-25":"Targovishte","BG-03":"Varna","BG-04":"Veliko Tarnovo","BG-05":"Vidin","BG-06":"Vratsa","BG-28":"Yambol"},"IT":{"AG":"Agrigento","AL":"Alessandria","AN":"Ancona","AO":"Aosta","AR":"Arezzo","AP":"Ascoli Piceno","AT":"Asti","AV":"Avellino","BA":"Bari","BT":"Barletta-Andria-Trani","BL":"Belluno","BN":"Benevento","BG":"Bergamo","BI":"Biella","BO":"Bologna","BZ":"Bolzano","BS":"Brescia","BR":"Brindisi","CA":"Cagliari","CL":"Caltanissetta","CB":"Campobasso","CI":"Carbonia-Iglesias","CE":"Caserta","CT":"Catania","CZ":"Catanzaro","CH":"Chieti","CO":"Como","CS":"Cosenza","CR":"Cremona","KR":"Crotone","CN":"Cuneo","EN":"Enna","FM":"Fermo","FE":"Ferrara","FI":"Firenze","FG":"Foggia","FC":"Forl\u00ec-Cesena","FR":"Frosinone","GE":"Genova","GO":"Gorizia","GR":"Grosseto","IM":"Imperia","IS":"Isernia","SP":"La Spezia","AQ":"L&apos;Aquila","LT":"Latina","LE":"Lecce","LC":"Lecco","LI":"Livorno","LO":"Lodi","LU":"Lucca","MC":"Macerata","MN":"Mantova","MS":"Massa-Carrara","MT":"Matera","ME":"Messina","MI":"Milano","MO":"Modena","MB":"Monza e della Brianza","NA":"Napoli","NO":"Novara","NU":"Nuoro","OT":"Olbia-Tempio","OR":"Oristano","PD":"Padova","PA":"Palermo","PR":"Parma","PV":"Pavia","PG":"Perugia","PU":"Pesaro e Urbino","PE":"Pescara","PC":"Piacenza","PI":"Pisa","PT":"Pistoia","PN":"Pordenone","PZ":"Potenza","PO":"Prato","RG":"Ragusa","RA":"Ravenna","RC":"Reggio Calabria","RE":"Reggio Emilia","RI":"Rieti","RN":"Rimini","RM":"Roma","RO":"Rovigo","SA":"Salerno","VS":"Medio Campidano","SS":"Sassari","SV":"Savona","SI":"Siena","SR":"Siracusa","SO":"Sondrio","TA":"Taranto","TE":"Teramo","TR":"Terni","TO":"Torino","OG":"Ogliastra","TP":"Trapani","TN":"Trento","TV":"Treviso","TS":"Trieste","UD":"Udine","VA":"Varese","VE":"Venezia","VB":"Verbano-Cusio-Ossola","VC":"Vercelli","VR":"Verona","VV":"Vibo Valentia","VI":"Vicenza","VT":"Viterbo"},"ID":{"AC":"Daerah Istimewa Aceh","SU":"Sumatera Utara","SB":"Sumatera Barat","RI":"Riau","KR":"Kepulauan Riau","JA":"Jambi","SS":"Sumatera Selatan","BB":"Bangka Belitung","BE":"Bengkulu","LA":"Lampung","JK":"DKI Jakarta","JB":"Jawa Barat","BT":"Banten","JT":"Jawa Tengah","JI":"Jawa Timur","YO":"Daerah Istimewa Yogyakarta","BA":"Bali","NB":"Nusa Tenggara Barat","NT":"Nusa Tenggara Timur","KB":"Kalimantan Barat","KT":"Kalimantan Tengah","KI":"Kalimantan Timur","KS":"Kalimantan Selatan","KU":"Kalimantan Utara","SA":"Sulawesi Utara","ST":"Sulawesi Tengah","SG":"Sulawesi Tenggara","SR":"Sulawesi Barat","SN":"Sulawesi Selatan","GO":"Gorontalo","MA":"Maluku","MU":"Maluku Utara","PA":"Papua","PB":"Papua Barat"},"HU":{"BK":"B\u00e1cs-Kiskun","BE":"B\u00e9k\u00e9s","BA":"Baranya","BZ":"Borsod-Aba\u00faj-Zempl\u00e9n","BU":"Budapest","CS":"Csongr\u00e1d","FE":"Fej\u00e9r","GS":"Gy\u0151r-Moson-Sopron","HB":"Hajd\u00fa-Bihar","HE":"Heves","JN":"J\u00e1sz-Nagykun-Szolnok","KE":"Kom\u00e1rom-Esztergom","NO":"N\u00f3gr\u00e1d","PE":"Pest","SO":"Somogy","SZ":"Szabolcs-Szatm\u00e1r-Bereg","TO":"Tolna","VA":"Vas","VE":"Veszpr\u00e9m","ZA":"Zala"},"BD":{"BAG":"Bagerhat","BAN":"Bandarban","BAR":"Barguna","BARI":"Barisal","BHO":"Bhola","BOG":"Bogra","BRA":"Brahmanbaria","CHA":"Chandpur","CHI":"Chittagong","CHU":"Chuadanga","COM":"Comilla","COX":"Cox's Bazar","DHA":"Dhaka","DIN":"Dinajpur","FAR":"Faridpur ","FEN":"Feni","GAI":"Gaibandha","GAZI":"Gazipur","GOP":"Gopalganj","HAB":"Habiganj","JAM":"Jamalpur","JES":"Jessore","JHA":"Jhalokati","JHE":"Jhenaidah","JOY":"Joypurhat","KHA":"Khagrachhari","KHU":"Khulna","KIS":"Kishoreganj","KUR":"Kurigram","KUS":"Kushtia","LAK":"Lakshmipur","LAL":"Lalmonirhat","MAD":"Madaripur","MAG":"Magura","MAN":"Manikganj ","MEH":"Meherpur","MOU":"Moulvibazar","MUN":"Munshiganj","MYM":"Mymensingh","NAO":"Naogaon","NAR":"Narail","NARG":"Narayanganj","NARD":"Narsingdi","NAT":"Natore","NAW":"Nawabganj","NET":"Netrakona","NIL":"Nilphamari","NOA":"Noakhali","PAB":"Pabna","PAN":"Panchagarh","PAT":"Patuakhali","PIR":"Pirojpur","RAJB":"Rajbari","RAJ":"Rajshahi","RAN":"Rangamati","RANP":"Rangpur","SAT":"Satkhira","SHA":"Shariatpur","SHE":"Sherpur","SIR":"Sirajganj","SUN":"Sunamganj","SYL":"Sylhet","TAN":"Tangail","THA":"Thakurgaon"},"MX":{"Distrito Federal":"Distrito Federal","Jalisco":"Jalisco","Nuevo Leon":"Nuevo Le\u00f3n","Aguascalientes":"Aguascalientes","Baja California":"Baja California","Baja California Sur":"Baja California Sur","Campeche":"Campeche","Chiapas":"Chiapas","Chihuahua":"Chihuahua","Coahuila":"Coahuila","Colima":"Colima","Durango":"Durango","Guanajuato":"Guanajuato","Guerrero":"Guerrero","Hidalgo":"Hidalgo","Estado de Mexico":"Edo. de M\u00e9xico","Michoacan":"Michoac\u00e1n","Morelos":"Morelos","Nayarit":"Nayarit","Oaxaca":"Oaxaca","Puebla":"Puebla","Queretaro":"Quer\u00e9taro","Quintana Roo":"Quintana Roo","San Luis Potosi":"San Luis Potos\u00ed","Sinaloa":"Sinaloa","Sonora":"Sonora","Tabasco":"Tabasco","Tamaulipas":"Tamaulipas","Tlaxcala":"Tlaxcala","Veracruz":"Veracruz","Yucatan":"Yucat\u00e1n","Zacatecas":"Zacatecas"},"IN":{"AP":"Andra Pradesh","AR":"Arunachal Pradesh","AS":"Assam","BR":"Bihar","CT":"Chhattisgarh","GA":"Goa","GJ":"Gujarat","HR":"Haryana","HP":"Himachal Pradesh","JK":"Jammu and Kashmir","JH":"Jharkhand","KA":"Karnataka","KL":"Kerala","MP":"Madhya Pradesh","MH":"Maharashtra","MN":"Manipur","ML":"Meghalaya","MZ":"Mizoram","NL":"Nagaland","OR":"Orissa","PB":"Punjab","RJ":"Rajasthan","SK":"Sikkim","TN":"Tamil Nadu","TS":"Telangana","TR":"Tripura","UK":"Uttarakhand","UP":"Uttar Pradesh","WB":"West Bengal","AN":"Andaman and Nicobar Islands","CH":"Chandigarh","DN":"Dadar and Nagar Haveli","DD":"Daman and Diu","DL":"Delhi","LD":"Lakshadeep","PY":"Pondicherry (Puducherry)"},"PT":{"NO":"Norte","CE":"Centro","LT":"Lisboa e Vale do Tejo","AG":"Algarve","AT":"Alentejo","MD":"Madeira","AC":"A\u00e7ores"},"ES":{"C":"A Coru&ntilde;a","VI":"Araba\/&Aacute;lava","AB":"Albacete","A":"Alicante","AL":"Almer&iacute;a","O":"Asturias","AV":"&Aacute;vila","BA":"Badajoz","PM":"Baleares","B":"Barcelona","BU":"Burgos","CC":"C&aacute;ceres","CA":"C&aacute;diz","S":"Cantabria","CS":"Castell&oacute;n","CE":"Ceuta","CR":"Ciudad Real","CO":"C&oacute;rdoba","CU":"Cuenca","GI":"Girona","GR":"Granada","GU":"Guadalajara","SS":"Gipuzkoa","H":"Huelva","HU":"Huesca","J":"Ja&eacute;n","LO":"La Rioja","GC":"Las Palmas","LE":"Le&oacute;n","L":"Lleida","LU":"Lugo","M":"Madrid","MA":"M&aacute;laga","ML":"Melilla","MU":"Murcia","NA":"Navarra","OR":"Ourense","P":"Palencia","PO":"Pontevedra","SA":"Salamanca","TF":"Santa Cruz de Tenerife","SG":"Segovia","SE":"Sevilla","SO":"Soria","T":"Tarragona","TE":"Teruel","TO":"Toledo","V":"Valencia","VA":"Valladolid","BI":"Bizkaia","ZA":"Zamora","Z":"Zaragoza"},"ZA":{"EC":"Eastern Cape","FS":"Free State","GP":"Gauteng","KZN":"KwaZulu-Natal","LP":"Limpopo","MP":"Mpumalanga","NC":"Northern Cape","NW":"North West","WC":"Western Cape"},"TH":{"TH-37":"Amnat Charoen (&#3629;&#3635;&#3609;&#3634;&#3592;&#3648;&#3592;&#3619;&#3636;&#3597;)","TH-15":"Ang Thong (&#3629;&#3656;&#3634;&#3591;&#3607;&#3629;&#3591;)","TH-14":"Ayutthaya (&#3614;&#3619;&#3632;&#3609;&#3588;&#3619;&#3624;&#3619;&#3637;&#3629;&#3618;&#3640;&#3608;&#3618;&#3634;)","TH-10":"Bangkok (&#3585;&#3619;&#3640;&#3591;&#3648;&#3607;&#3614;&#3617;&#3627;&#3634;&#3609;&#3588;&#3619;)","TH-38":"Bueng Kan (&#3610;&#3638;&#3591;&#3585;&#3634;&#3628;)","TH-31":"Buri Ram (&#3610;&#3640;&#3619;&#3637;&#3619;&#3633;&#3617;&#3618;&#3660;)","TH-24":"Chachoengsao (&#3593;&#3632;&#3648;&#3594;&#3636;&#3591;&#3648;&#3607;&#3619;&#3634;)","TH-18":"Chai Nat (&#3594;&#3633;&#3618;&#3609;&#3634;&#3607;)","TH-36":"Chaiyaphum (&#3594;&#3633;&#3618;&#3616;&#3641;&#3617;&#3636;)","TH-22":"Chanthaburi (&#3592;&#3633;&#3609;&#3607;&#3610;&#3640;&#3619;&#3637;)","TH-50":"Chiang Mai (&#3648;&#3594;&#3637;&#3618;&#3591;&#3651;&#3627;&#3617;&#3656;)","TH-57":"Chiang Rai (&#3648;&#3594;&#3637;&#3618;&#3591;&#3619;&#3634;&#3618;)","TH-20":"Chonburi (&#3594;&#3621;&#3610;&#3640;&#3619;&#3637;)","TH-86":"Chumphon (&#3594;&#3640;&#3617;&#3614;&#3619;)","TH-46":"Kalasin (&#3585;&#3634;&#3628;&#3626;&#3636;&#3609;&#3608;&#3640;&#3660;)","TH-62":"Kamphaeng Phet (&#3585;&#3635;&#3649;&#3614;&#3591;&#3648;&#3614;&#3594;&#3619;)","TH-71":"Kanchanaburi (&#3585;&#3634;&#3597;&#3592;&#3609;&#3610;&#3640;&#3619;&#3637;)","TH-40":"Khon Kaen (&#3586;&#3629;&#3609;&#3649;&#3585;&#3656;&#3609;)","TH-81":"Krabi (&#3585;&#3619;&#3632;&#3610;&#3637;&#3656;)","TH-52":"Lampang (&#3621;&#3635;&#3611;&#3634;&#3591;)","TH-51":"Lamphun (&#3621;&#3635;&#3614;&#3641;&#3609;)","TH-42":"Loei (&#3648;&#3621;&#3618;)","TH-16":"Lopburi (&#3621;&#3614;&#3610;&#3640;&#3619;&#3637;)","TH-58":"Mae Hong Son (&#3649;&#3617;&#3656;&#3630;&#3656;&#3629;&#3591;&#3626;&#3629;&#3609;)","TH-44":"Maha Sarakham (&#3617;&#3627;&#3634;&#3626;&#3634;&#3619;&#3588;&#3634;&#3617;)","TH-49":"Mukdahan (&#3617;&#3640;&#3585;&#3604;&#3634;&#3627;&#3634;&#3619;)","TH-26":"Nakhon Nayok (&#3609;&#3588;&#3619;&#3609;&#3634;&#3618;&#3585;)","TH-73":"Nakhon Pathom (&#3609;&#3588;&#3619;&#3611;&#3600;&#3617;)","TH-48":"Nakhon Phanom (&#3609;&#3588;&#3619;&#3614;&#3609;&#3617;)","TH-30":"Nakhon Ratchasima (&#3609;&#3588;&#3619;&#3619;&#3634;&#3594;&#3626;&#3637;&#3617;&#3634;)","TH-60":"Nakhon Sawan (&#3609;&#3588;&#3619;&#3626;&#3623;&#3619;&#3619;&#3588;&#3660;)","TH-80":"Nakhon Si Thammarat (&#3609;&#3588;&#3619;&#3624;&#3619;&#3637;&#3608;&#3619;&#3619;&#3617;&#3619;&#3634;&#3594;)","TH-55":"Nan (&#3609;&#3656;&#3634;&#3609;)","TH-96":"Narathiwat (&#3609;&#3619;&#3634;&#3608;&#3636;&#3623;&#3634;&#3626;)","TH-39":"Nong Bua Lam Phu (&#3627;&#3609;&#3629;&#3591;&#3610;&#3633;&#3623;&#3621;&#3635;&#3616;&#3641;)","TH-43":"Nong Khai (&#3627;&#3609;&#3629;&#3591;&#3588;&#3634;&#3618;)","TH-12":"Nonthaburi (&#3609;&#3609;&#3607;&#3610;&#3640;&#3619;&#3637;)","TH-13":"Pathum Thani (&#3611;&#3607;&#3640;&#3617;&#3608;&#3634;&#3609;&#3637;)","TH-94":"Pattani (&#3611;&#3633;&#3605;&#3605;&#3634;&#3609;&#3637;)","TH-82":"Phang Nga (&#3614;&#3633;&#3591;&#3591;&#3634;)","TH-93":"Phatthalung (&#3614;&#3633;&#3607;&#3621;&#3640;&#3591;)","TH-56":"Phayao (&#3614;&#3632;&#3648;&#3618;&#3634;)","TH-67":"Phetchabun (&#3648;&#3614;&#3594;&#3619;&#3610;&#3641;&#3619;&#3603;&#3660;)","TH-76":"Phetchaburi (&#3648;&#3614;&#3594;&#3619;&#3610;&#3640;&#3619;&#3637;)","TH-66":"Phichit (&#3614;&#3636;&#3592;&#3636;&#3605;&#3619;)","TH-65":"Phitsanulok (&#3614;&#3636;&#3625;&#3603;&#3640;&#3650;&#3621;&#3585;)","TH-54":"Phrae (&#3649;&#3614;&#3619;&#3656;)","TH-83":"Phuket (&#3616;&#3641;&#3648;&#3585;&#3655;&#3605;)","TH-25":"Prachin Buri (&#3611;&#3619;&#3634;&#3592;&#3637;&#3609;&#3610;&#3640;&#3619;&#3637;)","TH-77":"Prachuap Khiri Khan (&#3611;&#3619;&#3632;&#3592;&#3623;&#3610;&#3588;&#3637;&#3619;&#3637;&#3586;&#3633;&#3609;&#3608;&#3660;)","TH-85":"Ranong (&#3619;&#3632;&#3609;&#3629;&#3591;)","TH-70":"Ratchaburi (&#3619;&#3634;&#3594;&#3610;&#3640;&#3619;&#3637;)","TH-21":"Rayong (&#3619;&#3632;&#3618;&#3629;&#3591;)","TH-45":"Roi Et (&#3619;&#3657;&#3629;&#3618;&#3648;&#3629;&#3655;&#3604;)","TH-27":"Sa Kaeo (&#3626;&#3619;&#3632;&#3649;&#3585;&#3657;&#3623;)","TH-47":"Sakon Nakhon (&#3626;&#3585;&#3621;&#3609;&#3588;&#3619;)","TH-11":"Samut Prakan (&#3626;&#3617;&#3640;&#3607;&#3619;&#3611;&#3619;&#3634;&#3585;&#3634;&#3619;)","TH-74":"Samut Sakhon (&#3626;&#3617;&#3640;&#3607;&#3619;&#3626;&#3634;&#3588;&#3619;)","TH-75":"Samut Songkhram (&#3626;&#3617;&#3640;&#3607;&#3619;&#3626;&#3591;&#3588;&#3619;&#3634;&#3617;)","TH-19":"Saraburi (&#3626;&#3619;&#3632;&#3610;&#3640;&#3619;&#3637;)","TH-91":"Satun (&#3626;&#3605;&#3641;&#3621;)","TH-17":"Sing Buri (&#3626;&#3636;&#3591;&#3627;&#3660;&#3610;&#3640;&#3619;&#3637;)","TH-33":"Sisaket (&#3624;&#3619;&#3637;&#3626;&#3632;&#3648;&#3585;&#3625;)","TH-90":"Songkhla (&#3626;&#3591;&#3586;&#3621;&#3634;)","TH-64":"Sukhothai (&#3626;&#3640;&#3650;&#3586;&#3607;&#3633;&#3618;)","TH-72":"Suphan Buri (&#3626;&#3640;&#3614;&#3619;&#3619;&#3603;&#3610;&#3640;&#3619;&#3637;)","TH-84":"Surat Thani (&#3626;&#3640;&#3619;&#3634;&#3625;&#3598;&#3619;&#3660;&#3608;&#3634;&#3609;&#3637;)","TH-32":"Surin (&#3626;&#3640;&#3619;&#3636;&#3609;&#3607;&#3619;&#3660;)","TH-63":"Tak (&#3605;&#3634;&#3585;)","TH-92":"Trang (&#3605;&#3619;&#3633;&#3591;)","TH-23":"Trat (&#3605;&#3619;&#3634;&#3604;)","TH-34":"Ubon Ratchathani (&#3629;&#3640;&#3610;&#3621;&#3619;&#3634;&#3594;&#3608;&#3634;&#3609;&#3637;)","TH-41":"Udon Thani (&#3629;&#3640;&#3604;&#3619;&#3608;&#3634;&#3609;&#3637;)","TH-61":"Uthai Thani (&#3629;&#3640;&#3607;&#3633;&#3618;&#3608;&#3634;&#3609;&#3637;)","TH-53":"Uttaradit (&#3629;&#3640;&#3605;&#3619;&#3604;&#3636;&#3605;&#3606;&#3660;)","TH-95":"Yala (&#3618;&#3632;&#3621;&#3634;)","TH-35":"Yasothon (&#3618;&#3650;&#3626;&#3608;&#3619;)"},"TR":{"TR01":"Adana","TR02":"Ad&#305;yaman","TR03":"Afyon","TR04":"A&#287;r&#305;","TR05":"Amasya","TR06":"Ankara","TR07":"Antalya","TR08":"Artvin","TR09":"Ayd&#305;n","TR10":"Bal&#305;kesir","TR11":"Bilecik","TR12":"Bing&#246;l","TR13":"Bitlis","TR14":"Bolu","TR15":"Burdur","TR16":"Bursa","TR17":"&#199;anakkale","TR18":"&#199;ank&#305;r&#305;","TR19":"&#199;orum","TR20":"Denizli","TR21":"Diyarbak&#305;r","TR22":"Edirne","TR23":"Elaz&#305;&#287;","TR24":"Erzincan","TR25":"Erzurum","TR26":"Eski&#351;ehir","TR27":"Gaziantep","TR28":"Giresun","TR29":"G&#252;m&#252;&#351;hane","TR30":"Hakkari","TR31":"Hatay","TR32":"Isparta","TR33":"&#304;&#231;el","TR34":"&#304;stanbul","TR35":"&#304;zmir","TR36":"Kars","TR37":"Kastamonu","TR38":"Kayseri","TR39":"K&#305;rklareli","TR40":"K&#305;r&#351;ehir","TR41":"Kocaeli","TR42":"Konya","TR43":"K&#252;tahya","TR44":"Malatya","TR45":"Manisa","TR46":"Kahramanmara&#351;","TR47":"Mardin","TR48":"Mu&#287;la","TR49":"Mu&#351;","TR50":"Nev&#351;ehir","TR51":"Ni&#287;de","TR52":"Ordu","TR53":"Rize","TR54":"Sakarya","TR55":"Samsun","TR56":"Siirt","TR57":"Sinop","TR58":"Sivas","TR59":"Tekirda&#287;","TR60":"Tokat","TR61":"Trabzon","TR62":"Tunceli","TR63":"&#350;anl&#305;urfa","TR64":"U&#351;ak","TR65":"Van","TR66":"Yozgat","TR67":"Zonguldak","TR68":"Aksaray","TR69":"Bayburt","TR70":"Karaman","TR71":"K&#305;r&#305;kkale","TR72":"Batman","TR73":"&#350;&#305;rnak","TR74":"Bart&#305;n","TR75":"Ardahan","TR76":"I&#287;d&#305;r","TR77":"Yalova","TR78":"Karab&#252;k","TR79":"Kilis","TR80":"Osmaniye","TR81":"D&#252;zce"},"NZ":{"NL":"Northland","AK":"Auckland","WA":"Waikato","BP":"Bay of Plenty","TK":"Taranaki","HB":"Hawke&rsquo;s Bay","MW":"Manawatu-Wanganui","WE":"Wellington","NS":"Nelson","MB":"Marlborough","TM":"Tasman","WC":"West Coast","CT":"Canterbury","OT":"Otago","SL":"Southland"}},"ajaxurl":"https:\/\/thecampaignmaker.com\/devganesh\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script src="<?php echo home_url(); ?>/wp-content/plugins/memberpress/js/i18n.js?ver=4.9.6"></script>
<script src="<?php echo home_url(); ?>/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4"></script>
<script src="<?php echo home_url(); ?>/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4"></script>
<script type="text/javascript">
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script src="<?php echo home_url(); ?>/wp-content/plugins/memberpress/js/jquery-ui-timepicker-addon.js?ver=4.9.6"></script>
<script type='text/javascript'>
/* <![CDATA[ */
var MeprDatePicker = {"timeFormat":"","showTime":""};
/* ]]> */
</script>
<script src="<?php echo home_url(); ?>/wp-content/plugins/memberpress/js/date_picker.js?ver=1.3.34"></script>
<script type='text/javascript'>
/* <![CDATA[ */
var MeprSignup = {"coupon_nonce":"d8d3408c39"};
/* ]]> */
</script>
<script src="<?php echo home_url(); ?>/wp-content/plugins/memberpress/js/signup.js?ver=4.9.6"></script>
<script src="https://js.stripe.com/v3/?ver=1.3.34"></script>
<script src="<?php echo home_url(); ?>/wp-content/plugins/memberpress/js/checkout.js?ver=1.3.34"></script>
<script type='text/javascript'>
/* <![CDATA[ */
var MeprStripeGateway = {"public_key":"pk_test_t6ge0P1VpiuCqUVoxm0CajPp"};
/* ]]> */
</script>

 <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js?ver=4.9.6"></script>
<script src="<?php echo home_url(); ?>/wp-content/themes/campaign-maker/assets/js/bootstrap.min.js?ver=4.9.6"></script>
<script src="<?php echo home_url(); ?>/wp-content/themes/campaign-maker/assets/js/jquery.main.js?ver=4.9.6"></script>
<script>
var mm,yy="";
  jQuery(document).ready(function() {
       jQuery('.panel').css('display','none');
       jQuery('.form-title').html(''); 
       jQuery('.mepr-submit').val('Sign Up');
       jQuery('.mp_wrapper form').css('text-align','center');
       jQuery('.mepr-form-has-errors').html('');
        jQuery('#mepr_payment_method-pco5s9-1kt .mepr-payment-method-desc-text').html('Sign up with credit card via stripe');
       jQuery('#mepr_payment_method-pcskws-76e .mepr-payment-method-desc-text').html('Sign up via your paypal account');
        jQuery('.mp_wrapper form').append('<div class="mp-form-row mepr_tos"> <label for="year_mepr_agree_to_tos" class="mepr-checkbox-field mepr-form-input">By clicking the button above you agree to our <a href="https://thecampaignmaker.com/blog/terms-of-service/" target="_blank" rel="noopener noreferrer">Terms of Service</a> </label> <span id="year-errors" role="alert" class="stripe_element_error"></span></div>');
			var aa="mepr_payment_method-"+jQuery("#yearlypay input[name=mepr_payment_method]:checked").val();
		jQuery('#yearlypay #'+aa).css( "box-shadow", "0 0 30px #0073fa" );
		var aa="mepr_payment_method-"+jQuery("#monthlypay input[name=mepr_payment_method]:checked").val();
		jQuery('#monthlypay #'+aa).css( "box-shadow", "0 0 30px #0073fa" );
		
     jQuery('#monthlypay .mepr-signup-form').on('submit', function (e) {
          e.preventDefault();
          jQuery.ajax({
            type: 'post',
            url: '/kale/make-payment/#mepr_jump',
            data: jQuery('#monthlypay .mepr-signup-form').serialize(),
            success: function (data) {
               	if(data.lastIndexOf('Invalid Payment Method')>-1){
					   jQuery('#monthlypay').html('<div class="mepr_error" id="mepr_jump">   <ul><li><strong>ERROR</strong>: Invalid Payment Method</li></ul></div>');
				}else{
				  var mySubString = data.substring((data.lastIndexOf('<main id="main">') + 16 ), data.lastIndexOf('</main>'));
				  
                 var s = document.createElement("script");
                 s.type = "text/javascript";
                 s.src = "<?php echo home_url(); ?>/wp-content/plugins/memberpress/app/gateways/stripe/create_token3.js?ver=1.3.34";
                  if(yy!="" && yy!==undefined){
                      //jQuery('#yearlypay').html(yy);
                      yy="";
                  }
                  mm=jQuery('#monthlypay').html();
                  jQuery('#monthlypay').html(mySubString);
                 // jQuery('#monthlypay').append(s); 
                  setTimeout(function(){
						jQuery('#monthlypay .stripe-button-el span').html('Sign Up');
						},300);
				}
                  }
          });
        });
        jQuery('#yearlypay .mepr-signup-form').on('submit', function (e) {
          e.preventDefault();
          jQuery.ajax({
            type: 'post',
            url: '/kale/make-payment/#mepr_jump',
            data: jQuery('#yearlypay .mepr-signup-form').serialize(),
            success: function (data) {
		
               	if(data.lastIndexOf('Invalid Payment Method')>-1){
					   jQuery('#yearlypay').html('<div class="mepr_error" id="mepr_jump">   <ul><li><strong>ERROR</strong>: Invalid Payment Method</li></ul></div>');
				}else{
                var mySubString = data.substring(data.lastIndexOf('<main id="main">') + 16 , data.lastIndexOf('</main>'));
			
                 var s = document.createElement("script");
                 s.type = "text/javascript";
                 s.src = "<?php echo home_url(); ?>/wp-content/plugins/memberpress/app/gateways/stripe/create_token3.js?ver=1.3.34";
                  if(mm!="" && mm!==undefined){
                      //jQuery('#monthlypay').html(mm);
                      mm="";
                  }
					yy=jQuery('#yearlypay').html();
                     jQuery('#yearlypay').html(mySubString);
                    // jQuery('#yearlypay').append(s);
                   setTimeout(function(){
						jQuery('#yearlypay .stripe-button-el span').html('Sign Up');
						},300);
                  }
			}
          });
        });
		
		jQuery("#yearlypay input[name=mepr_payment_method]").on( "click", function(event) {
			jQuery('#yearlypay #mepr_payment_method .mp-form-row .mepr-payment-method').css( "box-shadow", "none" );
			var aa="mepr_payment_method-"+jQuery(event.target).val();
			jQuery('#yearlypay #'+aa).css( "box-shadow", "0 0 30px #0073fa" );
		});
		jQuery("#monthlypay input[name=mepr_payment_method]").on( "click", function(event) {
			jQuery('#monthlypay #mepr_payment_method .mp-form-row .mepr-payment-method').css( "box-shadow", "none" );
			var aa="mepr_payment_method-"+jQuery(event.target).val();
			jQuery('#monthlypay #'+aa).css( "box-shadow", "0 0 30px #0073fa" );
		});
});
function checkmonth()
{
    if(!jQuery('#month_mepr_agree_to_tos').is(':checked')){
     jQuery('#month-errors').html('You must agree to the Terms of Service');
     return false;
    }else{
         jQuery('#month-errors').html('');
     return true;
    }
}
function checkyear()
{
    if(!jQuery('#year_mepr_agree_to_tos').is(':checked')){
         jQuery('#year-errors').html('You must agree to the Terms of Service');
         return false;
    }else{
         jQuery('#year-errors').html('');
         return true;
    }
}
</script>
<script src="//ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js?ver=4.9.6"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Montserrat:400,500,600']
			}
		});
	</script>
</body>
</html>

  <?php
} else { header('Location: '.home_url().'/login');  } ?>