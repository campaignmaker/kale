<?php
/**
 * Template Name: Register
 */
?>
<?php get_header('basic'); ?>

<div class="form-wrap">
  <div class="holder s1">
    <form action="/connect-to-facebook/" class="form s1">
      <h2 class="form-title">REGISTER FOR YOUR FREE TRIAL</h2>
      <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" id="name" type="text" required>
      </div>
      <div class="form-group">
        <label for="email-address">Email Address</label>
        <input class="form-control" id="email-address" type="email" required>
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input class="form-control" id="password" type="password" required>
      </div>
      <div class="btn-row">
        <div class="d-sm-flex align-items-center">
          <div class="col">
            <div class="btn-holder text-center text-sm-left">
              <button type="submit" class="btn btn-primary">REGISTER</button>
            </div>
          </div>
          <!-- <div class="option text-center">
           <span>OR</span>
          </div>
          <div class="col">
            <div class="btn-holder text-center text-sm-right">
              <a href="#" class="btn btn-fb"><span class="icon-facebook"></span>REGISTER WITH FB</a>
            </div>
          </div> -->
        </div>
      </div>
    </form>
  </div>
  <div class="panel">
    <div class="panel-holder d-sm-flex justify-content-between align-items-center">
      <div class="link-holder text-center text-sm-left">
        <a href="<?php the_field('login_page_link', 'option'); ?>" class="link-s2">Already have an account?</a>
      </div>
      <div class="btn-holder text-center text-sm-right">
        <a href="<?php the_field('login_page_link', 'option'); ?>" class="btn btn-secondary">LOGIN NOW</a>
      </div>
    </div>
  </div>
</div>

<?php get_footer('basic'); ?>