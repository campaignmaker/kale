<?php
/**
 * Template Name: Step 7 : Final
 */
?>
<?php if ( is_user_logged_in() ) { get_header('steps');
$usr = new MeprUser($_SESSION['user']->ID);
 ?>

<div class="step-bar">
  <div class="holder d-flex flex-wrap">
    <div class="step done">
      <strong class="step-name">Welcome</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Ebook</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Analyze</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Create</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Optimize</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Report</strong>
    </div>
  </div>
</div>
<div class="main-content s1">
  <div class="container">
    <div class="center-box s1">
      <div class="holder s1">
        <h2 class="screen-heading text-center">One Step Closer</h2>
        <div class="text-holder text-center">
          <p>Creating profitable Facebook ads is a tough task but let me you tell that Facebook ads is one of the very few marketing platforms that can be extremely profitable when done correctly.</p>
          <p>If you use each of our tools correctly you will find drastically better results within a few days/weeks with Facebook ads.</p>
          <p>We built each of our tools with a goal in mind that relieves much of the stress and hard work using automation and pre-set rules that most successful Facebook marketers use.</p>
          <p>In addition to that we offer expert done for you help to limit your wasted spend even quicker.</p>
        </div>
        <div class="btn-holder text-center">
		<?php 
		if((isset($_SESSION['where_pay']) && $_SESSION['where_pay']=="ltd") || ($usr->active_txn_count > 0 || $usr->trial_txn_count > 0)){ 
					$_SESSION['where_pay']="";
					unset($_SESSION['where_pay']);
		?>
			<a href="<?php echo get_site_url(); ?>/app/hello" class="btn btn-primary">GET STARTED</a>
		<?php }else if($usr->active_txn_count <= 0 && $usr->expired_txn_count > 0 && $usr->trial_txn_count <= 0){
			?>
			<a href="<?php echo get_site_url(); ?>/final-payment" class="btn btn-primary">GET STARTED</a>
			<?php }else{ ?>
			<a href="<?php echo get_site_url(); ?>/make-payment" class="btn btn-primary">GET STARTED</a>
		<?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer('steps'); ?>
  <?php
} else { header('Location: '.home_url().'/login');  } ?>