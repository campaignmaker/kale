<?php
/**
 * Template Name: Connect to Facebook
 */
?>
<?php if ( is_user_logged_in() ) { get_header('basic'); ?>


<div class="center-box s3">
  <div class="holder s1">
    <div class="img-holder text-center">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/artwork.svg" alt="artwork">
    </div>
    <h2 class="screen-heading text-center"><?php the_title(); ?></h2>
    <div class="text-holder text-center">
      <p>You will need to connect The Campaign Maker to your Facebook account. This allows TCM to create campaigns on your behalf.</p>
      <div class="highlight-text">
        <p>The Campaign Maker will not create campaigns or make any changes to your account without your explicit knowledge.</p>
      </div>
     
      <p>Just click the button below and follow the on-screen prompts.</p>
    </div>
    <div class="btn-holder text-center">
      <!--a href="<?php // the_field('step_1_link', 'option'); ?>" class="btn btn-fb"><span class="icon-facebook"></span>CLICK TO CONNECT</a-->
      <a href="<?php echo get_site_url(); ?>/app/dashboard" class="btn btn-fb"><span class="icon-facebook"></span>CLICK TO CONNECT</a>
    </div>
  </div>
</div>

<?php get_footer('basic'); ?>  <?php
} else { header('Location: '.home_url().'/login');  } ?>