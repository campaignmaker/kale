<?php
/**
 * Template Name: Login
 */
?>
<?php get_header('basic'); ?>

<div class="form-wrap">
  <div class="holder">
    <form action="#" class="form s1">
      <h2 class="form-title">Login</h2>
      <div class="form-group">
        <label for="email-address">Email Address</label>
        <input class="form-control" id="email-address" type="email">
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input class="form-control" id="password" type="password">
      </div>
      <div class="btn-row">
        <div class="d-sm-flex">
          <div class="col">
            <div class="btn-holder text-center text-sm-left">
              <a href="#" class="btn btn-primary">Login</a>
            </div>
          </div>
          <div class="col">
            <div class="btn-holder text-center text-sm-right">
              <a href="#" class="btn btn-fb"><span class="icon-facebook"></span>LOGIN WITH FB</a>
            </div>
          </div>
        </div>
        <div class="link-holder text-center">
          <a class="link-s1" href="#">Forgot Your Password?</a>
        </div>
      </div>
    </form>
  </div>
  <div class="panel">
    <div class="panel-holder d-sm-flex justify-content-between align-items-center">
      <div class="link-holder text-center text-sm-left">
        <a href="<?php the_field('register_page_link', 'option'); ?>" class="link-s2">Don’t have an account?</a>
      </div>
      <div class="btn-holder text-center text-sm-right">
        <a href="<?php the_field('register_page_link', 'option'); ?>" class="btn btn-secondary">REGISTER NOW</a>
      </div>
    </div>
  </div>
</div>

<?php get_footer('basic'); ?>