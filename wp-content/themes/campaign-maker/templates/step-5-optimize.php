<?php
/**
 * Template Name: Step 5 : Optimize
 */
?>
<?php if ( is_user_logged_in() ) { get_header('steps'); ?>

<div class="step-bar">
  <div class="holder d-flex flex-wrap">
    <div class="step done">
      <strong class="step-name">Welcome</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Ebook</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Analyze</strong>
    </div>
    <div class="step done">
      <strong class="step-name">Create</strong>
    </div>
    <div class="step active">
      <strong class="step-name">Optimize</strong>
    </div>
    <div class="step">
      <strong class="step-name">Report</strong>
    </div>
  </div>
</div>
<div class="main-content s1">
  <div class="container">
    <div class="center-box s2">
      <div class="holder">
        <h2 class="screen-heading text-center">Optimzing Your Campaigns Automatically</h2>
        <div class="text-holder text-center">
          <p>Once you have your new split tested campaign created, it would be extremely time consuming and tedious for you to go through each individual ad and look through it for ads that are wasting your money.</p>
          <p>The solution to this is the Optimization tool. Simply setup optimization rules that weed out wasteful ads from your campaign every 30 minutes so you can stop them before they start siphoning the budget from the winning ads.</p>
        </div>
        <!--<div class="video-holder">
          <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/video-player.png" alt="video">
        </div> -->
		<div class="video-holder">
          <img class="img-fluid" src="https://thecampaignmaker.com/wp-content/uploads/2018/05/browser03.png" alt="video">
        </div> 
        <div class="btn-holder s1 text-center d-flex flex-wrap justify-content-center">
          <div class="btn-col">
            <a href="<?php the_field('step_4_link','option'); ?>" class="btn btn-prev">
              <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-prev-pointer.svg" alt="pointer"></span>
              Previous Step
            </a>
          </div>
          <div class="btn-col">
            <a href="<?php the_field('step_6_link','option'); ?>" class="btn btn-next">
              <span class="icon-holder"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-pointer.svg" alt="pointer"></span>
              Next Step
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer('steps'); ?>  <?php
} else { header('Location: '.home_url().'/login');  } ?>