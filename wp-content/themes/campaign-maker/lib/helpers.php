<?php /** Helpers Methods */

 // Encode an email address
 function encode_email($email)
 {
   $output = '';
   for ($i = 0; $i < strlen($email); $i++) {
     $output .= '&#' . ord($email[$i]) . ';';
   }
   return $output;
 }

/**
 * Get image data
 *
 * @param object $image
 * @param string $size
 * @return array
 */
 function get_image_data($image, $size = false)
 {
   $data = [
   'url' => $size ? $image['sizes'][$size] : $image['url'],
   'alt' => $image['alt'],
   'width' => $size ? $image['sizes'][$size . '-width'] : $image['width'],
   'height' => $size ? $image['sizes'][$size . '-height'] : $image['height']
   ];
   return $data;
 }

 /**
  * Get composed <img>
  *
  * @param object $image
  * @param string $size
  * @param string $attr
  * @return mixed
  */
 function get_image($image, $size = false, $attr = false)
 {
   if ($image) {
     $data = get_image_data($image, $size);
     return '<img src="' . $data['url'] . '" alt="' . $data['alt'] . '" width="' . $data['width'] . '" height="' . $data['height'] . '" ' . $attr . '>';
   }
 }

 /**
  * Get image wrapped with <div> and class
  * Default class is 'bg-img'
  *
  * @param object $image
  * @param string $size
  * @param string $class
  * @return mixed
  */
 function get_bg_image($image, $size, $class = 'bg-img')
 {
   if ($image) {
     $image = get_image($image, $size);
     return '<div class="' . $class . '">' . $image . '</div>';
   }
 }

 function a_link($data, $title = null, $attr = false)
 {
   if ($data) {
     $atag = '<a href="' . $data . '" ' . $attr . '>' . $title . '</a>';
     return $atag;
   }
 }
