<?php

namespace Lib\CPT\Cpt;

/**
 * Add CPT
 */
function codex_custom_init()
{
//  $labels = [
//    'name' => _x('Reviews', 'post type general name'),
//    'singular_name' => _x('Reviews', 'post type singular name'),
//    'add_new' => _x('Add New', 'book'),
//    'add_new_item' => __('Add Reviews'),
//    'edit_item' => __('Edit Reviews'),
//    'new_item' => __('New Reviews'),
//    'all_items' => __('All Reviews'),
//    'view_item' => __('View Reviews'),
//    'search_items' => __('Search Reviews'),
//    'not_found' => __('Reviews not found'),
//    'not_found_in_trash' => __('Reviews not found in trash'),
//    'parent_item_colon' => '',
//    'menu_name' => 'Reviews'
//  ];
//  $args = [
//    'labels' => $labels,
//    'description' => 'Holds our Reviews and Reviews specific data',
//    'public' => true,
//    'supports' => ['title'],
//    'has_archive' => false,
//    'hierarchical' => true,
//    'menu_position' => 2,
//    'menu_icon' => 'dashicons-format-aside',
//    'rewrite' => ['slug' => 'nyheter', 'with_front' => false],
//  ];
//  register_post_type('reviews', $args);
}
add_action('init', __NAMESPACE__ . '\\codex_custom_init');
