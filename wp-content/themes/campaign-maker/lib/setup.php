<?php

namespace Lib\Setup;

/**
 * Theme setup
 */
function setup()
{
  // add title tag support
  add_theme_support( 'title-tag' );
  
  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus(
    [
      'header' => __( 'Header Menu',      'campaign-maker' ),
      'footer' => __( 'Footer Menu Column',      'campaign-maker' ),
    ]
  );

  // Enable post thumbnails
  add_theme_support('post-thumbnails');
  // add_image_size('logo-img', 198, 0, true );

  // Enable post formats
  // add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

// ----------------------------------------

/**
 * Theme assets
 */

// Dequeue wordpress jquery
function move_wordpress_jquery()
{
  wp_dequeue_script('jquery');
  wp_dequeue_script('jquery-core');
  wp_dequeue_script('jquery-migrate');
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\move_wordpress_jquery');

// Enqueue scripts and styles
function assets()
{
  // css
  wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', false, null);
  wp_enqueue_style('main-css', get_template_directory_uri() . '/assets/css/main.css', false, null);
  wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', false, null);

  // jquery
  wp_enqueue_script('jquery', false, [], false, true);
  wp_enqueue_script('jquery-core', false, [], false, true);
  wp_enqueue_script('jquery-migrate', false, [], false, true);

  // custom scripts
  wp_enqueue_script('popper-js', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', '', '', true);
  wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', '', '', true);
  wp_enqueue_script('main-js', get_stylesheet_directory_uri() . '/assets/js/jquery.main.js', '', '', true);
  wp_enqueue_script('webfont-js', '//ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js', '', '', true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

function custom_login_logo()
{
  $logo = null;
  if ($logo_image = get_field('login_page_logo', 'option')) {
    $logo = '<style>';
    $logo .= 'h1 a { background: transparent url(';
    $logo .= $logo_image;
    $logo .= ') no-repeat 50% 50% !important; background-size: 10rem !important; width: 15rem !important; }';
    $logo .= '</style>';
  }

  echo $logo;
}
add_action('login_head', __NAMESPACE__ . '\\custom_login_logo');

// hide admin bar
show_admin_bar(false);
