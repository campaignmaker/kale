<?php
/**
 * Supercharge Block Layout
 */
?>

<div class="supercharge-block">
  <div class="text-holder text-center">
    <?php if($super_block_title = get_sub_field('super_block_title')): ?>
      <h2><?php echo $super_block_title; ?></h2>
    <?php endif; ?>
    <?php the_sub_field('super_block_content'); ?>
    <?php if($super_charge_link = get_sub_field('super_charge_link')): ?>
      <a href="<?php echo $super_charge_link['link']; ?>" class="btn btn-get-started"><?php echo $super_charge_link['title']; ?></a>
    <?php endif; ?>
  </div>
  <?php if($super_block_bg = get_sub_field('super_block_bg')): ?>
    <div class="img-holder text-center">
      <div class="img-wrap">
        <?php if($super_block_bg_text = $super_block_bg['text']): ?>
          <span class="water-mark"><?php echo $super_block_bg_text; ?></span>
        <?php endif; ?>
        <?php if($super_block_bg_image = $super_block_bg['image']): ?>
          <img src="<?php echo $super_block_bg_image['url']; ?>" alt="<?php echo $super_block_bg_image['alt']; ?>">
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>
</div>