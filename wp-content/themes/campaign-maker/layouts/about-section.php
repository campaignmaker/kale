<?php
/**
 * About Section Layout
 */
?>

<div class="about-section d-md-flex align-items-start">
  <div class="text-holder d-flex flex-wrap justify-content-end order-2">
    <div class="text-wrap">
      <?php if($about_section_title = get_sub_field('about_section_title')): ?>
        <h2 class="decorative-heding"><?php echo $about_section_title; ?></h2>
      <?php endif; ?>
      <?php the_sub_field('about_section_content'); ?>
      <?php if($about_section_quote = get_sub_field('about_section_quote')): ?>
        <blockquote class="highlight-quote">
          <q><?php echo $about_section_quote; ?></q>
        </blockquote>
      <?php endif; ?>
    </div>
  </div>

  <?php if($about_section_bg = get_sub_field('about_section_bg')): ?>
    <div class="img-holder text-center order-1">
      <div class="img-wrap">
        <?php if($about_section_bg_text = $about_section_bg['text']): ?>
          <span class="water-mark"><?php echo $about_section_bg_text; ?></span>
        <?php endif; ?>
        <?php if($about_section_bg_image = $about_section_bg['image']): ?>
          <img src="<?php echo $about_section_bg_image['url']; ?>" alt="<?php echo $about_section_bg_image['alt']; ?>">
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>
</div>