<?php
/**
 * Step Block Layout
 */
?>
<a name="features"></a>
<div class="step-detail-section">
  <div class="intro-section text-center">
    <?php if($step_block_title = get_sub_field('step_block_title')): ?>
	  <a name="<?php echo $step_block_title; ?>"></a>
      <h2><?php echo $step_block_title; ?></h2>
    <?php endif; ?>
    <?php the_sub_field('step_block_description'); ?>
  </div>
  <div class="main-detail d-flex flex-wrap">
    <div class="text-holder">
      <div class="text-wrap">
        <?php if($step_content_title = get_sub_field('step_content_title')): ?>
          <h3 class="decorative-heding"><?php echo $step_content_title; ?><?php echo ($step_content_sub_title = get_sub_field('step_content_sub_title')) ? '<br>' . $step_content_sub_title : ''; ?></h3>
        <?php endif; ?>
        <?php the_sub_field('step_content'); ?>
      </div>
      <?php if($step_content_quote = get_sub_field('step_content_quote')): ?>
        <div class="quote-holder">
          <blockquote class="highlight-quote">
            <q><?php echo $step_content_quote; ?></q>
          </blockquote>
        </div>
      <?php endif; ?>
    </div>
    <?php if($step_content_image = get_sub_field('step_content_image')): ?>
      <div class="img-holder">
        <div class="img-wrap">
          <img src="<?php echo $step_content_image['url']; ?>" alt="<?php echo $step_content_image['alt']; ?>">
        </div>
      </div>
    <?php endif; ?>
  </div>
  <?php if(have_rows('step_features')): ?>
  <div class="feature-list-holder d-flex flex-wrap">
    <div class="row">
      <ul class="features-list">
        <?php while(have_rows('step_features')) : the_row(); ?>
        <li class="col-md-6">
          <div class="d-flex flex-wrap">
            <div class="icon-holder">
              <?php if($feature_icon = get_sub_field('icon')): ?>
                <img src="<?php echo $feature_icon['url']; ?>" alt="<?php echo $feature_icon['alt']; ?>">
              <?php endif; ?>
            </div>
            <div class="text-wrap">
              <?php if($feature_title = get_sub_field('title')): ?>
                <h4><?php echo $feature_title; ?></h4>
              <?php endif; ?>
              <?php the_sub_field('details'); ?>
            </div>
          </div>
        </li>
        <?php endwhile; ?>
      </ul>
    </div>
  </div>
  <?php endif; ?>
</div>