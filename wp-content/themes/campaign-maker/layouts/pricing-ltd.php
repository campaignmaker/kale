<?php
/**
 * Pricing Section Layout
 */
?>
<a name="price"></a>
<div class="price-section">
  <header class="header">
    <h2 class="text-center">HOW MUCH WILL ALL OF THIS COST?</h2>
    <p>We decided that our platform should be accessible to people of all budget types. Our pricing is based on how big your team is.</p>
  </header>
  <div class="price-table tab-content">
    <div id="monthly">
      <div class="row justify-content-center">
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card">
            <div class="plan-header">
              <span class="category">SINGLE</span>
              <strong class="price">$69</strong>
              <span class="plan-duration">1 User Account</span>
            </div>
            <div class="features">
              <strong class="title">Unlimited Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> No Sub Account</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">GRAB YOUR LIFETIME ACCOUNT</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card highlight">
            <div class="plan-header">
              <span class="category">DOUBLE</span>
              <strong class="price">$119</strong>
              <span class="plan-duration">2 User Accounts</span>
            </div>
            <div class="features">
              <strong class="title">Unlimited Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 1 Sub Account</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">GRAB YOUR LIFETIME ACCOUNT</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card">
            <div class="plan-header">
              <span class="category">TRIPLE</span>
              <strong class="price">$149</strong>
              <span class="plan-duration">3 User Accounts</span>
            </div>
            <div class="features">
              <strong class="title">Unlimited Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 2 Sub Accounts</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">GRAB YOUR LIFETIME ACCOUNT</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="note">
    <p>This exclusive lifetime offer ends on July 31st 2018.</p>
  </div>
</div>
