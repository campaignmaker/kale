<?php
/**
 * Cost Section Layout
 */
?>

<div class="cost-cut-section d-md-flex flex-wrap align-items-start">
  <div class="text-holder d-flex flex-wrap justify-content-end2">
    <div class="text-wrap">
      <?php if($cost_section_title = get_sub_field('cost_section_title')): ?>
        <h2 class="decorative-heding"><?php echo $cost_section_title; ?></h2>
      <?php endif; ?>

      <?php the_sub_field('cost_section_content'); ?>

      <?php if($cost_section_image = get_sub_field('cost_section_image')): ?>
        <div class="img-holder">
          <img src="<?php echo $cost_section_image['url']; ?>" alt="<?php echo $cost_section_image['alt']; ?>">
        </div>
      <?php endif; ?>
    </div>
  </div>

  <?php if(have_rows('cost_section_features')): ?>
    <div class="step-list d-flex flex-wrap justify-content-end2">
      <?php $i = 1; while(have_rows('cost_section_features')) : the_row(); ?>
        <a href="<?php if($block_link = get_sub_field('block_link')): ?>
              <?php echo $block_link['url']; ?>
            <?php endif; ?>" class="step-item ml-auto d-flex flex-wrap">
          <div class="icon-holder">
            <?php if($icon = get_sub_field('icon')): ?>
              <img class="black-icon" src="<?php echo $icon['url']; ?>" alt="<?php echo $ico['alt']; ?>">
            <?php endif; ?>
            <?php if($hover_icon = get_sub_field('hover_icon')): ?>
              <img class="blue-icon" src="<?php echo $hover_icon['url']; ?>" alt="<?php echo $hover_icon['alt']; ?>">
            <?php endif; ?>
          </div>
          <div class="step-info">
            <?php if($title = get_sub_field('title')): ?>
              <h3><?php echo $title; ?></h3>
            <?php endif; ?>
            <?php the_sub_field('description'); ?>
          </div>
        </a>
      <?php $i++; endwhile; ?>
    </div>
  <?php endif; ?>
</div>