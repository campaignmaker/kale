<?php
/**
 * Features Section Layout
 */
?>

<div class="feature-section">
  <h2 class="text-center">PLUS THESE ESSENTIAL FEATURES</h2>
  <div class="content-holder d-md-flex">
    <div class="text-holder left-text-holder">
      <div class="feature-item">
        <h3>FREE 65pg EBOOK</h3>
        <p>We understand how difficult Facebook ads can be for a brand new advertiser, so we included a comprehensive introductory ebook for beginners when you sign up.</p>
      </div>
      <div class="feature-item">
        <h3>EPIC SUPPORT STAFF</h3>
        <p>Our support staff is available via chat and email. Speed and reliability in our support is what seperates us from the rest.</p>
      </div>
    </div>
    <div class="img-holder text-center d-flex align-items-center">
      <div class="img-wrap">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-trophy.svg" alt="trophy">
      </div>
    </div>
    <div class="text-holder right-text-holder">
      <div class="feature-item">
        <h3>DETAILED GUIDES</h3>
        <p>We offer a detailed help section that includes both detailed videos or articles for each tool and feature we offer, so you can maximize your ads.</p>
      </div>
      <div class="feature-item">
        <h3>OPTIONAL SERVICE</h3>
        <p>In addition to The Campaign Maker, we offer a premium optional done for you service. Let us take care of your campaigns for you.</p>
      </div>
    </div>
  </div>
  <div class="quote-holder">
    <blockquote class="highlight-quote">GO AHEAD AND TRY OUR ACOR METHOD, IF IT DOESNT WORK IN 7 DAYS <br>
      JUST CANCEL YOUR TRIAL…. WHAT DO YOU HAVE TO LOSE?
    </blockquote>
  </div>
</div>