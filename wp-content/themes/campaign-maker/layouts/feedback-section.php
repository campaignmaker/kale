<?php
/**
 * Feedback Section Layout
 */
?>
<a name="reviews"></a>
<div class="feedback-section">
  <header class="text-center">
    <span class="water-mark">Feedback</span>
    <h2>WHAT DO YOU HAVE TO LOSE?</h2>
  </header>
  <div class="testimonials">
    <div class="testimonial-holder d-md-flex flex-wrap">
      <div class="testimonial-item d-flex">
        <blockquote class="d-flex flex-wrap">
          <div class="quote-holder">
            <cite class="d-flex align-items-center">
              <span class="avatar">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-boy-rounded.svg" alt="boy">
              </span>
              <span class="text">
                <span class="name">Alex R.</span>
                <span class="designation">SMBO</span>
              </span>
            </cite>
            <q>Great value for money and has most of the features that the other "bigger" companies offer. I like the clear cut instructions and ease of use. And the development team is passionate! Big plus for customer service.</q>
          </div>
        </blockquote>
      </div>
      <div class="testimonial-item d-flex">
        <blockquote class="d-flex flex-wrap">
          <div class="quote-holder">
            <cite class="d-flex align-items-center">
              <span class="avatar">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-boy-rounded.svg" alt="boy">
              </span>
              <span class="text">
                <span class="name">Tucker J.</span>
                <span class="designation">Ecommerce Biz Owner</span>
              </span>
            </cite>
            <q>I love how something as simple as setting up a rule to stop a high CPC ad can be so effective in lowering the overall cost to a conversion! These preset rules are awesome!</q>
          </div>
        </blockquote>
      </div>
      <div class="testimonial-item d-flex">
        <blockquote class="d-flex flex-wrap">
          <div class="quote-holder">
            <cite class="d-flex align-items-center">
              <span class="avatar">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-boy-rounded.svg" alt="boy">
              </span>
              <span class="text">
                <span class="name">Lane W.</span>
                <span class="designation">Online Aficionado</span>
              </span>
            </cite>
            <q>If you are at all serious about running FB ads, or hope to start doing it right, then get this software. If you know what youre doing, youll thank yourself for being able to save time, and money.</q>
          </div>
        </blockquote>
      </div>
      <div class="testimonial-item d-flex">
        <blockquote class="d-flex flex-wrap">
          <div class="quote-holder">
            <cite class="d-flex align-items-center">
              <span class="avatar">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-boy-rounded.svg" alt="girl">
              </span>
              <span class="text">
                <span class="name">Bruce C.</span>
                <span class="designation">C. Media</span>
              </span>
            </cite>
            <q>Super easy to use for your own campaigns or for managing client's campaigns. The speed that campaign manager creates ads is unbelievable!</q>
          </div>
        </blockquote>
      </div>
    </div>
  </div>
</div>