<?php
/**
 * FAQ Section Layout
 */
?>
<a name="faq"></a>
<div class="faq-section">
  <header class="text-center">
    <span class="water-mark">FAQ</span>
    <h2>FREQUENTLY ASKED QUESTIONS</h2>
  </header>
  <div class="faq-content">
    <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">What is the main difference between TCM and Other Similar Softwares?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>While other softwares and TCM offer very similar features, we differ in pricing. We are priced for maximum value to you the user. Many competitors charge well over $100 per month, while our annual account charges a much lower cost of $8.25 per month with complete and unlimited access. In addition to that we offer unparalleled support.</p>
      </div>
    </div>
    <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">What Makes Your Software Better Than The Ads Manager?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>Using the Facebook ads manager is like using a butter knife to cut your steak. Using the Facebook power editor is like using a steak knife, gets the job done with your effort. Using The Campaign Maker is like having a butler cut your steak with a chef's knife while you watch.
Now imagine you have a massive steak…. that's the difference between them.
At its best it makes a 45 minute task into a 3 minute task. And that is assuming the marketer knows how to split test manually… The Campaign Maker opens the door to beginners to split test while veterans can only split test on power editor.
In terms of reporting, the facebook ads dashboard is insanely complicated for a beginner, especially if they get into the customization dashboard.
With FB you need to dig in, while our reporting tool and analysis tool is seen at a glance.</p>
      </div>
    </div>
    <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">What happens if my adspend goes over the limit??</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>Nothing happens. Your account continues to work normally until your next renewal date. Thats why our annual plans give you the biggest value, because your account is guaranteed for the full year (even if you go over your limit).</p>
      </div>
    </div>
    <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">How can I cancel my trial?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>Cancelling your trial is extremely simple. You can cancel by going to your account settings > subscriptions then click cancel. Or you can contact our support staff. Even if you forget to cancel your trial, we offer you a 30 day money back guarantee.</p>
      </div>
    </div>
    <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">Is there a money back guarantee?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>Yes, and we made it very simple. We will offer you a full refund if for any reason you do not like our software within 30 days of making the payment.</p>
      </div>
    </div>
	  <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">Do I need an ad account to use the software?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>Absolutely! we do not offer or supply ad accounts.</p>
      </div>
    </div>
	  <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">Is the Facebook ad spend included?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>No, the payment made to use is completely seperate from Facebook ads. The Facebook ads cost is payable to Facebook only and is handled purely by them.</p>
      </div>
    </div>
	  <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">How many ad accounts can I use?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>All our accounts allow for up to 500 ad accounts per account. If you have more than 500 ad accounts in your account, contact us and we will extend your account. </p>
      </div>
    </div>
    <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">How many campaigns/adsets/ads can I create?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>All accounts can publish an unlimited amount of campaigns, however a single campaign can contain a maximum of 400 ads.</p>
      </div>
    </div>
    <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">What are sub-users?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>A sub account is basically a secondary account where you can add another Facebook user of your choosing to use The Campaign Maker platform under your subscription. This is ideal for teams who need to collaborate together.</p>
      </div>
    </div>
    <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">Is The Campaign Maker Safe to Use?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>Our platform is authorized and approved by Facebook. We are hoping to become a Facebook marketing partner within the next few months.</p>
      </div>
    </div>
	  <div class="faq-item open-close">
      <a class="question opener" href="#">
        <span class="text">How can I use The Campaign Maker?</span>
        <span class="icon-holder icon-angle-right"></span>
      </a>
      <div class="answer slide">
        <p>We offer a comprehensive help section which includes videos and articles to help you maximize our platform and lowering your Facebook ad costs.</p>
      </div>
    </div>
  </div>
</div>