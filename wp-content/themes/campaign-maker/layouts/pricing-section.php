<?php
/**
 * Pricing Section Layout
 */
?>
<a name="price"></a>
<div class="price-section">
  <header class="header">
    <h2 class="text-center">HOW MUCH WILL ALL OF THIS COST?</h2>
    <p>We decided that our platform should be accessible to people of all budget types. Our pricing is based on how much you spent on Facebook in the past 30 days.</p>
  </header>
  <ul class="tab-list tabset d-flex flex-wrap justify-content-center">
    <li class="active"><a class="tabset-btn btn-monthly" href="#monthly">Monthly</a></li>
    <li><a class="tabset-btn btn-yearly" href="#yearly">Yearly</a></li>
  </ul>
  <div class="price-table tab-content">
    <div id="monthly">
      <div class="row justify-content-center">
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card">
            <div class="plan-header">
              <span class="category">ROOKIE</span>
              <strong class="price">$9</strong>
              <span class="plan-duration">per month</span>
            </div>
            <div class="features">
              <strong class="title">Up to $1,000 Monthly Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 1 Sub Account</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">FREE 7 DAY TRIAL</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card highlight">
            <div class="plan-header">
              <span class="category">SEMIPRO</span>
              <strong class="price">$29</strong>
              <span class="plan-duration">per month</span>
            </div>
            <div class="features">
              <strong class="title">Up to $3,000 Monthly Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 1 Sub Account</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">FREE 7 DAY TRIAL</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card">
            <div class="plan-header">
              <span class="category">EXPERT</span>
              <strong class="price">$69</strong>
              <span class="plan-duration">per month</span>
            </div>
            <div class="features">
              <strong class="title">Up to $10,000 Monthly Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 3 Sub Accounts</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">FREE 7 DAY TRIAL</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card">
            <div class="plan-header">
              <span class="category">AGENCY</span>
              <strong class="price">$149</strong>
              <span class="plan-duration">per month</span>
            </div>
            <div class="features">
              <strong class="title">Up to $50,000 Monthly Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 10 Sub Accounts</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">FREE 7 DAY TRIAL</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="yearly">
      <div class="row justify-content-center">
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card">
            <div class="plan-header">
              <span class="category">ROOKIE</span>
              <strong class="price">$99</strong>
              <span class="plan-duration">Per Year</span>
            </div>
            <div class="features">
              <strong class="title">Up to $1,000 Monthly Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 1 Sub Account</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">FREE 7 DAY TRIAL</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card highlight">
            <div class="plan-header">
              <span class="category">SEMIPRO</span>
              <strong class="price">$290</strong>
              <span class="plan-duration">Per Year</span>
            </div>
            <div class="features">
              <strong class="title">Up to $3,000 Monthly Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 1 Sub Account</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">FREE 7 DAY TRIAL</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card">
            <div class="plan-header">
              <span class="category">EXPERT</span>
              <strong class="price">$690</strong>
              <span class="plan-duration">Per Year</span>
            </div>
            <div class="features">
              <strong class="title">Up to $10,000 Monthly Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 3 Sub Accounts</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">FREE 7 DAY TRIAL</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="plan-card">
            <div class="plan-header">
              <span class="category">AGENCY</span>
              <strong class="price">$1,490</strong>
              <span class="plan-duration">Per Year</span>
            </div>
            <div class="features">
              <strong class="title">Up to $50,000 Monthly Adspend</strong>
              <ul class="feature-list">
                <li><span class="icon-holder icon-check"></span> 10 Sub Accounts</li>
                <li><span class="icon-holder icon-check"></span> Unlimited Ad Accounts</li>
                <li><span class="icon-holder icon-check"></span> Access to All Features</li>
              </ul>
            </div>
            <a href="#" class="btn btn-trial">FREE 7 DAY TRIAL</a>
            <div class="info">
              <span>30 Day Refund Guarantee</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="note">
    <p>SPEND MORE THAN $50,000/MONTH ON FACEBOOK ADS? CONTACT US FOR SPECIAL PRICING.</p>
  </div>
</div>