<?php
/**
 * Function includes
 *
 * The $function_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */

$function_includes = [
  // General
  'lib/setup.php',     // Theme setup
  'lib/extras.php',    // Custom functions
  'lib/helpers.php',    // helpers
  'lib/clean-up.php',    // Clean up

  // Post meta
  'lib/posttype-taxonomy/cpt-template.php',    // Custom post types
  'lib/posttype-taxonomy/tax-template.php',    // Custom taxonomies

  // Plugins
  'lib/plugins/acf-settings.php',    // ACF Settings
  'lib/plugins/gravity-forms.php',    // Gravity Forms Settings
  'lib/plugins/gform-custom-field.php',    // Gravity Forms Custom Field
  'lib/plugins/custom_navwalker.php',    // Navwalker
  'lib/plugins/expanded-search.php',  // Expand WP Search
];

foreach ($function_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

//ACF Google MAP Key Setting
function my_acf_init() {
  // add google map api key
  acf_update_setting('google_api_key', 'AIzaSyADfnDKUzaIDUoSvOohou3_5z6Wy7kezCM');
}

add_action('acf/init', 'my_acf_init');

// flexible content auto layout
// @uses the_flexible_content('page_blocks')
function the_flexible_content($name) {
  if(have_rows($name)) :
    while(have_rows($name)) : the_row();
      $layout = str_replace('_', '-', strtolower(sanitize_title(get_row_layout())));
      get_template_part('layouts/'. $layout);
    endwhile;
  endif;
}

/**
 * Custom Gravity Form Container
 *
 * @param mixed $field_container
 * @param mixed $field
 * @param mixed $form
 * @param mixed $css_class
 * @param mixed $style
 * @param mixed $field_content
 * @return mixed
 */
function custom_field_container( $field_container, $field, $form, $css_class, $style, $field_content ) {
  if(strpos($field->cssClass, 'gfield-small gfield-incrementer') !== false) {
    $buttons = '<span class="button-increment plus-button"><span class="icon icon-plus"></span></span><span class="button-increment minus-button"><span class="icon icon-minus"></span></span>';
    $field_container = str_replace('{FIELD_CONTENT}', '{FIELD_CONTENT}' . $buttons, $field_container);
  }

  return $field_container;
}
add_filter( 'gform_field_container', 'custom_field_container', 10, 6 );

// function add_types_protectable($types, $rules) {
//   echo "<pre>";
//   print_r($types);
//   die;
//   $types[] = 'html';
//   $types[] = 'htm';

//   return $types;
// }
// add_filter('mepr_rewrite_rules_protect_types', 'add_types_protectable', 11, 2);

if( !function_exists( 'custom_applogin_setup') ){
  function custom_applogin_setup( $user ){
    $user = get_user_by('login',$user);
    $_SESSION['user'] = $user;
    $_SESSION['user_meta'] = get_user_meta( $user->ID );

  }
add_action('wp_login' , 'custom_applogin_setup' );
}

add_action('init', 'set_cookie');

function set_cookie(){
  $cookie_name = "userData";
  $cookie_value = json_encode($_SESSION);
  setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
}



if( function_exists( 'custom_log_unset' )){
  function custom_log_unset(){
      unset($_SESSION);
      setcookie("userData", "", time() - 3600);
  }
  add_action('wp_logout', 'custom_log_unset');
}

if( function_exists('check_session') ){
  function check_session(){
    $session = $_SESSION;
      echo json_encode($_SESSION);
    wp_die();
  }
add_action('wp_ajax_nopriv_check_session', 'check_session');
add_action('wp_check_session', 'check_session');
}


// echo "<pre>";
// print_r($_SESSION);
// die;