			</main>
			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<nav class="footer-nav">
                <?php wp_nav_menu(['location' => 'theme_location', 'container' => false, 'menu_class' => 'd-flex flex-wrap justify-content-center justify-content-md-start text-center']); ?>
							</nav>
            </div>
            <?php if($made_by_text = get_field('made_by_text', 'option')): ?>
              <div class="col-md-4">
                <div class="made-with text-center text-md-right">
                  <?php echo $made_by_text; ?>
                </div>
              </div>
            <?php endif; ?>
					</div>
				</div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-76300210-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-76300210-1');
</script>
<!-- End Google Analytics -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1352351844789066');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1352351844789066&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

			</footer>
		</div>
  </div>
  
  <?php wp_footer(); ?>
	<script>
		WebFont.load({
			google: {
				families: ['Montserrat:400,500,600']
			}
		});
	</script>

</body>
</html>