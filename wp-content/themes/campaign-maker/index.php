<?php get_header(); ?>

<div class="main-content <?php the_field('content_extra_classes'); ?>">
  <div class="container">

	<?php if(have_posts()): while(have_posts()) : the_post(); ?>

		<?php the_content(); ?>

    		<?php the_flexible_content('components'); ?>

	<?php endwhile; endif; ?>
  </div>
</div>

<?php get_footer(); ?>