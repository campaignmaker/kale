<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Facebook App details
| -------------------------------------------------------------------
|
| To get an facebook app details you have to be a registered developer
| at http://developer.facebook.com and create an app for your project.
|
|  facebook_app_id               string  Your facebook app ID.
|  facebook_app_secret           string  Your facebook app secret.
|  facebook_login_type           string  Set login type. (web, js, canvas)
|  facebook_login_redirect_url   string  URL tor redirect back to after login. Do not include domain.
|  facebook_logout_redirect_url  string  URL tor redirect back to after login. Do not include domain.
|  facebook_permissions          array   The permissions you need.
|  facebook_graph_version        string  Set Facebook Graph version to be used.
*/

$config['facebook_app_id']              = '1198746903472361';
$config['facebook_app_secret']          = '65110e5912df5b74d01a1f78d9ce3047';
$config['facebook_permissions']         = array('public_profile','email','ads_read','ads_management','pages_show_list','publish_pages','manage_pages');
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'dashboard';
$config['facebook_logout_redirect_url'] = 'logout';
$config['facebook_graph_version']       = 'v3.0';
$config['facebook_graph_url']           = 'https://graph.facebook.com/';
