<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
use FacebookAds\Api;

use FacebookAds\Object\Ad;

use FacebookAds\Object\AdUser;

use Facebook\Facebook;

use Facebook\FacebookApp;

use FacebookAds\Object\AdAccount;

use FacebookAds\Object\Fields\AdAccountFields;

use FacebookAds\Object\Campaign;

use FacebookAds\Object\Fields\CampaignFields;

use FacebookAds\Object\Fields\AdSetFields;

use FacebookAds\Object\Values\AdObjectives;

use FacebookAds\Object\Insights;

use FacebookAds\Object\Fields\InsightsFields;

use FacebookAds\Object\Values\InsightsPresets;

use FacebookAds\Object\Fields\AdFields;

use FacebookAds\Object\Values\InsightsActionBreakdowns;

use FacebookAds\Object\Values\InsightsBreakdowns;

use FacebookAds\Object\Values\InsightsLevels;

use FacebookAds\Object\Values\InsightsIncrements;

use FacebookAds\Object\Values\InsightsOperators;

use Facebook\FacebookRequest;

use Facebook\FacebookResponse;
class Analysis extends CI_Controller {
	public $access_token;

    public $user_id;

    public $add_account_id;

    public $add_title;

    public $is_ajax;

    public $limit;
    public $field;
    public function __construct() {
		 error_reporting(1);
        parent::__construct();
        $this->load->model('Users_model');
		$this->load->model('Campaign_model');
        $this->is_ajax = 0;
        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
        $this->user_id = $this->session->userdata['logged_in']['id'];
        $this->getAdAccount();
        // User subscriptions expire 
       

        // User account spend limit reach
        
        if (isset($this->session->userdata['logged_in']) && !empty($this->session->userdata['logged_in'])) {
            $app_id = $this->config->item('facebook_app_id');
            $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
          
            //echo current_url(); exit;
            $this->session->set_userdata('last_page', current_url());
            
            redirect('login');
        
        //    redirect('/');
        }
         if (userSubcriptionInvalid()) {
            redirect('/updateprofile');
        }
        if (userAccountSpendCheck()) {
            redirect('/updateprofile');
        }
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));     
    }

    public function index($adAccountId) {
		
        //$data = $this->Users_model->Getautomaticoptimization($this->user_id);
		//echo $this->input->get('adaccountid', TRUE);
		$limit = "lifetime";//$this->dateval;//$this->sDate."#".$this->eDate;

        $data = new stdClass();
        $adAccountId = $adAccountId;//$this->input->get('adaccountid', TRUE);//$this->input->post('adAccountId');
        $limit = "lifetime";//$this->input->post('limit');
        $field = "spend";//$this->input->post('field');
        $this->session->set_userdata('dateval', $limit);
		if(empty($adAccountId)){
			$adAccountId = $this->add_account_id;
		}
        if (!empty($adAccountId)) {
            $this->setAddAccountId($adAccountId);
            $this->getCurrency();
            if (strpos($limit, '#') !== false) {
                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
            } else {
                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
            }
        } else {
            $data->adAccountData = $this->getAdAccount();
        }
        $data->addAccountId = $this->add_account_id;
        $data->adaccounts = $data->adAccountData['response'];
        $data->adaccountsCount = $data->adAccountData['count'];
        $data->error = $data->adAccountData['error'];
        $data->success = $data->adAccountData['success'];
        $data->limit = $limit;
        $accountArr = $this->Users_model->getUserAddAccountName($adAccountId);
        $data->accountName = $accountArr[0]->add_title;

        #echo "<PRE>";print_R($data);exit;
        $data->automaticoptimizationdata = $this->Campaign_model->Getautomaticoptimization($this->user_id);
        
		loadView('analysis', $data);
    }
    

    public function getCampaignAsdsets() {
        $finaladsethtml = '';
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$this->input->post('campaignid')."/adsets?fields=id,name&limit=1000&access_token=" . $this->access_token;
        #echo $url;exit;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            if (isset($response['data'])) {
                $finaladsethtml .= '<option value="0">Select Adest</option>';
                foreach ($response['data'] as $row) {
                    $finaladsethtml .= '<option value="'.$row["id"].'">'.$row["name"].'</option>';
                }
            }
            else{
                echo '<option value="0">Select Adest</option>';
                exit;
            }
            /*print_r($response);
            exit;*/
        }
        catch (Exception $ex) {
            echo $e->getMessage();
            exit;
        }
        echo $finaladsethtml;
        exit;    
    }


    function getCurlAdsetlifetime() {
       //$returnRow = array();
        //$resultArray = array();
        $returnmaindata = '';
        //$dataArray = explode("#", $limit);
        //$addSetFields = $this->getAdsFields(1);
        $addSetId = $this->input->post('addSetId');//"23842585923380160";
        $campaignObjectivce = $this->input->post('campaignObjectivce'); //"CONVERSIONS";
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/?fields=insights.date_preset(lifetime){inline_link_clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&access_token=" . $this->access_token;
        #echo $url;exit;
        //echo $url;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            if(isset($response['insights'])){
                if($campaignObjectivce == 'CONVERSIONS'){
                    $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
                    foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'offsite_conversion'){
                            $conversionval = $row['value'];
                        }
                    }
                    

                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td><img src="'.$this->config->item('assets').'newdesign/images/total-result.png"
                                 style="margin-right: 2px"/>
                            <span class="text-primary">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" style="margin-top: -5px"/>
                            <span class="text-primary">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'offsite_conversion';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";*/
                        //exit;
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
								if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

						if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                        $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            13 - 17';
                            if($spendarray['13-17s']/$tempArr['13-17'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="13-17"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
					<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            18 - 24';
                            if($spendarray['18-24s']/$tempArr['18-24'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="18-24"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n25.png" height="18" width="18"/>
                            25 - 34';
                            if($spendarray['25-34s']/$tempArr['25-34'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="25-34"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n35.png" height="18" width="18"/>
                            35 - 44';
                            if($spendarray['35-44s']/$tempArr['35-44'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="35-44"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/n45.png" height="18" width="18"/>
                            45 - 54';
                            if($spendarray['45-54s']/$tempArr['45-54'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="45-54"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            55 - 65';
                            if($spendarray['55-64s']/$tempArr['55-64'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="55-64"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            65+';
                            if($spendarray['65+s']/$tempArr['65+'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['45-54s']/$tempArr['45-54'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="65+"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'offsite_conversion';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nMen.png" height="15" width="15"/>
                            Men';
                            if($genspendarray['males']/$male < $genspendarray['unknowns']/$other && $genspendarray['males']/$male < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nWomen.png" height="15" width="15"/>
                            Women';
                            if($genspendarray['females']/$female < $genspendarray['males']/$male && $genspendarray['females']/$female < $genspendarray['unknowns']/$other)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="2"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            
                            Other';
                            if($genspendarray['unknowns']/$other < $genspendarray['males']/$male && $genspendarray['unknowns']/$other < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'offsite_conversion';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }

                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nDesktop.png" height="15" width="15"/>
                            Desktop Newsfeed';
                            if($placespendarray['desktops']/$desktop < $placespendarray['mobiles']/$mobile && $placespendarray['desktops']/$desktop < $placespendarray['instagrams']/$instant_article && $placespendarray['desktops']/$desktop < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['desktops']/$desktop < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="desktop"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nMobile.png" height="15" width="15"/>
                            Mobile Newsfeed';
                            if($placespendarray['mobiles']/$mobile < $placespendarray['desktops']/$desktop && $placespendarray['mobiles']/$mobile < $placespendarray['instagrams']/$instant_article && $placespendarray['mobiles']/$mobile < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['mobiles']/$mobile < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="mobile"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nInstagram.png" height="15" width="15"/>
                            Instagram Feed';
                            if($placespendarray['instagrams']/$instant_article < $placespendarray['mobiles']/$mobile && $placespendarray['instagrams']/$instant_article < $placespendarray['desktops']/$desktop && $placespendarray['instagrams']/$instant_article < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['instagrams']/$instant_article < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="instagram"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nUser.png" height="15" width="15"/>
                            Audience Network';
                            if($placespendarray['audnetworks']/$mobile_external_only < $placespendarray['mobiles']/$mobile && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['instagrams']/$instant_article && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['desktops']/$desktop && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="audience"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nComputer.png" height="15" width="15"/>
                            Desktop Right Side';
                            if($placespendarray['rhss']/$right_hand < $placespendarray['mobiles']/$mobile && $placespendarray['rhss']/$right_hand < $placespendarray['instagrams']/$instant_article && $placespendarray['rhss']/$right_hand < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['rhss']/$right_hand < $placespendarray['desktops']/$desktop)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="rhs"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'offsite_conversion';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;

                }
                elseif($campaignObjectivce == 'LINK_CLICKS' || $campaignObjectivce == 'PRODUCT_CATALOG_SALES'){
                    $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
                    /*foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'offsite_conversion'){
                            $conversionval = $row['value'];
                        }
                    }*/
                    $conversionval = $response['insights']['data'][0]['inline_link_clicks'];
                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td><img src="'.$this->config->item('assets').'newdesign/images/total-result.png"
                                 style="margin-right: 2px"/>
                            <span class="text-primary">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" style="margin-top: -5px"/>
                            <span class="text-primary">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'inline_link_clicks';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";
                        exit;*/
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
                                if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
								if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

						if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                        $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            13 - 17';
                            if($spendarray['13-17s']/$tempArr['13-17'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="13-17"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
					<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            18 - 24';
                            if($spendarray['18-24s']/$tempArr['18-24'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="18-24"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n25.png" height="18" width="18"/>
                            25 - 34';
                            if($spendarray['25-34s']/$tempArr['25-34'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="25-34"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n35.png" height="18" width="18"/>
                            35 - 44';
                            if($spendarray['35-44s']/$tempArr['35-44'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="35-44"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/n45.png" height="18" width="18"/>
                            45 - 54';
                            if($spendarray['45-54s']/$tempArr['45-54'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="45-54"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            55 - 65';
                            if($spendarray['55-64s']/$tempArr['55-64'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="55-64"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            65+';
                            if($spendarray['65+s']/$tempArr['65+'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['45-54s']/$tempArr['45-54'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="65+"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'inline_link_clicks';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nMen.png" height="15" width="15"/>
                            Men';
                            if($genspendarray['males']/$male < $genspendarray['unknowns']/$other && $genspendarray['males']/$male < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nWomen.png" height="15" width="15"/>
                            Women';
                            if($genspendarray['females']/$female < $genspendarray['males']/$male && $genspendarray['females']/$female < $genspendarray['unknowns']/$other)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="2"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            
                            Other';
                            if($genspendarray['unknowns']/$other < $genspendarray['males']/$male && $genspendarray['unknowns']/$other < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'inline_link_clicks';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }
                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nDesktop.png" height="15" width="15"/>
                            Desktop Newsfeed';
                            if($placespendarray['desktops']/$desktop < $placespendarray['mobiles']/$mobile && $placespendarray['desktops']/$desktop < $placespendarray['instagrams']/$instant_article && $placespendarray['desktops']/$desktop < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['desktops']/$desktop < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="desktop"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nMobile.png" height="15" width="15"/>
                            Mobile Newsfeed';
                            if($placespendarray['mobiles']/$mobile < $placespendarray['desktops']/$desktop && $placespendarray['mobiles']/$mobile < $placespendarray['instagrams']/$instant_article && $placespendarray['mobiles']/$mobile < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['mobiles']/$mobile < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="mobile"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nInstagram.png" height="15" width="15"/>
                            Instagram Feed';
                            if($placespendarray['instagrams']/$instant_article < $placespendarray['mobiles']/$mobile && $placespendarray['instagrams']/$instant_article < $placespendarray['desktops']/$desktop && $placespendarray['instagrams']/$instant_article < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['instagrams']/$instant_article < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="instagram"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nUser.png" height="15" width="15"/>
                            Audience Network';
                            if($placespendarray['audnetworks']/$mobile_external_only < $placespendarray['mobiles']/$mobile && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['instagrams']/$instant_article && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['desktops']/$desktop && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="audience"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nComputer.png" height="15" width="15"/>
                            Desktop Right Side';
                            if($placespendarray['rhss']/$right_hand < $placespendarray['mobiles']/$mobile && $placespendarray['rhss']/$right_hand < $placespendarray['instagrams']/$instant_article && $placespendarray['rhss']/$right_hand < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['rhss']/$right_hand < $placespendarray['desktops']/$desktop)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="rhs"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'inline_link_clicks';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;

                }
				elseif($campaignObjectivce == 'POST_ENGAGEMENT'){
                    $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
					foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'page_engagement'){
                            $conversionval = $row['value'];
                        }
                    }
                   // $conversionval = $response['insights']['data'][0]['inline_link_clicks'];
                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td><img src="'.$this->config->item('assets').'newdesign/images/total-result.png"
                                 style="margin-right: 2px"/>
                            <span class="text-primary">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" style="margin-top: -5px"/>
                            <span class="text-primary">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'page_engagement';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";*/
                        //exit;
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
								if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

						if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                        $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            13 - 17';
                            if($spendarray['13-17s']/$tempArr['13-17'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="13-17"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
					<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            18 - 24';
                            if($spendarray['18-24s']/$tempArr['18-24'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="18-24"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n25.png" height="18" width="18"/>
                            25 - 34';
                            if($spendarray['25-34s']/$tempArr['25-34'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="25-34"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n35.png" height="18" width="18"/>
                            35 - 44';
                            if($spendarray['35-44s']/$tempArr['35-44'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="35-44"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/n45.png" height="18" width="18"/>
                            45 - 54';
                            if($spendarray['45-54s']/$tempArr['45-54'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="45-54"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            55 - 65';
                            if($spendarray['55-64s']/$tempArr['55-64'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="55-64"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            65+';
                            if($spendarray['65+s']/$tempArr['65+'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['45-54s']/$tempArr['45-54'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="65+"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'page_engagement';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nMen.png" height="15" width="15"/>
                            Men';
                            if($genspendarray['males']/$male < $genspendarray['unknowns']/$other && $genspendarray['males']/$male < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nWomen.png" height="15" width="15"/>
                            Women';
                            if($genspendarray['females']/$female < $genspendarray['males']/$male && $genspendarray['females']/$female < $genspendarray['unknowns']/$other)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="2"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            
                            Other';
                            if($genspendarray['unknowns']/$other < $genspendarray['males']/$male && $genspendarray['unknowns']/$other < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'page_engagement';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }
                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nDesktop.png" height="15" width="15"/>
                            Desktop Newsfeed';
                            if($placespendarray['desktops']/$desktop < $placespendarray['mobiles']/$mobile && $placespendarray['desktops']/$desktop < $placespendarray['instagrams']/$instant_article && $placespendarray['desktops']/$desktop < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['desktops']/$desktop < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="desktop"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nMobile.png" height="15" width="15"/>
                            Mobile Newsfeed';
                            if($placespendarray['mobiles']/$mobile < $placespendarray['desktops']/$desktop && $placespendarray['mobiles']/$mobile < $placespendarray['instagrams']/$instant_article && $placespendarray['mobiles']/$mobile < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['mobiles']/$mobile < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="mobile"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nInstagram.png" height="15" width="15"/>
                            Instagram Feed';
                            if($placespendarray['instagrams']/$instant_article < $placespendarray['mobiles']/$mobile && $placespendarray['instagrams']/$instant_article < $placespendarray['desktops']/$desktop && $placespendarray['instagrams']/$instant_article < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['instagrams']/$instant_article < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="instagram"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nUser.png" height="15" width="15"/>
                            Audience Network';
                            if($placespendarray['audnetworks']/$mobile_external_only < $placespendarray['mobiles']/$mobile && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['instagrams']/$instant_article && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['desktops']/$desktop && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="audience"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nComputer.png" height="15" width="15"/>
                            Desktop Right Side';
                            if($placespendarray['rhss']/$right_hand < $placespendarray['mobiles']/$mobile && $placespendarray['rhss']/$right_hand < $placespendarray['instagrams']/$instant_article && $placespendarray['rhss']/$right_hand < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['rhss']/$right_hand < $placespendarray['desktops']/$desktop)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="rhs"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'page_engagement';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;

                }
				elseif($campaignObjectivce == 'PAGE_LIKES'){
                    $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
					foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'like'){
                            $conversionval = $row['value'];
                        }
                    }
                   // $conversionval = $response['insights']['data'][0]['inline_link_clicks'];
                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td><img src="'.$this->config->item('assets').'newdesign/images/total-result.png"
                                 style="margin-right: 2px"/>
                            <span class="text-primary">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" style="margin-top: -5px"/>
                            <span class="text-primary">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'like';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";*/
                        //exit;
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
								if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

						if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                       $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            13 - 17';
                            if($spendarray['13-17s']/$tempArr['13-17'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="13-17"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
					<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            18 - 24';
                            if($spendarray['18-24s']/$tempArr['18-24'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="18-24"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n25.png" height="18" width="18"/>
                            25 - 34';
                            if($spendarray['25-34s']/$tempArr['25-34'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="25-34"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n35.png" height="18" width="18"/>
                            35 - 44';
                            if($spendarray['35-44s']/$tempArr['35-44'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="35-44"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/n45.png" height="18" width="18"/>
                            45 - 54';
                            if($spendarray['45-54s']/$tempArr['45-54'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="45-54"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            55 - 65';
                            if($spendarray['55-64s']/$tempArr['55-64'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="55-64"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            65+';
                            if($spendarray['65+s']/$tempArr['65+'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['45-54s']/$tempArr['45-54'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="65+"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'like';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                   $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nMen.png" height="15" width="15"/>
                            Men';
                            if($genspendarray['males']/$male < $genspendarray['unknowns']/$other && $genspendarray['males']/$male < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nWomen.png" height="15" width="15"/>
                            Women';
                            if($genspendarray['females']/$female < $genspendarray['males']/$male && $genspendarray['females']/$female < $genspendarray['unknowns']/$other)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="2"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            
                            Other';
                            if($genspendarray['unknowns']/$other < $genspendarray['males']/$male && $genspendarray['unknowns']/$other < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'like';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }
                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                           $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nDesktop.png" height="15" width="15"/>
                            Desktop Newsfeed';
                            if($placespendarray['desktops']/$desktop < $placespendarray['mobiles']/$mobile && $placespendarray['desktops']/$desktop < $placespendarray['instagrams']/$instant_article && $placespendarray['desktops']/$desktop < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['desktops']/$desktop < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="desktop"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nMobile.png" height="15" width="15"/>
                            Mobile Newsfeed';
                            if($placespendarray['mobiles']/$mobile < $placespendarray['desktops']/$desktop && $placespendarray['mobiles']/$mobile < $placespendarray['instagrams']/$instant_article && $placespendarray['mobiles']/$mobile < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['mobiles']/$mobile < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="mobile"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nInstagram.png" height="15" width="15"/>
                            Instagram Feed';
                            if($placespendarray['instagrams']/$instant_article < $placespendarray['mobiles']/$mobile && $placespendarray['instagrams']/$instant_article < $placespendarray['desktops']/$desktop && $placespendarray['instagrams']/$instant_article < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['instagrams']/$instant_article < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="instagram"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nUser.png" height="15" width="15"/>
                            Audience Network';
                            if($placespendarray['audnetworks']/$mobile_external_only < $placespendarray['mobiles']/$mobile && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['instagrams']/$instant_article && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['desktops']/$desktop && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="audience"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nComputer.png" height="15" width="15"/>
                            Desktop Right Side';
                            if($placespendarray['rhss']/$right_hand < $placespendarray['mobiles']/$mobile && $placespendarray['rhss']/$right_hand < $placespendarray['instagrams']/$instant_article && $placespendarray['rhss']/$right_hand < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['rhss']/$right_hand < $placespendarray['desktops']/$desktop)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="rhs"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'like';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;

                }
                elseif($campaignObjectivce == 'LEAD_GENERATION'){
                        $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
                    foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'leadgen.other'){
                            $conversionval = $row['value'];
                        }
                    }
                    

                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td><img src="'.$this->config->item('assets').'newdesign/images/total-result.png"
                                 style="margin-right: 2px"/>
                            <span class="text-primary">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" style="margin-top: -5px"/>
                            <span class="text-primary">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'leadgen.other';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";*/
                        //exit;
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
                                if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

                        if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                        $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            13 - 17';
                            if($spendarray['13-17s']/$tempArr['13-17'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['13-17s']/$tempArr['13-17'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="13-17"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
					<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n18.png" height="18" width="18"/>
                            18 - 24';
                            if($spendarray['18-24s']/$tempArr['18-24'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['18-24s']/$tempArr['18-24'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="18-24"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n25.png" height="18" width="18"/>
                            25 - 34';
                            if($spendarray['25-34s']/$tempArr['25-34'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['25-34s']/$tempArr['25-34'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="25-34"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n35.png" height="18" width="18"/>
                            35 - 44';
                            if($spendarray['35-44s']/$tempArr['35-44'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['35-44s']/$tempArr['35-44'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="35-44"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/n45.png" height="18" width="18"/>
                            45 - 54';
                            if($spendarray['45-54s']/$tempArr['45-54'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['45-54s']/$tempArr['45-54'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="45-54"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            55 - 65';
                            if($spendarray['55-64s']/$tempArr['55-64'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['45-54s']/$tempArr['45-54'] && $spendarray['55-64s']/$tempArr['55-64'] < $spendarray['65+s']/$tempArr['65+'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="55-64"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n55.png" height="18" width="18"/>
                            65+';
                            if($spendarray['65+s']/$tempArr['65+'] < $spendarray['13-17s']/$tempArr['13-17'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['18-24s']/$tempArr['18-24'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['25-34s']/$tempArr['25-34'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['35-44s']/$tempArr['35-44'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['55-64s']/$tempArr['55-64'] && $spendarray['65+s']/$tempArr['65+'] < $spendarray['45-54s']/$tempArr['45-54'])
                                $returnmaindata .= '<input type="hidden" id="bestagerange" name="bestagerange" value="65+"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'leadgen.other';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nMen.png" height="15" width="15"/>
                            Men';
                            if($genspendarray['males']/$male < $genspendarray['unknowns']/$other && $genspendarray['males']/$male < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nWomen.png" height="15" width="15"/>
                            Women';
                            if($genspendarray['females']/$female < $genspendarray['males']/$male && $genspendarray['females']/$female < $genspendarray['unknowns']/$other)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="2"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            
                            Other';
                            if($genspendarray['unknowns']/$other < $genspendarray['males']/$male && $genspendarray['unknowns']/$other < $genspendarray['females']/$female)
                                $returnmaindata .= '<input type="hidden" id="bestgender" name="bestgender" value="1"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'leadgen.other';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }

                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nDesktop.png" height="15" width="15"/>
                            Desktop Newsfeed';
                            if($placespendarray['desktops']/$desktop < $placespendarray['mobiles']/$mobile && $placespendarray['desktops']/$desktop < $placespendarray['instagrams']/$instant_article && $placespendarray['desktops']/$desktop < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['desktops']/$desktop < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="desktop"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nMobile.png" height="15" width="15"/>
                            Mobile Newsfeed';
                            if($placespendarray['mobiles']/$mobile < $placespendarray['desktops']/$desktop && $placespendarray['mobiles']/$mobile < $placespendarray['instagrams']/$instant_article && $placespendarray['mobiles']/$mobile < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['mobiles']/$mobile < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="mobile"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nInstagram.png" height="15" width="15"/>
                            Instagram Feed';
                            if($placespendarray['instagrams']/$instant_article < $placespendarray['mobiles']/$mobile && $placespendarray['instagrams']/$instant_article < $placespendarray['desktops']/$desktop && $placespendarray['instagrams']/$instant_article < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['instagrams']/$instant_article < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="instagram"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nUser.png" height="15" width="15"/>
                            Audience Network';
                            if($placespendarray['audnetworks']/$mobile_external_only < $placespendarray['mobiles']/$mobile && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['instagrams']/$instant_article && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['desktops']/$desktop && $placespendarray['audnetworks']/$mobile_external_only < $placespendarray['rhss']/$right_hand)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="audience"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/nComputer.png" height="15" width="15"/>
                            Desktop Right Side';
                            if($placespendarray['rhss']/$right_hand < $placespendarray['mobiles']/$mobile && $placespendarray['rhss']/$right_hand < $placespendarray['instagrams']/$instant_article && $placespendarray['rhss']/$right_hand < $placespendarray['audnetworks']/$mobile_external_only && $placespendarray['rhss']/$right_hand < $placespendarray['desktops']/$desktop)
                                $returnmaindata .= '<input type="hidden" id="bestadplacement" name="bestadplacement" value="rhs"><img src="'.$this->config->item('assets').'newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'leadgen.other';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>
                            <img src="'.$this->config->item('assets').'newdesign/images/nFolder.png" height="12" width="15"/>
                            '.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;
                }
            }
            $urlb3 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version').'/'.$addSetId.'/?fields=name,end_time,daily_budget,targeting&access_token=' . $this->access_token;
                        //echo $url;exit;
                        $chb3 = curl_init($urlb3);
                        curl_setopt($chb3, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($chb3, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($chb3, CURLOPT_FOLLOWLOCATION, true);
                        $resultb3 = curl_exec($chb3);
                        curl_close($chb3);
                        $responseb3 = json_decode($resultb3, true);  
                        $returnmaindata .= '|||||';
                        if(isset($responseb3['daily_budget']))
                            $returnmaindata .= $this->session->userdata('cur_currency').$responseb3['daily_budget']/100;
                        else
                            $returnmaindata .= $this->session->userdata('cur_currency')."0";
           /* echo "<pre>";
            print_r($response);
            echo "</pre>";*/
            } catch (Exception $ex) {
                echo "error";
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $e->getMessage(),
                        "success" => ""
                    );
                }
                echo $returnmaindata;
                exit;
        }

    
    public function getCurrency() {

            $adAccountId = "act_" . $this->getAddAccountId();

            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/?fields=name,currency&access_token=" . $this->access_token;



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);



            $currency = 'USD';

            if (!empty($response['currency'])) {

                $currency = $response['currency'];

            }

        $locale = 'en-US'; //browser or user locale

        $fmt = new NumberFormatter($locale . "@currency=$currency", NumberFormatter::CURRENCY);

        $symbol = $fmt->getSymbol(NumberFormatter::CURRENCY_SYMBOL);

        $this->session->set_userdata('cur_currency', $symbol);

    }    
    protected function getAdAccount() {

        $result = $this->Users_model->getUserAddAccountId($this->user_id);



        if (count($result) > 0) {

            if (count($result) == 1) {

                $finalRes = "";

                $this->setAddAccountId($result[0]->ad_account_id);

                $this->getCurrency();

                $this->add_account_id = $result[0]->ad_account_id;

                $this->add_title = $result[0]->add_title;

                // $limit=date('Y-m-d', strtotime('-7 days'))."#".date('Y-m-d');

                //$finalRes = $this->getCurlCampaignsDateRange($limit);

                //$finalRes = $this->getCurlCampaigns("");

                $finalRes = $this->getCurlCampaignsDateRange_new($this->dateval);

                // $this->getPrintResult($finalRes);

                $resultArray = array(

                    "count" => $finalRes['count'],

                    "response" => $finalRes['response'],

                    "error" => $finalRes['error'],

                    "success" => $finalRes['succes']

                    );

                // End Get Ad  

            } else {

                $resultArray = array(

                    "count" => 2,

                    "response" => $result,

                    "error" => "",

                    "success" => ""

                    );

            }

        } else {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => "No data available against your account.",

                "success" => ""

                );

        }

        //  $this->getPrintResult($resultArray);

        return $resultArray;

    }



    /* From LIVE */



    protected function fbAdAccount($add_account_id, $selected_limit) {

        try {

            $resultArray = $this->getAllCampaigns($add_account_id, $selected_limit);

        } catch (Exception $e) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        return $resultArray;

    }



    protected function getAllCampaigns($add_account_id, $selected_limit) {

        try {

            $adAccountOjb = new AdAccount($add_account_id);

            $insightsArray = array();

            $campaignDataArray = array();



            $fields = $this->getCampaignFields();

            $insights = $this->getCampaignInsightFields();

            $params = $this->getCampaignParams($selected_limit);



            $response = $adAccountOjb->getCampaigns($fields);



            foreach ($response as $row) {

                try {



                    $campaignObj = new Campaign($row->id);

                    $res = $this->get_hub_insight_per($campaignObj, $insights, $params);

                    foreach ($res as $rs) {

                        //$rs->account_id;

                        // $this->getPrintResult($res);

                        $insightsArray[] = array(

                            "account_name" => $rs->account_name,

                            "actions_per_impression" => $rs->actions_per_impression,

                            "call_to_action_clicks" => $rs->call_to_action_clicks,

                            "campaign_id" => $rs->campaign_id,

                            "campaign_name" => $rs->campaign_name,

                            "cost_per_action_type" => $rs->cost_per_action_type,

                            "cost_per_total_action" => $rs->cost_per_total_action,

                            "cost_per_unique_click" => $rs->cost_per_unique_click,

                            "cost_per_inline_link_click" => $rs->cost_per_inline_link_click,

                            "cost_per_inline_post_engagement" => $rs->cost_per_inline_post_engagement,

                            "cpm" => $rs->cpm,

                            "cpp" => $rs->cpp,

                            "ctr" => $rs->ctr,

                            "date_start" => $rs->date_start,

                            "date_stop" => $rs->date_stop,

                            "frequency" => $rs->frequency,

                            "impressions" => $rs->impressions,

                            "inline_link_clicks" => $rs->inline_link_clicks,

                            "inline_post_engagement" => $rs->inline_post_engagement,

                            "reach" => $rs->reach,

                            "spend" => $rs->spend,

                            "total_action_value" => $rs->total_action_value,

                            "total_actions" => $rs->total_actions,

                            "total_unique_actions" => $rs->total_unique_actions,

                            "unique_clicks" => $rs->unique_clicks,

                            "unique_ctr" => $rs->unique_ctr

                            );

                    }

                    if ($insightsArray) {

                        $campaignDataArray[] = array(

                            "id" => $row->id,

                            "name" => $row->name,

                            "created_time" => $row->created_time,

                            "effective_status" => $row->effective_status,

                            "status_paused" => $row->status_paused,

                            "AllArray" => $row,

                            "insights" => $insightsArray

                            );

                        $insightsArray = array();

                    }

                } catch (Exception $e) {

                    return $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $e->getMessage(),

                        "success" => ""

                        );

                }

            }



            if (count($campaignDataArray) > 1) {

                $finalArray = $this->getFinalResult($campaignDataArray);

                $resultArray = array(

                    "count" => 1,

                    "response" => $finalArray,

                    "error" => "",

                    "success" => ""

                    );

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => "No record found.",

                    "success" => ""

                    );

            }

        } catch (Exception $e) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        return $resultArray;

    }



    /* Privat functions */



    private function setAddAccountId($adAccountId) {

        $this->add_account_id = $adAccountId;

    }



    private function getAddAccountId() {

        return $this->add_account_id;

    }



    /* End Private */



    function get_hub_insight_per($obj, $insights, $param) {

        try {

//            if (!empty($param))

//            {

//                $params = array(

//                    $param

//                );

//                return $response = $obj->getInsights($insights, $params);

//            }

//            else

//            {

            return $response = $obj->getInsights($insights);

            // }

        } catch (Exception $e) {

            return $e->getMessage();

        }

    }



    function getCampaignFields($curl) {

        if ($curl == 1) {

            $fields = "id,account_id,name,objective,buying_type,created_time,start_time,stop_time,effective_status,currency,configured_status";

        } else {

            $fields = array(

                CampaignFields::ID,

                CampaignFields::ACCOUNT_ID,

                CampaignFields::NAME,

                CampaignFields::OBJECTIVE,

                CampaignFields::BUYING_TYPE,

                CampaignFields::PROMOTED_OBJECT,

                CampaignFields::ADLABELS,

                CampaignFields::CREATED_TIME,

                CampaignFields::START_TIME,

                CampaignFields::STOP_TIME,

                CampaignFields::UPDATED_TIME,

                CampaignFields::EFFECTIVE_STATUS,

                CampaignFields::STATUS_PAUSED,

                );

        }

        return $fields;

    }



    function getCampaignInsightFields($curl) {

        if ($curl == 1) {

            $fields = "{date_start,date_stop,buying_type,campaign_id,actions{action_type,value}}";

        } else {



            $fields = array(

                InsightsFields::ACCOUNT_ID,

                InsightsFields::IMPRESSIONS,

                InsightsFields::UNIQUE_CLICKS,

                InsightsFields::REACH,

                InsightsFields::INLINE_LINK_CLICKS,

                InsightsFields::COST_PER_INLINE_LINK_CLICK,

                InsightsFields::COST_PER_UNIQUE_CLICK,

                InsightsFields::SPEND,

                InsightsFields::COST_PER_ACTION_TYPE

                );

        }

        return $fields;

    }



    function getCampaignParams($type) {

        return $params = ".date_preset($type)";

    }



    function getFinalResult($campaignDataArray) {

        $activeCampaigns = 0;

        $inActiveCampaigns = 0;

        $cost_per_unique_click = 0;

        $total_cost_per_inline_click = 0;

        $impressions = 0;

        $total_inline_clicks = 0;

        $reach = 0;

        $unique_clicks = 0;

        $unique_ctr = 0;

        $spent = 0;

        $actions = 0;

        $cost_per_total_action = 0;



        $inline_clicks = 0;

        $cost_per_inline_clicks = 0;

        $pendingCampaigns = 0;

        $disapprovedCampaigns = 0;

        $total = 0;

        $total1 = 0;

        $total2 = 0;

        $conversion = 0;

        #echo "<PRE>";print_R($campaignDataArray);exit;

        $returnRow = array();

        foreach ($campaignDataArray as $row) {

            if ($row['effective_status'] == "ACTIVE" || $row['effective_status'] == "PREAPPROVED") {

                $activeCampaigns++;

                $status = "Active";

            } 

            else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {

                $inActiveCampaigns++;

                $status = "Inactive";

            } 

            else if ($row['effective_status'] == "PENDING_REVIEW") {

                $pendingCampaigns++;

                $status = "In Review";

            } 

            else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {

                $disapprovedCampaigns++;

                $status = "Denied";

            }

            $total++;

            

            if (isset($row['insights'])) {

                // echo $row['name']."<br>";

                $inline_post_engagement1 = $like1 = '0';

                $conversion = $cost_per_conversion = 0;

                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';

                if (isset($row['insights']['data'][0])) {

                    foreach ($row['insights']['data'][0]['actions'] as $rs) {

                        #echo "<PRE>";print_r($row['insights']);exit;

                        if ($row['insights']['data'][0]['clicks']) {

                            $inline_clicks = $row['insights']['data'][0]['clicks'];

                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];

                        }

                        if ($rs['action_type'] == "leadgen.other") {

                            if($rs['value']){

                                $conversion = $rs['value'];

                            }

                            else{

                                $conversion = 0;

                            }

                        } 

                        if ($rs['action_type'] == "offsite_conversion") {

                            if($rs['value']){

                                $conversion += $rs['value'];

                                $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];

                            }

                            else{

                                $conversion = 0;

                                $cost_per_conversion = 0;

                            }

                        } 

                        if($rs['action_type'] == 'page_engagement'){

                            if($rs['value']){

                                $total1++;

                                $inline_post_engagement1 = $rs['value'];

                            }

                            else{

                                $inline_post_engagement1 = 0;

                            }

                        }

                        if($rs['action_type'] == 'like'){

                            if($rs['value']){

                                $total2++;

                                $like1 = $rs['value'];

                            }

                            else{

                                $like1 = 0;

                            }

                        }

                    }

                    foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {

                        if($rs['action_type'] == 'page_engagement'){

                            if($rs['value']){

                                $cost_per_inline_post_engagement1 = $rs['value'];

                            }

                            else{

                                $cost_per_inline_post_engagement1 = 0;

                            }

                        }

                        if ($rs['action_type'] == "leadgen.other") {

                            if($rs['value']){

                                $cost_per_conversion = $rs['value'];

                            }

                            else{

                                $cost_per_conversion = 0;

                            }

                        } 

                        if($rs['action_type'] == 'like'){

                            if($rs['value']){

                                $cost_per_like1 = $rs['value'];

                            }

                            else{

                                $cost_per_like1 = 0;

                            }

                        }

                    }

                }

                

                $total_inline_clicks+=$inline_clicks;

                $total_cost_per_inline_click+=$cost_per_inline_clicks;



                $returnRow['campaigns'][] = array(

                    "campaign_id" => $row['id'],

                    "campaign_name" => $row['name'],

                    "campaign_created_time" => $row['created_time'],

                    "campaign_effective_status" => $status, //$row['effective_status'],

                    "campaign_cost_per_unique_click" => $row['insights']['data'][0]['cost_per_unique_click'],

                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,

                    "campaign_impressions" => $row['insights']['data'][0]['impressions'],

                    "campaign_inline_link_clicks" => $inline_clicks,

                    "campaign_reach" => $row['insights']['data'][0]['reach'],

                    "campaign_unique_clicks" => $row['insights']['data'][0]['unique_clicks'],

                    "campaign_unique_ctr" => $row['insights']['data'][0]['ctr'],

                    "campaign_spent" => $row['insights']['data'][0]['spend'],

                    "campaign_actions" => $conversion,

                    "campaign_cost_per_total_action" => $cost_per_conversion,

                    "campaign_reach" => $row['insights']['data'][0]['reach'],

                    "campaign_objective" => $row['objective'],

                    "inline_post_engagement" => $inline_post_engagement1,

                    "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement1,

                    "page_like" => $like1,

                    "cost_per_like1" => $cost_per_like1,

                    "leadgen" => $leadgen,

                    "cost_leadgen" => $cost_leadgen

                    );



                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];

                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];

                $impressions += $row['insights']['data'][0]['impressions'];

                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];

                $reach += $row['insights']['data'][0]['reach'];

                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];

                $unique_ctr += $row['insights']['data'][0]['ctr'];

                $spent += $row['insights']['data'][0]['spend'];

                $actions += $conversion;

                $cost_per_total_action += $cost_per_conversion;

                $inline_post_engagement += $inline_post_engagement1;

                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;



                $page_like += $like1;

                $cost_per_like += $cost_per_like1;

                $cost_leadgen1 += $cost_leadgen;

                $leadgen1 += $leadgen;



                //Reset   

                $inline_clicks = '--';

                $cost_per_inline_clicks = '--';

            } 

            else {



                $returnRow['campaigns'][] = array(

                    "campaign_id" => $row['id'],

                    "campaign_name" => $row['name'],

                    "campaign_created_time" => $row['created_time'],

                    "campaign_effective_status" => $status, //$row['effective_status'],

                    "campaign_cost_per_unique_click" => 0,

                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,

                    "campaign_impressions" => 0,

                    "campaign_inline_link_clicks" => $inline_clicks,

                    "campaign_reach" => 0,

                    "campaign_unique_clicks" => 0,

                    "campaign_unique_ctr" => 0,

                    "campaign_spent" => 0,

                    "campaign_actions" => 0,

                    "campaign_cost_per_total_action" => 0,

                    "campaign_reach" => 0,

                    "inline_post_engagement" => 0,

                    "cost_per_inline_post_engagement" => 0,

                    "page_like" => 0,

                    "cost_per_like1" => 0,

                    "campaign_objective" => $row['objective'],

                    "leadgen" => 0,

                    "cost_leadgen" => 0

                    );



                $cost_per_unique_click += 0;

                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];

                $impressions += 0;

                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];

                $reach += 0;

                $unique_clicks += 0;

                $unique_ctr += 0;

                $spent += 0;

                $actions += 0;

                $cost_per_total_action += 0;

                $inline_post_engagement += 0;

                $cost_per_inline_post_engagement += 0;



                $page_like += 0;

                $cost_per_like += 0;

                $cost_leadgen1 += 0;

                $leadgen1 += 0;

                

                //Reset   

                $inline_clicks = '--';

                $cost_per_inline_clicks = '--';

            }

        }

        if ($unique_ctr > 0) {

            $unique_ctr = ($unique_ctr / $total);

        }

        if ($total_cost_per_inline_click > 0) {

            $total_cost_per_inline_click = ($total_cost_per_inline_click / $total);

        }

        

        if ($cost_per_inline_post_engagement > 0) {

            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);

        }

        if ($cost_leadgen1 > 0) {

            $cost_leadgen1 = ($cost_leadgen1 / $total3);

        }

        if ($cost_per_like > 0) {

            $cost_per_like = ($cost_per_like / $total2);

        }

        //echo $actions;

        if ($cost_per_total_action > 0) {

            $cost_per_total_action = ($spent / $actions);

        }

        $returnRow['campaigns'] = $this->sksort($returnRow['campaigns'], "campaign_effective_status");

        $returnRow['campaign_header'] = array(

            "campaign_active" => $activeCampaigns,

            "campaign_inactive" => $inActiveCampaigns,

            "campaign_pending" => $pendingCampaigns,

            "campaign_disapproved" => $disapprovedCampaigns,

            "cost_per_unique_click" => $cost_per_unique_click,

            "cost_per_inline_link_click" => $total_cost_per_inline_click,

            "impressions" => $impressions,

            "inline_link_clicks" => $total_inline_clicks,

            "reach" => $reach,

            "unique_clicks" => $unique_clicks,

            "unique_ctr" => $unique_ctr,

            "spent" => $spent,

            "actions" => $actions,

            "actions" => $actions,

            "cost_per_total_action" => $cost_per_total_action,

            "inline_post_engagement" => $inline_post_engagement,

            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,

            "page_like" => $page_like,

            "cost_per_like" => $cost_per_like,

            "leadgen" => $leadgen1,

            "cost_leadgen" => $cost_leadgen1,

            );



        #echo "<PRE>";print_R($returnRow);exit;

        return $returnRow;

    }



    function getPrintResult($array) {

        echo "<pre>";

        print_r($array);

        echo "</pre>";

        exit;

    }



    function getCurlCampaigns($limit) {



        $adAccountId = "";

        $date_preset = "";

        $date_preset1 = "";

        $resultArray = array();

        $adAccountId = "act_" . $this->getAddAccountId();

        $campaignFields = $this->getCampaignFields(1);

        $dataArray = "";

        if (!empty($limit)) {

            $date_preset = $this->getCampaignParams($limit);



            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights$date_preset$date_preset$campaignFields$date_preset1" . "&limit=250&access_token=" . $this->access_token;

        } else {

            $date_preset = $this->getCampaignParams("last_7_days");



            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights$date_preset$campaignFields$date_preset1" . "&limit=250&access_token=" . $this->access_token;

        }

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights{clicks,impressions,reach,actions,date_start,campaign_name,ctr,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&time_range={'since':'".$this->sDate."','until':'".$this->eDate."'}&limit=250&access_token=" . $this->access_token;

        try {

            //  echo $url;

            // exit;

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            //$result = json_decode($result);

            // print_r($response);

            $error = $response['error']['message']; //($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;

            // $this->getPrintResult($response);

            if (isset($response['data'])) {

                $campaignDataArray = $this->getFinalResult($response['data']);

                if ($campaignDataArray) {

                    $resultArray = array(

                        "count" => 1,

                        "response" => $campaignDataArray,

                        "error" => "",

                        "success" => ""

                        );

                } else {

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $error,

                        "success" => ""

                        );

                }

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                    );

            }

        } catch (Exception $ex) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        //$this->getPrintResult($resultArray);

        return $resultArray;

    }



    function getCurlCampaignsDateRange($limit) {



        $cost_per_unique_click = 0;

        $total_cost_per_inline_click = 0;

        $impressions = 0;

        $total_inline_clicks = 0;

        $reach = 0;

        $unique_clicks = 0;

        $unique_ctr = 0;

        $spent = 0;

        $actions = 0;

        $cost_per_total_action = 0;

        $cost_per_conversion = 0;

        $inline_clicks = 0;

        $cost_per_inline_clicks = 0;

        $total = 0;

        $conversion = 0;



        $returnRow = array();

        $adAccountId = "";



        $resultArray = array();

        $adAccountId = "act_" . $this->getAddAccountId();

        $campaignFields = $this->getCampaignFields(1);

        $dataArray = explode("#", $limit);

        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?time_range={'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'}&limit=250&access_token=" . $this->access_token;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights.time_range({'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'})$campaignFields" . "&limit=250&access_token=" . $this->access_token;

        echo $url;exit;

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            $error = $response['error']['message'];

            // $this->getPrintResult($response);

            if (isset($response['data'])) {

                $campaignDataArray = $this->getFinalResult($response['data']);

                if ($campaignDataArray) {

                    $resultArray = array(

                        "count" => 1,

                        "response" => $campaignDataArray,

                        "error" => "",

                        "success" => ""

                        );

                } else {

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $error,

                        "success" => ""

                        );

                }

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                    );

            }

        } catch (Exception $ex) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        //$this->getPrintResult($resultArray);

        return $resultArray;

    }

    

    function getCurlCampaignsDateRange_new($limit) {

        $resultArray = array();

        $adAccountId = "act_" . $this->getAddAccountId();

        $campaignFields = $this->getCampaignFields(1);

        $dataArray = explode("#", $limit);

        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=$campaignFields" . "&limit=250&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&access_token=" . $this->access_token;

        //OLD $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights.time_range({'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'})$campaignFields" . "&limit=250&access_token=" . $this->access_token;

        #echo $url;

        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights{clicks,impressions,reach,actions,date_start,campaign_name,ctr,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250&access_token=" . $this->access_token;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights.date_preset(".$limit."){clicks,impressions,reach,actions,date_start,campaign_name,ctr,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&limit=250&access_token=" . $this->access_token;

        #echo "<br>".$url;exit;

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            $error = $response['error']['message'];

            // $this->getPrintResult($response);

            /*if (isset($response['data'])) {

                $finalArr = array();

                foreach ($response['data'] as $campaignData){

                    $campaignId = $campaignData['id'];

                    $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?fields=clicks,impressions,reach,actions,date_start,campaign_name,ctr,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250&access_token=" . $this->access_token;

                    $ch = curl_init($url);

                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                    $result = curl_exec($ch);

                    curl_close($ch);

                    $response1 = json_decode($result, true);

                    $campaignData['insights']['data'] = $response1['data'];

                    $campaignData['insights']['paging'] = $response1['paging'];

                    array_push($finalArr, $campaignData);

                }

                

                $campaignDataArray = $this->getFinalResult($finalArr);

                #echo "<PRE>";print_R($campaignDataArray);exit;

                if ($campaignDataArray) {

                    $resultArray = array(

                        "count" => 1,

                        "response" => $campaignDataArray,

                        "error" => "",

                        "success" => ""

                    );

                } else {

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $error,

                        "success" => ""

                    );

                }

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                );

            }*/

            

            if (isset($response['data'])) {

                $campaignDataArray = $this->getFinalResult($response['data']);

                if ($campaignDataArray) {

                    $resultArray = array(

                        "count" => 1,

                        "response" => $campaignDataArray,

                        "error" => "",

                        "success" => ""

                        );

                } else {

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $error,

                        "success" => ""

                        );

                }

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                    );

            }

        } catch (Exception $ex) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        //$this->getPrintResult($resultArray);

        return $resultArray;

    }
	function sksort(&$array, $subkey = "id", $subkey2 = null, $sort_ascending = true) {

    if (count($array))

        $temp_array[key($array)] = array_shift($array);

    foreach ($array as $key => $val) {

        $offset = 0;

        $found = false;

        foreach ($temp_array as $tmp_key => $tmp_val) {

            if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {

                $temp_array = array_merge(

                    (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));

                $found = true;

            } elseif (!$found

                and $subkey2 and strtolower($val[$subkey]) == strtolower($tmp_val[$subkey])

                and strtolower($val[$subkey2]) > strtolower($tmp_val[$subkey2])) {

                $temp_array = array_merge(

                    (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));

                $found = true;

            }

            $offset++;

        }

        if (!$found)

            $temp_array = array_merge($temp_array, array($key => $val));

    }

    if ($sort_ascending)

        $array = array_reverse($temp_array);

    else

        $array = $temp_array;



    return $array;

}
        function copyAdsets() {
			
			
				$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/?asyncbatch=['.urlencode('{ "method":"POST", "relative_url":"'.$this->input->post('addSetId').'/copies", "name":"copy_adset_1345","body":"deep_copy=true" }').']&access_token=' . $this->access_token;
			  
                
                #echo $url;exit;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                $result = curl_exec($ch);
				//print_r($result);
                curl_close($ch);
                $response = json_decode($result, true);
                /*echo "<pre>";
                print_r($response);
                
                echo "</pre>";*/

                 if(isset($response['async_sessions'][0]['id'])){
                     $optadsetdata = $response['async_sessions'][0]['id']."||$$".$this->input->post('agerange')."||$$".$this->input->post('gender')."||$$".$this->input->post('adPlacement')."||$$".$this->input->post('adsetbudget');
                     $currentuserid = $this->user_id;
            
                     $this->Campaign_model->UpdateUserOptimizedData($optadsetdata,$currentuserid);
                }
			
			
				return "success";
                /*$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/23842585923380160/copies?name=duplicate_adset_1&deep_copy=true&access_token=' . $this->access_token;
                
                //echo $url;exit;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result, true);
                print_r($response);
                exit;*/

                   
                $postFields = array(
                    'access_token' => $this->access_token,
                );

                //$postFields['targeting'] = "{'age_min': 35, 'age_max': 44}";
                //$postFields['targeting'] = "{'age_min':35,'age_max':44,'geo_locations': {'countries':['IE','IT','JP','MY','NL','NZ','CA','SA','SG','ZA','KR','TR','GR','VE','AR']}}";
                /* $postFields['targeting'] = '{
    "age_max": 44,
    "age_min": 35,
    "flexible_spec": [
      {
        "interests": [
          {
            "id": "6003146343826",
            "name": "Facebook for Business"
          }
        ]
      }
    ],
    "genders": [
      1
    ],
    "geo_locations": {
      "countries": [
        "IE",
        "IT",
        "JP",
        "MY",
        "MX",
        "NL",
        "NZ",
        "CA",
        "SA",
        "SG",
        "ZA",
        "KR",
        "TR",
        "GR",
        "VE",
        "AR",
        "AU",
        "TH",
        "PE",
        "ID",
        "AT",
        "PH",
        "HK",
        "BR",
        "PL",
        "TW",
        "PT",
        "CO",
        "MA",
        "EC",
        "FR",
        "VN",
        "DE"
      ],
      "location_types": [
        "home"
      ]
    },
    "locales": [
      24,
      6
    ],
    "publisher_platforms": [
      "facebook"
    ],
    "facebook_positions": [
      "feed",
      "right_hand_column"
    ],
    "device_platforms": [
      "mobile",
      "desktop"
    ]
  }';
                $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/23842597133640160');
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt_array($ch, array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => $postFields
                    ));

                    $response = curl_exec($ch);
                    curl_close($ch);
                    $result = json_decode($response);
                    print_r($result);
                exit;
                
*/
                //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/23842585923380160/?fields=name,start_time,targeting&access_token=' . $this->access_token;
              /*$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/23842597133640160/?fields=name,start_time,targeting&access_token=' . $this->access_token;
                //echo $url;exit;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result, true);
                echo "<pre>";
                print_r($response);
                $response['targeting']['age_max'] = '1';
                $response['targeting']['age_min'] = '2';
                print_r($response);
                print_r(json_encode($response));
                echo "</pre>";
                exit;*/
				
				
				//,campaign_id=23842529156750160"
			    

                

                /*$response['async_sessions'][0]['id'] = '144222369501686';
                if(isset($response['async_sessions'][0]['id'])){
                    $ch2 = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'?id='.$response['async_sessions'][0]['id'].'&pretty=true&fields=result&access_token=' . $this->access_token);
                    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt_array($ch2, array(
                        CURLOPT_RETURNTRANSFER => true
                    ));

                    $response = curl_exec($ch2);
                    curl_close($ch2);
                    $result = json_decode($response);
                    if(isset(json_decode($result->result)->copied_adset_id)){
                        echo json_decode($result->result)->copied_adset_id; 
                    }
                    else{
                        echo "something wrong";
                    }
                }    
                
                exit;*/
                //echo $response['async_sessions'][0]['id'];
                
                /*  $ch2 = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'?id=1403307186404009&pretty=true&fields=result&access_token=' . $this->access_token);
                    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt_array($ch2, array(
                        CURLOPT_RETURNTRANSFER => true
                    ));

                    $response = curl_exec($ch2);
                    curl_close($ch2);
                    $result = json_decode($response);
                    echo "<pre>";
                    print_r(json_decode($result->result));
                    echo json_decode($result->result)->copied_adset_id;
                    echo "</pre>";
                exit;*/
                
        }

        function generateanalysispdf(){

            $returnmaindata = '';
        //$dataArray = explode("#", $limit);
        //$addSetFields = $this->getAdsFields(1);
        $addSetId = $this->input->get('addSetId',TRUE);//"23842585923380160";
        $campaignObjectivce = $this->input->get('campaignObjectivce',TRUE); //"CONVERSIONS";


        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/?fields=insights.date_preset(lifetime){inline_link_clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&access_token=" . $this->access_token;
        #echo $url;exit;
        #echo $url;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            #print_r($response);
            #exit;
            if(isset($response['insights'])){
                if($campaignObjectivce == 'CONVERSIONS'){
                    $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
                    foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'offsite_conversion'){
                            $conversionval = $row['value'];
                        }
                    }
                    

                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }

                    
                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />
                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />
                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'offsite_conversion';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";*/
                        //exit;
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
                                if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

                        if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;


                        $returnmaindata .= '<tr>
                        <td>13 - 17';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>18 - 24';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>25 - 34';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>35 - 44';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>45 - 54';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>55 - 65';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>65+';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'offsite_conversion';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td>Men';
                        $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Women';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Other';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'offsite_conversion';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }

                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td>Desktop Newsfeed';
                            $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Mobile Newsfeed';
                            $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Instagram Feed';
                            $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Audience Network';
                            $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Desktop Right Side';
                            $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'offsite_conversion';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;

                }
                elseif($campaignObjectivce == 'LINK_CLICKS' || $campaignObjectivce == 'PRODUCT_CATALOG_SALES'){
                    $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
                    /*foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'offsite_conversion'){
                            $conversionval = $row['value'];
                        }
                    }*/
                    $conversionval = $response['insights']['data'][0]['inline_link_clicks'];
                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />
                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />
                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'inline_link_clicks';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";
                        exit;*/
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
                                if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

                        if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                        $returnmaindata .= '<tr>
                        <td>13 - 17';
                            $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>18 - 24';
                            $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>25 - 34';
                            $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>35 - 44';
                            $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>45 - 54';
                           $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>55 - 65';
                          $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>65+';
                            $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'inline_link_clicks';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td>Men';
                            $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Women';
                           $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Other';
                           $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'inline_link_clicks';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }
                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td>Desktop Newsfeed';
                            $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Mobile Newsfeed';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Instagram Feed';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Audience Network';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Desktop Right Side';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'inline_link_clicks';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;

                }
                elseif($campaignObjectivce == 'POST_ENGAGEMENT'){
                    $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
                    foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'page_engagement'){
                            $conversionval = $row['value'];
                        }
                    }
                   // $conversionval = $response['insights']['data'][0]['inline_link_clicks'];
                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />
                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />
                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'page_engagement';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";*/
                        //exit;
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
                                if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

                        if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                        $returnmaindata .= '<tr>
                        <td>13 - 17';
                            $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>18 - 24';
                            $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>25 - 34';
                           $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>35 - 44';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>45 - 54';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>55 - 65';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>65+';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'page_engagement';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td>Men';
                           $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Women';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Other';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'page_engagement';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }
                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td>Desktop Newsfeed';
                        $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Mobile Newsfeed';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Instagram Feed';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Audience Network';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Desktop Right Side';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'page_engagement';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;

                }
                elseif($campaignObjectivce == 'PAGE_LIKES'){
                    $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
                    foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'like'){
                            $conversionval = $row['value'];
                        }
                    }
                   // $conversionval = $response['insights']['data'][0]['inline_link_clicks'];
                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />
                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />
                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'like';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";*/
                        //exit;
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
                                if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

                        if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                        $returnmaindata .= '<tr>
                        <td>13 - 17';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>18 - 24';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>25 - 34';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>35 - 44';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>45 - 54';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>55 - 65';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>65+';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'like';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td>Men';
                        $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Women';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Other';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'like';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }
                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td>Desktop Newsfeed';
                        $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Mobile Newsfeed';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Instagram Feed';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Audience Network';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Desktop Right Side';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'like';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;

                }
                elseif($campaignObjectivce == 'LEAD_GENERATION'){
                        $conversionval = 0;
                    $costperconversion = 0;
                    $resultrate = 0;
                    foreach($response['insights']['data'][0]['actions'] as $row){
                        if($row['action_type'] == 'leadgen.other'){
                            $conversionval = $row['value'];
                        }
                    }
                    

                    if($response['insights']['data'][0]['spend']>0){
                        $costperconversion1 = 0;
                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;
                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');
                    }
                    if($response['insights']['data'][0]['impressions']>0){
                        $resultrate1 = 0;
                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;
                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";


                    }
                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />
                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>
                            <br class="hidden-lg">Total Results</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />
                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>
                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>
                            Best Performing</td>';


                     // agegroup started
                     
                        $female = 0;
                        $male = 0;
                        $other = 0;
                        $total = 0;
                        
                        $date_preset = "";
                        $dataArray = "";
                        
                        $date_preset = "date_preset=lifetime&";
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
                        
                        #echo $url;exit;
                        $res = array();
                        $resultArray = array();
                        $impressionarray = array();
                        $spendarray = array();
                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
                        if($pageName == 'adddsReport'){
                            $pageName = 'impressions';
                        }*/
                        $pageName = 'leadgen.other';
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $response = json_decode($result, true);
                        /*echo "<pre>";
                        print_r($response);
                        echo "</pre>";*/
                        //exit;
                        if (isset($response['data'])) {
                            foreach ($response['data'] as $ages) {
                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){
                                    foreach ($ages['actions'] as $feeds1) {
                                        if ($feeds1['action_type'] == $pageName) {
                                            $res[] = array(
                                                "range" => $ages['age'],
                                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                                "impressions" => $feeds1['value'],
                                                "gender" => $ages['gender'],
                                                "original"
                                            );
                                            $total += $ages['inline_link_clicks'];
                                        }
                                    }
                                }
                                else{
                                    $res[] = array(
                                        "range" => $ages['age'],
                                        "inline_link_clicks" => $ages['inline_link_clicks'],
                                        "impressions" => $ages[$pageName],
                                        "gender" => $ages['gender']
                                    );
                                    $total += $ages['inline_link_clicks'];
                                }
                                if($ages['age'] == '13-17'){
                                    $impressionarray['13-17i'] += $ages['impressions'];
                                     $spendarray['13-17s'] += $ages['spend'];
                                }
                                if($ages['age'] == '18-24'){
                                    $impressionarray['18-24i'] += $ages['impressions'];
                                     $spendarray['18-24s'] += $ages['spend'];
                                }
                                if($ages['age'] == '25-34'){
                                    $impressionarray['25-34i'] += $ages['impressions'];
                                     $spendarray['25-34s'] += $ages['spend'];
                                }
                                if($ages['age'] == '35-44'){
                                    $impressionarray['35-44i'] += $ages['impressions'];
                                     $spendarray['35-44s'] += $ages['spend'];
                                }
                                if($ages['age'] == '45-54'){
                                    $impressionarray['45-54i'] += $ages['impressions'];
                                     $spendarray['45-54s'] += $ages['spend'];
                                }
                                if($ages['age'] == '55-64'){
                                    $impressionarray['55-64i'] += $ages['impressions'];
                                     $spendarray['55-64s'] += $ages['spend'];
                                }
                                if($ages['age'] == '65+'){
                                    $impressionarray['65+i'] += $ages['impressions'];
                                     $spendarray['65+s'] += $ages['spend'];
                                }

                            }
                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;
                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;
                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;
                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;
                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;
                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;
                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;
                            /*echo "<pre>";
                        print_r($res);
                        echo "</pre>";*/
                        
                            foreach ($res as $rs) {
                                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                                if (array_key_exists($rs['range'], $resultArray)) {
                                    $resultArray[$rs['range']] = $resultArray1;
                                } else {
                                    $resultArray[$rs['range']] = array();
                                    $resultArray[$rs['range']] = $resultArray1;
                                }
                            }
                        }
                        /*echo "<pre>";
                        print_r($resultArray);
                        echo "</pre>";
                        exit;*/
                        $tempArr = array();
                        foreach ($resultArray as $k => $value) {
                            $tempStr1 = '0';
                            foreach ($value as $k1 => $value1) {
                                $tempStr1 += $value1['impressions'];
                            }
                            $tempArr[$k] = $tempStr1;
                        }

                        if (!array_key_exists('13-17', $resultArray)) {
                            $tempArr['13-17'] = 0;
                        }
                        if (!array_key_exists('18-24', $resultArray)) {
                            $tempArr['18-24'] = 0;
                        }
                        if (!array_key_exists('25-34', $resultArray)) {
                            $tempArr['25-34'] = 0;
                        }
                        if (!array_key_exists('35-44', $resultArray)) {
                            $tempArr['35-44'] = 0;
                        }
                        if (!array_key_exists('45-54', $resultArray)) {
                            $tempArr['45-54'] = 0;
                        }
                        if (!array_key_exists('55-64', $resultArray)) {
                            $tempArr['55-64'] = 0;
                        }
                        if (!array_key_exists('65+', $resultArray)) {
                            $tempArr['65+'] = 0;
                        }

                        /*print_r($impressionarray);
                        print_r($spendarray);
                        exit;*/
                        $returnmaindata .= '||||||';
                        $iag=1;

                        $returnmaindata .= '<tr>
                        <td>13 - 17';
                            $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['13-17']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>18 - 24';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['18-24']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>25 - 34';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['25-34']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>35 - 44';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['35-44']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>45 - 54';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['45-54']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>55 - 65';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['55-64']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>65+';
                        $returnmaindata .= '</td>
                        <td>'.number_format($tempArr['65+']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>
                    </tr>';
                         /*foreach ($tempArr as $k => $value) {
                            $agegroup_total += $value;
                            if()
                            $returnmaindata .= '<tr>
                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>'.number_format($value).'</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>';
                            
                            $iag++;
                        }*/



                        // Gender started
                        
                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                            
                            $date_preset = "";
                            $dataArray = "";
                            
                            $genimpressionarray = array();
                            $genspendarray = array();

                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
                            

                           $pageName = 'leadgen.other';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            
                            /*echo "<pre>";
                            print_r($response);
                            echo "</pre>";
                            exit;*/

                            if (isset($response['data'])) {
                                foreach ($response['data'] as $genders) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){
                                        foreach ($genders['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($genders['gender'] == "female") {
                                                    $female = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "male") {
                                                    $male = $feeds1['value'];
                                                    $total+=$feeds1['value'];
                                                } else if ($genders['gender'] == "unknown") {
                                                    $other = $feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($genders['gender'] == "female") {
                                            $female = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "male") {
                                            $male = $genders[$pageName];
                                            $total+=$genders[$pageName];
                                        } else if ($genders['gender'] == "unknown") {
                                            $other = $genders[$pageName];
                                        }
                                    }

                                    if($genders['gender'] == 'male'){
                                        $genimpressionarray['malei'] += $genders['impressions'];
                                         $genspendarray['males'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'female'){
                                        $genimpressionarray['femalei'] += $genders['impressions'];
                                         $genspendarray['females'] += $genders['spend'];
                                    }
                                    if($genders['gender'] == 'unknown'){
                                        $genimpressionarray['unknowni'] += $genders['impressions'];
                                         $genspendarray['unknowns'] += $genders['spend'];
                                    }
                                }
                            }
                            if(empty($genspendarray['males'])) $genspendarray['males']=0;
                            if(empty($genspendarray['females'])) $genspendarray['females']=0;
                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;
                            $returnmaindata .= '||||||';
                    $returnmaindata .= '<tr>
                        <td>Men';
                        $returnmaindata .= '</td>
                        <td>'.$male.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Women';
                        $returnmaindata .= '</td>
                        <td>'.$female.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Other';
                        $returnmaindata .= '</td>
                        <td>'.$other.'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>
                    </tr>';


                            //adplacement started
                            $desktop = 0;
                            $mobile = 0;
                            $other = 0;
                            $total = 0;
                            $dataArray = 0;
                            
                            $date_preset = "";

                            $placeimpressionarray = array();
                            $placespendarray = array();

                            
                            $date_preset = "date_preset=lifetime&";
                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
                            
                            $res = array();
                            $resultArray = array();

                            $pageName = 'leadgen.other';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
                            //$this->getPrintResult($response);
            /*                 echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $feeds) {
                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){
                                        foreach ($feeds['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $desktop = $feeds1['value'];
                                                    $total += $feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                                    $mobile = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "instagram") {
                                                    $instant_article = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['publisher_platform'] == "audience_network") {
                                                    $mobile_external_only = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                                    $right_hand = $feeds1['value'];
                                                    $total +=$feeds1['value'];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $desktop = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $mobile = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $instant_article = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $mobile_external_only = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $right_hand = $feeds[$pageName];
                                            $total +=$feeds[$pageName];
                                        }
                                    }
                                    
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];
                                             $placespendarray['desktops'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];
                                             $placespendarray['mobiles'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "instagram") {
                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];
                                             $placespendarray['instagrams'] += $feeds['spend'];
                                        }
                                        if ($feeds['publisher_platform'] == "audience_network") {
                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];
                                             $placespendarray['audnetworks'] += $feeds['spend'];
                                        }
                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];
                                             $placespendarray['rhss'] += $feeds['spend'];
                                        }

                                }

                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;
                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;
                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;
                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;
                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;
                                if ($desktop > 0) {
                                    $desktopPer = round(($desktop / $total) * 100);
                                }
                                if ($mobile > 0) {
                                    $mobilePer = round(($mobile / $total) * 100);
                                }
                                if ($instant_article > 0) {
                                    $instant_articlePer = round(($instant_article / $total) * 100);
                                }
                                if ($mobile_external_only > 0) {
                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
                                }
                                if ($right_hand > 0) {
                                    $right_handPer = round(($right_hand / $total) * 100);
                                }
                            }
                           /* echo "<pre>";
            print_r($placeimpressionarray);
            print_r($placespendarray);
            echo "</pre>";
            exit;*/
                            $returnmaindata .= '||||||';
                            $returnmaindata .= '<tr>
                        <td>Desktop Newsfeed';
                        $returnmaindata .= '</td>
                        <td>'.number_format($desktop).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Mobile Newsfeed';
                        $returnmaindata .= '
                        </td>
                        <td>'.number_format($mobile).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Instagram Feed';
                        $returnmaindata .= '</td>
                        <td>'.number_format($instant_article).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Audience Network';
                        $returnmaindata .= '</td>
                        <td>'.number_format($mobile_external_only).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>
                    </tr>
                    <tr>
                        <td>Desktop Right Side';
                        $returnmaindata .= '</td>
                        <td>'.number_format($right_hand).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>
                    </tr>';


                            //country started

                            $female = 0;
                            $male = 0;
                            $other = 0;
                            $total = 0;
                           $tempStr = '';
                            $date_preset = "";
                            $dataArray = "";
                            
                                $date_preset = "date_preset=lifetime&";
                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;
                            

                            $res = array();
                            $resultArray = array();

                            $pageName = 'leadgen.other';

                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $result = curl_exec($ch);
                            curl_close($ch);
                            $response = json_decode($result, true);
/*echo "<pre>";
            print_r($response);
            echo "</pre>";
            exit;*/
                            
                            if (isset($response['data'])) {
                                foreach ($response['data'] as $ages) {
                                    if(empty($ages['spend'])) $ages['spend']=0;
                                    $result = $this->Users_model->getCountryName($ages['country']);

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){
                                        foreach ($ages['actions'] as $feeds1) {
                                            if ($feeds1['action_type'] == $pageName) {
                                                $countrytotal += (int)$feeds1['value'];
                                                $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($feeds1['value']).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                                            }
                                        }
                                    }
                                    else{
                                        $countrytotal += (int)$ages[$pageName];
                                        $tempStr .= ' <tr>
                        <td>'.strtoupper($result[0]->Country).'
                        </td>
                        <td>'.number_format($ages[$pageName]).'</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>
                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>
                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>
                    </tr>';
                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                                    }
                                }
                            }
                            $returnmaindata .= '||||||';
                            $returnmaindata .= $tempStr;
                }
            }
                    
            /*echo "<pre>";
            print_r($response);
            echo "</pre>";*/
            } catch (Exception $ex) {
                echo "error";
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $e->getMessage(),
                        "success" => ""
                    );
                }
                /*print_r($returnmaindata);
                exit;*/

            $this->load->library('Pdf');
            //$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);





            
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Muhammad Junaid Tufail');
$pdf->SetTitle('The Campaign Maker Analysis Report');
$pdf->SetSubject('The Campaign Maker Analysis Report');
$pdf->SetKeywords('AnalysisReport, CampaignMaker');

$pdf->setHeaderData($ln='', $lw=0, $ht='', $hs='<div style="background-color:#212F41;color:#ffffff;text-align:center;font-size:13px;line-height:300%;">Analysis Report for '.$this->input->get("camptitle",TRUE).'</div>', $tc=array(0,0,0), $lc=array(0,0,0));
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);




// test custom bullet points for list


                    $phpvar = explode("|||||",$returnmaindata);

// add a page
$pdf->AddPage();

$html = <<<EOF
<style>
    img{
            font-size:10px;width:15px;height:15px;
        }
		th {
			font-weight:bold;
			text-align:center;
		}
		td{
			text-align:center;
		}
		tr {
			border-top: 2px solid #d8e5ee;
		}
		.heading{
				background-color: #fafbfc;
			border-top: 2px solid #d8e5ee;
			color: #8dabc4;
			font-size: 15px;
			height: 42px;
			letter-spacing: 3px;
			margin-bottom: 20px;
			padding: 8px 10px 11px 36px;
			text-transform: uppercase;
		}
</style>    
<div class="analysis-done" id="analysis-done" style="">
            <!--Head-->
            <div class="head-analysis table-responsive">
                <table class="table">
                    <thead>
                    <tr id="adsettotals">$phpvar[0]</tr>
                    </thead>
                </table>
            </div>
            <!--End Head-->

            <!--AGE GROUP ANALYSIS-->
            <div class="heading">AGE GROUP ANALYSIS</div>
			<div></div>
            <table class="table table-hover dataTable no-footer" id="analysistbl1" role="grid" aria-describedby="analysistbl1_info" style="width: 100%;" width="100%">
                    <thead>
                    <tr role="row"><th>AGE GROUP</th>
					<th>RESULTS</th>
					<th>COST PER RESULT</th>
					<th>RESULT RATE</th>
					<th>TOTAL SPENT</th>
					</tr>
                    </thead>
                    <tbody id="agegrouptotals">$phpvar[1]</tbody>
                </table>
            
            

            <!--GENDER ANALYSIS-->
			<div></div>
			<div class="heading">GENDER ANALYSIS</div>
			<div></div>
            
            <table class="table table-hover dataTable no-footer" id="analysistbl2" role="grid" aria-describedby="analysistbl2_info" style="width: 100%;" width="100%">
                    <thead>
                    <tr role="row">
					<th>GENDER</th>
					<th>RESULTS</th>
					<th>COST PER RESULT</th>
					<th>RESULT RATE</th>
					<th>TOTAL SPENT</th>
					</tr>
                    </thead>
                    <tbody id="gendertotals">$phpvar[2]</tbody>
                </table>
            <!--End GENDER ANALYSIS-->

            <!--AD PLACEMENT ANALYSIS-->
			<div></div>
            <div class="heading">AD PLACEMENT ANALYSIS</div>
			<div></div>
            <table class="table table-hover dataTable no-footer" id="analysistbl3" role="grid" aria-describedby="analysistbl3_info" style="width: 100%;" width="100%">
                    <thead>
                    <tr role="row">
					<th>PLACEMENT</th>
					<th>RESULTS</th>
					<th>COST PER RESULT</th>
					<th>RESULT RATE</th>
					<th>TOTAL SPENT</th></tr>
                    </thead>
                    <tbody id="adplacementtotals">$phpvar[3]</tbody>
                </table>
				
            <!--End AD PLACEMENT ANALYSIS-->

            <!--LOCATION ANALYSIS-->
			<div></div>
            <div class="heading">LOCATION ANALYSIS</div>
			<div></div>
            <table class="table table-hover dataTable no-footer" id="analysistbl4" role="grid" aria-describedby="analysistbl4_info" style="width: 100%;" width="100%">
                    <thead>
                    <tr role="row">
					<th>COUNTRY</th>
					<th>RESULTS</th>
					<th>COST PER RESULT</th>
					<th>RESULT RATE</th>
					<th>TOTAL SPENT</th>
					</tr>
                    </thead>
                    <tbody id="countrytotals">$phpvar[4]</tbody>
                </table>
                <!--End Analysis Buttons-->

            </div>
        </div>
EOF;

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Analysis_TCM.pdf', 'I');
            
            
            /*
            $pdf->SetTitle('Pdf Example');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->Write(5, 'CodeIgniter TCPDF Integration');
            ob_clean();
            $pdf->Output('pdfexample.pdf', 'I');
            */
        }

        function testgenerateanalysispdf(){

            $this->load->library('Pdf');
            //$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Muhammad Junaid Tufail');
$pdf->SetTitle('CampaignMaker Analysis Report');
$pdf->SetSubject('Analysis Report');
$pdf->SetKeywords('AnalysisReport, CampaignMaker');

$pdf->setHeaderData($ln='', $lw=0, $ht='', $hs='<div style="background-color:#212F41;color:#ffffff;text-align:center;font-size:13px;line-height:300%;">Analysis Report</div>', $tc=array(0,0,0), $lc=array(0,0,0));
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);




// test custom bullet points for list



// add a page
$pdf->AddPage();

$html = <<<EOF
<style>
    img{
            font-size:10px;width:15px;height:15px;
        }
        th {
            font-weight:bold;
            text-align:center;
        }
        td{
            text-align:center;
        }
        tr {
            border-top: 2px solid #d8e5ee;
        }
        .heading{
                background-color: #fafbfc;
            border-top: 2px solid #d8e5ee;
            color: #8dabc4;
            font-size: 15px;
            height: 42px;
            letter-spacing: 3px;
            margin-bottom: 20px;
            padding: 8px 10px 11px 36px;
            text-transform: uppercase;
        }
</style>    
<div class="analysis-done" id="analysis-done" style="">
            <!--Head-->
            <div class="head-analysis table-responsive">
                <table class="table">
                    <thead>
                    <tr id="adsettotals"><td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/total-result.png">
                            <span class="text-primary" style="font-size:10px;">13</span>
                            <br class="hidden-lg">Total Results</td>
                        <td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/employee.png">
                            <span class="text-primary" style="font-size:10px;">$3.54</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/percentage.png">
                            <span class="text-primary" style="font-size:10px;">0.13%</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/money-bag.png">
                            <span class="text-primary" style="font-size:10px;">$46.01</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/best.png">
                            Best Performing</td></tr>
                    </thead>
                </table>
            </div>
            <!--End Head-->

            <!--AGE GROUP ANALYSIS-->
            <div class="heading">AGE GROUP ANALYSIS</div>
            <div></div>
            <table class="table table-hover dataTable no-footer" id="analysistbl1" role="grid" aria-describedby="analysistbl1_info" style="width: 100%;" width="100%">
                    <thead>
                    <tr role="row"><th>AGE GROUP</th>
                    <th>RESULTS</th>
                    <th>COST PER RESULT</th>
                    <th>RESULT RATE</th>
                    <th>TOTAL SPENT</th>
                    </tr>
                    </thead>
                    <tbody id="agegrouptotals">
                    
                    
                    
                    
                    
                    <tr role="row" class="odd">
                        <td class="sorting_1">
                            13 - 17</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td class="text-success">0 %</td>
                        <td>$0.00</td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">
                            18 - 24</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td class="text-success">0 %</td>
                        <td>$3.54</td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">
                            25 - 34</td>
                        <td>7</td>
                        <td>$3.70</td>
                        <td class="text-success">0.11 %</td>
                        <td>$25.91</td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">
                            35 - 44</td>
                        <td>2</td>
                        <td>$5.40</td>
                        <td class="text-success">0.11 %</td>
                        <td>$10.80</td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">
                            
                            45 - 54</td>
                        <td>2</td>
                        <td>$1.92</td>
                        <td class="text-success">0.42 %</td>
                        <td>$3.84</td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">
                            55 - 65</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td class="text-success">0 %</td>
                        <td>$0.42</td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">
                            65+<input id="bestagerange" name="bestagerange" value="65+" type="hidden"></td>
                        <td>2</td>
                        <td>$0.75</td>
                        <td class="text-success">1.75 %</td>
                        <td>$1.50</td>
                    </tr></tbody>
                </table>
            
            

            <!--GENDER ANALYSIS-->
            <div></div>
            <div class="heading">GENDER ANALYSIS</div>
            <div></div>
            
            <table class="table table-hover dataTable no-footer" id="analysistbl2" role="grid" aria-describedby="analysistbl2_info" style="width: 100%;" width="100%">
                    <thead>
                    <tr role="row">
                    <th>GENDER</th>
                    <th>RESULTS</th>
                    <th>COST PER RESULT</th>
                    <th>RESULT RATE</th>
                    <th>TOTAL SPENT</th>
                    </tr>
                    </thead>
                    <tbody id="gendertotals">
                    
                    <tr role="row" class="odd">
                        <td class="sorting_1">Men</td>
                        <td>13</td>
                        <td>$3.42</td>
                        <td class="text-success">0.14 %</td>
                        <td>$44.40</td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">Women</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td class="text-success">0 %</td>
                        <td>$1.28</td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">Other</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td class="text-success">0 %</td>
                        <td>$0.33</td>
                    </tr></tbody>
                </table>
            <!--End GENDER ANALYSIS-->

            <!--AD PLACEMENT ANALYSIS-->
            <div></div>
            <div class="heading">AD PLACEMENT ANALYSIS</div>
            <div></div>
            <table class="table table-hover dataTable no-footer" id="analysistbl3" role="grid" aria-describedby="analysistbl3_info" style="width: 100%;" width="100%">
                    <thead>
                    <tr role="row">
                    <th>PLACEMENT</th>
                    <th>RESULTS</th>
                    <th>COST PER RESULT</th>
                    <th>RESULT RATE</th>
                    <th>TOTAL SPENT</th></tr>
                    </thead>
                    <tbody id="adplacementtotals">
                    
                    
                    
                    <tr role="row" class="odd">
                        <td class="sorting_1">Audience Network</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td class="text-success">0 %</td>
                        <td>$0.23</td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">Desktop Newsfeed</td>
                        <td>10</td>
                        <td>$2.24</td>
                        <td class="text-success">0.38 %</td>
                        <td>$22.37</td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">Desktop Right Side</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td class="text-success">0 %</td>
                        <td>$3.22</td>
                    </tr><tr role="row" class="even">
                        <td class="sorting_1">Instagram Feed</td>
                        <td>0</td>
                        <td>$0.00</td>
                        <td class="text-success">0 %</td>
                        <td>$2.09</td>
                    </tr><tr role="row" class="odd">
                        <td class="sorting_1">Mobile Newsfeed</td>
                        <td>3</td>
                        <td>$5.94</td>
                        <td class="text-success">0.12 %</td>
                        <td>$17.81</td>
                    </tr></tbody>
                </table>
                
            <!--End AD PLACEMENT ANALYSIS-->

            <!--LOCATION ANALYSIS-->
            <div></div>
            <div class="heading">LOCATION ANALYSIS</div>
            <div></div>
            <table class="table table-hover dataTable no-footer" id="analysistbl4" role="grid" aria-describedby="analysistbl4_info" style="width: 100%;" width="100%">
                    <thead>
                    <tr role="row">
                    <th>COUNTRY</th>
                    <th>RESULTS</th>
                    <th>COST PER RESULT</th>
                    <th>RESULT RATE</th>
                    <th>TOTAL SPENT</th>
                    </tr>
                    </thead>
                    <tbody id="countrytotals"><tr role="row" class="odd">
                        <td class="sorting_1">PHILIPPINES</td>
                        <td>3</td>
                        <td>$1.36</td>
                        <td class="text-success">0.32 %</td>
                        <td>$4.09</td>
                    </tr></tbody>
                </table>
                <!--End Analysis Buttons-->

            </div>
        </div>
EOF;

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Analysis_TCM.pdf', 'I');
            
            
            /*
            $pdf->SetTitle('Pdf Example');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('Author');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->Write(5, 'CodeIgniter TCPDF Integration');
            ob_clean();
            $pdf->Output('pdfexample.pdf', 'I');
            */
        }
		
		function testbatchrequest(){
		    
		    $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/23842601197720160/ads?fields=id,name&access_token=" . $this->access_token;
        #echo $url;exit;
        

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
		    echo "<pre>";
				print_r($response);
				echo "</pre>";
                /*$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/?batch=['.urlencode('{ "method":"GET", "relative_url":"me"},{ "method":"GET", "relative_url":"act_104413260013985?fields=insights.date_preset(lifetime){frequency,date_start}"}').']&access_token=' . $this->access_token;
	
                
                #echo $url;exit;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_POST, 1);
                $result = curl_exec($ch);
				//print_r($result);
                curl_close($ch);
                $response = json_decode($result, true);
				echo "<pre>";
				print_r($response);
				echo "</pre>";*/

                /* $url3 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version').'/23842604154690160/?fields=name,end_time,daily_budget,targeting&access_token=' . $this->access_token;
                        //echo $url;exit;
                        $ch3 = curl_init($url3);
                        curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch3, CURLOPT_FOLLOWLOCATION, true);
                        $result3 = curl_exec($ch3);
                        curl_close($ch3);
                        $response3 = json_decode($result3, true);
                        echo "<pre>";
                    print_r($response3);
                    echo "</pre>";

*/
                    /*$postFields = array(
                    'access_token' => $this->access_token,
                );
                    $finalbudget = 2000/100;    
                    $postFields['daily_budget'] = $finalbudget/0.01;
                            
                        echo "<pre>";
                    print_r($postFields);
                    echo "</pre>";
                       
                        $ch = curl_init('https://graph.facebook.com/v2.9/23842604115680160');
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt_array($ch, array(
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $postFields
                        ));

                        $response = curl_exec($ch);
                        curl_close($ch);
                        $result = json_decode($response);
                        
                        echo "<pre>";
                    print_r($result);
                    echo "</pre>";*/
		}   
}
//End of class