<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Welcome extends CI_Controller {

    public function __construct() {
		 error_reporting(1);
       		parent::__construct();
		$this->load->model('Users_model');
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}

    }

    public function index() {

        $data = new stdClass();
		$this->load->view('header');
		$this->load->view('topbar');
        $this->load->view('welcome');
		   $this->load->view('footer');
		//loadView('welcomes', $data);
    }
	
	}


//End of class
