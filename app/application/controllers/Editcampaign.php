<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\TargetingSearch;
use FacebookAds\Object\Search\TargetingSearchTypes;
use FacebookAds\Object\TargetingSpecs;
use FacebookAds\Object\Fields\TargetingSpecsFields;
use FacebookAds\Object\AdImage;
use FacebookAds\Object\Fields\AdImageFields;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdPreviewFields;
use FacebookAds\Object\Values\AdFormats;
use FacebookAds\Object\ObjectStorySpec;
use FacebookAds\Object\Fields\ObjectStory\AttachmentDataFields;
use FacebookAds\Object\ObjectStory\AttachmentData;
use FacebookAds\Object\Fields\ObjectStory\LinkDataFields;
use FacebookAds\Object\Fields\ObjectStorySpecFields;
use FacebookAds\Object\ObjectStory\LinkData;
use FacebookAds\Object\Values\CallToActionTypes;
use FacebookAds\Object\Values\OptimizationGoals;
use FacebookAds\Object\Values\PageTypes;
use FacebookAds\Object\Values\BillingEvents;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;

global $data;
$data = new stdClass();

class Editcampaign extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
   

    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        global $fb;
        $this->seo->SetValues('Title', "Edit Campaign");
        $this->seo->SetValues('Description', "Edit Campaign");
       if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}
        /*
         * 
         * Facebook Configuration-------------------- 
         * 
         */
        $fb = new Facebook([
            'app_id' => $this->config->item('facebook_app_id'),
            'app_secret' => $this->config->item('facebook_app_secret'),
            'default_graph_version' => $this->config->item('facebook_graph_version'),
        ]);
        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
        $this->user_id = $this->session->userdata['logged_in']['id'];

        if (!empty($this->session->userdata['logged_in'])) {
            $app_id = $this->config->item('facebook_app_id');
            $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
            redirect('/');
        }
    }

    public function index($adAccountId = NULL) {

        $limit = "";
        $data = new stdClass();

        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $adAccountId = $this->input->post('adAccountId');
            $campaignId = $this->input->post('campaignId');
            $limit = $this->input->post('limit');

            $data->adAccountData = $this->getCurlCampaigns($campaignId, $adAccountId);
            $data->addAccountId = $adAccountId;
            $data->adaccounts = $data->adAccountData['response'];
            $data->adaccountsCount = $data->adAccountData['count'];
            $data->error = $data->adAccountData['error'];
            $data->success = $data->adAccountData['success'];

            echo $this->load->view('edit_campaign', $data, true);
        } else {
            $this->load->view("edit_campaign");
        }
    }

    function getPrintResult($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }

    function getCurlCampaigns($campaignId, $adAccountId) {

        $CampaignFields = "id,account_id,name,objective,buying_type,created_time,start_time,stop_time,effective_status";

        $resultArray = array();
        //$adAccountId = "act_" . $adAccountId;
        $campaignFields = $CampaignFields;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/?fields=$campaignFields" . "&access_token=" . $this->access_token;

        try {

            // exit;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            // $this->getPrintResult($response);
            $dataArray = $this->getFinalResult($response, $campaignId);
            if ($response) {
                $resultArray = array(
                    "count" => 1,
                    "response" => $dataArray,
                    "error" => "",
                    "success" => ""
                );
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getFinalResult($dataArray, $campaignId) {


        $returnRow = array();
        //$this->getPrintResult($dataArray);
        $campaignName = $dataArray['name'];
        $campaignObjective = $dataArray['objective'];

        $returnRow['campaign'] = array(
            'campaignId' => $campaignId,
            'campaignName' => $campaignName,
            'campaignObjective' => $campaignObjective
        );
        return $returnRow;
    }

    function loadModal() {
        $data = new stdClass();
        $data->campaign = 1;
        //var_dump($data);
        echo $this->load->view('modals_again', $data, true);
        //$this->load->view("modals", $data);
    }

    function saveCampaign() {


        $campaignId = $this->input->post('campaignId');
     
        $campaignName = $this->input->post('campaignName');

        $campaign = new Campaign($campaignId);

        

        try {
            $campaign->update(array(
            CampaignFields::NAME => $campaignName,
        ));
            echo "100";
        } catch (Exception $ex) {

            $ex->getMessage();
            echo "200";
        }
        
    }

}

//end of class
