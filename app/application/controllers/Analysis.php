<?php

if (!defined('BASEPATH'))

    exit('No direct script access allowed');

use FacebookAds\Api;



use FacebookAds\Object\Ad;



use FacebookAds\Object\AdUser;



use Facebook\Facebook;



use Facebook\FacebookApp;



use FacebookAds\Object\AdAccount;



use FacebookAds\Object\Fields\AdAccountFields;



use FacebookAds\Object\Campaign;



use FacebookAds\Object\Fields\CampaignFields;



use FacebookAds\Object\Fields\AdSetFields;



use FacebookAds\Object\Values\AdObjectives;



use FacebookAds\Object\Insights;



use FacebookAds\Object\Fields\InsightsFields;



use FacebookAds\Object\Values\InsightsPresets;



use FacebookAds\Object\Fields\AdFields;



use FacebookAds\Object\Values\InsightsActionBreakdowns;



use FacebookAds\Object\Values\InsightsBreakdowns;



use FacebookAds\Object\Values\InsightsLevels;



use FacebookAds\Object\Values\InsightsIncrements;



use FacebookAds\Object\Values\InsightsOperators;



use Facebook\FacebookRequest;



use Facebook\FacebookResponse;

class Analysis extends CI_Controller {

	public $access_token;



    public $user_id;



    public $add_account_id;



    public $add_title;



    public $is_ajax;



    public $limit;

    public $field;

    public function __construct() {

		 error_reporting(1);

        parent::__construct();

        $this->load->model('Users_model');

		$this->load->model('Campaign_model');

        $this->is_ajax = 0;

        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];

        $this->user_id = $this->session->userdata['logged_in']['id'];

        $this->getAdAccount();
		

        // User subscriptions expire 

       



        // User account spend limit reach

        

        if (isset($this->session->userdata['logged_in']) && !empty($this->session->userdata['logged_in'])) {

            $app_id = $this->config->item('facebook_app_id');

            $facebook_app_secret = $this->config->item('facebook_app_secret');

            Api::init($app_id, $facebook_app_secret, $this->access_token);

        } else {

          

            //echo current_url(); exit;

            $this->session->set_userdata('last_page', current_url());

             header("Location:".$this->config->item('site_url'));die;

        }

        $response = $this->Users_model->get_cancel_subscription($this->session->userdata['logged_in']['email']);
     
		if($response == 'No Subscribe'){

		if($this->session->userdata['logged_in']['accesstoken'] == ''){

				header("Location:".$this->config->item('site_url').'connect-to-facebook/');

		}else{	redirect($this->config->item('site_url').'one-step-closer/'); }

		}elseif($response == 'Cancelled' || $response == 'Expired'){

			//redirect($this->config->item('site_url').'final-payment/');

		}

        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));     

    }
	
	

    public function index($adAccountId) {

		

        //$data = $this->Users_model->Getautomaticoptimization($this->user_id);

		//echo $this->input->get('adaccountid', TRUE);

		$limit = "lifetime";//$this->dateval;//$this->sDate."#".$this->eDate;

		

        $data = new stdClass();
		$data->sub_id = $this->Users_model->get_subscription_id($this->session->userdata['logged_in']['email']);
        $adAccountId = $adAccountId;//$this->input->get('adaccountid', TRUE);//$this->input->post('adAccountId');
		$data->adAccountId =$adAccountId;
        $limit = "lifetime";//$this->input->post('limit');

        $field = "spend";//$this->input->post('field');

        $this->session->set_userdata('dateval', $limit);

		if(empty($adAccountId)){

			$adAccountId = $this->add_account_id;

		}

        if (!empty($adAccountId)) {

            $this->setAddAccountId($adAccountId);

            $this->getCurrency();

            if (strpos($limit, '#') !== false) {

                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);

            } else {

                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);

            }

        } else {

            $data->adAccountData = $this->getAdAccount();

        }

        $data->addAccountId = $this->add_account_id;

        $data->adaccounts = $data->adAccountData['response'];

        $data->adaccountsCount = $data->adAccountData['count'];

        $data->error = $data->adAccountData['error'];

        $data->success = $data->adAccountData['success'];

        $data->limit = $limit;

        $accountArr = $this->Users_model->getUserAddAccountName($adAccountId);

        $data->accountName = $accountArr[0]->add_title;

        

        $data->cur_currency = $this->session->userdata('cur_currency');



        #echo "<PRE>";print_R($data);exit;

        $data->automaticoptimizationdata = $this->Campaign_model->Getautomaticoptimization($this->user_id);

        

		loadView('analysis', $data);

    }

    



    public function getCampaignAsdsets() {

        

        $finaladsethtml = '';

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$this->input->post('campaignid')."/adsets?fields=id,name&limit=1000&access_token=" . $this->access_token;

        #echo $url;exit;

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            if (isset($response['data'])) {

                $finaladsethtml .= '<option value="0">Select Adset</option>';

                foreach ($response['data'] as $row) {

                    $finaladsethtml .= '<option value="'.$row["id"].'">'.$row["name"].'</option>';

                }

            }

            else{

                echo '<option value="0">Select Adset</option>';

                exit;

            }

            /*print_r($response);

            exit;*/

        }

        catch (Exception $ex) {

            echo $e->getMessage();

            exit;

        }

        echo $finaladsethtml;

        exit;    

    }



   public function getCampaignAd() {

        $adset = $this->input->post('adset');

        $finaladsethtml = '';

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$adset."/ads?fields=id,name&limit=1000&access_token=" . $this->access_token;

        #echo $url;exit;

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            if (isset($response['data'])) {

                $finaladsethtml .= '<option value="0">Select Ad</option>';

                foreach ($response['data'] as $row) {

                    $finaladsethtml .= '<option value="'.$row["id"].'">'.$row["name"].'</option>';

                }

            }

            else{

                echo '<option value="0">Select Ad</option>';

                exit;

            }

            /*print_r($response);

            exit;*/

        }

        catch (Exception $ex) {

            echo $e->getMessage();

            exit;

        }

        echo $finaladsethtml;

        exit;    

    }











    function getCurlAdsetlifetime() {

        

        

        $analyze_level = $this->input->post('analyze_level');

        $addSetIdlist =  $this->input->post('addSetId');

        $datapoints =  $this->input->post('datapoints');

        $timeFrame = strtolower($this->input->post('timeFrame'));

        $token = $this->access_token;

        $returnmaindata = '';

        

        $campaignObjectivce = $this->input->post('campaignObjectivce'); //"POST_ENGAGEMENT";

         $maindata = array();

         $spenddata = array();

         

          //echo '<pre>';

          

          

            foreach($addSetIdlist as $adsetkey => $addSetIdtemp){

            

            $compagain = $addSetIdtemp['name'];

            $addSetId = $addSetIdtemp['id'];

            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/?fields=insights.date_preset(lifetime){inline_link_clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&access_token=" . $this->access_token;

            

             $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

          

          if(!isset($response['insights']['data'])){

                unset($addSetIdlist[$adsetkey]);

            }

            }

        foreach($addSetIdlist as $addSetIdtemp){

            

            $compagain = $addSetIdtemp['name'];

            $addSetId = $addSetIdtemp['id'];

            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/?fields=insights.date_preset(lifetime){inline_link_clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&access_token=" . $this->access_token;

            

           

      

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            

             // print_r($response);

              

              

            if(!isset($response['insights']['data'])){

                echo "error";

                exit;

            }

          

            if(isset($response['insights'])){

                

               

                       $date_preset = 'lifetime';

                       $spenddata[$compagain]['lifetime'] = $this->spendtimeFrame($addSetId,$date_preset,$token);

                       

                       

                       $date_preset = 'today';

                       $spenddata[$compagain]['last24_hour'] = $this->spendtimeFrame($addSetId,$date_preset,$token);

                       

                       $date_preset = 'last_3d';

                       $spenddata[$compagain]['past3days'] = $this->spendtimeFrame($addSetId,$date_preset,$token);

                       

                     

                       $date_preset = 'last_30d';

                       $spenddata[$compagain]['past_30days'] = $this->spendtimeFrame($addSetId,$date_preset,$token);

                       

                      

            

            }

            }catch (Exception $ex) {

                echo "error";

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $e->getMessage(),

                        "success" => ""

                    );

                }  

                     

              }  

           

              

              $maindata['spend'] = $spenddata;

              

            //   echo '<pre>';

            //   print_r($maindata['spend']);

             // exit;

                

                    

                        $date_preset = "";

                        $dataArray = "";

                        $date_preset = "date_preset=".$timeFrame."&";

                        

                      

                        $peopleSaw = array();

                        foreach($addSetIdlist as $addSetIdtemp){

            

                        $compagain = $addSetIdtemp['name'];

                        $addSetId = $addSetIdtemp['id'];

                        

                         $peopleSaw[$compagain] = $this->hitstimeFrame($addSetId,$date_preset,$token);

                        

                        }

                        

                       

                       

                        $maindata['peopleSaw'] = $peopleSaw;

                      

                        $date_preset = $timeFrame;

                        $datatocompair = $this->getdatapoint($addSetIdlist,$date_preset,$datapoints,$token);

                       

                       $maindata['datatocompair'] = $datatocompair;

                       echo json_encode($maindata);

                       exit;

               

        }

        

    



    

    public function getCurrency() {



            $adAccountId = "act_" . $this->getAddAccountId();



            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/?fields=name,currency&access_token=" . $this->access_token;







            $ch = curl_init($url);



            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);



            $result = curl_exec($ch);



            curl_close($ch);



            $response = json_decode($result, true);







            $currency = 'USD';



            if (!empty($response['currency'])) {



                $currency = $response['currency'];



            }



        $locale = 'en-US'; //browser or user locale



        $fmt = new NumberFormatter($locale . "@currency=$currency", NumberFormatter::CURRENCY);



        $symbol = $fmt->getSymbol(NumberFormatter::CURRENCY_SYMBOL);



        $this->session->set_userdata('cur_currency', $symbol);



    }    

    protected function getAdAccount() {



        $result = $this->Users_model->getUserAddAccountId($this->user_id);







        if (count($result) > 0) {



            if (count($result) == 1) {



                $finalRes = "";



                $this->setAddAccountId($result[0]->ad_account_id);



                $this->getCurrency();



                $this->add_account_id = $result[0]->ad_account_id;



                $this->add_title = $result[0]->add_title;



                // $limit=date('Y-m-d', strtotime('-7 days'))."#".date('Y-m-d');



                //$finalRes = $this->getCurlCampaignsDateRange($limit);



                //$finalRes = $this->getCurlCampaigns("");



                $finalRes = $this->getCurlCampaignsDateRange_new($this->dateval);



                // $this->getPrintResult($finalRes);



                $resultArray = array(



                    "count" => $finalRes['count'],



                    "response" => $finalRes['response'],



                    "error" => $finalRes['error'],



                    "success" => $finalRes['succes']



                    );



                // End Get Ad  



            } else {



                $resultArray = array(



                    "count" => 2,



                    "response" => $result,



                    "error" => "",



                    "success" => ""



                    );



            }



        } else {



            $resultArray = array(



                "count" => 0,



                "response" => "",



                "error" => "No data available against your account.",



                "success" => ""



                );



        }



        //  $this->getPrintResult($resultArray);



        return $resultArray;



    }







    /* From LIVE */







    protected function fbAdAccount($add_account_id, $selected_limit) {



        try {



            $resultArray = $this->getAllCampaigns($add_account_id, $selected_limit);



        } catch (Exception $e) {



            $resultArray = array(



                "count" => 0,



                "response" => "",



                "error" => $e->getMessage(),



                "success" => ""



                );



        }



        return $resultArray;



    }







    protected function getAllCampaigns($add_account_id, $selected_limit) {



        try {



            $adAccountOjb = new AdAccount($add_account_id);



            $insightsArray = array();



            $campaignDataArray = array();







            $fields = $this->getCampaignFields();



            $insights = $this->getCampaignInsightFields();



            $params = $this->getCampaignParams($selected_limit);







            $response = $adAccountOjb->getCampaigns($fields);







            foreach ($response as $row) {



                try {







                    $campaignObj = new Campaign($row->id);



                    $res = $this->get_hub_insight_per($campaignObj, $insights, $params);



                    foreach ($res as $rs) {



                        //$rs->account_id;



                        // $this->getPrintResult($res);



                        $insightsArray[] = array(



                            "account_name" => $rs->account_name,



                            "actions_per_impression" => $rs->actions_per_impression,



                            "campaign_id" => $rs->campaign_id,



                            "campaign_name" => $rs->campaign_name,



                            "cost_per_action_type" => $rs->cost_per_action_type,



                            "cost_per_total_action" => $rs->cost_per_total_action,



                            "cost_per_unique_click" => $rs->cost_per_unique_click,



                            "cost_per_inline_link_click" => $rs->cost_per_inline_link_click,



                            "cost_per_inline_post_engagement" => $rs->cost_per_inline_post_engagement,



                            "cpm" => $rs->cpm,



                            "cpp" => $rs->cpp,



                            "ctr" => $rs->ctr,



                            "date_start" => $rs->date_start,



                            "date_stop" => $rs->date_stop,



                            "frequency" => $rs->frequency,



                            "impressions" => $rs->impressions,



                            "inline_link_clicks" => $rs->inline_link_clicks,



                            "inline_post_engagement" => $rs->inline_post_engagement,



                            "reach" => $rs->reach,



                            "spend" => $rs->spend,



                            "total_action_value" => $rs->total_action_value,



                            "total_actions" => $rs->total_actions,



                            "total_unique_actions" => $rs->total_unique_actions,



                            "unique_clicks" => $rs->unique_clicks,



                            "unique_ctr" => $rs->unique_ctr



                            );



                    }



                    if ($insightsArray) {



                        $campaignDataArray[] = array(



                            "id" => $row->id,



                            "name" => $row->name,



                            "created_time" => $row->created_time,



                            "effective_status" => $row->effective_status,



                            "status_paused" => $row->status_paused,



                            "AllArray" => $row,



                            "insights" => $insightsArray



                            );



                        $insightsArray = array();



                    }



                } catch (Exception $e) {



                    return $resultArray = array(



                        "count" => 0,



                        "response" => "",



                        "error" => $e->getMessage(),



                        "success" => ""



                        );



                }



            }







            if (count($campaignDataArray) > 1) {



                $finalArray = $this->getFinalResult($campaignDataArray);



                $resultArray = array(



                    "count" => 1,



                    "response" => $finalArray,



                    "error" => "",



                    "success" => ""



                    );



            } else {



                $resultArray = array(



                    "count" => 0,



                    "response" => "",



                    "error" => "No record found.",



                    "success" => ""



                    );



            }



        } catch (Exception $e) {



            $resultArray = array(



                "count" => 0,



                "response" => "",



                "error" => $e->getMessage(),



                "success" => ""



                );



        }



        return $resultArray;



    }







    /* Privat functions */







    private function setAddAccountId($adAccountId) {



        $this->add_account_id = $adAccountId;



    }







    private function getAddAccountId() {



        return $this->add_account_id;



    }







    /* End Private */







    function get_hub_insight_per($obj, $insights, $param) {



        try {



//            if (!empty($param))



//            {



//                $params = array(



//                    $param



//                );



//                return $response = $obj->getInsights($insights, $params);



//            }



//            else



//            {



            return $response = $obj->getInsights($insights);



            // }



        } catch (Exception $e) {



            return $e->getMessage();



        }



    }







    function getCampaignFields($curl) {



        if ($curl == 1) {



            $fields = "id,account_id,name,objective,buying_type,created_time,start_time,stop_time,effective_status,currency,configured_status";



        } else {



            $fields = array(



                CampaignFields::ID,



                CampaignFields::ACCOUNT_ID,



                CampaignFields::NAME,



                CampaignFields::OBJECTIVE,



                CampaignFields::BUYING_TYPE,



                CampaignFields::PROMOTED_OBJECT,



                CampaignFields::ADLABELS,



                CampaignFields::CREATED_TIME,



                CampaignFields::START_TIME,



                CampaignFields::STOP_TIME,



                CampaignFields::UPDATED_TIME,



                CampaignFields::EFFECTIVE_STATUS,



                CampaignFields::STATUS_PAUSED,



                );



        }



        return $fields;



    }







    function getCampaignInsightFields($curl) {



        if ($curl == 1) {



            $fields = "{date_start,date_stop,buying_type,campaign_id,actions{action_type,value}}";



        } else {







            $fields = array(



                InsightsFields::ACCOUNT_ID,



                InsightsFields::IMPRESSIONS,



                InsightsFields::UNIQUE_CLICKS,



                InsightsFields::REACH,



                InsightsFields::INLINE_LINK_CLICKS,



                InsightsFields::COST_PER_INLINE_LINK_CLICK,



                InsightsFields::COST_PER_UNIQUE_CLICK,



                InsightsFields::SPEND,



                InsightsFields::COST_PER_ACTION_TYPE



                );



        }



        return $fields;



    }







    function getCampaignParams($type) {



        return $params = ".date_preset($type)";



    }







    function getFinalResult($campaignDataArray) {



        $activeCampaigns = 0;



        $inActiveCampaigns = 0;



        $cost_per_unique_click = 0;



        $total_cost_per_inline_click = 0;



        $impressions = 0;



        $total_inline_clicks = 0;



        $reach = 0;



        $unique_clicks = 0;



        $unique_ctr = 0;



        $spent = 0;



        $actions = 0;



        $cost_per_total_action = 0;







        $inline_clicks = 0;



        $cost_per_inline_clicks = 0;



        $pendingCampaigns = 0;



        $disapprovedCampaigns = 0;



        $total = 0;



        $total1 = 0;



        $total2 = 0;



        $conversion = 0;



        #echo "<PRE>";print_R($campaignDataArray);exit;



        $returnRow = array();



        foreach ($campaignDataArray as $row) {



            if ($row['effective_status'] == "ACTIVE" || $row['effective_status'] == "PREAPPROVED") {



                $activeCampaigns++;



                $status = "Active";



            } 



            else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {



                $inActiveCampaigns++;



                $status = "Inactive";



            } 



            else if ($row['effective_status'] == "PENDING_REVIEW") {



                $pendingCampaigns++;



                $status = "In Review";



            } 



            else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {



                $disapprovedCampaigns++;



                $status = "Denied";



            }



            $total++;



            



            if (isset($row['insights'])) {



                // echo $row['name']."<br>";



                $inline_post_engagement1 = $like1 = '0';



                $conversion = $cost_per_conversion = 0;



                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';



                if (isset($row['insights']['data'][0])) {



                    foreach ($row['insights']['data'][0]['actions'] as $rs) {



                        #echo "<PRE>";print_r($row['insights']);exit;



                        if ($row['insights']['data'][0]['clicks']) {



                            $inline_clicks = $row['insights']['data'][0]['clicks'];



                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];



                        }



                        if ($rs['action_type'] == "leadgen.other") {



                            if($rs['value']){



                                $conversion = $rs['value'];



                            }



                            else{



                                $conversion = 0;



                            }



                        } 



                        if ($rs['action_type'] == "offsite_conversion") {



                            if($rs['value']){



                                $conversion += $rs['value'];



                                $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];



                            }



                            else{



                                $conversion = 0;



                                $cost_per_conversion = 0;



                            }



                        } 



                        if($rs['action_type'] == 'page_engagement'){



                            if($rs['value']){



                                $total1++;



                                $inline_post_engagement1 = $rs['value'];



                            }



                            else{



                                $inline_post_engagement1 = 0;



                            }



                        }



                        if($rs['action_type'] == 'like'){



                            if($rs['value']){



                                $total2++;



                                $like1 = $rs['value'];



                            }



                            else{



                                $like1 = 0;



                            }



                        }



                    }



                    foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {



                        if($rs['action_type'] == 'page_engagement'){



                            if($rs['value']){



                                $cost_per_inline_post_engagement1 = $rs['value'];



                            }



                            else{



                                $cost_per_inline_post_engagement1 = 0;



                            }



                        }



                        if ($rs['action_type'] == "leadgen.other") {



                            if($rs['value']){



                                $cost_per_conversion = $rs['value'];



                            }



                            else{



                                $cost_per_conversion = 0;



                            }



                        } 



                        if($rs['action_type'] == 'like'){



                            if($rs['value']){



                                $cost_per_like1 = $rs['value'];



                            }



                            else{



                                $cost_per_like1 = 0;



                            }



                        }



                    }



                }



                



                $total_inline_clicks+=$inline_clicks;



                $total_cost_per_inline_click+=$cost_per_inline_clicks;







                $returnRow['campaigns'][] = array(



                    "campaign_id" => $row['id'],



                    "campaign_name" => $row['name'],



                    "campaign_created_time" => $row['created_time'],



                    "campaign_effective_status" => $status, //$row['effective_status'],



                    "campaign_cost_per_unique_click" => $row['insights']['data'][0]['cost_per_unique_click'],



                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,



                    "campaign_impressions" => $row['insights']['data'][0]['impressions'],



                    "campaign_inline_link_clicks" => $inline_clicks,



                    "campaign_reach" => $row['insights']['data'][0]['reach'],



                    "campaign_unique_clicks" => $row['insights']['data'][0]['unique_clicks'],



                    "campaign_unique_ctr" => $row['insights']['data'][0]['ctr'],



                    "campaign_spent" => $row['insights']['data'][0]['spend'],



                    "campaign_actions" => $conversion,



                    "campaign_cost_per_total_action" => $cost_per_conversion,



                    "campaign_reach" => $row['insights']['data'][0]['reach'],



                    "campaign_objective" => $row['objective'],



                    "inline_post_engagement" => $inline_post_engagement1,



                    "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement1,



                    "page_like" => $like1,



                    "cost_per_like1" => $cost_per_like1,



                    "leadgen" => $leadgen,



                    "cost_leadgen" => $cost_leadgen



                    );







                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];



                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];



                $impressions += $row['insights']['data'][0]['impressions'];



                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];



                $reach += $row['insights']['data'][0]['reach'];



                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];



                $unique_ctr += $row['insights']['data'][0]['ctr'];



                $spent += $row['insights']['data'][0]['spend'];



                $actions += $conversion;



                $cost_per_total_action += $cost_per_conversion;



                $inline_post_engagement += $inline_post_engagement1;



                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;







                $page_like += $like1;



                $cost_per_like += $cost_per_like1;



                $cost_leadgen1 += $cost_leadgen;



                $leadgen1 += $leadgen;







                //Reset   



                $inline_clicks = '--';



                $cost_per_inline_clicks = '--';



            } 



            else {







                $returnRow['campaigns'][] = array(



                    "campaign_id" => $row['id'],



                    "campaign_name" => $row['name'],



                    "campaign_created_time" => $row['created_time'],



                    "campaign_effective_status" => $status, //$row['effective_status'],



                    "campaign_cost_per_unique_click" => 0,



                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,



                    "campaign_impressions" => 0,



                    "campaign_inline_link_clicks" => $inline_clicks,



                    "campaign_reach" => 0,



                    "campaign_unique_clicks" => 0,



                    "campaign_unique_ctr" => 0,



                    "campaign_spent" => 0,



                    "campaign_actions" => 0,



                    "campaign_cost_per_total_action" => 0,



                    "campaign_reach" => 0,



                    "inline_post_engagement" => 0,



                    "cost_per_inline_post_engagement" => 0,



                    "page_like" => 0,



                    "cost_per_like1" => 0,



                    "campaign_objective" => $row['objective'],



                    "leadgen" => 0,



                    "cost_leadgen" => 0



                    );







                $cost_per_unique_click += 0;



                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];



                $impressions += 0;



                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];



                $reach += 0;



                $unique_clicks += 0;



                $unique_ctr += 0;



                $spent += 0;



                $actions += 0;



                $cost_per_total_action += 0;



                $inline_post_engagement += 0;



                $cost_per_inline_post_engagement += 0;







                $page_like += 0;



                $cost_per_like += 0;



                $cost_leadgen1 += 0;



                $leadgen1 += 0;



                



                //Reset   



                $inline_clicks = '--';



                $cost_per_inline_clicks = '--';



            }



        }



        if ($unique_ctr > 0) {



            $unique_ctr = ($unique_ctr / $total);



        }



        if ($total_cost_per_inline_click > 0) {



            $total_cost_per_inline_click = ($total_cost_per_inline_click / $total);



        }



        



        if ($cost_per_inline_post_engagement > 0) {



            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);



        }



        if ($cost_leadgen1 > 0) {



            $cost_leadgen1 = ($cost_leadgen1 / $total3);



        }



        if ($cost_per_like > 0) {



            $cost_per_like = ($cost_per_like / $total2);



        }



        //echo $actions;



        if ($cost_per_total_action > 0) {



            $cost_per_total_action = ($spent / $actions);



        }



        $returnRow['campaigns'] = $this->sksort($returnRow['campaigns'], "campaign_effective_status");



        $returnRow['campaign_header'] = array(



            "campaign_active" => $activeCampaigns,



            "campaign_inactive" => $inActiveCampaigns,



            "campaign_pending" => $pendingCampaigns,



            "campaign_disapproved" => $disapprovedCampaigns,



            "cost_per_unique_click" => $cost_per_unique_click,



            "cost_per_inline_link_click" => $total_cost_per_inline_click,



            "impressions" => $impressions,



            "inline_link_clicks" => $total_inline_clicks,



            "reach" => $reach,



            "unique_clicks" => $unique_clicks,



            "unique_ctr" => $unique_ctr,



            "spent" => $spent,



            "actions" => $actions,



            "actions" => $actions,



            "cost_per_total_action" => $cost_per_total_action,



            "inline_post_engagement" => $inline_post_engagement,



            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,



            "page_like" => $page_like,



            "cost_per_like" => $cost_per_like,



            "leadgen" => $leadgen1,



            "cost_leadgen" => $cost_leadgen1,



            );







        #echo "<PRE>";print_R($returnRow);exit;



        return $returnRow;



    }







    function getPrintResult($array) {



        echo "<pre>";



        print_r($array);



        echo "</pre>";



        exit;



    }







    function getCurlCampaigns($limit) {







        $adAccountId = "";



        $date_preset = "";



        $date_preset1 = "";



        $resultArray = array();



        $adAccountId = "act_" . $this->getAddAccountId();



        $campaignFields = $this->getCampaignFields(1);



        $dataArray = "";



        if (!empty($limit)) {



            $date_preset = $this->getCampaignParams($limit);







            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights$date_preset$date_preset$campaignFields$date_preset1" . "&limit=250&access_token=" . $this->access_token;



        } else {



            $date_preset = $this->getCampaignParams("last_7_days");







            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights$date_preset$campaignFields$date_preset1" . "&limit=250&access_token=" . $this->access_token;



        }



        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights{clicks,impressions,reach,actions,date_start,campaign_name,ctr,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&time_range={'since':'".$this->sDate."','until':'".$this->eDate."'}&limit=250&access_token=" . $this->access_token;



        try {



            //  echo $url;



            // exit;



            $ch = curl_init($url);



            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);



            $result = curl_exec($ch);



            curl_close($ch);



            $response = json_decode($result, true);



            //$result = json_decode($result);



            // print_r($response);



            $error = $response['error']['message']; //($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;



            // $this->getPrintResult($response);



            if (isset($response['data'])) {



                $campaignDataArray = $this->getFinalResult($response['data']);



                if ($campaignDataArray) {



                    $resultArray = array(



                        "count" => 1,



                        "response" => $campaignDataArray,



                        "error" => "",



                        "success" => ""



                        );



                } else {



                    $resultArray = array(



                        "count" => 0,



                        "response" => "",



                        "error" => $error,



                        "success" => ""



                        );



                }



            } else {



                $resultArray = array(



                    "count" => 0,



                    "response" => "",



                    "error" => $error,



                    "success" => ""



                    );



            }



        } catch (Exception $ex) {



            $resultArray = array(



                "count" => 0,



                "response" => "",



                "error" => $e->getMessage(),



                "success" => ""



                );



        }



        //$this->getPrintResult($resultArray);



        return $resultArray;



    }







    function getCurlCampaignsDateRange($limit) {







        $cost_per_unique_click = 0;



        $total_cost_per_inline_click = 0;



        $impressions = 0;



        $total_inline_clicks = 0;



        $reach = 0;



        $unique_clicks = 0;



        $unique_ctr = 0;



        $spent = 0;



        $actions = 0;



        $cost_per_total_action = 0;



        $cost_per_conversion = 0;



        $inline_clicks = 0;



        $cost_per_inline_clicks = 0;



        $total = 0;



        $conversion = 0;







        $returnRow = array();



        $adAccountId = "";







        $resultArray = array();



        $adAccountId = "act_" . $this->getAddAccountId();



        $campaignFields = $this->getCampaignFields(1);



        $dataArray = explode("#", $limit);



        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?time_range={'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'}&limit=250&access_token=" . $this->access_token;



        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights.time_range({'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'})$campaignFields" . "&limit=250&access_token=" . $this->access_token;



        echo $url;exit;



        try {







            $ch = curl_init($url);



            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);



            $result = curl_exec($ch);



            curl_close($ch);



            $response = json_decode($result, true);



            $error = $response['error']['message'];



            // $this->getPrintResult($response);



            if (isset($response['data'])) {



                $campaignDataArray = $this->getFinalResult($response['data']);



                if ($campaignDataArray) {



                    $resultArray = array(



                        "count" => 1,



                        "response" => $campaignDataArray,



                        "error" => "",



                        "success" => ""



                        );



                } else {



                    $resultArray = array(



                        "count" => 0,



                        "response" => "",



                        "error" => $error,



                        "success" => ""



                        );



                }



            } else {



                $resultArray = array(



                    "count" => 0,



                    "response" => "",



                    "error" => $error,



                    "success" => ""



                    );



            }



        } catch (Exception $ex) {



            $resultArray = array(



                "count" => 0,



                "response" => "",



                "error" => $e->getMessage(),



                "success" => ""



                );



        }



        //$this->getPrintResult($resultArray);



        return $resultArray;



    }



    



    function getCurlCampaignsDateRange_new($limit) {



        $resultArray = array();



        $adAccountId = "act_" . $this->getAddAccountId();



        $campaignFields = $this->getCampaignFields(1);



        $dataArray = explode("#", $limit);



        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=$campaignFields" . "&limit=250&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&access_token=" . $this->access_token;



        //OLD $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights.time_range({'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'})$campaignFields" . "&limit=250&access_token=" . $this->access_token;



        #echo $url;



        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights{clicks,impressions,reach,actions,date_start,campaign_name,ctr,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250&access_token=" . $this->access_token;



        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights.date_preset(".$limit."){clicks,impressions,reach,actions,date_start,campaign_name,ctr,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&limit=250&access_token=" . $this->access_token;



        #echo "<br>".$url;exit;



        try {







            $ch = curl_init($url);



            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);



            $result = curl_exec($ch);



            curl_close($ch);



            $response = json_decode($result, true);



            $error = $response['error']['message'];



            // $this->getPrintResult($response);



            /*if (isset($response['data'])) {



                $finalArr = array();



                foreach ($response['data'] as $campaignData){



                    $campaignId = $campaignData['id'];



                    $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?fields=clicks,impressions,reach,actions,date_start,campaign_name,ctr,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250&access_token=" . $this->access_token;



                    $ch = curl_init($url);



                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);



                    $result = curl_exec($ch);



                    curl_close($ch);



                    $response1 = json_decode($result, true);



                    $campaignData['insights']['data'] = $response1['data'];



                    $campaignData['insights']['paging'] = $response1['paging'];



                    array_push($finalArr, $campaignData);



                }



                



                $campaignDataArray = $this->getFinalResult($finalArr);



                #echo "<PRE>";print_R($campaignDataArray);exit;



                if ($campaignDataArray) {



                    $resultArray = array(



                        "count" => 1,



                        "response" => $campaignDataArray,



                        "error" => "",



                        "success" => ""



                    );



                } else {



                    $resultArray = array(



                        "count" => 0,



                        "response" => "",



                        "error" => $error,



                        "success" => ""



                    );



                }



            } else {



                $resultArray = array(



                    "count" => 0,



                    "response" => "",



                    "error" => $error,



                    "success" => ""



                );



            }*/



            



            if (isset($response['data'])) {



                $campaignDataArray = $this->getFinalResult($response['data']);



                if ($campaignDataArray) {



                    $resultArray = array(



                        "count" => 1,



                        "response" => $campaignDataArray,



                        "error" => "",



                        "success" => ""



                        );



                } else {



                    $resultArray = array(



                        "count" => 0,



                        "response" => "",



                        "error" => $error,



                        "success" => ""



                        );



                }



            } else {



                $resultArray = array(



                    "count" => 0,



                    "response" => "",



                    "error" => $error,



                    "success" => ""



                    );



            }



        } catch (Exception $ex) {



            $resultArray = array(



                "count" => 0,



                "response" => "",



                "error" => $e->getMessage(),



                "success" => ""



                );



        }



        //$this->getPrintResult($resultArray);



        return $resultArray;



    }

	function sksort(&$array, $subkey = "id", $subkey2 = null, $sort_ascending = true) {



    if (count($array))



        $temp_array[key($array)] = array_shift($array);



    foreach ($array as $key => $val) {



        $offset = 0;



        $found = false;



        foreach ($temp_array as $tmp_key => $tmp_val) {



            if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {



                $temp_array = array_merge(



                    (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));



                $found = true;



            } elseif (!$found



                and $subkey2 and strtolower($val[$subkey]) == strtolower($tmp_val[$subkey])



                and strtolower($val[$subkey2]) > strtolower($tmp_val[$subkey2])) {



                $temp_array = array_merge(



                    (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));



                $found = true;



            }



            $offset++;



        }



        if (!$found)



            $temp_array = array_merge($temp_array, array($key => $val));



    }



    if ($sort_ascending)



        $array = array_reverse($temp_array);



    else



        $array = $temp_array;







    return $array;



}

        function copyAdsets() {

			

			

				$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/?asyncbatch=['.urlencode('{ "method":"POST", "relative_url":"'.$this->input->post('addSetId').'/copies", "name":"copy_adset_1345","body":"deep_copy=true" }').']&access_token=' . $this->access_token;

			  

                

                #echo $url;exit;

                $ch = curl_init($url);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                curl_setopt($ch, CURLOPT_POST, 1);

                $result = curl_exec($ch);

				//print_r($result);

                curl_close($ch);

                $response = json_decode($result, true);

                /*echo "<pre>";

                print_r($response);

                

                echo "</pre>";*/



                 if(isset($response['async_sessions'][0]['id'])){

                     $optadsetdata = $response['async_sessions'][0]['id']."||$$".$this->input->post('agerange')."||$$".$this->input->post('gender')."||$$".$this->input->post('adPlacement')."||$$".$this->input->post('adsetbudget');

                     $currentuserid = $this->user_id;

            

                     $this->Campaign_model->UpdateUserOptimizedData($optadsetdata,$currentuserid);

                }

			

			

				return "success";

                /*$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/23842585923380160/copies?name=duplicate_adset_1&deep_copy=true&access_token=' . $this->access_token;

                

                //echo $url;exit;

                $ch = curl_init($url);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                curl_setopt($ch, CURLOPT_POST, 1);

                $result = curl_exec($ch);

                curl_close($ch);

                $response = json_decode($result, true);

                print_r($response);

                exit;*/



                   

                $postFields = array(

                    'access_token' => $this->access_token,

                );



                //$postFields['targeting'] = "{'age_min': 35, 'age_max': 44}";

                //$postFields['targeting'] = "{'age_min':35,'age_max':44,'geo_locations': {'countries':['IE','IT','JP','MY','NL','NZ','CA','SA','SG','ZA','KR','TR','GR','VE','AR']}}";

                /* $postFields['targeting'] = '{

    "age_max": 44,

    "age_min": 35,

    "flexible_spec": [

      {

        "interests": [

          {

            "id": "6003146343826",

            "name": "Facebook for Business"

          }

        ]

      }

    ],

    "genders": [

      1

    ],

    "geo_locations": {

      "countries": [

        "IE",

        "IT",

        "JP",

        "MY",

        "MX",

        "NL",

        "NZ",

        "CA",

        "SA",

        "SG",

        "ZA",

        "KR",

        "TR",

        "GR",

        "VE",

        "AR",

        "AU",

        "TH",

        "PE",

        "ID",

        "AT",

        "PH",

        "HK",

        "BR",

        "PL",

        "TW",

        "PT",

        "CO",

        "MA",

        "EC",

        "FR",

        "VN",

        "DE"

      ],

      "location_types": [

        "home"

      ]

    },

    "locales": [

      24,

      6

    ],

    "publisher_platforms": [

      "facebook"

    ],

    "facebook_positions": [

      "feed",

      "right_hand_column"

    ],

    "device_platforms": [

      "mobile",

      "desktop"

    ]

  }';

                $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/23842597133640160');

                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    curl_setopt_array($ch, array(

                        CURLOPT_RETURNTRANSFER => true,

                        CURLOPT_POST => true,

                        CURLOPT_POSTFIELDS => $postFields

                    ));



                    $response = curl_exec($ch);

                    curl_close($ch);

                    $result = json_decode($response);

                    print_r($result);

                exit;

                

*/

                //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/23842585923380160/?fields=name,start_time,targeting&access_token=' . $this->access_token;

              /*$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/23842597133640160/?fields=name,start_time,targeting&access_token=' . $this->access_token;

                //echo $url;exit;

                $ch = curl_init($url);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $result = curl_exec($ch);

                curl_close($ch);

                $response = json_decode($result, true);

                echo "<pre>";

                print_r($response);

                $response['targeting']['age_max'] = '1';

                $response['targeting']['age_min'] = '2';

                print_r($response);

                print_r(json_encode($response));

                echo "</pre>";

                exit;*/

				

				

				//,campaign_id=23842529156750160"

			    



                



                /*$response['async_sessions'][0]['id'] = '144222369501686';

                if(isset($response['async_sessions'][0]['id'])){

                    $ch2 = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'?id='.$response['async_sessions'][0]['id'].'&pretty=true&fields=result&access_token=' . $this->access_token);

                    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);

                    curl_setopt_array($ch2, array(

                        CURLOPT_RETURNTRANSFER => true

                    ));



                    $response = curl_exec($ch2);

                    curl_close($ch2);

                    $result = json_decode($response);

                    if(isset(json_decode($result->result)->copied_adset_id)){

                        echo json_decode($result->result)->copied_adset_id; 

                    }

                    else{

                        echo "something wrong";

                    }

                }    

                

                exit;*/

                //echo $response['async_sessions'][0]['id'];

                

                /*  $ch2 = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'?id=1403307186404009&pretty=true&fields=result&access_token=' . $this->access_token);

                    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);

                    curl_setopt_array($ch2, array(

                        CURLOPT_RETURNTRANSFER => true

                    ));



                    $response = curl_exec($ch2);

                    curl_close($ch2);

                    $result = json_decode($response);

                    echo "<pre>";

                    print_r(json_decode($result->result));

                    echo json_decode($result->result)->copied_adset_id;

                    echo "</pre>";

                exit;*/

                

        }



        function generateanalysispdf(){



            $returnmaindata = '';

        //$dataArray = explode("#", $limit);

        //$addSetFields = $this->getAdsFields(1);

        $addSetId = $this->input->get('addSetId',TRUE);//"23842585923380160";

        $campaignObjectivce = $this->input->get('campaignObjectivce',TRUE); //"CONVERSIONS";





        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/?fields=insights.date_preset(lifetime){inline_link_clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&access_token=" . $this->access_token;

        #echo $url;exit;

        #echo $url;

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            #print_r($response);

            #exit;

            if(isset($response['insights'])){

                if($campaignObjectivce == 'CONVERSIONS'){

                    $conversionval = 0;

                    $costperconversion = 0;

                    $resultrate = 0;

                    foreach($response['insights']['data'][0]['actions'] as $row){

                        if($row['action_type'] == 'offsite_conversion'){

                            $conversionval = $row['value'];

                        }

                    }

                    



                    if($response['insights']['data'][0]['spend']>0){

                        $costperconversion1 = 0;

                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;

                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');

                    }

                    if($response['insights']['data'][0]['impressions']>0){

                        $resultrate1 = 0;

                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;

                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";





                    }



                    

                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />

                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>

                            <br class="hidden-lg">Total Results</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>

                            <br class="hidden-lg">Cost Per Result</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />

                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>

                            <br class="hidden-lg">Result Rate</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>

                            <br class="hidden-lg">Total Spent</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>

                            Best Performing</td>';





                     // agegroup started

                     

                        $female = 0;

                        $male = 0;

                        $other = 0;

                        $total = 0;

                        

                        $date_preset = "";

                        $dataArray = "";

                        

                        $date_preset = "date_preset=lifetime&";

                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;

                        

                        #echo $url;exit;

                        $res = array();

                        $resultArray = array();

                        $impressionarray = array();

                        $spendarray = array();

                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';

                        if($pageName == 'adddsReport'){

                            $pageName = 'impressions';

                        }*/

                        $pageName = 'offsite_conversion';

                        $ch = curl_init($url);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result = curl_exec($ch);

                        curl_close($ch);

                        $response = json_decode($result, true);

                        /*echo "<pre>";

                        print_r($response);

                        echo "</pre>";*/

                        //exit;

                        if (isset($response['data'])) {

                            foreach ($response['data'] as $ages) {

                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                    foreach ($ages['actions'] as $feeds1) {

                                        if ($feeds1['action_type'] == $pageName) {

                                            $res[] = array(

                                                "range" => $ages['age'],

                                                "inline_link_clicks" => $ages['inline_link_clicks'],

                                                "impressions" => $feeds1['value'],

                                                "gender" => $ages['gender'],

                                                "original"

                                            );

                                            $total += $ages['inline_link_clicks'];

                                        }

                                    }

                                }

                                else{

                                    $res[] = array(

                                        "range" => $ages['age'],

                                        "inline_link_clicks" => $ages['inline_link_clicks'],

                                        "impressions" => $ages[$pageName],

                                        "gender" => $ages['gender']

                                    );

                                    $total += $ages['inline_link_clicks'];

                                }

                                if($ages['age'] == '13-17'){

                                    $impressionarray['13-17i'] += $ages['impressions'];

                                     $spendarray['13-17s'] += $ages['spend'];

                                }

                                if($ages['age'] == '18-24'){

                                    $impressionarray['18-24i'] += $ages['impressions'];

                                     $spendarray['18-24s'] += $ages['spend'];

                                }

                                if($ages['age'] == '25-34'){

                                    $impressionarray['25-34i'] += $ages['impressions'];

                                     $spendarray['25-34s'] += $ages['spend'];

                                }

                                if($ages['age'] == '35-44'){

                                    $impressionarray['35-44i'] += $ages['impressions'];

                                     $spendarray['35-44s'] += $ages['spend'];

                                }

                                if($ages['age'] == '45-54'){

                                    $impressionarray['45-54i'] += $ages['impressions'];

                                     $spendarray['45-54s'] += $ages['spend'];

                                }

                                if($ages['age'] == '55-64'){

                                    $impressionarray['55-64i'] += $ages['impressions'];

                                     $spendarray['55-64s'] += $ages['spend'];

                                }

                                if($ages['age'] == '65+'){

                                    $impressionarray['65+i'] += $ages['impressions'];

                                     $spendarray['65+s'] += $ages['spend'];

                                }



                            }

                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;

                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;

                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;

                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;

                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;

                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;

                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;

                            /*echo "<pre>";

                        print_r($res);

                        echo "</pre>";*/

                        

                            foreach ($res as $rs) {

                                $resultArray1[$rs['gender']]['range'] = $rs['range'];

                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];

                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];



                                if (array_key_exists($rs['range'], $resultArray)) {

                                    $resultArray[$rs['range']] = $resultArray1;

                                } else {

                                    $resultArray[$rs['range']] = array();

                                    $resultArray[$rs['range']] = $resultArray1;

                                }

                            }

                        }

                        /*echo "<pre>";

                        print_r($resultArray);

                        echo "</pre>";

                        exit;*/

                        $tempArr = array();

                        foreach ($resultArray as $k => $value) {

                            $tempStr1 = '0';

                            foreach ($value as $k1 => $value1) {

                                $tempStr1 += $value1['impressions'];

                            }

                            $tempArr[$k] = $tempStr1;

                        }



                        if (!array_key_exists('13-17', $resultArray)) {

                            $tempArr['13-17'] = 0;

                        }

                        if (!array_key_exists('18-24', $resultArray)) {

                            $tempArr['18-24'] = 0;

                        }

                        if (!array_key_exists('25-34', $resultArray)) {

                            $tempArr['25-34'] = 0;

                        }

                        if (!array_key_exists('35-44', $resultArray)) {

                            $tempArr['35-44'] = 0;

                        }

                        if (!array_key_exists('45-54', $resultArray)) {

                            $tempArr['45-54'] = 0;

                        }

                        if (!array_key_exists('55-64', $resultArray)) {

                            $tempArr['55-64'] = 0;

                        }

                        if (!array_key_exists('65+', $resultArray)) {

                            $tempArr['65+'] = 0;

                        }



                        /*print_r($impressionarray);

                        print_r($spendarray);

                        exit;*/

                        $returnmaindata .= '||||||';

                        $iag=1;





                        $returnmaindata .= '<tr>

                        <td>13 - 17';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['13-17']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>18 - 24';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['18-24']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>25 - 34';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['25-34']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>35 - 44';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['35-44']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>45 - 54';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['45-54']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>55 - 65';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['55-64']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>65+';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['65+']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>

                    </tr>';

                         /*foreach ($tempArr as $k => $value) {

                            $agegroup_total += $value;

                            if()

                            $returnmaindata .= '<tr>

                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>

                            18 - 24</td>

                        <td>'.number_format($value).'</td>

                        <td>$0.67</td>

                        <td class="text-success">0.45 %</td>

                        <td>$60.3</td>

                    </tr>';

                            

                            $iag++;

                        }*/







                        // Gender started

                        

                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                            

                            $date_preset = "";

                            $dataArray = "";

                            

                            $genimpressionarray = array();

                            $genspendarray = array();



                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;

                            



                           $pageName = 'offsite_conversion';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            

                            /*echo "<pre>";

                            print_r($response);

                            echo "</pre>";

                            exit;*/



                            if (isset($response['data'])) {

                                foreach ($response['data'] as $genders) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($genders['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($genders['gender'] == "female") {

                                                    $female = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "male") {

                                                    $male = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "unknown") {

                                                    $other = $feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($genders['gender'] == "female") {

                                            $female = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "male") {

                                            $male = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "unknown") {

                                            $other = $genders[$pageName];

                                        }

                                    }



                                    if($genders['gender'] == 'male'){

                                        $genimpressionarray['malei'] += $genders['impressions'];

                                         $genspendarray['males'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'female'){

                                        $genimpressionarray['femalei'] += $genders['impressions'];

                                         $genspendarray['females'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'unknown'){

                                        $genimpressionarray['unknowni'] += $genders['impressions'];

                                         $genspendarray['unknowns'] += $genders['spend'];

                                    }

                                }

                            }

                            if(empty($genspendarray['males'])) $genspendarray['males']=0;

                            if(empty($genspendarray['females'])) $genspendarray['females']=0;

                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;

                            $returnmaindata .= '||||||';

                    $returnmaindata .= '<tr>

                        <td>Men';

                        $returnmaindata .= '</td>

                        <td>'.$male.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Women';

                        $returnmaindata .= '</td>

                        <td>'.$female.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Other';

                        $returnmaindata .= '</td>

                        <td>'.$other.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>

                    </tr>';





                            //adplacement started

                            $desktop = 0;

                            $mobile = 0;

                            $other = 0;

                            $total = 0;

                            $dataArray = 0;

                            

                            $date_preset = "";



                            $placeimpressionarray = array();

                            $placespendarray = array();



                            

                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;

                            

                            $res = array();

                            $resultArray = array();



                            $pageName = 'offsite_conversion';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            //$this->getPrintResult($response);

            /*                 echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $feeds) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($feeds['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $desktop = $feeds1['value'];

                                                    $total += $feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $mobile = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "instagram") {

                                                    $instant_article = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "audience_network") {

                                                    $mobile_external_only = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                                    $right_hand = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $desktop = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $mobile = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $instant_article = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $mobile_external_only = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $right_hand = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                    }

                                    

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];

                                             $placespendarray['desktops'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];

                                             $placespendarray['mobiles'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];

                                             $placespendarray['instagrams'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];

                                             $placespendarray['audnetworks'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];

                                             $placespendarray['rhss'] += $feeds['spend'];

                                        }



                                }



                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;

                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;

                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;

                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;

                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;

                                if ($desktop > 0) {

                                    $desktopPer = round(($desktop / $total) * 100);

                                }

                                if ($mobile > 0) {

                                    $mobilePer = round(($mobile / $total) * 100);

                                }

                                if ($instant_article > 0) {

                                    $instant_articlePer = round(($instant_article / $total) * 100);

                                }

                                if ($mobile_external_only > 0) {

                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);

                                }

                                if ($right_hand > 0) {

                                    $right_handPer = round(($right_hand / $total) * 100);

                                }

                            }

                           /* echo "<pre>";

            print_r($placeimpressionarray);

            print_r($placespendarray);

            echo "</pre>";

            exit;*/

                            $returnmaindata .= '||||||';

                            $returnmaindata .= '<tr>

                        <td>Desktop Newsfeed';

                            $returnmaindata .= '</td>

                        <td>'.number_format($desktop).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Mobile Newsfeed';

                            $returnmaindata .= '

                        </td>

                        <td>'.number_format($mobile).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Instagram Feed';

                            $returnmaindata .= '</td>

                        <td>'.number_format($instant_article).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Audience Network';

                            $returnmaindata .= '</td>

                        <td>'.number_format($mobile_external_only).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Desktop Right Side';

                            $returnmaindata .= '</td>

                        <td>'.number_format($right_hand).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>

                    </tr>';





                            //country started



                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                           $tempStr = '';

                            $date_preset = "";

                            $dataArray = "";

                            

                                $date_preset = "date_preset=lifetime&";

                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;

                            



                            $res = array();

                            $resultArray = array();



                            $pageName = 'offsite_conversion';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

/*echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $ages) {

                                    if(empty($ages['spend'])) $ages['spend']=0;

                                    $result = $this->Users_model->getCountryName($ages['country']);



                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($ages['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                $countrytotal += (int)$feeds1['value'];

                                                $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($feeds1['value']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';

                                            }

                                        }

                                    }

                                    else{

                                        $countrytotal += (int)$ages[$pageName];

                                        $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($ages[$pageName]).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';

                                    }

                                }

                            }

                            $returnmaindata .= '||||||';

                            $returnmaindata .= $tempStr;



                }

                elseif($campaignObjectivce == 'LINK_CLICKS' || $campaignObjectivce == 'PRODUCT_CATALOG_SALES'){

                    $conversionval = 0;

                    $costperconversion = 0;

                    $resultrate = 0;

                    /*foreach($response['insights']['data'][0]['actions'] as $row){

                        if($row['action_type'] == 'offsite_conversion'){

                            $conversionval = $row['value'];

                        }

                    }*/

                    $conversionval = $response['insights']['data'][0]['inline_link_clicks'];

                    if($response['insights']['data'][0]['spend']>0){

                        $costperconversion1 = 0;

                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;

                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');

                    }

                    if($response['insights']['data'][0]['impressions']>0){

                        $resultrate1 = 0;

                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;

                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";





                    }

                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />

                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>

                            <br class="hidden-lg">Total Results</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>

                            <br class="hidden-lg">Cost Per Result</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />

                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>

                            <br class="hidden-lg">Result Rate</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>

                            <br class="hidden-lg">Total Spent</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>

                            Best Performing</td>';





                     // agegroup started

                     

                        $female = 0;

                        $male = 0;

                        $other = 0;

                        $total = 0;

                        

                        $date_preset = "";

                        $dataArray = "";

                        

                        $date_preset = "date_preset=lifetime&";

                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;

                        

                        #echo $url;exit;

                        $res = array();

                        $resultArray = array();

                        $impressionarray = array();

                        $spendarray = array();

                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';

                        if($pageName == 'adddsReport'){

                            $pageName = 'impressions';

                        }*/

                        $pageName = 'inline_link_clicks';

                        $ch = curl_init($url);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result = curl_exec($ch);

                        curl_close($ch);

                        $response = json_decode($result, true);

                        /*echo "<pre>";

                        print_r($response);

                        echo "</pre>";

                        exit;*/

                        if (isset($response['data'])) {

                            foreach ($response['data'] as $ages) {

                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                    foreach ($ages['actions'] as $feeds1) {

                                        if ($feeds1['action_type'] == $pageName) {

                                            $res[] = array(

                                                "range" => $ages['age'],

                                                "inline_link_clicks" => $ages['inline_link_clicks'],

                                                "impressions" => $feeds1['value'],

                                                "gender" => $ages['gender'],

                                                "original"

                                            );

                                            $total += $ages['inline_link_clicks'];

                                        }

                                    }

                                }

                                else{

                                    $res[] = array(

                                        "range" => $ages['age'],

                                        "inline_link_clicks" => $ages['inline_link_clicks'],

                                        "impressions" => $ages[$pageName],

                                        "gender" => $ages['gender']

                                    );

                                    $total += $ages['inline_link_clicks'];

                                }

                                if($ages['age'] == '13-17'){

                                    $impressionarray['13-17i'] += $ages['impressions'];

                                     $spendarray['13-17s'] += $ages['spend'];

                                }

                                if($ages['age'] == '18-24'){

                                    $impressionarray['18-24i'] += $ages['impressions'];

                                     $spendarray['18-24s'] += $ages['spend'];

                                }

                                if($ages['age'] == '25-34'){

                                    $impressionarray['25-34i'] += $ages['impressions'];

                                     $spendarray['25-34s'] += $ages['spend'];

                                }

                                if($ages['age'] == '35-44'){

                                    $impressionarray['35-44i'] += $ages['impressions'];

                                     $spendarray['35-44s'] += $ages['spend'];

                                }

                                if($ages['age'] == '45-54'){

                                    $impressionarray['45-54i'] += $ages['impressions'];

                                     $spendarray['45-54s'] += $ages['spend'];

                                }

                                if($ages['age'] == '55-64'){

                                    $impressionarray['55-64i'] += $ages['impressions'];

                                     $spendarray['55-64s'] += $ages['spend'];

                                }

                                if($ages['age'] == '65+'){

                                    $impressionarray['65+i'] += $ages['impressions'];

                                     $spendarray['65+s'] += $ages['spend'];

                                }



                            }

                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;

                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;

                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;

                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;

                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;

                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;

                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;

                            /*echo "<pre>";

                        print_r($res);

                        echo "</pre>";*/

                        

                            foreach ($res as $rs) {

                                $resultArray1[$rs['gender']]['range'] = $rs['range'];

                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];

                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];



                                if (array_key_exists($rs['range'], $resultArray)) {

                                    $resultArray[$rs['range']] = $resultArray1;

                                } else {

                                    $resultArray[$rs['range']] = array();

                                    $resultArray[$rs['range']] = $resultArray1;

                                }

                            }

                        }

                        /*echo "<pre>";

                        print_r($resultArray);

                        echo "</pre>";

                        exit;*/

                        $tempArr = array();

                        foreach ($resultArray as $k => $value) {

                            $tempStr1 = '0';

                            foreach ($value as $k1 => $value1) {

                                $tempStr1 += $value1['impressions'];

                            }

                            $tempArr[$k] = $tempStr1;

                        }



                        if (!array_key_exists('13-17', $resultArray)) {

                            $tempArr['13-17'] = 0;

                        }

                        if (!array_key_exists('18-24', $resultArray)) {

                            $tempArr['18-24'] = 0;

                        }

                        if (!array_key_exists('25-34', $resultArray)) {

                            $tempArr['25-34'] = 0;

                        }

                        if (!array_key_exists('35-44', $resultArray)) {

                            $tempArr['35-44'] = 0;

                        }

                        if (!array_key_exists('45-54', $resultArray)) {

                            $tempArr['45-54'] = 0;

                        }

                        if (!array_key_exists('55-64', $resultArray)) {

                            $tempArr['55-64'] = 0;

                        }

                        if (!array_key_exists('65+', $resultArray)) {

                            $tempArr['65+'] = 0;

                        }



                        /*print_r($impressionarray);

                        print_r($spendarray);

                        exit;*/

                        $returnmaindata .= '||||||';

                        $iag=1;



                        $returnmaindata .= '<tr>

                        <td>13 - 17';

                            $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['13-17']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>18 - 24';

                            $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['18-24']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>25 - 34';

                            $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['25-34']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>35 - 44';

                            $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['35-44']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>45 - 54';

                           $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['45-54']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>55 - 65';

                          $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['55-64']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>65+';

                            $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['65+']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>

                    </tr>';

                         /*foreach ($tempArr as $k => $value) {

                            $agegroup_total += $value;

                            if()

                            $returnmaindata .= '<tr>

                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>

                            18 - 24</td>

                        <td>'.number_format($value).'</td>

                        <td>$0.67</td>

                        <td class="text-success">0.45 %</td>

                        <td>$60.3</td>

                    </tr>';

                            

                            $iag++;

                        }*/







                        // Gender started

                        

                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                            

                            $date_preset = "";

                            $dataArray = "";

                            

                            $genimpressionarray = array();

                            $genspendarray = array();



                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;

                            



                           $pageName = 'inline_link_clicks';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            

                            /*echo "<pre>";

                            print_r($response);

                            echo "</pre>";

                            exit;*/



                            if (isset($response['data'])) {

                                foreach ($response['data'] as $genders) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($genders['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($genders['gender'] == "female") {

                                                    $female = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "male") {

                                                    $male = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "unknown") {

                                                    $other = $feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($genders['gender'] == "female") {

                                            $female = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "male") {

                                            $male = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "unknown") {

                                            $other = $genders[$pageName];

                                        }

                                    }



                                    if($genders['gender'] == 'male'){

                                        $genimpressionarray['malei'] += $genders['impressions'];

                                         $genspendarray['males'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'female'){

                                        $genimpressionarray['femalei'] += $genders['impressions'];

                                         $genspendarray['females'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'unknown'){

                                        $genimpressionarray['unknowni'] += $genders['impressions'];

                                         $genspendarray['unknowns'] += $genders['spend'];

                                    }

                                }

                            }

                            if(empty($genspendarray['males'])) $genspendarray['males']=0;

                            if(empty($genspendarray['females'])) $genspendarray['females']=0;

                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;

                            $returnmaindata .= '||||||';

                    $returnmaindata .= '<tr>

                        <td>Men';

                            $returnmaindata .= '</td>

                        <td>'.$male.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Women';

                           $returnmaindata .= '</td>

                        <td>'.$female.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Other';

                           $returnmaindata .= '</td>

                        <td>'.$other.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>

                    </tr>';





                            //adplacement started

                            $desktop = 0;

                            $mobile = 0;

                            $other = 0;

                            $total = 0;

                            $dataArray = 0;

                            

                            $date_preset = "";



                            $placeimpressionarray = array();

                            $placespendarray = array();



                            

                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;

                            

                            $res = array();

                            $resultArray = array();



                            $pageName = 'inline_link_clicks';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            //$this->getPrintResult($response);

            /*                 echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $feeds) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($feeds['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $desktop = $feeds1['value'];

                                                    $total += $feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $mobile = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "instagram") {

                                                    $instant_article = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "audience_network") {

                                                    $mobile_external_only = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                                    $right_hand = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $desktop = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $mobile = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $instant_article = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $mobile_external_only = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $right_hand = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                    }

                                    

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];

                                             $placespendarray['desktops'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];

                                             $placespendarray['mobiles'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];

                                             $placespendarray['instagrams'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];

                                             $placespendarray['audnetworks'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];

                                             $placespendarray['rhss'] += $feeds['spend'];

                                        }



                                }

                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;

                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;

                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;

                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;

                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;

                                if ($desktop > 0) {

                                    $desktopPer = round(($desktop / $total) * 100);

                                }

                                if ($mobile > 0) {

                                    $mobilePer = round(($mobile / $total) * 100);

                                }

                                if ($instant_article > 0) {

                                    $instant_articlePer = round(($instant_article / $total) * 100);

                                }

                                if ($mobile_external_only > 0) {

                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);

                                }

                                if ($right_hand > 0) {

                                    $right_handPer = round(($right_hand / $total) * 100);

                                }

                            }

                           /* echo "<pre>";

            print_r($placeimpressionarray);

            print_r($placespendarray);

            echo "</pre>";

            exit;*/

                            $returnmaindata .= '||||||';

                            $returnmaindata .= '<tr>

                        <td>Desktop Newsfeed';

                            $returnmaindata .= '</td>

                        <td>'.number_format($desktop).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Mobile Newsfeed';

                        $returnmaindata .= '

                        </td>

                        <td>'.number_format($mobile).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Instagram Feed';

                        $returnmaindata .= '</td>

                        <td>'.number_format($instant_article).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Audience Network';

                        $returnmaindata .= '</td>

                        <td>'.number_format($mobile_external_only).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Desktop Right Side';

                        $returnmaindata .= '</td>

                        <td>'.number_format($right_hand).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>

                    </tr>';





                            //country started



                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                           $tempStr = '';

                            $date_preset = "";

                            $dataArray = "";

                            

                                $date_preset = "date_preset=lifetime&";

                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;

                            



                            $res = array();

                            $resultArray = array();



                            $pageName = 'inline_link_clicks';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

/*echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $ages) {

                                    if(empty($ages['spend'])) $ages['spend']=0;

                                    $result = $this->Users_model->getCountryName($ages['country']);



                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($ages['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                $countrytotal += (int)$feeds1['value'];

                                                $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($feeds1['value']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';

                                            }

                                        }

                                    }

                                    else{

                                        $countrytotal += (int)$ages[$pageName];

                                        $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($ages[$pageName]).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';

                                    }

                                }

                            }

                            $returnmaindata .= '||||||';

                            $returnmaindata .= $tempStr;



                }

                elseif($campaignObjectivce == 'POST_ENGAGEMENT'){

                    $conversionval = 0;

                    $costperconversion = 0;

                    $resultrate = 0;

                    foreach($response['insights']['data'][0]['actions'] as $row){

                        if($row['action_type'] == 'page_engagement'){

                            $conversionval = $row['value'];

                        }

                    }

                   // $conversionval = $response['insights']['data'][0]['inline_link_clicks'];

                    if($response['insights']['data'][0]['spend']>0){

                        $costperconversion1 = 0;

                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;

                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');

                    }

                    if($response['insights']['data'][0]['impressions']>0){

                        $resultrate1 = 0;

                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;

                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";





                    }

                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />

                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>

                            <br class="hidden-lg">Total Results</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>

                            <br class="hidden-lg">Cost Per Result</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />

                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>

                            <br class="hidden-lg">Result Rate</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>

                            <br class="hidden-lg">Total Spent</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>

                            Best Performing</td>';





                     // agegroup started

                     

                        $female = 0;

                        $male = 0;

                        $other = 0;

                        $total = 0;

                        

                        $date_preset = "";

                        $dataArray = "";

                        

                        $date_preset = "date_preset=lifetime&";

                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;

                        

                        #echo $url;exit;

                        $res = array();

                        $resultArray = array();

                        $impressionarray = array();

                        $spendarray = array();

                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';

                        if($pageName == 'adddsReport'){

                            $pageName = 'impressions';

                        }*/

                        $pageName = 'page_engagement';

                        $ch = curl_init($url);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result = curl_exec($ch);

                        curl_close($ch);

                        $response = json_decode($result, true);

                        /*echo "<pre>";

                        print_r($response);

                        echo "</pre>";*/

                        //exit;

                        if (isset($response['data'])) {

                            foreach ($response['data'] as $ages) {

                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                    foreach ($ages['actions'] as $feeds1) {

                                        if ($feeds1['action_type'] == $pageName) {

                                            $res[] = array(

                                                "range" => $ages['age'],

                                                "inline_link_clicks" => $ages['inline_link_clicks'],

                                                "impressions" => $feeds1['value'],

                                                "gender" => $ages['gender'],

                                                "original"

                                            );

                                            $total += $ages['inline_link_clicks'];

                                        }

                                    }

                                }

                                else{

                                    $res[] = array(

                                        "range" => $ages['age'],

                                        "inline_link_clicks" => $ages['inline_link_clicks'],

                                        "impressions" => $ages[$pageName],

                                        "gender" => $ages['gender']

                                    );

                                    $total += $ages['inline_link_clicks'];

                                }

                                if($ages['age'] == '13-17'){

                                    $impressionarray['13-17i'] += $ages['impressions'];

                                     $spendarray['13-17s'] += $ages['spend'];

                                }

                                if($ages['age'] == '18-24'){

                                    $impressionarray['18-24i'] += $ages['impressions'];

                                     $spendarray['18-24s'] += $ages['spend'];

                                }

                                if($ages['age'] == '25-34'){

                                    $impressionarray['25-34i'] += $ages['impressions'];

                                     $spendarray['25-34s'] += $ages['spend'];

                                }

                                if($ages['age'] == '35-44'){

                                    $impressionarray['35-44i'] += $ages['impressions'];

                                     $spendarray['35-44s'] += $ages['spend'];

                                }

                                if($ages['age'] == '45-54'){

                                    $impressionarray['45-54i'] += $ages['impressions'];

                                     $spendarray['45-54s'] += $ages['spend'];

                                }

                                if($ages['age'] == '55-64'){

                                    $impressionarray['55-64i'] += $ages['impressions'];

                                     $spendarray['55-64s'] += $ages['spend'];

                                }

                                if($ages['age'] == '65+'){

                                    $impressionarray['65+i'] += $ages['impressions'];

                                     $spendarray['65+s'] += $ages['spend'];

                                }



                            }

                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;

                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;

                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;

                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;

                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;

                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;

                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;

                            /*echo "<pre>";

                        print_r($res);

                        echo "</pre>";*/

                        

                            foreach ($res as $rs) {

                                $resultArray1[$rs['gender']]['range'] = $rs['range'];

                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];

                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];



                                if (array_key_exists($rs['range'], $resultArray)) {

                                    $resultArray[$rs['range']] = $resultArray1;

                                } else {

                                    $resultArray[$rs['range']] = array();

                                    $resultArray[$rs['range']] = $resultArray1;

                                }

                            }

                        }

                        /*echo "<pre>";

                        print_r($resultArray);

                        echo "</pre>";

                        exit;*/

                        $tempArr = array();

                        foreach ($resultArray as $k => $value) {

                            $tempStr1 = '0';

                            foreach ($value as $k1 => $value1) {

                                $tempStr1 += $value1['impressions'];

                            }

                            $tempArr[$k] = $tempStr1;

                        }



                        if (!array_key_exists('13-17', $resultArray)) {

                            $tempArr['13-17'] = 0;

                        }

                        if (!array_key_exists('18-24', $resultArray)) {

                            $tempArr['18-24'] = 0;

                        }

                        if (!array_key_exists('25-34', $resultArray)) {

                            $tempArr['25-34'] = 0;

                        }

                        if (!array_key_exists('35-44', $resultArray)) {

                            $tempArr['35-44'] = 0;

                        }

                        if (!array_key_exists('45-54', $resultArray)) {

                            $tempArr['45-54'] = 0;

                        }

                        if (!array_key_exists('55-64', $resultArray)) {

                            $tempArr['55-64'] = 0;

                        }

                        if (!array_key_exists('65+', $resultArray)) {

                            $tempArr['65+'] = 0;

                        }



                        /*print_r($impressionarray);

                        print_r($spendarray);

                        exit;*/

                        $returnmaindata .= '||||||';

                        $iag=1;



                        $returnmaindata .= '<tr>

                        <td>13 - 17';

                            $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['13-17']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>18 - 24';

                            $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['18-24']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>25 - 34';

                           $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['25-34']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>35 - 44';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['35-44']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>45 - 54';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['45-54']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>55 - 65';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['55-64']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>65+';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['65+']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>

                    </tr>';

                         /*foreach ($tempArr as $k => $value) {

                            $agegroup_total += $value;

                            if()

                            $returnmaindata .= '<tr>

                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>

                            18 - 24</td>

                        <td>'.number_format($value).'</td>

                        <td>$0.67</td>

                        <td class="text-success">0.45 %</td>

                        <td>$60.3</td>

                    </tr>';

                            

                            $iag++;

                        }*/







                        // Gender started

                        

                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                            

                            $date_preset = "";

                            $dataArray = "";

                            

                            $genimpressionarray = array();

                            $genspendarray = array();



                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;

                            



                           $pageName = 'page_engagement';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            

                            /*echo "<pre>";

                            print_r($response);

                            echo "</pre>";

                            exit;*/



                            if (isset($response['data'])) {

                                foreach ($response['data'] as $genders) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($genders['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($genders['gender'] == "female") {

                                                    $female = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "male") {

                                                    $male = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "unknown") {

                                                    $other = $feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($genders['gender'] == "female") {

                                            $female = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "male") {

                                            $male = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "unknown") {

                                            $other = $genders[$pageName];

                                        }

                                    }



                                    if($genders['gender'] == 'male'){

                                        $genimpressionarray['malei'] += $genders['impressions'];

                                         $genspendarray['males'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'female'){

                                        $genimpressionarray['femalei'] += $genders['impressions'];

                                         $genspendarray['females'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'unknown'){

                                        $genimpressionarray['unknowni'] += $genders['impressions'];

                                         $genspendarray['unknowns'] += $genders['spend'];

                                    }

                                }

                            }

                            if(empty($genspendarray['males'])) $genspendarray['males']=0;

                            if(empty($genspendarray['females'])) $genspendarray['females']=0;

                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;

                            $returnmaindata .= '||||||';

                    $returnmaindata .= '<tr>

                        <td>Men';

                           $returnmaindata .= '</td>

                        <td>'.$male.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Women';

                        $returnmaindata .= '</td>

                        <td>'.$female.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Other';

                        $returnmaindata .= '</td>

                        <td>'.$other.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>

                    </tr>';





                            //adplacement started

                            $desktop = 0;

                            $mobile = 0;

                            $other = 0;

                            $total = 0;

                            $dataArray = 0;

                            

                            $date_preset = "";



                            $placeimpressionarray = array();

                            $placespendarray = array();



                            

                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;

                            

                            $res = array();

                            $resultArray = array();



                            $pageName = 'page_engagement';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            //$this->getPrintResult($response);

            /*                 echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $feeds) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($feeds['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $desktop = $feeds1['value'];

                                                    $total += $feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $mobile = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "instagram") {

                                                    $instant_article = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "audience_network") {

                                                    $mobile_external_only = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                                    $right_hand = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $desktop = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $mobile = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $instant_article = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $mobile_external_only = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $right_hand = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                    }

                                    

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];

                                             $placespendarray['desktops'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];

                                             $placespendarray['mobiles'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];

                                             $placespendarray['instagrams'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];

                                             $placespendarray['audnetworks'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];

                                             $placespendarray['rhss'] += $feeds['spend'];

                                        }



                                }

                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;

                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;

                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;

                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;

                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;

                                if ($desktop > 0) {

                                    $desktopPer = round(($desktop / $total) * 100);

                                }

                                if ($mobile > 0) {

                                    $mobilePer = round(($mobile / $total) * 100);

                                }

                                if ($instant_article > 0) {

                                    $instant_articlePer = round(($instant_article / $total) * 100);

                                }

                                if ($mobile_external_only > 0) {

                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);

                                }

                                if ($right_hand > 0) {

                                    $right_handPer = round(($right_hand / $total) * 100);

                                }

                            }

                           /* echo "<pre>";

            print_r($placeimpressionarray);

            print_r($placespendarray);

            echo "</pre>";

            exit;*/

                            $returnmaindata .= '||||||';

                            $returnmaindata .= '<tr>

                        <td>Desktop Newsfeed';

                        $returnmaindata .= '</td>

                        <td>'.number_format($desktop).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Mobile Newsfeed';

                        $returnmaindata .= '

                        </td>

                        <td>'.number_format($mobile).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Instagram Feed';

                        $returnmaindata .= '</td>

                        <td>'.number_format($instant_article).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Audience Network';

                        $returnmaindata .= '</td>

                        <td>'.number_format($mobile_external_only).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Desktop Right Side';

                        $returnmaindata .= '</td>

                        <td>'.number_format($right_hand).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>

                    </tr>';





                            //country started



                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                           $tempStr = '';

                            $date_preset = "";

                            $dataArray = "";

                            

                                $date_preset = "date_preset=lifetime&";

                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;

                            



                            $res = array();

                            $resultArray = array();



                            $pageName = 'page_engagement';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

/*echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $ages) {

                                    if(empty($ages['spend'])) $ages['spend']=0;

                                    $result = $this->Users_model->getCountryName($ages['country']);



                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($ages['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                $countrytotal += (int)$feeds1['value'];

                                                $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($feeds1['value']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';

                                            }

                                        }

                                    }

                                    else{

                                        $countrytotal += (int)$ages[$pageName];

                                        $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($ages[$pageName]).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';

                                    }

                                }

                            }

                            $returnmaindata .= '||||||';

                            $returnmaindata .= $tempStr;



                }

                elseif($campaignObjectivce == 'PAGE_LIKES'){

                    $conversionval = 0;

                    $costperconversion = 0;

                    $resultrate = 0;

                    foreach($response['insights']['data'][0]['actions'] as $row){

                        if($row['action_type'] == 'like'){

                            $conversionval = $row['value'];

                        }

                    }

                   // $conversionval = $response['insights']['data'][0]['inline_link_clicks'];

                    if($response['insights']['data'][0]['spend']>0){

                        $costperconversion1 = 0;

                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;

                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');

                    }

                    if($response['insights']['data'][0]['impressions']>0){

                        $resultrate1 = 0;

                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;

                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";





                    }

                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />

                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>

                            <br class="hidden-lg">Total Results</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>

                            <br class="hidden-lg">Cost Per Result</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />

                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>

                            <br class="hidden-lg">Result Rate</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>

                            <br class="hidden-lg">Total Spent</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>

                            Best Performing</td>';





                     // agegroup started

                     

                        $female = 0;

                        $male = 0;

                        $other = 0;

                        $total = 0;

                        

                        $date_preset = "";

                        $dataArray = "";

                        

                        $date_preset = "date_preset=lifetime&";

                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;

                        

                        #echo $url;exit;

                        $res = array();

                        $resultArray = array();

                        $impressionarray = array();

                        $spendarray = array();

                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';

                        if($pageName == 'adddsReport'){

                            $pageName = 'impressions';

                        }*/

                        $pageName = 'like';

                        $ch = curl_init($url);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result = curl_exec($ch);

                        curl_close($ch);

                        $response = json_decode($result, true);

                        /*echo "<pre>";

                        print_r($response);

                        echo "</pre>";*/

                        //exit;

                        if (isset($response['data'])) {

                            foreach ($response['data'] as $ages) {

                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                    foreach ($ages['actions'] as $feeds1) {

                                        if ($feeds1['action_type'] == $pageName) {

                                            $res[] = array(

                                                "range" => $ages['age'],

                                                "inline_link_clicks" => $ages['inline_link_clicks'],

                                                "impressions" => $feeds1['value'],

                                                "gender" => $ages['gender'],

                                                "original"

                                            );

                                            $total += $ages['inline_link_clicks'];

                                        }

                                    }

                                }

                                else{

                                    $res[] = array(

                                        "range" => $ages['age'],

                                        "inline_link_clicks" => $ages['inline_link_clicks'],

                                        "impressions" => $ages[$pageName],

                                        "gender" => $ages['gender']

                                    );

                                    $total += $ages['inline_link_clicks'];

                                }

                                if($ages['age'] == '13-17'){

                                    $impressionarray['13-17i'] += $ages['impressions'];

                                     $spendarray['13-17s'] += $ages['spend'];

                                }

                                if($ages['age'] == '18-24'){

                                    $impressionarray['18-24i'] += $ages['impressions'];

                                     $spendarray['18-24s'] += $ages['spend'];

                                }

                                if($ages['age'] == '25-34'){

                                    $impressionarray['25-34i'] += $ages['impressions'];

                                     $spendarray['25-34s'] += $ages['spend'];

                                }

                                if($ages['age'] == '35-44'){

                                    $impressionarray['35-44i'] += $ages['impressions'];

                                     $spendarray['35-44s'] += $ages['spend'];

                                }

                                if($ages['age'] == '45-54'){

                                    $impressionarray['45-54i'] += $ages['impressions'];

                                     $spendarray['45-54s'] += $ages['spend'];

                                }

                                if($ages['age'] == '55-64'){

                                    $impressionarray['55-64i'] += $ages['impressions'];

                                     $spendarray['55-64s'] += $ages['spend'];

                                }

                                if($ages['age'] == '65+'){

                                    $impressionarray['65+i'] += $ages['impressions'];

                                     $spendarray['65+s'] += $ages['spend'];

                                }



                            }

                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;

                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;

                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;

                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;

                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;

                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;

                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;

                            /*echo "<pre>";

                        print_r($res);

                        echo "</pre>";*/

                        

                            foreach ($res as $rs) {

                                $resultArray1[$rs['gender']]['range'] = $rs['range'];

                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];

                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];



                                if (array_key_exists($rs['range'], $resultArray)) {

                                    $resultArray[$rs['range']] = $resultArray1;

                                } else {

                                    $resultArray[$rs['range']] = array();

                                    $resultArray[$rs['range']] = $resultArray1;

                                }

                            }

                        }

                        /*echo "<pre>";

                        print_r($resultArray);

                        echo "</pre>";

                        exit;*/

                        $tempArr = array();

                        foreach ($resultArray as $k => $value) {

                            $tempStr1 = '0';

                            foreach ($value as $k1 => $value1) {

                                $tempStr1 += $value1['impressions'];

                            }

                            $tempArr[$k] = $tempStr1;

                        }



                        if (!array_key_exists('13-17', $resultArray)) {

                            $tempArr['13-17'] = 0;

                        }

                        if (!array_key_exists('18-24', $resultArray)) {

                            $tempArr['18-24'] = 0;

                        }

                        if (!array_key_exists('25-34', $resultArray)) {

                            $tempArr['25-34'] = 0;

                        }

                        if (!array_key_exists('35-44', $resultArray)) {

                            $tempArr['35-44'] = 0;

                        }

                        if (!array_key_exists('45-54', $resultArray)) {

                            $tempArr['45-54'] = 0;

                        }

                        if (!array_key_exists('55-64', $resultArray)) {

                            $tempArr['55-64'] = 0;

                        }

                        if (!array_key_exists('65+', $resultArray)) {

                            $tempArr['65+'] = 0;

                        }



                        /*print_r($impressionarray);

                        print_r($spendarray);

                        exit;*/

                        $returnmaindata .= '||||||';

                        $iag=1;



                        $returnmaindata .= '<tr>

                        <td>13 - 17';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['13-17']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>18 - 24';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['18-24']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>25 - 34';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['25-34']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>35 - 44';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['35-44']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>45 - 54';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['45-54']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>55 - 65';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['55-64']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>65+';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['65+']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>

                    </tr>';

                         /*foreach ($tempArr as $k => $value) {

                            $agegroup_total += $value;

                            if()

                            $returnmaindata .= '<tr>

                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>

                            18 - 24</td>

                        <td>'.number_format($value).'</td>

                        <td>$0.67</td>

                        <td class="text-success">0.45 %</td>

                        <td>$60.3</td>

                    </tr>';

                            

                            $iag++;

                        }*/







                        // Gender started

                        

                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                            

                            $date_preset = "";

                            $dataArray = "";

                            

                            $genimpressionarray = array();

                            $genspendarray = array();



                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;

                            



                           $pageName = 'like';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            

                            /*echo "<pre>";

                            print_r($response);

                            echo "</pre>";

                            exit;*/



                            if (isset($response['data'])) {

                                foreach ($response['data'] as $genders) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($genders['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($genders['gender'] == "female") {

                                                    $female = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "male") {

                                                    $male = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "unknown") {

                                                    $other = $feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($genders['gender'] == "female") {

                                            $female = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "male") {

                                            $male = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "unknown") {

                                            $other = $genders[$pageName];

                                        }

                                    }



                                    if($genders['gender'] == 'male'){

                                        $genimpressionarray['malei'] += $genders['impressions'];

                                         $genspendarray['males'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'female'){

                                        $genimpressionarray['femalei'] += $genders['impressions'];

                                         $genspendarray['females'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'unknown'){

                                        $genimpressionarray['unknowni'] += $genders['impressions'];

                                         $genspendarray['unknowns'] += $genders['spend'];

                                    }

                                }

                            }

                            if(empty($genspendarray['males'])) $genspendarray['males']=0;

                            if(empty($genspendarray['females'])) $genspendarray['females']=0;

                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;

                            $returnmaindata .= '||||||';

                    $returnmaindata .= '<tr>

                        <td>Men';

                        $returnmaindata .= '</td>

                        <td>'.$male.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Women';

                        $returnmaindata .= '</td>

                        <td>'.$female.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Other';

                        $returnmaindata .= '</td>

                        <td>'.$other.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>

                    </tr>';





                            //adplacement started

                            $desktop = 0;

                            $mobile = 0;

                            $other = 0;

                            $total = 0;

                            $dataArray = 0;

                            

                            $date_preset = "";



                            $placeimpressionarray = array();

                            $placespendarray = array();



                            

                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;

                            

                            $res = array();

                            $resultArray = array();



                            $pageName = 'like';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            //$this->getPrintResult($response);

            /*                 echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $feeds) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($feeds['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $desktop = $feeds1['value'];

                                                    $total += $feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $mobile = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "instagram") {

                                                    $instant_article = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "audience_network") {

                                                    $mobile_external_only = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                                    $right_hand = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $desktop = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $mobile = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $instant_article = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $mobile_external_only = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $right_hand = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                    }

                                    

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];

                                             $placespendarray['desktops'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];

                                             $placespendarray['mobiles'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];

                                             $placespendarray['instagrams'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];

                                             $placespendarray['audnetworks'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];

                                             $placespendarray['rhss'] += $feeds['spend'];

                                        }



                                }

                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;

                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;

                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;

                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;

                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;

                                if ($desktop > 0) {

                                    $desktopPer = round(($desktop / $total) * 100);

                                }

                                if ($mobile > 0) {

                                    $mobilePer = round(($mobile / $total) * 100);

                                }

                                if ($instant_article > 0) {

                                    $instant_articlePer = round(($instant_article / $total) * 100);

                                }

                                if ($mobile_external_only > 0) {

                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);

                                }

                                if ($right_hand > 0) {

                                    $right_handPer = round(($right_hand / $total) * 100);

                                }

                            }

                           /* echo "<pre>";

            print_r($placeimpressionarray);

            print_r($placespendarray);

            echo "</pre>";

            exit;*/

                            $returnmaindata .= '||||||';

                            $returnmaindata .= '<tr>

                        <td>Desktop Newsfeed';

                        $returnmaindata .= '</td>

                        <td>'.number_format($desktop).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Mobile Newsfeed';

                        $returnmaindata .= '

                        </td>

                        <td>'.number_format($mobile).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Instagram Feed';

                        $returnmaindata .= '</td>

                        <td>'.number_format($instant_article).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Audience Network';

                        $returnmaindata .= '</td>

                        <td>'.number_format($mobile_external_only).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Desktop Right Side';

                        $returnmaindata .= '</td>

                        <td>'.number_format($right_hand).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>

                    </tr>';





                            //country started



                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                           $tempStr = '';

                            $date_preset = "";

                            $dataArray = "";

                            

                                $date_preset = "date_preset=lifetime&";

                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;

                            



                            $res = array();

                            $resultArray = array();



                            $pageName = 'like';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

/*echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $ages) {

                                    if(empty($ages['spend'])) $ages['spend']=0;

                                    $result = $this->Users_model->getCountryName($ages['country']);



                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){

                                        foreach ($ages['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                $countrytotal += (int)$feeds1['value'];

                                                $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($feeds1['value']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';

                                            }

                                        }

                                    }

                                    else{

                                        $countrytotal += (int)$ages[$pageName];

                                        $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($ages[$pageName]).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';

                                    }

                                }

                            }

                            $returnmaindata .= '||||||';

                            $returnmaindata .= $tempStr;



                }

                elseif($campaignObjectivce == 'LEAD_GENERATION'){

                        $conversionval = 0;

                    $costperconversion = 0;

                    $resultrate = 0;

                    foreach($response['insights']['data'][0]['actions'] as $row){

                        if($row['action_type'] == 'leadgen.other'){

                            $conversionval = $row['value'];

                        }

                    }

                    



                    if($response['insights']['data'][0]['spend']>0){

                        $costperconversion1 = 0;

                        $costperconversion1 = $response['insights']['data'][0]['spend']/$conversionval;

                        $costperconversion = $this->session->userdata('cur_currency') . number_format($costperconversion1, 2, '.', ',');

                    }

                    if($response['insights']['data'][0]['impressions']>0){

                        $resultrate1 = 0;

                        $resultrate1 = $conversionval/$response['insights']['data'][0]['impressions']*100;

                        $resultrate = round(number_format($resultrate1, 3, '.', ','),2)."%";





                    }

                    $returnmaindata .= '<td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/total-result.png" />

                            <span class="text-primary" style="font-size:10px;">'.number_format($conversionval).'</span>

                            <br class="hidden-lg">Total Results</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/employee.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$costperconversion.'</span>

                            <br class="hidden-lg">Cost Per Result</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/percentage.png" />

                            <span class="text-primary" style="font-size:10px;">'.$resultrate.'</span>

                            <br class="hidden-lg">Result Rate</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/money-bag.png"/>

                            <span class="text-primary" style="font-size:10px;">'.$this->session->userdata('cur_currency').number_format($response['insights']['data'][0]['spend'], 2, '.', ',').'</span>

                            <br class="hidden-lg">Total Spent</td>

                        <td style="font-size:10px;"><img src="'.$this->config->item('assets').'newdesign/images/best.png"/>

                            Best Performing</td>';





                     // agegroup started

                     

                        $female = 0;

                        $male = 0;

                        $other = 0;

                        $total = 0;

                        

                        $date_preset = "";

                        $dataArray = "";

                        

                        $date_preset = "date_preset=lifetime&";

                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;

                        

                        #echo $url;exit;

                        $res = array();

                        $resultArray = array();

                        $impressionarray = array();

                        $spendarray = array();

                        /*$pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';

                        if($pageName == 'adddsReport'){

                            $pageName = 'impressions';

                        }*/

                        $pageName = 'leadgen.other';

                        $ch = curl_init($url);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result = curl_exec($ch);

                        curl_close($ch);

                        $response = json_decode($result, true);

                        /*echo "<pre>";

                        print_r($response);

                        echo "</pre>";*/

                        //exit;

                        if (isset($response['data'])) {

                            foreach ($response['data'] as $ages) {

                                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){

                                    foreach ($ages['actions'] as $feeds1) {

                                        if ($feeds1['action_type'] == $pageName) {

                                            $res[] = array(

                                                "range" => $ages['age'],

                                                "inline_link_clicks" => $ages['inline_link_clicks'],

                                                "impressions" => $feeds1['value'],

                                                "gender" => $ages['gender'],

                                                "original"

                                            );

                                            $total += $ages['inline_link_clicks'];

                                        }

                                    }

                                }

                                else{

                                    $res[] = array(

                                        "range" => $ages['age'],

                                        "inline_link_clicks" => $ages['inline_link_clicks'],

                                        "impressions" => $ages[$pageName],

                                        "gender" => $ages['gender']

                                    );

                                    $total += $ages['inline_link_clicks'];

                                }

                                if($ages['age'] == '13-17'){

                                    $impressionarray['13-17i'] += $ages['impressions'];

                                     $spendarray['13-17s'] += $ages['spend'];

                                }

                                if($ages['age'] == '18-24'){

                                    $impressionarray['18-24i'] += $ages['impressions'];

                                     $spendarray['18-24s'] += $ages['spend'];

                                }

                                if($ages['age'] == '25-34'){

                                    $impressionarray['25-34i'] += $ages['impressions'];

                                     $spendarray['25-34s'] += $ages['spend'];

                                }

                                if($ages['age'] == '35-44'){

                                    $impressionarray['35-44i'] += $ages['impressions'];

                                     $spendarray['35-44s'] += $ages['spend'];

                                }

                                if($ages['age'] == '45-54'){

                                    $impressionarray['45-54i'] += $ages['impressions'];

                                     $spendarray['45-54s'] += $ages['spend'];

                                }

                                if($ages['age'] == '55-64'){

                                    $impressionarray['55-64i'] += $ages['impressions'];

                                     $spendarray['55-64s'] += $ages['spend'];

                                }

                                if($ages['age'] == '65+'){

                                    $impressionarray['65+i'] += $ages['impressions'];

                                     $spendarray['65+s'] += $ages['spend'];

                                }



                            }

                            if(empty($spendarray['13-17s'])) $spendarray['13-17s']=0;

                            if(empty($spendarray['18-24s'])) $spendarray['18-24s']=0;

                            if(empty($spendarray['25-34s'])) $spendarray['25-34s']=0;

                            if(empty($spendarray['35-44s'])) $spendarray['35-44s']=0;

                            if(empty($spendarray['45-54s'])) $spendarray['45-54s']=0;

                            if(empty($spendarray['55-64s'])) $spendarray['55-64s']=0;

                            if(empty($spendarray['65+s'])) $spendarray['65+s']=0;

                            /*echo "<pre>";

                        print_r($res);

                        echo "</pre>";*/

                        

                            foreach ($res as $rs) {

                                $resultArray1[$rs['gender']]['range'] = $rs['range'];

                                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];

                                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];



                                if (array_key_exists($rs['range'], $resultArray)) {

                                    $resultArray[$rs['range']] = $resultArray1;

                                } else {

                                    $resultArray[$rs['range']] = array();

                                    $resultArray[$rs['range']] = $resultArray1;

                                }

                            }

                        }

                        /*echo "<pre>";

                        print_r($resultArray);

                        echo "</pre>";

                        exit;*/

                        $tempArr = array();

                        foreach ($resultArray as $k => $value) {

                            $tempStr1 = '0';

                            foreach ($value as $k1 => $value1) {

                                $tempStr1 += $value1['impressions'];

                            }

                            $tempArr[$k] = $tempStr1;

                        }



                        if (!array_key_exists('13-17', $resultArray)) {

                            $tempArr['13-17'] = 0;

                        }

                        if (!array_key_exists('18-24', $resultArray)) {

                            $tempArr['18-24'] = 0;

                        }

                        if (!array_key_exists('25-34', $resultArray)) {

                            $tempArr['25-34'] = 0;

                        }

                        if (!array_key_exists('35-44', $resultArray)) {

                            $tempArr['35-44'] = 0;

                        }

                        if (!array_key_exists('45-54', $resultArray)) {

                            $tempArr['45-54'] = 0;

                        }

                        if (!array_key_exists('55-64', $resultArray)) {

                            $tempArr['55-64'] = 0;

                        }

                        if (!array_key_exists('65+', $resultArray)) {

                            $tempArr['65+'] = 0;

                        }



                        /*print_r($impressionarray);

                        print_r($spendarray);

                        exit;*/

                        $returnmaindata .= '||||||';

                        $iag=1;



                        $returnmaindata .= '<tr>

                        <td>13 - 17';

                            $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['13-17']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s']/$tempArr['13-17'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['13-17']/$impressionarray['13-17i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['13-17s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>18 - 24';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['18-24']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s']/$tempArr['18-24'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['18-24']/$impressionarray['18-24i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['18-24s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>25 - 34';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['25-34']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s']/$tempArr['25-34'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['25-34']/$impressionarray['25-34i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['25-34s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>35 - 44';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['35-44']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s']/$tempArr['35-44'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['35-44']/$impressionarray['35-44i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['35-44s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>45 - 54';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['45-54']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s']/$tempArr['45-54'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['45-54']/$impressionarray['45-54i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['45-54s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>55 - 65';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['55-64']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s']/$tempArr['55-64'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['55-64']/$impressionarray['55-64i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['55-64s'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>65+';

                        $returnmaindata .= '</td>

                        <td>'.number_format($tempArr['65+']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s']/$tempArr['65+'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($tempArr['65+']/$impressionarray['65+i']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($spendarray['65+s'], 2, '.', ',').'</td>

                    </tr>';

                         /*foreach ($tempArr as $k => $value) {

                            $agegroup_total += $value;

                            if()

                            $returnmaindata .= '<tr>

                        <td><img src="'.$this->config->item('assets').'newdesign/images/n'.$iag.'.png" height="18" width="18"/>

                            18 - 24</td>

                        <td>'.number_format($value).'</td>

                        <td>$0.67</td>

                        <td class="text-success">0.45 %</td>

                        <td>$60.3</td>

                    </tr>';

                            

                            $iag++;

                        }*/







                        // Gender started

                        

                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                            

                            $date_preset = "";

                            $dataArray = "";

                            

                            $genimpressionarray = array();

                            $genspendarray = array();



                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;

                            



                           $pageName = 'leadgen.other';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            

                            /*echo "<pre>";

                            print_r($response);

                            echo "</pre>";

                            exit;*/



                            if (isset($response['data'])) {

                                foreach ($response['data'] as $genders) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){

                                        foreach ($genders['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($genders['gender'] == "female") {

                                                    $female = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "male") {

                                                    $male = $feeds1['value'];

                                                    $total+=$feeds1['value'];

                                                } else if ($genders['gender'] == "unknown") {

                                                    $other = $feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($genders['gender'] == "female") {

                                            $female = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "male") {

                                            $male = $genders[$pageName];

                                            $total+=$genders[$pageName];

                                        } else if ($genders['gender'] == "unknown") {

                                            $other = $genders[$pageName];

                                        }

                                    }



                                    if($genders['gender'] == 'male'){

                                        $genimpressionarray['malei'] += $genders['impressions'];

                                         $genspendarray['males'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'female'){

                                        $genimpressionarray['femalei'] += $genders['impressions'];

                                         $genspendarray['females'] += $genders['spend'];

                                    }

                                    if($genders['gender'] == 'unknown'){

                                        $genimpressionarray['unknowni'] += $genders['impressions'];

                                         $genspendarray['unknowns'] += $genders['spend'];

                                    }

                                }

                            }

                            if(empty($genspendarray['males'])) $genspendarray['males']=0;

                            if(empty($genspendarray['females'])) $genspendarray['females']=0;

                            if(empty($genspendarray['unknowns'])) $genspendarray['unknowns']=0;

                            $returnmaindata .= '||||||';

                    $returnmaindata .= '<tr>

                        <td>Men';

                        $returnmaindata .= '</td>

                        <td>'.$male.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males']/$male, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($male/$genimpressionarray['malei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['males'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Women';

                        $returnmaindata .= '</td>

                        <td>'.$female.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females']/$female, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($female/$genimpressionarray['femalei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['females'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Other';

                        $returnmaindata .= '</td>

                        <td>'.$other.'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns']/$other, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($other/$genimpressionarray['unknowni']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($genspendarray['unknowns'], 2, '.', ',').'</td>

                    </tr>';





                            //adplacement started

                            $desktop = 0;

                            $mobile = 0;

                            $other = 0;

                            $total = 0;

                            $dataArray = 0;

                            

                            $date_preset = "";



                            $placeimpressionarray = array();

                            $placespendarray = array();



                            

                            $date_preset = "date_preset=lifetime&";

                            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;

                            

                            $res = array();

                            $resultArray = array();



                            $pageName = 'leadgen.other';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

                            //$this->getPrintResult($response);

            /*                 echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $feeds) {

                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){

                                        foreach ($feeds['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $desktop = $feeds1['value'];

                                                    $total += $feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                                    $mobile = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "instagram") {

                                                    $instant_article = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['publisher_platform'] == "audience_network") {

                                                    $mobile_external_only = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                                if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                                    $right_hand = $feeds1['value'];

                                                    $total +=$feeds1['value'];

                                                }

                                            }

                                        }

                                    }

                                    else{

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $desktop = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $mobile = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $instant_article = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $mobile_external_only = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $right_hand = $feeds[$pageName];

                                            $total +=$feeds[$pageName];

                                        }

                                    }

                                    

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['desktopi'] += $feeds['impressions'];

                                             $placespendarray['desktops'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {

                                            $placeimpressionarray['mobilei'] += $feeds['impressions'];

                                             $placespendarray['mobiles'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "instagram") {

                                            $placeimpressionarray['instagrami'] += $feeds['impressions'];

                                             $placespendarray['instagrams'] += $feeds['spend'];

                                        }

                                        if ($feeds['publisher_platform'] == "audience_network") {

                                            $placeimpressionarray['audnetworki'] += $feeds['impressions'];

                                             $placespendarray['audnetworks'] += $feeds['spend'];

                                        }

                                        if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {

                                            $placeimpressionarray['rhsi'] += $feeds['impressions'];

                                             $placespendarray['rhss'] += $feeds['spend'];

                                        }



                                }



                                if(empty($placespendarray['desktops'])) $placespendarray['desktops']=0;

                                if(empty($placespendarray['mobiles'])) $placespendarray['mobiles']=0;

                                if(empty($placespendarray['instagrams'])) $placespendarray['instagrams']=0;

                                if(empty($placespendarray['audnetworks'])) $placespendarray['audnetworks']=0;

                                if(empty($placespendarray['rhss'])) $placespendarray['rhss']=0;

                                if ($desktop > 0) {

                                    $desktopPer = round(($desktop / $total) * 100);

                                }

                                if ($mobile > 0) {

                                    $mobilePer = round(($mobile / $total) * 100);

                                }

                                if ($instant_article > 0) {

                                    $instant_articlePer = round(($instant_article / $total) * 100);

                                }

                                if ($mobile_external_only > 0) {

                                    $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);

                                }

                                if ($right_hand > 0) {

                                    $right_handPer = round(($right_hand / $total) * 100);

                                }

                            }

                           /* echo "<pre>";

            print_r($placeimpressionarray);

            print_r($placespendarray);

            echo "</pre>";

            exit;*/

                            $returnmaindata .= '||||||';

                            $returnmaindata .= '<tr>

                        <td>Desktop Newsfeed';

                        $returnmaindata .= '</td>

                        <td>'.number_format($desktop).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops']/$desktop, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($desktop/$placeimpressionarray['desktopi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['desktops'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Mobile Newsfeed';

                        $returnmaindata .= '

                        </td>

                        <td>'.number_format($mobile).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles']/$mobile, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile/$placeimpressionarray['mobilei']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['mobiles'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Instagram Feed';

                        $returnmaindata .= '</td>

                        <td>'.number_format($instant_article).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams']/$instant_article, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($instant_article/$placeimpressionarray['instagrami']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['instagrams'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Audience Network';

                        $returnmaindata .= '</td>

                        <td>'.number_format($mobile_external_only).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks']/$mobile_external_only, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($mobile_external_only/$placeimpressionarray['audnetworki']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['audnetworks'], 2, '.', ',').'</td>

                    </tr>

                    <tr>

                        <td>Desktop Right Side';

                        $returnmaindata .= '</td>

                        <td>'.number_format($right_hand).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss']/$right_hand, 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($right_hand/$placeimpressionarray['rhsi']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($placespendarray['rhss'], 2, '.', ',').'</td>

                    </tr>';





                            //country started



                            $female = 0;

                            $male = 0;

                            $other = 0;

                            $total = 0;

                           $tempStr = '';

                            $date_preset = "";

                            $dataArray = "";

                            

                                $date_preset = "date_preset=lifetime&";

                                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=5&access_token=" . $this->access_token;

                            



                            $res = array();

                            $resultArray = array();



                            $pageName = 'leadgen.other';



                            $ch = curl_init($url);

                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                            $result = curl_exec($ch);

                            curl_close($ch);

                            $response = json_decode($result, true);

/*echo "<pre>";

            print_r($response);

            echo "</pre>";

            exit;*/

                            

                            if (isset($response['data'])) {

                                foreach ($response['data'] as $ages) {

                                    if(empty($ages['spend'])) $ages['spend']=0;

                                    $result = $this->Users_model->getCountryName($ages['country']);



                                    if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'leadgen.other'){

                                        foreach ($ages['actions'] as $feeds1) {

                                            if ($feeds1['action_type'] == $pageName) {

                                                $countrytotal += (int)$feeds1['value'];

                                                $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($feeds1['value']).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$feeds1['value'], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($feeds1['value']/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';

                                            }

                                        }

                                    }

                                    else{

                                        $countrytotal += (int)$ages[$pageName];

                                        $tempStr .= ' <tr>

                        <td>'.strtoupper($result[0]->Country).'

                        </td>

                        <td>'.number_format($ages[$pageName]).'</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend']/$ages[$pageName], 2, '.', ',').'</td>

                        <td class="text-success">'.round(number_format($ages[$pageName]/$ages['impressions']*100, 3, '.', ','),2).' %</td>

                        <td>'.$this->session->userdata('cur_currency') . number_format($ages['spend'], 2, '.', ',').'</td>

                    </tr>';

                                                //$tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';

                                    }

                                }

                            }

                            $returnmaindata .= '||||||';

                            $returnmaindata .= $tempStr;

                }

            }

                    

            /*echo "<pre>";

            print_r($response);

            echo "</pre>";*/

            } catch (Exception $ex) {

                echo "error";

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $e->getMessage(),

                        "success" => ""

                    );

                }

                /*print_r($returnmaindata);

                exit;*/



            $this->load->library('Pdf');

            //$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);











            

            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);



// set document information

$pdf->SetCreator(PDF_CREATOR);

$pdf->SetAuthor('Muhammad Junaid Tufail');

$pdf->SetTitle('The Campaign Maker Analysis Report');

$pdf->SetSubject('The Campaign Maker Analysis Report');

$pdf->SetKeywords('AnalysisReport, CampaignMaker');



$pdf->setHeaderData($ln='', $lw=0, $ht='', $hs='<div style="background-color:#212F41;color:#ffffff;text-align:center;font-size:13px;line-height:300%;">Analysis Report for '.$this->input->get("camptitle",TRUE).'</div>', $tc=array(0,0,0), $lc=array(0,0,0));

// set header and footer fonts

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));



// set default monospaced font

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);



// set margins

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);



// set auto page breaks

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);





// ---------------------------------------------------------



// set font

$pdf->SetFont('dejavusans', '', 10);









// test custom bullet points for list





                    $phpvar = explode("|||||",$returnmaindata);



// add a page

$pdf->AddPage();



$html = <<<EOF

<style>

    img{

            font-size:10px;width:15px;height:15px;

        }

		th {

			font-weight:bold;

			text-align:center;

		}

		td{

			text-align:center;

		}

		tr {

			border-top: 2px solid #d8e5ee;

		}

		.heading{

				background-color: #fafbfc;

			border-top: 2px solid #d8e5ee;

			color: #8dabc4;

			font-size: 15px;

			height: 42px;

			letter-spacing: 3px;

			margin-bottom: 20px;

			padding: 8px 10px 11px 36px;

			text-transform: uppercase;

		}

</style>    

<div class="analysis-done" id="analysis-done" style="">

            <!--Head-->

            <div class="head-analysis table-responsive">

                <table class="table">

                    <thead>

                    <tr id="adsettotals">$phpvar[0]</tr>

                    </thead>

                </table>

            </div>

            <!--End Head-->



            <!--AGE GROUP ANALYSIS-->

            <div class="heading">AGE GROUP ANALYSIS</div>

			<div></div>

            <table class="table table-hover dataTable no-footer" id="analysistbl1" role="grid" aria-describedby="analysistbl1_info" style="width: 100%;" width="100%">

                    <thead>

                    <tr role="row"><th>AGE GROUP</th>

					<th>RESULTS</th>

					<th>COST PER RESULT</th>

					<th>RESULT RATE</th>

					<th>TOTAL SPENT</th>

					</tr>

                    </thead>

                    <tbody id="agegrouptotals">$phpvar[1]</tbody>

                </table>

            

            



            <!--GENDER ANALYSIS-->

			<div></div>

			<div class="heading">GENDER ANALYSIS</div>

			<div></div>

            

            <table class="table table-hover dataTable no-footer" id="analysistbl2" role="grid" aria-describedby="analysistbl2_info" style="width: 100%;" width="100%">

                    <thead>

                    <tr role="row">

					<th>GENDER</th>

					<th>RESULTS</th>

					<th>COST PER RESULT</th>

					<th>RESULT RATE</th>

					<th>TOTAL SPENT</th>

					</tr>

                    </thead>

                    <tbody id="gendertotals">$phpvar[2]</tbody>

                </table>

            <!--End GENDER ANALYSIS-->



            <!--AD PLACEMENT ANALYSIS-->

			<div></div>

            <div class="heading">AD PLACEMENT ANALYSIS</div>

			<div></div>

            <table class="table table-hover dataTable no-footer" id="analysistbl3" role="grid" aria-describedby="analysistbl3_info" style="width: 100%;" width="100%">

                    <thead>

                    <tr role="row">

					<th>PLACEMENT</th>

					<th>RESULTS</th>

					<th>COST PER RESULT</th>

					<th>RESULT RATE</th>

					<th>TOTAL SPENT</th></tr>

                    </thead>

                    <tbody id="adplacementtotals">$phpvar[3]</tbody>

                </table>

				

            <!--End AD PLACEMENT ANALYSIS-->



            <!--LOCATION ANALYSIS-->

			<div></div>

            <div class="heading">LOCATION ANALYSIS</div>

			<div></div>

            <table class="table table-hover dataTable no-footer" id="analysistbl4" role="grid" aria-describedby="analysistbl4_info" style="width: 100%;" width="100%">

                    <thead>

                    <tr role="row">

					<th>COUNTRY</th>

					<th>RESULTS</th>

					<th>COST PER RESULT</th>

					<th>RESULT RATE</th>

					<th>TOTAL SPENT</th>

					</tr>

                    </thead>

                    <tbody id="countrytotals">$phpvar[4]</tbody>

                </table>

                <!--End Analysis Buttons-->



            </div>

        </div>

EOF;



// output the HTML content

$pdf->writeHTML($html, true, false, true, false, '');



// ---------------------------------------------------------



//Close and output PDF document

$pdf->Output('Analysis_TCM.pdf', 'I');

            

            

            /*

            $pdf->SetTitle('Pdf Example');

            $pdf->SetHeaderMargin(30);

            $pdf->SetTopMargin(20);

            $pdf->setFooterMargin(20);

            $pdf->SetAutoPageBreak(true);

            $pdf->SetAuthor('Author');

            $pdf->SetDisplayMode('real', 'default');

            $pdf->Write(5, 'CodeIgniter TCPDF Integration');

            ob_clean();

            $pdf->Output('pdfexample.pdf', 'I');

            */

        }



        function testgenerateanalysispdf(){



            $this->load->library('Pdf');

            //$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

            

            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);



// set document information

$pdf->SetCreator(PDF_CREATOR);

$pdf->SetAuthor('Muhammad Junaid Tufail');

$pdf->SetTitle('CampaignMaker Analysis Report');

$pdf->SetSubject('Analysis Report');

$pdf->SetKeywords('AnalysisReport, CampaignMaker');



$pdf->setHeaderData($ln='', $lw=0, $ht='', $hs='<div style="background-color:#212F41;color:#ffffff;text-align:center;font-size:13px;line-height:300%;">Analysis Report</div>', $tc=array(0,0,0), $lc=array(0,0,0));

// set header and footer fonts

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));



// set default monospaced font

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);



// set margins

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);



// set auto page breaks

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);





// ---------------------------------------------------------



// set font

$pdf->SetFont('dejavusans', '', 10);









// test custom bullet points for list







// add a page

$pdf->AddPage();



$html = <<<EOF

<style>

    img{

            font-size:10px;width:15px;height:15px;

        }

        th {

            font-weight:bold;

            text-align:center;

        }

        td{

            text-align:center;

        }

        tr {

            border-top: 2px solid #d8e5ee;

        }

        .heading{

                background-color: #fafbfc;

            border-top: 2px solid #d8e5ee;

            color: #8dabc4;

            font-size: 15px;

            height: 42px;

            letter-spacing: 3px;

            margin-bottom: 20px;

            padding: 8px 10px 11px 36px;

            text-transform: uppercase;

        }

</style>    

<div class="analysis-done" id="analysis-done" style="">

            <!--Head-->

            <div class="head-analysis table-responsive">

                <table class="table">

                    <thead>

                    <tr id="adsettotals"><td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/total-result.png">

                            <span class="text-primary" style="font-size:10px;">13</span>

                            <br class="hidden-lg">Total Results</td>

                        <td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/employee.png">

                            <span class="text-primary" style="font-size:10px;">$3.54</span>

                            <br class="hidden-lg">Cost Per Result</td>

                        <td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/percentage.png">

                            <span class="text-primary" style="font-size:10px;">0.13%</span>

                            <br class="hidden-lg">Result Rate</td>

                        <td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/money-bag.png">

                            <span class="text-primary" style="font-size:10px;">$46.01</span>

                            <br class="hidden-lg">Total Spent</td>

                        <td style="font-size:10px;"><img src="https://thecampaignmaker.com/tcm3/assets/newdesign/images/best.png">

                            Best Performing</td></tr>

                    </thead>

                </table>

            </div>

            <!--End Head-->



            <!--AGE GROUP ANALYSIS-->

            <div class="heading">AGE GROUP ANALYSIS</div>

            <div></div>

            <table class="table table-hover dataTable no-footer" id="analysistbl1" role="grid" aria-describedby="analysistbl1_info" style="width: 100%;" width="100%">

                    <thead>

                    <tr role="row"><th>AGE GROUP</th>

                    <th>RESULTS</th>

                    <th>COST PER RESULT</th>

                    <th>RESULT RATE</th>

                    <th>TOTAL SPENT</th>

                    </tr>

                    </thead>

                    <tbody id="agegrouptotals">

                    

                    

                    

                    

                    

                    <tr role="row" class="odd">

                        <td class="sorting_1">

                            13 - 17</td>

                        <td>0</td>

                        <td>$0.00</td>

                        <td class="text-success">0 %</td>

                        <td>$0.00</td>

                    </tr><tr role="row" class="even">

                        <td class="sorting_1">

                            18 - 24</td>

                        <td>0</td>

                        <td>$0.00</td>

                        <td class="text-success">0 %</td>

                        <td>$3.54</td>

                    </tr><tr role="row" class="odd">

                        <td class="sorting_1">

                            25 - 34</td>

                        <td>7</td>

                        <td>$3.70</td>

                        <td class="text-success">0.11 %</td>

                        <td>$25.91</td>

                    </tr><tr role="row" class="even">

                        <td class="sorting_1">

                            35 - 44</td>

                        <td>2</td>

                        <td>$5.40</td>

                        <td class="text-success">0.11 %</td>

                        <td>$10.80</td>

                    </tr><tr role="row" class="odd">

                        <td class="sorting_1">

                            

                            45 - 54</td>

                        <td>2</td>

                        <td>$1.92</td>

                        <td class="text-success">0.42 %</td>

                        <td>$3.84</td>

                    </tr><tr role="row" class="even">

                        <td class="sorting_1">

                            55 - 65</td>

                        <td>0</td>

                        <td>$0.00</td>

                        <td class="text-success">0 %</td>

                        <td>$0.42</td>

                    </tr><tr role="row" class="odd">

                        <td class="sorting_1">

                            65+<input id="bestagerange" name="bestagerange" value="65+" type="hidden"></td>

                        <td>2</td>

                        <td>$0.75</td>

                        <td class="text-success">1.75 %</td>

                        <td>$1.50</td>

                    </tr></tbody>

                </table>

            

            



            <!--GENDER ANALYSIS-->

            <div></div>

            <div class="heading">GENDER ANALYSIS</div>

            <div></div>

            

            <table class="table table-hover dataTable no-footer" id="analysistbl2" role="grid" aria-describedby="analysistbl2_info" style="width: 100%;" width="100%">

                    <thead>

                    <tr role="row">

                    <th>GENDER</th>

                    <th>RESULTS</th>

                    <th>COST PER RESULT</th>

                    <th>RESULT RATE</th>

                    <th>TOTAL SPENT</th>

                    </tr>

                    </thead>

                    <tbody id="gendertotals">

                    

                    <tr role="row" class="odd">

                        <td class="sorting_1">Men</td>

                        <td>13</td>

                        <td>$3.42</td>

                        <td class="text-success">0.14 %</td>

                        <td>$44.40</td>

                    </tr><tr role="row" class="even">

                        <td class="sorting_1">Women</td>

                        <td>0</td>

                        <td>$0.00</td>

                        <td class="text-success">0 %</td>

                        <td>$1.28</td>

                    </tr><tr role="row" class="odd">

                        <td class="sorting_1">Other</td>

                        <td>0</td>

                        <td>$0.00</td>

                        <td class="text-success">0 %</td>

                        <td>$0.33</td>

                    </tr></tbody>

                </table>

            <!--End GENDER ANALYSIS-->



            <!--AD PLACEMENT ANALYSIS-->

            <div></div>

            <div class="heading">AD PLACEMENT ANALYSIS</div>

            <div></div>

            <table class="table table-hover dataTable no-footer" id="analysistbl3" role="grid" aria-describedby="analysistbl3_info" style="width: 100%;" width="100%">

                    <thead>

                    <tr role="row">

                    <th>PLACEMENT</th>

                    <th>RESULTS</th>

                    <th>COST PER RESULT</th>

                    <th>RESULT RATE</th>

                    <th>TOTAL SPENT</th></tr>

                    </thead>

                    <tbody id="adplacementtotals">

                    

                    

                    

                    <tr role="row" class="odd">

                        <td class="sorting_1">Audience Network</td>

                        <td>0</td>

                        <td>$0.00</td>

                        <td class="text-success">0 %</td>

                        <td>$0.23</td>

                    </tr><tr role="row" class="even">

                        <td class="sorting_1">Desktop Newsfeed</td>

                        <td>10</td>

                        <td>$2.24</td>

                        <td class="text-success">0.38 %</td>

                        <td>$22.37</td>

                    </tr><tr role="row" class="odd">

                        <td class="sorting_1">Desktop Right Side</td>

                        <td>0</td>

                        <td>$0.00</td>

                        <td class="text-success">0 %</td>

                        <td>$3.22</td>

                    </tr><tr role="row" class="even">

                        <td class="sorting_1">Instagram Feed</td>

                        <td>0</td>

                        <td>$0.00</td>

                        <td class="text-success">0 %</td>

                        <td>$2.09</td>

                    </tr><tr role="row" class="odd">

                        <td class="sorting_1">Mobile Newsfeed</td>

                        <td>3</td>

                        <td>$5.94</td>

                        <td class="text-success">0.12 %</td>

                        <td>$17.81</td>

                    </tr></tbody>

                </table>

                

            <!--End AD PLACEMENT ANALYSIS-->



            <!--LOCATION ANALYSIS-->

            <div></div>

            <div class="heading">LOCATION ANALYSIS</div>

            <div></div>

            <table class="table table-hover dataTable no-footer" id="analysistbl4" role="grid" aria-describedby="analysistbl4_info" style="width: 100%;" width="100%">

                    <thead>

                    <tr role="row">

                    <th>COUNTRY</th>

                    <th>RESULTS</th>

                    <th>COST PER RESULT</th>

                    <th>RESULT RATE</th>

                    <th>TOTAL SPENT</th>

                    </tr>

                    </thead>

                    <tbody id="countrytotals"><tr role="row" class="odd">

                        <td class="sorting_1">PHILIPPINES</td>

                        <td>3</td>

                        <td>$1.36</td>

                        <td class="text-success">0.32 %</td>

                        <td>$4.09</td>

                    </tr></tbody>

                </table>

                <!--End Analysis Buttons-->



            </div>

        </div>

EOF;



// output the HTML content

$pdf->writeHTML($html, true, false, true, false, '');



// ---------------------------------------------------------



//Close and output PDF document

$pdf->Output('Analysis_TCM.pdf', 'I');

            

            

            /*

            $pdf->SetTitle('Pdf Example');

            $pdf->SetHeaderMargin(30);

            $pdf->SetTopMargin(20);

            $pdf->setFooterMargin(20);

            $pdf->SetAutoPageBreak(true);

            $pdf->SetAuthor('Author');

            $pdf->SetDisplayMode('real', 'default');

            $pdf->Write(5, 'CodeIgniter TCPDF Integration');

            ob_clean();

            $pdf->Output('pdfexample.pdf', 'I');

            */

        }

		

	public function testbatchrequest2(){	

	/*	$ch = curl_init($this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/act_116232728/campaigns');

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt_array($ch, array(

            CURLOPT_RETURNTRANSFER => true,

            CURLOPT_POST => true,

            CURLOPT_POSTFIELDS => array(

                'access_token' => $this->access_token,

                'status' => 'ACTIVE',

                'objective' => 'CONVERSIONS',

                'name' => 'OfferAd Campaign2',

            )

        ));

        $response = curl_exec($ch);

        curl_close($ch);

        //print_r($response);

        $result = json_decode($response);

        print_r($result);

        exit;*/

        

     /*   

        $ch = curl_init($this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/act_116232728/adsets');

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt_array($ch, array(

            CURLOPT_RETURNTRANSFER => true,

            CURLOPT_POST => true,

            CURLOPT_POSTFIELDS => array(

                'access_token' => $this->access_token,

                'name' => 'OfferAd adset2',

                'lifetime_budget' => '6500000',

                'start_time' => '1512438548',

                'end_time' => '1536530400',

                'campaign_id' => '6101019761165',

                'is_autobid' => '1',

                'billing_event' => 'LINK_CLICKS',

                'optimization_goal' => 'LINK_CLICKS',

                'promoted_object' => '{"page_id":"1832198410328098","offer_id":"316381685536420"}',

                'targeting' => '{ 

    "age_max": 55, 

    "age_min": 25, 

    "genders": [0], 

    "geo_locations": {"countries":["US"]}, 

    "interests": [{"id":6003107902433,"name":"Association football (Soccer)"}] 

  }'

            )

        ));

        $response = curl_exec($ch);

        curl_close($ch);

        //print_r($response);

        $result = json_decode($response);

        print_r($result);

        exit;*/

        

        echo $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version'); exit;

        $ch = curl_init($this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/act_116232728/ads');

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt_array($ch, array(

            CURLOPT_RETURNTRANSFER => true,

            CURLOPT_POST => true,

            CURLOPT_POSTFIELDS => array(

                'access_token' => $this->access_token,

                'name' => 'OfferAd ad 2',

                'adset_id' => '6101020029965',

                'creative' => '{ 

    "name": "My Offer Ad Creative", 

    "object_story_spec": { 

      "link_data": { 

        "picture": "https://scontent.xx.fbcdn.net/v/t45.1600-4/26757537_6101019580365_2547782043165523968_n.png?oh=ae3160a49bb44a39858b0e3039e3a9ba&oe=5AF70406", 

        "link": "http://www.wptester.info", 

        "message": "Great Deal!", 

        "offer_id": "316381685536420" 

      }, 

      "page_id": "1832198410328098" 

    } 

  }'

            )

        ));

        $response = curl_exec($ch);

        curl_close($ch);

        //print_r($response);

        $result = json_decode($response);

        print_r($result);

        exit;

       

       

}

		

		function testbatchrequest(){

		    

		    $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/23842601197720160/ads?fields=id,name&access_token=" . $this->access_token;

        #echo $url;exit;

        



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

		    echo "<pre>";

				print_r($response);

				echo "</pre>";

                /*$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/?batch=['.urlencode('{ "method":"GET", "relative_url":"me"},{ "method":"GET", "relative_url":"act_104413260013985?fields=insights.date_preset(lifetime){frequency,date_start}"}').']&access_token=' . $this->access_token;

	

                

                #echo $url;exit;

                $ch = curl_init($url);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                curl_setopt($ch, CURLOPT_POST, 1);

                $result = curl_exec($ch);

				//print_r($result);

                curl_close($ch);

                $response = json_decode($result, true);

				echo "<pre>";

				print_r($response);

				echo "</pre>";*/



                /* $url3 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version').'/23842604154690160/?fields=name,end_time,daily_budget,targeting&access_token=' . $this->access_token;

                        //echo $url;exit;

                        $ch3 = curl_init($url3);

                        curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch3, CURLOPT_FOLLOWLOCATION, true);

                        $result3 = curl_exec($ch3);

                        curl_close($ch3);

                        $response3 = json_decode($result3, true);

                        echo "<pre>";

                    print_r($response3);

                    echo "</pre>";



*/

                    /*$postFields = array(

                    'access_token' => $this->access_token,

                );

                    $finalbudget = 2000/100;    

                    $postFields['daily_budget'] = $finalbudget/0.01;

                            

                        echo "<pre>";

                    print_r($postFields);

                    echo "</pre>";

                       

                        $ch = curl_init('https://graph.facebook.com/v2.9/23842604115680160');

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt_array($ch, array(

                            CURLOPT_RETURNTRANSFER => true,

                            CURLOPT_POST => true,

                            CURLOPT_POSTFIELDS => $postFields

                        ));



                        $response = curl_exec($ch);

                        curl_close($ch);

                        $result = json_decode($response);

                        

                        echo "<pre>";

                    print_r($result);

                    echo "</pre>";*/

		}  

		

		

		function spendtimeFrame($addSetId,$date_preset_value,$token){

		    

		                

                        $date_preset = "date_preset=".$date_preset_value."&";

                        

                        $url_without_breakdown = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['spend']&access_token=" . $token;

         

                        $ch = curl_init($url_without_breakdown);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_without_breakdown = curl_exec($ch);

                        curl_close($ch);

                       

                        $response_without_breakdown =  json_decode($result_without_breakdown, true);

                        

                       

                        $url_age = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['spend']&breakdowns=['age']&access_token=" . $token;

         

                        $ch = curl_init($url_age);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_age = curl_exec($ch);

                        curl_close($ch);

                       

                        $response_age =  json_decode($result_age, true);

                        

                          $url_gender = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['spend']&breakdowns=['gender']&access_token=" . $token;

         

                        $ch = curl_init($url_gender);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_gender = curl_exec($ch);

                        curl_close($ch);

                       

                        $response_gender =  json_decode($result_gender, true);

                        

                        

                        $url_placementdata = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','cpm','frequency','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $token;

         

                        $ch = curl_init($url_placementdata);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result = curl_exec($ch);

                        curl_close($ch);

                               

                       $response_placement =  json_decode($result, true);

                       

                        

                        $totalSpend = array();

                        

                        $totalSpend['spend'] = 0;

                        

                               

                          

                                 if(isset($response_without_breakdown[data][0]) && $response_without_breakdown[data][0] !=''){

                                 $totalSpend['spend'] = $response_without_breakdown[data][0]['spend'];

                                 }else{

                                   $totalSpend['spend'] = 0.00;

                                 }

                                 

                                 $totalSpend['breakdown']['gender'] = $this->getbreakdowngender( $response_gender[data],'spend');

                                 $totalSpend['breakdown']['age'] = $this->getbreakdownage($response_age[data],'spend');

                                

                               

                                 $totalSpend['breakdown']['placement'] = $this->getbreakdownplacement($response_placement[data],'spend');

                                 

                              

                         return $totalSpend;

		}

		

		function getbreakdowngender($responsdata,$breakdownfor){

		    

		      

		     

		             $female = 0;

		             $male = 0;

		             $other = 0;

		              $totalSpend = array();

		              

		                  foreach($responsdata as $genders){

                                       

                                        if ($genders['gender'] == "female") {

                                        $female = $genders[$breakdownfor];

                                        

                                        

                                    } else if ($genders['gender'] == "male") {

                                        $male = $genders[$breakdownfor];

                                       

                                    } else if ($genders['gender'] == "unknown") {

                                        $other = $genders[$breakdownfor];

                                    }else{

                                        

                                        

                                    }

                                       

                                       

                                  }

                                   

                                  $totalSpend['female'] = $female;

                                  $totalSpend['male'] = $male;

                                  $totalSpend['unknown'] = $other;

                             

                              

		    return $totalSpend;

		    

		}

		

		

		function getbreakdownage($responsdata,$breakdownfor){

		    

		    

		                           $age18to24 = 0;

                                   $age25to34 = 0;

                                   $age35to44 = 0;

                                   $age45to54 = 0;

                                   $age55to64 = 0;

                                   $age65plus = 0;

                                  

                                  $totalSpend = array(); 

                                  

                                   foreach($responsdata as $age){

                                       

                                    if($age['age'] == "18-24") {

                                        

                                        $age18to24 = $age[$breakdownfor];

                                        

                                    } elseif ($age['age'] == "25-34") {

                                        $age25to34 = $age[$breakdownfor];

                                       

                                    } elseif ($age['age'] == "35-44") {

                                        $age35to44 = $age[$breakdownfor];

                                        

                                    }elseif ($age['age'] == "45-54") {

                                        $age45to54 = $age[$breakdownfor];

                                        

                                    }elseif($age['age'] == "55-64"){

                                         $age55to64 = $age[$breakdownfor];

                                        

                                    }

                                    elseif($age['age'] == "65+"){

                                         $age65plus = $age[$breakdownfor];

                                        

                                    }

                                   

                                  }

                                   

                                  $totalSpend['18-24'] = $age18to24;

                                  $totalSpend['25-34'] = $age25to34;

                                  $totalSpend['35-44'] = $age35to44;

                                  $totalSpend['45-54'] = $age45to54;

                                  $totalSpend['55-64'] = $age55to64;

                                  $totalSpend['65'] = $age65plus;

                                  

                               

		     return $totalSpend;

		}

		

		

		function hitstimeFrame($addSetId,$date_preset_value,$token){

		    

		                $url_age_gender = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset_value . "fields=['impressions','cpm','frequency']&access_token=" . $token;

         

                        $ch = curl_init($url_age_gender);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result = curl_exec($ch);

                        curl_close($ch);

                       

                        $response_age_gender =  json_decode($result, true);

                        

                        $url_gender_brekdown = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset_value . "fields=['clicks','impressions','cpm','frequency','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $token;

         

                        $ch = curl_init($url_gender_brekdown);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_gender_brekdown = curl_exec($ch);

                        curl_close($ch);

                       

                       

                        $response_gender_breakdown =  json_decode($result_gender_brekdown, true);

                       

                        

                        $url_age_brekdown = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset_value . "fields=['clicks','impressions','cpm','frequency','reach','actions','date_start','campaign_name','ctr','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age']&access_token=" . $token;

         

                        $ch = curl_init($url_age_brekdown);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_age_brekdown = curl_exec($ch);

                        curl_close($ch);

                       

                        $response_age_breakdown =  json_decode($result_age_brekdown, true);

                        

                        $url_placementdata = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset_value . "fields=['clicks','impressions','cpm','frequency']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $token;

         

                        $ch = curl_init($url_placementdata);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_placementdata = curl_exec($ch);

                        curl_close($ch);

                               

                       $response_placement =  json_decode($result_placementdata, true);

                       

                      

                       

                        $peopleSaw['impressions']['hits'] = 0;

                        $peopleSaw['cpm']['hits'] = 0;

                        $peopleSaw['frequency']['hits'] = 0;

                       

                        foreach($response_age_gender[data] as $key => $tempValue){

                          

                          $peopleSaw['impressions']['hits'] = $tempValue['impressions'];

                          $peopleSaw['cpm']['hits'] =  $tempValue['cpm'];

                          $peopleSaw['frequency']['hits'] =  $tempValue['frequency'];

                       

                        }

                       

                        

                         $peopleSaw['impressions']['breakdown']['gender'] = $this->gethitsbreakdowngender($response_gender_breakdown[data],'impressions');

                         $peopleSaw['impressions']['breakdown']['age'] = $this->gethitsbreakdownage($response_age_breakdown[data],'impressions');

                         $peopleSaw['impressions']['breakdown']['placement'] = $this->getbreakdownplacement($response_placement[data],'impressions');

                       

                         $peopleSaw['cpm']['breakdown']['gender'] = $this->gethitsbreakdowngender($response_gender_breakdown[data],'cpm');

                         $peopleSaw['cpm']['breakdown']['age'] = $this->gethitsbreakdownage($response_age_breakdown[data],'cpm');

                         $peopleSaw['cpm']['breakdown']['placement'] = $this->getbreakdownplacement($response_placement[data],'cpm');

                         

                      

                         $peopleSaw['frequency']['breakdown']['gender'] = $this->gethitsbreakdowngender($response_gender_breakdown[data],'frequency');

                         $peopleSaw['frequency']['breakdown']['age'] = $this->gethitsbreakdownage($response_age_breakdown[data],'frequency');

                         $peopleSaw['frequency']['breakdown']['placement'] = $this->getbreakdownplacement($response_placement[data],'frequency');

                        

                 return $peopleSaw;      

                       

		}

		

		

		function getbreakdownplacement($responsdata,$breakdownfor){

		    

            		            $dekstopfeed = 0;

                                $mobilefeed = 0;

                                $instagram = 0;

                                $audience = 0;

                                $rightside = 0;

            		            $totalSpend = array();

		          

		          

		               

		                  

		                  $dekstopfeedcount = 0;

		                  $mobilefeedcount = 0;

		                  $instagramcount = 0;

		                  $audiencecount = 0;

		                  $rightsidecount =0;

		                      

                           

                                foreach($responsdata as $placement){

                                       

                                       

                                       if ($placement['device_platform'] == "desktop" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "feed") {     

                                            

                                        $dekstopfeed += $placement[$breakdownfor];

                                        $dekstopfeedcount++;

                                        

                                    } elseif ($placement['device_platform'] == "mobile" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "feed") {    

                                        

                                        $mobilefeed += $placement[$breakdownfor];

                                        $mobilefeedcount++;

                                       

                                    } elseif ($placement['publisher_platform'] == "instagram") {

                                       

                                        $instagram += $placement[$breakdownfor];

                                        $instagramcount++;

                                    }elseif ($placement['publisher_platform'] == "audience_network") {

                                        $audience += $placement[$breakdownfor];

                                        $audiencecount++;

                                    }

                                    elseif ($placement['device_platform'] == "desktop" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "right_hand_column") {

                                        $rightside += $placement[$breakdownfor];

                                        $rightsidecount++;

                                    }

                                    else{

                                        

                                        

                                    }

                                       

                                       

                                  }

                                  

                                  if($breakdownfor == 'frequency'){

                                      

                                      $totalSpend['dekstopfeed'] = $dekstopfeed/$dekstopfeedcount;

                                  $totalSpend['mobilefeed'] = $mobilefeed/$mobilefeedcount;

                                  $totalSpend['instagram'] = $instagram/$instagramcount;

                                  $totalSpend['audience'] = $audience/$audiencecount;

                                  $totalSpend['rightside'] = $rightside/$rightsidecount;

                                      

                                      

                                  }elseif($breakdownfor == 'cpm'){

                                      

                                       $totalSpend['dekstopfeed'] = $dekstopfeed/$dekstopfeedcount;

                                  $totalSpend['mobilefeed'] = $mobilefeed/$mobilefeedcount;

                                  $totalSpend['instagram'] = $instagram/$instagramcount;

                                  $totalSpend['audience'] = $audience/$audiencecount;

                                  $totalSpend['rightside'] = $rightside/$rightsidecount;

                                      

                                  }else{

                                  

                                  $totalSpend['dekstopfeed'] = $dekstopfeed;

                                  $totalSpend['mobilefeed'] = $mobilefeed;

                                  $totalSpend['instagram'] = $instagram;

                                  $totalSpend['audience'] = $audience;

                                  $totalSpend['rightside'] = $rightside;

                                  }

                                  

                                  foreach($totalSpend as $placementkey => $placementvalue){

                                     if($placementvalue == false){

                                         $totalSpend[$placementkey] = 0;

                                     } 

                                      

                                  }

                        

		                  

		    return $totalSpend;

		    

		}

		

		

		

		function gethitsbreakdowngender($responsdata,$breakdownfor){

		      

		             

		             $female = 0;

		             $male = 0;

		             $other = 0;

		             

		             $femalecount = 0;

		             $malecount = 0;

		             $othercount = 0;

		              $totalSpend = array();

		               

		               

		                  foreach($responsdata as $genders){

                                       

                                        if ($genders['gender'] == "female") {

                                        $female = $genders[$breakdownfor];

                                        

                                       $femalecount++; 

                                       

                                    } elseif ($genders['gender'] == "male") {

                                        $male = $genders[$breakdownfor];

                                        $malecount++;

                                       

                                    } elseif ($genders['gender'] == "unknown") {

                                        $other = $genders[$breakdownfor];

                                        $othercount++;

                                    }else{

                                        

                                        

                                    }

                                       

                                       

                                  }

                                   

                                //  if($breakdownfor == 'frequency'){

                                     

                                //   $totalSpend['female'] = $female;

                                //   $totalSpend['male'] = $male;

                                //   $totalSpend['unknown'] = $other;

                                     

                                     

                                //  } elseif($breakdownfor == 'cpm'){

                                     

                                //       $totalSpend['female'] = $female;

                                //       $totalSpend['male'] = $male;

                                //       $totalSpend['unknown'] = $other;

                                  

                                //  } else{

                                   

                                //   $totalSpend['female'] = $female;

                                //   $totalSpend['male'] = $male;

                                //   $totalSpend['unknown'] = $other;

                                //  }

                                 

                                 

                                   $totalSpend['female'] = $female;

                                  $totalSpend['male'] = $male;

                                  $totalSpend['unknown'] = $other;

                                 

                                  foreach($totalSpend as $genderkey => $gendervalue){

                                      

                                      if($gendervalue == false){

                                          $totalSpend[$genderkey] = 0;

                                          

                                      }

                                      

                                  }

                         

                       

                       

                             

		    return $totalSpend;

		    

		}

		

		function gethitsbreakdownage($responsdata,$breakdownfor){

		

		   

                                   $age18to24 = 0;

                                   $age25to34 = 0;

                                   $age35to44 = 0;

                                   $age55to64 = 0;

                                   $age65plus = 0;

                                  

                                  $age18to24count = 0;

                                  $age25to34count=0;

                                  $age35to44count=0;

                                  $age45to54count=0;

                                  $age55to64count=0;

                                  $age65pluscount=0;

                                  

                                  

                                  

                                   foreach($responsdata as $age){

                                       

                                        if ($age['age'] == "18-24") {

                                        $age18to24 += $age[$breakdownfor];

                                        $age18to24count++;

                                        

                                    } elseif ($age['age'] == "25-34") {

                                        $age25to34 += $age[$breakdownfor];

                                        $age25to34count++;

                                       

                                    } elseif ($age['age'] == "35-44") {

                                        $age35to44 += $age[$breakdownfor];

                                        $age35to44count++;

                                        

                                    }elseif ($age['age'] == "45-54") {

                                        $age45to54 += $age[$breakdownfor];

                                        $age45to54count++;

                                        

                                    }

                                    elseif($age['age'] == "55-64"){

                                         $age55to64 += $age[$breakdownfor];

                                         $age55to64count++;

                                        

                                    }

                                    elseif($age['age'] == "65+"){

                                         $age65plus += $age[$breakdownfor];

                                         $age65pluscount++;

                                        

                                    }

                                   

                                  }

                                 

                                  

                                  if($breakdownfor == 'frequency'){

                                      

                                      $totalSpend['18-24'] = $age18to24;

                                  $totalSpend['25-34'] = $age25to34;

                                  $totalSpend['35-44'] = $age35to44;

                                  $totalSpend['45-54'] = $age45to54;

                                  $totalSpend['55-64'] = $age55to64;

                                  $totalSpend['65'] = $age65plus; 

                                      

                                  } elseif($breakdownfor == 'cpm'){     

                                     $totalSpend['18-24'] = $age18to24;

                                  $totalSpend['25-34'] = $age25to34;

                                  $totalSpend['35-44'] = $age35to44;

                                  $totalSpend['45-54'] = $age45to54;

                                  $totalSpend['55-64'] = $age55to64;

                                  $totalSpend['65'] = $age65plus; 

                                   

                                  } else{

                                  $totalSpend['18-24'] = $age18to24;

                                  $totalSpend['25-34'] = $age25to34;

                                  $totalSpend['35-44'] = $age35to44;

                                  $totalSpend['45-54'] = $age45to54;

                                  $totalSpend['55-64'] = $age55to64;

                                  $totalSpend['65'] = $age65plus;

                                  }

                                  

                                  foreach($totalSpend as $agekey => $agevalue){

                                      

                                      if($agevalue == false){

                                          $totalSpend[$agekey] = 0;

                                          

                                      }

                                      

                                  }

                                  

                      

		       return $totalSpend;

		}

		

		

		

		function gedatapointbreakdowngender($responsdata,$breakdownfor,$breakdownforrow){

		    

		     

		             $female = 0;

		             $male = 0;

		             $other = 0;

		             $totalSpend = array();

		              

		             

		              $femalespend = 0;

		              $femaleimpressions = 0;

		              $malespend = 0;

		              $maleimpressions = 0;

		              $otherspend = 0;

		              $otherimpressions = 0;

		              

		                  foreach($responsdata as $genders){

                                      

                                          if ($genders['gender'] == "female") {

                                            

                                       

                                        foreach ($genders['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $female =  $feeds1['value'];

                                                    }

                                               }

                                               

                                          $femalespend =  $genders['spend'];

                                          $femaleimpressions =  $genders['impressions']; 

                                        

                                        

                                        

                                        

                                    } else if ($genders['gender'] == "male") {

                                       

                                       foreach ($genders['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $male =  $feeds1['value'];

                                                    }

                                               }

                                               

                                          $malespend =  $genders['spend'];

                                          $maleimpressions =  $genders['impressions'];  

                                        

                                        

                                       

                                    } else if ($genders['gender'] == "unknown") {

                                       

                                        foreach ($genders['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $other =  $feeds1['value'];

                                                    }

                                               }

                                               

                                          $otherspend =  $genders['spend'];

                                          $otherimpressions =  $genders['impressions'];  

                                        

                                        

                                        

                                    }else{

                                        

                                        

                                    }

                                  

                                  

                                  }

                                   

                                

                                   

                                   if($breakdownforrow == 'cost_per'){

                                     

                                     $totalSpend['female'] =  ( $femalespend/$female) ?  $femalespend/$female : 0;

                                     $totalSpend['male'] =  ( $malespend/$male) ?  $malespend/$male : 0;

                                     $totalSpend['unknown'] = ( $otherspend/$other) ?  $otherspend/$other : 0;

                                    

                                    }else if($breakdownforrow == 'rate'){

                                     

                                      $totalSpend['female'] = ( (($female/$femaleimpressions)*100)) ?  (($female/$femaleimpressions)*100) : 0;

                                      $totalSpend['male'] =  ( (($male/$maleimpressions)*100)) ?  (($male/$maleimpressions)*100) : 0; 

                                      $totalSpend['unknown'] =( (($other/$otherimpressions)*100)) ?  (($other/$otherimpressions)*100) : 0;  

                                    

                                    }else{

                                        

                                      $totalSpend['female'] = $female;

                                      $totalSpend['male'] = $male;

                                      $totalSpend['unknown'] = $other;  

                                     

                                    }

                              

                                foreach($totalSpend as $tempkey => $tempvalue){

                                    

                                    if($tempvalue == false){

                                        

                                       $totalSpend[$tempkey] = 0;

                                    }

                                }

                              

		    return $totalSpend;

		    

		}

		

		function gedatapointbreakdownage($responsdata,$breakdownfor,$breakdownforrow){

		    

		                            $age18to24 = 0;

                                   $age18to24spend = 0;

                                   $age18to24impressions = 0;

                                   

                                   $age25to34 = 0;

                                   $age25to34spend = 0;

                                   $age25to34impressions = 0;

                                   

                                   $age35to44 = 0;

                                   $age35to44spend = 0;

                                   $age35to44impressions = 0;

                                   

                                   $age45to54 = 0;

                                   $age45to54spend = 0;

                                   $age45to54impressions = 0;

                                   

                                   

                                   $age55to64 = 0;

                                   $age55to64spend = 0;

                                   $age55to64impressions = 0;

                                   

                                   $age65plus = 0;

                                   $age65plusspend = 0;

                                   $age65plusimpressions = 0;

                                  

                                  

                                  

                                   foreach($responsdata as $age){

                                       

                                   

                                           if ($age['age'] == "18-24") {

                                          

                                        foreach ($age['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $age18to24 =  $feeds1['value'];

                                                    }

                                               }

                                               

                                        $age18to24spend = $age['spend'];

                                        $age18to24impressions = $age['impressions'];

                                        

                                        

                                        

                                    } else if ($age['age'] == "25-34") {

                                      

                                        foreach ($age['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $age25to34 =  $feeds1['value'];

                                                    }

                                               }

                                        $age25to34spend += $age['spend'];

                                        $age25to34impressions += $age['impressions'];

                                       

                                    } else if ($age['age'] == "35-44") {

                                      

                                        foreach ($age['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $age35to44 =  $feeds1['value'];

                                                    }

                                               }

                                        $age35to44spend += $age['spend'];

                                        $age35to44impressions = $age['impressions'];

                                        

                                    } else if ($age['age'] == "45-54") {

                                      

                                        foreach ($age['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $age45to54 =  $feeds1['value'];

                                                    }

                                               }

                                        $age45to54spend += $age['spend'];

                                        $age45to54impressions += $age['impressions'];

                                        

                                    }

                                    else if($age['age'] == "55-64"){

                                        

                                          foreach ($age['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $age55to64 =  $feeds1['value'];

                                                    }

                                               }

                                         $age55to64spend += $age['spend'];

                                         $age55to64impressions = $age['impressions'];

                                        

                                    }

                                    else if($age['age'] == "65+"){

                                        

                                           foreach ($age['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $age65plus =  $feeds1['value'];

                                                    }

                                               }

                                         $age65plusspend = $age['spend'];

                                         $age65plusimpressions = $age['impressions'];

                                        

                                    }

                                   

                                   

                                  }

                                  

                                  

                                  if($breakdownforrow == 'cost_per'){

                                       

                                    

                                          $totalSpend['18-24'] = ( $age18to24spend/$age18to24) ?  $age18to24spend/$age18to24 : 0;

                                          $totalSpend['25-34'] = ($age25to34spend/$age25to34) ? $age25to34spend/$age25to34 : 0; 

                                          $totalSpend['35-44'] = ($age35to44spend/$age35to44) ? $age35to44spend/$age35to44 : 0; 

                                          $totalSpend['45-54'] = ($age45to54spend/$age45to54) ? $age45to54spend/$age45to54 : 0; 

                                          $totalSpend['55-64'] = ( $age55to64spend/$age55to64) ?  $age55to64spend/$age55to64 : 0;

                                          $totalSpend['65'] =   ( $age65plusspend/$age65plus) ?  $age65plusspend/$age65plus : 0;

                                      

                                      

                                    }else if($breakdownforrow == 'rate'){

                                      

                                          $totalSpend['18-24'] = (($age18to24/$age18to24impressions)*100) ?  (($age18to24/$age18to24impressions)*100) : 0;

                                          $totalSpend['25-34'] = (($age25to34/$age25to34impressions)*100) ?  (($age25to34/$age25to34impressions)*100) : 0;

                                          $totalSpend['35-44'] = (($age35to44/$age35to44impressions)*100) ?  (($age35to44/$age35to44impressions)*100) : 0;

                                          $totalSpend['45-54'] = (($age45to54/$age45to54impressions)*100) ?  (($age45to54/$age45to54impressions)*100) : 0;

                                          $totalSpend['55-64'] = (($age55to64/$age55to64impressions)*100) ?  (($age55to64/$age55to64impressions)*100) : 0;

                                          $totalSpend['65'] = (($age65plus/$age65plusimpressions)*100) ?  (($age65plus/$age65plusimpressions)*100) : 0;

                                      

                                        

                                    }else{

                                        

                                          $totalSpend['18-24'] = $age18to24;

                                          $totalSpend['25-34'] = $age25to34;

                                          $totalSpend['35-44'] = $age35to44;

                                          $totalSpend['45-54'] = $age45to54;

                                          $totalSpend['55-64'] = $age55to64;

                                          $totalSpend['65'] = $age65plus;

                                      

                                    }

                                    

                 return $totalSpend;            

		}

		

		

		

		

		function getdatapointbreakdownplacement($responsdata,$breakdownfor,$breakdownforrow){

		    

            		            $dekstopfeed = 0;

                                $mobilefeed = 0;

                                $instagram = 0;

                                $audience = 0;

                                $rightside = 0;

                                $dekstopfeedspend = 0;

                                $dekstopfeedimpressions = 0;

                                $mobilefeedspend = 0;

                                $mobilefeedimpressions = 0;

                                $instagramspend = 0;

                                $instagramimpressions = 0;  

                                $audiencespend = 0;

                                $audienceimpressions = 0;

                                $rightsidespend = 0;

                                $rightsideimpressions = 0;           

                                

            		            $totalSpend = array();

		                       

		                      //  echo '<pre>';

		                      //  print_r($responsdata);

		                      //  exit;

		                       

		                      //  echo '<pre>';

                        //         print_r($responsdata);

                           

                                foreach($responsdata as $placement){

                                       

                                      // if($breakdownforrow == 'cost_per' || $breakdownforrow == 'rate'){

                                      

                                        //if (strpos($breakdownfor, '.') != false) {   

                                            

                                         if ($placement['device_platform'] == "desktop" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "feed") {     

                                                    

                                                

                                                 foreach ($placement['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $dekstopfeed +=  $feeds1['value'];

                                                    }

                                               }

                                               

                                              $dekstopfeedspend +=  $placement['spend'];

                                              $dekstopfeedimpressions +=  $placement['impressions']; 

                                             

                                                

                                            } else if ($placement['device_platform'] == "mobile" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "feed") {    

                                                

                                                foreach ($placement['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $mobilefeed +=  $feeds1['value'];

                                                    }

                                               }

                                               

                                              $mobilefeedspend +=  $placement['spend'];

                                              $mobilefeedimpressions +=  $placement['impressions'];  

                                                

                                                

                                                

                                               

                                            } else if ($placement['publisher_platform'] == "instagram") {

                                              

                                                foreach ($placement['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $instagram +=  $feeds1['value'];

                                                    }

                                               }

                                               

                                              $instagramspend +=  $placement['spend'];

                                              $instagramimpressions +=  $placement['impressions'];  

                                             

                                            }else if ($placement['publisher_platform'] == "audience_network") {

                                                

                                              

                                                 foreach ($placement['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $audience +=  $feeds1['value'];

                                                    }

                                               }

                                               

                                              $audiencespend +=  $placement['spend'];

                                              $audienceimpressions +=  $placement['impressions'];  

                                                

                                                

                                                

                                            }

                                            else if ($placement['device_platform'] == "desktop" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "right_hand_column") {

                                               

                                                 foreach ($placement['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $breakdownfor) {

                                                         $rightside +=  $feeds1['value'];

                                                    }

                                               }

                                               

                                              $rightsidespend +=  $placement['spend'];

                                              $rightsideimpressions +=  $placement['impressions'];  

                                                

                                                

                                            }

                                            else{

                                            

                                            }

                                              

                                           

                                           

                                      /* 

                                       }

                                       

                                       else{

                                           

                                       

                                               if ($placement['device_platform'] == "desktop" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "feed") {     

                                                    

                                                 $dekstopfeed += $placement[$breakdownfor];

                                                

                                                 $dekstopfeedspend += $placement['spend'];

                                                 $dekstopfeedimpressions += $placement['impressions'];

                                                

                                            } else if ($placement['device_platform'] == "mobile" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "feed") {    

                                                

                                                $mobilefeed += $placement[$breakdownfor];

                                                

                                                $mobilefeedspend += $placement['spend'];

                                                $mobilefeedimpressions += $placement['impressions'];

                                               

                                            } else if ($placement['publisher_platform'] == "instagram") {

                                               

                                                $instagram += $placement[$breakdownfor];

                                                $instagramspend += $placement['spend'];

                                                $instagramimpressions += $placement['impressions'];

                                                

                                            }else if ($placement['publisher_platform'] == "audience_network") {

                                                

                                                $audience += $placement[$breakdownfor];

                                                $audiencespend += $placement['spend'];

                                                $audienceimpressions += $placement['impressions'];

                                            }

                                            else if ($placement['device_platform'] == "desktop" && $placement['publisher_platform'] == "facebook" && $placement['platform_position'] == "right_hand_column") {

                                               

                                                $rightside += $placement[$breakdownfor];

                                                $rightsidespend += $placement['spend'];

                                                $rightsideimpressions += $placement['impressions'];

                                            }

                                            else{

                                            

                                            }

                                            

                                            

                                            

                                       }     

                                       */

                                      

                                  }

                                  

                                

                                  

                                

                                 if($breakdownforrow == 'cost_per'){



                                          $totalSpend['dekstopfeed'] = (($dekstopfeedspend/$dekstopfeed)==false)? 0:$dekstopfeedspend/$dekstopfeed;

                                          $totalSpend['mobilefeed'] = (($mobilefeedspend/$mobilefeed)==false)? 0:$mobilefeedspend/$mobilefeed;

                                          $totalSpend['instagram'] = (($instagramspend/$instagram)==false)? 0:$instagramspend/$instagram;

                                          $totalSpend['audience'] = (($audiencespend/$audience)==false)? 0:$audiencespend/$audience;

                                          $totalSpend['rightside'] = (($rightsidespend/$rightside)==false)? 0:$rightsidespend/$rightside;

                                     

                                    }else if($breakdownforrow == 'rate'){

                                      

                                        

                                          $totalSpend['dekstopfeed'] = ((($dekstopfeed/$dekstopfeedimpressions)*100) == false) ? 0:($dekstopfeed/$dekstopfeedimpressions)*100;

                                          $totalSpend['mobilefeed'] = ((($mobilefeed/$mobilefeedimpressions)*100) == false) ? 0:($mobilefeed/$mobilefeedimpressions)*100;

                                          $totalSpend['instagram'] = ((($instagram/$instagramimpressions)*100) == false) ? 0:($instagram/$instagramimpressions)*100;

                                          $totalSpend['audience'] = ((($audience/$audienceimpressions)*100) == false) ? 0:($audience/$audienceimpressions)*100;

                                          $totalSpend['rightside'] = ((($rightside/$rightsideimpressions)*100) == false) ? 0:($rightside/$rightsideimpressions)*100;

                                      

                                        

                                    }else{

                                        

                                              $totalSpend['dekstopfeed'] = $dekstopfeed;

                                              $totalSpend['mobilefeed'] = $mobilefeed;

                                              $totalSpend['instagram'] = $instagram;

                                              $totalSpend['audience'] = $audience;

                                              $totalSpend['rightside'] = $rightside;

                                    } 

                                    

                                // echo $breakdownforrow;

                                // echo '<pre>';

                                // print_r($totalSpend);

                                // exit;

                               

		    return $totalSpend;

		    

		}

		

		function getlabel($stringlabel){

		    

        if (strpos($stringlabel, '.') != false) {

            

          $temparray = explode('fb_pixel_',$stringlabel);

        }else{

          

          $temparray = explode('.',$stringlabel);

          

        }

        

          $templabel =  $temparray[1];

          $labelarray = explode('_',$templabel);

          $labelstring = implode(' ',$labelarray);

          

          $label = ucwords($labelstring);

          if($label == ''){

             $label = $stringlabel; 

          }

		  return   $label;

		}

		

		

		

		

		

		

		

		function getdatapoint($addSetIdlist,$date_preset_value,$fields,$token){

		    

		

		                 $date_preset = "date_preset=".$date_preset_value."&";

		               

		                 $datapoints[] = 'actions';

		                 $datapoints[] = 'impressions';

		                 $datapoints[] = 'spend';

		                 $field_string = json_encode($datapoints);

		               

		               

		               $fieldarray = array();

		               $count_impression = count($fields)-1;

		               $count_spend = $count_impression - 1;

		               

		               unset($datapoints[$count_impression]) ;

		               unset($datapoints[$count_spend]) ;

		             

		               

		                  foreach($addSetIdlist as $addSetIdtemp){

		                     

            

                            $addsetName = $addSetIdtemp['name'];

                            $addSetId = $addSetIdtemp['id'];

                            

                          

                      $url_without_breakdown = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=".$field_string."&access_token=" . $token;

                   

                        $ch = curl_init($url_without_breakdown);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_withoutbreakdown = curl_exec($ch);

                        curl_close($ch);

                        $result_withoutbreakdown;

                        $response_without_breakdown =  json_decode($result_withoutbreakdown, true);   

                      

                        

                        $url_age = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=".$field_string."&breakdowns=['age']&access_token=" . $token;

                        

         

                        $ch = curl_init($url_age);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_age = curl_exec($ch);

                        curl_close($ch);

                       

                        $response_age =  json_decode($result_age, true);

                        

                        

                        $url_gender = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=".$field_string."&breakdowns=['gender']&access_token=" . $token;

                        

         

                        $ch = curl_init($url_gender);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result_gender = curl_exec($ch);

                        curl_close($ch);

                       

                        $response_gender =  json_decode($result_gender, true);

                        

                        

                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=".$field_string."&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $token;

                        

         

                        $ch = curl_init($url);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        $result = curl_exec($ch);

                        curl_close($ch);

                       

                        $response_placement =  json_decode($result, true);

                        

                                    foreach($fields as $tempfield){

                                        

                                        //$label =  $this->getlabel($tempfield);

                                        $label = $tempfield;

                                        

                                        foreach($response_without_breakdown[data] as $key => $tempValue){

                                        

                                                foreach ($tempValue['actions'] as $feeds1) {

                                                    if ($feeds1['action_type'] == $tempfield) {

                                                         $fieldarray[$label][$addsetName]['total']['counts'] =  $feeds1['value'];

                                                    }

                                               }

                                               

                                      $fieldarray[$label][$addsetName]['spend'] =  $tempValue['spend'];

                                      $fieldarray[$label][$addsetName]['impressions'] =  $tempValue['impressions'];

                                      

                                         

                                       $fieldarray[$label][$addsetName]['cost_per']['counts'] =  (($fieldarray[$label][$addsetName]['spend']/$fieldarray[$label][$addsetName]['total']['counts'])==false)? 0:$fieldarray[$label][$addsetName]['spend']/$fieldarray[$label][$addsetName]['total']['counts'];

                                       $fieldarray[$label][$addsetName]['rate']['counts'] =  ((($fieldarray[$label][$addsetName]['total']['counts']/$fieldarray[$label][$addsetName]['impressions'])*100)==false)?0:($fieldarray[$label][$addsetName]['total']['counts']/$fieldarray[$label][$addsetName]['impressions'])*100;

                                       unset($fieldarray[$label][$addsetName]['spend']);

                                       unset($fieldarray[$label][$addsetName]['impressions']);

                                       

                                    

                                    $fieldarray[$label][$addsetName]['total']['breakdown']['gender'] = $this->gedatapointbreakdowngender($response_gender[data],$tempfield,'');

                                    $fieldarray[$label][$addsetName]['total']['breakdown']['age'] = $this->gedatapointbreakdownage($response_age[data],$tempfield,'');

                                    $fieldarray[$label][$addsetName]['total']['breakdown']['placement'] = $this->getdatapointbreakdownplacement($response_placement[data],$tempfield,'');

                                    

                                    

                                    $fieldarray[$label][$addsetName]['cost_per']['breakdown']['gender'] = $this->gedatapointbreakdowngender($response_gender[data],$tempfield,'cost_per');

                                    $fieldarray[$label][$addsetName]['cost_per']['breakdown']['age'] = $this->gedatapointbreakdownage($response_age[data],$tempfield,'cost_per');

                                    $fieldarray[$label][$addsetName]['cost_per']['breakdown']['placement'] = $this->getdatapointbreakdownplacement($response_placement[data],$tempfield,'cost_per');

                                    

                                    $fieldarray[$label][$addsetName]['rate']['breakdown']['gender'] = $this->gedatapointbreakdowngender($response_gender[data],$tempfield,'rate');

                                    $fieldarray[$label][$addsetName]['rate']['breakdown']['age'] = $this->gedatapointbreakdownage($response_age[data],$tempfield,'rate');

                                    $fieldarray[$label][$addsetName]['rate']['breakdown']['placement'] = $this->getdatapointbreakdownplacement($response_placement[data],$tempfield,'rate');  

                                 

                                 }

                             

		                  }

		            

		                  }

		                  

		                  //echo '<pre>';

		                  //print_r($fieldarray);

		                  //exit;

                         return $fieldarray;

		    

		    

		}

		public function uploadlogo(){
			$type=['image/png','image/jpeg'];
			
			 if ($_FILES['file']['error'] > 0 ){
				echo 'Error: ' . $_FILES['file']['error'];
			}
			else
			if(!in_array($_FILES['file']['type'],$type)){
				echo 'Error: Logo should be JPG/JPEG/PNG type.';
			}else
				{
				$arr=explode('.',$_FILES['file']['name']);
				$n=count($arr);
				
				if(move_uploaded_file($_FILES['file']['tmp_name'], 'assets/analysis_logo/' . $_POST['adAccountId'].'.'.$arr[$n-1]))
				{
					echo "Logo uploaded! Click Generate PDF to see new report.";
				}
			}
			die;
		}

		

		

		

		

		

}

//End of class