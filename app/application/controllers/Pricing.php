<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Pricing extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->seo->SetValues('Title', "Pricing");
        $this->seo->SetValues('Description', "Pricing");
    }

    public function index() {
        header('Location: http://thecampaignmaker.com/#price');exit;
        $data = new stdClass();
        loadView('pricing', $data);
    }

    public function yearly() {
        $data = new stdClass();
        loadView('pricing_yearly', $data);
    }

    public function upgrade() {
        $data = new stdClass();
        loadView('upgrade', $data);
    }

}
