<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use FacebookAds\Object\AdAccount;
use FacebookAds\Api;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\InsightsFields;

class Service extends CI_Controller {

    public $user_id;
    public $access_token;
    public $add_account_id;

    public function __construct() {
        error_reporting(1);
        parent::__construct();
        $this->load->model('Users_model');
        if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
            $this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
        }

        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];

        if (isset($this->session->userdata['logged_in']) && !empty($this->session->userdata['logged_in'])) {

            $app_id = $this->config->item('facebook_app_id');

            $facebook_app_secret = $this->config->item('facebook_app_secret');

            Api::init($app_id, $facebook_app_secret, $this->access_token);
        }

        $this->user_id = $this->session->userdata['logged_in']['id'];
    }

    public function index() {

        $data = new stdClass();
        $data->adAccountData = $this->getAdAccount();
        $data->adaccounts = $data->adAccountData['response'];
        loadView('service_landing', $data);
    }
    public function contact() {

        $data = new stdClass();
        $data->adAccountData = $this->getAdAccount();
        $data->adaccounts = $data->adAccountData['response'];
        loadView('service', $data);
    }
    
    public function payment()
    {
        $data = new stdClass();
        loadView('service_payment', $data);
    }
	
    public function getCampaigns() {
        if (isset($_POST['ad_account_id'])) {
            $ad_account_id = $_POST['ad_account_id'];
            $this->setAddAccountId($ad_account_id);
            $Campaigns = $this->getCurlCampaignsDateRange_new(100);
            if(empty($Campaigns['response']['campaigns'])){
                echo '[]'; die;
            } 
            
            echo json_encode($Campaigns['response']['campaigns']);
        }
    }

    private function setAddAccountId($adAccountId) {



        $this->add_account_id = $adAccountId;
    }

    private function getAddAccountId() {



        return $this->add_account_id;
    }

    function getCurlCampaignsDateRange_new($limit) {



        $resultArray = array();



        $adAccountId = "act_" . $this->getAddAccountId();



        $campaignFields = $this->getCampaignFields(1);



        $dataArray = explode("#", $limit);


        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=" . $campaignFields . "&limit=250&access_token=" . $this->access_token;



        #echo "<br>".$url;exit;



        try {
            $ch = curl_init($url);



            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);



            $result = curl_exec($ch);



            curl_close($ch);



            $response = json_decode($result, true);



            $error = $response['error']['message'];
            if (isset($response['data'])) {



                $campaignDataArray = $this->getFinalResult($response['data']);



                if ($campaignDataArray) {



                    $resultArray = array(
                        "count" => 1,
                        "response" => $campaignDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {



                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $error,
                        "success" => ""
                    );
                }
            } else {



                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => $error,
                    "success" => ""
                );
            }
        } catch (Exception $ex) {



            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }



        //$this->getPrintResult($resultArray);



        return $resultArray;
    }

    function getCampaignFields($curl) {



        if ($curl == 1) {



            $fields = "id,account_id,name,objective,buying_type,created_time,start_time,stop_time,effective_status,currency,configured_status";
        } else {



            $fields = array(
                CampaignFields::ID,
                CampaignFields::ACCOUNT_ID,
                CampaignFields::NAME,
                CampaignFields::OBJECTIVE,
                CampaignFields::BUYING_TYPE,
                CampaignFields::PROMOTED_OBJECT,
                CampaignFields::ADLABELS,
                CampaignFields::CREATED_TIME,
                CampaignFields::START_TIME,
                CampaignFields::STOP_TIME,
                CampaignFields::UPDATED_TIME,
                CampaignFields::EFFECTIVE_STATUS,
            );
        }



        return $fields;
    }

    protected function getAdAccount() {

        $result = $this->Users_model->getUserAddAccountId($this->user_id);



        if (count($result) > 0) {

            if (count($result) == 1) {

                $finalRes = "";

                $this->setAddAccountId($result[0]->ad_account_id);

                $this->getCurrency();

                $this->add_account_id = $result[0]->ad_account_id;

                $this->add_title = $result[0]->add_title;

                // $limit=date('Y-m-d', strtotime('-7 days'))."#".date('Y-m-d');
                //$finalRes = $this->getCurlCampaignsDateRange($limit);
                //$finalRes = $this->getCurlCampaigns("");

                $finalRes = $this->getCurlCampaignsDateRange_new($this->dateval);

                // $this->getPrintResult($finalRes);

                $resultArray = array(
                    "count" => $finalRes['count'],
                    "response" => $finalRes['response'],
                    "error" => $finalRes['error'],
                    "success" => $finalRes['succes']
                );

                // End Get Ad  
            } else {

                $resultArray = array(
                    "count" => 2,
                    "response" => $result,
                    "error" => "",
                    "success" => ""
                );
            }
        } else {

            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => "No data available against your account.",
                "success" => ""
            );
        }

        //  $this->getPrintResult($resultArray);

        return $resultArray;
    }

    function submit_quote() {
        if (isset($_POST['email'])) {
            try {
                $to = "samy@thecampaignmaker.com";
                //$to = "ganeshcro@gmail.com";
                $subject = "Quote Submission for " . $this->input->post('usernamae') . "";

                $message = "
                <html>
                <head>
                <title>Quote Submission for " . $this->input->post('usernamae') . "</title>
                </head>
                <body>
                <p>Quote Information</p>
                <strong>Name :</strong>
                <p>" . $this->input->post('usernamae') . "</p>
                <br>
                <strong>Email :</strong>
                <p>" . $this->input->post('email') . "</p>
                <br>
                <strong>Ad Account :</strong>
                <p>" . $this->input->post('add_account') . "</p>
                <br>
                <strong>Campaign :</strong>
                <p>" . $this->input->post('campaign_account') . "</p>
                <br>
                <strong>Offer Info :</strong>
                <p>" . $this->input->post('offer_info') . "</p>
                <br>
                <strong>Competitors :</strong>
                <p>" . $this->input->post('competitors_url') . "</p>
                <br>
                <strong>Restrictions :</strong>
                <p>" . $this->input->post('restrictions') . "</p>
                </body>
                </html>
                ";

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                // More headers
                //$headers .= 'From: <'.$this->input->post('rcemail').'>' . "\r\n";
                $headers .= 'From: <' . $this->input->post('email') . '>' . "\r\n";
                //$headers .= 'Cc: muhammadjunaid85@gmail.com' . "\r\n";
                //$headers .= 'Reply-To: '.$this->input->post('rcemail') . "\r\n";
                $headers .= 'Reply-To: ' . $this->input->post('email') . "\r\n";

                $isMailsent = mail($to, $subject, $message, $headers);
                if ($isMailsent) {
                    die(array('status' => 'success', 'message' => 'submited successfully'));
                } else {
                    die(array('status' => 'success', 'message' => 'error to send email'));
                }
            } catch (Exception $ex) {
                //echo $ex->getMessage();
                die(array('status' => 'fail', 'message' => $ex->getMessage()));
            }
        }
    }

    function process_payment() {
   
         
            require_once(APPPATH . 'libraries/stripe/Stripe.php');
            $amount_original = (int) $_POST['amount'];
            $amount =  $amount_original * 100;
            $token = $_POST['stripeToken'];
            Stripe::setApiKey($this->config->item('stripe_api_key'));
            
            try {

                $charge = Stripe_Charge::create(array(
                  'amount' => $amount,
                  'currency' => 'usd',
                  'description' => $this->session->userdata['logged_in']['email'],
                  'source' => $token,
			
                  )); 
                
                if($charge->id){
                    die(json_encode(array('status' => 'success', 'message' => "Your payment of $".$amount_original." is successful and your transaction id is {$charge->id}. ")));
                }else{
                    die(json_encode(array('status' => 'fail', 'errmessage' => "your payment is failed")));
                }
            } catch (Exception $e) {
                die(json_encode(array('status' => 'fail', 'errmessage' => "your payment is failed. Error: ".$e->getMessage()."")));
			} 
    }

    function thankyou() {
        $data = new stdClass();
        $data->message = $this->session->flashdata('message');
        loadView('thankyou', $data);
    }

    function getFinalResult($campaignDataArray) {



        $activeCampaigns = 0;



        $inActiveCampaigns = 0;



        $cost_per_unique_click = 0;



        $total_cost_per_inline_click = 0;



        $impressions = 0;



        $total_inline_clicks = 0;



        $reach = 0;



        $unique_clicks = 0;



        $unique_ctr = 0;



        $spent = 0;



        $actions = 0;



        $cost_per_total_action = 0;







        $inline_clicks = 0;



        $cost_per_inline_clicks = 0;



        $pendingCampaigns = 0;



        $disapprovedCampaigns = 0;



        $total = 0;



        $total1 = 0;



        $total2 = 0;



        $conversion = 0;



        #echo "<PRE>";print_R($campaignDataArray);exit;



        $returnRow = array();



        foreach ($campaignDataArray as $row) {



            if ($row['effective_status'] == "ACTIVE" || $row['effective_status'] == "PREAPPROVED") {



                $activeCampaigns++;



                $status = "Active";
            } else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {



                $inActiveCampaigns++;



                $status = "Inactive";
            } else if ($row['effective_status'] == "PENDING_REVIEW") {



                $pendingCampaigns++;



                $status = "In Review";
            } else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {



                $disapprovedCampaigns++;



                $status = "Denied";
            }



            $total++;







            if (isset($row['insights'])) {



                // echo $row['name']."<br>";



                $inline_post_engagement1 = $like1 = '0';



                $conversion = $cost_per_conversion = 0;



                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';



                if (isset($row['insights']['data'][0])) {



                    foreach ($row['insights']['data'][0]['actions'] as $rs) {



                        #echo "<PRE>";print_r($row['insights']);exit;



                        if ($row['insights']['data'][0]['clicks']) {



                            $inline_clicks = $row['insights']['data'][0]['clicks'];



                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];
                        }



                        if ($rs['action_type'] == "leadgen.other") {



                            if ($rs['value']) {



                                $conversion = $rs['value'];
                            } else {



                                $conversion = 0;
                            }
                        }



                        if ($rs['action_type'] == "offsite_conversion") {



                            if ($rs['value']) {



                                $conversion += $rs['value'];



                                $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];
                            } else {



                                $conversion = 0;



                                $cost_per_conversion = 0;
                            }
                        }



                        if ($rs['action_type'] == 'page_engagement') {



                            if ($rs['value']) {



                                $total1++;



                                $inline_post_engagement1 = $rs['value'];
                            } else {



                                $inline_post_engagement1 = 0;
                            }
                        }



                        if ($rs['action_type'] == 'like') {



                            if ($rs['value']) {



                                $total2++;



                                $like1 = $rs['value'];
                            } else {



                                $like1 = 0;
                            }
                        }
                    }



                    foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {



                        if ($rs['action_type'] == 'page_engagement') {



                            if ($rs['value']) {



                                $cost_per_inline_post_engagement1 = $rs['value'];
                            } else {



                                $cost_per_inline_post_engagement1 = 0;
                            }
                        }



                        if ($rs['action_type'] == "leadgen.other") {



                            if ($rs['value']) {



                                $cost_per_conversion = $rs['value'];
                            } else {



                                $cost_per_conversion = 0;
                            }
                        }



                        if ($rs['action_type'] == 'like') {



                            if ($rs['value']) {



                                $cost_per_like1 = $rs['value'];
                            } else {



                                $cost_per_like1 = 0;
                            }
                        }
                    }
                }







                $total_inline_clicks += $inline_clicks;



                $total_cost_per_inline_click += $cost_per_inline_clicks;







                $returnRow['campaigns'][] = array(
                    "campaign_id" => $row['id'],
                    "campaign_name" => $row['name'],
                    "campaign_created_time" => $row['created_time'],
                    "campaign_effective_status" => $status, //$row['effective_status'],
                    "campaign_cost_per_unique_click" => $row['insights']['data'][0]['cost_per_unique_click'],
                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,
                    "campaign_impressions" => $row['insights']['data'][0]['impressions'],
                    "campaign_inline_link_clicks" => $inline_clicks,
                    "campaign_reach" => $row['insights']['data'][0]['reach'],
                    "campaign_unique_clicks" => $row['insights']['data'][0]['unique_clicks'],
                    "campaign_unique_ctr" => $row['insights']['data'][0]['ctr'],
                    "campaign_spent" => $row['insights']['data'][0]['spend'],
                    "campaign_actions" => $conversion,
                    "campaign_cost_per_total_action" => $cost_per_conversion,
                    "campaign_reach" => $row['insights']['data'][0]['reach'],
                    "campaign_objective" => $row['objective'],
                    "inline_post_engagement" => $inline_post_engagement1,
                    "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement1,
                    "page_like" => $like1,
                    "cost_per_like1" => $cost_per_like1,
                    "leadgen" => $leadgen,
                    "cost_leadgen" => $cost_leadgen
                );







                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];



                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];



                $impressions += $row['insights']['data'][0]['impressions'];



                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];



                $reach += $row['insights']['data'][0]['reach'];



                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];



                $unique_ctr += $row['insights']['data'][0]['ctr'];



                $spent += $row['insights']['data'][0]['spend'];



                $actions += $conversion;



                $cost_per_total_action += $cost_per_conversion;



                $inline_post_engagement += $inline_post_engagement1;



                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;







                $page_like += $like1;



                $cost_per_like += $cost_per_like1;



                $cost_leadgen1 += $cost_leadgen;



                $leadgen1 += $leadgen;







                //Reset   



                $inline_clicks = '--';



                $cost_per_inline_clicks = '--';
            } else {







                $returnRow['campaigns'][] = array(
                    "campaign_id" => $row['id'],
                    "campaign_name" => $row['name'],
                    "campaign_created_time" => $row['created_time'],
                    "campaign_effective_status" => $status, //$row['effective_status'],
                    "campaign_cost_per_unique_click" => 0,
                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,
                    "campaign_impressions" => 0,
                    "campaign_inline_link_clicks" => $inline_clicks,
                    "campaign_reach" => 0,
                    "campaign_unique_clicks" => 0,
                    "campaign_unique_ctr" => 0,
                    "campaign_spent" => 0,
                    "campaign_actions" => 0,
                    "campaign_cost_per_total_action" => 0,
                    "campaign_reach" => 0,
                    "inline_post_engagement" => 0,
                    "cost_per_inline_post_engagement" => 0,
                    "page_like" => 0,
                    "cost_per_like1" => 0,
                    "campaign_objective" => $row['objective'],
                    "leadgen" => 0,
                    "cost_leadgen" => 0
                );







                $cost_per_unique_click += 0;



                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];



                $impressions += 0;



                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];



                $reach += 0;



                $unique_clicks += 0;



                $unique_ctr += 0;



                $spent += 0;



                $actions += 0;



                $cost_per_total_action += 0;



                $inline_post_engagement += 0;



                $cost_per_inline_post_engagement += 0;







                $page_like += 0;



                $cost_per_like += 0;



                $cost_leadgen1 += 0;



                $leadgen1 += 0;







                //Reset   



                $inline_clicks = '--';



                $cost_per_inline_clicks = '--';
            }
        }



        if ($unique_ctr > 0) {



            $unique_ctr = ($unique_ctr / $total);
        }



        if ($total_cost_per_inline_click > 0) {



            $total_cost_per_inline_click = ($total_cost_per_inline_click / $total);
        }







        if ($cost_per_inline_post_engagement > 0) {



            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);
        }



        if ($cost_leadgen1 > 0) {



            $cost_leadgen1 = ($cost_leadgen1 / $total3);
        }



        if ($cost_per_like > 0) {



            $cost_per_like = ($cost_per_like / $total2);
        }



        //echo $actions;



        if ($cost_per_total_action > 0) {



            $cost_per_total_action = ($spent / $actions);
        }



        $returnRow['campaigns'] = $this->sksort($returnRow['campaigns'], "campaign_effective_status");



        $returnRow['campaign_header'] = array(
            "campaign_active" => $activeCampaigns,
            "campaign_inactive" => $inActiveCampaigns,
            "campaign_pending" => $pendingCampaigns,
            "campaign_disapproved" => $disapprovedCampaigns,
            "cost_per_unique_click" => $cost_per_unique_click,
            "cost_per_inline_link_click" => $total_cost_per_inline_click,
            "impressions" => $impressions,
            "inline_link_clicks" => $total_inline_clicks,
            "reach" => $reach,
            "unique_clicks" => $unique_clicks,
            "unique_ctr" => $unique_ctr,
            "spent" => $spent,
            "actions" => $actions,
            "actions" => $actions,
            "cost_per_total_action" => $cost_per_total_action,
            "inline_post_engagement" => $inline_post_engagement,
            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,
            "page_like" => $page_like,
            "cost_per_like" => $cost_per_like,
            "leadgen" => $leadgen1,
            "cost_leadgen" => $cost_leadgen1,
        );







        #echo "<PRE>";print_R($returnRow);exit;



        return $returnRow;
    }

    function sksort(&$array, $subkey = "id", $subkey2 = null, $sort_ascending = true) {



        if (count($array))
            $temp_array[key($array)] = array_shift($array);



        foreach ($array as $key => $val) {



            $offset = 0;



            $found = false;



            foreach ($temp_array as $tmp_key => $tmp_val) {



                if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {



                    $temp_array = array_merge(
                            (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));



                    $found = true;
                } elseif (!$found



                        and $subkey2 and strtolower($val[$subkey]) == strtolower($tmp_val[$subkey])



                        and strtolower($val[$subkey2]) > strtolower($tmp_val[$subkey2])) {



                    $temp_array = array_merge(
                            (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));



                    $found = true;
                }



                $offset++;
            }



            if (!$found)
                $temp_array = array_merge($temp_array, array($key => $val));
        }



        if ($sort_ascending)
            $array = array_reverse($temp_array);
        else
            $array = $temp_array;







        return $array;
    }

}

//End of class
