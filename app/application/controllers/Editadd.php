<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Editadd extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->load->model('Users_model');
        $this->seo->SetValues('Title', "Adds");
        $this->seo->SetValues('Description', "Adds");
        $this->is_ajax = 0;
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}

        if (!empty($this->session->userdata['logged_in'])) {
            $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
            $this->user_id = $this->session->userdata['logged_in']['id'];
            $app_id = $this->config->item('facebook_app_id'); //"1648455885427227";
            $facebook_app_secret = $this->config->item('facebook_app_secret'); //"028ead291200f399ce7f1c8aa7232a31";
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
            redirect('/');
        }
    }

    public function index($adAccountId = NULL) {

        $limit = "";
        $data = new stdClass();

        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $adAccountId = $this->input->post('adAccountId');
            $campaignId = $this->input->post('campaignId');
            $adsetId = $this->input->post('adsetId');
            $addId = $this->input->post('addId');
            $limit = $this->input->post('limit');

            $data->adAccountData = $this->getCurlAdd($campaignId, $adAccountId, $adsetId, $addId);
            $data->addAccountId = $adAccountId;
            $data->adaccounts = $data->adAccountData['response'];
            $data->adaccountsCount = $data->adAccountData['count'];
            $data->error = $data->adAccountData['error'];
            $data->success = $data->adAccountData['success'];

            echo $this->load->view('edit_add', $data, true);
        } else {
            $this->load->view("edit_add");
        }
    }

    function getPrintResult($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }

    function getCurlAdd($campaignId, $adAccountId, $adsetId, $addId) {

        $adSetFields = "id,account_id,name,effective_status";

        $resultArray = array();
        //$adAccountId = "act_" . $adAccountId;
        $campaignFields = $adSetFields;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/?fields=$campaignFields" . "&access_token=" . $this->access_token;

        try {

            // exit;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            // $this->getPrintResult($response);
            $dataArray = $this->getFinalResult($response, $addId);
            if ($response) {
                $resultArray = array(
                    "count" => 1,
                    "response" => $dataArray,
                    "error" => "",
                    "success" => ""
                );
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getFinalResult($dataArray, $addId) {


        $returnRow = array();
        //  $this->getPrintResult($dataArray);
        $addName = $dataArray['name'];
        // $campaignObjective = $dataArray['objective'];

        $returnRow['add'] = array(
            'addId' => $addId,
            'addName' => $addName
                // 'campaignObjective' => $campaignObjective
        );
        return $returnRow;
    }

    function loadModal() {
        $data = new stdClass();
        $data->add = 1;
        //var_dump($data);
        echo $this->load->view('modals_again', $data, true);
        //$this->load->view("modals", $data);
    }

    function saveAdd() {

        $addId = $this->input->post('addId');

        $addName = $this->input->post('addName');


        $add = new Ad($addId);
        $add->name = $addName;

        try {
            $add->update();
            echo "100";
        } catch (Exception $ex) {

            $ex->getMessage();
            echo "200";
        }
    }

}

//end of class
