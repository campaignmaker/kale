<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Logout extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->seo->SetValues('Title', "Logout");
        $this->seo->SetValues('Description', "Logout");
    }

    public function index() {
		$this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('listing');
        $this->session->unset_userdata('user_subs');
		$this->session->unset_userdata('last_page');
        $this->session->sess_destroy();
        redirect($this->config->item('site_url').'wp-login.php?action=logout');
    }

}
