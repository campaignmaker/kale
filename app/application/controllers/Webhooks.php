<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Webhooks extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->load->model('Upgrade_model');
        $this->load->model('Users_model');
        $this->load->library("braintree_lib");
        $this->seo->SetValues('Title', "Register");
        $this->seo->SetValues('Description', "Register");
    }

    public function index() {
//        echo "<pre>";
//        $subscription = Braintree_Subscription::find('j5qdhr');
//        //print_r($subscription);
//        echo "<br>";
//        echo $subscription->billingPeriodEndDate->format('Y-m-d');
//        echo "<br>";
//        echo $subscription->billingPeriodStartDate->format('Y-m-d');
//        echo "<br>";
//        echo $subscription->currentBillingCycle;
//        echo "<br>";
//        echo $subscription->daysPastDue;
//        echo "<br>";
//        echo $subscription->status;
//        echo "<br>";
//        echo $subscription->updatedAt->format('Y-m-d H:i:s');


        if (isset($_GET["bt_challenge"])) {
            echo(Braintree_WebhookNotification::verify($_GET["bt_challenge"]));
        }

        if (
                isset($_POST["bt_signature"]) &&
                isset($_POST["bt_payload"])
        ) {
            $webhookNotification = Braintree_WebhookNotification::parse(
                            $_POST["bt_signature"], $_POST["bt_payload"]
            );

            $message = "[Webhook Received " . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
                    . "Kind: " . $webhookNotification->kind . " | "
                    . "Subscription: " . $webhookNotification->subscription->id . "\r\n";

            file_put_contents("webhook.log", $message, FILE_APPEND);
        }
    }

}
