<?php
use FacebookAds\Api;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Insights;
use FacebookAds\Object\AdImage;
use FacebookAds\Object\Fields\AdImageFields;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use FacebookAds\Object\Fields\AdCampaignFields;
use FacebookAds\Object\AdCampaign;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class VideoCron extends CI_Controller {
    private $update_date;
    
    function __construct() {
        parent::__construct();
        ini_set('display_errors', 1);
        error_reporting(E_ALL | E_STRICT);
        #ini_set('display_errors', 0);
        #error_reporting(0);
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        $this->load->model('Spendcron_model');
    }
    
    public function asArray($data) {
        foreach ($data as $k => $v)
            $data[$k] = $v->getData();
        return $data;
    }

    function index() {
        $cron_start_time = date('d-m-y H:i:s A', time());
        echo "<BR> CRON START TIME : " . $cron_start_time . "<br>";
        #$whereArr = array('id' => '381');
        #$userArr = $this->Spendcron_model->getData('users', $whereArr);
        $userArr = $this->Spendcron_model->getData('users');
        if(!empty($userArr)){
            foreach($userArr as $user){
                $this->access_token = $user->accesstoken;
                $whereArr = array('user_id' => $user->id);
                #$whereArr = array('user_id' => $user->id, 'id' => 805);
                $userAdsArr = $this->Spendcron_model->getData('user_ad_account', $whereArr);
                Api::init('1198746903472361', '65110e5912df5b74d01a1f78d9ce3047', $this->access_token);
                if(!empty($userAdsArr)){
                    foreach($userAdsArr as $usesAds){
                        $adAccountId = "act_".$usesAds->ad_account_id;
                        
                        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/advideos?fields=source,title,picture&access_token=" . $this->access_token;
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        
                        $response = curl_exec($ch);
                        curl_close($ch);
                        $results = json_decode($response);
                        $str = '';
                        $finalArr = array();
                        if (isset($results->error)) {
                            // $option = "<option value=''>No Record Found!</option>";
                        } else {
                            if ($results->data) {
                                foreach ($results->data as $result) {
                                    $videoId = $result->id;
                                    $whereArr = array('user_id' => $user->id, 'adAccountId' => $usesAds->ad_account_id, 'video_id' => $videoId);
                                    $userArr = $this->Spendcron_model->getData('user_ad_videos', $whereArr);
                                    if(empty($userArr)){
                                        $url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$result->id."/thumbnails?access_token=" . $this->access_token;
                            
                                        $ch = curl_init($url1);
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                        
                                        $response1 = curl_exec($ch);
                                        curl_close($ch);
                                        $results1 = json_decode($response1);
                                        $viewUrl = $results1->data[0]->uri;

                                        $img_hash = $this->videoThumbImg($viewUrl, $usesAds->ad_account_id);
                                        if($img_hash != 'not'){
                                            $insertArr = array('user_id' => $user->id, 'adAccountId' => $usesAds->ad_account_id, 'video_id' => $videoId, 'url' => $viewUrl, 'img_hash' => $img_hash);
                                            $userAdsArr = $this->Spendcron_model->insertData('user_ad_videos', $insertArr);
                                        }
                                    }
                                }
                            }
                            $flag = true;
                            if(empty($results->paging->cursors->after)){
                                $flag = false;
                            }
                            else{
                                $nexturl = $url."&after=".$results->paging->cursors->after;
                            }
                            while ($flag) {
                                $ch = curl_init($nexturl);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                
                                $response = curl_exec($ch);
                                curl_close($ch);
                                $results = json_decode($response);
                                if ($results->data) {
                                    foreach ($results->data as $result) {

                                        $videoId = $result->id;
                                        $whereArr = array('user_id' => $user->id, 'adAccountId' => $usesAds->ad_account_id, 'video_id' => $videoId);
                                        $userArr = $this->Spendcron_model->getData('user_ad_videos', $whereArr);
                                        if(empty($userArr)){
                                            $url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$result->id."/thumbnails?access_token=" . $this->access_token;
                                
                                            $ch = curl_init($url1);
                                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                            
                                            $response1 = curl_exec($ch);
                                            curl_close($ch);
                                            $results1 = json_decode($response1);
                                            $viewUrl = $results1->data[0]->uri;

                                            $img_hash = $this->videoThumbImg($viewUrl, $usesAds->ad_account_id);
                                            if($img_hash != 'not'){
                                                $insertArr = array('user_id' => $user->id, 'adAccountId' => $usesAds->ad_account_id, 'video_id' => $videoId, 'url' => $viewUrl, 'img_hash' => $img_hash);
                                                $userAdsArr = $this->Spendcron_model->insertData('user_ad_videos', $insertArr);
                                            }
                                        }
                                    }
                                }
                                if(empty($results->paging->cursors->after)){
                                    $flag = false;
                                }
                                else{
                                    $nexturl = $url."&after=".$results->paging->cursors->after;
                                }
                            }
                        }
                    }
                }
            }
        }
        $cron_end_time = date('d-m-y H:i:s A', time());
        echo "<BR> CRON END TIME : " . $cron_end_time . "<br>";
    }

    function videoThumbImg($viewUrl, $adaccountid){
        $tempArr = explode('/', $viewUrl);
        $tempArr1 = explode('?', $tempArr[5]);
        
        $filename = $tempArr1[0];//rand().'.jpg';

        $filenameOut = FCPATH.'uploads/campaign/temp/' . $filename;

        $contentOrFalseOnFailure = @file_get_contents($viewUrl, true);
        if($contentOrFalseOnFailure === FALSE) { 
            return "not";
        }
        else{
            $byteCountOrFalseOnFailure = file_put_contents($filenameOut, $contentOrFalseOnFailure);

            $image = new AdImage(null, "act_".$adaccountid);
            $image->{AdImageFields::FILENAME} = $filenameOut;
            $image->create();
            return $image->hash;
        }
    }
}
?>