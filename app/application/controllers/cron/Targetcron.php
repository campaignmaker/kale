<?php
use FacebookAds\Api;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use FacebookAds\Object\Fields\AdCampaignFields;
use FacebookAds\Object\AdCampaign;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Targetcron extends CI_Controller {
    private $update_date;
    
    function __construct() {
        parent::__construct();
        ob_start();
        set_time_limit(0);
        ini_set('display_errors', 1);
        error_reporting(E_ALL | E_STRICT);
        #ini_set('display_errors', 0);
        #error_reporting(0);
        $this->load->model('Spendcron_model');
        $this->access_token = 'EAARCQQNbaOkBAK2Tk2nJAZAXBlldisXlp6GD8dVSUM0ToQkZAMoFe7VlJtnhbwMRoWxStdTWLkAaXb3KJb9G2Ol2N6n1QEOwttXtxlU6KZBejaI1bfCTvu5PjfLbNHy8e4LmaGpvZARy156qvESYKlcFWGwUp6QZD';
    }
    
    public function asArray($data) {
        foreach ($data as $k => $v)
            $data[$k] = $v->getData();
        return $data;
    }

    function index() {
        $cron_start_time = date('d-m-y H:i:s A', time());
        echo "<BR> CRON START TIME : " . $cron_start_time . "<br>";
        
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adTargetingCategory&access_token=" . $this->access_token . '&limit=100';
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if(!empty($response['data'])){
            $this->Spendcron_model->deleteData('demographic_data');
            foreach ($response['data'] as $key => $value) {
                $insertData = array(
                                    'item_id'=> !empty($value['id']) ? $value['id'] : '',
                                    'name'=> !empty($value['name']) ? $value['name'] : '',
                                    'type'=> !empty($value['type']) ? $value['type'] : '',
                                    'description'=> !empty($value['description']) ? $value['description'] : '',
                                    'audience_size'=> !empty($value['audience_size']) ? $value['audience_size'] : '',
                                    );
                $this->Spendcron_model->insertData('demographic_data', $insertData);
            }
        }

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adTargetingCategory&class=behaviors&access_token=" . $this->access_token . '&limit=100';
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if(!empty($response['data'])){
            foreach ($response['data'] as $key => $value) {
                $insertData = array(
                                    'item_id'=> !empty($value['id']) ? $value['id'] : '',
                                    'name'=> !empty($value['name']) ? $value['name'] : '',
                                    'type'=> !empty($value['type']) ? $value['type'] : '',
                                    'description'=> !empty($value['description']) ? $value['description'] : '',
                                    'audience_size'=> !empty($value['audience_size']) ? $value['audience_size'] : '',
                                    );
                $this->Spendcron_model->insertData('demographic_data', $insertData);
            }
        }
        
        $cron_end_time = date('d-m-y H:i:s A', time());
        echo "<BR> CRON END TIME : " . $cron_end_time . "<br>";
    }
}
?>