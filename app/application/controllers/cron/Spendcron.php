<?php
use FacebookAds\Api;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use FacebookAds\Object\Fields\AdCampaignFields;
use FacebookAds\Object\AdCampaign;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Spendcron extends CI_Controller {
    private $update_date;
    
    function __construct() {
        parent::__construct();
        //ini_set('display_errors', 1);
        //error_reporting(E_ALL | E_STRICT);
        ini_set('display_errors', 0);
        error_reporting(0);
        $this->load->model('Spendcron_model');
    }
    
    public function asArray($data) {
        foreach ($data as $k => $v)
            $data[$k] = $v->getData();
        return $data;
    }

    function index() {
        $cron_start_time = date('d-m-y H:i:s A', time());
        echo "<BR> CRON START TIME : " . $cron_start_time . "<br>";
        //$whereArr = array('id' => '103');
        //$userArr = $this->Spendcron_model->getData('users', $whereArr);
        $userArr = $this->Spendcron_model->getData('users');
        if(!empty($userArr)){
            foreach($userArr as $user){
                $this->access_token = $user->accesstoken;
                $whereArr = array('user_id' => $user->id);
                $userAdsArr = $this->Spendcron_model->getData('user_ad_account', $whereArr);
                
                $whereArr = array('user_id' => $user->id);
                $userSubScArr = $this->Spendcron_model->getData('user_subscription', $whereArr);
                $billing_start_date = $userSubScArr[0]->billing_start_date;
                $billing_end_date = $userSubScArr[0]->billing_end_date;
                
                if(!empty($userAdsArr)){
                    foreach($userAdsArr as $usesAds){
                        $adAccountId = $usesAds->ad_account_id;
                        
                        Api::init('1198746903472361', '65110e5912df5b74d01a1f78d9ce3047', $this->access_token);
                        
                        $whereArr = array('user_id' => $user->id, 'ad_account_id' => $adAccountId);
                        $maxDateArr = $this->Spendcron_model->getDataMaxDate('user_daily_spend', $whereArr);
                        $lastInsertdate = $maxDateArr[0]->date;
                        
                        if(!empty($lastInsertdate)){
                            $date = $lastInsertdate;
                        }
                        else{
                            $date = $billing_start_date;
                        }
                        $this->date_to = date("Y-m-d", strtotime(date("Y-m-d")) - 86400);;
                        
                        try {
                            $campaignObj = new Campaign("act_".$adAccountId);
                            $fields = array(
                                CampaignFields::ID,
                                CampaignFields::NAME
                            );

                            $res = $campaignObj->read($fields);
                            while ($date <= $this->date_to) {
                                try {
                                    $campaignObj = new Campaign("act_".$adAccountId);
                                    $params = array(
                                        'time_range' => array(
                                            'since' => $date,
                                            'until' => $date,
                                        ),
                                        'time_increment' => 'all_days',
                                    );
                                    $fields = array('spend');
                                    $insights = $campaignObj->getInsights($fields, $params);
                                    
                                    $res = $insights->getObjects();
                                    $res = $this->asArray($res);
                                    $spend = !empty($res[0]['spend']) ? $res[0]['spend'] : "0";

                                    $whereArr = array('user_id' => $user->id, 'ad_account_id' => $adAccountId, 'date' => $date);
                                    $checkDailySpendArr = $this->Spendcron_model->getData('user_daily_spend', $whereArr);
                                    if(empty($checkDailySpendArr)){
                                        $insertArr = array('user_id' => $user->id, 'ad_account_id' => $adAccountId, 'spend' => $spend, 'date' => $date, 'date_added' => date('Y-m-d H:i:s'));
                                        $userAdsArr = $this->Spendcron_model->insertData('user_daily_spend', $insertArr);
                                    }

                                    $date = date("Y-m-d", strtotime($date) + 86400);
                                } 
                                catch (Exception $ex) {
                                    echo "<PRE>";print_R($ex);exit;
                                }
                            }
                        } catch (Exception $ex) {
                            #echo "<PRE>";print_R($ex);exit;
                        }
                    }
                }
            }
        }
        $cron_end_time = date('d-m-y H:i:s A', time());
        echo "<BR> CRON END TIME : " . $cron_end_time . "<br>";
    }
}
?>