<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
use FacebookAds\Api;

use FacebookAds\Object\Ad;

use FacebookAds\Object\AdUser;

use Facebook\Facebook;

use Facebook\FacebookApp;

use FacebookAds\Object\AdAccount;

use FacebookAds\Object\Fields\AdAccountFields;

use FacebookAds\Object\Campaign;

use FacebookAds\Object\Fields\CampaignFields;

use FacebookAds\Object\Fields\AdSetFields;

use FacebookAds\Object\Values\AdObjectives;

use FacebookAds\Object\Insights;

use FacebookAds\Object\Fields\InsightsFields;

use FacebookAds\Object\Values\InsightsPresets;

use FacebookAds\Object\Fields\AdFields;

use FacebookAds\Object\Values\InsightsActionBreakdowns;

use FacebookAds\Object\Values\InsightsBreakdowns;

use FacebookAds\Object\Values\InsightsLevels;

use FacebookAds\Object\Values\InsightsIncrements;

use FacebookAds\Object\Values\InsightsOperators;

use Facebook\FacebookRequest;

use Facebook\FacebookResponse;
class Automaticoptimization extends CI_Controller {
	public $access_token;

    public $user_id;

    public $add_account_id;

    public $add_title;

    public $is_ajax;

    public $limit;
    public $field;
    public function __construct() {
		 error_reporting(1);
        parent::__construct();
        $this->load->model('Users_model');
		$this->load->model('Campaign_model');
        $this->is_ajax = 0;
        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
        $this->user_id = $this->session->userdata['logged_in']['id'];
        $this->getAdAccount();
       $response = $this->Users_model->get_cancel_subscription($this->session->userdata['logged_in']['email']);
		if($response == 'No Subscribe'){
		if($this->session->userdata['logged_in']['accesstoken'] == ''){
				header("Location:".$this->config->item('site_url').'connect-to-facebook/');
		}else{	redirect($this->config->item('site_url').'one-step-closer/'); }
		}elseif($response == 'Cancelled' || $response == 'Expired'){
			//redirect($this->config->item('site_url').'final-payment/');
		}
        if (isset($this->session->userdata['logged_in']) && !empty($this->session->userdata['logged_in'])) {
            $app_id = $this->config->item('facebook_app_id');
            $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
            redirect('/');
        }
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));     
    }

    public function index($adAccountId) {
		
        //$data = $this->Users_model->Getautomaticoptimization($this->user_id);
		//echo $this->input->get('adaccountid', TRUE);
		$limit = "lifetime";//$this->dateval;//$this->sDate."#".$this->eDate;

        $data = new stdClass();
        $adAccountId = $adAccountId;//$this->input->get('adaccountid', TRUE);//$this->input->post('adAccountId');
        $limit = "lifetime";//$this->input->post('limit');
        $field = "spend";//$this->input->post('field');
        $this->session->set_userdata('dateval', $limit);
		if(empty($adAccountId)){
			$adAccountId = $this->add_account_id;
		}
        if (!empty($adAccountId)) {
            $this->setAddAccountId($adAccountId);
            $this->getCurrency();
            if (strpos($limit, '#') !== false) {
                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
            } else {
                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
            }
        } else {
            $data->adAccountData = $this->getAdAccount();
        }
        $data->addAccountId = $this->add_account_id;
        $data->adaccounts = $data->adAccountData['response'];
        $data->adaccountsCount = $data->adAccountData['count'];
        $data->error = $data->adAccountData['error'];
        $data->success = $data->adAccountData['success'];
        $data->limit = $limit;
        $accountArr = $this->Users_model->getUserAddAccountName($adAccountId);
        $data->accountName = $accountArr[0]->add_title;

        #echo "<PRE>";print_R($data);exit;
        //$data->automaticoptimizationdata = $this->Campaign_model->Getautomaticoptimization($this->user_id);
        

        $data->adruleslibrarydata = $this->Getadrulelibdata();
        $data->campaigns_list = $this->get_campaigns_list($data->adaccounts['campaigns']);
        
       // echo "<pre>"; print_r($data->adruleslibrarydata); echo "</pre>"; exit;
        
		loadView('automaticoptimization', $data);
    }
    
    function get_campaigns_list($campaigns){
           $campaigns_list = array();
        	foreach($campaigns as $temp_compaign){
        	   // if($temp_compaign['campaign_effective_status'] == 'Active'){
                 $campaigns_list[$temp_compaign['campaign_id']]=$temp_compaign['campaign_name'];
        	    //}
	}
        return $campaigns_list;
    }

   public function createRule($adAccountId){
       
      if(isset($_POST)){
         $step = $this->input->post("step");
           
      }
       
       
		$limit = "lifetime";

        $data = new stdClass();
        $adAccountId = $adAccountId;//$this->input->get('adaccountid', TRUE);//$this->input->post('adAccountId');
        $limit = "lifetime";//$this->input->post('limit');
        $field = "spend";//$this->input->post('field');
        $this->session->set_userdata('dateval', $limit);
		if(empty($adAccountId)){
			$adAccountId = $this->add_account_id;
		}
        if (!empty($adAccountId)) {
            $this->setAddAccountId($adAccountId);
            $this->getCurrency();
            if (strpos($limit, '#') !== false) {
                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
            } else {
                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
            }
        } else {
            $data->adAccountData = $this->getAdAccount();
        }
        $data->addAccountId = $this->add_account_id;
        $data->adaccounts = $data->adAccountData['response'];
        $data->adaccountsCount = $data->adAccountData['count'];
        $data->error = $data->adAccountData['error'];
        $data->success = $data->adAccountData['success'];
        $data->limit = $limit;
        $accountArr = $this->Users_model->getUserAddAccountName($adAccountId);
        $data->accountName = $accountArr[0]->add_title;

        #echo "<PRE>";print_R($data);exit;
        //$data->automaticoptimizationdata = $this->Campaign_model->Getautomaticoptimization($this->user_id);

        $data->adruleslibrarydata = $this->Getadrulelibdata();

       // echo "<pre>"; print_r($data->adruleslibrarydata); echo "</pre>"; exit;
        $data->campaigns_list = $this->get_campaigns_list($data->adaccounts['campaigns']);
        
        $data->step = (int)$step+1;
        
		loadView('createRule', $data);
       
       
       
   }





    public function create_automaticoptimization() {
        //echo "<PRE>";print_R($_POST);exit;
        
     $autoopti = $this->input->post('');
      $autoopti = $_POST;

     
       $evaluation_spec_dyn = array();
       
       $static_params = '{ "evaluation_type" : "SCHEDULE", "filters" : [ { "field": "entity_type", "value": "AD", "operator": "EQUAL" }, { "field": "time_preset", "value": "'.$autoopti['timeFrame'].'", "operator": "EQUAL" },{"field": "campaign.effective_status","value": "ACTIVE","operator": "IN"}, { "field": "campaign.id", "value": "'.$autoopti['compaign'].'", "operator": "IN" }  ] }';
       $static_params_array =  json_decode($static_params);   
  
       for ($incre=0; $incre < 2; $incre++){
       
           $static_params_array->filters[$incre+4]->field = $autoopti['rules']['datapoint'][$incre];
          
            if( $static_params_array->filters[$incre+4]->field == 'spent' || strpos( $static_params_array->filters[$incre+4]->field, 'cost' ) !== false) {
               $static_params_array->filters[$incre+4]->value =   $autoopti['rules']['fieldvalue'][$incre]*100;
            }else{
               $static_params_array->filters[$incre+4]->value =   $autoopti['rules']['fieldvalue'][$incre];
            }
           
           //$static_params_array->filters[$incre+4]->value =   $autoopti['rules']['fieldvalue'][$incre]*100;
           $static_params_array->filters[$incre+4]->operator = $autoopti['rules']['operator'][$incre] ;
         
       }
       
      
      $evaluation_spec = json_encode($static_params_array);
    
     
        if($this->input->post('theidautocamp') == "0"){
            
            $postFields = array (
              'name' => $autoopti['ruleName'],
              'evaluation_spec' => $evaluation_spec,
               'execution_spec' => '{ "execution_type": "'.$autoopti['actions'].'" }',
              'schedule_spec' => '{ "schedule_type": "'.$autoopti['frequency'].'" }',
              'access_token' => $this->access_token
            );
          
            $url = 'https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $autoopti['addAccountId'] . '/adrules_library';
          

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postFields
            ));

            $response = curl_exec($ch);
            curl_close($ch);
           
        }
        else{
            
             $postFields = array (
              'name' => $autoopti['ruleName'],
              'evaluation_spec' =>  $evaluation_spec,
              'execution_spec' => '{ "execution_type": "'.$autoopti['actions'].'" }',
              'schedule_spec' => '{ "schedule_type": "'.$autoopti['frequency'].'" }',
              'access_token' => $this->access_token
            );
           
          
            $url = 'https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/' . $this->input->post('theidautocamp');
         
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postFields
            ));

            $response = curl_exec($ch);
            curl_close($ch);
           
        }
       echo $response;
       exit;
       // redirect('automaticoptimization/'.$autoopti['addAccountId']);
    }
	public function update_aoactivation() {
        #echo "<PRE>";print_R($_POST);exit;
        /*global $data;
		$autooptiac = $this->input->post('isactive');
		$autooptid = $this->input->post('Id');
		
		$this->Campaign_model->Updateautooptimizationactivation($autooptid,$autooptiac);
		*/
		$postFields = array();
		$autooptiac = $this->input->post('isactive');
		$autooptid = $this->input->post('Id');
		if($autooptiac == "1"){
    		$postFields = array (
              'status' => 'ENABLED',
              'access_token' => $this->access_token
            );
		}
		else{
		    $postFields = array (
              'status' => 'DISABLED',
              'access_token' => $this->access_token
            );
		}
        //$url = 'https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->getAddAccountId() . '/adrules_library?name='.$autoopti['rulename'].'&evaluation_spec={ "evaluation_type" : "SCHEDULE", "filters" : [ { "field": "entity_type", "value": "AD", "operator": "EQUAL" }, { "field": "time_preset", "value": "'.$autoopti['timeframe'].'", "operator": "EQUAL" }, { "field": "delivery_info", "value": ["ACTIVE"], "operator": "IN" }, { "field": "campaign.id", "value": ['.$autoopti['campaignsid'].'], "operator": "IN" }, { "field": "'.$autoopti['param1'].'", "value": '.$autoopti['param3'].', "operator": "'.$autoopti['param2'].'" }, { "field": "'.$autoopti['param4'].'", "value": '.$autoopti['param6'].', "operator": "'.$autoopti['param5'].'" } ] }&execution_spec={ "execution_type": "'.$autoopti['ruleMet'].'" }&schedule_spec={ "schedule_type": "'.$autoopti['checkRule'].'" }&access_token=' . $this->access_token;
        $url = 'https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/' . $autooptid;
        //echo $url;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postFields
        ));

        $response = curl_exec($ch);
        //print_r($response);
        curl_close($ch);
		
		
		echo "Successfully";
        
    }
	
	        public function getCurrency() {

            $adAccountId = "act_" . $this->getAddAccountId();

            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/?fields=name,currency&access_token=" . $this->access_token;



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);



            $currency = 'USD';

            if (!empty($response['currency'])) {

                $currency = $response['currency'];

            }

        $locale = 'en-US'; //browser or user locale

        $fmt = new NumberFormatter($locale . "@currency=$currency", NumberFormatter::CURRENCY);

        $symbol = $fmt->getSymbol(NumberFormatter::CURRENCY_SYMBOL);

        $this->session->set_userdata('cur_currency', $symbol);

    }


    /* From DB */
	
	public function deleteap($id)
    {  

         $postFields = array (
          'access_token' => $this->access_token
        );
        $url = 'https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/' . $id;
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        //$result = json_decode($result);
        curl_close($ch);
		//$this->db->where('id', $id);
		///$this->db->delete('automaticoptimization_new');
		//echo "Successfully";
		//redirect('automaticoptimization/index');
    }


    protected function getAdAccount() {

        $result = $this->Users_model->getUserAddAccountId($this->user_id);



        if (count($result) > 0) {

            if (count($result) == 1) {

                $finalRes = "";

                $this->setAddAccountId($result[0]->ad_account_id);

                $this->getCurrency();

                $this->add_account_id = $result[0]->ad_account_id;

                $this->add_title = $result[0]->add_title;

                // $limit=date('Y-m-d', strtotime('-7 days'))."#".date('Y-m-d');

                //$finalRes = $this->getCurlCampaignsDateRange($limit);

                //$finalRes = $this->getCurlCampaigns("");

                $finalRes = $this->getCurlCampaignsDateRange_new($this->dateval);

                // $this->getPrintResult($finalRes);

                $resultArray = array(

                    "count" => $finalRes['count'],

                    "response" => $finalRes['response'],

                    "error" => $finalRes['error'],

                    "success" => $finalRes['succes']

                    );

                // End Get Ad  

            } else {

                $resultArray = array(

                    "count" => 2,

                    "response" => $result,

                    "error" => "",

                    "success" => ""

                    );

            }

        } else {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => "No data available against your account.",

                "success" => ""

                );

        }

        //  $this->getPrintResult($resultArray);

        return $resultArray;

    }



    /* From LIVE */



    protected function fbAdAccount($add_account_id, $selected_limit) {

        try {

            $resultArray = $this->getAllCampaigns($add_account_id, $selected_limit);

        } catch (Exception $e) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        return $resultArray;

    }



    protected function getAllCampaigns($add_account_id, $selected_limit) {

        try {

            $adAccountOjb = new AdAccount($add_account_id);

            $insightsArray = array();

            $campaignDataArray = array();



            $fields = $this->getCampaignFields();

            $insights = $this->getCampaignInsightFields();

            $params = $this->getCampaignParams($selected_limit);



            $response = $adAccountOjb->getCampaigns($fields);



            foreach ($response as $row) {

                try {



                    $campaignObj = new Campaign($row->id);

                    $res = $this->get_hub_insight_per($campaignObj, $insights, $params);

                    foreach ($res as $rs) {

                        //$rs->account_id;

                        // $this->getPrintResult($res);

                        $insightsArray[] = array(

                            "account_name" => $rs->account_name,

                            "actions_per_impression" => $rs->actions_per_impression,

                            "campaign_id" => $rs->campaign_id,

                            "campaign_name" => $rs->campaign_name,

                            "cost_per_action_type" => $rs->cost_per_action_type,

                            "cost_per_total_action" => $rs->cost_per_total_action,

                            "cost_per_unique_click" => $rs->cost_per_unique_click,

                            "cost_per_inline_link_click" => $rs->cost_per_inline_link_click,

                            "cost_per_inline_post_engagement" => $rs->cost_per_inline_post_engagement,

                            "cpm" => $rs->cpm,

                            "cpp" => $rs->cpp,

                            "ctr" => $rs->ctr,

                            "date_start" => $rs->date_start,

                            "date_stop" => $rs->date_stop,

                            "frequency" => $rs->frequency,

                            "impressions" => $rs->impressions,

                            "inline_link_clicks" => $rs->inline_link_clicks,

                            "inline_post_engagement" => $rs->inline_post_engagement,

                            "reach" => $rs->reach,

                            "spend" => $rs->spend,

                            "total_action_value" => $rs->total_action_value,

                            "total_actions" => $rs->total_actions,

                            "total_unique_actions" => $rs->total_unique_actions,

                            "unique_clicks" => $rs->unique_clicks,

                            "unique_ctr" => $rs->unique_ctr

                            );

                    }

                    if ($insightsArray) {

                        $campaignDataArray[] = array(

                            "id" => $row->id,

                            "name" => $row->name,

                            "created_time" => $row->created_time,

                            "effective_status" => $row->effective_status,

                            "status_paused" => $row->status_paused,

                            "AllArray" => $row,

                            "insights" => $insightsArray

                            );

                        $insightsArray = array();

                    }

                } catch (Exception $e) {

                    return $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $e->getMessage(),

                        "success" => ""

                        );

                }

            }



            if (count($campaignDataArray) > 1) {

                $finalArray = $this->getFinalResult($campaignDataArray);

                $resultArray = array(

                    "count" => 1,

                    "response" => $finalArray,

                    "error" => "",

                    "success" => ""

                    );

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => "No record found.",

                    "success" => ""

                    );

            }

        } catch (Exception $e) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        return $resultArray;

    }



    /* Privat functions */



    private function setAddAccountId($adAccountId) {

        $this->add_account_id = $adAccountId;

    }



    private function getAddAccountId() {

        return $this->add_account_id;

    }



    /* End Private */



    function get_hub_insight_per($obj, $insights, $param) {

        try {

//            if (!empty($param))

//            {

//                $params = array(

//                    $param

//                );

//                return $response = $obj->getInsights($insights, $params);

//            }

//            else

//            {

            return $response = $obj->getInsights($insights);

            // }

        } catch (Exception $e) {

            return $e->getMessage();

        }

    }



    function getCampaignFields($curl) {

        if ($curl == 1) {

            $fields = "id,account_id,name,objective,buying_type,created_time,start_time,stop_time,effective_status,currency,configured_status";

        } else {

            $fields = array(

                CampaignFields::ID,

                CampaignFields::ACCOUNT_ID,

                CampaignFields::NAME,

                CampaignFields::OBJECTIVE,

                CampaignFields::BUYING_TYPE,

                CampaignFields::PROMOTED_OBJECT,

                CampaignFields::ADLABELS,

                CampaignFields::CREATED_TIME,

                CampaignFields::START_TIME,

                CampaignFields::STOP_TIME,

                CampaignFields::UPDATED_TIME,

                CampaignFields::EFFECTIVE_STATUS,

                CampaignFields::STATUS_PAUSED,

                );

        }

        return $fields;

    }



    function getCampaignInsightFields($curl) {

        if ($curl == 1) {

            $fields = "{date_start,date_stop,buying_type,campaign_id,actions{action_type,value}}";

        } else {



            $fields = array(

                InsightsFields::ACCOUNT_ID,

                InsightsFields::IMPRESSIONS,

                InsightsFields::UNIQUE_CLICKS,

                InsightsFields::REACH,

                InsightsFields::INLINE_LINK_CLICKS,

                InsightsFields::COST_PER_INLINE_LINK_CLICK,

                InsightsFields::COST_PER_UNIQUE_CLICK,

                InsightsFields::SPEND,

                InsightsFields::COST_PER_ACTION_TYPE

                );

        }

        return $fields;

    }



    function getCampaignParams($type) {

        return $params = ".date_preset($type)";

    }



    function getFinalResult($campaignDataArray) {

        $activeCampaigns = 0;

        $inActiveCampaigns = 0;

        $cost_per_unique_click = 0;

        $total_cost_per_inline_click = 0;

        $impressions = 0;

        $total_inline_clicks = 0;

        $reach = 0;

        $unique_clicks = 0;

        $unique_ctr = 0;

        $spent = 0;

        $actions = 0;

        $cost_per_total_action = 0;



        $inline_clicks = 0;

        $cost_per_inline_clicks = 0;

        $pendingCampaigns = 0;

        $disapprovedCampaigns = 0;

        $total = 0;

        $total1 = 0;

        $total2 = 0;

        $conversion = 0;

        #echo "<PRE>";print_R($campaignDataArray);exit;

        $returnRow = array();

        foreach ($campaignDataArray as $row) {

            if ($row['effective_status'] == "ACTIVE" || $row['effective_status'] == "PREAPPROVED") {

                $activeCampaigns++;

                $status = "Active";

            } 

            else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {

                $inActiveCampaigns++;

                $status = "Inactive";

            } 

            else if ($row['effective_status'] == "PENDING_REVIEW") {

                $pendingCampaigns++;

                $status = "In Review";

            } 

            else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {

                $disapprovedCampaigns++;

                $status = "Denied";

            }

            $total++;

            

            if (isset($row['insights'])) {

                // echo $row['name']."<br>";

                $inline_post_engagement1 = $like1 = '0';

                $conversion = $cost_per_conversion = 0;

                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';

                if (isset($row['insights']['data'][0])) {

                    foreach ($row['insights']['data'][0]['actions'] as $rs) {

                        #echo "<PRE>";print_r($row['insights']);exit;

                        if ($row['insights']['data'][0]['clicks']) {

                            $inline_clicks = $row['insights']['data'][0]['clicks'];

                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];

                        }

                        if ($rs['action_type'] == "leadgen.other") {

                            if($rs['value']){

                                $conversion = $rs['value'];

                            }

                            else{

                                $conversion = 0;

                            }

                        } 

                        if ($rs['action_type'] == "offsite_conversion") {

                            if($rs['value']){

                                $conversion += $rs['value'];

                                $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];

                            }

                            else{

                                $conversion = 0;

                                $cost_per_conversion = 0;

                            }

                        } 

                        if($rs['action_type'] == 'page_engagement'){

                            if($rs['value']){

                                $total1++;

                                $inline_post_engagement1 = $rs['value'];

                            }

                            else{

                                $inline_post_engagement1 = 0;

                            }

                        }

                        if($rs['action_type'] == 'like'){

                            if($rs['value']){

                                $total2++;

                                $like1 = $rs['value'];

                            }

                            else{

                                $like1 = 0;

                            }

                        }

                    }

                    foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {

                        if($rs['action_type'] == 'page_engagement'){

                            if($rs['value']){

                                $cost_per_inline_post_engagement1 = $rs['value'];

                            }

                            else{

                                $cost_per_inline_post_engagement1 = 0;

                            }

                        }

                        if ($rs['action_type'] == "leadgen.other") {

                            if($rs['value']){

                                $cost_per_conversion = $rs['value'];

                            }

                            else{

                                $cost_per_conversion = 0;

                            }

                        } 

                        if($rs['action_type'] == 'like'){

                            if($rs['value']){

                                $cost_per_like1 = $rs['value'];

                            }

                            else{

                                $cost_per_like1 = 0;

                            }

                        }

                    }

                }

                

                $total_inline_clicks+=$inline_clicks;

                $total_cost_per_inline_click+=$cost_per_inline_clicks;



                $returnRow['campaigns'][] = array(

                    "campaign_id" => $row['id'],

                    "campaign_name" => $row['name'],

                    "campaign_created_time" => $row['created_time'],

                    "campaign_effective_status" => $status, //$row['effective_status'],

                    "campaign_cost_per_unique_click" => $row['insights']['data'][0]['cost_per_unique_click'],

                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,

                    "campaign_impressions" => $row['insights']['data'][0]['impressions'],

                    "campaign_inline_link_clicks" => $inline_clicks,

                    "campaign_reach" => $row['insights']['data'][0]['reach'],

                    "campaign_unique_clicks" => $row['insights']['data'][0]['unique_clicks'],

                    "campaign_unique_ctr" => $row['insights']['data'][0]['ctr'],

                    "campaign_spent" => $row['insights']['data'][0]['spend'],

                    "campaign_actions" => $conversion,

                    "campaign_cost_per_total_action" => $cost_per_conversion,

                    "campaign_reach" => $row['insights']['data'][0]['reach'],

                    "campaign_objective" => $row['objective'],

                    "inline_post_engagement" => $inline_post_engagement1,

                    "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement1,

                    "page_like" => $like1,

                    "cost_per_like1" => $cost_per_like1,

                    "leadgen" => $leadgen,

                    "cost_leadgen" => $cost_leadgen

                    );



                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];

                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];

                $impressions += $row['insights']['data'][0]['impressions'];

                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];

                $reach += $row['insights']['data'][0]['reach'];

                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];

                $unique_ctr += $row['insights']['data'][0]['ctr'];

                $spent += $row['insights']['data'][0]['spend'];

                $actions += $conversion;

                $cost_per_total_action += $cost_per_conversion;

                $inline_post_engagement += $inline_post_engagement1;

                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;



                $page_like += $like1;

                $cost_per_like += $cost_per_like1;

                $cost_leadgen1 += $cost_leadgen;

                $leadgen1 += $leadgen;



                //Reset   

                $inline_clicks = '--';

                $cost_per_inline_clicks = '--';

            } 

            else {



                $returnRow['campaigns'][] = array(

                    "campaign_id" => $row['id'],

                    "campaign_name" => $row['name'],

                    "campaign_created_time" => $row['created_time'],

                    "campaign_effective_status" => $status, //$row['effective_status'],

                    "campaign_cost_per_unique_click" => 0,

                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,

                    "campaign_impressions" => 0,

                    "campaign_inline_link_clicks" => $inline_clicks,

                    "campaign_reach" => 0,

                    "campaign_unique_clicks" => 0,

                    "campaign_unique_ctr" => 0,

                    "campaign_spent" => 0,

                    "campaign_actions" => 0,

                    "campaign_cost_per_total_action" => 0,

                    "campaign_reach" => 0,

                    "inline_post_engagement" => 0,

                    "cost_per_inline_post_engagement" => 0,

                    "page_like" => 0,

                    "cost_per_like1" => 0,

                    "campaign_objective" => $row['objective'],

                    "leadgen" => 0,

                    "cost_leadgen" => 0

                    );



                $cost_per_unique_click += 0;

                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];

                $impressions += 0;

                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];

                $reach += 0;

                $unique_clicks += 0;

                $unique_ctr += 0;

                $spent += 0;

                $actions += 0;

                $cost_per_total_action += 0;

                $inline_post_engagement += 0;

                $cost_per_inline_post_engagement += 0;



                $page_like += 0;

                $cost_per_like += 0;

                $cost_leadgen1 += 0;

                $leadgen1 += 0;

                

                //Reset   

                $inline_clicks = '--';

                $cost_per_inline_clicks = '--';

            }

        }

        if ($unique_ctr > 0) {

            $unique_ctr = ($unique_ctr / $total);

        }

        if ($total_cost_per_inline_click > 0) {

            $total_cost_per_inline_click = ($total_cost_per_inline_click / $total);

        }

        

        if ($cost_per_inline_post_engagement > 0) {

            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);

        }

        if ($cost_leadgen1 > 0) {

            $cost_leadgen1 = ($cost_leadgen1 / $total3);

        }

        if ($cost_per_like > 0) {

            $cost_per_like = ($cost_per_like / $total2);

        }

        //echo $actions;

        if ($cost_per_total_action > 0) {

            $cost_per_total_action = ($spent / $actions);

        }

        $returnRow['campaigns'] = $this->sksort($returnRow['campaigns'], "campaign_effective_status");

        $returnRow['campaign_header'] = array(

            "campaign_active" => $activeCampaigns,

            "campaign_inactive" => $inActiveCampaigns,

            "campaign_pending" => $pendingCampaigns,

            "campaign_disapproved" => $disapprovedCampaigns,

            "cost_per_unique_click" => $cost_per_unique_click,

            "cost_per_inline_link_click" => $total_cost_per_inline_click,

            "impressions" => $impressions,

            "inline_link_clicks" => $total_inline_clicks,

            "reach" => $reach,

            "unique_clicks" => $unique_clicks,

            "unique_ctr" => $unique_ctr,

            "spent" => $spent,

            "actions" => $actions,

            "actions" => $actions,

            "cost_per_total_action" => $cost_per_total_action,

            "inline_post_engagement" => $inline_post_engagement,

            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,

            "page_like" => $page_like,

            "cost_per_like" => $cost_per_like,

            "leadgen" => $leadgen1,

            "cost_leadgen" => $cost_leadgen1,

            );



        #echo "<PRE>";print_R($returnRow);exit;

        return $returnRow;

    }



    function getPrintResult($array) {

        echo "<pre>";

        print_r($array);

        echo "</pre>";

        exit;

    }



    function getCurlCampaigns($limit) {



        $adAccountId = "";

        $date_preset = "";

        $date_preset1 = "";

        $resultArray = array();

        $adAccountId = "act_" . $this->getAddAccountId();

        $campaignFields = $this->getCampaignFields(1);

        $dataArray = "";

        if (!empty($limit)) {

            $date_preset = $this->getCampaignParams($limit);



            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights$date_preset$date_preset$campaignFields$date_preset1" . "&limit=250&access_token=" . $this->access_token;

        } else {

            $date_preset = $this->getCampaignParams("last_7_days");



            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights$date_preset$campaignFields$date_preset1" . "&limit=250&access_token=" . $this->access_token;

        }

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights{clicks,impressions,reach,actions,date_start,campaign_name,ctr,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&time_range={'since':'".$this->sDate."','until':'".$this->eDate."'}&limit=250&access_token=" . $this->access_token;

        try {

            //  echo $url;

            // exit;

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            //$result = json_decode($result);

            // print_r($response);

            $error = $response['error']['message']; //($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;

            // $this->getPrintResult($response);

            if (isset($response['data'])) {

                $campaignDataArray = $this->getFinalResult($response['data']);

                if ($campaignDataArray) {

                    $resultArray = array(

                        "count" => 1,

                        "response" => $campaignDataArray,

                        "error" => "",

                        "success" => ""

                        );

                } else {

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $error,

                        "success" => ""

                        );

                }

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                    );

            }

        } catch (Exception $ex) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        //$this->getPrintResult($resultArray);

        return $resultArray;

    }



    function getCurlCampaignsDateRange($limit) {



        $cost_per_unique_click = 0;

        $total_cost_per_inline_click = 0;

        $impressions = 0;

        $total_inline_clicks = 0;

        $reach = 0;

        $unique_clicks = 0;

        $unique_ctr = 0;

        $spent = 0;

        $actions = 0;

        $cost_per_total_action = 0;

        $cost_per_conversion = 0;

        $inline_clicks = 0;

        $cost_per_inline_clicks = 0;

        $total = 0;

        $conversion = 0;



        $returnRow = array();

        $adAccountId = "";



        $resultArray = array();

        $adAccountId = "act_" . $this->getAddAccountId();

        $campaignFields = $this->getCampaignFields(1);

        $dataArray = explode("#", $limit);

        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?time_range={'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'}&limit=250&access_token=" . $this->access_token;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights.time_range({'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'})$campaignFields" . "&limit=250&access_token=" . $this->access_token;

        echo $url;exit;

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            $error = $response['error']['message'];

            // $this->getPrintResult($response);

            if (isset($response['data'])) {

                $campaignDataArray = $this->getFinalResult($response['data']);

                if ($campaignDataArray) {

                    $resultArray = array(

                        "count" => 1,

                        "response" => $campaignDataArray,

                        "error" => "",

                        "success" => ""

                        );

                } else {

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $error,

                        "success" => ""

                        );

                }

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                    );

            }

        } catch (Exception $ex) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        //$this->getPrintResult($resultArray);

        return $resultArray;

    }

    function Getadrulelibdata(){
        $resultArray = array();

        $adAccountId = "act_" . $this->getAddAccountId();

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/adrules_library?fields=id,name,status,schedule_spec,updated_time,execution_spec,evaluation_spec&limit=50&access_token=" . $this->access_token;
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/23842636130610160?fields=id,name,status,schedule_spec,updated_time,execution_spec,evaluation_spec&access_token=" . $this->access_token;
        
        #echo "<br>".$url;exit;

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            //$this->getPrintResult($response);
            //exit;
            $error = $response['error']['message'];

            if (isset($response['data'])) {

               $resultArray = $response['data'];

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                    );

            }

        } catch (Exception $ex) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        //$this->getPrintResult($resultArray);

        return $resultArray;
    }

    function getCurlCampaignsDateRange_new($limit) {

        $resultArray = array();

        $adAccountId = "act_" . $this->getAddAccountId();

        $campaignFields = $this->getCampaignFields(1);

        $dataArray = explode("#", $limit);

        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=$campaignFields" . "&limit=250&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&access_token=" . $this->access_token;

        //OLD $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights.time_range({'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'})$campaignFields" . "&limit=250&access_token=" . $this->access_token;

        #echo $url;

        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights{clicks,impressions,reach,actions,date_start,campaign_name,ctr,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250&access_token=" . $this->access_token;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights.date_preset(".$limit."){clicks,impressions,reach,actions,date_start,campaign_name,ctr,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&limit=250&access_token=" . $this->access_token;

        #echo "<br>".$url;exit;

        try {



            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

            $error = $response['error']['message'];

            // $this->getPrintResult($response);

            /*if (isset($response['data'])) {

                $finalArr = array();

                foreach ($response['data'] as $campaignData){

                    $campaignId = $campaignData['id'];

                    $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?fields=clicks,impressions,reach,actions,date_start,campaign_name,ctr,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250&access_token=" . $this->access_token;

                    $ch = curl_init($url);

                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                    $result = curl_exec($ch);

                    curl_close($ch);

                    $response1 = json_decode($result, true);

                    $campaignData['insights']['data'] = $response1['data'];

                    $campaignData['insights']['paging'] = $response1['paging'];

                    array_push($finalArr, $campaignData);

                }

                

                $campaignDataArray = $this->getFinalResult($finalArr);

                #echo "<PRE>";print_R($campaignDataArray);exit;

                if ($campaignDataArray) {

                    $resultArray = array(

                        "count" => 1,

                        "response" => $campaignDataArray,

                        "error" => "",

                        "success" => ""

                    );

                } else {

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $error,

                        "success" => ""

                    );

                }

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                );

            }*/

            

            if (isset($response['data'])) {

                $campaignDataArray = $this->getFinalResult($response['data']);

                if ($campaignDataArray) {

                    $resultArray = array(

                        "count" => 1,

                        "response" => $campaignDataArray,

                        "error" => "",

                        "success" => ""

                        );

                } else {

                    $resultArray = array(

                        "count" => 0,

                        "response" => "",

                        "error" => $error,

                        "success" => ""

                        );

                }

            } else {

                $resultArray = array(

                    "count" => 0,

                    "response" => "",

                    "error" => $error,

                    "success" => ""

                    );

            }

        } catch (Exception $ex) {

            $resultArray = array(

                "count" => 0,

                "response" => "",

                "error" => $e->getMessage(),

                "success" => ""

                );

        }

        //$this->getPrintResult($resultArray);

        return $resultArray;

    }
	function sksort(&$array, $subkey = "id", $subkey2 = null, $sort_ascending = true) {

        if (count($array))
    
            $temp_array[key($array)] = array_shift($array);
    
        foreach ($array as $key => $val) {
    
            $offset = 0;
    
            $found = false;
    
            foreach ($temp_array as $tmp_key => $tmp_val) {
    
                if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
    
                    $temp_array = array_merge(
    
                        (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));
    
                    $found = true;
    
                } elseif (!$found
    
                    and $subkey2 and strtolower($val[$subkey]) == strtolower($tmp_val[$subkey])
    
                    and strtolower($val[$subkey2]) > strtolower($tmp_val[$subkey2])) {
    
                    $temp_array = array_merge(
    
                        (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));
    
                    $found = true;
    
                }
    
                $offset++;
    
            }
    
            if (!$found)
    
                $temp_array = array_merge($temp_array, array($key => $val));
    
        }
    
        if ($sort_ascending)
    
            $array = array_reverse($temp_array);
    
        else
    
            $array = $temp_array;
    
    
    
        return $array;
    
    }
    public function AddRulesApply()
    {  
		echo $this->access_token;
		exit;
    }
   
}

//End of class