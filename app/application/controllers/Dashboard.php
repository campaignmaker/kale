<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use Facebook\AccessToken;
use FacebookAds\Object\AdUser;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Values\AdBuyingTypes;
use FacebookAds\Object\TargetingSearch;
use FacebookAds\Object\Search\TargetingSearchTypes;

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Dashboard extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
		$this->load->model('Users_model');
		$cookie_name = 'wordpress_logged_in_'. md5(trim($this->config->item('site_url'),'/'));
		if(isset($this->session->userdata['logged_in']['email']) && $this->session->userdata['logged_in']['email']!=""){
				$user1 = $this->Users_model->get_user_id_from_email($this->session->userdata['logged_in']['email']);
				$user = $this->Users_model->get_user($user1);
		}else
		if( isset($_COOKIE[$cookie_name]) && !empty($_COOKIE[$cookie_name])){
				$user_data =  explode('|',$_COOKIE[$cookie_name]);
		    	$user_email = $this->Users_model->get_user_email_from_username($user_data[0]);
				$user_id = $this->Users_model->get_user_id_from_email($user_email);
				$user = $this->Users_model->get_user($user_id);
				
			}
	
		$session_array = array(
            'id' => (int) $user->id,
            'first_name' => (string) $user->first_name,
            'last_name' => (string) $user->last_name,
            'full_name' => (string) $user->first_name.' '.$user->last_name,
            'email' => (string) $user->email,
            'picture' => (string) $user->picture,
            'accesstoken' => (string) $user->accesstoken,
			'status' => (int) $user->disable_user,
            'logged_in' => (bool) true,
        );
		$this->session->set_userdata('logged_in', $session_array);
		$usersubs = $this->Users_model->checktrilaexpired($user->id);
        $session_arraysub = array(
            'trialexpited' => (string) $usersubs['exp'],
			'packgid' => $usersubs['pkgid'],
			'stripestatus' => $usersubs['stripestatus1'],
			'bill_start_date1' => $usersubs['bill_start_date'],
			'bill_end_date1' => $usersubs['bill_end_date']
        );
		$this->session->set_userdata('user_subs', $session_arraysub);
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}
        
        $this->seo->SetValues('Title', "Facebook campaign Dashboard");
        $this->seo->SetValues('Description', "Facebook campaign Dashboard");
    }

    public function index() {
        $session_data = $this->session->userdata('logged_in');
        if ($session_data) {

            $data = new stdClass();
            $error = NULL;


            add_js(array('canvas-to-blob.min.js', 'fileinput.min.js'));
            add_css('fileinput.min.css');

            $fb = new Facebook([
                'app_id' => $this->config->item('facebook_app_id'),
                'app_secret' => $this->config->item('facebook_app_secret'),
                'default_graph_version' => $this->config->item('facebook_graph_version'),
            ]);

            $helper = $fb->getRedirectLoginHelper();

            $permissions = $this->config->item('facebook_permissions');
            $data->loginurl = $helper->getLoginUrl(site_url($this->config->item('facebook_login_redirect_url')), $permissions);

            if ($this->input->get('code')) {

                try {
                    $accessToken = $helper->getAccessToken();
                } catch (FacebookResponseException $e) {
                    // When Graph returns an error
                    $error = $e->getMessage();
                } catch (FacebookSDKException $e) {
                    // When validation fails or other local issues
                    $error = $e->getMessage();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
                if (isset($accessToken)) {

                    try {
                        // OAuth 2.0 client handler
                        $oAuth2Client = $fb->getOAuth2Client();
                        // Exchanges a short-lived access token for a long-lived one
                        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                    } catch (Facebook\Exceptions\FacebookResponseException $e) {
                        $error = $e->getMessage();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                    }

                    // $longLivedAccessToken = '';

                    if ($longLivedAccessToken) {
                        $fb->setDefaultAccessToken($longLivedAccessToken);


                        try {

                            $res = $fb->get('/me?fields=adaccounts.limit(100){id,account_id,account_status,name}');
                            $userData = $res->getDecodedBody();

                            if (isset($userData['adaccounts'])) {
                                foreach ($userData['adaccounts']['data'] as $my_adaccount) {
                                    if ($my_adaccount['account_status'] == 1) {
                                        $myadaccounts[] = array('id' => $my_adaccount['id'], 'account_id' => $my_adaccount['account_id'], 'name' => $my_adaccount['name'], 'account_status' => $my_adaccount['account_status']);
                                    }
                                }
                            }
							$this->ad_adsaccount2($myadaccounts,$longLivedAccessToken);
                            if (isset($myadaccounts) && $myadaccounts != NULL) {
                                //$this->session->set_userdata('long_Lived_Access_Token', $longLivedAccessToken);
                                $data->adaccounts = $myadaccounts;
                                $data->fbaccesstoken = $longLivedAccessToken;
                            } else {
                                $error = 'No ad account is associated with your Facebook account. Please create an ad account first through Facebook';
                            }
                        } catch (Exception $ex) {
                            $error = $ex->getMessage();
                        }
                    }
                }


                $data->error = $error;
                loadView('addfacebook', $data);
            } else if ($this->input->get('acode')) {

                    try {
                        // OAuth 2.0 client handler
                        $oAuth2Client = $fb->getOAuth2Client();
                        // Exchanges a short-lived access token for a long-lived one
                        $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($this->input->get('acode'));
                    } catch (Facebook\Exceptions\FacebookResponseException $e) {
                        $error = $e->getMessage();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                    }
                    // $longLivedAccessToken = '';

                    if ($longLivedAccessToken) {
                        $fb->setDefaultAccessToken($longLivedAccessToken);


                        try {

                            $res = $fb->get('/me?fields=adaccounts.limit(100){id,account_id,account_status,name}');
                            $userData = $res->getDecodedBody();

                            if (isset($userData['adaccounts'])) {
                                foreach ($userData['adaccounts']['data'] as $my_adaccount) {
                                    if ($my_adaccount['account_status'] == 1) {
                                        $myadaccounts[] = array('id' => $my_adaccount['id'], 'account_id' => $my_adaccount['account_id'], 'name' => $my_adaccount['name'], 'account_status' => $my_adaccount['account_status']);
                                    }
                                }
                            }
							$this->ad_adsaccount2($myadaccounts,$longLivedAccessToken);
							
                            if (isset($myadaccounts) && $myadaccounts != NULL) {
                                //$this->session->set_userdata('long_Lived_Access_Token', $longLivedAccessToken);
                                $data->adaccounts = $myadaccounts;
                                $data->fbaccesstoken = $longLivedAccessToken;
                            } else {
                                $error = 'No ad account is associated with your Facebook account. Please create an ad account first through Facebook';
                            }
                        } catch (Exception $ex) {
                            $error = $ex->getMessage();
                        }
                    }
                


                $data->error = $error;
                //loadView('addfacebook', $data);
            } else if ($this->session->userdata['logged_in']['accesstoken'] != NULL) {
                
                  $urll= $this->config->item('site_url');
                   redirect($urll.'app/hey/');
            } else {
                header("Location:".$data->loginurl);
            }
        } else {
			$this->session->set_userdata('last_page', current_url());
            redirect('/');
        }
    }
	
	function ad_adsaccount2($adaccount2,$fbtoken2) {
		//print_r($adaccount2);
		//exit;
        $session_data = $this->session->userdata('logged_in');

        if ($session_data) {
            $data = new stdClass();
            $error = NULL;

            $fb = new Facebook([
                'app_id' => $this->config->item('facebook_app_id'),
                'app_secret' => $this->config->item('facebook_app_secret'),
                'default_graph_version' => $this->config->item('facebook_graph_version'),
            ]);

            $helper = $fb->getRedirectLoginHelper();

            $permissions = $this->config->item('facebook_permissions');
            $data->loginurl = $helper->getLoginUrl(site_url($this->config->item('facebook_login_redirect_url')), $permissions);


            //$this->form_validation->set_rules('account_id_name[]', 'Ad Account', 'trim|required');
            //  $this->form_validation->set_rules('account_name[]', 'Ad Account Name', 'trim|required');

            //if (isset($adaccount) && $adaccount != NULL) {

                //$accountIdNames = $this->input->post('account_id_name');

//                print_r($accountIdNames);
                $response = '';

                if (isset($adaccount2) && $adaccount2 != NULL) {
                    $longLivedAccessToken = $fbtoken2;//$this->input->post('fbtoken'); // $this->session->userdata('long_Lived_Access_Token');//
                    $fb->setDefaultAccessToken($longLivedAccessToken);
                    try {
                        $res = $fb->get('/me?fields=id,email,first_name,last_name');
                        $userData = $res->getDecodedBody();
                        $user = array('fbemail' => @$userData['email'], 'last_name' => @$userData['last_name'], 'fbid' => @$userData['id'], 'accesstoken' => @$longLivedAccessToken);

                        $file = FCPATH . "assets/images/user_avatar/" . $userData['id'] . '.jpg';

                        file_put_contents($file, file_get_contents('https://graph.facebook.com/' . $userData['id'] . '/picture?type=small'));
                        $userPicture = $userData['id'] . '.jpg';
                        $user['picture'] = $userPicture;
			
                        if ($this->Users_model->edit_profile($user, $session_data['id'])) {
                            
                            $this->Users_model->deleteAdAccount($session_data['id']);

                            foreach ($adaccount2 as $adaccount2s) {

                                //$respons = explode('@IN@', $accountIdName);

                                $response = $this->Users_model->addAdAccountId(array('user_id' => $session_data['id'], 'add_title' => $adaccount2s['name'], 'ad_account_id' => $adaccount2s['account_id'], 'status' => 1));
                            }
                        } else {
                            $error = 'Your Facebook account is already linked with another account, please contact support for more information.';
                        }
                    } catch (Facebook\Exceptions\FacebookSDKException $e) {
                        $error = $e->getMessage();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                    }
                }
				
                if ($response) {
					
                    $session_data = $this->session->userdata('logged_in');
					//$this->session->unset_userdata('accesstoken');
					
                    $session_data['accesstoken'] = (string)$longLivedAccessToken;
                    $session_data['picture'] = $userPicture;
					//echo "junaidnouman".$session_data['accesstoken'];
					//print_r($session_data);
					
                    $this->session->set_userdata('logged_in', $session_data);
					//print_r($this->session->userdata('logged_in'));
					//exit;
                    redirect(site_url('dashboard'));
                } else if ($error) {
                    $data->error = $error;
                   // loadView('addfacebook', $data);
                   redirect(site_url('dashboard'));
                } else {
                    $data->error = "Something is not right. Please contact support";
                    // loadView('addfacebook', $data);
                    redirect(site_url('dashboard'));
                }
            //}
        }
    }

    function ad_account() {

        $session_data = $this->session->userdata('logged_in');

        if ($session_data) {
            $data = new stdClass();
            $error = NULL;

            $fb = new Facebook([
                'app_id' => $this->config->item('facebook_app_id'),
                'app_secret' => $this->config->item('facebook_app_secret'),
                'default_graph_version' => $this->config->item('facebook_graph_version'),
            ]);

            $helper = $fb->getRedirectLoginHelper();

            $permissions = $this->config->item('facebook_permissions');
            $data->loginurl = $helper->getLoginUrl(site_url($this->config->item('facebook_login_redirect_url')), $permissions);


            $this->form_validation->set_rules('account_id_name[]', 'Ad Account', 'trim|required');
            //  $this->form_validation->set_rules('account_name[]', 'Ad Account Name', 'trim|required');

            if ($this->form_validation->run() == false) {
                loadView('addfacebook', $data);
            } else {

                $accountIdNames = $this->input->post('account_id_name');

//                print_r($accountIdNames);
                $response = '';

                if (isset($accountIdNames) && $accountIdNames != NULL) {
                    $longLivedAccessToken = $this->input->post('fbtoken'); // $this->session->userdata('long_Lived_Access_Token');//
                    $fb->setDefaultAccessToken($longLivedAccessToken);
                    try {
                        $res = $fb->get('/me?fields=id,email,first_name,last_name');
                        $userData = $res->getDecodedBody();
                        $user = array('fbemail' => @$userData['email'], 'last_name' => @$userData['last_name'], 'fbid' => @$userData['id'], 'accesstoken' => @$longLivedAccessToken);

                        $file = FCPATH . "assets/images/user_avatar/" . $userData['id'] . '.jpg';

                        file_put_contents($file, file_get_contents('https://graph.facebook.com/' . $userData['id'] . '/picture?type=small'));
                        $userPicture = $userData['id'] . '.jpg';
                        $user['picture'] = $userPicture;
			
                        if ($this->Users_model->edit_profile($user, $session_data['id'])) {
                            
                            $this->Users_model->deleteAdAccount($session_data['id']);

                            foreach ($accountIdNames as $accountIdName) {

                                $respons = explode('@IN@', $accountIdName);

                                $response = $this->Users_model->addAdAccountId(array('user_id' => $session_data['id'], 'add_title' => $respons[1], 'ad_account_id' => $respons[0], 'status' => 1));
                            }
                        } else {
                            $error = 'Your Facebook account is already linked with another account';
                        }
                    } catch (Facebook\Exceptions\FacebookSDKException $e) {
                        $error = $e->getMessage();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                    }
                }

                if ($response) {
                    $session_data = $this->session->userdata('logged_in');
                    $session_data['accesstoken'] = $longLivedAccessToken;
                    $session_data['picture'] = $userPicture;
                    $this->session->set_userdata('logged_in', $session_data);
                    redirect(site_url('dashboard'));
                } else if ($error) {
                    $data->error = $error;
                    //loadView('addfacebook', $data);
                    redirect(site_url('dashboard'));
                } else {
                    $data->error = "Something is not right. Please contact support for more information";
                    //loadView('addfacebook', $data);
                    redirect(site_url('dashboard'));
                }
            }
        }
    }

    function arrayCombine($arr1, $arr2) {
        $count = min(count($arr1), count($arr2));
        return array_combine(array_slice($arr1, 0, $count), array_slice($arr2, 0, $count));
    }
	function resetFacebook() {
        $session_data = $this->session->userdata('logged_in');
		$this->Users_model->reset_facebook($session_data['id']);
		header("Location:".$this->config->item('site_url').'connect-to-facebook/');
		die;
    }
}
