<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Campaigns extends CI_Controller
{

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->seo->SetValues('Title', "Campaign");
        $this->seo->SetValues('Description', "Campaign");
        add_css("chart/examples.css");
        add_js("chart/jquery.flot.js");
        add_js("chart/jquery.flot.pie.js");
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}
        if (isset($this->session->userdata['logged_in']) && !empty($this->session->userdata['logged_in']))
        {
            $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
            $this->user_id = $this->session->userdata['logged_in']['id'];
            $app_id = $this->config->item('facebook_app_id');
            $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        }
        else
        {
            redirect('/');
        }
    }

    public function index()
    {
        $data = new stdClass();
        
        loadView('test', $data);
    }

    function getPrintResult($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }

}
