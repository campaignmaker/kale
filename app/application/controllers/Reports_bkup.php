<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Reports extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public $access_token;
    public $user_id;
    public $add_account_id;
    public $add_title;
    public $is_ajax;
    public $limit;

    public function __construct() {
        error_reporting(0);
        parent::__construct();
        if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
              header("Location:".$this->config->item('site_url'));die;
		}
        $this->load->model('Users_model');
        $this->seo->SetValues('Title', "Campaigns");
        $this->seo->SetValues('Description', "Reports");
        $this->is_ajax = 0;
        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
        $this->user_id = $this->session->userdata['logged_in']['id'];
        $this->getAdAccount();
		
		$response = $this->Users_model->get_cancel_subscription($this->session->userdata['logged_in']['email']);
		if($response == 'No Subscribe'){
		if($this->session->userdata['logged_in']['accesstoken'] == ''){
				header("Location:".$this->config->item('site_url').'connect-to-facebook/');
		}else{	redirect($this->config->item('site_url').'make-payment/'); }
		}elseif($response == 'Cancelled' || $response == 'Expired'){
			//redirect($this->config->item('site_url').'final-payment/');
		}
		
        $usersubs = $this->Users_model->checktrilaexpired($this->session->userdata['logged_in']['id']);
        $session_arraysub = array(
            'trialexpited' => (string) $usersubs['exp'],
			'packgid' => $usersubs['pkgid'],
			'stripestatus' => $usersubs['stripestatus1'],
			'bill_start_date1' => $usersubs['bill_start_date'],
			'bill_end_date1' => $usersubs['bill_end_date']
        );
		$this->session->set_userdata('user_subs', $session_arraysub);
		
        if (isset($this->session->userdata['logged_in']) && !empty($this->session->userdata['logged_in'])) {
            $this->app_id = $app_id = $this->config->item('facebook_app_id');
            $this->app_secret = $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);
			
        } else {
			$this->session->set_userdata('last_page', current_url());
            redirect('/');
        }

        if (!empty($this->session->userdata('dateval'))){
            
            $this->dateval = $this->session->userdata['dateval'];
        }
        else{
           
            $this->session->set_userdata('dateval', 'today');
            $this->dateval = $this->session->userdata['dateval'];
        }
        
        #echo $this->sDate."<br>".$this->eDate;exit;
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
    }

    public function index($adAccountId = NULL) {
        $limit = $this->dateval;//$this->sDate."#".$this->eDate;
        // var_dump($limit);
        // exit;
        //$limit = 'today';
        $data = new stdClass();
        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $adAccountId = $this->input->post('adAccountId');
            $limit = $this->input->post('limit');
            $this->session->set_userdata('dateval', $limit);
            //$dataArray = explode("#", $limit);
            //$this->session->set_userdata('sDate', $dataArray[0]);
            //$this->session->set_userdata('eDate', $dataArray[1]);
        }

        if (!empty($adAccountId)) {
            $this->setAddAccountId($adAccountId);
            $this->getCurrency();
            if (strpos($limit, '#') !== false) {
                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
            } else {
                //$data->adAccountData = $this->getCurlCampaigns($limit);
                $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
            }
        } else {
            $data->adAccountData = $this->getAdAccount();
        }
        #echo "<PRE>";print_R($data);exit;
        $data->addAccountId = $this->add_account_id;
        $data->adaccounts = $data->adAccountData['response'];
        $data->adaccountsCount = $data->adAccountData['count'];
        $data->error = $data->adAccountData['error'];
        $data->success = $data->adAccountData['success'];
        $data->limit = $limit;

        // batch start


        $adAccountId = "act_" . $this->add_account_id;
        //$limit = $limit1;
        $date_preset = "";
        $dataArray = "";
       /* if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url10 = $adAccountId."/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=6";
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url10 = $adAccountId."/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6";
            }
        } else {
            $date_preset = "date_preset=last_7d&";
            $url10 = $adAccountId."/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6";
        }



         if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];

                $url11 = $adAccountId."/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&breakdowns=['gender']&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250";
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url11 = $adAccountId."/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&limit=250";
            }
        } else {
            // $params = ".date_preset($type)";

            $date_preset = "date_preset=last_7d&";
            $url11 = $adAccountId."/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&limit=250";
        }


        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url12 = $adAccountId."/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250";
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url12 = $adAccountId."/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&limit=250";
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url12 = $adAccountId."/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&limit=250";
        }


        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url13 = $adAccountId."/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions','total_actions']&breakdowns=['publisher_platform','device_platform','platform_position']&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250";
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url13 = $adAccountId."/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&limit=250";
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url13 = $adAccountId."/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&limit=250";
        }

        $urlmain = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/?batch=['.urlencode('{ "method":"GET", "relative_url":"'.$url10.'"},{ "method":"GET", "relative_url":"'.$url11.'"},{ "method":"GET", "relative_url":"'.$url12.'"},{ "method":"GET", "relative_url":"'.$url13.'"}').']&access_token=' . $this->access_token;
    //        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_104413260013985?fields=insights.date_preset(lifetime){frequency,date_start}&access_token=" . $this->access_token;
                
                #echo $url;exit;
                $chmain = curl_init($urlmain);
                curl_setopt($chmain, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($chmain, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($chmain, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($chmain, CURLOPT_POST, 1);
                $resultmain = curl_exec($chmain);
                //print_r($result);
                curl_close($chmain);
                $responsemain = json_decode($resultmain, true);

        // batch end







        $countryarray = $this->getCampaignCountryChart_without_ajax($this->add_account_id,$limit,"campaignsReport",json_decode($responsemain[0]['body'], true));
        $data->adaccounts["country_total"] = $countryarray[0];
        $data->adaccounts["country_detail"] = $countryarray[1];

        $genderarray = $this->getCampaignGendrs_withoutajax($this->add_account_id,$limit,"campaignsReport",json_decode($responsemain[1]['body'], true));
        $data->adaccounts["gender_total"] = $genderarray[0];
        $data->adaccounts["gender_detail"] = $genderarray[1];
        

        $agegrouparray = $this->getCampaignChart_without_ajax($this->add_account_id,$limit,"campaignsReport",json_decode($responsemain[2]['body'], true));
        $data->adaccounts["agegroup_total"] = $agegrouparray[0];
        $data->adaccounts["agegroup_detail"] = $agegrouparray[1];

        $feedsarray = $this->getCampaignFeeds_without_ajax($this->add_account_id,$limit,"campaignsReport",json_decode($responsemain[3]['body'], true));
        $data->adaccounts["feeds_total"] = $feedsarray[0];
        $data->adaccounts["feeds_detail"] = $feedsarray[1];
        

        */

        /*echo "<pre>";
        print_r($data->adaccounts);
        echo "</pre>";
        exit;*/
		#echo "<PRE>";print_R($data);exit;
        // Save into the cache for 5 minutes => 300
        
        /*
        
        if (!empty($this->session->userdata('dateval'))){
			$this->session->unset_userdata('dateval');
        }
        
       */
        if ($this->is_ajax == 1) {
            echo $this->load->view('campign_ajax_view', $data, true);
        } else {
            loadView('completeaccountoverview', $data);
        }
    }

    public function getCurrency() {
        $adAccountId = "act_" . $this->getAddAccountId();
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/?fields=name,currency&access_token=" . $this->access_token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //print_r($response);exit;
        $currency = 'USD';
        if (!empty($response['currency'])) {
            $currency = $response['currency'];
        }
        $locale = 'en-US'; //browser or user locale
        $fmt = new NumberFormatter($locale . "@currency=$currency", NumberFormatter::CURRENCY);
        $symbol = $fmt->getSymbol(NumberFormatter::CURRENCY_SYMBOL);
        $this->session->set_userdata('cur_currency', $symbol);
        $this->session->set_userdata('cur_code', $currency);
    }

    /* From DB */

    protected function getAdAccount() {
        $result = $this->Users_model->getUserAddAccountId($this->user_id);

        if (count($result) > 0) {
            if (count($result) == 1) {
                $finalRes = "";
                $this->setAddAccountId($result[0]->ad_account_id);
                $this->getCurrency();
                $this->add_account_id = $result[0]->ad_account_id;
                $this->add_title = $result[0]->add_title;
                // $limit=date('Y-m-d', strtotime('-7 days'))."#".date('Y-m-d');
                //$finalRes = $this->getCurlCampaignsDateRange($limit);
                //$finalRes = $this->getCurlCampaigns("");
                $finalRes = $this->getCurlCampaignsDateRange_new($this->dateval);
                 
                // $this->getPrintResult($finalRes);
                $resultArray = array(
                    "count" => $finalRes['count'],
                    "response" => $finalRes['response'],
                    "error" => $finalRes['error'],
                    "success" => $finalRes['succes']
                );
                /* End Get Ad  */
            } else {
                $resultArray = array(
                    "count" => 2,
                    "response" => $result,
                    "error" => "",
                    "success" => ""
                );
            }
        } else {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => "No data available against your account.",
                "success" => ""
            );
        }
        //  $this->getPrintResult($resultArray);
        return $resultArray;
    }

    /* From LIVE */

    protected function fbAdAccount($add_account_id, $selected_limit) {
        try {
            $resultArray = $this->getAllCampaigns($add_account_id, $selected_limit);
        } catch (Exception $e) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        return $resultArray;
    }

    protected function getAllCampaigns($add_account_id, $selected_limit) {
        try {
            $adAccountOjb = new AdAccount($add_account_id);
            $insightsArray = array();
            $campaignDataArray = array();

            $fields = $this->getCampaignFields();
            $insights = $this->getCampaignInsightFields();
            $params = $this->getCampaignParams($selected_limit);

            $response = $adAccountOjb->getCampaigns($fields);

            foreach ($response as $row) {
                try {

                    $campaignObj = new Campaign($row->id);
                    $res = $this->get_hub_insight_per($campaignObj, $insights, $params);
                    foreach ($res as $rs) {
                        //$rs->account_id;
                        // $this->getPrintResult($res);
                        $insightsArray[] = array(
                            "account_name" => $rs->account_name,
                            "actions_per_impression" => $rs->actions_per_impression,
                            "call_to_action_clicks" => $rs->call_to_action_clicks,
                            "campaign_id" => $rs->campaign_id,
                            "campaign_name" => $rs->campaign_name,
                            "cost_per_action_type" => $rs->cost_per_action_type,
                            "cost_per_total_action" => $rs->cost_per_total_action,
                            "cost_per_unique_click" => $rs->cost_per_unique_click,
                            "cost_per_inline_link_click" => $rs->cost_per_inline_link_click,
                            "cost_per_inline_post_engagement" => $rs->cost_per_inline_post_engagement,
                            "cpm" => $rs->cpm,
                            "cpp" => $rs->cpp,
                            "ctr" => $rs->ctr,
                            "date_start" => $rs->date_start,
                            "date_stop" => $rs->date_stop,
                            "frequency" => $rs->frequency,
                            "impressions" => $rs->impressions,
                            "inline_link_clicks" => $rs->inline_link_clicks,
                            "inline_post_engagement" => $rs->inline_post_engagement,
                            "reach" => $rs->reach,
                            "spend" => $rs->spend,
                            "total_action_value" => $rs->total_action_value,
                            "total_actions" => $rs->total_actions,
                            "total_unique_actions" => $rs->total_unique_actions,
                            "unique_clicks" => $rs->unique_clicks,
                            "unique_ctr" => $rs->unique_ctr
                        );
                    }
                    if ($insightsArray) {
                        $campaignDataArray[] = array(
                            "id" => $row->id,
                            "name" => $row->name,
                            "created_time" => $row->created_time,
                            "effective_status" => $row->effective_status,
                            "status_paused" => $row->status_paused,
                            "AllArray" => $row,
                            "insights" => $insightsArray
                        );
                        $insightsArray = array();
                    }
                } catch (Exception $e) {
                    return $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $e->getMessage(),
                        "success" => ""
                    );
                }
            }

            if (count($campaignDataArray) > 1) {
                $finalArray = $this->getFinalResult($campaignDataArray);
                $resultArray = array(
                    "count" => 1,
                    "response" => $finalArray,
                    "error" => "",
                    "success" => ""
                );
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }
        } catch (Exception $e) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        return $resultArray;
    }

    /* Privat functions */

    private function setAddAccountId($adAccountId) {
        $this->add_account_id = $adAccountId;
    }

    private function getAddAccountId() {
        return $this->add_account_id;
    }

    /* End Private */

    function get_hub_insight_per($obj, $insights, $param) {
        try {
            return $response = $obj->getInsights($insights);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function getCampaignFields($curl) {
        if ($curl == 1) {
            $fields = "id,account_id,name,objective,buying_type,created_time,start_time,stop_time,effective_status,currency,configured_status";
        } else {
            $fields = array(
                CampaignFields::ID,
                CampaignFields::ACCOUNT_ID,
                CampaignFields::NAME,
                CampaignFields::OBJECTIVE,
                CampaignFields::BUYING_TYPE,
                CampaignFields::PROMOTED_OBJECT,
                CampaignFields::ADLABELS,
                CampaignFields::CREATED_TIME,
                CampaignFields::START_TIME,
                CampaignFields::STOP_TIME,
                CampaignFields::UPDATED_TIME,
                CampaignFields::EFFECTIVE_STATUS,
                CampaignFields::STATUS_PAUSED,
            );
        }
        return $fields;
    }

    function getCampaignInsightFields($curl) {
        if ($curl == 1) {
            $fields = "{date_start,date_stop,buying_type,campaign_id,actions{action_type,value}}";
        } else {

            $fields = array(
                InsightsFields::ACCOUNT_ID,
                InsightsFields::IMPRESSIONS,
                InsightsFields::UNIQUE_CLICKS,
                InsightsFields::REACH,
                InsightsFields::INLINE_LINK_CLICKS,
                InsightsFields::COST_PER_INLINE_LINK_CLICK,
                InsightsFields::COST_PER_UNIQUE_CLICK,
                InsightsFields::SPEND,
                InsightsFields::COST_PER_ACTION_TYPE
            );
        }
        return $fields;
    }

    function getCampaignParams($type) {
        return $params = ".date_preset($type)";
    }

    function getFinalResult($campaignDataArray) {
        #echo "<PRE>";print_R($campaignDataArray);exit;
        $activeCampaigns = 0;
        $inActiveCampaigns = 0;
        $cost_per_unique_click = 0;
        $total_cost_per_inline_click = 0;
        $impressions = 0;
        $total_inline_clicks = 0;
        $reach = 0;
        $unique_clicks = 0;
        $unique_ctr = 0;
        $spent = 0;
        $actions = 0;
        $cost_per_total_action = 0;
		$offsite_conversion_fb_pixel_add_payment_info1 = $offsite_conversion_fb_pixel_add_to_cart1 = $offsite_conversion_fb_pixel_add_to_wishlist1 = $offsite_conversion_fb_pixel_complete_registration1 = $offsite_conversion_fb_pixel_initiate_checkout1 = $offsite_conversion_fb_pixel_lead1 = $offsite_conversion_fb_pixel_purchase1 = $offsite_conversion_fb_pixel_search1 = $offsite_conversion_fb_pixel_view_content1 = $offsite_conversion_key_page_view1 = $onsite_conversion_messaging_first_reply1 = 0;
        $inline_clicks = 0;
        $cost_per_inline_clicks = 0;
        $pendingCampaigns = 0;
        $disapprovedCampaigns = 0;
        $total = 0;
        $total1 = 0;
        $total2 = 0;
        $total3 = 0;
        $total4 = 0;
        $total5 = 0;
        $total6 = $total7 = $total8 = $total9 = $total10 = $total11 = $total12 =  $total13 = $total14 = $total15 = $total16 = 0;        
        $conversion = 0;
        $total_actions = 0;
		
		
		$app_install = $leadgenother = $video_10_sec_watched_actions = $video_avg_percent_watched_actions = $offline_conversion = 0;
		$totalcost_per_inline_link_click2 = 0;
		$totalinline_link_click_ctr2 = 0;
        #echo "<PRE>";print_R($campaignDataArray);exit;
        $returnRow = array();
        foreach ($campaignDataArray as $row) {
            if ($row['effective_status'] == "ACTIVE" || $row['effective_status'] == "PREAPPROVED") {
                $activeCampaigns++;
                $status = "Active";
            } 
            else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {
                $inActiveCampaigns++;
                $status = "Inactive";
            } 
            else if ($row['effective_status'] == "PENDING_REVIEW") {
                $pendingCampaigns++;
                $status = "In Review";
            } 
            else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {
                $disapprovedCampaigns++;
                $status = "Denied";
            }
            $total++;

            
            
            if (isset($row['insights'])) {
              /* echo "<pre>";
            print_r($row['insights']);
            echo "</pre>";
            exit;*/
                // echo $row['name']."<br>";
                $inline_post_engagement1 = $like1 = '0';
                $conversion = $cost_per_conversion = 0;
                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';
                $video_3_sec_views =$video_3_sec_views_per_click=$leadgen_other=$leadgen_other_per_click=$mobile_app_install=$mobile_app_install_per_click= $offsite_conversion_fb_pixel_add_payment_info = $offsite_conversion_fb_pixel_add_to_cart = $offsite_conversion_fb_pixel_add_to_wishlist = $offsite_conversion_fb_pixel_complete_registration = $offsite_conversion_fb_pixel_initiate_checkout = $offsite_conversion_fb_pixel_lead = $offsite_conversion_fb_pixel_purchase = $offsite_conversion_fb_pixel_search = $offsite_conversion_fb_pixel_view_content = $offsite_conversion_key_page_view = $onsite_conversion_messaging_first_reply = 0;
                if (isset($row['insights']['data'][0])) {
                    foreach ($row['insights']['data'][0]['actions'] as $rs) {
                        #echo "<PRE>";print_r($row['insights']);exit;
                        /*if ($row['insights']['data'][0]['clicks']) {
                            $inline_clicks = $row['insights']['data'][0]['clicks'];
                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];
                        }*/
						if ($row['insights']['data'][0]['inline_link_clicks']) {
                            $inline_clicks = $row['insights']['data'][0]['inline_link_clicks'];
                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['inline_link_clicks'];
                        }
						
						
						if ($row['insights']['data'][0]['cost_per_inline_link_click']) {
                            $totalcost_per_inline_link_click2 += $row['insights']['data'][0]['cost_per_inline_link_click'];
                            //$cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['inline_link_clicks'];
                        }
						
						if ($row['insights']['data'][0]['inline_link_click_ctr']) {
                            $totalinline_link_click_ctr2 += $row['insights']['data'][0]['inline_link_click_ctr'];
                            //$cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['inline_link_clicks'];
                        }
						
						
						
                        if ($rs['action_type'] == "leadgen.other") {
                            if($rs['value']){
                                $conversion = $rs['value'];
                            }
                            else{
                                $conversion = 0;
                            }
                        } 
                        if ($rs['action_type'] == "offsite_conversion") {
                            if($rs['value']){
                                $conversion += $rs['value'];
                                $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];
                            }
                            else{
                                $conversion = 0;
                            $cost_per_conversion = 0;
                            }
                        } 
                        if($rs['action_type'] == 'page_engagement'){
                            if($rs['value']){
                                $total1++;
                                $inline_post_engagement1 = $rs['value'];
                            }
                            else{
                                $inline_post_engagement1 = 0;
                            }
                        }
                        if($rs['action_type'] == 'like'){
                            if($rs['value']){
                                $total2++;
                                $like1 = $rs['value'];
                            }
                            else{
                                $like1 = 0;
                            }
                        }
                        //new dp here - samy
                        if($rs['action_type'] == 'video_view'){
                            if($rs['value']){
                                $total3++;
                                $video_3_sec_views = $rs['value'];
                            }
                            else{
                                $video_3_sec_views = 0;
                            }
                        }
						if($rs['action_type'] == 'leadgen.other'){
                            if($rs['value']){
                                $total4++;
                                $leadgen_other = $rs['value'];
                            }
                            else{
                                $leadgen_other = 0;
                            }
                        }
						if($rs['action_type'] == 'mobile_app_install'){
                            if($rs['value']){
                                $total5++;
                                $mobile_app_install = $rs['value'];
                            }
                            else{
                                $mobile_app_install = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_add_payment_info'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_add_payment_info = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_add_payment_info = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_add_to_cart'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_add_to_cart = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_add_to_cart = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_add_to_wishlist'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_add_to_wishlist = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_add_to_wishlist = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_complete_registration'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_complete_registration = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_complete_registration = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_initiate_checkout'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_initiate_checkout = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_initiate_checkout = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_lead'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_lead = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_lead = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_purchase'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_purchase = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_purchase = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_search'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_search = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_search = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.fb_pixel_view_content'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_fb_pixel_view_content = $rs['value'];
                            }
                            else{
                                $offsite_conversion_fb_pixel_view_content = 0;
                            }
                        }
						if($rs['action_type'] == 'offsite_conversion.key_page_view'){
                            if($rs['value']){
                                $total5++;
                                $offsite_conversion_key_page_view = $rs['value'];
                            }
                            else{
                                $offsite_conversion_key_page_view = 0;
                            }
                        }
						if($rs['action_type'] == 'onsite_conversion.messaging_first_reply'){
                            if($rs['value']){
                                $total5++;
                                $onsite_conversion_messaging_first_reply = $rs['value'];
                            }
                            else{
                                $onsite_conversion_messaging_first_reply = 0;
                            }
                        }
                        //change end here - samy
                    }
                    foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {
                        if($rs['action_type'] == 'page_engagement'){
                            if($rs['value']){
                                $cost_per_inline_post_engagement1 = $rs['value'];
                            }
                            else{
                                $cost_per_inline_post_engagement1 = 0;
                            }
                        }
                        if ($rs['action_type'] == "leadgen.other") {
                            if($rs['value']){
                                $cost_per_conversion = $rs['value'];
                            }
                            else{
                                $cost_per_conversion = 0;
                            }
                        } 
                        if($rs['action_type'] == 'like'){
                            if($rs['value']){
                                $cost_per_like1 = $rs['value'];
                            }
                            else{
                                $cost_per_like1 = 0;
                            }
                        }
						if($rs['action_type'] == 'app_install'){
                            if($rs['value']){
                                $app_install = $rs['value'];
                            }
                            else{
                                $app_install = 0;
                            }
                        }
						if($rs['action_type'] == 'leadgen.other'){
                            if($rs['value']){
                                $leadgenother = $rs['value'];
                            }
                            else{
                                $leadgenother = 0;
                            }
                        }
						if($rs['action_type'] == 'offline_conversion'){
                            if($rs['value']){
                                $offline_conversion = $rs['value'];
                            }
                            else{
                                $offline_conversion = 0;
                            }
                        }
						
						
                    }
					
					foreach ($row['insights']['data'][0]['video_10_sec_watched_actions'] as $rs) {
                        if($rs['action_type'] == 'video_view'){
                            if($rs['value']){
                                $video_10_sec_watched_actions = $rs['value'];
                            }
                            else{
                                $video_10_sec_watched_actions = 0;
                            }
                        }
                    }
					foreach ($row['insights']['data'][0]['video_avg_percent_watched_actions'] as $rs) {
                        if($rs['action_type'] == 'video_view'){
                            if($rs['value']){
                                $video_avg_percent_watched_actions = $rs['value'];
                            }
                            else{
                                $video_avg_percent_watched_actions = 0;
                            }
                        }
                    }
                }
                
                $total_inline_clicks+=$inline_clicks;
                $total_cost_per_inline_click+=$cost_per_inline_clicks;

                $returnRow['campaigns'][] = array(
                    "campaign_id" => $row['id'],
                    "campaign_name" => $row['name'],
                    "campaign_created_time" => $row['created_time'],
                    "campaign_effective_status" => $status, //$row['effective_status'],
                    "campaign_cost_per_unique_click" => $row['insights']['data'][0]['cost_per_unique_click'],
                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,
                    "campaign_impressions" => $row['insights']['data'][0]['impressions'],
                    "campaign_cpm" => $row['insights']['data'][0]['cpm'],
                    "campaign_inline_link_clicks" => $inline_clicks,
                    "campaign_reach" => $row['insights']['data'][0]['reach'],
                    "campaign_unique_clicks" => $row['insights']['data'][0]['unique_clicks'],
                    "campaign_unique_ctr" => $row['insights']['data'][0]['ctr'],
                    "campaign_spent" => $row['insights']['data'][0]['spend'],
                    "campaign_actions" => $conversion,
                    "campaign_cost_per_total_action" => $cost_per_conversion,
                    "campaign_objective" => $row['objective'],
                    "inline_post_engagement" => $inline_post_engagement1,
                    "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement1,
                    "page_like" => $like1,
                    "video_3_sec_views " => $video_3_sec_views,
                    "leadgen_other" => $leadgen_other,
                    "offsite_conversion_fb_pixel_add_payment_info" => $offsite_conversion_fb_pixel_add_payment_info,
					"offsite_conversion_fb_pixel_add_to_cart" => $offsite_conversion_fb_pixel_add_to_cart,
					"offsite_conversion_fb_pixel_add_to_wishlist" => $offsite_conversion_fb_pixel_add_to_wishlist,
					"offsite_conversion_fb_pixel_complete_registration" => $offsite_conversion_fb_pixel_complete_registration,
					"offsite_conversion_fb_pixel_initiate_checkout" => $offsite_conversion_fb_pixel_initiate_checkout,
					"offsite_conversion_fb_pixel_lead" => $offsite_conversion_fb_pixel_lead,
					"offsite_conversion_fb_pixel_purchase" => $offsite_conversion_fb_pixel_purchase,
					"offsite_conversion_fb_pixel_search" => $offsite_conversion_fb_pixel_search,
					"offsite_conversion_fb_pixel_view_content" => $offsite_conversion_fb_pixel_view_content,
					"offsite_conversion_key_page_view" => $offsite_conversion_key_page_view,
					"onsite_conversion_messaging_first_reply" => $onsite_conversion_messaging_first_reply,
					"mobile_app_install" => $mobile_app_install,
                    "cost_per_like1" => $cost_per_like1,
                    "leadgen" => $leadgen,
                    "cost_leadgen" => $cost_leadgen,
                    "reach" => $row['insights']['data'][0]['reach'],
                    "frequency" => $row['insights']['data'][0]['frequency'],
                    "app_install" => $app_install,
                    "video_10_sec_watched_actions" => $video_10_sec_watched_actions,
			        "video_avg_percent_watched_actions" => $video_avg_percent_watched_actions
                );

                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];
                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];
                $impressions += $row['insights']['data'][0]['impressions'];
                $cpm += $row['insights']['data'][0]['cpm'];
                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];
                $reach += $row['insights']['data'][0]['reach'];
                $frequency += $row['insights']['data'][0]['frequency'];
                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];
                $unique_ctr += $row['insights']['data'][0]['ctr'];
                $spent += $row['insights']['data'][0]['spend'];
                $total_actions += $row['insights']['data'][0]['total_actions'];
                $actions += $conversion;
                $cost_per_total_action += $cost_per_conversion;
                $inline_post_engagement += $inline_post_engagement1;
                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;
                $page_like += $like1;
                $total_video_3_sec_views += $video_3_sec_views;
                $total_leadgen_other += $leadgen_other;
                $total_mobile_app_install += $mobile_app_install;
				$offsite_conversion_fb_pixel_add_payment_info1+= $offsite_conversion_fb_pixel_add_payment_info;
				$offsite_conversion_fb_pixel_add_to_cart1+= $offsite_conversion_fb_pixel_add_to_cart;
				$offsite_conversion_fb_pixel_add_to_wishlist1+= $offsite_conversion_fb_pixel_add_to_wishlist;
				$offsite_conversion_fb_pixel_complete_registration1+= $offsite_conversion_fb_pixel_complete_registration;
				$offsite_conversion_fb_pixel_initiate_checkout1+= $offsite_conversion_fb_pixel_initiate_checkout;
				$offsite_conversion_fb_pixel_lead1+= $offsite_conversion_fb_pixel_lead;
				$offsite_conversion_fb_pixel_purchase1+= $offsite_conversion_fb_pixel_purchase;
				$offsite_conversion_fb_pixel_search1+= $offsite_conversion_fb_pixel_search;
				$offsite_conversion_fb_pixel_view_content1+= $offsite_conversion_fb_pixel_view_content;
				$offsite_conversion_key_page_view1+= $offsite_conversion_key_page_view;
				$onsite_conversion_messaging_first_reply1+= $onsite_conversion_messaging_first_reply;
                $cost_per_like += $cost_per_like1;
                $cost_leadgen1 += $cost_leadgen;
                $leadgen1 += $leadgen;

                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            } 
            else {

                $returnRow['campaigns'][] = array(
                    "campaign_id" => $row['id'],
                    "campaign_name" => $row['name'],
                    "campaign_created_time" => $row['created_time'],
                    "campaign_effective_status" => $status, //$row['effective_status'],
                    "campaign_cost_per_unique_click" => 0,
                    "campaign_cost_per_inline_link_click" => $cost_per_inline_clicks,
                    "campaign_impressions" => 0,
                    "campaign_cpm" => 0,
                    "campaign_inline_link_clicks" => $inline_clicks,
                    "campaign_reach" => 0,
                    "campaign_unique_clicks" => 0,
                    "campaign_unique_ctr" => 0,
                    "campaign_spent" => 0,
                    "campaign_actions" => 0,
                    "campaign_cost_per_total_action" => 0,
                    "campaign_reach" => 0,
                    "inline_post_engagement" => 0,
                    "cost_per_inline_post_engagement" => 0,
                    "page_like" => 0,
                    "video_3_sec_views " => 0,
                    "leadgen_other" => 0,
                    "mobile_app_install" => 0,
					"offsite_conversion_fb_pixel_add_payment_info" => 0,
					"offsite_conversion_fb_pixel_add_to_cart" => 0,
					"offsite_conversion_fb_pixel_add_to_wishlist" => 0,
					"offsite_conversion_fb_pixel_complete_registration" => 0,
					"offsite_conversion_fb_pixel_initiate_checkout" => 0,
					"offsite_conversion_fb_pixel_lead" => 0,
					"offsite_conversion_fb_pixel_purchase" => 0,
					"offsite_conversion_fb_pixel_search" => 0,
					"offsite_conversion_fb_pixel_view_content" => 0,
					"offsite_conversion_key_page_view" => 0,
					"onsite_conversion_messaging_first_reply" => 0,
                    "cost_per_like1" => 0,
                    "campaign_objective" => $row['objective'],
                    "leadgen" => 0,
                    "cost_leadgen" => 0,
                    "reach" => 0,
                    "frequency" => 0,
                    "app_install" => 0,
                    "video_10_sec_watched_actions" => 0,
			        "video_avg_percent_watched_actions" => 0
                );

                $cost_per_unique_click += 0;
                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];
                $impressions += 0;
                $cpm += 0;
                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];
                $reach += 0;
                $frequency += 0;
                $unique_clicks += 0;
                $unique_ctr += 0;
                $spent += 0;
                $actions += 0;
                $cost_per_total_action += 0;
                $inline_post_engagement += 0;
                $cost_per_inline_post_engagement += 0;
                $page_like += 0;
                $total_video_3_sec_views += 0;
                $total_leadgen_other += 0;
                $total_mobile_app_install += 0;
				$offsite_conversion_fb_pixel_add_payment_info1+= 0;
				$offsite_conversion_fb_pixel_add_to_cart1+= 0;
				$offsite_conversion_fb_pixel_add_to_wishlist1+= 0;
				$offsite_conversion_fb_pixel_complete_registration1+= 0;
				$offsite_conversion_fb_pixel_initiate_checkout1+= 0;
				$offsite_conversion_fb_pixel_lead1+= 0;
				$offsite_conversion_fb_pixel_purchase1+= 0;
				$offsite_conversion_fb_pixel_search1+= 0;
				$offsite_conversion_fb_pixel_view_content1+= 0;
				$offsite_conversion_key_page_view1+= 0;
				$onsite_conversion_messaging_first_reply1+= 0;
                $cost_per_like += 0;
                $cost_leadgen1 += 0;
                $leadgen1 += 0;
                $total_actions += 0;
                
                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            }
        }
        if ($unique_ctr > 0) {
            $unique_ctr = ($unique_ctr / $total);
        }
        if ($total_cost_per_inline_click > 0) {
            $total_cost_per_inline_click = ($total_cost_per_inline_click / $total);
        }
        
        if ($cost_per_inline_post_engagement > 0) {
            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);
        }
        if ($cost_leadgen1 > 0) {
            $cost_leadgen1 = ($cost_leadgen1 / $total3);
        }
        if ($cost_per_like > 0) {
            $cost_per_like = ($cost_per_like / $total2);
        }
        //echo $actions;
        if ($cost_per_total_action > 0) {
            $cost_per_total_action = ($spent / $actions);
        }
        $returnRow['campaigns'] = $this->sksort($returnRow['campaigns'], "campaign_effective_status");
        $returnRow['campaign_header'] = array(
            "campaign_active" => $activeCampaigns,
            "campaign_inactive" => $inActiveCampaigns,
            "campaign_pending" => $pendingCampaigns,
            "campaign_disapproved" => $disapprovedCampaigns,
            "cost_per_unique_click" => $cost_per_unique_click,
            "cost_per_inline_link_click" => $total_cost_per_inline_click,
            "impressions" => $impressions,
            "cpm" => $cpm,
            "inline_link_clicks" => $total_inline_clicks,
            "reach" => $reach,
            "frequency" => $frequency,
            "unique_clicks" => $unique_clicks,
            "unique_ctr" => $unique_ctr,
            "spent" => $spent,
            "actions" => $actions,
            "actions" => $actions,
            "cost_per_total_action" => $cost_per_total_action,
            "inline_post_engagement" => $inline_post_engagement,
            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,
            "page_like" => $page_like,
            "total_video_3_sec_views" => $total_video_3_sec_views,
			"total_leadgen_other" => $total_leadgen_other,
			"total_mobile_app_install" => $total_mobile_app_install,
			"offsite_conversion_fb_pixel_add_payment_info" => $offsite_conversion_fb_pixel_add_payment_info1,
			"offsite_conversion_fb_pixel_add_to_cart" => $offsite_conversion_fb_pixel_add_to_cart1,
			"offsite_conversion_fb_pixel_add_to_wishlist" => $offsite_conversion_fb_pixel_add_to_wishlist1,
			"offsite_conversion_fb_pixel_complete_registration" => $offsite_conversion_fb_pixel_complete_registration1,
			"offsite_conversion_fb_pixel_initiate_checkout" => $offsite_conversion_fb_pixel_initiate_checkout1,
			"offsite_conversion_fb_pixel_lead" => $offsite_conversion_fb_pixel_lead1,
			"offsite_conversion_fb_pixel_purchase" => $offsite_conversion_fb_pixel_purchase1,
			"offsite_conversion_fb_pixel_search" => $offsite_conversion_fb_pixel_search1,
			"offsite_conversion_fb_pixel_view_content" => $offsite_conversion_fb_pixel_view_content1,
			"offsite_conversion_key_page_view" => $offsite_conversion_key_page_view1,
			"onsite_conversion_messaging_first_reply" => $onsite_conversion_messaging_first_reply1,
            "cost_per_like" => $cost_per_like,
            "leadgen" => $leadgen1,
            "cost_leadgen" => $cost_leadgen1,
			"cost_per_inline_link_click2" => $totalcost_per_inline_link_click2,
			"inline_link_click_ctr" => $totalinline_link_click_ctr2,
			"app_install" => $app_install,
			"leadgenother" => $leadgenother,
			"video_10_sec_watched_actions" => $video_10_sec_watched_actions,
			"video_avg_percent_watched_actions" => $video_avg_percent_watched_actions,
			"offline_conversion" => $offline_conversion,
			"total_actions" => $total_actions
        );

        //echo "<PRE>";print_R($returnRow);exit;
        return $returnRow;
    }

    function getPrintResult($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }

    function getCurlCampaigns($limit) {

        $adAccountId = "";
        $date_preset = "";
        $date_preset1 = "";
        $resultArray = array();
        $adAccountId = "act_" . $this->getAddAccountId();
        $campaignFields = $this->getCampaignFields(1);
        $dataArray = "";
        if (!empty($limit)) {
            $date_preset = $this->getCampaignParams($limit);

            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights$date_preset$date_preset$campaignFields$date_preset1" . "&limit=250&access_token=" . $this->access_token;
        } else {
            $date_preset = $this->getCampaignParams("last_7_days");

            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights$date_preset$campaignFields$date_preset1" . "&limit=250&access_token=" . $this->access_token;
        }
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights{clicks,impressions,reach,actions,date_start,campaign_name,ctr,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&time_range={'since':'".$this->sDate."','until':'".$this->eDate."'}&limit=250&access_token=" . $this->access_token;
        try {
            //  echo $url;
            // exit;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            //$result = json_decode($result);
            // print_r($response);
            $error = $response['error']['message']; //($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
            // $this->getPrintResult($response);
            if (isset($response['data'])) {
                $campaignDataArray = $this->getFinalResult($response['data']);
                if ($campaignDataArray) {
                    $resultArray = array(
                        "count" => 1,
                        "response" => $campaignDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $error,
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => $error,
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getCurlCampaignsDateRange($limit) {

        $cost_per_unique_click = 0;
        $total_cost_per_inline_click = 0;
        $impressions = 0;
        $total_inline_clicks = 0;
        $reach = 0;
        $unique_clicks = 0;
        $unique_ctr = 0;
        $spent = 0;
        $actions = 0;
        $cost_per_total_action = 0;
        $cost_per_conversion = 0;
        $inline_clicks = 0;
        $cost_per_inline_clicks = 0;
        $total = 0;
        $conversion = 0;

        $returnRow = array();
        $adAccountId = "";

        $resultArray = array();
        $adAccountId = "act_" . $this->getAddAccountId();
        $campaignFields = $this->getCampaignFields(1);
        $dataArray = explode("#", $limit);
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?time_range={'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'}&limit=250&access_token=" . $this->access_token;
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights.time_range({'since':'" . $dataArray[0] . "','until':'" . $dataArray[1] . "'})$campaignFields" . "&limit=250&access_token=" . $this->access_token;
        echo $url;exit;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            $error = $response['error']['message'];
            // $this->getPrintResult($response);
            if (isset($response['data'])) {
                $campaignDataArray = $this->getFinalResult($response['data']);
                if ($campaignDataArray) {
                    $resultArray = array(
                        "count" => 1,
                        "response" => $campaignDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $error,
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => $error,
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    public function asArray($data) {
        foreach ($data as $k => $v)
            $data[$k] = $v->getData();
        return $data;
    }
    
    function getCurlCampaignsDateRange_new($limit) {
    //   var_dump($limit);
    //   exit;
        $resultArray = array();
        $adAccountId = "act_" . $this->getAddAccountId();
        $campaignFields = $this->getCampaignFields(1);
        $dataArray = explode("#", $limit);
        /*$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights.date_preset(".$limit."){clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective,frequency}&limit=250&access_token=" . $this->access_token;*/
		$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights.date_preset(".$limit."){inline_link_clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,call_to_action_clicks,cost_per_action_type,video_10_sec_watched_actions,video_avg_percent_watched_actions,cost_per_unique_click,unique_clicks,spend,objective,frequency,cost_per_inline_link_click,inline_link_click_ctr,total_actions}&limit=300&access_token=" . $this->access_token;
		
	   // echo $limit;
    //     echo $url;
    //     exit;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
			//echo "<pre>";
			//print_r($response);
			//echo "</pre>";
			//exit;
            $error = $response['error']['message'];
            
			/*if(!$error){
				echo $url;
				echo "<pre>";
			print_r($response);
			echo "</pre>";
			exit;
			}*/
            if (isset($response['data'])) {
                $campaignDataArray = $this->getFinalResult($response['data']);
                if ($campaignDataArray) {

					// start batch
					//$url4 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/?batch=['.urlencode('{ "method":"GET", "relative_url":"'.$adAccountId.'?fields=insights.date_preset('.$limit.'){frequency,date_start}"},{ "method":"GET", "relative_url":"'.$adAccountId.'/campaigns?fields='.$campaignFields.',insights.date_preset(today){spend}&limit=250"},{ "method":"GET", "relative_url":"'.$adAccountId.'/campaigns?fields='.$campaignFields.',insights.date_preset(last_7d){spend}&limit=250"}').']&access_token=' . $this->access_token;
					$url4 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/?batch=['.urlencode('{ "method":"GET", "relative_url":"'.$adAccountId.'?fields=insights.date_preset('.$limit.'){frequency,date_start}"}').']&access_token=' . $this->access_token;
	//		  $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_104413260013985?fields=insights.date_preset(lifetime){frequency,date_start}&access_token=" . $this->access_token;
                
                #echo $url;exit;
                $ch4 = curl_init($url4);
                curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch4, CURLOPT_POST, 1);
                $result4 = curl_exec($ch4);
				//print_r($result);
                curl_close($ch4);
                $response4 = json_decode($result4, true);
				/*echo "<pre>";
				print_r($response4);
				echo "</pre>";*/
					// end batch


                    /*$url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId?fields=insights.date_preset(".$limit.")%7Bfrequency%2Cdate_start%7D&access_token=" . $this->access_token;

                    $ch = curl_init($url1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response1 = json_decode($result, true);*/
					$response1 = json_decode($response4[0]['body'], true);
                    $campaignDataArray['campaign_header']['newFrequ'] = $response1['insights']['data'][0]['frequency'];

                    $accountArr = $this->Users_model->getUserAddAccountName($this->getAddAccountId());
                    $campaignDataArray['campaign_header']['accountName'] = $accountArr[0]->add_title;
                    
					/*$url2 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights.date_preset(today){spend}&limit=250&access_token=" . $this->access_token;

                    $ch = curl_init($url2);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result2 = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($result2, true);*/
					$response2 = array();//json_decode($response4[1]['body'], true);
					
					
					
					
					/*$url3 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=".$campaignFields.",insights.date_preset(last_7d){spend}&limit=250&access_token=" . $this->access_token;

                    $ch = curl_init($url3);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result3 = curl_exec($ch);
                    curl_close($ch);
                    $response3 = json_decode($result3, true);*/
					$response3 = array();//json_decode($response4[2]['body'], true);
					$todayspend = $sevendayspend = 0;
					
                    foreach ($response2['data'] as $row1) {
                        $todayspend += $row1['insights']['data'][0]['spend'];
                    }
					/*if($response2['data'][0]['insights']['data'][0]['spend']){
						$todayspend = $response2['data'][0]['insights']['data'][0]['spend'];
					}
					if($response3['data'][0]['insights']['data'][0]['spend']){
						$sevendayspend = $response3['data'][0]['insights']['data'][0]['spend'];
					}*/
                    foreach ($response3['data'] as $row2) {
                        $sevendayspend += $row2['insights']['data'][0]['spend'];
                    }
					
					$campaignDataArray['campaign_header']['todayspend'] = $todayspend;
					$campaignDataArray['campaign_header']['sevendayspend'] = $sevendayspend;
					
					
                    $resultArray = array(
                        "count" => 1,
                        "response" => $campaignDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $error,
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => $error,
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        #echo "<PRE>";print_R($resultArray);exit;
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getCampaignGendrs() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            // $params = ".date_preset($type)";

            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&limit=250&access_token=" . $this->access_token;
        }

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($genders['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($genders['gender'] == "female") {
                                $female = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "male") {
                                $male = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "unknown") {
                                $other = $feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($genders['gender'] == "female") {
                        $female = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "male") {
                        $male = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "unknown") {
                        $other = $genders[$pageName];
                    }
                }
            }
        }


        $response = array(
            "code" => 100,
            "male" => $male,
            "female" => $female,
            "other" => $other,
        );
        echo json_encode($response);
    }
	
	function getCampaignGendrsvarious() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            // $params = ".date_preset($type)";

            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&limit=250&access_token=" . $this->access_token;
        }

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'offsite_conversion.fb_pixel_add_payment_info' || $pageName == 'offsite_conversion.fb_pixel_add_to_cart' || $pageName == 'offsite_conversion.fb_pixel_add_to_wishlist' || $pageName == 'offsite_conversion.fb_pixel_complete_registration' || $pageName == 'offsite_conversion.fb_pixel_custom' || $pageName == 'offsite_conversion.fb_pixel_initiate_checkout' || $pageName == 'offsite_conversion.fb_pixel_lead' || $pageName == 'offsite_conversion.fb_pixel_purchase' || $pageName == 'offsite_conversion.fb_pixel_search' || $pageName == 'offsite_conversion.fb_pixel_view_content' || $pageName == 'offsite_conversion.key_page_view' || $pageName == 'offsite_conversion.lead' || $pageName == 'offsite_conversion.other' || $pageName == 'offsite_conversion.registration'){
                    foreach ($genders['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($genders['gender'] == "female") {
                                $female = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "male") {
                                $male = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "unknown") {
                                $other = $feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($genders['gender'] == "female") {
                        $female = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "male") {
                        $male = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "unknown") {
                        $other = $genders[$pageName];
                    }
                }
            }
        }
		$tempstr1 = array();
        if($pageName=="ctr"){
            $tempstr1[] = $this->input->post('clickrate');//number_format((number_format($male+$female+$other)/3))."%";
        }
        else{
            $tempstr1[] = ($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($male+$female+$other);
        }
        $tempstr1[] = '<tr><td class="highlight">MEN</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($male).($pageName=="ctr" ? "%" : "").'</td>/tr><tr><td class="highlight">WOMEN</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($female).($pageName=="ctr" ? "%" : "").'</td>/tr><tr><td class="highlight">OTHER</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($other).($pageName=="ctr" ? "%" : "").'</td>/tr>';

        echo json_encode($tempstr1);
		exit;
    }

    function getCampaignGendrs_withoutajax($adAccountId,$limit1,$pagename,$finalcurlarray) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $adAccountId = "act_" . $adAccountId;
        $limit = $limit1;
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            // $params = ".date_preset($type)";

            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&limit=250&access_token=" . $this->access_token;
        }

        $pageName = !empty($pagename) ? $pagename : 'clicks';
        if($pageName == 'campaignsReport'){
            $pageName = 'clicks';
        }
        /*$ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);*/
        $response = $finalcurlarray;
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($genders['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($genders['gender'] == "female") {
                                $female = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "male") {
                                $male = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "unknown") {
                                $other = $feeds1['value'];
                            }
                        }
                    }
                }
                elseif($pageName == 'clicks'){
                    if ($genders['gender'] == "female") {
                        $female = $genders['inline_link_clicks'];
                        $total+=$genders['inline_link_clicks'];
                    } else if ($genders['gender'] == "male") {
                        $male = $genders['inline_link_clicks'];
                        $total+=$genders['inline_link_clicks'];
                    } else if ($genders['gender'] == "unknown") {
                        $other = $genders['inline_link_clicks'];
                    }
                }
                else{
                    if ($genders['gender'] == "female") {
                        $female = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "male") {
                        $male = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "unknown") {
                        $other = $genders[$pageName];
                    }
                }
            }
        }


        $tempstr1 = array();
        $tempstr1[] = number_format($male+$female+$other);
        $tempstr1[] = '<li><span class="text-muted location">MEN</span> <span class="text-bold">'.number_format($male).'</span></li><li><span class="text-muted location">WOMEN</span> <span class="text-bold">'.number_format($female).'</span></li><li><span class="text-muted location">OTHER</span> <span class="text-bold">'.number_format($other).'</span></li>';
        return $tempstr1;
    }

    function getCampaignChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&limit=250&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();
        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $res[] = array(
                                "range" => $ages['age'],
                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                "impressions" => $feeds1['value'],
                                "gender" => $ages['gender']
                            );
                            $total += $ages['inline_link_clicks'];
                        }
                    }
                }
                else{
                    $res[] = array(
                        "range" => $ages['age'],
                        "inline_link_clicks" => $ages['inline_link_clicks'],
                        "impressions" => $ages[$pageName],
                        "gender" => $ages['gender']
                    );
                    $total += $ages['inline_link_clicks'];
                }
            }

            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']][$pageName] = $rs['impressions'];

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }
        $tempArr = array();
        foreach ($resultArray as $k => $value) {
            $tempStr1 = '0';
            foreach ($value as $k1 => $value1) {
                $tempStr1 += $value1[$pageName];
            }
            $tempArr[$k] = $tempStr1;
        }


        if (!array_key_exists('18-24', $resultArray)) {
            $tempArr['18-24'] = 0;
        }
        if (!array_key_exists('25-34', $resultArray)) {
            $tempArr['25-34'] = 0;
        }
        if (!array_key_exists('35-44', $resultArray)) {
            $tempArr['35-44'] = 0;
        }
        if (!array_key_exists('45-54', $resultArray)) {
            $tempArr['45-54'] = 0;
        }
        if (!array_key_exists('55-64', $resultArray)) {
            $tempArr['55-64'] = 0;
        }
        if (!array_key_exists('65+', $resultArray)) {
            $tempArr['65+'] = 0;
        }
        
        foreach ($tempArr as $k => $value) {
            $tempStr .= '{
                            "y":"' . $k . '",
                            "value":' . $value . '
                         },';
        }
        $tempStr2 = rtrim($tempStr, ',');
        $tempstr1 = '[
            ' . $tempStr2 . '
            ]';
            
        echo $tempstr1;
        exit;

    }

	function getCampaignChartvarious() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&limit=250&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();
        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'offsite_conversion.fb_pixel_add_payment_info' || $pageName == 'offsite_conversion.fb_pixel_add_to_cart' || $pageName == 'offsite_conversion.fb_pixel_add_to_wishlist' || $pageName == 'offsite_conversion.fb_pixel_complete_registration' || $pageName == 'offsite_conversion.fb_pixel_custom' || $pageName == 'offsite_conversion.fb_pixel_initiate_checkout' || $pageName == 'offsite_conversion.fb_pixel_lead' || $pageName == 'offsite_conversion.fb_pixel_purchase' || $pageName == 'offsite_conversion.fb_pixel_search' || $pageName == 'offsite_conversion.fb_pixel_view_content' || $pageName == 'offsite_conversion.key_page_view' || $pageName == 'offsite_conversion.lead' || $pageName == 'offsite_conversion.other' || $pageName == 'offsite_conversion.registration'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $res[] = array(
                                "range" => $ages['age'],
                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                "impressions" => $feeds1['value'],
                                "gender" => $ages['gender']
                            );
                            $total += $ages['inline_link_clicks'];
                        }
                    }
                }
                else{
                    $res[] = array(
                        "range" => $ages['age'],
                        "inline_link_clicks" => $ages['inline_link_clicks'],
                        "impressions" => $ages[$pageName],
                        "gender" => $ages['gender']
                    );
                    $total += $ages['inline_link_clicks'];
                }
            }

            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']][$pageName] = $rs['impressions'];

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }
        $tempArr = array();
        foreach ($resultArray as $k => $value) {
            $tempStr1 = '0';
            foreach ($value as $k1 => $value1) {
                $tempStr1 += $value1[$pageName];
            }
            $tempArr[$k] = $tempStr1;
        }


        if (!array_key_exists('18-24', $resultArray)) {
            $tempArr['18-24'] = 0;
        }
        if (!array_key_exists('25-34', $resultArray)) {
            $tempArr['25-34'] = 0;
        }
        if (!array_key_exists('35-44', $resultArray)) {
            $tempArr['35-44'] = 0;
        }
        if (!array_key_exists('45-54', $resultArray)) {
            $tempArr['45-54'] = 0;
        }
        if (!array_key_exists('55-64', $resultArray)) {
            $tempArr['55-64'] = 0;
        }
        if (!array_key_exists('65+', $resultArray)) {
            $tempArr['65+'] = 0;
        }
        
        foreach ($tempArr as $k => $value) {
            $agegroup_total += $value;
            $tempStr .= '<tr><td class="highlight">'.$k.' Years</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($value).($pageName=="ctr" ? "%" : "").'</td></tr>';
            //$tempStr .= '<li><span class="text-muted location">'.$k.'</span> <span class="text-bold">'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($value).($pageName=="ctr" ? "%" : "").'</span></li>';
        }
        $tempstr1 = array();
        if($pageName=="ctr"){
            $tempstr1[] = $this->input->post('clickrate');//number_format((number_format($agegroup_total)/count($tempArr)))."%";
        }
        else{
            $tempstr1[] = ($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($agegroup_total);
        }
        $tempstr1[] = $tempStr;
        echo json_encode($tempstr1);
        exit;

    }
	
    function getCampaignChart_without_ajax($adAccountId,$limit1,$pagename,$finalcurlarray) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $agegroup_total = 0;
        $adAccountId = "act_" . $adAccountId;
        $limit = $limit1;
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&limit=250&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();
        $pageName = !empty($pagename) ? $pagename : 'clicks';
        if($pageName == 'campaignsReport'){
            $pageName = 'clicks';
        }

        /*$ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);*/
        $response = $finalcurlarray;
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $res[] = array(
                                "range" => $ages['age'],
                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                "impressions" => $feeds1['value'],
                                "gender" => $ages['gender']
                            );
                            $total += $ages['inline_link_clicks'];
                        }
                    }
                }
                elseif($pageName == 'clicks'){
                    $res[] = array(
                        "range" => $ages['age'],
                        "inline_link_clicks" => $ages['inline_link_clicks'],
                        "impressions" => $ages['inline_link_clicks'],
                        "gender" => $ages['gender']
                    );
                    $total += $ages['inline_link_clicks'];
                }
                else{
                    $res[] = array(
                        "range" => $ages['age'],
                        "inline_link_clicks" => $ages['inline_link_clicks'],
                        "impressions" => $ages[$pageName],
                        "gender" => $ages['gender']
                    );
                    $total += $ages['inline_link_clicks'];
                }
            }

            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']][$pageName] = $rs['impressions'];

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }
        $tempArr = array();
        foreach ($resultArray as $k => $value) {
            $tempStr1 = '0';
            foreach ($value as $k1 => $value1) {
                $tempStr1 += $value1[$pageName];
            }
            $tempArr[$k] = $tempStr1;
        }


        if (!array_key_exists('18-24', $resultArray)) {
            $tempArr['18-24'] = 0;
        }
        if (!array_key_exists('25-34', $resultArray)) {
            $tempArr['25-34'] = 0;
        }
        if (!array_key_exists('35-44', $resultArray)) {
            $tempArr['35-44'] = 0;
        }
        if (!array_key_exists('45-54', $resultArray)) {
            $tempArr['45-54'] = 0;
        }
        if (!array_key_exists('55-64', $resultArray)) {
            $tempArr['55-64'] = 0;
        }
        if (!array_key_exists('65+', $resultArray)) {
            $tempArr['65+'] = 0;
        }
        
        foreach ($tempArr as $k => $value) {
            $agegroup_total += $value;
            $tempStr .= '<li><span class="text-muted location">'.$k.'</span> <span class="text-bold">'.number_format($value).'</span></li>';
        }
        $tempstr1 = array();
        $tempstr1[] = number_format($agegroup_total);
        $tempstr1[] = $tempStr;
        return $tempstr1;
    }

    function getCampaignCountryChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
        }



        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $tempStr .= '{
                                            "label":"' . $result[0]->Country . '",
                                            "value":' . $feeds1['value'] . '
                                         },';
                        }
                    }
                }
                else{
                    $tempStr .= '{
                            "label":"' . $result[0]->Country . '",
                            "value":' . $ages[$pageName] . '
                         },';
                }
            }
        }

        $tempStr2 = rtrim($tempStr, ',');
        $tempstr1 = '[
            ' . $tempStr2 . '
            ]';
        echo $tempstr1;
        exit;
    }
	
	function getCampaignCountryChartvarious() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $countrytotal = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=6&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=6&access_token=" . $this->access_token;
        }



        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'offsite_conversion.fb_pixel_add_payment_info' || $pageName == 'offsite_conversion.fb_pixel_add_to_cart' || $pageName == 'offsite_conversion.fb_pixel_add_to_wishlist' || $pageName == 'offsite_conversion.fb_pixel_complete_registration' || $pageName == 'offsite_conversion.fb_pixel_custom' || $pageName == 'offsite_conversion.fb_pixel_initiate_checkout' || $pageName == 'offsite_conversion.fb_pixel_lead' || $pageName == 'offsite_conversion.fb_pixel_purchase' || $pageName == 'offsite_conversion.fb_pixel_search' || $pageName == 'offsite_conversion.fb_pixel_view_content' || $pageName == 'offsite_conversion.key_page_view' || $pageName == 'offsite_conversion.lead' || $pageName == 'offsite_conversion.other' || $pageName == 'offsite_conversion.registration'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $countrytotal += $feeds1['value'];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'. ($pageName=="spend" ? $this->session->userdata('cur_currency') : ""). number_format($feeds1['value']).($pageName=="ctr" ? "%" : "") .'</span></li>';
                        }
                    }
                }
                else{
                    $countrytotal += $ages[$pageName];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'. ($pageName=="spend" ? $this->session->userdata('cur_currency') : "") . number_format($ages[$pageName]).($pageName=="ctr" ? "%" : "") .'</span></li>';
                }
            }
        }

        
        $tempstr1 = array();
        if($pageName=="ctr"){
            $tempstr1[] = $this->input->post('clickrate');//number_format((number_format($countrytotal)/count($response['data'])))."%";
        }
        else{
            $tempstr1[] = ($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($countrytotal);
        }
        $tempstr1[] = $tempStr;
        echo json_encode($tempstr1);
        exit;
    }

    function getCampaignCountryChart_without_ajax($adAccountId,$limit1,$pagename,$finalcurlarray) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $countrytotal = 0;
        $adAccountId = "act_" . $adAccountId;
        $limit = $limit1;
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
        }



        $res = array();
        $resultArray = array();

        $pageName = !empty($pagename) ? $pagename : 'clicks';
        if($pageName == 'campaignsReport'){
            $pageName = 'clicks';
        }

        /*$ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);*/
        $response = $finalcurlarray;
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $countrytotal += (int)$feeds1['value'];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                        }
                    }
                }
                elseif($pageName == 'clicks'){
                    $countrytotal += (int)$ages['inline_link_clicks'];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages['inline_link_clicks']).'</span></li>';
                }
                else{
                    $countrytotal += (int)$ages[$pageName];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                }
            }
        }

        
        $tempstr1 = array();
        $tempstr1[] = number_format($countrytotal);
        $tempstr1[] = $tempStr;
        return $tempstr1;
    }

    function getCampaignTimeChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,reach,impressions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&sort=['reach_descending']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=30&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
        }
       # echo $url;exit;
        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $time_4_am = strtotime('04:00:00');
        $time_8_am = strtotime('08:00:00');
        $time_12_pm = strtotime('12:00:00');
        $time_4_pm = strtotime('16:00:00');
        $time_8_pm = strtotime('20:00:00');
        $time_12_am = strtotime('23:59:59');

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $timeArr = explode(' - ', $ages['hourly_stats_aggregated_by_audience_time_zone']);
                $time = strtotime($timeArr[0]);
                if ($time_12_am > $time && $time_4_am >= $time) {
                    $post_12am_to4am[] = $ages;
                } else if ($time_4_am < $time && $time_8_am >= $time) {
                    $post_4am_to8am[] = $ages;
                } else if ($time_8_am < $time && $time_12_pm >= $time) {
                    $post_8am_to12pm[] = $ages;
                } else if ($time_12_pm < $time && $time_4_pm >= $time) {
                    $post_12pm_to4pm[] = $ages;
                } else if ($time_4_pm < $time && $time_8_pm >= $time) {
                    $post_4pm_to8pm[] = $ages;
                } else if ($time_8_pm < $time && $time_12_am >= $time) {
                    $post_8pm_to12am[] = $ages;
                }
            }
        }
        $count_12am_to4am = 0;
        if (is_array($post_12am_to4am)) {
            foreach ($post_12am_to4am as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_12am_to4am = $count_12am_to4am + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_12am_to4am = $count_12am_to4am + $post[$pageName];
                }
            }
        }
        $count_4am_to8am = 0;
        if (is_array($post_4am_to8am)) {
            foreach ($post_4am_to8am as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_4am_to8am = $count_4am_to8am + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_4am_to8am = $count_4am_to8am + $post[$pageName];
                }
            }
        }
        $count_8am_to12pm = 0;
        if (is_array($post_8am_to12pm)) {
            foreach ($post_8am_to12pm as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_8am_to12pm = $count_8am_to12pm + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_8am_to12pm = $count_8am_to12pm + $post[$pageName];
                }
            }
        }
        $count_12pm_to4pm = 0;
        if (is_array($post_12pm_to4pm)) {
            foreach ($post_12pm_to4pm as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_12pm_to4pm = $count_12pm_to4pm + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_12pm_to4pm = $count_12pm_to4pm + $post[$pageName];
                }
            }
        }
        $count_4pm_to8pm = 0;
        if (is_array($post_4pm_to8pm)) {
            foreach ($post_4pm_to8pm as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_4pm_to8pm = $count_4pm_to8pm + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_4pm_to8pm = $count_4pm_to8pm + $post[$pageName];
                }
            }
        }
        $count_8pm_to12am = 0;
        if (is_array($post_8pm_to12am)) {
            foreach ($post_8pm_to12am as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_8pm_to12am = $count_8pm_to12am + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_8pm_to12am = $count_8pm_to12am + $post[$pageName];
                }
            }
        }

        $tempStr .= '{
                        "y":"0-4",
                        "Impressions":' . $count_12am_to4am . '
                     },{
                        "y":"5-8",
                        "Impressions":' . $count_4am_to8am . '
                     },{
                        "y":"9-12",
                        "Impressions":' . $count_8am_to12pm . '
                     },{
                        "y":"13-16",
                        "Impressions":' . $count_12pm_to4pm . '
                     },{
                        "y":"17-20",
                        "Impressions":' . $count_4pm_to8pm . '
                     },{
                        "y":"21-24",
                        "Impressions":' . $count_8pm_to12am . '
                     }';

        $tempstr1 = '[
            ' . $tempStr . '
            ]';
        echo $tempstr1;
        exit;
    }

    function getCampaignFeeds() {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions','total_actions']&breakdowns=['publisher_platform','device_platform','platform_position']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&limit=250&access_token=" . $this->access_token;
        }

        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);
        //echo "<pre>";print_r($response);echo "</pre>";exit;
        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
               /* if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['placement'] == "desktop_feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "instant_article") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_external_only") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "right_hand") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['placement'] == "desktop_feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "instant_article") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_external_only") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "right_hand") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                } */
				
				if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "instagram") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "audience_network") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "instagram") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "audience_network") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }
            }
            if ($desktop > 0) {
                $desktopPer = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobilePer = round(($mobile / $total) * 100);
            }
            if ($instant_article > 0) {
                $instant_articlePer = round(($instant_article / $total) * 100);
            }
            if ($mobile_external_only > 0) {
                $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
            }
            if ($right_hand > 0) {
                $right_handPer = round(($right_hand / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => !empty($desktop) ? $desktop : '0',
            "mobile" => !empty($mobile) ? $mobile : '0',
            "instant_article" => !empty($instant_article) ? $instant_article : '0',
            "mobile_external_only" => !empty($mobile_external_only) ? $mobile_external_only : '0',
            "right_hand" => !empty($right_hand) ? $right_hand : '0',
            "desktopPer" => !empty($desktopPer) ? $desktopPer.'%' : '0%',
            "mobilePer" => !empty($mobilePer) ? $mobilePer.'%' : '0%',
            "instant_articlePer" => !empty($instant_articlePer) ? $instant_articlePer.'%' : '0%',
            "mobile_external_onlyPer" => !empty($mobile_external_onlyPer) ? $mobile_external_onlyPer.'%' : '0%',
            "right_handPer" => !empty($right_handPer) ? $right_handPer.'%' : '0%',
            "total" => $total
        );
		//echo "<pre>";print_r($response);echo "</pre>";exit;
        echo json_encode($response);
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/adsets?name='My Ad Set'&optimization_goal=REACH&campaign_id=CAMPAIGN_ID&access_token=" . $this->access_token;
    }
	function getCampaignFeedsvarious() {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $limit = $this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions','total_actions']&breakdowns=['publisher_platform','device_platform','platform_position']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&limit=250&access_token=" . $this->access_token;
        }

        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'campaignsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);
        //echo "<pre>";print_r($response);echo "</pre>";exit;
        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
               /* if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['placement'] == "desktop_feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "instant_article") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_external_only") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "right_hand") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['placement'] == "desktop_feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "instant_article") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_external_only") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "right_hand") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                } */
				
				if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion' || $pageName == 'offsite_conversion.fb_pixel_add_payment_info' || $pageName == 'offsite_conversion.fb_pixel_add_to_cart' || $pageName == 'offsite_conversion.fb_pixel_add_to_wishlist' || $pageName == 'offsite_conversion.fb_pixel_complete_registration' || $pageName == 'offsite_conversion.fb_pixel_custom' || $pageName == 'offsite_conversion.fb_pixel_initiate_checkout' || $pageName == 'offsite_conversion.fb_pixel_lead' || $pageName == 'offsite_conversion.fb_pixel_purchase' || $pageName == 'offsite_conversion.fb_pixel_search' || $pageName == 'offsite_conversion.fb_pixel_view_content' || $pageName == 'offsite_conversion.key_page_view' || $pageName == 'offsite_conversion.lead' || $pageName == 'offsite_conversion.other' || $pageName == 'offsite_conversion.registration'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "instagram") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "audience_network") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "instagram") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "audience_network") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }
            }
            if ($desktop > 0) {
                $desktopPer = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobilePer = round(($mobile / $total) * 100);
            }
            if ($instant_article > 0) {
                $instant_articlePer = round(($instant_article / $total) * 100);
            }
            if ($mobile_external_only > 0) {
                $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
            }
            if ($right_hand > 0) {
                $right_handPer = round(($right_hand / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => !empty($desktop) ? $desktop : '0',
            "mobile" => !empty($mobile) ? $mobile : '0',
            "instant_article" => !empty($instant_article) ? $instant_article : '0',
            "mobile_external_only" => !empty($mobile_external_only) ? $mobile_external_only : '0',
            "right_hand" => !empty($right_hand) ? $right_hand : '0',
            "desktopPer" => !empty($desktopPer) ? $desktopPer.'%' : '0%',
            "mobilePer" => !empty($mobilePer) ? $mobilePer.'%' : '0%',
            "instant_articlePer" => !empty($instant_articlePer) ? $instant_articlePer.'%' : '0%',
            "mobile_external_onlyPer" => !empty($mobile_external_onlyPer) ? $mobile_external_onlyPer.'%' : '0%',
            "right_handPer" => !empty($right_handPer) ? $right_handPer.'%' : '0%',
            "total" => $total
        );
		
		$tempstr1 = array();
        if($pageName=="ctr"){
            $tempstr1[] = $this->input->post('clickrate');//number_format((number_format($total)/5))."%";
        }
        else{
            $tempstr1[] = ($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($total);
        }
        if(empty($desktop)) $desktop=0;
        if(empty($mobile)) $mobile=0;
        if(empty($instant_article)) $instant_article=0;
        if(empty($mobile_external_only)) $mobile_external_only=0;
        if(empty($right_hand)) $right_hand=0;
        $tempstr1[] = '<tr><td class="highlight">DESKTOP FEED</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($desktop).($pageName=="ctr" ? "%" : "").'</td></tr>
                                <tr><td class="highlight">MOBILE FEED</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($mobile).($pageName=="ctr" ? "%" : "").'</td></tr>
                                <tr><td class="highlight">INSTAGRAM</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($instant_article).($pageName=="ctr" ? "%" : "").'</td></tr>
                                <tr><td class="highlight">AUDIENCE</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($mobile_external_only).($pageName=="ctr" ? "%" : "").'</td></tr>
                                <tr><td class="highlight">RIGHT SIDE</td><td>'.($pageName=="spend" ? $this->session->userdata('cur_currency') : "").number_format($right_hand).($pageName=="ctr" ? "%" : "").'</td></tr>';
        
		//echo "<pre>";print_r($response);echo "</pre>";exit;
        echo json_encode($tempstr1);
		exit;
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/adsets?name='My Ad Set'&optimization_goal=REACH&campaign_id=CAMPAIGN_ID&access_token=" . $this->access_token;
    }

    function getCampaignFeeds_without_ajax($adAccountId,$limit1,$pagename,$finalcurlarray) {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $adAccountId = "act_" . $adAccountId;
        $limit = $limit1;
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions','total_actions']&breakdowns=['publisher_platform','device_platform','platform_position']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&limit=250&access_token=" . $this->access_token;
        }

        $res = array();
        $resultArray = array();

        $pageName = !empty($pagename) ? $pagename : 'clicks';
        if($pageName == 'campaignsReport'){
            $pageName = 'clicks';
        }

       /* $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);*/
        $response = $finalcurlarray;
        //$this->getPrintResult($response);
        //echo "<pre>";print_r($response);echo "</pre>";exit;
        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
               /* if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['placement'] == "desktop_feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "instant_article") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_external_only") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "right_hand") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['placement'] == "desktop_feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "instant_article") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_external_only") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "right_hand") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                } */
                
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "instagram") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "audience_network") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                elseif($pageName == 'clicks'){
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $desktop = $feeds['inline_link_clicks'];
                        $total +=$feeds['inline_link_clicks'];
                    }
                    if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $mobile = $feeds['inline_link_clicks'];
                        $total +=$feeds['inline_link_clicks'];
                    }
                    if ($feeds['publisher_platform'] == "instagram") {
                        $instant_article = $feeds['inline_link_clicks'];
                        $total +=$feeds['inline_link_clicks'];
                    }
                    if ($feeds['publisher_platform'] == "audience_network") {
                        $mobile_external_only = $feeds['inline_link_clicks'];
                        $total +=$feeds['inline_link_clicks'];
                    }
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                        $right_hand = $feeds['inline_link_clicks'];
                        $total +=$feeds['inline_link_clicks'];
                    }
                }
                else{
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "instagram") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "audience_network") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }
            }
            if ($desktop > 0) {
                $desktopPer = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobilePer = round(($mobile / $total) * 100);
            }
            if ($instant_article > 0) {
                $instant_articlePer = round(($instant_article / $total) * 100);
            }
            if ($mobile_external_only > 0) {
                $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
            }
            if ($right_hand > 0) {
                $right_handPer = round(($right_hand / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => !empty($desktop) ? $desktop : '0',
            "mobile" => !empty($mobile) ? $mobile : '0',
            "instant_article" => !empty($instant_article) ? $instant_article : '0',
            "mobile_external_only" => !empty($mobile_external_only) ? $mobile_external_only : '0',
            "right_hand" => !empty($right_hand) ? $right_hand : '0',
            "desktopPer" => !empty($desktopPer) ? $desktopPer.'%' : '0%',
            "mobilePer" => !empty($mobilePer) ? $mobilePer.'%' : '0%',
            "instant_articlePer" => !empty($instant_articlePer) ? $instant_articlePer.'%' : '0%',
            "mobile_external_onlyPer" => !empty($mobile_external_onlyPer) ? $mobile_external_onlyPer.'%' : '0%',
            "right_handPer" => !empty($right_handPer) ? $right_handPer.'%' : '0%',
            "total" => $total
        );

        $tempstr1 = array();
        $tempstr1[] = number_format($total);
        if(empty($desktop)) $desktop=0;
        if(empty($mobile)) $mobile=0;
        if(empty($instant_article)) $instant_article=0;
        if(empty($mobile_external_only)) $mobile_external_only=0;
        if(empty($right_hand)) $right_hand=0;
        $tempstr1[] = '<li><span class="text-muted location">DESKTOP FEED</span> <span class="text-bold">'.number_format($desktop).'</span></li>
                                <li><span class="text-muted location">MOBILE FEED</span> <span class="text-bold">'.number_format($mobile).'</span></li>
                                <li><span class="text-muted location">INSTAGRAM</span> <span class="text-bold">'.number_format($instant_article).'</span></li>
                                <li><span class="text-muted location">AUDIENCE</span> <span class="text-bold">'.number_format($mobile_external_only).'</span></li>
                                <li><span class="text-muted location">RIGHT SIDE</span> <span class="text-bold">'.number_format($right_hand).'</span></li>';
        return $tempstr1;
        //echo "<pre>";print_r($response);echo "</pre>";exit;
        //echo json_encode($response);
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/adsets?name='My Ad Set'&optimization_goal=REACH&campaign_id=CAMPAIGN_ID&access_token=" . $this->access_token;
    }

    function sksort(&$array, $subkey = "id", $subkey2 = null, $sort_ascending = true) {
        if (count($array))
            $temp_array[key($array)] = array_shift($array);
        foreach ($array as $key => $val) {
            $offset = 0;
            $found = false;
            foreach ($temp_array as $tmp_key => $tmp_val) {
                if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                    $temp_array = array_merge(
                            (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));
                    $found = true;
                } elseif (!$found
                        and $subkey2 and strtolower($val[$subkey]) == strtolower($tmp_val[$subkey])
                        and strtolower($val[$subkey2]) > strtolower($tmp_val[$subkey2])) {
                    $temp_array = array_merge(
                            (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));
                    $found = true;
                }
                $offset++;
            }
            if (!$found)
                $temp_array = array_merge($temp_array, array($key => $val));
        }
        if ($sort_ascending)
            $array = array_reverse($temp_array);
        else
            $array = $temp_array;

        return $array;
    }

    function clickData(){
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $AdSet = $userArr[0]->fb_ad_adset_id;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $campaignObj = new AdAccount($adAccountId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'last_7_days',
            'time_increment' => 1,//all_days
        );

        //$fields = array('clicks','date_start','spend');
		$fields = array('inline_link_clicks','date_start','spend');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'inline_link_clicks' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $clicks = 0;
                if(!empty($value['inline_link_clicks'])){
                    $clicks = $value['inline_link_clicks'];
                }
                $tempStr .= '{
                            "year":"'.$value['date_start'].'",
                            "value":' . $clicks . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function engagementData(){
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $AdSet = $userArr[0]->fb_ad_adset_id;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $campaignObj = new AdAccount($adAccountId);

         $this->date_from = date('Y-m-d', strtotime('-7 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'last_7_days',
            'time_increment' => 1,//all_days
        );

        $fields = array('actions','cost_per_action_type');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if($k1 == 'actions'){
                        $i=0;
                        foreach ($pt['actions'] as $key1 => $value1) {
                            if($value1['action_type'] == 'page_engagement'){
                                $arr[$k][$value1['action_type']] = $value1['value'];
                                $arr[$k]['cost_per_action_type'] = $pt['cost_per_action_type'][$i]['value'];
                            }
                            $i++;
                        }
                        $l++;
                    }
                    if ($k1 == 'date_start') {
                        $arr[$k]['date_start'] = $v1;
                        if (isset($kv_dates[$v1])) {
                            unset($kv_dates[$v1]);
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'page_engagement' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);

        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $page_engagement = 0;
                if(!empty($value['page_engagement'])){
                    $page_engagement = $value['page_engagement'];
                }
                $tempStr .= '{
                    "year":"'.$value['date_start'].'",
                    "value":' . $page_engagement . '
                 },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;
            exit;
        }
    }

    function pageLikeData(){
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $AdSet = $userArr[0]->fb_ad_adset_id;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $campaignObj = new AdAccount($adAccountId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'last_7_days',
            'time_increment' => 1,//all_days
        );

        $fields = array('actions','cost_per_action_type');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if($k1 == 'actions'){
                        $i=0;
                        foreach ($pt['actions'] as $key1 => $value1) {
                            if($value1['action_type'] == 'like'){
                                $arr[$k]['like'] = $value1['value'];
                                $arr[$k]['cost_per_action_type'] = $pt['cost_per_action_type'][$i]['value'];
                            }
                            $i++;
                        }
                        $l++;
                    }
                    if ($k1 == 'date_start') {
                        $arr[$k]['date_start'] = $v1;
                        if (isset($kv_dates[$v1])) {
                            unset($kv_dates[$v1]);
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'like' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);

        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $like = 0;
                if(!empty($value['like'])){
                    $like = $value['like'];
                }
                $tempStr .= '{
                    "y":"'.$value['date_start'].'",
                    "a":' . $like . '
                 },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;
            exit;
        }
    }

    function conversionData(){
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $AdSet = $userArr[0]->fb_ad_adset_id;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $campaignObj = new AdAccount($adAccountId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'last_7_days',
            'time_increment' => 1,//all_days
        );

        $fields = array('actions','cost_per_action_type');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if($k1 == 'actions'){
                        $i=0;
                        foreach ($pt['actions'] as $key1 => $value1) {
                            if($value1['action_type'] == 'offsite_conversion'){
                                $arr[$k]['offsite_conversion'] = $value1['value'];
                                $arr[$k]['cost_per_action_type'] = $pt['cost_per_action_type'][$i]['value'];
                            }
                            $i++;
                        }
                        $l++;
                    }
                    if ($k1 == 'date_start') {
                        $arr[$k]['date_start'] = $v1;
                        if (isset($kv_dates[$v1])) {
                            unset($kv_dates[$v1]);
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'offsite_conversion' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);

        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $offsite_conversion = 0;
                if(!empty($value['offsite_conversion'])){
                    $offsite_conversion = $value['offsite_conversion'];
                }
                $tempStr .= '{
                    "year":"'.$value['date_start'].'",
                    "value":' . $offsite_conversion . '
                 },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;
            exit;
        }
    }

    function ctrData(){
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $AdSet = $userArr[0]->fb_ad_adset_id;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $campaignObj = new AdAccount($adAccountId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'last_7_days',
            'time_increment' => 1,//all_days
        );

        $fields = array('ctr','date_start');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'ctr' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $ctr = 0;
                if(!empty($value['ctr'])){
                    $ctr = $value['ctr'];
                }
                $tempStr .= '{
                            "y":"'.$value['date_start'].'",
                            "a":' . $ctr . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function getCampaignSpentData(){
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $AdSet = $userArr[0]->fb_ad_adset_id;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $campaignObj = new AdAccount($adAccountId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'last_7_days',
            'time_increment' => 1,//all_days
        );

        $fields = array('spend','date_start');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'spend' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $spend = 0;
                if(!empty($value['spend'])){
                    $spend = $value['spend'];
                }
                $tempStr .= '{
                            "year":"'.$value['date_start'].'",
                            "value":' . $spend . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function getCampaignImpressionData(){
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $AdSet = $userArr[0]->fb_ad_adset_id;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $campaignObj = new AdAccount($adAccountId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'last_7_days',
            'time_increment' => 1,//all_days
        );

        $fields = array('impressions','date_start');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'impressions' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $impressions = 0;
                if(!empty($value['impressions'])){
                    $impressions = $value['impressions'];
                }
                $tempStr .= '{
                            "year":"'.$value['date_start'].'",
                            "value":' . $impressions . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function getCampaignCtrData(){
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $AdSet = $userArr[0]->fb_ad_adset_id;
        $adAccountId = "act_" . $this->input->post('adAccountId');
        $campaignObj = new AdAccount($adAccountId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'last_7_days',
            'time_increment' => 1,//all_days
        );

        //$fields = array('ctr','date_start');
		$fields = array('inline_link_click_ctr','date_start');
		
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'inline_link_click_ctr' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $ctr = 0;
                if(!empty($value['inline_link_click_ctr'])){
                    $ctr = $value['inline_link_click_ctr'];
                }
                $tempStr .= '{
                            "year":"'.$value['date_start'].'",
                            "value":' . $ctr . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function setDateSession(){
        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $limit = $this->input->post('limit');
            $this->session->set_userdata('dateval', $limit);
        }        
    }
    
    function setcampaignSession(){
        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $limit = $this->input->post('limit');
            $this->session->set_userdata('camptype', $limit);
        }        
    }

    public static function fillDatesKeys($date_from, $date_to) {
        $kv = [];
        $date = $date_from;
        while ($date <= $date_to) {
            $kv[$date] = 0;
            $date = date("Y-m-d", strtotime($date) + 86400);
        }

        return $kv;
    }

    public static function orderByField($arr, $f, $ascending = true) {
        if (is_array($f)) {
            $f1 = $f[0];
            $ka = [];
            foreach ($arr as $el) {
                $f1v = $el[$f1];
                //hr($f1v);
                if (!isset($ka[$f1v]))
                    $ka[$f1v] = [];
                $ka[$f1v][] = $el;
            }

            //hre($ka);

            if ($ascending)
                ksort($ka);
            else
                krsort($ka);

            $arr = [];
            foreach ($ka as $els) {
                $f2 = $f[1];
                $els = $this->orderByField($els, $f2, $ascending);
                $arr = array_merge($arr, $els);
            }

            return $arr;
        }

        $kv = [];
        foreach ($arr as $k => $v)
            $kv[$k] = $v[$f];

        if ($ascending)
            asort($kv);
        else
            arsort($kv);

        $res = [];
        foreach ($kv as $k => $v)
            $res[] = $arr[$k];

        return $res;
    }
}
