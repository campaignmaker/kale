<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
    }

    public function index() {
        //echo 'Hello World!';
    }

    public function about_us() {
        loadView('aboutus');
    }

    public function privacy_policy() {
        loadView('privacypolicy');
    }

    public function terms_of_services() {
        loadView('termsofservices');
    }
    public function helps() {
        loadView('helps');
    }
     public function welcome() { 
        //remove_css(array('style.css','waqas.css','font-awesome.css','bootstrap.css'));
        //$this->load->view('welcome/header');
        $this->load->view('welcome/index');
        //$this->load->view('welcome/footer');
    }

    function sendHelpQuery(){
		
		try{
		$to = "samy@thecampaignmaker.com";
		$subject = "QUERY";
		
		$message = "
		<html>
		<head>
		<title>QUERY</title>
		</head>
		<body>
		<p>Hey Admin,</p>
		<br>
		<p>This email contains query!</p>
		<strong>First Name :</strong>
		<p>".$this->input->post('name')."</p>
		<br>
		<strong>Last Name :</strong>
		<p>".$this->input->post('surname')."</p>
		<br>
		<strong>Email :</strong>
		<p>".$this->input->post('email')."</p>
		<br>
		<strong>Phone :</strong>
		<p>".$this->input->post('phone')."</p>
		<br>
		<strong>Message :</strong>
		<p>".$this->input->post('message')."</p>
		</body>
		</html>
		";
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// More headers
		//$headers .= 'From: <'.$this->input->post('rcemail').'>' . "\r\n";
		$headers .= 'From: <'.$this->input->post('email').'>' . "\r\n";
		//$headers .= 'Cc: muhammadjunaid85@gmail.com' . "\r\n";
		//$headers .= 'Reply-To: '.$this->input->post('rcemail') . "\r\n";
		$headers .= 'Reply-To: '.$this->input->post('email') . "\r\n";
		
		mail($to,$subject,$message,$headers);
		
		}
		catch(Exception $ex){
			echo $ex->getMessage();
		}
		//exit;
		$this->session->set_flashdata('settingsmessagehelp', 'y');
		
        // After that you need to used redirect function instead of load view such as 
        redirect("helps");
    }
}