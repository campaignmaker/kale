<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\TargetingSearch;
use FacebookAds\Object\Search\TargetingSearchTypes;
use FacebookAds\Object\TargetingSpecs;
use FacebookAds\Object\Fields\TargetingSpecsFields;
use FacebookAds\Object\AdImage;
use FacebookAds\Object\Fields\AdImageFields;
use FacebookAds\Object\AdVideo;
use FacebookAds\Object\Fields\AdVideoFields;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdPreviewFields;
use FacebookAds\Object\Values\AdFormats;
use FacebookAds\Object\Fields\ObjectStory\AttachmentDataFields;
use FacebookAds\Object\ObjectStory\AttachmentData;
use FacebookAds\Object\ObjectStory\LinkData;
use FacebookAds\Object\Fields\ObjectStory\LinkDataFields;
use FacebookAds\Object\ObjectStory\VideoData;
use FacebookAds\Object\Fields\ObjectStory\VideoDataFields;
use FacebookAds\Object\ObjectStorySpec;
use FacebookAds\Object\Fields\ObjectStorySpecFields;
use FacebookAds\Object\Values\CallToActionTypes;
use FacebookAds\Object\Values\OptimizationGoals;
use FacebookAds\Object\Values\PageTypes;
use FacebookAds\Object\Values\BillingEvents;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\AdCreativeVideoData;
use FacebookAds\Object\Fields\AdCreativeVideoDataFields;
use FacebookAds\Object\Values\AdCreativeCallToActionTypeValues;

global $data;
$data = new stdClass();

class Createcampaign extends CI_Controller {

    public $creative_pageId, $ad_account_id, $creative_link, $creative_img_hash, $creative_name, $creative_msg, $creative_cpation, $creative_call_to_action, $creative_link_desc, $creative_body;
    public $compaingname_1, $campaignobjective, $adsLimit, $adsetLmit, $cmp_ID, $ageMin, $ageMix, $budget_type, $budgetAmount, $adset_start_date, $adset_end_date, $genders, $page_types, $relationship_statuses, $trackingPixel, $tracking_pixel_tag, $newtracking_pixel_name;

    public function getTracking_pixel_tag() {
        return $this->tracking_pixel_tag;
    }

    public function getNewtracking_pixel_name() {
        return $this->newtracking_pixel_name;
    }

    public function setTracking_pixel_tag($tracking_pixel_tag) {
        $this->tracking_pixel_tag = $tracking_pixel_tag;
    }

    public function setNewtracking_pixel_name($newtracking_pixel_name) {
        $this->newtracking_pixel_name = $newtracking_pixel_name;
    }

    public function getTrackingPixel() {
        return $this->trackingPixel;
    }

    public function setTrackingPixel($trackingPixel) {
        $this->trackingPixel = $trackingPixel;
    }

    public function getRelationship_statuses() {
        return $this->relationship_statuses;
    }

    public function setRelationship_statuses($relationship_statuses) {
        $this->relationship_statuses = $relationship_statuses;
    }

    public function getAdset_start_date() {
        return $this->adset_start_date;
    }

    public function getAdset_end_date() {
        return $this->adset_end_date;
    }

    public function getGenders() {
        return $this->genders;
    }

    public function getPage_types() {
        return $this->page_types;
    }

    public function setAdset_start_date($adset_start_date) {
        $this->adset_start_date = $adset_start_date;
    }

    public function setAdset_end_date($adset_end_date) {
        $this->adset_end_date = $adset_end_date;
    }

    public function setGenders($genders) {
        $this->genders = $genders;
    }

    public function setPage_types($page_types) {
        $this->page_types = $page_types;
    }

    public function getAgeMin() {
        return $this->ageMin;
    }

    public function getAgeMix() {
        return $this->ageMix;
    }

    public function getBudget_type() {
        return $this->budget_type;
    }

    public function getBudgetAmount() {
        return $this->budgetAmount;
    }

    public function setAgeMin($ageMin) {
        $this->ageMin = $ageMin;
    }

    public function setAgeMix($ageMix) {
        $this->ageMix = $ageMix;
    }

    public function setBudget_type($budget_type) {
        $this->budget_type = $budget_type;
    }

    public function setBudgetAmount($budgetAmount) {
        $this->budgetAmount = $budgetAmount;
    }

    public function getCmp_ID() {
        return $this->cmp_ID;
    }

    public function setCmp_ID($cmp_ID) {
        $this->cmp_ID = $cmp_ID;
    }

    public function getCompaingname_1() {
        return $this->compaingname_1;
    }

    public function getCampaignobjective() {
        return $this->campaignobjective;
    }

    public function setCompaingname_1($compaingname_1) {
        $this->compaingname_1 = $compaingname_1;
    }

    public function setCampaignobjective($campaignobjective) {
        $this->campaignobjective = $campaignobjective;
    }

    public function getCreative_pageId() {
        return $this->creative_pageId;
    }

    public function getAd_accountt() {
        return $this->ad_account_id;
    }

    public function getCreative_link() {
        return $this->creative_link;
    }

    public function getCreative_img_hash() {
        return $this->creative_img_hash;
    }

    public function getCreative_name() {
        return $this->creative_name;
    }

    public function getCreative_msg() {
        return $this->creative_msg;
    }

    public function getCreative_cpation() {
        return $this->creative_cpation;
    }

    public function getCreative_call_to_action() {
        return $this->creative_call_to_action;
    }

    public function getCreative_link_desc() {
        return $this->creative_link_desc;
    }

    public function getCreative_body() {
        return $this->creative_body;
    }

    public function setCreative_pageId($creative_pageId) {
        $this->creative_pageId = $creative_pageId;
    }

    public function setAd_accountt($ad_account_id) {
        $this->ad_account_id = $ad_account_id;
    }

    public function setCreative_link($creative_link) {
        $this->creative_link = $creative_link;
    }

    public function setCreative_img_hash($creative_img_hash) {
        $this->creative_img_hash = $creative_img_hash;
    }

    public function setCreative_name($creative_name) {
        $this->creative_name = $creative_name;
    }

    public function setCreative_msg($creative_msg) {
        $this->creative_msg = $creative_msg;
    }

    public function setCreative_cpation($creative_cpation) {
        $this->creative_cpation = $creative_cpation;
    }

    public function setCreative_call_to_action($creative_call_to_action) {
        $this->creative_call_to_action = $creative_call_to_action;
    }

    public function setCreative_link_desc($creative_link_desc) {
        $this->creative_link_desc = $creative_link_desc;
    }

    public function setCreative_body($creative_body) {
        $this->creative_body = $creative_body;
    }

    public function __construct() {
        parent::__construct();
        global $fb;
        error_reporting(E_ERROR);
        ini_set('max_execution_time', 2000); //300 seconds = 5 minutes
        ini_set('max_input_time', 2000); //300 seconds = 5 minutes
        $this->seo->SetValues('Title', "Create Campaign");
        $this->seo->SetValues('Description', "Create Campaign");
        $this->load->model('Users_model');
        $this->load->model('Campaign_model');
        $this->load->model('Addesign_model');
        $this->load->model('AddObjective_model');
        $this->load->model('Audience_model');
        $this->load->model('Campaignobjective_model');
        $this->load->model('Userbudget_model');
        $this->load->model('Campaigndarft_model');
        $this->load->model('Spendcron_model');

        
        //Ads and AdsetLmit
        $this->adsLimit = 20;
        $this->adsetLmit = 50;

        /*
         * Facebook Configuration-------------------- 
         */
        $fb = new Facebook([
            'app_id' => $this->config->item('facebook_app_id'),
            'app_secret' => $this->config->item('facebook_app_secret'),
            'default_graph_version' => $this->config->item('facebook_graph_version'),
        ]);
        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
        $this->user_id = $this->session->userdata['logged_in']['id'];

         if (!empty($this->session->userdata['logged_in'])) {
            $app_id = $this->config->item('facebook_app_id');
            $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);

            
        } else {
			//echo current_url(); exit;
			$this->session->set_userdata('last_page', current_url());
			
			header("Location:".$this->config->item('site_url'));die;
        }
        $response = $this->Users_model->get_cancel_subscription($this->session->userdata['logged_in']['email']);
		if($response == 'No Subscribe'){
		if($this->session->userdata['logged_in']['accesstoken'] == ''){
				header("Location:".$this->config->item('site_url').'connect-to-facebook/');
		}else{	redirect($this->config->item('site_url').'one-step-closer/'); }
		}elseif($response == 'Cancelled' || $response == 'Expired'){
			//redirect($this->config->item('site_url').'final-payment/');
		}
    }
    
    public function getCurrency() {
        $adAccountId = "act_" . $this->session->userdata('UserAdAccount');
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/?fields=name,currency&access_token=" . $this->access_token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $currency = 'USD';
        if (!empty($response['currency'])) {
            $currency = $response['currency'];
        }
        $locale = 'en-US'; //browser or user locale
        $fmt = new NumberFormatter($locale . "@currency=$currency", NumberFormatter::CURRENCY);
        $symbol = $fmt->getSymbol(NumberFormatter::CURRENCY_SYMBOL);
        $this->session->set_userdata('cur_currency', $symbol);
        $this->session->set_userdata('cur_code', $currency);
    }

    public function index($adAccount = NULL, $campaignId = NULL) {
        global $data;
        /*
         * Get F.b Pages of current Usrr
         */
        try {
            $data->getUsersPages = $this->getUsersPages();
        } catch (Exception $e) {
            //echo 'Caught exception: ', $e->getMessage(), "\n";
        }

        /*
         * Get User Ad Account---------------------------------------------
         */
        $data->getAdAccountes = $this->Users_model->getUserAddAccountId($this->user_id);
        if ($adAccount) {
            $this->adAccount = $adAccount;
            $draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $adAccount);
            if ($draft_data) {
                $data->Cmpdraft = $draft_data;
            } else {
                $data->Cmpdraft = $this->getCurlAdset(1, $campaignId);
            }

            $adAccountId = 'act_'.$data->Cmpdraft->adaccount;
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/customaudiences?fields=id,name,approximate_count&limit=300&access_token=" . $this->access_token;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $response = curl_exec($ch);
            curl_close($ch);
            $data->customaudiences = json_decode($response);
        };
        $data->userId = $this->user_id;
        $this->getCurrency();
        $data->current_cur_code = $this->session->userdata('cur_code');
        $data->current_cur_currency = $this->session->userdata('cur_currency');
        
        
		/*
		
		$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/171962383153994/videos?fields=source,title,picture&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		
		
		$url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/457190207964542/thumbnails?access_token=" . $this->access_token;
        
                    $ch1 = curl_init($url1);
                    curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, true);
                    
                    $response1 = curl_exec($ch1);
                    curl_close($ch1);
                    $results1 = json_decode($response1);
		echo "<pre>";
		print_r($results1);
		echo "</pre>";			
		
		exit;
		
		*/
		
        #echo "<pre>"; print_r( $data->customaudiences); die();
        loadView('createcampaign', $data);
    }
    
    function getadcopydraft(){
        
        	if(!empty($_POST['campaignId'])){
        	    
					//	$draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $_POST['campaignId']);
						$draft_data = $this->Campaigndarft_model->getadcopydetail($this->user_id, $_POST['campaignId']);
						echo json_encode($draft_data);
						exit;
        	}
    }

    public function create_campaign() {
        try{
            global $data;
            $this->form_validation->set_rules('campaign[compaingname]', 'Campaign Name', 'trim|xss_clean|required|min_length[4]');
            if ($this->form_validation->run() === false) {
                echo "ValidationError@" . validation_errors();
            } else {
                $campaign = $this->input->post('campaign');
                $campaign_obj = new Campaign(null, "act_" . $campaign['adaccountid']);
                $campaign_obj->setData(array(
                    CampaignFields::NAME => $campaign['compaingname'],
                    CampaignFields::OBJECTIVE => $campaign['campaignobjective'],
                    CampaignFields::EFFECTIVE_STATUS => Campaign::STATUS_ACTIVE,
                ));
                //Create Facebook Campaign----------------
                $campaign_obj->validate()->create();
                //----------------------------------------
                $campaign['fbcampaignid'] = $campaign_obj->id;
                $campaign['campaignobjective'] = 'PAGE_LIKES';
                $campaign['campaignstatus'] = 'ACTIVE';
                $campaign['user_id'] = $this->user_id;
                $this->Campaign_model->SaveCampaign($campaign);
                echo "Successfully". $campaign_obj->id;
                echo "Successfully";
            }
        }
        catch(Exception $e) {
          echo 'Message: ' .$e->getMessage();
          exit;
        }
    }

    public function validate_ad_design() {
        #echo "<PRE>";print_R($_POST);exit;
        $this->cs_validation();
        echo "successfully";
        die();
    }

    private function cs_validation() {
        $this->form_validation->set_rules('creatives[object_id]', 'Page', 'trim|xss_clean|required|min_length[4]');
        if ($this->input->post('campaignobjective') == 'LINK_CLICKS' || $this->input->post('campaignobjective') == 'CONVERSIONS') {
            $this->form_validation->set_rules('creatives[viewUrl][0]', 'Image', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('creatives[link_url][0]', 'URL', 'trim|xss_clean|required|min_length[4]');
        } 
        elseif ($this->input->post('campaignobjective') == 'PAGE_LIKES') {
            $this->form_validation->set_rules('creatives[viewUrl][0]', 'Image', 'trim|xss_clean|required|min_length[4]');
            //$this->form_validation->set_rules('creatives[link_url][0]', 'URL', 'trim|xss_clean|required|min_length[4]', array('required' => 'Page Tabe must be choose'));
            //$this->form_validation->set_message('creatives[link_url][0]', 'The ff is already taken');
        } 
        elseif ($this->input->post('campaignobjective') == 'POST_ENGAGEMENT') {
            $this->form_validation->set_rules('creatives[story_id]', 'Post', 'trim|xss_clean|required|min_length[4]');
        } 
        elseif ($this->input->post('campaignobjective') == 'CANVAS_APP_INSTALLS') {
            $this->form_validation->set_rules('creatives[app_id]', 'App', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('creatives[viewUrl][0]', 'Image', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('creatives[call_to_action3][0]', 'Call to action', 'trim|xss_clean|required|min_length[4]');
        } 
        elseif ($this->input->post('campaignobjective') == 'POST_ENGAGEMENT') {
            $this->form_validation->set_rules('creatives[story_id]', 'Post', 'trim|xss_clean|required|min_length[4]');
        } 
        elseif ($this->input->post('campaignobjective') == 'PRODUCT_CATALOG_SALES') {
            $this->form_validation->set_rules('products_link', 'URL', 'trim|xss_clean|required|min_length[4]');
            //$this->form_validation->set_rules('products_link_cap', 'Link Caption', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('url_pro1', 'Product 1 URL', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('url_pro2', 'Product 2 URL', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('url_pro3', 'Product 3 URL', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('product1_img_url', 'Product 1 Image', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('product2_img_url', 'Product 2 Image', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('product3_img_url', 'Product 3 Image', 'trim|xss_clean|required|min_length[4]');
        } 
        elseif ($this->input->post('campaignobjective') == 'OFFER_CLAIMS') {
            $this->form_validation->set_rules('offer_Id', 'Offer Id', 'trim|xss_clean|required|min_length[4]');
        } 
        elseif ($this->input->post('campaignobjective') == 'LEAD_GENERATION') {
          
            $this->form_validation->set_rules('creatives[viewUrl][0]', 'Image', 'trim|xss_clean|required|min_length[4]');
           
           // $this->form_validation->set_rules('creatives[lead_gen_form][0]', 'Lead Form', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('creatives[call_to_action3][0]', 'Call to action', 'trim|xss_clean|required|min_length[4]');
            //$this->form_validation->set_rules('creatives[call_to_action]', 'Call to action', 'trim|xss_clean|required|min_length[4]');
             
            
        }
      
        if ($this->form_validation->run() === false) {
            echo "ValidationError@" . validation_errors();
            die();
        }
    }

    public function pagetab_check($str) {
        if ($str == '') {
            $this->form_validation->set_message('creatives[link_url][0]', 'The s field can not be the word');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function ad_design_intoDb($campaign_id) {
        global $data;
        //$this->cs_validation();
        $creatives = $this->input->post('creatives');
        $adObjective_id = $this->AddObjective_model->get_objective_id_byname($this->input->post('campaignobjective'));

        $data = array(
            'pageId' => isset($creatives['object_id'][0]) ? $creatives['object_id'][0] : "",
            'appId' => isset($creatives['app_id'][0]) ? $creatives['app_id'][0] : "",
            'campaign_id' => $campaign_id,
            'postid' => isset($creatives['story_id']) ? $creatives['story_id'] : "",
            'objective_id' => $adObjective_id,
            'ad_accoundtId' => isset($creatives['adaccountid'][0]) ? $creatives['adaccountid'][0] : "",
            'headlines' => isset($creatives['title'][0]) ? $creatives['title'][0] : "",
            'adbody' => isset($creatives['body'][0]) ? $creatives['body'][0] : "",
            'adimage' => isset($creatives['viewUrl'][0]) ? basename($creatives['viewUrl'][0]) : "",
            'image_2' => isset($creatives['viewUrl'][1]) ? basename($creatives['viewUrl'][1]) : "",
            'image_3' => isset($creatives['viewUrl'][2]) ? basename($creatives['viewUrl'][2]) : "",
            'image_4' => isset($creatives['viewUrl'][3]) ? basename($creatives['viewUrl'][3]) : "",
            'image_5' => isset($creatives['viewUrl'][4]) ? basename($creatives['viewUrl'][4]) : "",
            'adurl' => isset($creatives['link_url'][0]) ? $creatives['link_url'][0] : "",
            'linkdescription' => isset($creatives['link_description'][0]) ? $creatives['link_description'][0] : "",
            'link_caption' => isset($creatives['link_caption'][0]) ? $creatives['link_caption'][0] : "",
            'calltoaction' => isset($creatives['call_to_action'][0]) ? $creatives['call_to_action3'][0] : "",
        );
        $data['uid'] = $this->user_id;
        $id = $this->Addesign_model->ads_design_insert($data);
        // echo "The draft has been  saved successfully.";
    }

    public function SaveAudience_intoDb($campaign_id) {
        $data = $this->input->post();
        if (isset($data['page_types'])) {
            $page_types = implode(', ', $data['page_types']);
        } else {
            $page_types = "";
        }
        if (isset($data['relationship_statuses'])) {
            $relationship_statuses = implode(', ', $data['relationship_statuses']);
        } else {
            $relationship_statuses = NULL;
        }
        if (isset($data['mobile_device_user'])) {
            $mobile_device_user = implode(', ', $data['mobile_device_user']);
        } else {
            $mobile_device_user = NULL;
        }
        //die();
        $data = array(
            'uid' => $this->session->userdata['logged_in']['id'],
            'campaign_id' => $campaign_id,
            'include_location' => isset($data['locations_autocomplete_include']) ? $data['locations_autocomplete_include'] : "",
            'exclude_location' => isset($data['locations_autocomplete_exclude']) ? $data['locations_autocomplete_exclude'] : "",
            'language' => isset($data['locales_autocomplete']) ? $data['locales_autocomplete'] : "",
            'gender' => isset($data['genders']) ? $data['genders'] : "All",
            'agegroup_from' => isset($data['form']) ? $data['form'] : "",
            'agegroup_to' => isset($data['to']) ? $data['to'] : "",
            'interest' => isset($data['interests1']) ? $data['interests1'] : "",
            'placement' => $page_types,
            'realationship' => $relationship_statuses,
            'education' => isset($data['edu_autocomplete']) ? $data['edu_autocomplete'] : "",
            'mobile_deveice_user' => $mobile_device_user,
        );

        $id = $this->Audience_model->audience_insert($data);
        // echo "The draft has been  saved successfully.";
    }

    public function SaveuserBudget_intoDb($adSetId, $data) {
        $adset_start_date = date('Y-m-d');
        if ($this->input->post('asap') != 'on') {
            $adset_start_date = isset($data['adset_start_date']) ? $data['adset_start_date'] : "";
        }
        $adset_end_date = isset($data['adset_end_date']) ? $data['adset_end_date'] : "";
        $campaign_id = $data['campaign_id'];
        $adset_start_date = date('Y-m-d', strtotime($adset_start_date));
        $adset_end_date = !empty($adset_end_date) ? date('Y-m-d', strtotime($adset_end_date)) : '';
        $data = array(
            'uid' => $this->session->userdata['logged_in']['id'],
            'adsetId' => $adSetId,
            'campaign_id' => $campaign_id,
            'budget_type' => isset($data['budget_type']) ? $data['budget_type'] : "",
            'budgetamount' => isset($data['budgetAmount']) ? $data['budgetAmount'] : "",
            'bidamount' => isset($data['bid_amount']) ? $data['bid_amount'] : "Auto",
            'start_date' => $adset_start_date,
            'end_date' => $adset_end_date,
            'optimization_goal' => isset($data['optimization_goal']) ? $data['optimization_goal'] : "",
            'billing_event' => isset($data['billing_event']) ? $data['billing_event'] : "",
        );
        return $this->Userbudget_model->saveuserbudget($data);
        // echo $id . "@" . "The draft has been  saved successfully.";
    }

    public function create_creativeads() {
        global $data;
        $this->cs_validation();
        $creatives = $this->input->post('creatives');
        $creatives['object_id'][0];
        $creatives['title'][0];
        $creatives['body'][0];
        $creatives['link_url'][0];
        $creatives['link_description'][0];
        $creatives['link_caption'][0];
        $creatives['call_to_action3'][0];
        echo "Successfully";
    }

    public function getAdpreivewAv() {
        global $data;
       
        $this->cs_validation();
      
        $data_array = $this->input->post();
        
       
        
       // $viewUrl = $data_array['creatives']['viewUrl'][0];
        
        //  if($data_array['imgType'] != 'video'){
        //   $data_array['creatives']['viewUrl'][0] = str_replace("https://scontent.xx.fbcdn.net/","https://scontent.ftpa1-2.fna.fbcdn.net/v/",$viewUrl);
        //  }
         
          if($data_array['imgType'] != 'video'){
              
              foreach($data_array['creatives']['viewUrl'] as $tkey =>  $tempvurl){
              $data_array['creatives']['viewUrl'][$tkey] = str_replace("https://scontent.xx.fbcdn.net/","https://scontent.ftpa1-2.fna.fbcdn.net/v/",$tempvurl);
              }
         }
         
       
         
        if ($this->input->post('campaignobjective') == 'LINK_CLICKS' || $this->input->post('campaignobjective') == 'CONVERSIONS') {
            $this->getWebsiteClickPreview($data_array, "DESKTOP_FEED_STANDARD");
        } 
        elseif ($this->input->post('campaignobjective') == 'PAGE_LIKES') {
            
            $this->getPageLikePreview($data_array, "DESKTOP_FEED_STANDARD");
        } 
        elseif ($this->input->post('campaignobjective') == 'POST_ENGAGEMENT') {
            $this->getPostPreview($data_array, "DESKTOP_FEED_STANDARD");
        } 
        elseif ($this->input->post('campaignobjective') == 'CANVAS_APP_INSTALLS') {
            $this->getAppLikePreview($data_array, "DESKTOP_FEED_STANDARD");
        } 
        elseif ($this->input->post('campaignobjective') == 'PRODUCT_CATALOG_SALES') {
            
            $creatives = $this->input->post('creatives');
            $this->setAd_accountt($creatives['adaccountid'][0]);
            $this->setCreative_pageId($creatives['object_id'][0]);
            $creative_ID = $this->create_multi_product($data_array, 1);
            $this->session->set_userdata('creative_ID', $creative_ID);
            $this->getMultiProductPreview($creative_ID, "DESKTOP_FEED_STANDARD");
        } 
        elseif ($this->input->post('campaignobjective') == 'OFFER_CLAIMS') {
            $this->getOffertPreview($data_array, "DESKTOP_FEED_STANDARD");
        } 
        elseif ($this->input->post('campaignobjective') == 'LEAD_GENERATION') {
            $this->getLeadPreview($data_array, "DESKTOP_FEED_STANDARD");
        }
        
    }

    public function getAdMobilepreivewAv() {
        global $data;
        $this->cs_validation();
        $data_array = $this->input->post();
        if ($this->input->post('campaignobjective') == 'LINK_CLICKS' || $this->input->post('campaignobjective') == 'CONVERSIONS') {
            $this->getWebsiteClickPreview($data_array, "MOBILE_FEED_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'PAGE_LIKES') {
            $this->getPageLikePreview($data_array, "MOBILE_FEED_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'POST_ENGAGEMENT') {
            $this->getPostPreview($data_array, "MOBILE_FEED_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'CANVAS_APP_INSTALLS') {
            //$this->getAppLikePreview($data_array, "MOBILE_FEED_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'PRODUCT_CATALOG_SALES') {
            $creatives = $this->input->post('creatives');
            $this->setAd_accountt($creatives['adaccountid'][0]);
            $this->setCreative_pageId($creatives['object_id'][0]);
            $creative_ID = $this->session->userdata('creative_ID');
            $this->getMultiProductPreview($creative_ID, "MOBILE_FEED_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'OFFER_CLAIMS') {
            $this->getOffertPreview($data_array, "MOBILE_FEED_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'LEAD_GENERATION') {
            $this->getLeadPreview($data_array, "MOBILE_FEED_STANDARD");
        }
    }

    public function getAdRightColpreivewAv() {
        global $data;
        $this->cs_validation();

         $data_array = $this->input->post();
        
        if ($this->input->post('campaignobjective') == 'LINK_CLICKS' || $this->input->post('campaignobjective') == 'CONVERSIONS') {
            $this->getWebsiteClickPreview($data_array, "RIGHT_COLUMN_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'PAGE_LIKES') {
            $this->getPageLikePreview($data_array, "RIGHT_COLUMN_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'POST_ENGAGEMENT') {
            $this->getPostPreview($data_array, "RIGHT_COLUMN_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'CANVAS_APP_INSTALLS') {
            $this->getAppLikePreview($data_array, "RIGHT_COLUMN_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'PRODUCT_CATALOG_SALES') {
            $creatives = $this->input->post('creatives');
            $this->setAd_accountt($creatives['adaccountid'][0]);
            $this->setCreative_pageId($creatives['object_id'][0]);
            $creative_ID = $creative_ID = $this->session->userdata('creative_ID');
            $this->getMultiProductPreview($creative_ID, "RIGHT_COLUMN_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'OFFER_CLAIMS') {
            $this->getOffertPreview($data_array, "RIGHT_COLUMN_STANDARD");
        } elseif ($this->input->post('campaignobjective') == 'LEAD_GENERATION') {
            $this->getLeadPreview($data_array, "RIGHT_COLUMN_STANDARD");
        }
    }

    function videoThumbImg($viewUrl, $adaccountid){
        $tempArr = explode('/', $viewUrl);
        $tempArr1 = explode('?', $tempArr[5]);
        
        $filename = $tempArr1[0];//rand().'.jpg';

        $filenameOut = FCPATH.'uploads/campaign/temp/' . $filename;

        $contentOrFalseOnFailure   = file_get_contents($viewUrl);
        $byteCountOrFalseOnFailure = file_put_contents($filenameOut, $contentOrFalseOnFailure);

        $image = new AdImage(null, "act_".$adaccountid);
        $image->{AdImageFields::FILENAME} = $filenameOut;
        $image->create();
        return $image->hash;
    }

    public function getWebsiteClickPreview($data = NULL, $type = NULL) {
        
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		echo $url;
		exit;*/
		
        $finalArr = $this->combinations(
                    array(
                        $data['creatives']['link_url'], 
                        $data['creatives']['title'],
                        $data['creatives']['body'],
                        $data['creatives']['link_description'],
                        $data['creatives']['viewUrl'],
                    )
                );
        
		
        $videoId = '';
        $adsCounter = $data['adsCounter'];
        $link_caption = $data['creatives']['link_caption'][0];
        
        /*
        $link_url = $data['creatives']['link_url'][0];
        $title = $data['creatives']['title'][0];
        $body = $data['creatives']['body'][0];
        $link_description = $data['creatives']['link_description'][0];
        $viewUrl = $data['creatives']['viewUrl'][0];
        */
        
         $link_url = $finalArr[$adsCounter][0];
        $title = $finalArr[$adsCounter][1];
        $body = $finalArr[$adsCounter][2];
        $link_description = $finalArr[$adsCounter][3];
        $viewUrl = $finalArr[$adsCounter][4];
        
        
       # $viewUrl = 'https://scontent.ftpa1-2.fna.fbcdn.net/v/t45.1600-4/26757406_23842713024860105_1232479339904237568_n.png?_nc_cat=0&oh=e2dce4f4f257259a569bdd25abb7f209&oe=5BCA6902';
        
         #$viewUrl = str_replace("https://scontent.xx.fbcdn.net/","https://scontent.ftpa1-2.fna.fbcdn.net/v/",$viewUrl);
       // exit;
      
         # $viewUrl = 'https://scontent.ftpa1-2.fna.fbcdn.net/v/t45.1600-4/30789695_23842786500540604_6884353123993780224_n.png?_nc_cat=0&oh=964da16b716e1a37303218b367e68b2b&oe=5BCB2A04';
        /*$link_url = $finalArr[$adsCounter][0];
        $title = $finalArr[$adsCounter][1];
        $body = $finalArr[$adsCounter][2];
        $link_description = $finalArr[$adsCounter][3];
        $viewUrl = $finalArr[$adsCounter][4];*/
        
        if(empty($adsCounter)){ $adsCounter=0; }
        if($data['imgType'] == 'video'){
            #$viewUrl = $data['creatives']['viewUrl'][0];
            $videoId = $data['fbimghash'][$adsCounter];
            #$videoId = $this->videoUploadOnFb($filePath, $data['adaccountid_2']);

            //$image_hash = '89bf5ddb807eb464f8a2e83508fe8709';
            $image_hash = $viewUrl;
        }
//"https://scontent.xx.fbcdn.net/v/t15.0-10/18192009_290174154771227_8319850422609117184_n.jpg?oh=b9b893964740e77c2144322c2b117143&oe=59807509";print_r($viewUrl);
//exit;
     
        if(!empty($videoId)){            
            $fileds1 = 'creative={';
                    $fileds .= '"object_story_spec":{'
                    . '"video_data":{';
           /* $fileds .= '"call_to_action":{'
                    . '"type":"' . $data['creatives']['call_to_action'][0] . '",'
                    . '"value":{'
                    . '"link_caption":"' . $data['creatives']['link_caption'][0] . '",'
                    . '"link":"' . $data['creatives']['link_url'][0] . '"'
                    . '}'
                    . '},';
        
            $fileds .='"description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $link_description))) . '",'
                . '"title":"' . $title . '",'
				. '"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body))) . '",'
                . '"image_hash": "'.$image_hash.'",'
                . '"video_id": "' . $videoId . '"'
                . '},"page_id": "' . $data['creatives']['object_id'][0] . '",'
                . '}';*/
				$fileds .= '"call_to_action":{'
                    . '"type":"' . $data['creatives']['call_to_action3'][0] . '",'
                    . '"value":{'
                    . '"link_caption":"' . $data['creatives']['link_caption'][0] . '",'
                    . '"link":"' . $data['creatives']['link_url'][0] . '"'
                    . '}'
                    . '},';
        
            $fileds .='"link_description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $link_description))) . '",'
                . '"title":"' . $title . '",'
				. '"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body))) . '",'
                . '"image_url": "'.$image_hash.'",'
                . '"video_id": "' . $videoId . '"'
                . '},"page_id": "' . $data['creatives']['object_id'][0] . '",'
                . '}';
                $fileds2 = '}';
                
               
        }
        else{
            $fileds1 = 'creative={';
                $fileds .= '"object_story_spec":{'
                    . '"link_data":{';
            if ($data['creatives']['call_to_action3'][0] != "") {
                $fileds .= '"call_to_action":{'
                        . '"type":"' . $data['creatives']['call_to_action3'][0] . '",'
                        . '"value":{'
                        . '"link_caption":"' . $data['creatives']['link_caption'][0] . '",'
                        . '"link":"' . $data['creatives']['link_url'][0] . '"'
                        . '}'
                        . '},';
            }
        
            $fileds .='"caption":"' . $link_caption . '",'
                . '"link":"' . $link_url . '",'
                . '"name":"' . $title . '",'
                . '"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body))) . '",'
                . '"description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $link_description))) . '",'
                . '"picture": "' . $viewUrl . '" '
                . '},"page_id": "' . $data['creatives']['object_id'][0] . '",'
                . '}';
              $fileds2 = '}';
        }
        
     	$fileds = $fileds1 . urlencode($fileds) . $fileds2;
	

		//$fileds = $fileds1 . $fileds . $fileds2;
		//print_r($fileds);
		//exit;
        #echo "<pre>";print_r(json_decode($fileds));echo "</pre>";exit;
        
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $data['creatives']['adaccountid'][0] . "/generatepreviews?width=600&height=500&ad_format=" . $type . "&" . $fileds . "&access_token=" . $this->access_token;
        $url = str_replace(" ", "%20", $url);
		
	    #echo $url; exit;
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        #echo curl_error($ch);
        curl_close($ch);
        #echo "<PRE>";print_r($result);exit;
        echo $this->iframeGenerate($result, $type);
    }

    public function getPageLikePreview($data = NULL, $type = NULL) {
      
        $finalArr = $this->combinations(
                    array(
                        $data['creatives']['body'],
                        $data['creatives']['viewUrl'],
                    )
                );
        
        $videoId = '';
        $adsCounter = $data['adsCounter'];
        
          $body = $finalArr[$adsCounter][0];
         $viewUrl = $finalArr[$adsCounter][1];
        
        /*
        $body = $data['creatives']['body'][0];
        $viewUrl = $data['creatives']['viewUrl'][0];
        */
        $linkurl =  'https://www.facebook.com/'.$data['creatives']['object_id'][0].'/';
        
        
        if($data['imgType'] == 'video'){
            $videoId = $data['fbimghash'][$adsCounter];
            $image_hash = $viewUrl;
        }

     

        if(!empty($videoId)){      
			$fileds1 = 'creative={';
                    $fileds .= '"object_story_spec":{'      
            
                . '"video_data": {';
				
				  $fileds .= '"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body))) . '",'
                . '"image_url":"' . $image_hash . '",'
                . '"video_id": "' . $videoId . '",'
                . '},"page_id": "' . $data['creatives']['object_id'][0] . '",'
				 . '}';
              $fileds2 = '}';
        }
        else{
           $fileds1 = 'creative={';
                    $fileds .= '"object_story_spec":{' 
                . '"link_data": {';
            $fileds .= '"message": "' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body))) . '",'
                . '"link":"' . $linkurl . '",'
                . '"picture":"' . $viewUrl . '"},'
                . '"page_id": "' . $data['creatives']['object_id'][0] . '"}';
                $fileds2 =  '}';
        }
        
        
		$fileds = $fileds1 . urlencode($fileds) . $fileds2;
		
        
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $data['creatives']['adaccountid'][0] . "/generatepreviews?ad_format=" . $type . "&" . $fileds . "&access_token=" . $this->access_token;
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
       
        echo $this->iframeGenerate($result, $type);
        
        
    }

    public function getLeadPreview($data = NULL, $type = NULL) {
        
       
        $finalArr = $this->combinations(
                    array(
                        $data['creatives']['body'],
                        $data['creatives']['link_description'],
                        $data['creatives']['title'],
                        $data['creatives']['viewUrl'],
                    )
                );
        
     
        $adsCounter = $data['adsCounter'];
          
        $body = $finalArr[$adsCounter][0];
        $link_description = $finalArr[$adsCounter][1];
        $title = $finalArr[$adsCounter][2];
        $viewUrl = $finalArr[$adsCounter][3];
        
        
        $videoId = '';
        $adsCounter = $data['adsCounter'];
       
       /*
        $title = $data['creatives']['title'][0];
        $body = $data['creatives']['body'][0];
        $link_description = $data['creatives']['link_description'][0];
        $viewUrl = $data['creatives']['viewUrl'][0];
        */
        
        $lead_gen_formVal = $data['creatives']['lead_gen_form'][0];
        $lead_gen_formVal = explode("-$-", $lead_gen_formVal);
        
        // echo '<pre>';
        // print_r($lead_gen_formVal);
        // exit;
        
        
        $link = $lead_gen_formVal[0];
        $formId = $lead_gen_formVal[1];
        
        
        

        if($data['imgType'] == 'video'){
            $videoId = $data['fbimghash'][$adsCounter];
            $image_hash = $viewUrl;
        }

        if(!empty($videoId)){
            
            $fileds1 = 'creative={';
                    $fileds .= '"object_story_spec":{' 
                    . '"video_data":{';
			
					
            $fileds .= '"call_to_action":{'
                    . '"type":"' . $data['creatives']['call_to_action3'][0] . '",'
                    . '"value":{'
                    . '"lead_gen_form_id":"' . $formId . '"'
                    . '}'
                    . '},';
            
            $fileds .='"link_description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $link_description))) . '",'
				. '"title":"' . $title . '",'
				. '"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body))) . '",'
                . '"image_url": "' . $image_hash . '" ,'
                . '"video_id": "' . $videoId . '" '
                . '},"page_id": "' . $data['creatives']['object_id'][0] . '",'
                 . '}';
              $fileds2 = '}';
        }
        else{
            //echo $link;
            $fileds1 = 'creative={';
                    $fileds .= '"object_story_spec":{'
                    . '"link_data":{'
                    . '"call_to_action":{'
                    . '"type":"' . $data['creatives']['call_to_action3'][0] . '",'
                    . '"value":{"lead_gen_form_id":"' . $formId . '",'
                    . '}'
                    . '},'
                    . '"link": "' . $link . '",'
                    . '"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body))) . '",'
                    . '"description": "' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $link_description))) . '",'
                    . '"name": "' . $title . '",'
                    . '"picture": "' . $viewUrl . '"},'
                    . '"page_id": "' . $data['creatives']['object_id'][0] . '"'
                     . '}';
              $fileds2 = '}';
        }
        
       
		$fileds = $fileds1 . urlencode($fileds) . $fileds2;
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $data['creatives']['adaccountid'][0] . "/generatepreviews?ad_format=" . $type . "&" . $fileds . "&access_token=" . $this->access_token;
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);

        echo $this->iframeGenerate($result, $type);
    }

    public function getAppLikePreview($data = NULL, $type = NULL) {
         $fileds1 = 'creative={';
                    $fileds .= '"object_story_spec":{'
                . '"link_data":{'
                . '"call_to_action":{'
                . '"type":"' . $data['creatives']['call_to_action2'][0] . '",'
                . '"value":{"link":"https://apps.facebook.com/' . $data['creatives']['app_id'][0] . '/",'
                . '"link_caption":"' . $data['creatives']['link_title'][0] . '"'
                . '}'
                . '},'
                . '"link": "https://apps.facebook.com/' . $data['creatives']['app_id'][0] . '/",'
                . '"message": "' . $data['creatives']['body'][0] . '",'
                // . '"name": "' . $data['creatives']['link_title'][0] . '",'
                . '"picture": "' . $data['creatives']['viewUrl'][0] . '"},'
                . '"page_id": "' . $data['creatives']['object_id'][0] . '"'
                 . '}';
              $fileds2 = '}';
				$fileds = $fileds1 . urlencode($fileds) . $fileds2;
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $data['creatives']['adaccountid'][0] . "/generatepreviews?ad_format=" . $type . "&" . $fileds . "&access_token=" . $this->access_token;
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        echo $this->iframeGenerate($result, $type);
    }

    public function getPostPreview($data = NULL, $type = NULL) {
        
        $fileds = 'creative={"object_story_id":"' . $data['creatives']['story_id'] . '"}&ad_format=' . $type;
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $data['creatives']['adaccountid'][0] . "/generatepreviews?" . $fileds . "&access_token=" . $this->access_token;
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);

        echo $this->iframeGenerate($result, $type);
    }

    public function getOffertPreview($data = NULL, $type = NULL) {
        $fileds = 'creative={"object_story_id":"' . $data['offer_Id'] . '"}&ad_format=' . $type;
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $data['creatives']['adaccountid'][0] . "/generatepreviews?" . $fileds . "&access_token=" . $this->access_token;
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);

        echo $this->iframeGenerate($result, $type);
    }

    public function getMultiProductPreview($id, $type = NULL) {
        $fileds = $id . "/previews?ad_format=" . $type;
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/" . $fileds . "&access_token=" . $this->access_token;
        $url = str_replace(" ", "%20", $url);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        
        echo $this->iframeGenerate($result, $type);
    }

    public function iframeGenerate($dataResult, $type = NULL) {

        $result = json_decode($dataResult);
        

        if (isset($result->error)) {
            $error = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
            return $error;
        } else {
            foreach ($result->data as $getAdPreview) {
                $body = $getAdPreview->body;
            }
         
            preg_match('/src="([^"]+)"/', $body, $match);
            $url = $match[1];
            
            if($type == 'MOBILE_FEED_STANDARD'){
                return '<iframe src="' . $url . '" scrolling="no" frameBorder="0" 
                                            style="width:323px !important; height: 560px;min-height:500px;margin:0 auto;" align="middle"></iframe>';
            }
            else{
                //return '<iframe src="' . $url . '" scrolling="no" frameBorder="0" 
                                            //style="width:514px !important; height: 560px;min-height:500px;margin:0 auto;" align="middle"></iframe>';
                return '<iframe src="' . $url . '" scrolling="no" frameBorder="0" 
                                            style="width:650px !important; height: auto;min-height:500px;margin:0 auto;" align="middle"></iframe>';
            }
        }
    }

    public function last_proceed() {

        $data_array = $this->input->post();
        
        $creatives = $this->input->post('creatives');
        //Check package limit
        $this->load->library("availability_limit");
        
        //if ($this->availability_limit->dailyAvailabilityLimit($data_array)) {

            //if ($this->availability_limit->monthlyAvailabilityLimit($data_array)) {

                $this->setAd_accountt($creatives['adaccountid'][0]);
                $this->setCreative_pageId($creatives['object_id'][0]);
                if ($this->input->post('campaignobjective') == 'LINK_CLICKS' || $this->input->post('campaignobjective') == 'CONVERSIONS') {
                    $creative_ID = $this->websiteClicks_creative_ad();
                    //echo json_encode($creative_ID);exit;
                } 
                elseif ($this->input->post('campaignobjective') == 'PAGE_LIKES') {
                    $creative_ID = $this->pageLike_creative_ad($data_array);
                } 
                elseif ($this->input->post('campaignobjective') == 'POST_ENGAGEMENT') {
                    $creative_ID = $this->postEng_creative_ad($data_array);
                } 
                elseif ($this->input->post('campaignobjective') == 'PRODUCT_CATALOG_SALES') {

                    $creative_ID = $this->create_multi_product($data_array);
                    //}
                } 
                elseif ($this->input->post('campaignobjective') == 'CANVAS_APP_INSTALLS') {
                    $creative_ID = $this->canvasapp_creative_ad($data_array);
                } 
                elseif ($this->input->post('campaignobjective') == 'OFFER_CLAIMS') {
                    $creative_ID = $this->offer_creative_ad($data_array);
                } 
                elseif ($this->input->post('campaignobjective') == 'LEAD_GENERATION') {
                    $creative_ID = $this->leadfrm_creative_ad($data_array);
                }

                $creative_ID = json_decode($creative_ID);
                
               
                
                #echo "<PRE>";print_r($creative_ID);exit;
                
                $this->writeLog("creative_ID", $creative_ID);

                //Check if create Id generated or not
                if (is_array($creative_ID) && count($creative_ID) > 0) {
                    foreach ($creative_ID as $creative) {

                        if ($creative->status == 'success') {
                            $res_msg = array("status" => 1, "message" => $creative->message);
                        } else {
                            $res_msg = array("status" => 0, "message" => "ERROR 1000: " . $creative->message);
                        }
                        break;
                    }
                } else {
                    if ($creative_ID->status == 'success') {
                        $res_msg = array("status" => 1, "message" => $creative_ID->message);
                    } else {
                        $res_msg = array("status" => 0, "message" => "ERROR 1001: " . $creative_ID->message);
                    }
                }

                if ($res_msg['status'] == 1) { // If create id done
                
                    $this->setCompaingname_1($this->input->post('compaingname_1'));
                    $this->setCampaignobjective($this->input->post('campaignobjective'));
                    $this->setBudget_type($this->input->post('budget_type'));
                    $this->setBudgetAmount($this->input->post('budgetAmount'));
                    $cmpResponse = json_decode($this->create_cmp());
                    if ($cmpResponse->status == 'success') {
                        $cmpID = $cmpResponse->message;
                        $this->setCmp_ID($cmpID);
                        $data_array['Cmp_id'] = $cmpID;
                        $createadsets = json_decode($this->create_adset_new($data_array));
                        #echo "<PRE>";print_r($createadsets);exit;
						
                        $this->writeLog("createadsets", $createadsets);

                        if (is_array($createadsets) && count($createadsets) > 0) {

                            foreach ($createadsets as $createadset) {

                                if ($createadset->status == 'success') {
                                    $create_adsetID = $createadset->message;
                                    if (is_array($creative_ID) && count($creative_ID) > 0) {
                                        $counter = 0;
                                        $nameCounter = 1;
                                        foreach ($creative_ID as $creative) {
                                            $AD_ID = '';
                                            if ($creative->title) {
                                                $name = substr($creative->title, 0, 20) . " ad " . $nameCounter;
                                            } else {
                                                $name = $this->input->post('compaingname_1') . " ad " . $nameCounter;
                                            }


                                            if ($creative->status == 'success') {
                                                $creativeID = $creative->message;
                                                $responseAdid = $this->save_fb_ad($create_adsetID, $creativeID, $name);
                                                $this->writeLog("AD_ID", $responseAdid);
                                                $AD_ID = json_decode($responseAdid);
                                                $counter++;
                                            } else {
                                                $res_msg = array("status" => 0, "message" => "ERROR 1002: " . $creative->message);
                                                break 2;
                                            }
                                            $nameCounter ++;
                                        }
                                    } else {
                                        if ($creative_ID->status == 'success') {
                                            $creativeID = $creative_ID->message;

                                            if (isset($creatives['title'][0]) && $creatives['title'][0] != NULL) {
                                                $names = substr($creatives['title'], 0, 20);
                                            } else if (isset($data_array['title'][0]) && $data_array['title'][0] != NULL) {
                                                $names = substr($data_array['title'][0], 0, 20);
                                            } else {
                                                $names = '';
                                            }
                                            $responseAdid = $this->save_fb_ad($create_adsetID, $creativeID, $names);
                                            $this->writeLog("AD_ID", $responseAdid);
                                            $AD_ID = json_decode($responseAdid);
                                        } else {
                                            $res_msg = array("status" => 0, "message" => "ERROR 1003: " . $creative_ID->message);
                                            break;
                                        }
                                    }
                                    if ($AD_ID->status == 'success') {
                                        $message = 'Campaign Published Successfully!';
                                        $message .= '<div class="button_success"><a href="' . site_url("reports") . '" class="btn btn-primary" style="margin-right:10px;">Go To Reporting Dashboard</a><a href="' . site_url("createcampaign") . '" class="btn btn-success">Create Another Campaign</a></div>';
                                        $res_msg = array("status" => 1, "message" => $message);
                                    } else if ($AD_ID->status == 'false') {
                                        $res_msg = array("status" => 0, "message" => "ERROR 1004: " . $AD_ID->message);
                                        break;
                                    } else {
                                        $res_msg = array("status" => 0, "message" => "ERROR 1005: " . $AD_ID->message . " " . $creative_ID->message);
                                        break;
                                    }
                                } 
                                else {
                                    $res_msg = array("status" => 0, "message" => "ERROR 1006: " . $createadset->message);
                                    break;
                                }
                            }
                        } else {
                            $res_msg = array("status" => 0, "message" => "An error occurred while creating Adset");
                        }
                    } else {
                        $res_msg = array("status" => 0, "message" => "ERROR 1007: " . $cmpResponse->message);
                    }

                    if ($res_msg['status'] == 0) {
                        if (isset($cmpID) && $cmpID != NULL) {
                            $this->deleteCampaign($cmpID);
                        }
                    } else if ($res_msg['status'] == 1) {
                        //Save User Budget Into Database..............................

                        $data_array['campaign_id'] = $cmpResponse->campaign_id;
                        $this->SaveuserBudget_intoDb($create_adsetID, $data_array);
                        $this->ad_design_intoDb($cmpResponse->campaign_id);
                        $this->SaveAudience_intoDb($cmpResponse->campaign_id);
                    }
                } else {

                    $res_msg = array("status" => 0, "message" => $res_msg['message']);
                }
           /* } else {
                $res_msg = array("status" => 0, "message" => 'Your monthly limit reached. Please Upgrade your Package.');
            }*/
        /*} else {
            $res_msg = array("status" => 0, "message" => 'Your daily limit reached.Please Upgrade your Package.');
        }*/
        $data_array['cmpID'] = $cmpID;
        $data_array['Adset_ID'] = $create_adsetID;
        $data_array['creative_ID'] = $creative_ID;
        $data_array['response_message'] = $res_msg['message'];
        $this->writeLog("GenerlLog", $data_array);
        echo json_encode($res_msg);
        die();
    }

    public function create_offer() {
        $this->form_validation->set_rules('creatives[object_id]', 'Page', 'trim|xss_clean|required|min_length[4]');
        $this->form_validation->set_rules('offerTitle', 'Title', 'trim|xss_clean|required|min_length[4]');
        $this->form_validation->set_rules('descriptionTitle', 'Description', 'trim|xss_clean|required|min_length[4]');
        $this->form_validation->set_rules('expiration_time', 'Expiration time', 'trim|xss_clean|required|min_length[4]');
        $this->form_validation->set_rules('offer_img_url', 'Offer Image', 'trim|xss_clean|required|min_length[4]');
        if ($this->form_validation->run() === false) {
            echo "ValidationError@" . validation_errors();
            die();
        } else {
            $creatives = $this->input->post('creatives');
            $pageId = $creatives['object_id'][0];
            $pageToken = $this->get_publish_page_accesstoken($creatives['object_id'][0]);
            $claim_limit = $this->input->post('custom_limit');
            if (empty($claim_limit)) {
                $claim_limit = NULL;
            }
            $image_url = $this->input->post('offer_img_url');
            $message = $this->input->post('descriptionTitle');
            $title = $this->input->post('offerTitle');
            $expiration_time = $this->input->post('expiration_time');
            $expiration_time = (new \DateTime($expiration_time))->format(\DateTime::ISO8601);
            $offerId = $this->publish_offer($pageId, $pageToken, $image_url, $message, $title, $expiration_time, $claim_limit);
            echo "Successfully@" . $pageId . "_" . $offerId;
        }
    }

    public function publish_offer($pageId, $pageToken, $image_url, $message, $title, $expiration_time, $claim_limit = NULL) {
        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/' . $pageId . '/offers');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'access_token' => $pageToken,
                'claim_limit' => $claim_limit,
                'image_url' => $image_url, //"http://sheensol.com/thecampaign/assets/images/crtr1.png",
                'message' => $message,
                'title' => $title,
                'expiration_time' => $expiration_time,
            )
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response);

        $result = json_decode($response);

        if (isset($result->error)) {
            $error = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
            echo "ValidationError@ " . $error;
            die();
        } else {
            return $result->id;
        }
    }

    public function websiteClicks_creative_ad() {
        $response_result = array();
        $creatives = $this->input->post('creatives');
        $this->setAd_accountt($creatives['adaccountid'][0]);
        $this->setCreative_pageId($creatives['object_id'][0]);

        $title = ($creatives['title'] ? $creatives['title'] : array(''));
        $link_description = ($creatives['link_description'] ? $creatives['link_description'] : array(''));
        $viewUrl = ($this->input->post('fbimghash') ? $this->input->post('fbimghash') : array(''));
        $body = ($creatives['body'] ? $creatives['body'] : array(''));
        $link_url = ($creatives['link_url'] ? $creatives['link_url'] : array(''));
        $viewUrl1 = ($creatives['viewUrl'] ? $creatives['viewUrl'] : array(''));

        $this->writeLog("websiteClicks_creative_ad_data", $creatives);
        
        $f = 0; // Alwasy should be zero 
        for ($a = 0; $a < count($title); $a++) {
            for ($b = 0; $b < count($link_description); $b++) {
                for ($c = 0; $c < count($viewUrl); $c++) {
                    for ($d = 0; $d < count($body); $d++) {
                        for ($e = 0; $e < count($link_url); $e++) {
                            if ($f <= $this->adsLimit) {
                                //Google track enable
                                if ($this->input->post('track_google') == 'Yes') {
                                    $locationTemp = explode('],', $_POST['locations_autocomplete_include']);
                                    $finalLocationStr = '';
                                    if(!empty($locationTemp)){
                                        for($i=0; $i<=count($locationTemp)-1; $i++){
                                            $tempStr = explode(',', $locationTemp[$i]);
                                            $finalLocationStr .= $tempStr[0].',';
                                        }
                                    }

                                    $locationTemp = explode(',', trim($finalLocationStr, ','));
                                    $locationName = explode(",", trim($finalLocationStr, ','));
                                    $linkUrl = rtrim($link_url[$e], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode($creatives['title'][$a] . " ad " . $f + 1);
                                } else {
                                    $linkUrl = $link_url[$e];
                                }
								
								$instaaccountid = "";
								$pageaccess_token = $this->get_page_accesstoken($creatives["object_id"][0]);
								$chinstapc = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/'.$creatives["object_id"][0].'/instagram_accounts?access_token='.$pageaccess_token.'&fields=id,username');
									curl_setopt($chinstapc, CURLOPT_SSL_VERIFYPEER, false);
									$data_arraypc = array(
										'access_token' => $this->access_token
									);
									curl_setopt_array($chinstapc, array(
										CURLOPT_RETURNTRANSFER => true
									));
							
									$responsepc = json_decode(curl_exec($chinstapc));
									curl_close($chinstapc);
								//return json_encode($responsepc);
								if(isset($responsepc->data[0]->id)){
										$instaaccountid = $responsepc->data[0]->id;
									}
									else{
								
								$chinsta = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/'.$creatives["object_id"][0].'/page_backed_instagram_accounts?access_token='.$pageaccess_token.'&fields=id');
									curl_setopt($chinsta, CURLOPT_SSL_VERIFYPEER, false);
									$data_array = array(
										'access_token' => $pageaccess_token
									);
									curl_setopt_array($chinsta, array(
										CURLOPT_RETURNTRANSFER => true
									));
							
									$response = json_decode(curl_exec($chinsta));
									curl_close($chinsta);
									//print_r($response);
									//print_r($response->data[0]->id);
									if(isset($response->data[0]->id)){
										$instaaccountid = $response->data[0]->id;
									}
									else{
										//echo "junaid";
										$chinsin = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/'.$creatives["object_id"][0].'/page_backed_instagram_accounts');
										curl_setopt($chinsin, CURLOPT_SSL_VERIFYPEER, false);
										$data_array = array(
											'access_token' => $this->access_token
										);
										curl_setopt_array($chinsin, array(
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_POST => true,
											CURLOPT_POSTFIELDS => $data_array
										));
								
										$response = curl_exec($chinsin);
										curl_close($chinsin);
										
										$chinsta2 = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/'.$creatives["object_id"][0].'/page_backed_instagram_accounts?access_token='.$this->access_token.'&fields=id');
										curl_setopt($chinsta2, CURLOPT_SSL_VERIFYPEER, false);
										$data_array = array(
											'access_token' => $this->access_token
										);
										curl_setopt_array($chinsta2, array(
											CURLOPT_RETURNTRANSFER => true
										));
								
										$response = json_decode(curl_exec($chinsta2));
										curl_close($chinsta2);
										if(isset($response->data[0]->id)){
											$instaaccountid = $response->data[0]->id;
										}
									}
									
								}
								
                                $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $creatives['adaccountid'][0] . '/adcreatives');
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                

                                if($_POST['imgType'] == 'video'){
                                    $videoId = $viewUrl[$c];  
                                    $image_hash = $viewUrl1[$c];
									
									if($instaaccountid == ""){
																								
                                    $data_array = array(
                                        'access_token' => $this->access_token,
                                        'name' => $creatives['title'][$a],
                                        'object_story_spec' => '{"video_data":{"call_to_action":{"type":"' . $creatives['call_to_action3'][0] . '","value":{"link":"' . $linkUrl . '","link_caption":"' . $creatives['link_caption'][0] . '"}},"link_description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $creatives['link_description'][$d]))) . '","title":"' . $creatives['title'][$a] . '","message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $creatives['body'][$d]))) . '","image_url":"' . $image_hash . '","video_id":"' . $videoId . '"},"page_id":"' . $creatives['object_id'][0] . '"}'
                                    );
									}
									else{
										$data_array = array(
                                        'access_token' => $this->access_token,
                                        'name' => $creatives['title'][$a],
										"instagram_actor_id" => $instaaccountid,
                                        'object_story_spec' => '{"video_data":{"call_to_action":{"type":"' . $creatives['call_to_action3'][0] . '","value":{"link":"' . $linkUrl . '","link_caption":"' . $creatives['link_caption'][0] . '"}},"link_description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $creatives['link_description'][$d]))) . '","title":"' . $creatives['title'][$a] . '","message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $creatives['body'][$d]))) . '","image_url":"' . $image_hash . '","video_id":"' . $videoId . '"},"page_id":"' . $creatives['object_id'][0] . '"}'
                                    );
									}
                                }
                                else{
                                    $hasedImage1 = $this->getHashedImage($viewUrl[$c]);
									//$hasedImage1 = $this->getHashedImage($viewUrl1[$c]);
									if(empty($hasedImage1))
										$hasedImage1 = $viewUrl[$c];
									
                                    if($instaaccountid == ""){	
                                    $data_array = array(
                                        'access_token' => $this->access_token,
                                        'name' => $creatives['title'][$a],
                                        'object_story_spec' => '{"link_data":{"call_to_action":{"type":"' . $creatives['call_to_action3'][0] . '","value":{"link":"' . $linkUrl . '","link_caption":"' . $creatives['link_caption'][0] . '"}},"name":"' . $creatives['title'][$a] . '","description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $creatives['link_description'][$b]))) . '","link":"' . $linkUrl . '","message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $creatives['body'][$d]))) . '","image_hash":"' . $hasedImage1 . '"},"page_id":"' . $creatives['object_id'][0] . '"}'
                                    );
									
									
									}
									else{
										$data_array = array(
                                        'access_token' => $this->access_token,
                                        'name' => $creatives['title'][$a],
										"instagram_actor_id" => $instaaccountid,
                                        'object_story_spec' => '{"link_data":{"call_to_action":{"type":"' . $creatives['call_to_action3'][0] . '","value":{"link":"' . $linkUrl . '","link_caption":"' . $creatives['link_caption'][0] . '"}},"name":"' . $creatives['title'][$a] . '","description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $creatives['link_description'][$b]))) . '","link":"' . $linkUrl . '","message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $creatives['body'][$d]))) . '","image_hash":"' . $hasedImage1 . '"},"page_id":"' . $creatives['object_id'][0] . '"}'
                                    );
									}
                                }
                                //echo "<PRE>";print_r($data_array);
								//echo "<PRE>";print_r($viewUrl1);
								
								//exit;
                                curl_setopt_array($ch, array(
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_POST => true,
                                    CURLOPT_POSTFIELDS => $data_array
                                ));
                                $response = curl_exec($ch);
                                curl_close($ch);
								
                                $this->writeLog("websiteClicks_creative_ad", $data_array);
                                $result = json_decode($response);

                                if (isset($result->error)) {
                                    $response_result[$f]['status'] = 'false';
                                    $response_result[$f]['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
                                    break 5;
                                } else {
                                    $response_result[$f]['status'] = 'success';
                                    $response_result[$f]['message'] = $result->id;
                                    $response_result[$f]['title'] = $creatives['title'][$a];
                                }
                            } else {
                                break 5;
                            }
                            sleep(3);
                            $f++;
                        }
                    }
                }
            }
        }
		
        return json_encode($response_result);
    }

    public function pageLike_creative_ad($data) {

        $response_result = array();

        $title = ($data['creatives']['title'] ? $data['creatives']['title'] : array(''));
        $viewUrl = ($data['fbimghash'] ? $data['fbimghash'] : array(''));
        $body = ($data['creatives']['body'] ? $data['creatives']['body'] : array(''));
        $viewUrl1 = ($data['creatives']['viewUrl'] ? $data['creatives']['viewUrl'] : array(''));

        $f = 0; // Alwasy should be zero 
        for ($a = 0; $a < count($title); $a++) {
            for ($b = 0; $b < count($viewUrl); $b++) {
                for ($c = 0; $c < count($body); $c++) {
                    if ($f <= $this->adsLimit) {
                        
					
						
                        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->ad_account_id . '/adcreatives');
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        if($_POST['imgType'] == 'video'){
                            $videoId = $viewUrl[$b];  
                            $image_hash = $viewUrl1[$b];
                            /*$data_array = array(
                                'access_token' => $this->access_token,
                                'object_id' => $data['creatives']['object_id'][0],
                                //'image_hash' => $image_hash,
								'image_url' => $image_hash,
                                "video_id" => $videoId,
                                'body' => str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body[$c]))),
                            );*/
							
							$data_array = array(
                                        'access_token' => $this->access_token,
                                        'name' => $title[$a],
                                        'object_story_spec' => '{"video_data":{"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body[$c]))) . '","image_url":"' . $image_hash . '","video_id":"' . $videoId . '"},"page_id":"' . $data['creatives']['object_id'][0] . '"}'
                                    );
							
							
                        }
                        else{
                            $hasedImage1 = $this->getHashedImage($viewUrl[$b]);
                            $data_array = array(
                                'access_token' => $this->access_token,
                                'object_id' => $data['creatives']['object_id'][0],
                                'message' => str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body[$c]))),
                                'image_hash' => $hasedImage1,
                                'name' => $title[$a],
                                'caption' => $title[$a],
                                'body' => str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body[$c]))),
                            );
                        }
						//print_r($data_array);
						//exit;
                        curl_setopt_array($ch, array(
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $data_array
                        ));
                        $response = curl_exec($ch);
                        curl_close($ch);

                        $this->writeLog("pageLike_creative_ad", $data_array);
                        $result = json_decode($response);

                        if (isset($result->error)) {
                            $response_result[$f]['status'] = 'false';
                            $response_result[$f]['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
                            break 3;
                        } else {
                            $response_result[$f]['status'] = 'success';
                            $response_result[$f]['message'] = $result->id;
                            $response_result[$f]['title'] = $title[$a];
                        }
                    } else {
                        break 3;
                    }
                    sleep(3);
                    $f++;
                }
            }
        }

        return json_encode($response_result);
    }

    public function canvasapp_creative_ad($data) {
        $imgHash = $this->generateCreativeImg($this->ad_account_id, $data['creatives']['viewUrl'][0]);
        $link_data = new LinkData();
        $link_data->setData(array(
            LinkDataFields::MESSAGE => $data['creatives']['body'][0],
            LinkDataFields::LINK => 'https://apps.facebook.com/' . $data['creatives']['app_id'][0] . '/',
            LinkDataFields::CAPTION => $data['creatives']['link_title'][0],
            LinkDataFields::IMAGE_HASH => $imgHash,
            LinkDataFields::CALL_TO_ACTION => array(
                'type' => $data['creatives']['call_to_action2'][0],
                'value' => array(
                    'link' => 'https://apps.facebook.com/' . $data['creatives']['app_id'][0] . '/',
                    'link_caption' => $data['creatives']['link_title'][0],
                ),
            ),
        ));

        $object_story_spec = new ObjectStorySpec();
        $object_story_spec->setData(array(
            ObjectStorySpecFields::PAGE_ID => $data['creatives']['object_id'][0],
            ObjectStorySpecFields::LINK_DATA => $link_data,
        ));

        $creative = new AdCreative(null, 'act_' . $this->ad_account_id);

        $creative->setData(array(
            AdCreativeFields::NAME => 'Sample Creative',
            AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
        ));

        $creative->create();

        $this->writeLog("canvasapp_creative_ad", $creative);

        $response_result = array();

        if (isset($creative->id) && $creative->id != NULL) {
            $response_result['status'] = 'success';
            $response_result['message'] = $creative->id;
        } else {
            $response_result['status'] = 'false';
            $response_result['message'] = 'Issue in creating Canvas App Creative';
        }

        return json_encode($response_result);
    }

    public function leadfrm_creative_ad($data) {
        $response_result = array();
        $lead_gen_formVal = $data['creatives']['lead_gen_form'][0];
        $lead_gen_formVal = explode("-$-", $lead_gen_formVal);
        $link = $lead_gen_formVal[0];
        $formId = $lead_gen_formVal[1];

        $title = ($data['creatives']['title'] ? $data['creatives']['title'] : array(''));
        $link_description = ($data['creatives']['link_description'] ? $data['creatives']['link_description'] : array(''));
        $viewUrl = ($data['fbimghash'] ? $data['fbimghash'] : array(''));
        $body = ($data['creatives']['body'] ? $data['creatives']['body'] : array("") );
        $viewUrl1 = ($data['creatives']['viewUrl'] ? $data['creatives']['viewUrl'] : array(''));

        $f = 0; // Alwasy should be zero 
        for ($a = 0; $a < count($title); $a++) {
            for ($b = 0; $b < count($link_description); $b++) {
                for ($c = 0; $c < count($viewUrl); $c++) {
                    for ($d = 0; $d < count($body); $d++) {

                        if ($f <= $this->adsLimit) {

                            $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->ad_account_id . '/adcreatives');
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            
                            if($_POST['imgType'] == 'video'){
                                $videoId = $viewUrl[$c];  
                                $image_hash = $viewUrl1[$c];
                            
							
								$fileds1 = 'creative={';
                    $fileds .= '"object_story_spec":{' 
                    . '"video_data":{';
			
					
            $fileds .= '"call_to_action":{'
                    . '"type":"' . $data['creatives']['call_to_action3'][0] . '",'
                    . '"value":{'
                    . '"lead_gen_form_id":"' . $formId . '"'
                    . '}'
                    . '},';
            
            $fileds .='"link_description":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $link_description))) . '",'
				. '"title":"' . $title . '",'
				. '"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body))) . '",'
                . '"image_url": "' . $image_hash . '" ,'
                . '"video_id": "' . $videoId . '" '
                . '},"page_id": "' . $data['creatives']['object_id'][0] . '",'
                 . '}';
              $fileds2 = '}';
							
							
                                $data_array = array(
                                    'access_token' => $this->access_token,
                                    'name' => $title[$a],
									'object_story_spec' => '{"video_data":{"call_to_action":{"type":"' . $data['creatives']['call_to_action3'][0] . '","value":{"lead_gen_form_id":"' . $formId . '"}},"link_description":"' . $link_description[$b] . '","title":"' . $title[$a] . '","message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body[$d]))) . '","image_url":"' . $image_hash . '","video_id":"' . $videoId . '"},"page_id":"' . $data['creatives']['object_id'][0] . '"}'
                                );
                            }
                            else{
                                $hasedImage1 = $this->getHashedImage($viewUrl[$c]);
                                
                                $data_array = array(
                                    'access_token' => $this->access_token,
                                    'name' => $title[$a],
                                    'object_story_spec' => '{'
                                    . '"link_data":'
                                    . '{'
                                    . '"call_to_action":'
                                    . '{"type":"' . $data['creatives']['call_to_action3'][0] . '",'
                                    . '"value":{"lead_gen_form_id":"' . $formId . '"'                                    
                                    . '}'
                                    . '},'
                                    . '"name":"' . $title[$a] . '",'
                                    . '"link":"' . $link . '",'
									. '"description":"' . $link_description[$b] . '",'
                                    . '"message":"' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $body[$d]))) . '",'
                                    . '"image_hash":"' . $hasedImage1 . '"},'
                                    . '"page_id":"' . $data['creatives']['object_id'][0] . '"'
                                    . '}'
                                );
                            }
							
							
                            curl_setopt_array($ch, array(
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POST => true,
                                CURLOPT_POSTFIELDS => $data_array
                            ));
                            $response = curl_exec($ch);
                            curl_close($ch);

                            $this->writeLog("leadfrm_creative_ad", $data_array);
                            $result = json_decode($response);

                            if (isset($result->error)) {
                                $response_result[$f]['status'] = 'false';
                                $response_result[$f]['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
                                break 4;
                            } else {
                                $response_result[$f]['status'] = 'success';
                                $response_result[$f]['message'] = $result->id;
                                $response_result[$f]['title'] = $title[$a];
                            }
                        } else {
                            break 4;
                        }
                        sleep(3);
                        $f++;
                    }
                }
            }
        }

        return json_encode($response_result);
    }

    public function create_multi_product_($data, $frmPreview = NULL) {
        
        try {
            if ($this->input->post('track_google') == 'Yes') {
                $locationName = explode(",", $this->input->post('locations_autocomplete_include'));
                $linkUrl1 = rtrim($data['url_pro1'], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode($data['headlines_pro1']);
            } else {
                $linkUrl1 = $data['url_pro1'];
            }

            $tempArr = explode('_', $data['fbimghash1']);
            if($tempArr[1] == 'video' || !empty($tempArr[1])){

                $image_hash = $data['product1_img_url'];        
                $videoId = $tempArr[0];

                $product1 = (new AttachmentData())->setData(array(
                    AttachmentDataFields::LINK => $linkUrl1,
                    AttachmentDataFields::NAME => $data['headlines_pro1'],
                    AttachmentDataFields::DESCRIPTION => $data['desc_pro1'],
                    AttachmentDataFields::IMAGE_HASH => $image_hash, // $creative_img_hash1,
                    AttachmentDataFields::VIDEO_ID => $videoId, 
                    AttachmentDataFields::CALL_TO_ACTION => array(
                        'type' => $data['callToAction_pro1'],
                        'value' => array(
                            'link' => $linkUrl1,
                        ),
                    ),
                ));
            }
            else{
                $hasedImage1 = $this->getHashedImage($data['fbimghash1']);

                $product1 = (new AttachmentData())->setData(array(
                    AttachmentDataFields::LINK => $linkUrl1,
                    AttachmentDataFields::NAME => $data['headlines_pro1'],
                    AttachmentDataFields::DESCRIPTION => $data['desc_pro1'],
                    AttachmentDataFields::IMAGE_HASH => $hasedImage1, // $creative_img_hash1,
                    AttachmentDataFields::CALL_TO_ACTION => array(
                        'type' => $data['callToAction_pro1'],
                        'value' => array(
                            'link' => $linkUrl1,
                        ),
                    ),
                ));
            }
            
            if ($this->input->post('track_google') == 'Yes') {
                $locationName = explode(",", $this->input->post('locations_autocomplete_include'));
                $linkUrl2 = rtrim($data['url_pro2'], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode($data['headlines_pro2']);
            } else {
                $linkUrl2 = $data['url_pro2'];
            }

            $hasedImage2 = $this->getHashedImage($data['fbimghash2']);
            $hasedImage3 = $this->getHashedImage($data['fbimghash3']);

            $product2 = (new AttachmentData())->setData(array(
                AttachmentDataFields::LINK => $linkUrl2,
                AttachmentDataFields::NAME => $data['headlines_pro2'],
                AttachmentDataFields::DESCRIPTION => $data['desc_pro2'],
                AttachmentDataFields::IMAGE_HASH => $hasedImage2, //$creative_img_hash2,
                AttachmentDataFields::CALL_TO_ACTION => array(
                    'type' => $data['callToAction_pro2'],
                    'value' => array(
                        'link' => $linkUrl2,
                    ),
                ),
            ));


            if ($this->input->post('track_google') == 'Yes') {
                $locationName = explode(",", $this->input->post('locations_autocomplete_include'));
                $linkUrl3 = rtrim($data['url_pro3'], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode($data['headlines_pro3']);
            } else {
                $linkUrl3 = $data['url_pro3'];
            }

            $product3 = (new AttachmentData())->setData(array(
                AttachmentDataFields::LINK => $linkUrl3,
                AttachmentDataFields::NAME => $data['headlines_pro3'],
                AttachmentDataFields::DESCRIPTION => $data['desc_pro3'],
                AttachmentDataFields::IMAGE_HASH => $hasedImage3, //$creative_img_hash3,
                AttachmentDataFields::CALL_TO_ACTION => array(
                    'type' => $data['callToAction_pro3'],
                    'value' => array(
                        'link' => $linkUrl3,
                    ),
                ),
            ));

            if ($this->input->post('track_google') == 'Yes') {
                $locationName = explode(",", $this->input->post('locations_autocomplete_include'));
                $products_link = rtrim($data['products_link'], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode(trim($data['creatives']['body'][0], 0, 20));
            } else {
                $products_link = $data['products_link'];
            }

            if($tempArr[1] == 'video' || !empty($tempArr[1])){

                $fileds = '"video_data": {';
                $fileds .= '"description": "' . str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $data['creatives']['body'][0]))) . '",'
                    . '"image_hash":"3789896151fdc6b71fb9e338e0ab887a",'
                    . '"video_id": "' . $videoId . '"'
                    . '}';

                $video_data = new VideoData();
                $video_data->setData(array(
                    VideoDataFields::DESCRIPTION => $data['creatives']['body'][0],
                    VideoDataFields::CALL_TO_ACTION => $fileds
                ));

                $object_story_spec1 = new ObjectStorySpec();
                $object_story_spec1->setData(array(
                    ObjectStorySpecFields::PAGE_ID => $this->getCreative_pageId(),
                    ObjectStorySpecFields::VIDEO_DATA => $video_data,
                        //ObjectStorySpecFields::TEXT_DATA => $data['creatives']['body'][0],
                ));
            }
            else{
                $link_data = new LinkData();
                $link_data->setData(array(
                    LinkDataFields::LINK => $products_link,
                    LinkDataFields::MESSAGE => $data['creatives']['body'][0],
                    LinkDataFields::CHILD_ATTACHMENTS => array(
                        $product1, $product2, $product3,
                    ),
                ));

                $object_story_spec = new ObjectStorySpec();
                $object_story_spec->setData(array(
                    ObjectStorySpecFields::PAGE_ID => $this->getCreative_pageId(),
                    ObjectStorySpecFields::LINK_DATA => $link_data,
                ));
            }

            $creative = new AdCreative(null, 'act_' . $this->ad_account_id);
            $creative->setData(array(
                //AdCreativeFields::NAME => $data['creatives']['title'][0],
                AdCreativeFields::BODY => str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $data['creatives']['body'][0]))),
                AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
            ));

            $creative->create();

            $response_result = array();

            if (isset($creative->id) && $creative->id != NULL) {
                $response_result['status'] = 'success';
                $response_result['message'] = $creative->id;
            } else {
                $response_result['status'] = 'false';
                $response_result['message'] = 'Issue in creating Multi Product Creative';
            }
        } 
        catch (Exception $ex) {
            $response_result['status'] = 'false';
            $response_result['message'] = $ex->getMessage();
        }
        #echo "<PRE>";print_R($response_result);exit;
        $this->writeLog("create_multi_product", $response_result);
        if ($frmPreview != NULL) {
            return $creative->id;
        } else {
            return json_encode($response_result);
        }
    }
	

    public function create_multi_product($data, $frmPreview = NULL) {
        #echo "<PRE>";print_r($data);exit;
        try {
            if ($data['track_google'] == 'Yes') {
                $locationName = explode(",", $this->input->post('locations_autocomplete_include'));
                $linkUrl1 = rtrim($data['url_pro1'], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode($data['headlines_pro1']);
            } else {
                $linkUrl1 = $data['url_pro1'];
            }

            #$hasedImage1 = $this->getHashedImage($data['fbimghash1']);
            #$hasedImage2 = $this->getHashedImage($data['fbimghash2']);
            #$hasedImage3 = $this->getHashedImage($data['fbimghash3']);
            
            $tempArr = explode('_', $data['fbimghash1']);
            if($tempArr[1] == 'video' || !empty($tempArr[1])){

                $image_hash = $data['product1_img_url'];        
                $videoId = $tempArr[0];

                $product1 = (new AttachmentData())->setData(array(
                    AttachmentDataFields::LINK => $linkUrl1,
                    AttachmentDataFields::NAME => $data['headlines_pro1'],
                    AttachmentDataFields::DESCRIPTION => $data['desc_pro1'],
                    AttachmentDataFields::IMAGE_HASH => $image_hash, // $creative_img_hash1,
                    AttachmentDataFields::VIDEO_ID => $videoId, 
                    AttachmentDataFields::CALL_TO_ACTION => array(
                        'type' => $data['callToAction_pro1'],
                        'value' => array(
                            'link' => $linkUrl1,
                        ),
                    ),
                ));
            }
            else{
                $hasedImage1 = $this->getHashedImage($data['fbimghash1']);

                $product1 = (new AttachmentData())->setData(array(
                    AttachmentDataFields::LINK => $linkUrl1,
                    AttachmentDataFields::NAME => $data['headlines_pro1'],
                    AttachmentDataFields::DESCRIPTION => $data['desc_pro1'],
                    AttachmentDataFields::IMAGE_HASH => $hasedImage1, // $creative_img_hash1,
                    AttachmentDataFields::CALL_TO_ACTION => array(
                        'type' => $data['callToAction_pro1'],
                        'value' => array(
                            'link' => $linkUrl1,
                        ),
                    ),
                ));
            }

            /*$product1 = (new AttachmentData())->setData(array(
                AttachmentDataFields::LINK => $linkUrl1,
                AttachmentDataFields::NAME => $data['headlines_pro1'],
                AttachmentDataFields::DESCRIPTION => $data['desc_pro1'],
                AttachmentDataFields::IMAGE_HASH => $hasedImage1, // $creative_img_hash1,
                AttachmentDataFields::VIDEO_ID => '261348307653812',
                AttachmentDataFields::CALL_TO_ACTION => array(
                    'type' => $data['callToAction_pro1'],
                    'value' => array(
                        'link' => $linkUrl1,
                    ),
                ),
            ));*/
            
            if ($data['track_google'] == 'Yes') {
                $locationName = explode(",", $this->input->post('locations_autocomplete_include'));
                $linkUrl2 = rtrim($data['url_pro2'], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode($data['headlines_pro2']);
            } else {
                $linkUrl2 = $data['url_pro2'];
            }

            $tempArr2 = explode('_', $data['fbimghash2']);
            if($tempArr2[1] == 'video' || !empty($tempArr2[1])){

                $image_hash2 = $data['product2_img_url'];        
                $videoId2 = $tempArr2[0];

                $product2 = (new AttachmentData())->setData(array(
                    AttachmentDataFields::LINK => $linkUrl2,
                    AttachmentDataFields::NAME => $data['headlines_pro2'],
                    AttachmentDataFields::DESCRIPTION => $data['desc_pro2'],
                    AttachmentDataFields::IMAGE_HASH => $image_hash2, // $creative_img_hash1,
                    AttachmentDataFields::VIDEO_ID => $videoId2, 
                    AttachmentDataFields::CALL_TO_ACTION => array(
                        'type' => $data['callToAction_pro2'],
                        'value' => array(
                            'link' => $linkUrl2,
                        ),
                    ),
                ));
            }
            else{
                $hasedImage2 = $this->getHashedImage($data['fbimghash2']);

                $product2 = (new AttachmentData())->setData(array(
                    AttachmentDataFields::LINK => $linkUrl2,
                    AttachmentDataFields::NAME => $data['headlines_pro2'],
                    AttachmentDataFields::DESCRIPTION => $data['desc_pro2'],
                    AttachmentDataFields::IMAGE_HASH => $hasedImage2, // $creative_img_hash1,
                    AttachmentDataFields::CALL_TO_ACTION => array(
                        'type' => $data['callToAction_pro2'],
                        'value' => array(
                            'link' => $linkUrl2,
                        ),
                    ),
                ));
            }

            /*$product2 = (new AttachmentData())->setData(array(
                AttachmentDataFields::LINK => $linkUrl2,
                AttachmentDataFields::NAME => $data['headlines_pro2'],
                AttachmentDataFields::DESCRIPTION => $data['desc_pro2'],
                AttachmentDataFields::IMAGE_HASH => $hasedImage2, //$creative_img_hash2,
                AttachmentDataFields::CALL_TO_ACTION => array(
                    'type' => $data['callToAction_pro2'],
                    'value' => array(
                        'link' => $linkUrl2,
                    ),
                ),
            ));*/

            if ($data['track_google'] == 'Yes') {
                $locationName = explode(",", $this->input->post('locations_autocomplete_include'));
                $linkUrl3 = rtrim($data['url_pro3'], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode($data['headlines_pro3']);
            } else {
                $linkUrl3 = $data['url_pro3'];
            }

            $tempArr3 = explode('_', $data['fbimghash3']);
            if($tempArr3[1] == 'video' || !empty($tempArr3[1])){

                $image_hash3 = $data['product3_img_url'];        
                $videoId3 = $tempArr3[0];

                $product3 = (new AttachmentData())->setData(array(
                    AttachmentDataFields::LINK => $linkUrl3,
                    AttachmentDataFields::NAME => $data['headlines_pro3'],
                    AttachmentDataFields::DESCRIPTION => $data['desc_pro3'],
                    AttachmentDataFields::IMAGE_HASH => $image_hash3, // $creative_img_hash1,
                    AttachmentDataFields::VIDEO_ID => $videoId3, 
                    AttachmentDataFields::CALL_TO_ACTION => array(
                        'type' => $data['callToAction_pro3'],
                        'value' => array(
                            'link' => $linkUrl3,
                        ),
                    ),
                ));
            }
            else{
                $hasedImage3 = $this->getHashedImage($data['fbimghash3']);

                $product3 = (new AttachmentData())->setData(array(
                    AttachmentDataFields::LINK => $linkUrl3,
                    AttachmentDataFields::NAME => $data['headlines_pro3'],
                    AttachmentDataFields::DESCRIPTION => $data['desc_pro3'],
                    AttachmentDataFields::IMAGE_HASH => $hasedImage3, // $creative_img_hash1,
                    AttachmentDataFields::CALL_TO_ACTION => array(
                        'type' => $data['callToAction_pro3'],
                        'value' => array(
                            'link' => $linkUrl3,
                        ),
                    ),
                ));
            }

            /*$product3 = (new AttachmentData())->setData(array(
                AttachmentDataFields::LINK => $linkUrl3,
                AttachmentDataFields::NAME => $data['headlines_pro3'],
                AttachmentDataFields::DESCRIPTION => $data['desc_pro3'],
                AttachmentDataFields::IMAGE_HASH => $hasedImage3, //$creative_img_hash3,
                AttachmentDataFields::CALL_TO_ACTION => array(
                    'type' => $data['callToAction_pro3'],
                    'value' => array(
                        'link' => $linkUrl3,
                    ),
                ),
            ));*/


            if ($data['track_google'] == 'Yes') {
                $locationName = explode(",", $this->input->post('locations_autocomplete_include'));
                $products_link = rtrim($data['products_link'], '/') . "/?utm_source=facebook&utm_medium=TCM&utm_campaign=" . urlencode($this->input->post('campaign[compaingname]')) . "&utm_term=" . urlencode($locationName[0]) . "&utm_content=" . urlencode(trim($data['creatives']['body'][0], 0, 20));
            } else {
                $products_link = $data['products_link'];
            }

            $link_data = new LinkData();
            $link_data->setData(array(
                LinkDataFields::LINK => $products_link,
                LinkDataFields::MESSAGE => $data['creatives']['body'][0],
                //LinkDataFields::CAPTION => $data['products_link_cap'],
                LinkDataFields::CHILD_ATTACHMENTS => array(
                    $product1, $product2, $product3,
                ),
            ));

            $object_story_spec = new ObjectStorySpec();
            $object_story_spec->setData(array(
                ObjectStorySpecFields::PAGE_ID => $this->getCreative_pageId(),
                ObjectStorySpecFields::LINK_DATA => $link_data,
                    //ObjectStorySpecFields::TEXT_DATA => $data['creatives']['body'][0],
            ));
			
			$creatives = $this->input->post('creatives');
			$instaaccountid = "";
			$chinstapc = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/'.$creatives["object_id"][0].'/instagram_accounts?access_token='.$this->access_token.'&fields=id,username');
									curl_setopt($chinstapc, CURLOPT_SSL_VERIFYPEER, false);
									$data_arraypc = array(
										'access_token' => $this->access_token
									);
									curl_setopt_array($chinstapc, array(
										CURLOPT_RETURNTRANSFER => true
									));
							
									$responsepc = json_decode(curl_exec($chinstapc));
									curl_close($chinstapc);
									
								if(isset($responsepc->data[0]->id)){
										$instaaccountid = $responsepc->data[0]->id;
									}
									else{
								$chinsta = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/'.$creatives["object_id"][0].'/page_backed_instagram_accounts?access_token='.$this->access_token.'&fields=id');
									curl_setopt($chinsta, CURLOPT_SSL_VERIFYPEER, false);
									$data_array = array(
										'access_token' => $this->access_token
									);
									curl_setopt_array($chinsta, array(
										CURLOPT_RETURNTRANSFER => true
									));
							
									$response = json_decode(curl_exec($chinsta));
									curl_close($chinsta);
									//print_r($response);
									//print_r($response->data[0]->id);
									if(isset($response->data[0]->id)){
										$instaaccountid = $response->data[0]->id;
									}
									else{
										//echo "junaid";
										$chinsin = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/'.$creatives["object_id"][0].'/page_backed_instagram_accounts');
										curl_setopt($chinsin, CURLOPT_SSL_VERIFYPEER, false);
										$data_array = array(
											'access_token' => $this->access_token
										);
										curl_setopt_array($chinsin, array(
											CURLOPT_RETURNTRANSFER => true,
											CURLOPT_POST => true,
											CURLOPT_POSTFIELDS => $data_array
										));
								
										$response = curl_exec($chinsin);
										curl_close($chinsin);
										
										$chinsta2 = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/'.$creatives["object_id"][0].'/page_backed_instagram_accounts?access_token='.$this->access_token.'&fields=id');
										curl_setopt($chinsta2, CURLOPT_SSL_VERIFYPEER, false);
										$data_array = array(
											'access_token' => $this->access_token
										);
										curl_setopt_array($chinsta2, array(
											CURLOPT_RETURNTRANSFER => true
										));
								
										$response = json_decode(curl_exec($chinsta2));
										curl_close($chinsta2);
										if(isset($response->data[0]->id)){
											$instaaccountid = $response->data[0]->id;
										}
									}
								}
			
			
            $creative = new AdCreative(null, 'act_' . $this->ad_account_id);
			if($instaaccountid == ""){
            $creative->setData(array(
                //AdCreativeFields::NAME => $data['creatives']['title'][0],
                AdCreativeFields::BODY => str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $data['creatives']['body'][0]))),
                AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
            ));
			}
			else{
				$creative->setData(array(
                //AdCreativeFields::NAME => $data['creatives']['title'][0],
                AdCreativeFields::BODY => str_replace('"', "'", trim(preg_replace('/\s\s+/', '\n', $data['creatives']['body'][0]))),
				AdCreativeFields::INSTAGRAM_ACTOR_ID => $instaaccountid,
                AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec
            ));
			}

            $creative->create();
            
            $response_result = array();

            if (isset($creative->id) && $creative->id != NULL) {
                $response_result['status'] = 'success';
                $response_result['message'] = $creative->id;
            } else {
                $response_result['status'] = 'false';
                $response_result['message'] = 'Issue in creating Multi Product Creative';
            }
        } 
        catch (Exception $ex) {

            $response_result['status'] = 'false';
            $response_result['message'] = $ex->getMessage();
        }
        
        $this->writeLog("create_multi_product", $response_result);
        if ($frmPreview != NULL) {
            return $creative->id;
        } else {
            return json_encode($response_result);
        }
    }

    public function postEng_creative_ad($data) {

        $response_result = array();

        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->ad_account_id . '/adcreatives');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data_array = array(
            'access_token' => $this->access_token,
            'object_story_id' => $data['creatives']['story_id'],
        );
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data_array
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $this->writeLog("postEng_creative_ad", $data_array);


        $result = json_decode($response);

        if (isset($result->error)) {
            $response_result['status'] = 'false';
            $response_result['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
        } else {
            $response_result['status'] = 'success';
            $response_result['message'] = $result->id;
        }

        return json_encode($response_result);
    }

    public function offer_creative_ad($data) {

        $response_result = array();

        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->ad_account_id . '/adcreatives');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data_array = array(
            'access_token' => $this->access_token,
            'object_story_id' => $data['offer_Id'],
        );
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data_array
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $this->writeLog("offer_creative_ad", $data_array);

        $result = json_decode($response);

        if (isset($result->error)) {
            $response_result['status'] = 'false';
            $response_result['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
        } else {
            $response_result['status'] = 'success';
            $response_result['message'] = $result->id;
        }

        return json_encode($response_result);
    }

    private function getImageAbsloutePath($filePath) {
        $url_arr = explode('/', $filePath);
        $ct = count($url_arr);
        $name = $url_arr[$ct - 1];
        $name_div = explode('.', $name);
        $ct_dot = count($name_div);
        $img_type = $name_div[$ct_dot - 1];
        return FCPATH . "uploads/campaign/".$this->user_id."/" . $name;
    }

    private function getImgNameFrmUrl($filePath) {
        $url_arr = explode('/', $filePath);
        $ct = count($url_arr);
        $name = $url_arr[$ct - 1];
        $name_div = explode('.', $name);
        $ct_dot = count($name_div);
        $img_type = $name_div[$ct_dot - 1];
        return $name;
    }

    private function generateCreativeImg($ad_account, $filePath) {
        
        //$image->{AdImageFields::FILENAME} = $this->getImageAbsloutePath($filePath);
        //$image->{AdImageFields::FILENAME} = "https://fbcdn-creative-a.akamaihd.net/hads-ak-xtf1/t45.1600-4/".$filePath;
        if( strpos($filePath, 'thecampaignmaker') !== false ){ 
            $image = new AdImage(null, "act_" . $ad_account);
            $image->{AdImageFields::FILENAME} = $this->getImageAbsloutePath($filePath);
            $image->create();
            return $image->hash;
        }
        else{
            return $filePath;
        }
    }

    private function create_ad_creative() {
        $child_attachments = array(
            AttachmentDataFields::LINK => $this->getCreative_link(),
            AttachmentDataFields::IMAGE_HASH => $this->getCreative_img_hash(),
        );

        $object_story_spec = array(
            ObjectStorySpecFields::PAGE_ID => $this->getCreative_pageId(),
            ObjectStorySpecFields::LINK_DATA => array(
                LinkDataFields::MESSAGE => $this->getCreative_link_desc(),
                LinkDataFields::LINK => $this->getCreative_link(),
                LinkDataFields::CAPTION => $this->getCreative_cpation(),
            //LinkDataFields::CHILD_ATTACHMENTS => $child_attachments,
        ));
        $creative = new AdCreative(null, 'act_' . $this->ad_account_id);
        $creative->setData(array(
            AdCreativeFields::NAME => $this->getCreative_name(),
            AdCreativeFields::TITLE => $this->getCreative_name(),
            AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
            AdCreativeFields::BODY => $this->getCreative_body(),
            AdCreativeFields::IMAGE_HASH => $this->getCreative_img_hash(),
                //AdCreativeFields::OBJECT_URL => $this->getCreative_link(),
        ));
        $data = array_merge($child_attachments, $object_story_spec);
        $this->writeLog("ClickCreativeAd", $data);
        try {
            $creative->create();
            return $creative->id;
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }

    private function create_cmp() {
        if ($this->campaignobjective == 'PRODUCT_CATALOG_SALES') {
            $this->setCampaignobjective("LINK_CLICKS");
        }
        /* $campaign = new Campaign(null, 'act_' . $this->ad_account_id);
          $campaign->setData(array(
          CampaignFields::NAME => $this->getCompaingname_1(),
          CampaignFields::OBJECTIVE => $this->getCampaignobjective(),
          CampaignFields::EFFECTIVE_STATUS => Campaign::STATUS_PAUSED,
          ));
          $campaign->validate()->create(); */
        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->ad_account_id . '/campaigns');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'access_token' => $this->access_token,
                'status' => 'ACTIVE',
                'objective' => $this->getCampaignobjective(),
                'name' => $this->getCompaingname_1(),
            )
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        //print_r($response);
        $result = json_decode($response);

        $data['ad_account_id'] = $this->ad_account_id;
        $data['getCompaingname_1'] = $this->getCompaingname_1();
        $data['getCampaignobjective'] = $this->getCampaignobjective();
        $this->writeLog("cmpError", $data);
        // try {


        if (isset($result->id) && $result->id != NULL) {
            $campaign_id = $result->id;
            //SAVE INTO DATABSE Campaign Table
            $campaign1['compaingname'] = $this->getCompaingname_1();
            $campaign1['adaccountid'] = $this->ad_account_id;
            $campaign1['fbcampaignid'] = $campaign_id;
            $campaign1['campaignobjective'] = $this->getCampaignobjective();
            $campaign1['campaignstatus'] = 'ACTIVE';
            $campaign1['user_id'] = $this->user_id;
            $campaign_id = $this->Campaign_model->SaveCampaign($campaign1);
            //SAVE into Campaign_Objetive
            $adObjective_id = $this->AddObjective_model->get_objective_id_byname($this->input->post('campaignobjective'));
            $data = array(
                'campaign_id' => $campaign_id,
                'objective_id' => $adObjective_id,
            );
            $this->Campaignobjective_model->campaign_objective_save($data);
        }


        $response_result = array();
        if (isset($result->error)) {
            $response_result['status'] = 'false';
            $response_result['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
        } else {
            $response_result['status'] = 'success';
            $response_result['message'] = $result->id;
            $response_result['campaign_id'] = $campaign_id;
        }
        return json_encode($response_result);
    }

    public function deleteCampaign($campaignId) {
        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/' . $campaignId);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'access_token' => $this->access_token,
                'status' => 'DELETED',
            )
        ));
        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response);
        $response_result = array();
        if (isset($result->error)) {
            $response_result['status'] = 'false';
            $response_result['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
        } else {
            $response_result['status'] = 'success';
            $response_result['message'] = $result->id;
        }
        return json_encode($response_result);
    }

    private function create_adset_new($data = NULL) {
        $split_adset = $_POST['split_adset'];
        
        $optimize_for = $this->input->post('campaignobjective');
        if ($optimize_for == 'CLICK') {
            $optimize_for = 'CLICKS';
        }
        else if ($optimize_for == 'CONVERSIONS') {
            $optimize_for = 'OFFSITE_CONVERSIONS';
        } else if ($optimize_for == 'PRODUCT_CATALOG_SALES') {
            $optimize_for = 'OFFER_CLAIMS';
        } else if ($optimize_for == 'CANVAS_APP_INSTALLS') {
            $optimize_for = 'APP_INSTALLS';
        }

        $locationTemp = explode('],', $_POST['locations_autocomplete_include']);
        $finalLocationStr = '';
        if(!empty($locationTemp)){
            for($i=0; $i<=count($locationTemp)-1; $i++){
                $tempStr = explode(',', $locationTemp[$i]);
                $finalLocationStr .= $tempStr[0].',';
            }
        }

        $locationTemp = explode(',', trim($finalLocationStr, ','));
        $new_geo_locations = trim($this->input->post('new_geo_locations'), ",");
        $new_geo_temp = explode(',', $new_geo_locations);
        
        $i = 0;
        if (count($new_geo_temp) > 0) {
            foreach ($new_geo_temp as $rs) {
                if (isset($locationArr[$locationTemp[$i]])) {
                    if (!is_array($array[$key]))
                        $locationArr[$locationTemp[$i]] = (array) $locationArr[$locationTemp[$i]];
                    $locationArr[$locationTemp[$i]][] = $rs;
                } else {
                    $locationArr[$locationTemp[$i]] = $rs;
                }
                $i++;
            }
        }
        
        $finalTargetArr = array();
        for($i1=0; $i1<=count($_POST['interests'])-1; $i1++){
            $interestsTempArr = explode(',', $_POST['interests'][$i1]);
            //$new_interests = trim($this->input->post('new_interests'.$i1), ",");
            $new_interests = trim($_POST['new_interests'][$i1], ",");
            $new_interests_temp = explode(',', $new_interests);
            $life_events1Arr = array();
            $family_statuses1Arr = array();
            $politics1Arr = array();
            $industries1Arr = array();
            $ethnic_affinity1Arr = array();
            $generation1Arr = array();
            $household_composition1Arr = array();
            $behaviors1Arr = array();
            $interestsArr1 = array();

            $i = 0;
            if (count($new_interests_temp) > 0) {
                if (!empty($new_interests_temp[0])) {
                    foreach ($new_interests_temp as $rs) {
                        $e = explode('_', $rs);
                        $e1 = explode(':', $e[0]);
                        if($e1[1] == 'life'){
                            if (isset($life_events1Arr[$interestsTempArr[$i]])) {
                                if (!is_array($array[$key]))
                                    $life_events1Arr[$interestsTempArr[$i]] = (array) $life_events1Arr[$interestsTempArr[$i]];
                                $life_events1Arr[$interestsTempArr[$i]][] = 'life_events$#${"id":'.str_replace('}', '', $e[2]).'}';
                            } else {
                                $life_events1Arr[$interestsTempArr[$i]] = 'life_events$#${"id":'.str_replace('}', '', $e[2]).'}';//str_replace('}', '', $e[1]);
                            }
                            $life_eventsArr = 'life_events'.($i1+1).'Arr';
                            $finalTargetArr[$life_eventsArr] = $life_events1Arr;
                            $i++;
                        }
                        else if($e1[1] == 'family'){
                            
                            if (isset($family_statuses1Arr[$interestsTempArr[$i]])) {
                                if (!is_array($array[$key]))
                                    $family_statuses1Arr[$interestsTempArr[$i]] = (array) $family_statuses1Arr[$interestsTempArr[$i]];
                                $family_statuses1Arr[$interestsTempArr[$i]][] = 'family_statuses$#${"id":'.str_replace('}', '', $e[2]).'}';
                            } else {
                                $family_statuses1Arr[$interestsTempArr[$i]] = 'family_statuses$#${"id":'.str_replace('}', '', $e[2]).'}';//str_replace('}', '', $e[1]);
                            }
                            $family_statusesArr = 'family_statuses'.($i1+1).'Arr';
                            $finalTargetArr[$family_statusesArr] = $family_statuses1Arr;
                            $i++;
                        }
                        else if($e1[1] == 'politics'){
                            
                            if (isset($politics1Arr[$interestsTempArr[$i]])) {
                                if (!is_array($array[$key]))
                                    $politics1Arr[$interestsTempArr[$i]] = (array) $politics1Arr[$interestsTempArr[$i]];
                                $politics1Arr[$interestsTempArr[$i]][] = 'politics$#${"id":'.str_replace('}', '', $e[1]).'}';
                            } else {
                                $politics1Arr[$interestsTempArr[$i]] = 'politics$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                            }
                            $politicsArr = 'politics'.($i1+1).'Arr';
                            $finalTargetArr[$politicsArr] = $politics1Arr;
                            $i++;
                        }
                        else if($e1[1] == 'industries'){
                            
                            if (isset($industries1Arr[$interestsTempArr[$i]])) {
                                if (!is_array($array[$key]))
                                    $industries1Arr[$interestsTempArr[$i]] = (array) $industries1Arr[$interestsTempArr[$i]];
                                $industries1Arr[$interestsTempArr[$i]][] = 'industries$#${"id":'.str_replace('}', '', $e[1]).'}';
                            } else {
                                $industries1Arr[$interestsTempArr[$i]] = 'industries$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                            }
                            $industriesArr = 'industries'.($i1+1).'Arr';
                            $finalTargetArr[$industriesArr] = $industries1Arr;
                            $i++;
                        }
                        else if($e1[1] == 'ethnic_affinity'){
                            
                            if (isset($ethnic_affinity1Arr[$interestsTempArr[$i]])) {
                                if (!is_array($array[$key]))
                                    $ethnic_affinity1Arr[$interestsTempArr[$i]] = (array) $ethnic_affinity1Arr[$interestsTempArr[$i]];
                                $ethnic_affinity1Arr[$interestsTempArr[$i]][] = 'ethnic_affinity$#${"id":'.str_replace('}', '', $e[1]).'}';
                            } else {
                                $ethnic_affinity1Arr[$interestsTempArr[$i]] = 'ethnic_affinity$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                            }
                            $ethnic_affinityArr = 'ethnic_affinity'.($i1+1).'Arr';
                            $finalTargetArr[$ethnic_affinityArr] = $ethnic_affinity1Arr;
                            $i++;
                        }
                        else if($e1[1] == 'generation'){
                            
                            if (isset($generation1Arr[$interestsTempArr[$i]])) {
                                if (!is_array($array[$key]))
                                    $generation1Arr[$interestsTempArr[$i]] = (array) $generation1Arr[$interestsTempArr[$i]];
                                $generation1Arr[$interestsTempArr[$i]][] = 'generation$#${"id":'.str_replace('}', '', $e[1]).'}';
                            } else {
                                $generation1Arr[$interestsTempArr[$i]] = 'generation$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                            }
                            $generationArr = 'generation'.($i1+1).'Arr';
                            $finalTargetArr[$generationArr] = $generation1Arr;
                            $i++;
                        }
                        else if($e1[1] == 'household_composition'){
                            
                            if (isset($household_composition1Arr[$interestsTempArr[$i]])) {
                                if (!is_array($array[$key]))
                                    $household_composition1Arr[$interestsTempArr[$i]] = (array) $household_composition1Arr[$interestsTempArr[$i]];
                                $household_composition1Arr[$interestsTempArr[$i]][] = 'household_composition$#${"id":'.str_replace('}', '', $e[1]).'}';
                            } else {
                                $household_composition1Arr[$interestsTempArr[$i]] = 'household_composition$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                            }
                            $household_compositionArr = 'household_composition'.($i1+1).'Arr';
                            $finalTargetArr[$household_compositionArr] = $household_composition1Arr;
                            $i++;
                        }
                        else if($e1[1] == 'behaviors'){
                            
                            if (isset($behaviors1Arr[$interestsTempArr[$i]])) {
                                if (!is_array($array[$key]))
                                    $behaviors1Arr[$interestsTempArr[$i]] = (array) $behaviors1Arr[$interestsTempArr[$i]];
                                $behaviors1Arr[$interestsTempArr[$i]][] = 'behaviors$#${"id":'.str_replace('}', '', $e[1]).'}';
                            } else {
                                $behaviors1Arr[$interestsTempArr[$i]] = 'behaviors$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                            }
                            $behaviorsArr = 'behaviors'.($i1+1).'Arr';
                            $finalTargetArr[$behaviorsArr] = $behaviors1Arr;
                            $i++;
                        }
                        else{
                            if($e[2] != 'events' || $e[2] != 'statuses'){
                                
                                if (isset($interestsArr1[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $interestsArr1[$interestsTempArr[$i]] = (array) $interestsArr1[$interestsTempArr[$i]];
                                    $interestsArr1[$interestsTempArr[$i]][] = 'interests$#${"id":'.str_replace('}', '', $e[1]).'}';
                                } else {
                                    $interestsArr1[$interestsTempArr[$i]] = 'interests$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                }
                                $interestsArr = 'interestsArr'.($i1+1);
                                $finalTargetArr[$interestsArr] = $interestsArr1;
                                $i++;
                            }
                        }
                    }
                }
            }
        }

        $interestsTempArr1 = explode(',', $_POST['interests_exclude']);
        $new_interests1 = trim($this->input->post('new_interests_exclude'), ",");
        $new_interests_temp1 = explode(',', $new_interests1);
        $interestsArr11 = array(); 
        $family_statuses1Arr1 = array(); 
        $household_composition1Arr1 = array(); 
        $generation1Arr1 = array(); 
        $ethnic_affinity1Arr1 = array(); 
        $industries1Arr1 = array(); 
        $politics1Arr1 = array(); 
        $life_events1Arr1 = array(); 
        $behaviors1Arr1 = array(); 
        $i = 0;

        if (count($new_interests_temp1) > 0) {
            if (!empty($new_interests_temp1[0])) {
                foreach ($new_interests_temp1 as $rs) {
                    $e = explode('_', $rs);
                    $e1 = explode(':', $e[0]);
                    if($e1[1] == 'life'){
                        if (isset($life_events1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $life_events1Arr1[$interestsTempArr1[$i]] = (array) $life_events1Arr1[$interestsTempArr1[$i]];
                            $life_events1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[2]).'}';
                        } else {
                            $life_events1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[2]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'family_statuses'){
                        
                        if (isset($family_statuses1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $family_statuses1Arr1[$interestsTempArr1[$i]] = (array) $family_statuses1Arr1[$interestsTempArr1[$i]];
                            $family_statuses1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $family_statuses1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'politics'){
                        
                        if (isset($politics1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $politics1Arr1[$interestsTempArr1[$i]] = (array) $politics1Arr1[$interestsTempArr1[$i]];
                            $politics1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $politics1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'industries'){
                        
                        if (isset($industries1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $industries1Arr1[$interestsTempArr1[$i]] = (array) $industries1Arr1[$interestsTempArr1[$i]];
                            $industries1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $industries1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'ethnic_affinity'){
                        
                        if (isset($ethnic_affinity1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $ethnic_affinity1Arr1[$interestsTempArr1[$i]] = (array) $ethnic_affinity1Arr1[$interestsTempArr1[$i]];
                            $ethnic_affinity1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $ethnic_affinity1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'generation'){
                        
                        if (isset($generation1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $generation1Arr1[$interestsTempArr1[$i]] = (array) $generation1Arr1[$interestsTempArr1[$i]];
                            $generation1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $generation1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'household_composition'){
                        
                        if (isset($household_composition1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $household_composition1Arr1[$interestsTempArr1[$i]] = (array) $household_composition1Arr1[$interestsTempArr1[$i]];
                            $household_composition1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $household_composition1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'behaviors'){
                        
                        if (isset($behaviors1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $behaviors1Arr1[$interestsTempArr1[$i]] = (array) $behaviors1Arr1[$interestsTempArr1[$i]];
                            $behaviors1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $behaviors1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else{
                        if($e[2] != 'events'){
                            
                            if (isset($interestsArr11[$interestsTempArr1[$i]])) {
                                if (!is_array($array[$key]))
                                    $interestsArr11[$interestsTempArr1[$i]] = (array) $interestsArr11[$interestsTempArr1[$i]];
                                $interestsArr11[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                            } else {
                                $interestsArr11[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                            }
                            $i++;
                        }
                    }
                }
            }
        }
        $interestsArr11str = '';
        #echo "<pre>"; print_r($interestsArr11);
        if(is_array($interestsArr11)){
            $arrstr = 0;
            foreach ($interestsArr11 as $interestsArr11key => $interestsArr11val){
                if($arrstr == 0){
                    $interestsArr11str = $interestsArr11str.$interestsArr11val;
                }
                else{
                    $interestsArr11str = $interestsArr11str.",".$interestsArr11val;
                }

                $arrstr++;
            }
        }
        $age_min = $this->input->post('form');
        $age_max = $this->input->post('to');
        if(!empty($_POST['form2'])){
            array_push($_POST['form2'], $age_min);
        }
        if(!empty($_POST['to2'])){
            array_push($_POST['to2'], $age_max);
        }
        
        $ageTempStr = array();
        if(!empty($_POST['form2']) && !empty($_POST['to2'])){
            for($a=0; $a<=count($_POST['form2'])-1; $a++){
                array_push($ageTempStr, $_POST['form2'][$a].'-'.$_POST['to2'][$a]);
            }
            $ageTempArr[] = implode(',', $ageTempStr);
        }
        else{
            array_push($ageTempStr, $age_min.'-'.$age_max);
            $ageTempArr[] = implode(',', $ageTempStr);
        }
        $ageTempArr = explode(',', $ageTempArr[0]);
        
        $new_exclude_locations = trim($this->input->post('new_exclude_locations'), ",");
        $new_interests = trim($this->input->post('new_interests1'), ",");
        $genders = $this->input->post('genders');
        
        $page_types = $this->input->post('page_types');
        $relationship_statuses = '';
        $relationship_statuses = $this->input->post('relationship_statuses');
        if(empty($relationship_statuses[0])){
            $relationship_statuses = '';
        }
        $interested_in = $this->input->post('interested_in');
        $lng_locales = trim($this->input->post('new_ad_langauage'), ",");
        $education_schools = trim($this->input->post('new_education_schools'), ",");
        $education_majors = trim($this->input->post('new_education_majors'), ",");
        $work_employers = trim($this->input->post('new_work_employers'), ",");
        $work_positions = trim($this->input->post('new_work_positions'), ",");
        $mobile_device_users = $this->input->post('mobile_device_user');
        $education_statuses = '';
        $education_statuses = $this->input->post('education_statuses');
        if(empty($education_statuses[0])){
            $education_statuses = '';
        }
        $ad_account = $_POST['adaccountid_2'];
        $custom_audiences = $this->input->post('custom_audiences');
        
        /*if (!empty($custom_audiences)) {
            $ca_id = array();
            $ca_name = array();
            foreach ($custom_audiences as $ca) {
                $cain = explode('##', $ca);
                $ca_id[] = $cain[0];
                $ca_name[] = $cain[1];
            }
            $Clevnt = '';
            foreach ($ca_id as $le) {
                if(!empty($le)){
                    $Clevnt .= '{"id":' . $le . '},';
                }
            }
            $Clevnt = trim($Clevnt, ",");
        }
        
        */
         //if (!empty($custom_audiences[0])) {
            $ca_id = array();
            $ca_name = array();
            foreach ($custom_audiences as $ca) {
                if(!empty($ca)){
                    $cain = explode('##', $ca);
                    $ca_id[] = $cain[0];
                    $ca_name[] = $cain[1];
                }
            }
            //print_r($ca_id);
            $Clevnt = '';
            if(!empty($ca_id)){    
                foreach ($ca_id as $le) {
                    $Clevnt .= '{"id":' . $le . '},';
                }
                $Clevnt = trim($Clevnt, ",");
            }
       // }

        if (in_array('location', $split_adset)){
            $locationsArr = explode(',', trim($finalLocationStr, ','));
        } else {
            $locationsArr[] = trim($finalLocationStr, ',');
        }
        
        /*if (in_array('interests', $split_adset)){
            $interestsArr = explode(',', $_POST['interests1']);
        } else {
            $interestsArr[] = $_POST['interests1'];
        }*/
        
        if (in_array('placement', $split_adset)){
            for($p=0; $p<count($_POST['page_types']); $p++){
                if($_POST['page_types'][$p] == 'desktopfeed'){
                    $placementTempArr[] = 'DNF';
                }
                else if($_POST['page_types'][$p] == 'mobilefeed'){
                    $placementTempArr[] = 'MNF';
                }
                else if($_POST['page_types'][$p] == 'mobileexternal'){
                    $placementTempArr[] = 'TPMS';
                }
                else if($_POST['page_types'][$p] == 'rightcolumn'){
                    $placementTempArr[] = 'DRHS';
                }
                else if($_POST['page_types'][$p] == 'instagram'){
                    $placementTempArr[] = 'INSTA';
                }
            }
            $placementArr = $placementTempArr;
        } else {
            for($p=0; $p<count($_POST['page_types']); $p++){
                if($_POST['page_types'][$p] == 'desktopfeed'){
                    $placementTempArr[] = 'DNF';
                }
                else if($_POST['page_types'][$p] == 'mobilefeed'){
                    $placementTempArr[] = 'MNF';
                }
                else if($_POST['page_types'][$p] == 'mobileexternal'){
                    $placementTempArr[] = 'TPMS';
                }
                else if($_POST['page_types'][$p] == 'rightcolumn'){
                    $placementTempArr[] = 'DRHS';
                }
                else if($_POST['page_types'][$p] == 'instagram'){
                    $placementTempArr[] = 'INSTA';
                }
            }
            $placementArr[] = implode(',', $placementTempArr);
        }
        
        if (in_array('genders', $split_adset)) { //Split at at genders base
            $gendersArr = array('Men', 'Women');
        } else {
            if (!empty($_POST['genders'])) {
                if($_POST['genders'] == '1')
                    $gendersArr = array('Men');
                else
                    $gendersArr = array('Women');
            } else {
                $gendersArr1 = array('Men', 'Women');
                $gendersArr[] = implode(',', $gendersArr1);
            }
        }
            
        if (in_array('custom_audiences', $split_adset)){
            $custom_audiencesArr = $ca_name;
        }
        else{
            $custom_audiencesArr[] = implode(',', $ca_name);
        }
        //$custom_audiences1
                
        $i = 0;
        if (count($data['custom_audiences']) > 0) {
            foreach ($data['custom_audiences'] as $rs) {
                $cain = explode('##', $rs);
                $custom_audiences1[$cain[1]] = $cain[0];
                $i++;
            }
        }
        
        
        $demographicArr1 = array();
        if(!empty($locationsArr)){
            $finalTempArr[] = $locationsArr;//0
        }
        if (in_array('interests', $split_adset)){
            $finalTemp1Arr = array();
            $finalTemp2Arr = array();
            $finalTemp3Arr = array();
            $finalTemp4Arr = array();
            $finalTemp5Arr = array();
            if(!empty($finalTargetArr['interestsArr1'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr1']);
            }
            if(!empty($finalTargetArr['family_statuses1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses1Arr']);
            }
            if(!empty($finalTargetArr['life_events1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events1Arr']);
            }
            if(!empty($finalTargetArr['politics1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics1Arr']);
            }
            if(!empty($finalTargetArr['industries1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries1Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity1Arr']);
            }
            if(!empty($finalTargetArr['generation1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation1Arr']);
            }
            if(!empty($finalTargetArr['household_composition1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition1Arr']);
            }
            if(!empty($finalTargetArr['behaviors1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors1Arr']);
            }
            
            if(!empty($finalTemp1Arr)){
                $finalTempArr[] = $finalTemp1Arr;//1
            }
            else{
                $finalTempArr[] = array('0'=>'');  
            }

            if(!empty($finalTargetArr['interestsArr2'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['interestsArr2']);
            }
            if(!empty($finalTargetArr['family_statuses2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['family_statuses2Arr']);
            }
            if(!empty($finalTargetArr['life_events2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['life_events2Arr']);
            }
            if(!empty($finalTargetArr['politics2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['politics2Arr']);
            }
            if(!empty($finalTargetArr['industries2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['industries2Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['ethnic_affinity2Arr']);
            }
            if(!empty($finalTargetArr['generation2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['generation2Arr']);
            }
            if(!empty($finalTargetArr['household_composition2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['household_composition2Arr']);
            }
            if(!empty($finalTargetArr['behaviors2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['behaviors2Arr']);
            }
            if(!empty($finalTemp2Arr)){
                $finalTempArr[] = $finalTemp2Arr;//2
            }
            else{
                $finalTempArr[] = array('0'=>'');  //2
            }

            if(!empty($finalTargetArr['interestsArr3'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['interestsArr3']);
            }
            if(!empty($finalTargetArr['family_statuses3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['family_statuses3Arr']);
            }
            if(!empty($finalTargetArr['life_events3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['life_events3Arr']);
            }
            if(!empty($finalTargetArr['politics3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['politics3Arr']);
            }
            if(!empty($finalTargetArr['industries3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['industries3Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['ethnic_affinity3Arr']);
            }
            if(!empty($finalTargetArr['generation3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['generation3Arr']);
            }
            if(!empty($finalTargetArr['household_composition3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['household_composition3Arr']);
            }
            if(!empty($finalTargetArr['behaviors3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['behaviors3Arr']);
            }
            
            if(!empty($finalTemp3Arr)){
                $finalTempArr[] = $finalTemp3Arr;//3
            }
            else{
                $finalTempArr[] = array('0'=>'');  //3
            }
            
            if(!empty($finalTargetArr['interestsArr4'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['interestsArr4']);
            }
            if(!empty($finalTargetArr['family_statuses4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['family_statuses4Arr']);
            }
            if(!empty($finalTargetArr['life_events4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['life_events4Arr']);
            }
            if(!empty($finalTargetArr['politics4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['politics4Arr']);
            }
            if(!empty($finalTargetArr['industries4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['industries4Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['ethnic_affinity4Arr']);
            }
            if(!empty($finalTargetArr['generation4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['generation4Arr']);
            }
            if(!empty($finalTargetArr['household_composition4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['household_composition4Arr']);
            }
            if(!empty($finalTargetArr['behaviors4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['behaviors4Arr']);
            }
            
            if(!empty($finalTemp4Arr)){
                $finalTempArr[] = $finalTemp4Arr;//4
            }
            else{
                $finalTempArr[] = array('0'=>'');  //4
            }

            if(!empty($finalTargetArr['interestsArr5'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['interestsArr5']);
            }
            if(!empty($finalTargetArr['family_statuses5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['family_statuses5Arr']);
            }
            if(!empty($finalTargetArr['life_events5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['life_events5Arr']);
            }
            if(!empty($finalTargetArr['politics5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['politics5Arr']);
            }
            if(!empty($finalTargetArr['industries5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['industries5Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['ethnic_affinity5Arr']);
            }
            if(!empty($finalTargetArr['generation5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['generation5Arr']);
            }
            if(!empty($finalTargetArr['household_composition5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['household_composition5Arr']);
            }
            if(!empty($finalTargetArr['behaviors5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['behaviors5Arr']);
            }
            
            if(!empty($finalTemp5Arr)){
                $finalTempArr[] = $finalTemp5Arr;//5
            }
            else{
                $finalTempArr[] = array('0'=>'');  //5
            }
        }
        else{
            $finalTemp1Arr = array();
            if(!empty($finalTargetArr['interestsArr1'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr1']);
            }
            if(!empty($finalTargetArr['family_statuses1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses1Arr']);
            }
            if(!empty($finalTargetArr['life_events1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events1Arr']);
            }
            if(!empty($finalTargetArr['politics1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics1Arr']);
            }
            if(!empty($finalTargetArr['industries1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries1Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity1Arr']);
            }
            if(!empty($finalTargetArr['generation1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation1Arr']);
            }
            if(!empty($finalTargetArr['household_composition1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition1Arr']);
            }
            if(!empty($finalTargetArr['behaviors1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors1Arr']);
            }

            if(!empty($finalTargetArr['interestsArr2'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr2']);
            }
            if(!empty($finalTargetArr['family_statuses2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses2Arr']);
            }
            if(!empty($finalTargetArr['life_events2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events2Arr']);
            }
            if(!empty($finalTargetArr['politics2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics2Arr']);
            }
            if(!empty($finalTargetArr['industries2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries2Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity2Arr']);
            }
            if(!empty($finalTargetArr['generation2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation2Arr']);
            }
            if(!empty($finalTargetArr['household_composition2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition2Arr']);
            }
            if(!empty($finalTargetArr['behaviors2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors2Arr']);
            }

            if(!empty($finalTargetArr['interestsArr3'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr3']);
            }
            if(!empty($finalTargetArr['family_statuses3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses3Arr']);
            }
            if(!empty($finalTargetArr['life_events3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events3Arr']);
            }
            if(!empty($finalTargetArr['politics3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics3Arr']);
            }
            if(!empty($finalTargetArr['industries3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries3Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity3Arr']);
            }
            if(!empty($finalTargetArr['generation3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation3Arr']);
            }
            if(!empty($finalTargetArr['household_composition3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition3Arr']);
            }
            if(!empty($finalTargetArr['behaviors3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors3Arr']);
            }
            
            if(!empty($finalTargetArr['interestsArr4'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr4']);
            }
            if(!empty($finalTargetArr['family_statuses4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses4Arr']);
            }
            if(!empty($finalTargetArr['life_events4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events4Arr']);
            }
            if(!empty($finalTargetArr['politics4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics4Arr']);
            }
            if(!empty($finalTargetArr['industries4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries4Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity4Arr']);
            }
            if(!empty($finalTargetArr['generation4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation4Arr']);
            }
            if(!empty($finalTargetArr['household_composition4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition4Arr']);
            }
            if(!empty($finalTargetArr['behaviors4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors4Arr']);
            }

            if(!empty($finalTargetArr['interestsArr5'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr5']);
            }
            if(!empty($finalTargetArr['family_statuses5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses5Arr']);
            }
            if(!empty($finalTargetArr['life_events5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events5Arr']);
            }
            if(!empty($finalTargetArr['politics5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics5Arr']);
            }
            if(!empty($finalTargetArr['industries5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries5Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity5Arr']);
            }
            if(!empty($finalTargetArr['generation5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation5Arr']);
            }
            if(!empty($finalTargetArr['household_composition5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition5Arr']);
            }
            if(!empty($finalTargetArr['behaviors5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors5Arr']);
            }
            if(!empty($finalTemp1Arr)){
                $finalTempArr[] = array('0'=> str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(',', array_values($finalTemp1Arr))));//1
            }
            else{
                $finalTempArr[] = array('0'=>'');  
            }
            $finalTempArr[] = array('0'=>'');  //2
            $finalTempArr[] = array('0'=>'');  //3
            $finalTempArr[] = array('0'=>'');  //4
            $finalTempArr[] = array('0'=>'');  //5
        }
        if(!empty($placementArr)){
            $finalTempArr[] = $placementArr;//6
        }
        else{
            $finalTempArr[] = array('0'=>''); 
        }
        if(!empty($gendersArr)){
            $finalTempArr[] = $gendersArr;//7
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($custom_audiencesArr)){
            $finalTempArr[] = $custom_audiencesArr;//8
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($ageTempArr)){
            $finalTempArr[] = $ageTempArr;//9
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        
        if(!empty($demographicArr1)){
            $finalTempArr[] = $demographicArr1;//10
        }
        else{
            $finalTempArr[] = array('0'=>'');  
        }
        
        
        if(!empty($interestsArr11str)){
            $finalTempArr[] = array('0'=>$interestsArr11str);//11
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        /*
        if(!empty($interestsArr11)){
            $finalTempArr[] = $interestsArr11;//11
        }
        else{
            $finalTempArr[] = array('0'=>'');
        } */
        if(!empty($behaviors1Arr1)){
            $finalTempArr[] = $behaviors1Arr1;//12
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($family_statuses1Arr1)){
            $finalTempArr[] = $family_statuses1Arr1;//13
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($household_composition1Arr1)){
            $finalTempArr[] = $household_composition1Arr1;//14
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($generation1Arr1)){
            $finalTempArr[] = $generation1Arr1;//15
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($ethnic_affinity1Arr1)){
            $finalTempArr[] = $ethnic_affinity1Arr1;//16
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($industries1Arr1)){
            $finalTempArr[] = $industries1Arr1;//17
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($politics1Arr1)){
            $finalTempArr[] = $politics1Arr1;//18
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($life_events1Arr1)){
            $finalTempArr[] = $life_events1Arr1;//19
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        
        $finalArr = $this->combinations($finalTempArr);
        /*echo "<PRE>";print_R($custom_audiencesArr);
        echo "<PRE>";print_R($Clevnt);
        echo "<PRE>";print_R($finalArr);exit;*/
        
        $campaignobjective = $data['campaignobjective'];
        $new_exclude_locations = trim($data['new_exclude_locations'], ",");
        $relationship_statuses = $data['relationship_statuses'];
        $interested_in = $data['interested_in'];
        $lng_locales = trim($data['new_ad_langauage'], ",");
        $page_types = $data['page_types'];

        $education_schools = trim($data['new_education_schools'], ",");
        $education_majors = trim($data['new_education_majors'], ",");
        $work_employers = trim($data['new_work_employers'], ",");
        $work_positions = trim($data['new_work_positions'], ",");
        $mobile_device_users = $data['mobile_device_user'];
        $finalTempArr1 = array();
        $finalTempArr1[] = $data['day'];
        $finalTempArr1[] = $data['time'];
        $finalArr1 = $this->combinations($finalTempArr1);
        $str1 = '';
        if(!empty($finalArr1)){
            foreach ($finalArr1 as $key => $value) {
                $str1 .= $value[0].'#'.$value[1].',';
            }
        }
        $week_calendar = trim($str1, ',');
        //$week_calendar = trim($data['week_calendar'], ",");
        $this->budget_type = 'daily_budget';
        $data['adset_start_date'] = $data['adset_start_date'];
        $current_cur_code = $this->session->userdata('cur_code');
        $postFields = "";
        if($current_cur_code == "VND" || $current_cur_code == "TWD" || $current_cur_code == "CLP" || $current_cur_code == "COP" || $current_cur_code == "CRC" || $current_cur_code == "HUF" || $current_cur_code == "ISK" || $current_cur_code == "IDR" || $current_cur_code == "JPY" || $current_cur_code == "KRW" || $current_cur_code == "PYG"){
        
            $postFields = array(
                'access_token' => $this->access_token,
                'status' => 'ACTIVE',
                'campaign_id' => $data['Cmp_id'],
                $this->budget_type => $this->getBudgetAmount(),
                'billing_event' => $data['billing_event'],
                'optimization_goal' => $data['optimization_goal'],
                'start_time' => (new \DateTime($data['adset_start_date']))->format(\DateTime::ISO8601),
            );
        }
        else{
            $postFields = array(
                'access_token' => $this->access_token,
                'status' => 'ACTIVE',
                'campaign_id' => $data['Cmp_id'],
                $this->budget_type => $this->getBudgetAmount() / 0.01,
                'billing_event' => $data['billing_event'],
                'optimization_goal' => $data['optimization_goal'],
                'start_time' => (new \DateTime($data['adset_start_date']))->format(\DateTime::ISO8601),
            );
        }

        if ($data['adset_end_date'] != "") {
            $postFields['end_time'] = (new \DateTime($data['adset_end_date']))->format(\DateTime::ISO8601);
        }
        if ($campaignobjective == 'PAGE_LIKES') {
            $postFields['promoted_object'] = "{'page_id':'" . $this->creative_pageId . "'}";
        } elseif ($campaignobjective == 'CANVAS_APP_INSTALLS') {
            $storURl = 'https://apps.facebook.com/' . $data['creatives']['app_id'][0] . '/';
            $postFields['promoted_object'] = "{'application_id':'" . $data['creatives']['app_id'][0] . "','object_store_url':'" . $storURl . "'}";
        } elseif ($campaignobjective == 'OFFER_CLAIMS') {
            $offer_Id = explode('_', $data['offer_Id']);
            $postFields['promoted_object'] = "{'offer_id':'" . trim($offer_Id[1]) . "'}";
        } elseif ($campaignobjective == 'CONVERSIONS') {
            $fb_pixel = $this->getAdspixels();
            $pixel_id = $this->create_new_tracking_pixel('Pixel-' . $data['adset_start_date'], $this->access_token, $this->ad_account_id, $data['pixel_id1']);
            
            $postFields['promoted_object'] = "{'pixel_id':" . $fb_pixel . ", 'custom_event_type':'".$data['pixel_id1']."'}";
            
			
            //$postFields['promoted_object'] = "{'custom_event_type':'".$data['pixel_id1']."'}";
        } elseif ($campaignobjective == 'LEAD_GENERATION') {
            $postFields['promoted_object'] = '{"page_id":"' . $this->creative_pageId . '"}';
        }

        if (isset($data['bid_amount']) && $data['bid_amount'] > 0) {
            $postFields['bid_amount'] = $data['bid_amount'] / 0.01;
        } else {
            //$postFields['is_autobid'] = true;
            $postFields['bid_strategy'] = 'LOWEST_COST_WITHOUT_CAP';
            
        }
		//print_r($week_calendar);
        if ($this->budget_type == 'lifetime_budget' && !empty($week_calendar)) {
		//if ($this->budget_type == 'daily_budget' && !empty($week_calendar)) {	

            $week_calendar = $this->getWeekCalendar($week_calendar);
            $postFields['pacing_type'] = '["day_parting"]';
            $postFields['adset_schedule'] = "[" . $week_calendar . "]";
        }
		
		//echo $this->budget_type;
        //echo "<pre>";print_r($week_calendar); print_r($postFields); echo "</pre>"; exit;
        $locationName = explode(",", trim($finalLocationStr, ','));
        $interestName = explode(",", $data['interests1']);
        $fas = 0; //Should be zero at each cost
        
        #echo "<PRE>";print_R($postFields);exit;
        if(!empty($finalArr)){
            $tempStr1 = '';
            for($i=0; $i<=count($finalArr)-1; $i++){
                if ($fas <= $this->adsetLmit) {
                    if($finalArr[$i][7] == 'Men,Women'){
                        $genderType = ", Men+Women";//'Both';
                    }
                    else{
                        if($finalArr[$i][7] == 'Men'){
                            $genderType = ', Men';
                        }
                        else{
                            $genderType = ', Female';
                        }
                    }

                    //age group
                    $ageGroup = explode("-", $finalArr[$i][9]);
                    $data['form'] = $ageGroup[0];
                    $data['to'] = $ageGroup[1];
                    
                    $int = $beh = $plas = $demo = $cusInst = $life = $poli = $ind = $ethni = $gene = $hous = $famile = '';
                    if(!empty($finalArr[$i][1])){
                        $int = $finalArr[$i][1];
                    }
                    if(!empty($finalArr[$i][10])){
                        $beh = $finalArr[$i][10];
                    }
                    if(!empty($finalArr[$i][50])){
                        $demo = $finalArr[$i][50];
                    }
                    if(!empty($finalArr[$i][6])){
                        $plas = $finalArr[$i][6];
                    }
                    if(!empty($finalArr[$i][8])){
                        $cusInst = $finalArr[$i][8];
                    }
                    if(!empty($finalArr[$i][15])){
                        $life = $finalArr[$i][15];
                    }
                    if(!empty($finalArr[$i][20])){
                        $poli = $finalArr[$i][20];
                    }
                    if(!empty($finalArr[$i][25])){
                        $ind = $finalArr[$i][25];
                    }
                    if(!empty($finalArr[$i][30])){
                        $ethni = $finalArr[$i][30];
                    }
                    if(!empty($finalArr[$i][35])){
                        $gene = $finalArr[$i][35];
                    }
                    if(!empty($finalArr[$i][40])){
                        $hous = $finalArr[$i][40];
                    }
                    if(!empty($finalArr[$i][45])){
                        $famile = $finalArr[$i][45];
                    }
					
					$locationsfnamefinal  = "";
					if(!empty($finalArr[$i][0])){
						if (in_array('location', $split_adset)){
							$locationsfnamefinal = $finalArr[$i][0];
						}
						else{
							$locationsfname = explode(",", $finalArr[$i][0]);
							if(count($locationsfname) == 1){
								$locationsfnamefinal = $locationsfname[0];
							}
							else{
								$locationsfnamefinalcount = count($locationsfname)-1;
								$locationsfnamefinal = $locationsfname[0]." (+". $locationsfnamefinalcount ." Location(s))";
							}
						}
					}
					
					$agegroupfinal = ", ".$data['form']."-".$data['to'];
					
					$placementfnamefinal  = "";
					if(!empty($plas)){
						$placementfname = explode(",", $plas);
						if(count($placementfname) == 1){
							$placementfnamefinal = ", ".$placementfname[0];
						}
						else{
							$placementfnamefinalcount = count($placementfname)-1;
							$placementfnamefinal = ", ".$placementfname[0]." (+". $placementfnamefinalcount ." Placement(s))";
						}
					}
					
					$custfnamefinal  = "";
					if (in_array('custom_audiences', $split_adset)){
						if(!empty($finalArr[$i][8])){
							$custfnamefinal = ", ".$finalArr[$i][8];
						}
					}
					else {
						if(!empty($custom_audiences)){
							
								$custfname = array();
								foreach ($custom_audiences as $ca) {
									$cain = explode('##', $ca);
									$ca_id[] = $cain[0];
									$custfname[] = $cain[1];
								}
							if(count($custfname) == 1){
								$custfnamefinal = ", ".$custfname[0];
							}
							else{
								$custfnamefinalcount = count($custfname)-1;
								$custfnamefinal = ", ".$custfname[0]." (+". $custfnamefinalcount ." Custom Audience)";
							}
						}
					}
					
					$interestfnamefinal  = "";
					if(isset($_POST['interests'][0])){
						if(!empty($_POST['interests'][0])){
							
							$interstfname = explode(",", $_POST['interests'][0]);
							
							 if (in_array('interests', $split_adset)){
								 if(count($interstfname) == 1){
									$interestfnamefinal = ", ".$interstfname[0];
								 }
								 else{ 
									 if($i <= count($interstfname)-1){
										 $interestfnamefinal = ", ".$interstfname[$i];
									 }
									 else{
										 $realindex = ($i % count($interstfname));
										 //$getrealindex = $i-$realindex;
										 $interestfnamefinal = ", ".$interstfname[$realindex];
									 }
								 }
							 }
							 else{
							
									if(count($interstfname) == 1){
										$interestfnamefinal = ", ".$interstfname[0];
									}
									else{
										$interestfnamefinalcount = count($interstfname)-1;
										$interestfnamefinal = ", ".$interstfname[0]." (+". $interestfnamefinalcount ." Interest(s))";
									}
							 }
						}
					}
					//print_r($finalArr);
					//print_r($_POST['interests'][0]);
					
                    $postFields['name'] = $locationsfnamefinal.$genderType.$agegroupfinal.$interestfnamefinal.$placementfnamefinal.$custfnamefinal;
					//print_r($postFields['name']);
					//exit;
					/*print_r($postFields['name']);
					exit;*/
					//$finalArr[$i][0] . ', ' . $data['form'] . " to " . $data['to'] . ", " . $genderType . ", " . $int . ", " . $cusInst.", ".$beh.", ".$demo.", ".$plas.", ".$life.", ".$poli.", ".$ind.", ".$ethni.", ".$gene.", ".$hous.", ".$famile;

                    //$postFields['name'] = str_replace(", ,", ",", trim(trim(trim(trim($postFields['name']), ',')), ','));
					//print_r($_POST['interests']);
					//print_r($locationsfnamefinal.$genderType.$agegroupfinal.$interestfnamefinal.$placementfnamefinal.$custfnamefinal);
					//exit;




                    if (!in_array('location', $split_adset)){
                        $new_geo_locations1 = str_replace('$#$', ',', trim($data['new_geo_locations'], "$#$"));
                        $new_geo_locations = $this->getProperLocationArray($new_geo_locations1);
                    }
                    else{
                        $new_geo_locations = str_replace('$#$', ',', $locationArr[$finalArr[$i][0]]);
                    }

                    $param = '{';
					
					
					
					
				/*	$param = 'optimize_for=' . $optimize_for;
				$ageGroup = explode("-", $finalArr[$i][9]);
                $age_max = $ageGroup[0];
                $age_min = $ageGroup[1];
                    
                if (!empty($age_min)) {
                    $param .= "&targeting_spec={'age_min':" . $age_max;
                }
                if (!empty($age_max)) {
                    $param .= ",'age_max':" . $age_min;
                }
				
				
				
				if(!empty($new_geo_locations) && $new_geo_locations != ':[]'){
					$param .= ",'geo_locations': {" . $new_geo_locations . "}";
				}
				if(!empty($genders)){
					$param .= ",'genders':[" . $genders . "]";
				} */
					
					if (!empty($data['form'])) {
                        $param .= "'age_min':" . str_replace('+', '', $data['form']);
                    }
                    if (!empty($data['to'])) {
                        $param .= ",'age_max':" . str_replace('+', '', $data['to']);
                    }
					
					
					if(!empty($new_geo_locations) && $new_geo_locations != ':[]'){
                    	$param .= ",'geo_locations': {" . $new_geo_locations . "}";
					}
                    if (!empty($new_exclude_locations)) {
                        $new_exclude_locations = $this->getProperLocationArray($new_exclude_locations);
                        $param .= ",'excluded_geo_locations': {" . $new_exclude_locations . "}";
                    }

                    if (!in_array('genders', $split_adset)){
                        $param .= ",'genders':[" . $genders . "]";
                    }
                    else{
                        if($finalArr[$i][7] == 'Women')
                            $param .= ",'genders':[2]";
                        else
                            $param .= ",'genders':[1]";
                    }

                    

                    if (!in_array('interests', $split_adset)){
                        //not split
                        
                        if (!empty($interestsArr11) || !empty($family_statuses1Arr1) || !empty($household_composition1Arr1) || !empty($generation1Arr1) || !empty($ethnic_affinity1Arr1) || !empty($industries1Arr1) || !empty($politics1Arr1) || !empty($life_events1Arr1) || !empty($behaviors1Arr1)) {
                            
                            $param .= ", 'exclusions':{";
                            $tempstr = 0;
                            if (!empty($interestsArr11)){
                                $param .= "'interests':[" . implode(",", $interestsArr11) . "]";
                                $tempstr = 1; 
                            }
                            if(!empty($family_statuses1Arr1)){
                                if($tempstr != 0){
                                    $param .= ",'family_statuses':[" . implode(",", $family_statuses1Arr1) . "]";  
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'family_statuses':[" . implode(",", $family_statuses1Arr1) . "]";   
                                }
                            }
                            if(!empty($household_composition1Arr1)){
                                if($tempstr != 0){
                                    $param .= ",'household_composition':[" . implode(",", $household_composition1Arr1) . "]";
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'household_composition':[" . implode(",", $household_composition1Arr1) . "]";
                                }
                            }
                            if(!empty($generation1Arr1)){
                                if($tempstr != 0){
                                    $param .= ",'generation':[" . implode(",", $generation1Arr1) . "]";
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'generation':[" . implode(",", $generation1Arr1) . "]";  
                                }
                            }
                            if(!empty($ethnic_affinity1Arr1)){
                                if($tempstr != 0){
                                    $param .= ",'ethnic_affinity':[" . implode(",", $ethnic_affinity1Arr1) . "]";
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'ethnic_affinity':[" . implode(",", $ethnic_affinity1Arr1) . "]"; 
                                }
                            }
                            if(!empty($industries1Arr1)){
                                if($tempstr != 0){
                                    $param .= ",'industries':[" . implode(",", $industries1Arr1) . "]";
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'industries':[" . implode(",", $industries1Arr1) . "]";  
                                }
                            }
                            if(!empty($politics1Arr1)){
                                if($tempstr != 0){
                                    $param .= ",'politics':[" . implode(",", $politics1Arr1) . "]";
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'politics':[" . implode(",", $politics1Arr1) . "]";  
                                }
                            }
                            if(!empty($life_events1Arr1)){
                                if($tempstr != 0){
                                    $param .= ",'life_events':[" . implode(",", $life_events1Arr1) . "]";
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'life_events':[" . implode(",", $life_events1Arr1) . "]";
                                }
                            }
                            if(!empty($behaviors1Arr1)){
                                if($tempstr != 0){
                                    $param .= ",'behaviors':[" . implode(",", $behaviors1Arr1) . "]";
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'behaviors':[" . implode(",", $behaviors1Arr1) . "]";
                                }
                            }
                            $param .= "}";
                        }

                        $param .= ", 'flexible_spec':[";
                        if (!empty($finalTargetArr['interestsArr1']) || !empty($finalTargetArr['family_statuses1Arr']) || !empty($finalTargetArr['household_composition1Arr']) || !empty($finalTargetArr['generation1Arr']) || !empty($finalTargetArr['ethnic_affinity1Arr']) || !empty($finalTargetArr['industries1Arr']) || !empty($finalTargetArr['politics1Arr']) || !empty($finalTargetArr['life_events1Arr']) || !empty($finalTargetArr['behaviors1Arr'])) {
                            $param .= "{";
                            $tempstr = 0;
                            if (!empty($finalTargetArr['interestsArr1'])){
                                $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr1'])) . "]";
                                $tempstr = 1;
                            }
                            if(!empty($finalTargetArr['family_statuses1Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses1Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses1Arr'])) . "]";   
                                }
                            }
                            if(!empty($finalTargetArr['household_composition1Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition1Arr'])) . "]"; 
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition1Arr'])) . "]";
                                }
                            }
                            if(!empty($finalTargetArr['generation1Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation1Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation1Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['ethnic_affinity1Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity1Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity1Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['industries1Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries1Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries1Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['politics1Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics1Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics1Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['life_events1Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events1Arr'])) . "]";  
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events1Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['behaviors1Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors1Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors1Arr'])) . "]";
                                }
                            }
                            $param .= "}";
                        }
                        if (!empty($finalTargetArr['interestsArr2']) || !empty($finalTargetArr['family_statuses2Arr']) || !empty($finalTargetArr['household_composition2Arr']) || !empty($finalTargetArr['generation2Arr']) || !empty($finalTargetArr['ethnic_affinity2Arr']) || !empty($finalTargetArr['industries2Arr']) || !empty($finalTargetArr['politics2Arr']) || !empty($finalTargetArr['life_events2Arr']) || !empty($finalTargetArr['behaviors2Arr'])) {
                            $param .= ",{";
                            $tempstr = 0;
                            if (!empty($finalTargetArr['interestsArr2'])){
                                $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr2'])) . "]";
                                $tempstr = 1;
                            }
                            if(!empty($finalTargetArr['family_statuses2Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses2Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses2Arr'])) . "]";   
                                }
                            }
                            if(!empty($finalTargetArr['household_composition2Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition2Arr'])) . "]"; 
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition2Arr'])) . "]";
                                }
                            }
                            if(!empty($finalTargetArr['generation2Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation2Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation2Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['ethnic_affinity2Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity2Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity2Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['industries2Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries2Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries2Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['politics2Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics2Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics2Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['life_events2Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events2Arr'])) . "]";  
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events2Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['behaviors2Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors2Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors2Arr'])) . "]";
                                }
                            }
                            $param .= "}";
                        }
                        if (!empty($finalTargetArr['interestsArr3']) || !empty($finalTargetArr['family_statuses3Arr']) || !empty($finalTargetArr['household_composition3Arr']) || !empty($finalTargetArr['generation3Arr']) || !empty($finalTargetArr['ethnic_affinity3Arr']) || !empty($finalTargetArr['industries3Arr']) || !empty($finalTargetArr['politics3Arr']) || !empty($finalTargetArr['life_events3Arr']) || !empty($finalTargetArr['behaviors3Arr'])) {
                            $param .= ",{";
                            $tempstr = 0;
                            if (!empty($finalTargetArr['interestsArr3'])){
                                $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr3'])) . "]";
                                $tempstr = 1;
                            }
                            if(!empty($finalTargetArr['family_statuses3Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses3Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses3Arr'])) . "]";   
                                }
                            }
                            if(!empty($finalTargetArr['household_composition3Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition3Arr'])) . "]"; 
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition3Arr'])) . "]";
                                }
                            }
                            if(!empty($finalTargetArr['generation3Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation3Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation3Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['ethnic_affinity3Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity3Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity3Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['industries3Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries3Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries3Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['politics3Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics3Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics3Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['life_events3Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events3Arr'])) . "]";  
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events3Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['behaviors3Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors3Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors3Arr'])) . "]";
                                }
                            }
                            $param .= "}";
                        }
                        if (!empty($finalTargetArr['interestsArr4']) || !empty($finalTargetArr['family_statuses4Arr']) || !empty($finalTargetArr['household_composition4Arr']) || !empty($finalTargetArr['generation4Arr']) || !empty($finalTargetArr['ethnic_affinity4Arr']) || !empty($finalTargetArr['industries4Arr']) || !empty($finalTargetArr['politics1Arr']) || !empty($finalTargetArr['life_events4Arr']) || !empty($finalTargetArr['behaviors4Arr'])) {
                            $param .= "{";
                            $tempstr = 0;
                            if (!empty($finalTargetArr['interestsArr4'])){
                                $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr4'])) . "]";
                                $tempstr = 1;
                            }
                            if(!empty($finalTargetArr['family_statuses4Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses4Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses4Arr'])) . "]";   
                                }
                            }
                            if(!empty($finalTargetArr['household_composition4Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition4Arr'])) . "]"; 
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition4Arr'])) . "]";
                                }
                            }
                            if(!empty($finalTargetArr['generation4Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation4Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation4Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['ethnic_affinity4Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity4Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity4Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['industries4Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries4Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries4Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['politics4Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics4Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics4Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['life_events4Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events4Arr'])) . "]";  
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events4Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['behaviors4Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors4Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors4Arr'])) . "]";
                                }
                            }
                            $param .= "}";
                        }
                        if (!empty($finalTargetArr['interestsArr5']) || !empty($finalTargetArr['family_statuses5Arr']) || !empty($finalTargetArr['household_composition5Arr']) || !empty($finalTargetArr['generation5Arr']) || !empty($finalTargetArr['ethnic_affinity5Arr']) || !empty($finalTargetArr['industries5Arr']) || !empty($finalTargetArr['politics5Arr']) || !empty($finalTargetArr['life_events5Arr']) || !empty($finalTargetArr['behaviors5Arr'])) {
                            $param .= "{";
                            $tempstr = 0;
                            if (!empty($finalTargetArr['interestsArr5'])){
                                $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr5'])) . "]";
                                $tempstr = 1;
                            }
                            if(!empty($finalTargetArr['family_statuses5Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses5Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses5Arr'])) . "]";   
                                }
                            }
                            if(!empty($finalTargetArr['household_composition5Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition5Arr'])) . "]"; 
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition5Arr'])) . "]";
                                }
                            }
                            if(!empty($finalTargetArr['generation5Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation5Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation5Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['ethnic_affinity5Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity5Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity5Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['industries5Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries5Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries5Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['politics5Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics5Arr'])) . "]";   
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics5Arr'])) . "]"; 
                                }
                            }
                            if(!empty($finalTargetArr['life_events5Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events5Arr'])) . "]";  
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events5Arr'])) . "]";  
                                }
                            }
                            if(!empty($finalTargetArr['behaviors5Arr'])){
                                if($tempstr != 0){
                                    $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors5Arr'])) . "]";  
                                    $tempstr = 1;
                                }
                                else{
                                    $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors5Arr'])) . "]";
                                }
                            }
                            $param .= "}";
                        }
                        
                        $param .= "]";
                    }
                    else{
                        //split
                        if (!empty($interestsArr11) || !empty($family_statuses1Arr1) || !empty($household_composition1Arr1) || !empty($generation1Arr1) || !empty($ethnic_affinity1Arr1) || !empty($industries1Arr1) || !empty($politics1Arr1) || !empty($life_events1Arr1) || !empty($behaviors1Arr1)) {
                            
                            $param .= ", 'exclusions':{";
                            $tempstr = 0;
                            if (!empty($finalArr[$i][11])) {
                                $param .= "'interests':[" . $finalArr[$i][11] . "]";
                                $tempstr = 1; 
                            }
                            if(!empty($finalArr[$i][13])){
                                if($tempstr != 0){
                                    $param .= ",'family_statuses':[" . $finalArr[$i][13] . "]";
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'family_statuses':[" . $finalArr[$i][13] . "]";
                                }
                            }
                            if(!empty($finalArr[$i][14])){
                                if($tempstr != 0){
                                    $param .= ",'household_composition':[" . $finalArr[$i][14] . "]";
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'household_composition':[" . $finalArr[$i][14] . "]";   
                                }
                            }
                            if(!empty($finalArr[$i][15])){
                                if($tempstr != 0){
                                    $param .= ",'generation':[" . $finalArr[$i][15] . "]";
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'generation':[" . $finalArr[$i][15] . "]";
                                }
                            }
                            if(!empty($finalArr[$i][16])){
                                if($tempstr != 0){
                                    $param .= ",'ethnic_affinity':[" . $finalArr[$i][16] . "]";
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'ethnic_affinity':[" . $finalArr[$i][16] . "]";   
                                }
                            }
                            if(!empty($finalArr[$i][17])){
                                if($tempstr != 0){
                                    $param .= ",'industries':[" . $finalArr[$i][17] . "]";
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'industries':[" . $finalArr[$i][17] . "]";
                                }
                            }
                            if(!empty($finalArr[$i][18])){
                                if($tempstr != 0){
                                    $param .= ",'politics':[" . $finalArr[$i][18] . "]";
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'politics':[" . $finalArr[$i][18] . "]";
                                }   
                            }
                            if(!empty($finalArr[$i][19])){
                                if($tempstr != 0){
                                    $param .= ",'life_events':[" . $finalArr[$i][19] . "]";
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'life_events':[" . $finalArr[$i][19] . "]";
                                }
                            }
                            if(!empty($finalArr[$i][12])){
                                if($tempstr != 0){
                                    $param .= ",'behaviors':[" . $finalArr[$i][12] . "]";
                                    $tempstr = 1; 
                                }
                                else{
                                    $param .= "'behaviors':[" . $finalArr[$i][12] . "]";
                                }
                            }
                            $param .= "}";
                        }

                        $param .= ", 'flexible_spec':[";
                        $param .= "{";
                        $tempExploadArr = explode('$#$', $finalArr[$i][1]);
                        $tempstr = 0;
                        if($tempExploadArr[0] == 'interests'){
                            $param .= "'interests':[" . $tempExploadArr[1]."]";
                            $tempstr = 1; 
                        }
                        else if($tempExploadArr[0] == 'behaviors'){
                            if($tempstr == 1){
                                $param .= ",'behaviors':[" . $tempExploadArr[1]."]";
                            }
                            else{
                                $param .= "'behaviors':[" . $tempExploadArr[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr[0] == 'household_composition'){
                            if($tempstr == 1){
                                $param .= ",'household_composition':[" . $tempExploadArr[1]."]";
                            }
                            else{
                                $param .= "'household_composition':[" . $tempExploadArr[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr[0] == 'generation'){
                            if($tempstr == 1){
                                $param .= ",'generation':[" . $tempExploadArr[1]."]";
                            }
                            else{
                                $param .= "'generation':[" . $tempExploadArr[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr[0] == 'ethnic_affinity'){
                            if($tempstr == 1){
                                $param .= ",'ethnic_affinity':[" . $tempExploadArr[1]."]";
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . $tempExploadArr[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr[0] == 'industries'){
                            if($tempstr == 1){
                                $param .= ",'industries':[" . $tempExploadArr[1]."]";
                            }
                            else{
                                $param .= "'industries':[" . $tempExploadArr[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr[0] == 'politics'){
                            if($tempstr == 1){
                                $param .= ",'politics':[" . $tempExploadArr[1]."]";
                            }
                            else{
                                $param .= "'politics':[" . $tempExploadArr[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr[0] == 'family_statuses'){
                            if($tempstr == 1){
                                $param .= ",'family_statuses':[" . $tempExploadArr[1]."]";
                            }
                            else{
                                $param .= "'family_statuses':[" . $tempExploadArr[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr[0] == 'life_events'){
                            if($tempstr == 1){
                                $param .= ",'life_events':[" . $tempExploadArr[1]."]";
                            }
                            else{
                                $param .= "'life_events':[" . $tempExploadArr[1]."]";
                            }
                            $tempstr = 1;
                        }
                        $param .= "},{";
                        $tempExploadArr2 = explode('$#$', $finalArr[$i][2]);
                        $tempstr = 0;
                        if($tempExploadArr2[0] == 'interests'){
                            $param .= "'interests':[" . $tempExploadArr2[1]."]";
                            $tempstr = 1; 
                        }
                        else if($tempExploadArr2[0] == 'behaviors'){
                            if($tempstr == 1){
                                $param .= ",'behaviors':[" . $tempExploadArr2[1]."]";
                            }
                            else{
                                $param .= "'behaviors':[" . $tempExploadArr2[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr2[0] == 'household_composition'){
                            if($tempstr == 1){
                                $param .= ",'household_composition':[" . $tempExploadArr2[1]."]";
                            }
                            else{
                                $param .= "'household_composition':[" . $tempExploadArr2[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr2[0] == 'generation'){
                            if($tempstr == 1){
                                $param .= ",'generation':[" . $tempExploadArr2[1]."]";
                            }
                            else{
                                $param .= "'generation':[" . $tempExploadArr2[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr2[0] == 'ethnic_affinity'){
                            if($tempstr == 1){
                                $param .= ",'ethnic_affinity':[" . $tempExploadArr2[1]."]";
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . $tempExploadArr2[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr2[0] == 'industries'){
                            if($tempstr == 1){
                                $param .= ",'industries':[" . $tempExploadArr2[1]."]";
                            }
                            else{
                                $param .= "'industries':[" . $tempExploadArr2[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr2[0] == 'politics'){
                            if($tempstr == 1){
                                $param .= ",'politics':[" . $tempExploadArr2[1]."]";
                            }
                            else{
                                $param .= "'politics':[" . $tempExploadArr2[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr2[0] == 'family_statuses'){
                            if($tempstr == 1){
                                $param .= ",'family_statuses':[" . $tempExploadArr2[1]."]";
                            }
                            else{
                                $param .= "'family_statuses':[" . $tempExploadArr2[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr2[0] == 'life_events'){
                            if($tempstr == 1){
                                $param .= ",'life_events':[" . $tempExploadArr2[1]."]";
                            }
                            else{
                                $param .= "'life_events':[" . $tempExploadArr2[1]."]";
                            }
                            $tempstr = 1;
                        }
                        $param .= "},{";
                        $tempExploadArr3 = explode('$#$', $finalArr[$i][3]);
                        $tempstr = 0;
                        if($tempExploadArr3[0] == 'interests'){
                            $param .= "'interests':[" . $tempExploadArr3[1]."]";
                            $tempstr = 1; 
                        }
                        else if($tempExploadArr3[0] == 'behaviors'){
                            if($tempstr == 1){
                                $param .= ",'behaviors':[" . $tempExploadArr3[1]."]";
                            }
                            else{
                                $param .= "'behaviors':[" . $tempExploadArr3[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr3[0] == 'household_composition'){
                            if($tempstr == 1){
                                $param .= ",'household_composition':[" . $tempExploadArr3[1]."]";
                            }
                            else{
                                $param .= "'household_composition':[" . $tempExploadArr3[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr3[0] == 'generation'){
                            if($tempstr == 1){
                                $param .= ",'generation':[" . $tempExploadArr3[1]."]";
                            }
                            else{
                                $param .= "'generation':[" . $tempExploadArr3[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr3[0] == 'ethnic_affinity'){
                            if($tempstr == 1){
                                $param .= ",'ethnic_affinity':[" . $tempExploadArr3[1]."]";
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . $tempExploadArr3[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr3[0] == 'industries'){
                            if($tempstr == 1){
                                $param .= ",'industries':[" . $tempExploadArr3[1]."]";
                            }
                            else{
                                $param .= "'industries':[" . $tempExploadArr3[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr3[0] == 'politics'){
                            if($tempstr == 1){
                                $param .= ",'politics':[" . $tempExploadArr3[1]."]";
                            }
                            else{
                                $param .= "'politics':[" . $tempExploadArr3[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr3[0] == 'family_statuses'){
                            if($tempstr == 1){
                                $param .= ",'family_statuses':[" . $tempExploadArr3[1]."]";
                            }
                            else{
                                $param .= "'family_statuses':[" . $tempExploadArr3[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr3[0] == 'life_events'){
                            if($tempstr == 1){
                                $param .= ",'life_events':[" . $tempExploadArr3[1]."]";
                            }
                            else{
                                $param .= "'life_events':[" . $tempExploadArr3[1]."]";
                            }
                            $tempstr = 1;
                        }
                        $param .= "},{";
                        $tempExploadArr4 = explode('$#$', $finalArr[$i][4]);
                        $tempstr = 0;
                        if($tempExploadArr4[0] == 'interests'){
                            $param .= "'interests':[" . $tempExploadArr4[1]."]";
                            $tempstr = 1; 
                        }
                        else if($tempExploadArr4[0] == 'behaviors'){
                            if($tempstr == 1){
                                $param .= ",'behaviors':[" . $tempExploadArr4[1]."]";
                            }
                            else{
                                $param .= "'behaviors':[" . $tempExploadArr4[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr4[0] == 'household_composition'){
                            if($tempstr == 1){
                                $param .= ",'household_composition':[" . $tempExploadArr4[1]."]";
                            }
                            else{
                                $param .= "'household_composition':[" . $tempExploadArr4[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr4[0] == 'generation'){
                            if($tempstr == 1){
                                $param .= ",'generation':[" . $tempExploadArr4[1]."]";
                            }
                            else{
                                $param .= "'generation':[" . $tempExploadArr4[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr4[0] == 'ethnic_affinity'){
                            if($tempstr == 1){
                                $param .= ",'ethnic_affinity':[" . $tempExploadArr4[1]."]";
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . $tempExploadArr4[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr4[0] == 'industries'){
                            if($tempstr == 1){
                                $param .= ",'industries':[" . $tempExploadArr4[1]."]";
                            }
                            else{
                                $param .= "'industries':[" . $tempExploadArr4[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr4[0] == 'politics'){
                            if($tempstr == 1){
                                $param .= ",'politics':[" . $tempExploadArr4[1]."]";
                            }
                            else{
                                $param .= "'politics':[" . $tempExploadArr4[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr4[0] == 'family_statuses'){
                            if($tempstr == 1){
                                $param .= ",'family_statuses':[" . $tempExploadArr4[1]."]";
                            }
                            else{
                                $param .= "'family_statuses':[" . $tempExploadArr4[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr4[0] == 'life_events'){
                            if($tempstr == 1){
                                $param .= ",'life_events':[" . $tempExploadArr4[1]."]";
                            }
                            else{
                                $param .= "'life_events':[" . $tempExploadArr4[1]."]";
                            }
                            $tempstr = 1;
                        }
                        $param .= "},{";
                        $tempExploadArr5 = explode('$#$', $finalArr[$i][5]);
                        $tempstr = 0;
                        if($tempExploadArr5[0] == 'interests'){
                            $param .= "'interests':[" . $tempExploadArr5[1]."]";
                            $tempstr = 1; 
                        }
                        else if($tempExploadArr5[0] == 'behaviors'){
                            if($tempstr == 1){
                                $param .= ",'behaviors':[" . $tempExploadArr5[1]."]";
                            }
                            else{
                                $param .= "'behaviors':[" . $tempExploadArr5[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr5[0] == 'household_composition'){
                            if($tempstr == 1){
                                $param .= ",'household_composition':[" . $tempExploadArr5[1]."]";
                            }
                            else{
                                $param .= "'household_composition':[" . $tempExploadArr5[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr5[0] == 'generation'){
                            if($tempstr == 1){
                                $param .= ",'generation':[" . $tempExploadArr5[1]."]";
                            }
                            else{
                                $param .= "'generation':[" . $tempExploadArr5[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr5[0] == 'ethnic_affinity'){
                            if($tempstr == 1){
                                $param .= ",'ethnic_affinity':[" . $tempExploadArr5[1]."]";
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . $tempExploadArr5[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr5[0] == 'industries'){
                            if($tempstr == 1){
                                $param .= ",'industries':[" . $tempExploadArr5[1]."]";
                            }
                            else{
                                $param .= "'industries':[" . $tempExploadArr5[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr5[0] == 'politics'){
                            if($tempstr == 1){
                                $param .= ",'politics':[" . $tempExploadArr5[1]."]";
                            }
                            else{
                                $param .= "'politics':[" . $tempExploadArr5[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr5[0] == 'family_statuses'){
                            if($tempstr == 1){
                                $param .= ",'family_statuses':[" . $tempExploadArr5[1]."]";
                            }
                            else{
                                $param .= "'family_statuses':[" . $tempExploadArr5[1]."]";
                            }
                            $tempstr = 1;
                        }
                        else if($tempExploadArr5[0] == 'life_events'){
                            if($tempstr == 1){
                                $param .= ",'life_events':[" . $tempExploadArr5[1]."]";
                            }
                            else{
                                $param .= "'life_events':[" . $tempExploadArr5[1]."]";
                            }
                            $tempstr = 1;
                        }
                        $param .= "}";
                        $param .= "]";
                    }
                    //print_r($finalArr[$i][8]);
                    //print_r($custom_audiences1);
                    //exit;
                    if (!in_array('custom_audiences', $split_adset)){
                        if (!empty($Clevnt)) {
                            $param .= ",'custom_audiences':[" . $Clevnt . "]";
                        }
                    }
                    else{
                        if (!empty($custom_audiences1)) {
                            if(empty($custom_audiences1[0])){
                                 unset($custom_audiences1[0]);
                            }
                            if(!empty($finalArr[$i][6])){
                                $param .= ",'custom_audiences':[{'id':".$custom_audiences1[$finalArr[$i][8]]."}]";
                            }
                        }
                    }

                    if (!empty($relationship_statuses[0])) {
                        $param .= ",'relationship_statuses':[" . implode(',', $relationship_statuses) . "]";
                    }

                    if (!empty($interested_in)) {
                        $param .= ",'interested_in':[" . implode(',', $interested_in) . "]";
                    }

                    if (!empty($lng_locales)) {
                        $param .= ",'locales':[" . $lng_locales . "]";
                    }

                    if (!empty($education_statuses)) {
                        $param .= ",'education_statuses':[" . implode(',', $education_statuses) . "]";
                    }
                    if (!empty($education_schools)) {
                        $param .= ",'education_schools':[" . $education_schools . "]";
                    }
                    if (!empty($education_majors)) {
                        $param .= ",'education_majors':[" . $education_majors . "]";
                    }

                    if (!empty($work_employers)) {
                        $param .= ",'work_employers':[" . $work_employers . "]";
                    }

                    if (!empty($work_positions)) {
                        $param .= ",'work_positions':[" . $work_positions . "]";
                    }

                    if (!empty($mobile_device_users)) {

                        $user_device = array();
                        $user_os = array();
                        foreach ($mobile_device_users as $device) {
                            $dvc = explode('#', $device);
                            $user_device[] = $dvc[0];
                            $user_os[] = $dvc[1];
                        }
                        $param .= ",'user_device':['" . implode("','", array_unique($user_device)) . "']";
                        $param .= ",'user_os':['" . implode("','", array_unique($user_os)) . "']";
                    }

                    if (!in_array('placement', $split_adset)){
						$pub_pla = "";
						$dev_pla = "";
						$fbpos = "";
						$instapos = "";
						//print_r($page_types);
						//exit;
                        if (!empty($page_types[0])) {
							$pub_pla = "'facebook'";
							$dev_pla = "'desktop'";
							$fbpos = "'feed'";
                            //$param .= ",'device_platforms':['desktop'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                        }
                        if (!empty($page_types[1])) {
							if (strpos($pub_pla, 'facebook') !== false){
								
							}
							else{
								$pub_pla .= "'facebook'";
							}
							if ($dev_pla != ""){
								$dev_pla .= ",'mobile'";
							}
							else{
								$dev_pla = "'mobile'";
							}
							if (strpos($fbpos, 'feed') !== false){
								
							}
							else{
								$fbpos .= "'feed'";
							}
                            //$param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                        }
                        if (!empty($page_types[2])) {
							if($pub_pla == ""){
								$pub_pla .= "'facebook','audience_network'";
							}
							else if(strpos($pub_pla, 'facebook') !== false){
								$pub_pla .= ",'audience_network'";
							}
							if($dev_pla == ""){
								$dev_pla .= "'mobile'";
							}
							else if(strpos($dev_pla, 'mobile') !== false){
								
							}
							else{
								$dev_pla .= ",'mobile'";
							}
							
							if($fbpos == ""){
								$fbpos .= "'feed','instant_article'";
							}
							else if(strpos($fbpos, 'feed') !== false){
								$fbpos .= ",'instant_article'";
							}
							else{
								$fbpos .= ",'feed','instant_article'";
							}
							
                            //$param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed', 'instant_article']";
                        }
                        if (!empty($page_types[3])) {
							if (strpos($pub_pla, 'facebook') !== false){
								
							}
							else if($pub_pla == ""){
								$pub_pla .= "'facebook'";
							}
							else{
								$pub_pla .= ",'facebook'";
							}
							if($fbpos == ""){
								$fbpos .= "'right_hand_column'";
							}
							else{
								$fbpos .= ",'right_hand_column'";
							}
							
                            //$param .= ",'publisher_platforms':['facebook'],'facebook_positions':['right_hand_column']";
                        }
                        if (!empty($page_types[4]) && $optimize_for != "POST_ENGAGEMENT" && $optimize_for != "LEAD_GENERATION") {
							if($pub_pla == ""){
								$pub_pla .= "'instagram'";
							}
							else{
								$pub_pla .= ",'instagram'";
							}
                            //$param .= ",'publisher_platforms':['instagram']";
							$instapos = "'stream'";
                        }
						//$param .= ",";
						
						if($dev_pla != ""){
							$param .= ",'device_platforms':[".$dev_pla."]";
						}
						if($pub_pla != ""){
							$param .= ",'publisher_platforms':[".$pub_pla."]";
						}
						if($fbpos != ""){
							$param .= ",'facebook_positions':[".$fbpos."]";
						}
						if($instapos != ""){
							$param .= ",'instagram_positions':[".$instapos."]";
						}
                    }
                    else{
                        if($finalArr[$i][6] == 'DNF'){
                            $param .= ",'device_platforms':['desktop'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                        }
                        else if($finalArr[$i][6] == 'MNF'){
                            $param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                        }
                        else if($finalArr[$i][6] == 'TPMS'){
                            $param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed', 'instant_article']";
                        }
                        else if($finalArr[$i][6] == 'DRHS'){
                            $param .= ",'publisher_platforms':['facebook'],'facebook_positions':['right_hand_column']";
                        }
                        else if($finalArr[$i][6] == 'instagram'){
							
                            $param .= ",'publisher_platforms':['instagram']";
							
                        }
                    }
					
					/*$param .= ",'device_platforms':['mobile','desktop'],'publisher_platforms':['facebook','audience_network'],'facebook_positions'
:['feed','right_hand_column', 'instant_article']";*/
                    $param .= "}";
                  // print_r($page_types);print_r($param);exit;
                    //if (!empty($new_geo_locations[$al])) {
						$param = str_replace("Array", "", $param);
                        $postFields['targeting'] = $param;
						
						//print_r($postFields);
						
						//exit;
                    //}
                    #echo "<PRE>";print_r($param);exit;
                    //echo "<PRE>";print_r($postFields);exit;
					//echo "<pre>"; print_r($postFields); echo "</pre>"; exit;
                    $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->ad_account_id . '/adsets');
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt_array($ch, array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => $postFields
                    ));

                    $postFields['ad_account_id'] = $this->ad_account_id;
                    $this->writeLog('adset', $postFields);

                    $response = curl_exec($ch);
                    curl_close($ch);
                    $result = json_decode($response);

                    if (isset($result->error)) {
                        $response_result[$fas]['status'] = 'false';
                        $response_result[$fas]['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
                        break;
                    } else {
                        $response_result[$fas]['status'] = 'success';
                        $response_result[$fas]['message'] = $result->id;
                    }
                }
                else {
                    break;
                }
                sleep(3);
                $fas++;
            }
        }
        #echo "<PRE>";print_R($response_result);exit;
        return json_encode($response_result);
    }
    
    private function create_adset($data = NULL) {
        //Whole logic for multi adset, now need to test and apply
        //For age data
        if (!empty($data['form'])) {
            $age_min = $data['form'];
        } else {
            $age_min = 13;
        }
        if (!empty($data['to'])) {
            $age_max = $data['to'];
        } else {
            $age_max = 65;
        }
        $age_group = array($age_min . "#" . $age_max);
        for ($ai = 2; $ai <= 5; $ai++) {
            $frm = 'form' . $ai;
            $too = 'to' . $ai;
            if (!empty($data[$frm]) && !empty($data[$too])) {
                $age_group[] = $data[$frm] . "#" . $data[$too];
            }
        }

        $split_adset = $data['split_adset'];
        
        if (in_array('location', $split_adset)) { //Split at at location base
            $new_geo_locations = explode(",", trim($data['new_geo_locations'], ","));
        } else {
            $new_geo_locations = array($this->getProperLocationArray(trim($data['new_geo_locations'], ",")));
        }
                
        if (in_array('genders', $split_adset)) { //Split at at genders base
            $genders = array(1, 2);
        } else {
            if (!empty($data['genders'])) {
                $genders = array($data['genders']);
            } else {
                $genders = array('1,2');
            }
        }

        if (in_array('interests', $split_adset)) { //Split at at interest base
            $new_interests = explode(",", trim($data['new_interests1'], ","));
        } else {
            $new_interests = array(trim($data['new_interests1'], ","));
        }


        $custom_audiences = $data['custom_audiences'];

        if (!empty($custom_audiences)) {
            $ca_id = array();
            $ca_name = array();
            foreach ($custom_audiences as $ca) {
                $cain = explode('##', $ca);
                $ca_id[] = $cain[0];
                $ca_name[] = $cain[1];
            }
            $Clevnt = '';
            foreach ($ca_id as $le) {
                $Clevnt .= '{"id":' . $le . '},';
            }
        }

        if (in_array('custom_audiences', $split_adset)) { //Split at base of custom_audiences
            $new_customAudiences = explode(",", trim($Clevnt, ","));
        } else {
            $new_customAudiences = array(trim($Clevnt, ","));
        }


        $campaignobjective = $data['campaignobjective'];
        $new_exclude_locations = trim($data['new_exclude_locations'], ",");
        $relationship_statuses = $data['relationship_statuses'];
        $interested_in = $data['interested_in'];
        $lng_locales = trim($data['new_ad_langauage'], ",");
        $page_types = $data['page_types'];

        $education_schools = trim($data['new_education_schools'], ",");
        $education_majors = trim($data['new_education_majors'], ",");
        $work_employers = trim($data['new_work_employers'], ",");
        $work_positions = trim($data['new_work_positions'], ",");
        $mobile_device_users = $data['mobile_device_user'];
        $week_calendar = trim($data['week_calendar'], ",");

        $demographics = array('behaviors', 'life_events', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'family_statuses');
        foreach ($demographics as $dg) {
            if (isset($data[$dg]) && $data[$dg] != NULL) {
                $$dg = $data[$dg];
            }
        }
         $current_cur_code = $this->session->userdata('cur_code');
        $postFields = "";
        if($current_cur_code == "VND" || $current_cur_code == "TWD" || $current_cur_code == "CLP" || $current_cur_code == "COP" || $current_cur_code == "CRC" || $current_cur_code == "HUF" || $current_cur_code == "ISK" || $current_cur_code == "IDR" || $current_cur_code == "JPY" || $current_cur_code == "KRW" || $current_cur_code == "PYG"){
        
            $postFields = array(
                'access_token' => $this->access_token,
                'status' => 'ACTIVE',
                'campaign_id' => $data['Cmp_id'],
                $this->budget_type => $this->getBudgetAmount(),
                'billing_event' => $data['billing_event'],
                'optimization_goal' => $data['optimization_goal'],
                'start_time' => (new \DateTime($data['adset_start_date']))->format(\DateTime::ISO8601),
            );
        }
        else{
           $postFields = array(
                'access_token' => $this->access_token,
                'status' => 'ACTIVE',
                'campaign_id' => $data['Cmp_id'],
                $this->budget_type => $this->getBudgetAmount() / 0.01,
                'billing_event' => $data['billing_event'],
                'optimization_goal' => $data['optimization_goal'],
                'start_time' => (new \DateTime($data['adset_start_date']))->format(\DateTime::ISO8601),
            );
        }
        

        if ($data['adset_end_date'] != "") {
            $postFields['end_time'] = (new \DateTime($data['adset_end_date']))->format(\DateTime::ISO8601);
        }
        if ($campaignobjective == 'PAGE_LIKES') {
            $postFields['promoted_object'] = "{'page_id':'" . $this->creative_pageId . "'}";
        } elseif ($campaignobjective == 'CANVAS_APP_INSTALLS') {
            $storURl = 'https://apps.facebook.com/' . $data['creatives']['app_id'][0] . '/';
            $postFields['promoted_object'] = "{'application_id':'" . $data['creatives']['app_id'][0] . "','object_store_url':'" . $storURl . "'}";
        } elseif ($campaignobjective == 'OFFER_CLAIMS') {
            $offer_Id = explode('_', $data['offer_Id']);
            $postFields['promoted_object'] = "{'offer_id':'" . trim($offer_Id[1]) . "'}";
        } elseif ($campaignobjective == 'CONVERSIONS') {
            if ($data['pixel_id'] != '') {
                $postFields['promoted_object'] = "{'pixel_id':" . trim($data['pixel_id']) . "}";
            } else {
                $pixel_id = $this->create_new_tracking_pixel('Pixel-' . $data['adset_start_date'], $this->access_token, $this->ad_account_id);
                $postFields['promoted_object'] = "{'pixel_id':" . $pixel_id . "}";
            }
        } elseif ($campaignobjective == 'LEAD_GENERATION') {
            $postFields['promoted_object'] = "{'page_id':'" . $this->creative_pageId . "'}";
        }

        if (isset($data['bid_amount']) && $data['bid_amount'] > 0) {
            $postFields['bid_amount'] = $data['bid_amount'] / 0.01;
        } else {
           // $postFields['is_autobid'] = true;
            $postFields['bid_strategy'] = 'LOWEST_COST_WITHOUT_CAP';
            
        }


        if ($this->budget_type == 'lifetime_budget' && !empty($week_calendar)) {
		//if ($this->budget_type == 'daily_budget' && !empty($week_calendar)) {
            $week_calendar = $this->getWeekCalendar($week_calendar);
            $postFields['pacing_type'] = '["day_parting"]';
            $postFields['adset_schedule'] = "[" . $week_calendar . "]";
        }
        #echo "<PRE>";print_r($postFields);exit;


        $locationName = explode(",", trim($finalLocationStr, ','));
        $interestName = explode(",", $data['interests1']);
        $fas = 0; //Should be zero at each cost

        for ($ag = 0; $ag < count($genders); $ag++) {
            for ($al = 0; $al < count($new_geo_locations); $al++) {
                for ($an = 0; $an < count($new_interests); $an++) {
                    for ($aa = 0; $aa < count($age_group); $aa++) {
                        for ($ca = 0; $ca < count($new_customAudiences); $ca++) {
                                    if ($fas <= $this->adsetLmit) {
                                        //Repeted logic will be there
                                        //Gender type
                                        if ($genders[$ag] === '1,2') {
                                            $genderType = 'Both';
                                        } else {
                                            $genderType = ($genders[$ag] == 1 ? 'Male' : 'Female');
                                        }

                                        //age group
                                        $ageGroup = explode("#", explode(',', $age_group[$aa], 2)[0]);
                                        $data['form'] = $ageGroup[0];
                                        $data['to'] = $ageGroup[1];

                                        $postFields['name'] = explode(',', $locationName[$al], 2)[0] . ', ' . $data['form'] . " to " . $data['to'] . ", " . $genderType . ", " . explode(',', $interestName[$an], 2)[0] . ", " . $ca_name[$ca];

                                        $postFields['name'] = str_replace(", ,", ",", trim(trim(trim(trim($postFields['name']), ',')), ','));


                                        $param = '{';
                                        if (!empty($new_geo_locations[$al])) {
                                            $param .= '"geo_locations":{' . $new_geo_locations[$al] . '}';
                                        }
                                        if (!empty($new_exclude_locations)) {
                                            $new_exclude_locations = $this->getProperLocationArray($new_exclude_locations);
                                            $param .= ",'excluded_geo_locations': {" . $new_exclude_locations . "}";
                                        }
                                        if (!empty($genders[$ag])) {
                                            $param .= ",'genders':[" . $genders[$ag] . "]";
                                        }
                                        if (!empty($data['form'])) {
                                            $param .= ",'age_min':" . $data['form'];
                                        }
                                        if (!empty($data['to'])) {
                                            $param .= ",'age_max':" . $data['to'];
                                        }

                                        if (!empty($page_types)) {
                                            $param .= ",'page_types':['" . implode("','", $page_types) . "']";
                                        }
                                        if (!empty($new_interests[$an])) {
                                            $param .= ",'interests':[" . $new_interests[$an] . "]";
                                        }

                                        if (!empty($new_customAudiences[$ca])) {
                                            $param .= ",'custom_audiences':[" . $new_customAudiences[$ca] . "]";
                                        }

                                        if (!empty($relationship_statuses)) {
                                            $param .= ",'relationship_statuses':[" . implode(',', $relationship_statuses) . "]";
                                        }

                                        if (!empty($interested_in)) {
                                            $param .= ",'interested_in':[" . implode(',', $interested_in) . "]";
                                        }

                                        if (!empty($lng_locales)) {
                                            $param .= ",'locales':[" . $lng_locales . "]";
                                        }

                                        if (!empty($education_schools)) {
                                            $param .= ",'education_schools':[" . $education_schools . "]";
                                        }
                                        if (!empty($education_majors)) {
                                            $param .= ",'education_majors':[" . $education_majors . "]";
                                        }

                                        if (!empty($work_employers)) {
                                            $param .= ",'work_employers':[" . $work_employers . "]";
                                        }

                                        if (!empty($work_positions)) {
                                            $param .= ",'work_positions':[" . $work_positions . "]";
                                        }

                                        if (!empty($mobile_device_users)) {

                                            $user_device = array();
                                            $user_os = array();
                                            foreach ($mobile_device_users as $device) {
                                                $dvc = explode('#', $device);
                                                $user_device[] = $dvc[0];
                                                $user_os[] = $dvc[1];
                                            }
                                            $param .= ",'user_device':['" . implode("','", array_unique($user_device)) . "']";
                                            $param .= ",'user_os':['" . implode("','", array_unique($user_os)) . "']";
                                        }

                                        foreach ($demographics as $dg) {
                                            if (!empty($$dg)) {

                                                $$dg = explode(',', $$dg);
                                                $levnt = '';
                                                foreach ($$dg as $le) {
                                                    $levnt .= '{"id":' . $le . '},';
                                                }
                                                $param .= ",'" . $dg . "':[" . $levnt . "]";
                                            }
                                        }
                                        

                                        $param .= "}";

                                        if (!empty($new_geo_locations[$al])) {
                                            $postFields['targeting'] = $param;
                                        }
                                        #echo "<PRE>";print_r($postFields);
                                        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->ad_account_id . '/adsets');
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt_array($ch, array(
                                            CURLOPT_RETURNTRANSFER => true,
                                            CURLOPT_POST => true,
                                            CURLOPT_POSTFIELDS => $postFields
                                        ));

                                        $postFields['ad_account_id'] = $this->ad_account_id;
                                        $this->writeLog('adset', $postFields);

                                        $response = curl_exec($ch);
                                        curl_close($ch);
                                        $result = json_decode($response);

                                        if (isset($result->error)) {
                                            $response_result[$fas]['status'] = 'false';
                                            $response_result[$fas]['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
                                            break 5;
                                        } else {
                                            $response_result[$fas]['status'] = 'success';
                                            $response_result[$fas]['message'] = $result->id;
                                        }
                                    } else {
                                        break 5;
                                    }
                                    sleep(3);
                                    $fas++;
                        }
                    }
                }
            }
        }

        return json_encode($response_result);
    }

    private function save_fb_ad($adsetID, $creativID = NULL, $AdName = NULL) {
        //23842555257980160---23842555256910160---test page like ad 1
        if ($AdName != NULL) {
            $AdName = $AdName;
        } else {
            $AdName = $this->getCompaingname_1() . ' Ad';
        }

        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $this->ad_account_id . '/ads');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data_array = array(
            'access_token' => $this->access_token,
            'adset_id' => $adsetID,
            'creative' => '{"creative_id":' . $creativID . '}',
            'status' => 'ACTIVE',
            'name' => $AdName,
        );

        if ($this->input->post('track_fb_pixel') == 'Yes') {
            $fb_pixel = $this->getAdspixels();
            if ($fb_pixel) {
                $data_array2 = array(
                    'tracking_specs' => '{"action.type":"offsite_conversion","fb_pixel":"' . $fb_pixel . '"}',
                );
                $data_array = array_merge($data_array, $data_array2);
            }
        }
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data_array
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        $this->writeLog("save_fb_ad", $data_array);
        
        $result = json_decode($response);
        $response_result = array();
        if (isset($result->error)) {
            $response_result['status'] = 'false';
            $response_result['message'] = ($result->error->error_user_msg ) ? $result->error->error_user_msg : $result->error->message;
        } else {
            $response_result['status'] = 'success';
            $response_result['message'] = $result->id;
        }
        return json_encode($response_result);
    }

    public function create_new_tracking_pixel($name, $access_token, $ad_account_id, $tag) {
        $ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_' . $ad_account_id . '/offsitepixels');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'access_token' => $access_token,
                'name' => $name,
                'tag' => $tag
            )
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        //print_r($response);
        $resutl = json_decode($response);
        $this->writeLog("tracking_pixel", $resutl);
        return $resutl->id;
    }

    public function Savebudget() {
        global $data;
        $this->form_validation->set_rules('budgetAmount', 'Budget Amount', 'trim|xss_clean|required|min_length[1]');
        if ($this->input->post('asap') != 'on') {
            $this->form_validation->set_rules('adset_start_date', 'Start Date', 'trim|xss_clean|required|min_length[4]');
        }
        //$this->form_validation->set_rules('adset_end_date', 'End Date', 'trim|xss_clean|required|min_length[4]');
        if ($this->input->post('bid_strategy') == 'custom') {
            $this->form_validation->set_rules('bid_amount', 'Bid Amount', 'trim|xss_clean|required|min_length[1]');
        }


        if ($this->form_validation->run() === false) {
            echo "ValidationError@" . validation_errors();
            die();
        } else {
            if ($this->input->post('campaignobjective') == 'CONVERSIONS') {
                if ($this->input->post('pixel_id') === '' && $this->input->post('create_pixel_id') === '') {
                    // echo "ValidationError@ Please select Tracking Pixel or enter new Tracking Pixel name";
                    // die();
                }
            }
            echo "Successfully";
        }
    }

    public function get_pages_data() {

        $creatives = $this->input->post('creatives');
		$campID = $this->input->post('loadCampaing');
		
        $pageID = $creatives['object_id'][0];
        $loadCampaing = !empty($_POST['loadCampaing']) ? $_POST['loadCampaing'] : '';
        if ($this->input->post('campaignobjective') == 'PAGE_LIKES') {
            $this->get_pages_tabs($pageID);
        } elseif ($this->input->post('campaignobjective') == 'POST_ENGAGEMENT') {
            $this->get_pages_feed($pageID, $loadCampaing);
        } elseif ($this->input->post('campaignobjective') == 'LEAD_GENERATION') {
            $this->get_pages_leadgen_forms($pageID,$campID);
        } else {
            echo "NotAllowed";
        }
    }

    public function get_pages_leadgen_forms($pageID,$campID) {
        $pageaccess_token = $this->get_page_accesstoken($pageID);
		
			if(!empty($campID))
                    $draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $campID);
                
                $tempImg = "";
                if(!empty($draft_data)){
                    $tempImg = $draft_data->leadgenform;
                }
		
        //echo 'ggg'.$pageaccess_token;
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/{$pageID}/leadgen_forms?fields=follow_up_action_url,name,id&access_token=" . $pageaccess_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $counter = 1;
        echo "LEAD_GENERATION_OBJ@";
        echo '<option value="">Please select a form...</option>';
		 
        if (isset($response["data"]) && $response["data"] != '') {
            foreach ($response["data"] as $page_frm) {
				$selected = '';
                $Option_val = $page_frm['follow_up_action_url'] . "-$-" . $page_frm['id'];
				if($tempImg == $Option_val){
					$selected = '"selected"';
				}
                ?>
                <option value="<?= $Option_val ?>" <?= $selected ?>><?= $page_frm['name'] ?></option>
                <?php
                $counter++;
            }
        }
    }

    public function get_pages_tabs($pageID) {
        $pageaccess_token = $this->get_page_accesstoken($pageID);
        //echo $pageaccess_token;
        //die();
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/{$pageID}/tabs?access_token=" . $pageaccess_token;
		$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/{$pageID}/?fields=id,name,link&access_token=" . $pageaccess_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        var_dump($result);
        curl_close($ch);
        $response = json_decode($result, true);
        $counter = 1;
		//echo "<pre>"; print_r($response); echo "</pre>";
		//exit;
        echo "PAGE_LIKES_OBJ@";
		$link_url = $response['link'];	
		?>
        <input style="opacity:0;" checked="checked" type="radio" id="page_tab_url_<?= $counter ?>" name="creatives[page_tab_url]" value="<?= $link_url ?>" onchange="setLinkURL('page_tab_url_<?= $counter ?>');LoeadPreview();" />
                    <script> setLinkURL('page_tab_url_<?= $counter ?>');LoeadPreview();</script>
                    <a href="<?= $link_url ?>" target="_blank" class="icon ae-ico-linkout" style="opacity:0;"><span class="fa fa-external-link"></span></a>
        <?php
    }

    public function get_pages_feed($pageID, $loadCampaing) {
        
        $draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $loadCampaing);
        
        $pageaccess_token = $this->get_page_accesstoken($pageID);
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/{$pageID}/feed?fields=story,type,name,id,attachments{media}&limit=100&access_token=" . $pageaccess_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //($response);
        if (isset($response["data"]) && $response["data"] != '') {
            echo "POST_ENGAGEMENT_OBJ@";
            $counter = 1;
            echo '<option value="">Select Page Post</option>';
            foreach ($response["data"] as $page_feed) {
                $id = $page_feed['id'];
                //$id = explode("_", $id);
                if (isset($page_feed['name']) && !empty($page_feed['name'])) {
                    $name = $page_feed['name'];
                } elseif (isset($page_feed['story']) && !empty($page_feed['story'])) {
                    $name = isset($page_feed['story']) ? $page_feed['story'] : "";
                } elseif (isset($page_feed['message']) && !empty($page_feed['message'])) {
                    $name = $page_feed['message'];
                    //$story = isset($page_feed['message']) ? $page_feed['message'] : "";
                }
                elseif (isset($page_feed['type']) && !empty($page_feed['type'])) {
                    $name = $page_feed['type'];
                    //$story = isset($page_feed['message']) ? $page_feed['message'] : "";
                }
                if (!empty($name)) {
                    $sel = '';
                    if($draft_data->postid == $id){
                        $sel = 'selected';
                    }
                    $imageval = '';
                    if(isset($page_feed['attachments']['data'][0]['media']['image']['src'])){
                        $imageval = 'data-image="'.$page_feed['attachments']['data'][0]['media']['image']['src'].'"';
                    }
                    ?>
                    <option class="postimageselect" value="<?= isset($id) ? $id : "" ?>" <?php echo $sel; ?> <?php echo $imageval; ?>><?= isset($name) ? "&nbsp;&nbsp;".$name : "" ?></option>        
                    <?php
                }
                $counter++;
            }
        }
    }

    public function get_page_accesstoken($pageID) {
        // echo "aaaaaaa".$this->access_token;
        // die();
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/{$pageID}/?fields=access_token&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        $pageaccess_token = $response->access_token;
        return $pageaccess_token;
    }

    public function get_publish_page_accesstoken($pageID) {

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/{$pageID}/?fields=access_token,is_published&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        $pageaccess_token = $response->access_token;
        return $pageaccess_token;
    }

    public function getAdspixels() {

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $this->ad_account_id . "/adspixels?fields=name,id&access_token=" . $this->access_token;
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            return $response['data'][0]['id'];
        } else {
            return false;
        }
    }

    public function getUsersPages() {
        global $fb;
        $request = $fb->request('GET', '/me/accounts?type=page&limit=250');
        $request->setAccessToken($this->access_token);
        try {
            $response = $fb->getClient()->sendRequest($request);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphList();
        return $graphNode;
    }

    public function getAdInterest() {
        #$_REQUEST['q']  = 'parent';
		//consol.log(JSON.stringify($_REQUEST['q']));
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adinterest&limit=25&q=" . $_REQUEST['q'] . "&access_token=" . $this->access_token . '&limit=100';

        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();

        $i = 0;
        if(!empty($response['data'])){
            foreach ($response['data'] as $interest) {
                $arr[$i]['id'] = "interest_".$interest['id'];
                $arr[$i]['name'] = $interest['name']. '[Interests]';
                $i++;
            }   
        }

        $DemographicDataArr = $this->Spendcron_model->getDemographicData($_REQUEST['q']);
        if(!empty($DemographicDataArr)){
            foreach ($DemographicDataArr as $interest) {
                $arr[$i]['id'] = $interest['type']."_".$interest['item_id'];
                $arr[$i]['name'] = $interest['name']. '['.$interest['type'].']';
                $i++;
            }   
        }
        $json_response = json_encode($arr); 
        echo $json_response;
    }

    public function getAdLangauage() {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adlocale&q=" . $_REQUEST['q'] . "&access_token=" . $this->access_token . '&limit=100';
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        $response = json_decode($result, true);
        $arr = array();
        foreach ($response['data'] as $lang) {
            $arr[] = $lang;
        }
        $json_response = json_encode($arr);
        echo $json_response;
    }

    public function getAdEducation() {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adeducationschool&q=" . $_REQUEST['q'] . "&access_token=" . $this->access_token . '&limit=100';
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        foreach ($response['data'] as $edu) {
            $arr[] = $edu;
        }
        $json_response = json_encode($arr);
        echo $json_response;
    }

    public function getAdeducationmajor() {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adeducationmajor&q=" . $_REQUEST['q'] . "&access_token=" . $this->access_token . '&limit=100';
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        foreach ($response['data'] as $edu) {
            $arr[] = $edu;
        }
        $json_response = json_encode($arr);
        echo $json_response;
    }

    public function getAdworkemployer() {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adworkemployer&q=" . $_REQUEST['q'] . "&access_token=" . $this->access_token . '&limit=100';
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        foreach ($response['data'] as $edu) {
            $arr[] = $edu;
        }
        $json_response = json_encode($arr);
        echo $json_response;
    }

    public function getAdworkposition() {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adworkposition&q=" . $_REQUEST['q'] . "&access_token=" . $this->access_token . '&limit=100';
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        foreach ($response['data'] as $edu) {
            $arr[] = $edu;
        }
        $json_response = json_encode($arr);
        echo $json_response;
    }

    public function getAdgeolocation() {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adgeolocation&q=" . $_REQUEST['q'] . "&access_token=" . $this->access_token . "&limit=100";
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        $i = 0;
        
        if(!empty($response['data'])){
            foreach ($response['data'] as $interest) {
                $arr[$i]['key'] = $interest['key'];
                $arr[$i]['region_id'] = $interest['region_id'];
                $arr[$i]['name'] = $interest['name'].','.$interest['region'].','.$interest['country_name'].' ['.$interest['type'].']';
                $arr[$i]['type'] = $interest['type'];
                $arr[$i]['country_code'] = $interest['country_code'];
                $arr[$i]['country_name'] = $interest['country_name'];
                $arr[$i]['region'] = $interest['region'];
                $arr[$i]['supports_region'] = $interest['supports_region'];
                $arr[$i]['supports_city'] = $interest['supports_city'];
                $i++;
            }   
        }
        /*foreach ($response['data'] as $edu) {
            $arr[] = $edu;
        }*/
        $json_response = json_encode($arr);
        echo $json_response;
    }

    public function getAdgeoCountry($q) {

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adcountry&q=" . $q . "&access_token=" . $this->access_token . "&limit=100";
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        foreach ($response['data'] as $edu) {
            $arr[] = $edu;
        }
        $json_response = json_encode($arr);
        echo $json_response;
    }

    public function getAdgeoZipcode($q) {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adzipcode&q=" . $q . "&access_token=" . $this->access_token . "&limit=100";
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        foreach ($response['data'] as $edu) {
            $arr[] = $edu;
        }
        $json_response = json_encode($arr);
        echo $json_response;
    }

//By Nadeem
    function fileUpload() {
        $this->load->library('upload');

        //print_r($_FILES['fileinputs']['name']);
        $files = $_FILES;
        $cpt = count($_FILES['fileinputs']['name']);
        $p1 = $p2 = [];
        for ($i = 0; $i < $cpt; $i++) {
            $_FILES['fileinputs']['name'] = $files['fileinputs']['name'][$i];
            $_FILES['fileinputs']['type'] = $files['fileinputs']['type'][$i];
            $_FILES['fileinputs']['tmp_name'] = $files['fileinputs']['tmp_name'][$i];
            $_FILES['fileinputs']['error'] = $files['fileinputs']['error'][$i];
            $_FILES['fileinputs']['size'] = $files['fileinputs']['size'][$i];

            $this->upload->initialize($this->set_upload_options());

            if ($this->upload->do_upload('fileinputs')) {

                $data = $this->upload->data();
                $key = $data['file_name'];
                $viewurl = base_url() . "assets/campaign/" . $key;
                $delurl = site_url("createcampaign/deleteimage/" . $data['file_name']);

                $hasedImage = $this->getHashedImage($viewurl);

                $p1[$i] = "<img style='height:100px;width:100px;' src='{$viewurl}' class='file-preview-image' id='file-preview-image-" . $i . "'><input type='hidden' id='viewUrl1' name='creatives[viewUrl][]' value='" . $viewurl . "' />" . $hasedImage;
                $p2[$i] = ['caption' => 'Adset images', 'width' => '100px', 'url' => $delurl, 'key' => $key];
                // $p1[$i] = '';
            } else {
                echo $this->upload->display_errors();
            }
        }
        echo json_encode([
            'initialPreview' => $p1,
            'initialPreviewConfig' => $p2,
            'append' => true
        ]);
    }

    public function getHashedImage($imageUrl) {
        
        $adAccountId = $this->session->userdata('UserAdAccount');
        
        $imgHashUrl = '';

        if ($adAccountId) {
            $imgHash = $this->generateCreativeImg($adAccountId, $imageUrl);
            
            if ($imgHash) {
                //$imgHashUrl = "<input type='hidden' name='fbimghash[]' value='" . $imgHash . "' />";
                $imgHashUrl = $imgHash;
            }
        }

        return $imgHashUrl;
    }

    public function product1_img() {
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['img_pro1']['name']);
        $p1 = $p2 = [];
        $this->upload->initialize($this->set_upload_options());

        if ($this->upload->do_upload('img_pro1')) {

            $data = $this->upload->data();
            $key = $data['file_name'];
            $viewurl = base_url() . "assets/campaign/" . $key;
            $delurl = site_url("createcampaign/deleteimage/" . $data['file_name']);

            $hasedImage = $this->getHashedImage($viewurl);

            $p1[0] = "<img style='height:100px;width:100px;' src='{$viewurl}' class='file-preview-image' id='file-preview-image-1'>,<input type='hidden' id='product1_img_url' name='product1_img_url' value='" . $viewurl . "' />" . $hasedImage;
            $p2[0] = ['caption' => 'Adset images', 'width' => '100px', 'url' => $delurl, 'key' => $key];
        } else {
            echo $this->upload->display_errors();
        }
        echo json_encode([
            'initialPreview' => $p1,
            'initialPreviewConfig' => $p2,
            'append' => true
        ]);
    }

    public function product2_img() {
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['img_pro2']['name']);
        $p1 = $p2 = [];
        $this->upload->initialize($this->set_upload_options());

        if ($this->upload->do_upload('img_pro2')) {

            $data = $this->upload->data();
            $key = $data['file_name'];
            $viewurl = base_url() . "assets/campaign/" . $key;
            $delurl = site_url("createcampaign/deleteimage/" . $data['file_name']);

            $hasedImage = $this->getHashedImage($viewurl);

            $p1[0] = "<img style='height:100px;width:100px;' src='{$viewurl}' class='file-preview-image' id='file-preview-image-2'>,<input type='hidden' id='product2_img_url' name='product2_img_url' value='" . $viewurl . "' />" . $hasedImage;
            $p2[0] = ['caption' => 'Adset images', 'width' => '100px', 'url' => $delurl, 'key' => $key];
        } else {
            echo $this->upload->display_errors();
        }
        echo json_encode([
            'initialPreview' => $p1,
            'initialPreviewConfig' => $p2,
            'append' => true
        ]);
    }

    public function product3_img() {
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['img_pro3']['name']);
        $p1 = $p2 = [];
        $this->upload->initialize($this->set_upload_options());

        if ($this->upload->do_upload('img_pro3')) {

            $data = $this->upload->data();
            $key = $data['file_name'];
            $viewurl = base_url() . "assets/campaign/" . $key;
            $delurl = site_url("createcampaign/deleteimage/" . $data['file_name']);

            $hasedImage = $this->getHashedImage($viewurl);

            $p1[0] = "<img style='height:100px;width:100px;' src='{$viewurl}' class='file-preview-image' id='file-preview-image-3'>,<input type='hidden' id='product3_img_url' name='product3_img_url' value='" . $viewurl . "' />" . $hasedImage;
            $p2[0] = ['caption' => 'Adset images', 'width' => '100px', 'url' => $delurl, 'key' => $key];
        } else {
            echo $this->upload->display_errors();
        }
        echo json_encode([
            'initialPreview' => $p1,
            'initialPreviewConfig' => $p2,
            'append' => true
        ]);
    }

    public function offer_img_uplaod() {
        $this->load->library('upload');
        $files = $_FILES;
        $cpt = count($_FILES['offer_img']['name']);
        $p1 = $p2 = [];
        $this->upload->initialize($this->set_upload_options());

        if ($this->upload->do_upload('offer_img')) {

            $data = $this->upload->data();
            $key = $data['file_name'];
            $viewurl = base_url() . "assets/campaign/" . $key;
            $delurl = site_url("createcampaign/deleteimage/" . $data['file_name']);

            $hasedImage = $this->getHashedImage($viewurl);

            $p1[0] = "<img style='height:100px;width:100px;' src='{$viewurl}' class='file-preview-image' id='file-preview-image-3'>,<input type='hidden' id='offer_img_url' name='offer_img_url' value='" . $viewurl . "' />" . $hasedImage;
            $p2[0] = ['caption' => 'Offer images', 'width' => '100px', 'url' => $delurl, 'key' => $key];
        } else {
            echo $this->upload->display_errors();
        }
        echo json_encode([
            'initialPreview' => $p1,
            'initialPreviewConfig' => $p2,
            'append' => true
        ]);
    }

    private function set_upload_options() {
        //upload an image options
        $config = array();
        $config['upload_path'] = 'assets/campaign/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['encrypt_name'] = true;
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['overwrite'] = FALSE;
        return $config;
    }

    function deleteimage($file) {
        // if ($this->Listings_model->delete_listing_pictures($file)) {
        $success = @unlink(FCPATH . 'assets/campaign/' . $file);

        echo json_encode(array());
        // }
    }

    function get_fb_canvas_app($ad_account) {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $ad_account . "/connectionobjects?fields=id,name,type,supported_platforms,icon_url&access_token=" . $this->access_token . "&limit=100";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        foreach ($response['data'] as $connectionobjects) {
            //echo "<br />".$connectionobjects['name'];
            if ($connectionobjects['type'] == 2) {
                if (in_array("2", $connectionobjects['supported_platforms']))
                    $arr[] = $connectionobjects;
            }
        }
        if (!empty($arr)) {
            echo '<option value="">Select Apps</option>';
            foreach ($arr as $app) {
                echo '<option value="' . $app['id'] . '">' . $app['name'] . '</option>';
            }//end of foreach($arr as $page){
        }//end of  if(!empty($arr)){
    }

    public function get_fb_advertisable_pages($ad_account) {

        //Set adacount
        $this->session->set_userdata('UserAdAccount', $ad_account);

        $this->getCurrency();
        
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $ad_account . "/connectionobjects?fields=id,name,type,supported_platforms,picture&access_token=" . $this->access_token . "&limit=100";
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/me/accounts?fields=is_verified,is_published,name,id,picture{url,height,is_silhouette,width}&access_token=" . $this->access_token . "&limit=100";
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        
        $arr = array();
        if (isset($response['data']) && $response['data'] != NULL) {
            foreach ($response['data'] as $connectionobjects) {
                if ($connectionobjects['is_published'] == 1) {
                    $arr[] = $connectionobjects;
                }
            }
        }
        
        if (!empty($arr)) {
            echo '<option value="">Select Pages</option>';
            foreach ($arr as $page) {
                echo '<option value="' . $page['id'] . '">' . $page['name'] . '</option>';
            }//end of foreach($arr as $page){
        }//end of  if(!empty($arr)){
    }

    public function get_offsitepixels($ad_account) {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $ad_account . "/offsitepixels?fields=id,name,tag&access_token=" . $this->access_token . "&limit=100";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data']) && $response['data'] > 0) {
            echo '<option value="">Please select a pixel</option>';
            foreach ($response['data'] as $connectionobjects) {
                echo '<option value="' . $connectionobjects['id'] . '" >' . $connectionobjects['name'] . "-" . $connectionobjects['tag'] . '</option>';
            }
        }
    }

    public function getAdTargetingCategory() {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adTargetingCategory&class=behaviors&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        $str4 = '';
        $str3 = '';
        $str2 = '';
        $str1 = '';
        foreach ($response['data'] as $interest) {
            if (isset($interest['path']) && count($interest['path']) == 4) {
                $str4 .="'" . str_replace("'", '"', implode('/', $interest['path'])) . "=" . $interest['id'] . "=" . $interest['type'] . "'" . ",";
            }
            if (isset($interest['path']) && count($interest['path']) == 3) {
                $str3 .="'" . str_replace("'", '"', implode('/', $interest['path'])) . "=" . $interest['id'] . "=" . $interest['type'] . "'" . ",";
            }
            if (isset($interest['path']) && count($interest['path']) == 2) {
                $str2 .="'" . str_replace("'", '"', implode('/', $interest['path'])) . "=" . $interest['id'] . "=" . $interest['type'] . "'" . ",";
            }
            if (isset($interest['path']) && count($interest['path']) == 1) {
                $str1 .="'" . str_replace("'", '"', implode('/', $interest['path'])) . "=" . $interest['id'] . "=" . $interest['type'] . "'" . ",";
            }
        }
        echo $str4 . $str3 . $str2 . $str1;
        //$json_response = json_encode($arr);
        //echo $json_response;
    }

    public function getAdvanceDemographicTargetingCategory() {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/search/?type=adTargetingCategory&class=ethnic_affinity&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $arr = array();
        $str4 = '';
        $str3 = '';
        $str2 = '';
        $str1 = '';
        foreach ($response['data'] as $interest) {

            array_unshift($interest['path'], "Ethnic Affinity");
            if (isset($interest['path']) && count($interest['path']) == 4) {
                $str4 .="'" . str_replace("'", '"', implode('/', $interest['path'])) . "=" . $interest['id'] . "=" . $interest['type'] . "'" . ",";
            }
            if (isset($interest['path']) && count($interest['path']) == 3) {
                $str3 .="'" . str_replace("'", '"', implode('/', $interest['path'])) . "=" . $interest['id'] . "=" . $interest['type'] . "'" . ",";
            }
            if (isset($interest['path']) && count($interest['path']) == 2) {
                $str2 .="'" . str_replace("'", '"', implode('/', $interest['path'])) . "=" . $interest['id'] . "=" . $interest['type'] . "'" . ",";
            }
            if (isset($interest['path']) && count($interest['path']) == 1) {
                $str1 .="'" . str_replace("'", '"', implode('/', $interest['path'])) . "=" . $interest['id'] . "=" . $interest['type'] . "'" . ",";
            }
        }
        echo $str4 . $str3 . $str2 . $str1;
        //$json_response = json_encode($arr);
        //echo $json_response;
    }

    public function get_audience_size($ad_account) {
        //trim($data['new_interests'], ",");
        $optimize_for = $this->input->post('campaignobjective');

        if ($optimize_for == 'CONVERSIONS') {
            $optimize_for = 'OFFSITE_CONVERSIONS';
        } else if ($optimize_for == 'PRODUCT_CATALOG_SALES') {
            $optimize_for = 'OFFER_CLAIMS';
        } else if ($optimize_for == 'CANVAS_APP_INSTALLS') {
            $optimize_for = 'APP_INSTALLS';
        }

        $new_geo_locations = $this->getProperLocationArray(trim($this->input->post('new_geo_locations'), ","));
        $new_exclude_locations = trim($this->input->post('new_exclude_locations'), ",");
        $new_interests = trim($this->input->post('new_interests1'), ",");
        $genders = $this->input->post('genders');
        $age_min = $this->input->post('form');
        $age_max = $this->input->post('to');
        $page_types = $this->input->post('page_types');
        $relationship_statuses = $this->input->post('relationship_statuses');
        $interested_in = $this->input->post('interested_in');
        $lng_locales = trim($this->input->post('new_ad_langauage'), ",");
        $education_schools = trim($this->input->post('new_education_schools'), ",");
        $education_majors = trim($this->input->post('new_education_majors'), ",");
        $work_employers = trim($this->input->post('new_work_employers'), ",");
        $work_positions = trim($this->input->post('new_work_positions'), ",");
        $mobile_device_users = $this->input->post('mobile_device_user');
        //$behaviors = $this->input->post('more_behavious');

        $custom_audiences = $this->input->post('custom_audiences');

        if (!empty($custom_audiences)) {
            $ca_id = array();
            $ca_name = array();
            foreach ($custom_audiences as $ca) {
                $cain = explode('##', $ca);
                $ca_id[] = $cain[0];
                $ca_name[] = $cain[1];
            }
            $Clevnt = '';
            foreach ($ca_id as $le) {
                $Clevnt .= '{"id":' . $le . '},';
            }
            $Clevnt = trim($Clevnt, ",");
        }


        $demographics = array('behaviors', 'life_events', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'family_statuses');
        foreach ($demographics as $dg) {
            $$dg = $this->input->post($dg);
        }

        $param = 'optimize_for=' . $optimize_for;
        $param .= "&targeting_spec={'geo_locations': {" . $new_geo_locations . "},'genders':[" . $genders . "]";
        if (!empty($new_exclude_locations)) {
            $new_exclude_locations = $this->getProperLocationArray($new_exclude_locations);
            $param .= ",'excluded_geo_locations': {" . $new_exclude_locations . "}";
        }
        if (!empty($age_min)) {
            $param .= ",'age_min':" . $age_min;
        }
        if (!empty($age_max)) {
            $param .= ",'age_max':" . $age_max;
        }
        if (!empty($page_types)) {
            $param .= ",'page_types':['" . implode("','", $page_types) . "']";
        }
        if (!empty($new_interests)) {
            $param .= ",'interests':[" . $new_interests . "]";
        }

        if (!empty($Clevnt)) {
            $param .= ",'custom_audiences':[" . $Clevnt . "]";
        }

        if (!empty($relationship_statuses)) {
            $param .= ",'relationship_statuses':[" . implode(',', $relationship_statuses) . "]";
        }

        if (!empty($interested_in)) {
            $param .= ",'interested_in':[" . implode(',', $interested_in) . "]";
        }

        if (!empty($lng_locales)) {
            $param .= ",'locales':[" . $lng_locales . "]";
        }

        if (!empty($education_schools)) {
            $param .= ",'education_schools':[" . $education_schools . "]";
        }
        if (!empty($education_majors)) {
            $param .= ",'education_majors':[" . $education_majors . "]";
        }

        if (!empty($work_employers)) {
            $param .= ",'work_employers':[" . $work_employers . "]";
        }

        if (!empty($work_positions)) {
            $param .= ",'work_positions':[" . $work_positions . "]";
        }

        if (!empty($mobile_device_users)) {

            $user_device = array();
            $user_os = array();
            foreach ($mobile_device_users as $device) {
                $dvc = explode('#', $device);
                $user_device[] = $dvc[0];
                $user_os[] = $dvc[1];
            }
            $param .= ",'user_device':['" . implode("','", array_unique($user_device)) . "']";
            $param .= ",'user_os':['" . implode("','", array_unique($user_os)) . "']";
        }


        foreach ($demographics as $dg) {
            if (!empty($$dg)) {

                $$dg = explode(',', $$dg);
                $levnt = '';
                foreach ($$dg as $le) {
                    $levnt .= '{"id":' . $le . '},';
                }
                $param .= ",'" . $dg . "':[" . $levnt . "]";
            }
        }

        //print_r($this->input->post());
        //,"page_types":["desktopfeed"]
        $param .= "}";

        //Write log
        $this->writeLog('audience', $param);

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $ad_account . "/reachestimate?" . $param . "&access_token=" . $this->access_token;
        $url = str_replace("  ", "%20", $url);
        $url = str_replace(" ", "%20", $url);
        #echo $url;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            $audience = number_format($response['data']['users']) . " People";
        } else {
            $audience = "Sorry! something wrong";
        }

        echo $audience;
    }

    function writeLog($type, $data) {

        $file = "logs/" . $type . '.txt';
        $current = @file_get_contents($file);
        $current .= "\n=====================================" . Date('Y-m-d h:i:s') . "========================\n";
        if (is_array($data)) {
            $data['access_token'] = '';
            $current .= print_r($data, TRUE);
        } else {
            $current .= $data;
        }
        file_put_contents($file, $current);
    }

    function getProperLocationArray($string) {

        $res = explode(",", trim(trim($string), ","));
        if (count($res) > 0) {

            foreach ($res as $rs) {

                $key_values = explode(':', $rs, 2);
                $key = trim($key_values[0]);
                $new_value = trim(trim(trim($key_values[1]), '['), ']');

                if (isset($array[$key])) {
                    if (!is_array($array[$key]))
                        $array[$key] = (array) $array[$key];
                    $array[$key][] = $new_value;
                } else {
                    $array[$key] = $new_value;
                }
            }

            $stri = '';
            foreach ($array as $key => $val) {
                if (is_array($val)) {
                    $rst = implode(",", $val);
                } else {
                    $rst = $val;
                }
                $stri .= $key . ':[' . $rst . "]" . ",";
            }
            return trim(trim($stri), ',');
        } else {
            return $string;
        }
    }

    function getWeekCalendar($string) {

        $fnarry = explode(',', trim($string, ","));

        foreach ($fnarry as $r) {

            $key_values = explode('#', $r, 2);
            $key = trim($key_values[1]);
            $new_value = trim($key_values[0]);

            if (isset($array[$key])) {
                if (!is_array($array[$key]))
                    $array[$key] = (array) $array[$key];
                $array[$key][] = $new_value;
            } else {
                $array[$key] = $new_value;
            }
        }

        ksort($array);

        $str = '';
        foreach ($array as $key => $value) {
			$key1 = $key - 1;
            $s_m = $key1 * 240;
            $e_m = $s_m + 240;
            if (is_array($value)) {
                $rst = implode(",", $value);
            } else {
                $rst = $value;
            }
            $str .= "{'start_minute':" . $s_m . ",'end_minute':" . $e_m . ",'days':[" . $rst . "]},";
        }

        return trim($str, ",");
    }

    public function getSuggestions() {

        $dataArray = explode(',', $this->input->post('typeList'));
        $tempStr = '';
        for($i=0;$i<=count($dataArray)-1; $i++){
            $tempArr = explode('[', $dataArray[$i]);
            $tempStr .= $tempArr[0].',';
        }
        $tempStr1 = rtrim($tempStr,",");
        $dataArray = explode(',', $tempStr1);

        $interest_list = implode('","', array_unique($dataArray));
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/search?type=adinterestsuggestion&interest_list=["' . $interest_list . '"]&limit=10&access_token=' . $this->access_token;

        $url = str_replace("  ", "%20", $url);
        $url = str_replace(" ", "%20", $url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        //print_r($response);
        if (isset($response->data) && $response->data > 0) {
            foreach ($response->data as $suggestion) {
                ?>
                <div class="suggestion"><?= $suggestion->name ?>[Interest]<span> <?= number_format($suggestion->audience_size) ?> people </span><a href="javascript:void(0);" class="add" onClick="addSuggestion('<?= $suggestion->name ?>', '<?= $suggestion->id ?>', '<?= $_POST['id'] ?>', '<?= $_POST['counter'] ?>')">+</a></div>
                
                <?php
            }
        } else {
            //echo '<span><a href="javascript:void(0);"> No Suggestion </a></span>';
            
            echo '<div class="suggestion">No Suggestion</div>';
        }
    }
   
   public function saveAdcopy($data = NULL) {

        if ($data == NULL) {
            $data = $this->input->post();
        }
      
       
        $recordAlreadyExist = $this->Campaigndarft_model->checkadcopyalreadyexist($userId, $data['compaingname_1'], $data['campaignobjective']);
        
        if (!empty($recordAlreadyExist)) {
            $this->Campaigndarft_model->updateadcopy($recordAlreadyExist->ID, $data_arr);
            echo "The draft has been  saved successfully.";
        } else {
            $CreatedDate = date('Y-m-d H:i:s', time());
            $data['CreationTime'] = $CreatedDate;
            $this->Campaigndarft_model->saveadcopy($data);
            echo "The draft has been  saved successfully.";
        }
    }
   
   
   
   
    public function overllAllDrafts($data = NULL) {

        if ($data == NULL) {
            $data = $this->input->post();
        }
        $interests = '';
        $new_interests = '';
        if(!empty($data['interests'])){
            for ($i=0; $i <= count($data['interests'])-1 ; $i++) { 
                $interests .= $data['interests'][$i].'#$#';
                $new_interests .= $data['new_interests'][$i].'#$#';
            }
        }

        if (@$data['compaingname_1'] == NULL) {
            echo "Campaign name required";
            die();
        }

        $userId = $this->session->userdata['logged_in']['id'];
        if (isset($data['page_types'])) {
            $page_types = implode(', ', $data['page_types']);
        } else {
            $page_types = "";
        }
        if (!empty($data['relationship_statuses'])) {
            $relationship_statuses = implode(', ', $data['relationship_statuses']);
        } else {
            $relationship_statuses = NULL;
        }
        if (!empty($data['education_statuses'])) {
            $education_statuses = implode(', ', $data['education_statuses']);
        } else {
            $education_statuses = NULL;
        }

        if (!empty($data['interested_in'])) {
            $interested_in = implode(', ', $data['interested_in']);
        } else {
            $interested_in = NULL;
        }
        if (isset($data['new_education_schools'])) {
            $new_education_schools = implode(', ', $data['new_education_schools']);
        } else {
            $new_education_schools = NULL;
        }
        if (isset($data['mobile_device_user'])) {
            $mobile_device_user = implode(', ', $data['mobile_device_user']);
        } else {
            $mobile_device_user = NULL;
        }
        if(!empty($data['form2'])){
            $form2 = ",".implode(',', $data['form2']);
        }
        else{
            $form2 = '';   
        }
        if(!empty($data['to2'])){
            $to2 = ",".implode(',', $data['to2']);
        }
        else{
            $to2 = '';   
        }
        if(!empty($data['split_adset'])){
            $split_adset = ",".implode(',', $data['split_adset']);   
        }
        else{
            $split_adset = '';
        }
        if(!empty($data['behaviors'])){
            $behaviors = ",".implode(',',$data['behaviors']);
        }else{
            $behaviors = '';
        }
        $demographic = '';
        if(!empty($data['life_events'])){
            $demographic .= ",".implode(',',$data['life_events']);
        }else{
            $demographic .='';
        }if(!empty($data['politics'])){
            $demographic .=",".implode(',',$data['politics']);
        }else{
            $demographic.='';
        }if(!empty($data['industries'])){
            $demographic .=",".implode(',',$data['industries']);
        }else{
            $demographic .= '';
        }if(!empty($data['ethnic_affinity'])){
            $demographic .=",".implode(',',$data['ethnic_affinity']);
        }else{
            $demographic.='';
        }if(!empty($data['generation'])){
            $demographic .=",".implode(',',$data['generation']);
        }else{
            $demographic.='';
        }if(!empty($data['household_composition'])){
            $demographic .=",".implode(',',$data['household_composition']);
        }else{
            $demographic.='';
        }
        if(!empty($data['family_statuses'])){
            $demographic .=",".implode(',',$data['family_statuses']);
        }else{
            $demographic.='';
        }if(!empty($data['custom_audiences'])){
            //print_r($data['custom_audiences']); exit;
            if(empty($data['custom_audiences'][0])){
                 unset($data['custom_audiences'][0]);
            }
            $customAudi = "@".implode('@',$data['custom_audiences']);
        }else{
            $customAudi = '';
        }
        $data_arr = array(
            'userId' => $userId,
            'campaignname' => isset($data['compaingname_1']) ? $data['compaingname_1'] : "",
            'adaccount' => isset($data['creatives']['adaccountid'][0]) ? $data['creatives']['adaccountid'][0] : "",
            'adobjective' => isset($data['campaignobjective']) ? $data['campaignobjective'] : "",
            'imgType' => isset($data['imgType']) ? $data['imgType'] : "",
            'pageid' => isset($data['creatives']['object_id'][0]) ? $data['creatives']['object_id'][0] : "",
            'postid' => isset($data['creatives']['story_id']) ? $data['creatives']['story_id'] : "",
            'headlines1' => isset($data['creatives']['title'][0]) ? $data['creatives']['title'][0] : "",
            'headlines2' => isset($data['creatives']['title'][1]) ? $data['creatives']['title'][1] : "",
            'headlines3' => isset($data['creatives']['title'][2]) ? $data['creatives']['title'][2] : "",
            'headlines4' => isset($data['creatives']['title'][3]) ? $data['creatives']['title'][3] : "",
            'headlines5' => isset($data['creatives']['title'][4]) ? $data['creatives']['title'][4] : "",
            'adimage1' => isset($data['creatives']['viewUrl'][0]) ? basename($data['creatives']['viewUrl'][0]) : "",
            'adimage2' => isset($data['creatives']['viewUrl'][1]) ? basename($data['creatives']['viewUrl'][1]) : "",
            'adimage3' => isset($data['creatives']['viewUrl'][2]) ? basename($data['creatives']['viewUrl'][2]) : "",
            'adimage4' => isset($data['creatives']['viewUrl'][3]) ? basename($data['creatives']['viewUrl'][3]) : "",
            'adimage5' => isset($data['creatives']['viewUrl'][4]) ? basename($data['creatives']['viewUrl'][4]) : "",
            'fbimghash1' => isset($data['fbimghash'][0]) ? basename($data['fbimghash'][0]) : "",
            'fbimghash2' => isset($data['fbimghash'][1]) ? basename($data['fbimghash'][1]) : "",
            'fbimghash3' => isset($data['fbimghash'][2]) ? basename($data['fbimghash'][2]) : "",
            'fbimghash4' => isset($data['fbimghash'][3]) ? basename($data['fbimghash'][3]) : "",
            'fbimghash5' => isset($data['fbimghash'][4]) ? basename($data['fbimghash'][4]) : "",
            'adimagehash1' => isset($data['fbimghash1']) ? $data['fbimghash1'] : "",
            'adimagehash2' => isset($data['fbimghash2']) ? $data['fbimghash2'] : "",
            'adimagehash3' => isset($data['fbimghash3']) ? $data['fbimghash3'] : "",
            'adimagehash4' => "",
            'adimagehash5' => "",
            'adbody1' => isset($data['creatives']['body'][0]) ? $data['creatives']['body'][0] : "",
            'adbody2' => isset($data['creatives']['body'][1]) ? $data['creatives']['body'][1] : "",
            'adbody3' => isset($data['creatives']['body'][2]) ? $data['creatives']['body'][2] : "",
            'adbody4' => isset($data['creatives']['body'][3]) ? $data['creatives']['body'][3] : "",
            'adbody5' => isset($data['creatives']['body'][4]) ? $data['creatives']['body'][4] : "",
            'adurl1' => isset($data['creatives']['link_url'][0]) ? $data['creatives']['link_url'][0] : "",
            'adurl2' => isset($data['creatives']['link_url'][1]) ? $data['creatives']['link_url'][1] : "",
            'adurl3' => isset($data['creatives']['link_url'][2]) ? $data['creatives']['link_url'][2] : "",
            'adurl4' => isset($data['creatives']['link_url'][3]) ? $data['creatives']['link_url'][3] : "",
            'adurl5' => isset($data['creatives']['link_url'][4]) ? $data['creatives']['link_url'][4] : "",
            'linkdescription1' => isset($data['creatives']['link_description'][0]) ? $data['creatives']['link_description'][0] : "",
            'linkdescription2' => isset($data['creatives']['link_description'][1]) ? $data['creatives']['link_description'][1] : "",
            'linkdescription3' => isset($data['creatives']['link_description'][2]) ? $data['creatives']['link_description'][2] : "",
            'linkdescription4' => isset($data['creatives']['link_description'][3]) ? $data['creatives']['link_description'][3] : "",
            'linkdescription5' => isset($data['creatives']['link_description'][4]) ? $data['creatives']['link_description'][4] : "",
            'displaylink' => isset($data['creatives']['link_caption'][0]) ? $data['creatives']['link_caption'][0] : "",
            'calltoaction' => isset($data['creatives']['call_to_action'][0]) ? $data['creatives']['call_to_action3'][0] : "",
            'product1headline' => isset($data['headlines_pro1']) ? $data['headlines_pro1'] : "",
            'product2headline' => isset($data['headlines_pro2']) ? $data['headlines_pro2'] : "",
            'product3headline' => isset($data['headlines_pro3']) ? $data['headlines_pro3'] : "",
            'product4headline' => isset($data['headlines_pro4']) ? $data['headlines_pro4'] : "",
            'product5headline' => isset($data['headlines_pro5']) ? $data['headlines_pro5'] : "",
            'product1url' => isset($data['url_pro1']) ? $data['url_pro1'] : "",
            'product2url' => isset($data['url_pro2']) ? $data['url_pro2'] : "",
            'product3url' => isset($data['url_pro3']) ? $data['url_pro3'] : "",
            'product4url' => isset($data['url_pro4']) ? $data['url_pro4'] : "",
            'product5url' => isset($data['url_pro5']) ? $data['url_pro5'] : "",
            'product1description' => isset($data['desc_pro1']) ? $data['desc_pro1'] : "",
            'product2description' => isset($data['desc_pro2']) ? $data['desc_pro2'] : "",
            'product3description' => isset($data['desc_pro3']) ? $data['desc_pro3'] : "",
            'product4description' => isset($data['desc_pro4']) ? $data['desc_pro4'] : "",
            'product5description' => isset($data['desc_pro5']) ? $data['desc_pro5'] : "",
            'product1image' => isset($data['product1_img_url']) ? basename($data['product1_img_url']) : "",
            'product2image' => isset($data['product2_img_url']) ? basename($data['product2_img_url']) : "",
            'product3image' => isset($data['product3_img_url']) ? basename($data['product3_img_url']) : "",
            'product4image' => isset($data['product4_img_url']) ? basename($data['product4_img_url']) : "",
            'product5image' => isset($data['product5_img_url']) ? basename($data['product5_img_url']) : "",
            'callToAction_pro1' => isset($data['callToAction_pro1']) ? $data['callToAction_pro1'] : "",
            'callToAction_pro2' => isset($data['callToAction_pro2']) ? $data['callToAction_pro2'] : "",
            'callToAction_pro3' => isset($data['callToAction_pro3']) ? $data['callToAction_pro3'] : "",
            'productslink' => isset($data['products_link']) ? $data['products_link'] : "",
            'productslinkcaption' => isset($data['products_link_cap']) ? $data['products_link_cap'] : "",
            'pagetaburl' => isset($data['creatives']['page_tab_url'][0]) ? $data['creatives']['page_tab_url'][0] : "",
            'appid' => isset($data['creatives']['app_id'][0]) ? $data['creatives']['app_id'][0] : "", #####
            'calltoaction2' => isset($data['creatives']['call_to_action2'][0]) ? $data['creatives']['call_to_action3'][0] : "",
            'leadgenform' => isset($data['creatives']['lead_gen_form'][0]) ? $data['creatives']['lead_gen_form'][0] : "",
            'calltoaction3' => isset($data['creatives']['call_to_action3'][0]) ? $data['creatives']['call_to_action3'][0] : "", #
            'storyid' => isset($data['creatives']['story_id'][0]) ? $data['creatives']['story_id'][0] : "",
            'offertitle' => isset($data['offerTitle']) ? $data['offerTitle'] : "",
            'offerdescription' => isset($data['descriptionTitle']) ? $data['descriptionTitle'] : "",
            'offerexpiration' => isset($data['expiration_time']) ? $data['expiration_time'] : "",
            'claimlimit' => isset($data['custom_limit']) ? $data['custom_limit'] : "",
            'offerimage' => isset($data['offer_img_url']) ? basename($data['offer_img_url']) : "",
            //--------------------
            'includelocation' => isset($data['locations_autocomplete_include']) ? str_replace('],', ']$#$', $data['locations_autocomplete_include']) : "",
            'new_geo_locations' => isset($data['new_geo_locations']) ? str_replace('],', ']$#$', $data['new_geo_locations']) : "",
            'avoid_geo_locations' => isset($data['avoid_geo_locations']) ? str_replace(',', ']$#$', $data['avoid_geo_locations']) : "",
            'excludelocation' => isset($data['locations_autocomplete_exclude']) ? $data['locations_autocomplete_exclude'] : "",
            'language' => isset($data['locales_autocomplete']) ? $data['locales_autocomplete'] : "",
            'new_ad_langauage' => isset($data['new_ad_langauage']) ? $data['new_ad_langauage'] : "",
            'gender' => isset($data['genders']) ? $data['genders'] : "All",
            'agegroupfrom' => isset($data['form']) ? $data['form'].$form2 : "",
            'agegroupto' => isset($data['to']) ? $data['to'].$to2 : "",
            'interest' => trim($interests, '$#$'),
            'new_interests' => trim($new_interests, '$#$'),
            'interest_exclude' => isset($data['interests_exclude']) ? $data['interests_exclude'] : "",
            'new_interests_exclude' => isset($data['new_interests_exclude']) ? $data['new_interests_exclude'] : "",
            'placement' => $page_types,
            'realationship' => $relationship_statuses,
            'education' => $education_statuses,
            #'new_education_schools' => isset($data['new_education_schools_hidden']) ? $data['new_education_schools_hidden'] : "",
            #'new_education_id_schools' => isset($data['new_education_schools_id_hidden']) ? $data['new_education_schools_id_hidden'] : "",
            'mobiledeveiceuser' => $mobile_device_user,
            'interestedin' => $interested_in,
            'fieldsofstudy' => isset($data['std_autocomplete']) ? $data['std_autocomplete'] : "",
            'new_education_majors' => isset($data['new_education_majors_hidden']) ? $data['new_education_majors_hidden'] : "",
            'new_education_id_majors' => isset($data['new_education_majors_id_hidden']) ? $data['new_education_majors_id_hidden'] : "",
            'employers' => isset($data['employers_autocomplete']) ? $data['employers_autocomplete'] : "",
            'new_work_employers' => isset($data['new_work_employers_hidden']) ? $data['new_work_employers_hidden'] : "",
            'new_work_id_employers' => isset($data['new_work_employers_id_hidden']) ? $data['new_work_employers_id_hidden'] : "",
            'workpositions' => isset($data['job_autocomplete']) ? $data['job_autocomplete'] : "",
            'new_work_positions' => isset($data['new_work_positions_hidden']) ? $data['new_work_positions_hidden'] : "",
            'new_work_id_positions' => isset($data['new_work_positions_id_hidden']) ? $data['new_work_positions_id_hidden'] : "",
            'mobiledeviceuse' => "",
            'morebehaviours' => "",
            'moredemographic' => "",
            'behaviours'=>$behaviors,
            'demographic'=>$demographic,
            'custom_audience'=>$customAudi,
            'budgetammount' => isset($data['budgetAmount']) ? $data['budgetAmount'] : "",
            'budgettype' => isset($data['budget_type']) ? $data['budget_type'] : "",
            'biding' => isset($data['bid_amount']) ? $data['bid_amount'] : "Auto",
            'optimizationgoal' => isset($data['optimization_goal']) ? $data['optimization_goal'] : "",
            'billingevent' => isset($data['billing_event']) ? $data['billing_event'] : "",
            'trackingpixel' => isset($data['pixel_id']) ? $data['pixel_id'] : "",
            'desktoppreview' => isset($data['desktoppreview']) ? $data['desktoppreview'] : "",
            'mobilepreview' => isset($data['mobilepreview']) ? $data['mobilepreview'] : "",
            'righthandpreview' => isset($data['righthandpreview']) ? $data['righthandpreview'] : "",
            'track_fb_pixel' => isset($data['track_fb_pixel']) ? $data['track_fb_pixel'] : "",
            'track_google' => isset($data['track_google']) ? $data['track_google'] : "",
            'split_adset' => !empty($split_adset) ?  $split_adset : '',
            'campaign_day' => !empty($data['day']) ?  implode(',', $data['day']) : '',
            'campaign_time' => !empty($data['time']) ?  implode(',', $data['time']) : '',
            'conv_pixel' => !empty($data['pixel_id1']) ?  $data['pixel_id1'] : ''
        );
        $adset_start_date = isset($data['adset_start_date']) ? $data['adset_start_date'] : "";
        $adset_end_date = isset($data['adset_end_date']) ? $data['adset_end_date'] : "";
        if (!empty($adset_start_date)) {
            $adset_start_date = date('Y-m-d', strtotime($adset_start_date));
            $data_arr['startdate'] = $adset_start_date;
        }
        if (!empty($adset_end_date)) {
            $adset_end_date = date('Y-m-d', strtotime($adset_end_date));
            $data_arr['enddate'] = $adset_end_date;
        }
        $recordAlreadyExist = $this->Campaigndarft_model->recordAlreadyExist($userId, $data['compaingname_1'], $data['campaignobjective']);
        if (!empty($recordAlreadyExist)) {
            $this->Campaigndarft_model->updatecmpdraft($recordAlreadyExist->ID, $data_arr);
            echo "The draft has been  saved successfully.";
            //return 
        } else {
            $CreatedDate = date('Y-m-d H:i:s', time());
            $data_arr['CreationTime'] = $CreatedDate;
            $this->Campaigndarft_model->savecmpdraft($data_arr);
            echo "The draft has been  saved successfully.";
        }
    }

    public function loadDrafts() {
        global $data;
        //$drftdata = array ();
        $user_id = $this->input->post('user_id');
        $data->user_id = $this->input->post('user_id');
        //$data->getcmpdrfts = $this->Campaigndarft_model->getcmpdrfts($user_id);
        $data->getcmpdrfts = $this->Campaigndarft_model->getadcopy($user_id);
        //$this->load->views('loaddarfts', $data);
        echo json_encode($data);
        exit;
       // echo $this->load->view('loaddarfts', $data);
    }

    public function searchLoadDrafts() {
        global $data;
        //$drftdata = array ();
        $user_id = $this->input->post('user_id');
        $data->user_id = $this->input->post('user_id');
        $data->searchKeyword = $_POST['searchKeyword'];
        $getcmpdrfts = $this->Campaigndarft_model->getcmpdrfts1($user_id, $_POST['searchKeyword']);
        
        $str = '';
        $str .= '<div class="campaigns-list">';
            foreach ($getcmpdrfts as $getcmpdrft) {
                $str .= '<div class="campaign-item" id="row_'.$getcmpdrft->ID.'">';
                $str .= '<div class="campaign-left">';
                $str .= '<strong class="campaign-name"><a href="#">'.$getcmpdrft->campaignname.'</a></strong>';
                $str .= '<div class="meta">';
                $str .= '<span class="ico-holder"><i class="icon-single-user"></i></span>';
                $str .= '<span class="campaign-category">'.str_replace('_', ' ', $getcmpdrft->adobjective).'</span>';
                $str .= '<span class="time">created on <time datetime="2012-12-12">'.date('m/d/Y', strtotime($getcmpdrft->CreationTime)).'</time></span>';
                $str .= '</div>';
                $str .= '</div>';
                $str .= '<a href="'.base_url().'createcampaign/'.$getcmpdrft->ID.'" class="btn btn-load-campaign">LOAD CAMPAIGN</a>';
                $str .= '</div>';
            }
        $str .= '</div>';

        echo $str;exit;
    }

    public function delete_darft() {
        $darftId = $this->input->post('darftId');
        if ($this->Campaigndarft_model->deleteCmpDrft($darftId)) {
            echo "successfully";
        }
    }

    private function setAddAccountId($adAccountId) {
        $this->add_account_id = $adAccountId;
    }

    private function getAddAccountId() {
        return $this->add_account_id;
    }

    /* ADSET FUNCTIONS */

    function getCurlAdset($limit, $campaignId) {
        $adAccountId = "";
        $date_preset = "";
        $resultArray = array();
        $adAccountId = "act_" . $this->getAddAccountId();
        $addSetFields = $this->getAdsetFields();

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/adsets?fields=$addSetFields" . "&access_token=" . $this->access_token;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);


            //  $this->getPrintResult($response);
            if (isset($response['data'][0]['id'])) {
                $addSetId = $response['data'][0]['id'];
                $addsArray = $this->getCurlAdds($limit, $campaignId, $addSetId);
                $addsArray['response']['response']['addSetId'] = $addSetId;
                $response['addArray'] = $addsArray['response']['response'];
            }


            //$this->getPrintResult($addsArray['response']['response']);
            if (isset($response['data'])) {
                $addSetDataArray = $this->getAdsetFinalResult($campaignId, $response);
                if ($addSetDataArray) {
                    $resultArray = $addSetDataArray;
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => "No record found.",
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }

        return $resultArray;
    }

    function getAdsetFinalResult($campaignId, $addSetDataArray) {

        //$this->getPrintResult($addSetDataArray);
        $objectiveResponse = $this->getCampaignObjectiveCurl($campaignId);
        $campaingObjective = $objectiveResponse['objective'];
        $campaignName = $objectiveResponse['name'];
        $amount = "";
        $type = "";
        $endTime = "";
        if (isset($addSetDataArray['data'][0]['daily_budget']) && $addSetDataArray['data'][0]['daily_budget'] > 0) {
            $amount = $addSetDataArray['data'][0]['daily_budget'];
            $type = "daily_budget";
        }
        if (isset($addSetDataArray['data'][0]['lifetime_budget']) && $addSetDataArray['data'][0]['lifetime_budget'] > 0) {
            $amount = $addSetDataArray['data'][0]['lifetime_budget'];
            $type = "lifetime_budget";
        }

        if (isset($addSetDataArray['data'][0]['end_time'])) {
            $endTime = $addSetDataArray['data'][0]['end_time'];
        }

        $returnRow = (object) array(
                    "campaignname" => $campaignName,
                    "adaccount" => $this->getAddAccountId(),
                    "adobjective" => $campaingObjective,
                    "pageid" => $addSetDataArray['data'][0]['promoted_object']['page_id'],
                    "headlines1" => $addSetDataArray['addArray']['name'],
                    //"adimage1" => $addSetDataArray['addArray']['image_hash'],
                    "adbody1" => $addSetDataArray['addArray']['body'],
                    "language" => "",
                    "gender" => "",
                    "agegroupfrom" => $addSetDataArray['data'][0]['targeting']['age_min'],
                    "agegroupto" => $addSetDataArray['data'][0]['targeting']['age_max'],
                    "interest" => "",
                    "placement" => implode(', ', $addSetDataArray['data'][0]['targeting']['page_types']),
                    "realationship" => implode(', ', $addSetDataArray['data'][0]['targeting']['relationship_statuses']),
                    "education" => "",
                    "interestedin" => implode(', ', $addSetDataArray['data'][0]['targeting']['interested_in']),
                    "budgetammount" => $amount,
                    "budgettype" => $type,
                    "startdate" => $addSetDataArray['data'][0]['start_time'],
                    "enddate" => $endTime
        );

        //$this->getPrintResult($returnRow);
        return $returnRow;
    }

    function getAdsetFields() {

        $fields = "id,account_id,name,bid_amount,budget_remaining,campaign_id,created_time,daily_budget,end_time,lifetime_budget,start_time,promoted_object,product_ad_behaviour,billing_event,objective,configured_status,effective_status,targetingsentencelines{params,targetingsentencelines},targeting";

        return $fields;
    }

    function getCampaignObjectiveCurl($campaignId) {

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/?fields=name,objective,effective_status&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);
        return $response;
    }

    /* END ADSETS */

    /* ADDS FUNCTION */

    function getAdsFields() {

        $fields = ",id,account_id,name,bid_amount,budget_remaining,campaign_id,created_time,daily_budget,end_time,lifetime_budget,start_time,promoted_object,product_ad_behaviour,billing_event,configured_status,effective_status,targetingsentencelines{params,targetingsentencelines},targeting";

        return $fields;
    }

    function getCurlAdds($limit, $campaignId, $addSetId) {
        $adAccountId = "";
        $date_preset = "";
        $resultArray = array();
        $addSetFields = $this->getAdsFields();
        try {
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/ads?fields=insights$date_preset$addSetFields" . "&access_token=" . $this->access_token;

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            // $this->getPrintResult($response);
            if (isset($response['data'][0]['id'])) {
                $addId = $response['data'][0]['id'];
                $addArray = $this->getCurlAddOne($addId);
                $addArray['response']['addId'] = $addId;
            }

            //$this->getPrintResult($addArray['response']);
            if (isset($response['data'])) {
                $addSetDataArray = $addArray; //$this->getFinalResult($response['data'], $campaignId, $addSetId);
                if ($addSetDataArray) {
                    $resultArray = array(
                        "count" => 1,
                        "response" => $addSetDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => "No record found.",
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }

        return $resultArray;
    }

    /* END ADDS */

    /* ADDONE FUNCTIONS */

    function getAdFields() {

        $fields = ",id,account_id,name,bid_amount,campaign_id,created_time,configured_status,effective_status,creative,targetingsentencelines{params,targetingsentencelines},targeting";

        return $fields;
    }

    function getCurlAddOne($addId) {
        $adAccountId = "";
        $date_preset = "";
        $resultArray = array();


        $adFields = $this->getAdFields();
        try {
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/?fields=insights$date_preset$adFields" . "&access_token=" . $this->access_token;
            //exit; 

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            // $this->getPrintResult($response);
            if (isset($response['creative']['id'])) {
                $creative_id = $response['creative']['id'];
                $creativeArray = $this->getAdIfram($creative_id, "DESKTOP_FEED_STANDARD");
            }
            //$addSetDataArray['creative']['id'];
            //$this->getPrintResult($creativeArray);
            if (isset($response)) {
                $addSetDataArray = $creativeArray;
                if ($addSetDataArray) {
                    $resultArray = array(
                        "count" => 1,
                        "response" => $addSetDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => "No record found.",
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record within date.",
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }

        return $resultArray;
    }

    function getAdIfram($creative_id, $view) {
        $match = "";
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$creative_id/?fields=name,object_story_id,body,link_url,image_hash,call_to_action_type&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        return $response;
        // $this->getPrintResult($response);
    }

    public function countAds() {
        if ($this->input->post('campaignobjective') == 'LINK_CLICKS' || $this->input->post('campaignobjective') == 'CONVERSIONS') {

            $creatives = $this->input->post('creatives');


            $title = ($creatives['title'] ? $creatives['title'] : array(''));
            $link_description = ($creatives['link_description'] ? $creatives['link_description'] : array(''));
            $viewUrl = ($this->input->post('fbimghash') ? $this->input->post('fbimghash') : array(''));
            $body = ($creatives['body'] ? $creatives['body'] : array(''));
            $link_url = ($creatives['link_url'] ? $creatives['link_url'] : array(''));


            $f = 0; // Alwasy should be zero 
            for ($a = 0; $a < count($title); $a++) {
                for ($b = 0; $b < count($link_description); $b++) {
                    for ($c = 0; $c < count($viewUrl); $c++) {
                        for ($d = 0; $d < count($body); $d++) {
                            for ($e = 0; $e < count($link_url); $e++) {

                                if ($f <= $this->adsLimit) {
                                    $f++;
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($this->input->post('campaignobjective') == 'PAGE_LIKES') {


            $title = ($data['creatives']['title'] ? $data['creatives']['title'] : array(''));
            $viewUrl = ($data['fbimghash'] ? $data['fbimghash'] : array(''));
            $body = ($data['creatives']['body'] ? $data['creatives']['body'] : array(''));

            $f = 0; // Alwasy should be zero 
            for ($a = 0; $a < count($title); $a++) {
                for ($b = 0; $b < count($viewUrl); $b++) {
                    for ($c = 0; $c < count($body); $c++) {
                        if ($f <= $this->adsLimit) {
                            $f++;
                        }
                    }
                }
            }
        } elseif ($this->input->post('campaignobjective') == 'POST_ENGAGEMENT') {
            $f = 1;
        } elseif ($this->input->post('campaignobjective') == 'PRODUCT_CATALOG_SALES') {
            $f = 1;
        } elseif ($this->input->post('campaignobjective') == 'CANVAS_APP_INSTALLS') {
            $f = 1;
        } elseif ($this->input->post('campaignobjective') == 'OFFER_CLAIMS') {
            $f = 1;
        } elseif ($this->input->post('campaignobjective') == 'LEAD_GENERATION') {


            $title = ($data['creatives']['title'] ? $data['creatives']['title'] : array(''));
            $link_description = ($data['creatives']['link_description'] ? $data['creatives']['link_description'] : array(''));
            $viewUrl = ($data['fbimghash'] ? $data['fbimghash'] : array(''));
            $body = ($data['creatives']['body'] ? $data['creatives']['body'] : array("") );

            $f = 0; // Alwasy should be zero 
            for ($a = 0; $a < count($title); $a++) {
                for ($b = 0; $b < count($link_description); $b++) {
                    for ($c = 0; $c < count($viewUrl); $c++) {
                        for ($d = 0; $d < count($body); $d++) {

                            if ($f <= $this->adsLimit) {
                                $f++;
                            }
                        }
                    }
                }
            }
        }

        echo $f;
    }

    function countAdset() {
        $data = $this->input->post();
        if (!empty($data['form'])) {
            $age_min = $data['form'];
        } else {
            $age_min = 13;
        }
        if (!empty($data['to'])) {
            $age_max = $data['to'];
        } else {
            $age_max = 65;
        }
        $age_group = array($age_min . "#" . $age_max);
        for ($ai = 2; $ai <= 5; $ai++) {
            $frm = 'form' . $ai;
            $too = 'to' . $ai;
            if (!empty($data[$frm]) && !empty($data[$too])) {
                $age_group[] = $data[$frm] . "#" . $data[$too];
            }
        }

        $split_adset = $data['split_adset'];

        if (in_array('location', $split_adset)) { //Split at at location base
            $new_geo_locations = explode(",", trim($data['new_geo_locations'], ","));
        } else {
            $new_geo_locations = array($this->getProperLocationArray(trim($data['new_geo_locations'], ",")));
        }

        if (in_array('genders', $split_adset)) { //Split at at genders base
            $genders = array(1, 2);
        } else {
            if (!empty($data['genders'])) {
                $genders = array($data['genders']);
            } else {
                $genders = array('1,2');
            }
        }

        if (in_array('interests', $split_adset)) { //Split at at interest base
            $new_interests = explode(",", trim($data['new_interests1'], ","));
        } else {
            $new_interests = array(trim($data['new_interests1'], ","));
        }

        $custom_audiences = $data['custom_audiences'];

        if (!empty($custom_audiences)) {
            $ca_id = array();
            $ca_name = array();
            foreach ($custom_audiences as $ca) {
                $cain = explode('##', $ca);
                $ca_id[] = $cain[0];
                $ca_name[] = $cain[1];
            }
            $Clevnt = '';
            foreach ($ca_id as $le) {
                $Clevnt .= '{"id":' . $le . '},';
            }
        }

        if (in_array('custom_audiences', $split_adset)) { //Split at base of custom_audiences
            $new_customAudiences = explode(",", trim($Clevnt, ","));
        } else {
            $new_customAudiences = array(trim($Clevnt, ","));
        }

        $fas = 0; //Should be zero at each cost

        for ($ag = 0; $ag < count($genders); $ag++) {
            for ($al = 0; $al < count($new_geo_locations); $al++) {
                for ($an = 0; $an < count($new_interests); $an++) {
                    for ($aa = 0; $aa < count($age_group); $aa++) {
                        for ($ca = 0; $ca < count($new_customAudiences); $ca++) {
                            if ($fas <= $this->adsetLmit) {
                                $fas++;
                            }
                        }
                    }
                }
            }
        }

        echo $fas;
    }

    function customAudiences() {

        $adAccountId = $this->input->post('adAccountId');
        $adAccountId = 'act_' . $adAccountId;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/customaudiences?fields=id,name,approximate_count&limit=200&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $response = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($response);
        $option = '';
        if (isset($results->error)) {
            // $option = "<option value=''>No Record Found!</option>";
        } else {
            $option = "<option value=\"\" selected>CUSTOM AUDIENCE</option>";
            if ($results->data) {
                foreach ($results->data as $result) {
                    $option .= "<option value='" . $result->id . "##" . $result->name . "'>" . $result->name . "</option>";
                }
            }
        }

        echo $option;
    }

    /* END ADDONE */
    
    function fngetallimages(){
        $userId = $_POST['userId'];
        $Id = $_POST['Id'];
        $adAccountId = $this->input->post('adAccountId');
        $adAccountId = 'act_' . $adAccountId;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/adimages?fields=name,hash,id,url,permalink_url,original_height,original_width,status,created_time,account_id&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        
        $response = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($response);
        if (isset($results->error)) {
            // $option = "<option value=''>No Record Found!</option>";
        } else {
            if ($results->data) {
                
                if(!empty($_POST['campaignId']))
                    $draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $_POST['campaignId']);
                
                $tempImg = array();
                if(!empty($draft_data)){
                    $tempImg[] = $draft_data->adimage1; 
                    $tempImg[] = $draft_data->adimage2; 
                    $tempImg[] = $draft_data->adimage3; 
                    $tempImg[] = $draft_data->adimage4; 
                    $tempImg[] = $draft_data->adimage5; 
                    $tempImg[] = $draft_data->product1image; 
                    $tempImg[] = $draft_data->product2image; 
                    $tempImg[] = $draft_data->product3image; 
                }
                
                $labelinc=0;
                
                foreach ($results->data as $result) {
                    $labelinc++;
                    $imgArr = explode('/', $result->url);
                    $imgApp1 = explode('.', end($imgArr));
                    $temp = $imgApp1[0].rand();
                    $tempName = $imgApp1[0].'-'.$Id;
                    $class = '';
					$scriptcall = '';
                    if(in_array(end($imgArr), $tempImg)){
                        //$class = 'imgSelect';
						$scriptcall = '<script type="text/javascript">fnSelectGalleryImg(\''.$result->hash.'\', \''.$userId.'\', \''.$Id.'\', \''.end($imgArr).'\');</script>';
                    }
					
					$str .= '<div class="media-item '.$_POST['campaignId'].'">
															<input id="img-'.$labelinc.'" type="checkbox">
															<label for="img-'.$labelinc.'" onclick="fnSelectGalleryImg(\''.$result->hash.'\', \''.$userId.'\', \''.$Id.'\', \''.end($imgArr).'\')">
																<span class="img-holder">
																	<img class="'.$Id.' '.$class.'" src="'.$result->url.'" alt="image description" id="'.$result->hash.'-'.$Id.'">
																	
																	
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">IMAGE</span>
																	<time datetime="2017-06-01">01 Jun 2017</time>
																</span>
															</label>
														</div>';
                    //$str .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryImg(\''.$result->hash.'\', \''.$userId.'\', \''.$Id.'\', \''.end($imgArr).'\')"><img id="'.$result->hash.'-'.$Id.'"  class="'.$Id.' img-responsive img-thumbnail '.$class.'" src="'.$result->url.'" /></a></div>'.$scriptcall;
                }
            }
        }
        
        /*$userId = $_POST['userId'];
        $Id = $_POST['Id'];
        $storeFolder = 'uploads/campaign/';   //2
        $uploadDirPath2 = $storeFolder.$userId."/";
        
        $images = glob($uploadDirPath2."*.*");
        
        $str = '';
        foreach($images as $image) {
            $imgArr = explode('/', $image);
            $imgApp1 = explode('.', end($imgArr));
            $temp = $imgApp1[0].rand();
            $str .= '<div align="center" class="col-md-2 col-sm-6"><a class="gallery-img" onclick="fnSelectGalleryImg(\''.$temp.'\', \''.$userId.'\', \''.$Id.'\', \''.end($imgArr).'\')"><img id="'.$temp.'"  class="img-responsive img-thumbnail" src="'.base_url().$image.'" /></a></div>';
        }*/
        echo $str;exit;
    }
    
    function combinations($arrays, $i = 0) {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }

        // get combinations from subsequent arrays
        $tmp = $this->combinations($arrays, $i + 1);

        $result = array();

        // concat each array from tmp with each element from $arrays[$i]
        #echo "<pre>"; print_r($arrays); 
        #echo "<pre>"; print_r($tmp); 
        #exit;
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ? 
                    array_merge(array($v), $t) :
                    array($v, $t);
            }
        }
        #echo "<pre>"; print_r($result);
        return $result;
    }
    
    function createAudienceTable(){

        $split_adset = $_POST['split_adset'];
        
		$locationcount = "0"; 
		$interestcount = "0";
		$intarrcount = 0;
		
        $optimize_for = $this->input->post('campaignobjective');
        if ($optimize_for == 'CLICK') {
            $optimize_for = 'CLICKS';
        }
        else if ($optimize_for == 'CONVERSIONS') {
            $optimize_for = 'OFFSITE_CONVERSIONS';
        } else if ($optimize_for == 'PRODUCT_CATALOG_SALES') {
            $optimize_for = 'OFFER_CLAIMS';
        } else if ($optimize_for == 'CANVAS_APP_INSTALLS') {
            $optimize_for = 'APP_INSTALLS';
        }
        
        $locationTemp = explode('],', $_POST['locations_autocomplete_include']);
        $finalLocationStr = '';
        if(!empty($locationTemp)){
            for($i=0; $i<=count($locationTemp)-1; $i++){
                $tempStr = explode(',', $locationTemp[$i]);
                $finalLocationStr .= $tempStr[0].',';
            }
        }

        $locationTemp = explode(',', trim($finalLocationStr, ','));
        $new_geo_locations = trim($this->input->post('new_geo_locations'), ",");
        $new_geo_temp = explode(',', $new_geo_locations);
        
        $i = 0;
        if (count($new_geo_temp) > 0) {
            foreach ($new_geo_temp as $rs) {
                if (isset($locationArr[$locationTemp[$i]])) {
                    if (!is_array($array[$key]))
                        $locationArr[$locationTemp[$i]] = (array) $locationArr[$locationTemp[$i]];
                    $locationArr[$locationTemp[$i]][] = $rs;
                } else {
                    $locationArr[$locationTemp[$i]] = $rs;
                }
                $i++;
            }
        }
		//echo "junaid"; print_r($_POST['interests']); print_r(count($_POST['interests'])); exit;
        $finalTargetArr = array();
		
		
		
		
        for($i1=0; $i1<=count($_POST['interests'])-1; $i1++){
            if(!empty($_POST['interests'][$i1])){
                $interestsTempArr = explode(',', $_POST['interests'][$i1]);
    			$intarrcount = $intarrcount + count($interestsTempArr);
                //$new_interests = trim($this->input->post('new_interests'.$i1), ",");
                $new_interests = trim($_POST['new_interests'][$i1], ",");
                $new_interests_temp = explode(',', $new_interests);
                $life_events1Arr = array();
                $family_statuses1Arr = array();
                $politics1Arr = array();
                $industries1Arr = array();
                $ethnic_affinity1Arr = array();
                $generation1Arr = array();
                $household_composition1Arr = array();
                $behaviors1Arr = array();
                $interestsArr1 = array();
    
                $i = 0;
                if (count($new_interests_temp) > 0) {
                    if (!empty($new_interests_temp[0])) {
                        foreach ($new_interests_temp as $rs) {
                            $e = explode('_', $rs);
                            $e1 = explode(':', $e[0]);
                            if($e1[1] == 'life'){
                                if (isset($life_events1Arr[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $life_events1Arr[$interestsTempArr[$i]] = (array) $life_events1Arr[$interestsTempArr[$i]];
                                    $life_events1Arr[$interestsTempArr[$i]][] = 'life_events$#${"id":'.str_replace('}', '', $e[2]).'}';
                                } else {
                                    $life_events1Arr[$interestsTempArr[$i]] = 'life_events$#${"id":'.str_replace('}', '', $e[2]).'}';//str_replace('}', '', $e[1]);
                                }
                                $life_eventsArr = 'life_events'.($i1+1).'Arr';
                                $finalTargetArr[$life_eventsArr] = $life_events1Arr;
                                $i++;
                            }
                            else if($e1[1] == 'family'){
                                
                                if (isset($family_statuses1Arr[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $family_statuses1Arr[$interestsTempArr[$i]] = (array) $family_statuses1Arr[$interestsTempArr[$i]];
                                    $family_statuses1Arr[$interestsTempArr[$i]][] = 'family_statuses$#${"id":'.str_replace('}', '', $e[2]).'}';
                                } else {
                                    $family_statuses1Arr[$interestsTempArr[$i]] = 'family_statuses$#${"id":'.str_replace('}', '', $e[2]).'}';//str_replace('}', '', $e[1]);
                                }
                                $family_statusesArr = 'family_statuses'.($i1+1).'Arr';
                                $finalTargetArr[$family_statusesArr] = $family_statuses1Arr;
                                $i++;
                            }
                            else if($e1[1] == 'politics'){
                                
                                if (isset($politics1Arr[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $politics1Arr[$interestsTempArr[$i]] = (array) $politics1Arr[$interestsTempArr[$i]];
                                    $politics1Arr[$interestsTempArr[$i]][] = 'politics$#${"id":'.str_replace('}', '', $e[1]).'}';
                                } else {
                                    $politics1Arr[$interestsTempArr[$i]] = 'politics$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                }
                                $politicsArr = 'politics'.($i1+1).'Arr';
                                $finalTargetArr[$politicsArr] = $politics1Arr;
                                $i++;
                            }
                            else if($e1[1] == 'industries'){
                                
                                if (isset($industries1Arr[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $industries1Arr[$interestsTempArr[$i]] = (array) $industries1Arr[$interestsTempArr[$i]];
                                    $industries1Arr[$interestsTempArr[$i]][] = 'industries$#${"id":'.str_replace('}', '', $e[1]).'}';
                                } else {
                                    $industries1Arr[$interestsTempArr[$i]] = 'industries$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                }
                                $industriesArr = 'industries'.($i1+1).'Arr';
                                $finalTargetArr[$industriesArr] = $industries1Arr;
                                $i++;
                            }
                            else if($e1[1] == 'ethnic_affinity'){
                                
                                if (isset($ethnic_affinity1Arr[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $ethnic_affinity1Arr[$interestsTempArr[$i]] = (array) $ethnic_affinity1Arr[$interestsTempArr[$i]];
                                    $ethnic_affinity1Arr[$interestsTempArr[$i]][] = 'ethnic_affinity$#${"id":'.str_replace('}', '', $e[1]).'}';
                                } else {
                                    $ethnic_affinity1Arr[$interestsTempArr[$i]] = 'ethnic_affinity$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                }
                                $ethnic_affinityArr = 'ethnic_affinity'.($i1+1).'Arr';
                                $finalTargetArr[$ethnic_affinityArr] = $ethnic_affinity1Arr;
                                $i++;
                            }
                            else if($e1[1] == 'generation'){
                                
                                if (isset($generation1Arr[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $generation1Arr[$interestsTempArr[$i]] = (array) $generation1Arr[$interestsTempArr[$i]];
                                    $generation1Arr[$interestsTempArr[$i]][] = 'generation$#${"id":'.str_replace('}', '', $e[1]).'}';
                                } else {
                                    $generation1Arr[$interestsTempArr[$i]] = 'generation$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                }
                                $generationArr = 'generation'.($i1+1).'Arr';
                                $finalTargetArr[$generationArr] = $generation1Arr;
                                $i++;
                            }
                            else if($e1[1] == 'household_composition'){
                                
                                if (isset($household_composition1Arr[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $household_composition1Arr[$interestsTempArr[$i]] = (array) $household_composition1Arr[$interestsTempArr[$i]];
                                    $household_composition1Arr[$interestsTempArr[$i]][] = 'household_composition$#${"id":'.str_replace('}', '', $e[1]).'}';
                                } else {
                                    $household_composition1Arr[$interestsTempArr[$i]] = 'household_composition$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                }
                                $household_compositionArr = 'household_composition'.($i1+1).'Arr';
                                $finalTargetArr[$household_compositionArr] = $household_composition1Arr;
                                $i++;
                            }
                            else if($e1[1] == 'behaviors'){
                                
                                if (isset($behaviors1Arr[$interestsTempArr[$i]])) {
                                    if (!is_array($array[$key]))
                                        $behaviors1Arr[$interestsTempArr[$i]] = (array) $behaviors1Arr[$interestsTempArr[$i]];
                                    $behaviors1Arr[$interestsTempArr[$i]][] = 'behaviors$#${"id":'.str_replace('}', '', $e[1]).'}';
                                } else {
                                    $behaviors1Arr[$interestsTempArr[$i]] = 'behaviors$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                }
                                $behaviorsArr = 'behaviors'.($i1+1).'Arr';
                                $finalTargetArr[$behaviorsArr] = $behaviors1Arr;
                                $i++;
                            }
                            else{
                                if($e[2] != 'events' || $e[2] != 'statuses'){
                                    
                                    if (isset($interestsArr1[$interestsTempArr[$i]])) {
                                        if (!is_array($array[$key]))
                                            $interestsArr1[$interestsTempArr[$i]] = (array) $interestsArr1[$interestsTempArr[$i]];
                                        $interestsArr1[$interestsTempArr[$i]][] = 'interests$#${"id":'.str_replace('}', '', $e[1]).'}';
                                    } else {
                                        $interestsArr1[$interestsTempArr[$i]] = 'interests$#${"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                    }
                                    $interestsArr = 'interestsArr'.($i1+1);
                                    $finalTargetArr[$interestsArr] = $interestsArr1;
                                    $i++;
                                }
                            }
                        }
                    }
                }
            }
        }
		
        #echo "<PRE>";print_r($finalTargetArr);exit;
        
        $interestsTempArr1 = explode(',', $_POST['interests_exclude']);
        $new_interests1 = trim($this->input->post('new_interests_exclude'), ",");
        $new_interests_temp1 = explode(',', $new_interests1);
        $interestsArr11 = array(); 
        $family_statuses1Arr1 = array(); 
        $household_composition1Arr1 = array(); 
        $generation1Arr1 = array(); 
        $ethnic_affinity1Arr1 = array(); 
        $industries1Arr1 = array(); 
        $politics1Arr1 = array(); 
        $life_events1Arr1 = array(); 
        $behaviors1Arr1 = array(); 
        $i = 0;
		
		# print_r($new_interests_temp1); exit;
        #echo "<pre>"; print_r($interestsArr11);
        if (count($new_interests_temp1) > 0) {
            if (!empty($new_interests_temp1[0])) {
                foreach ($new_interests_temp1 as $rs) {
                    $e = explode('_', $rs);
                    $e1 = explode(':', $e[0]);
                    if($e1[1] == 'life'){
                        if (isset($life_events1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $life_events1Arr1[$interestsTempArr1[$i]] = (array) $life_events1Arr1[$interestsTempArr1[$i]];
                            $life_events1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[2]).'}';
                        } else {
                            $life_events1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[2]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'family_statuses'){
                        
                        if (isset($family_statuses1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $family_statuses1Arr1[$interestsTempArr1[$i]] = (array) $family_statuses1Arr1[$interestsTempArr1[$i]];
                            $family_statuses1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $family_statuses1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'politics'){
                        
                        if (isset($politics1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $politics1Arr1[$interestsTempArr1[$i]] = (array) $politics1Arr1[$interestsTempArr1[$i]];
                            $politics1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $politics1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'industries'){
                        
                        if (isset($industries1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $industries1Arr1[$interestsTempArr1[$i]] = (array) $industries1Arr1[$interestsTempArr1[$i]];
                            $industries1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $industries1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'ethnic_affinity'){
                        
                        if (isset($ethnic_affinity1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $ethnic_affinity1Arr1[$interestsTempArr1[$i]] = (array) $ethnic_affinity1Arr1[$interestsTempArr1[$i]];
                            $ethnic_affinity1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $ethnic_affinity1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'generation'){
                        
                        if (isset($generation1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $generation1Arr1[$interestsTempArr1[$i]] = (array) $generation1Arr1[$interestsTempArr1[$i]];
                            $generation1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $generation1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'household_composition'){
                        
                        if (isset($household_composition1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $household_composition1Arr1[$interestsTempArr1[$i]] = (array) $household_composition1Arr1[$interestsTempArr1[$i]];
                            $household_composition1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $household_composition1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else if($e1[1] == 'behaviors'){
                        
                        if (isset($behaviors1Arr1[$interestsTempArr1[$i]])) {
                            if (!is_array($array[$key]))
                                $behaviors1Arr1[$interestsTempArr1[$i]] = (array) $behaviors1Arr1[$interestsTempArr1[$i]];
                            $behaviors1Arr1[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                        } else {
                            $behaviors1Arr1[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                        }
                        $i++;
                    }
                    else{
                        if($e[2] != 'events'){
                            #echo "<pre>"; print_r($interestsArr11);
                            if (isset($interestsArr11[$interestsTempArr1[$i]])) {
                                if (!is_array($array[$key]))
                                    $interestsArr11[$interestsTempArr1[$i]] = (array) $interestsArr11[$interestsTempArr1[$i]];
                                $interestsArr11[$interestsTempArr1[$i]][] = '{"id":'.str_replace('}', '', $e[1]).'}';
                                #echo "<pre>"; print_r($interestsArr11);
                            } else {
                                $interestsArr11[$interestsTempArr1[$i]] = '{"id":'.str_replace('}', '', $e[1]).'}';//str_replace('}', '', $e[1]);
                                #echo "<pre>junaid"; print_r($interestsArr11);
                            }
                            $i++;
                        }
                    }
                    #echo "<pre>"; print_r($interestsArr11);
                }
            }
        }
        $interestsArr11str = '';
        #echo "<pre>"; print_r($interestsArr11);
        if(is_array($interestsArr11)){
            $arrstr = 0;
            foreach ($interestsArr11 as $interestsArr11key => $interestsArr11val){
                if($arrstr == 0){
                    $interestsArr11str = $interestsArr11str.$interestsArr11val;
                }
                else{
                    $interestsArr11str = $interestsArr11str.",".$interestsArr11val;
                }

                $arrstr++;
            }
        }
        #echo $interestsArr11str;
        $age_min = $this->input->post('form');
        $age_max = $this->input->post('to');
        if(!empty($_POST['form2'])){
            array_push($_POST['form2'], $age_min);
        }
        if(!empty($_POST['to2'])){
            array_push($_POST['to2'], $age_max);
        }
        
        $ageTempStr = array();
        if(!empty($_POST['form2']) && !empty($_POST['to2'])){
            for($a=0; $a<=count($_POST['form2'])-1; $a++){
                array_push($ageTempStr, $_POST['form2'][$a].'-'.$_POST['to2'][$a]);
            }
            $ageTempArr[] = implode(',', $ageTempStr);
        }
        else{
            array_push($ageTempStr, $age_min.'-'.$age_max);
            $ageTempArr[] = implode(',', $ageTempStr);
        }
       
        $ageTempArr = explode(',', $ageTempArr[0]);
        
        $new_exclude_locations = trim($this->input->post('new_exclude_locations'), ",");
        $new_interests = implode(',', $interestsArr1);//trim($this->input->post('new_interests'), ",");
        $genders = $this->input->post('genders');
        
        $page_types = $this->input->post('page_types');
        $relationship_statuses = '';
        $relationship_statuses = $this->input->post('relationship_statuses');
        if(empty($relationship_statuses[0])){
            $relationship_statuses = '';
        }
        $interested_in = $this->input->post('interested_in');
        $lng_locales = trim($this->input->post('new_ad_langauage'), ",");
        $education_schools = trim($this->input->post('new_education_schools'), ",");
        $education_majors = trim($this->input->post('new_education_majors'), ",");
        $work_employers = trim($this->input->post('new_work_employers'), ",");
        $work_positions = trim($this->input->post('new_work_positions'), ",");
        $education_statuses = '';
        $education_statuses = $this->input->post('education_statuses');
        if(empty($education_statuses[0])){
            $education_statuses = '';
        }

        $mobile_device_users = $this->input->post('mobile_device_user');
        $ad_account = $_POST['adaccountid_2'];
        $custom_audiences = $this->input->post('custom_audiences');
        //print_r($custom_audiences); 
        //if (!empty($custom_audiences[0])) {
            $ca_id = array();
            $ca_name = array();
            foreach ($custom_audiences as $ca) {
                if(!empty($ca)){
                    $cain = explode('##', $ca);
                    $ca_id[] = $cain[0];
                    $ca_name[] = $cain[1];
                }
            }
            //print_r($ca_id);
            $Clevnt = '';
            if(!empty($ca_id)){    
                foreach ($ca_id as $le) {
                    $Clevnt .= '{"id":' . $le . '},';
                }
                $Clevnt = trim($Clevnt, ",");
            }
       // }
           // print_r($Clevnt);

        $demographics = array('behaviors', 'life_events', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'family_statuses');
        foreach ($demographics as $dg) {
            $$dg = $this->input->post($dg);
        }
        
        if (in_array('location', $split_adset)){
            $locationsArr = explode(',', trim($finalLocationStr, ','));
        } else {
            $locationsArr[] = trim($finalLocationStr, ',');
        }
		$cacountttt = 0;
		$custom_audiencesArrppt = explode(',', trim($finalLocationStr, ','));
		foreach($custom_audiencesArrppt as $value11){
			if($value11 != ""){
				$cacountttt = $cacountttt+1;
			}
		}
		
		$locationcount = $cacountttt;
		//echo count(explode(',', trim($finalLocationStr, ','))); exit;

        if (in_array('placement', $split_adset)){
            //echo json_encode($_POST['page_types']);exit;
            for($p=0; $p<count($_POST['page_types']); $p++){
                if($_POST['page_types'][$p] == 'desktopfeed'){
                    $placementTempArr[] = 'DNF';
                }
                else if($_POST['page_types'][$p] == 'mobilefeed'){
                    $placementTempArr[] = 'MNF';
                }
                else if($_POST['page_types'][$p] == 'mobileexternal'){
                    $placementTempArr[] = 'TPMS';
                }
                else if($_POST['page_types'][$p] == 'rightcolumn'){
                    $placementTempArr[] = 'DRHS';
                }
                else if($_POST['page_types'][$p] == 'instagram'){
                    $placementTempArr[] = 'INSTA';
                }
            }
            $placementArr = $placementTempArr;
        } else {
            for($p=0; $p<count($_POST['page_types']); $p++){
                if($_POST['page_types'][$p] == 'desktopfeed'){
                    $placementTempArr[] = 'DNF';
                }
                else if($_POST['page_types'][$p] == 'mobilefeed'){
                    $placementTempArr[] = 'MNF';
                }
                else if($_POST['page_types'][$p] == 'mobileexternal'){
                    $placementTempArr[] = 'TPMS';
                }
                else if($_POST['page_types'][$p] == 'rightcolumn'){
                    $placementTempArr[] = 'DRHS';
                }
                else if($_POST['page_types'][$p] == 'instagram'){
                    $placementTempArr[] = 'INSTA';
                }
            }
            $placementArr[] = implode(',', $placementTempArr);
        }
		$placementcount = count($placementTempArr);
        if (in_array('genders', $split_adset)) { //Split at at genders base
            $gendersArr = array('Men', 'Women');
        } else {
            if (!empty($_POST['genders'])) {
                if($_POST['genders'] == '1')
                    $gendersArr = array('Men');
                else
                    $gendersArr = array('Women');
            } else {
                $gendersArr1 = array('Men', 'Women');
                $gendersArr[] = implode(',', $gendersArr1);
            }
        }
            
        if (in_array('custom_audiences', $split_adset)){
            $custom_audiencesArr = $_POST['custom_audiences'];
        }
        else{
            $custom_audiencesArr[] = implode(',', $_POST['custom_audiences']);
        }
		$cacounttt = 0;
		$custom_audiencesArrpp = $_POST['custom_audiences'];
		foreach($custom_audiencesArrpp as $value1){
			if($value1 != ""){
				$cacounttt = $cacounttt+1;
			}
		}
		
		
        $cacount = $cacounttt;
        $demographicArr1 = array();
        if(!empty($locationsArr)){
            $finalTempArr[] = $locationsArr;//0
        }
        
        if (in_array('interests', $split_adset)){
            $finalTemp1Arr = array();
            $finalTemp2Arr = array();
            $finalTemp3Arr = array();
            $finalTemp4Arr = array();
            $finalTemp5Arr = array();
            if(!empty($finalTargetArr['interestsArr1'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr1']);
            }
            if(!empty($finalTargetArr['family_statuses1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses1Arr']);
            }
            if(!empty($finalTargetArr['life_events1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events1Arr']);
            }
            if(!empty($finalTargetArr['politics1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics1Arr']);
            }
            if(!empty($finalTargetArr['industries1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries1Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity1Arr']);
            }
            if(!empty($finalTargetArr['generation1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation1Arr']);
            }
            if(!empty($finalTargetArr['household_composition1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition1Arr']);
            }
            if(!empty($finalTargetArr['behaviors1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors1Arr']);
            }
            
            if(!empty($finalTemp1Arr)){
                $finalTempArr[] = $finalTemp1Arr;//1
            }
            else{
                $finalTempArr[] = array('0'=>'');  
            }

            if(!empty($finalTargetArr['interestsArr2'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['interestsArr2']);
            }
            if(!empty($finalTargetArr['family_statuses2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['family_statuses2Arr']);
            }
            if(!empty($finalTargetArr['life_events2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['life_events2Arr']);
            }
            if(!empty($finalTargetArr['politics2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['politics2Arr']);
            }
            if(!empty($finalTargetArr['industries2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['industries2Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['ethnic_affinity2Arr']);
            }
            if(!empty($finalTargetArr['generation2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['generation2Arr']);
            }
            if(!empty($finalTargetArr['household_composition2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['household_composition2Arr']);
            }
            if(!empty($finalTargetArr['behaviors2Arr'])){
                $finalTemp2Arr = array_merge($finalTemp2Arr, $finalTargetArr['behaviors2Arr']);
            }
            if(!empty($finalTemp2Arr)){
                $finalTempArr[] = $finalTemp2Arr;//2
            }
            else{
                $finalTempArr[] = array('0'=>'');  //2
            }

            if(!empty($finalTargetArr['interestsArr3'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['interestsArr3']);
            }
            if(!empty($finalTargetArr['family_statuses3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['family_statuses3Arr']);
            }
            if(!empty($finalTargetArr['life_events3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['life_events3Arr']);
            }
            if(!empty($finalTargetArr['politics3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['politics3Arr']);
            }
            if(!empty($finalTargetArr['industries3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['industries3Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['ethnic_affinity3Arr']);
            }
            if(!empty($finalTargetArr['generation3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['generation3Arr']);
            }
            if(!empty($finalTargetArr['household_composition3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['household_composition3Arr']);
            }
            if(!empty($finalTargetArr['behaviors3Arr'])){
                $finalTemp3Arr = array_merge($finalTemp3Arr, $finalTargetArr['behaviors3Arr']);
            }
            
            if(!empty($finalTemp3Arr)){
                $finalTempArr[] = $finalTemp3Arr;//3
            }
            else{
                $finalTempArr[] = array('0'=>'');  //3
            }
            
            if(!empty($finalTargetArr['interestsArr4'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['interestsArr4']);
            }
            if(!empty($finalTargetArr['family_statuses4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['family_statuses4Arr']);
            }
            if(!empty($finalTargetArr['life_events4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['life_events4Arr']);
            }
            if(!empty($finalTargetArr['politics4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['politics4Arr']);
            }
            if(!empty($finalTargetArr['industries4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['industries4Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['ethnic_affinity4Arr']);
            }
            if(!empty($finalTargetArr['generation4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['generation4Arr']);
            }
            if(!empty($finalTargetArr['household_composition4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['household_composition4Arr']);
            }
            if(!empty($finalTargetArr['behaviors4Arr'])){
                $finalTemp4Arr = array_merge($finalTemp4Arr, $finalTargetArr['behaviors4Arr']);
            }
            
            if(!empty($finalTemp4Arr)){
                $finalTempArr[] = $finalTemp4Arr;//4
            }
            else{
                $finalTempArr[] = array('0'=>'');  //4
            }

            if(!empty($finalTargetArr['interestsArr5'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['interestsArr5']);
            }
            if(!empty($finalTargetArr['family_statuses5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['family_statuses5Arr']);
            }
            if(!empty($finalTargetArr['life_events5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['life_events5Arr']);
            }
            if(!empty($finalTargetArr['politics5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['politics5Arr']);
            }
            if(!empty($finalTargetArr['industries5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['industries5Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['ethnic_affinity5Arr']);
            }
            if(!empty($finalTargetArr['generation5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['generation5Arr']);
            }
            if(!empty($finalTargetArr['household_composition5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['household_composition5Arr']);
            }
            if(!empty($finalTargetArr['behaviors5Arr'])){
                $finalTemp5Arr = array_merge($finalTemp5Arr, $finalTargetArr['behaviors5Arr']);
            }
            
            if(!empty($finalTemp5Arr)){
                $finalTempArr[] = $finalTemp5Arr;//5
            }
            else{
                $finalTempArr[] = array('0'=>'');  //5
            }
        }
        else{
            $finalTemp1Arr = array();
            if(!empty($finalTargetArr['interestsArr1'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr1']);
            }
            if(!empty($finalTargetArr['family_statuses1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses1Arr']);
            }
            if(!empty($finalTargetArr['life_events1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events1Arr']);
            }
            if(!empty($finalTargetArr['politics1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics1Arr']);
            }
            if(!empty($finalTargetArr['industries1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries1Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity1Arr']);
            }
            if(!empty($finalTargetArr['generation1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation1Arr']);
            }
            if(!empty($finalTargetArr['household_composition1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition1Arr']);
            }
            if(!empty($finalTargetArr['behaviors1Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors1Arr']);
            }

            if(!empty($finalTargetArr['interestsArr2'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr2']);
            }
            if(!empty($finalTargetArr['family_statuses2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses2Arr']);
            }
            if(!empty($finalTargetArr['life_events2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events2Arr']);
            }
            if(!empty($finalTargetArr['politics2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics2Arr']);
            }
            if(!empty($finalTargetArr['industries2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries2Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity2Arr']);
            }
            if(!empty($finalTargetArr['generation2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation2Arr']);
            }
            if(!empty($finalTargetArr['household_composition2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition2Arr']);
            }
            if(!empty($finalTargetArr['behaviors2Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors2Arr']);
            }

            if(!empty($finalTargetArr['interestsArr3'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr3']);
            }
            if(!empty($finalTargetArr['family_statuses3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses3Arr']);
            }
            if(!empty($finalTargetArr['life_events3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events3Arr']);
            }
            if(!empty($finalTargetArr['politics3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics3Arr']);
            }
            if(!empty($finalTargetArr['industries3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries3Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity3Arr']);
            }
            if(!empty($finalTargetArr['generation3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation3Arr']);
            }
            if(!empty($finalTargetArr['household_composition3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition3Arr']);
            }
            if(!empty($finalTargetArr['behaviors3Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors3Arr']);
            }
            
            if(!empty($finalTargetArr['interestsArr4'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr4']);
            }
            if(!empty($finalTargetArr['family_statuses4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses4Arr']);
            }
            if(!empty($finalTargetArr['life_events4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events4Arr']);
            }
            if(!empty($finalTargetArr['politics4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics4Arr']);
            }
            if(!empty($finalTargetArr['industries4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries4Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity4Arr']);
            }
            if(!empty($finalTargetArr['generation4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation4Arr']);
            }
            if(!empty($finalTargetArr['household_composition4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition4Arr']);
            }
            if(!empty($finalTargetArr['behaviors4Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors4Arr']);
            }

            if(!empty($finalTargetArr['interestsArr5'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['interestsArr5']);
            }
            if(!empty($finalTargetArr['family_statuses5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['family_statuses5Arr']);
            }
            if(!empty($finalTargetArr['life_events5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['life_events5Arr']);
            }
            if(!empty($finalTargetArr['politics5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['politics5Arr']);
            }
            if(!empty($finalTargetArr['industries5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['industries5Arr']);
            }
            if(!empty($finalTargetArr['ethnic_affinity5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['ethnic_affinity5Arr']);
            }
            if(!empty($finalTargetArr['generation5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['generation5Arr']);
            }
            if(!empty($finalTargetArr['household_composition5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['household_composition5Arr']);
            }
            if(!empty($finalTargetArr['behaviors5Arr'])){
                $finalTemp1Arr = array_merge($finalTemp1Arr, $finalTargetArr['behaviors5Arr']);
            }
            if(!empty($finalTemp1Arr)){
                $finalTempArr[] = array('0'=> str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(',', array_values($finalTemp1Arr))));//1
            }
            else{
                $finalTempArr[] = array('0'=>'');  
            }
            $finalTempArr[] = array('0'=>'');  //2
            $finalTempArr[] = array('0'=>'');  //3
            $finalTempArr[] = array('0'=>'');  //4
            $finalTempArr[] = array('0'=>'');  //5
        }
		#echo "<pre>"; print_r($finalTempArr); echo "</pre>"; exit;
		/*$intarrcount = 0;


		foreach($finalTemp1Arr as $int_arrcount){
			if(is_array($int_arrcount)){
                foreach($int_arrcount as $int_arrcountarr){
				    $intarrcount = $intarrcount+1;
                }
			}
		}
		foreach($finalTemp2Arr as $int_arrcount1){
			if(is_array($int_arrcount1)){
                foreach($int_arrcount1 as $int_arrcount1arr){
				    $intarrcount = $intarrcount+1;
                }
			}
		}
		foreach($finalTemp3Arr as $int_arrcount2){
			if(is_array($int_arrcount2)){
                foreach($int_arrcount2 as $int_arrcount2arr){
				    $intarrcount = $intarrcount+1;
                }
			}
		}
		foreach($finalTemp4Arr as $int_arrcount3){
			if(is_array($int_arrcount3)){
                foreach($int_arrcount3 as $int_arrcount3arr){
				    $intarrcount = $intarrcount+1;
                }
			}
		}
		foreach($finalTemp5Arr as $int_arrcount4){
			if(is_array($int_arrcount4)){
                foreach($int_arrcount4 as $int_arrcount4arr){
				    $intarrcount = $intarrcount+1;
                }
			}
		}
		$interestcount = $intarrcount;*/
		
		//$locationcount = $cacountttt;
		
		//count($finalTemp1Arr)+count($finalTemp2Arr)+count($finalTemp3Arr)+count($finalTemp4Arr)+count($finalTemp5Arr);
        #echo "<PRE>";print_r($finalTempArr);exit;
		if(!empty($placementArr)){
            $finalTempArr[] = $placementArr;//6
        }
        else{
            $finalTempArr[] = array('0'=>''); 
        }
        if(!empty($gendersArr)){
            $finalTempArr[] = $gendersArr;//7
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        
        if(!empty($custom_audiencesArr)){
            
            /*foreach ($custom_audiencesArr as $custom_audiencesArr12) {
                if(!empty($custom_audiencesArr12))
                    $finalTempArr[] = $custom_audiencesArr12;//8
            }*/
            if(!isset($custom_audiencesArr[1]) && empty($custom_audiencesArr[0])){
                 $finalTempArr[] = array('0'=>'');
            }
            else{
                if(empty($custom_audiencesArr[0])){
                     unset($custom_audiencesArr[0]);
                }
                //print_r($custom_audiencesArr); exit;
                $finalTempArr[] = $custom_audiencesArr;//8
            }
            
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($ageTempArr)){
            $finalTempArr[] = $ageTempArr;//9
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        
        #echo "<PRE>";print_r($finalTempArr);exit;
        if(!empty($demographicArr1)){
            $finalTempArr[] = $demographicArr1;//10
        }
        else{
            $finalTempArr[] = array('0'=>'');  
        }
        
        if(!empty($interestsArr11str)){
            $finalTempArr[] = array('0'=>$interestsArr11str);//11
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        /*if(!empty($interestsArr11)){
            $finalTempArr[] = $interestsArr11;//11
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }*/
        if(!empty($behaviors1Arr1)){
            $finalTempArr[] = $behaviors1Arr1;//12
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($family_statuses1Arr1)){
            $finalTempArr[] = $family_statuses1Arr1;//13
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($household_composition1Arr1)){
            $finalTempArr[] = $household_composition1Arr1;//14
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($generation1Arr1)){
            $finalTempArr[] = $generation1Arr1;//15
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($ethnic_affinity1Arr1)){
            $finalTempArr[] = $ethnic_affinity1Arr1;//16
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($industries1Arr1)){
            $finalTempArr[] = $industries1Arr1;//17
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($politics1Arr1)){
            $finalTempArr[] = $politics1Arr1;//18
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        if(!empty($life_events1Arr1)){
            $finalTempArr[] = $life_events1Arr1;//19
        }
        else{
            $finalTempArr[] = array('0'=>'');
        }
        #echo "<pre>";print_r($interestsArr);echo "</pre>";
        #echo "<pre>";print_r($interestsArr11);echo "</pre>"; echo count($interestsArr11); exit;
		#echo "<pre>";print_r($finalTempArr);
        $finalArr = $this->combinations($finalTempArr);
        #echo "<pre>";print_r($finalArr);echo "</pre>";exit;
		
        $totalAudience = 0;
        if(!empty($finalArr)){
            $tempStr1 = '';
            for($i=0; $i<=count($finalArr)-1; $i++){
                if (!in_array('location', $split_adset)){
                    $new_geo_locations1 = str_replace('$#$', ',', trim($this->input->post('new_geo_locations'), "$#$"));
                    $new_geo_locations = $this->getProperLocationArray($new_geo_locations1);
                }
                else{
                    $new_geo_locations = str_replace('$#$', ',', $locationArr[$finalArr[$i][0]]);
                }
                
                //$param = 'optimize_for=' . $optimize_for;
                $param = '';
				$ageGroup = explode("-", $finalArr[$i][9]);
                $age_max = $ageGroup[0];
                $age_min = $ageGroup[1];
                    
                if (!empty($age_min)) {
                    $param .= "&targeting_spec={'age_min':" . $age_max;
                }
                if (!empty($age_max)) {
                    $param .= ",'age_max':" . $age_min;
                }
				
				
				//$param .= '&targeting_spec={"genders":[' . $genders . ']';
				
				if(!empty($new_geo_locations) && $new_geo_locations != ':[]'){
					$param .= ",'geo_locations': {" . $new_geo_locations . "}";
				}
				if(!empty($genders)){
					$param .= ",'genders':[" . $genders . "]";
				}
				
				
                
                if (!empty($new_exclude_locations)) {
                    $new_exclude_locations = $this->getProperLocationArray($new_exclude_locations);
                    $param .= ",'excluded_geo_locations': {" . $new_exclude_locations . "}";
                }
                
                
                
                if (!in_array('interests', $split_adset)){
                    //not split
                    
                    if (!empty($interestsArr11) || !empty($family_statuses1Arr1) || !empty($household_composition1Arr1) || !empty($generation1Arr1) || !empty($ethnic_affinity1Arr1) || !empty($industries1Arr1) || !empty($politics1Arr1) || !empty($life_events1Arr1) || !empty($behaviors1Arr1)) {
                        
                        $param .= ", 'exclusions':{";
                        $tempstr = 0;
                        if (!empty($interestsArr11)){
                            $param .= "'interests':[" . implode(",", $interestsArr11) . "]";
                            $tempstr = 1; 
                        }
                        if(!empty($family_statuses1Arr1)){
                            if($tempstr != 0){
                                $param .= ",'family_statuses':[" . implode(",", $family_statuses1Arr1) . "]";  
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'family_statuses':[" . implode(",", $family_statuses1Arr1) . "]";   
                            }
                        }
                        if(!empty($household_composition1Arr1)){
                            if($tempstr != 0){
                                $param .= ",'household_composition':[" . implode(",", $household_composition1Arr1) . "]";
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'household_composition':[" . implode(",", $household_composition1Arr1) . "]";
                            }
                        }
                        if(!empty($generation1Arr1)){
                            if($tempstr != 0){
                                $param .= ",'generation':[" . implode(",", $generation1Arr1) . "]";
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'generation':[" . implode(",", $generation1Arr1) . "]";  
                            }
                        }
                        if(!empty($ethnic_affinity1Arr1)){
                            if($tempstr != 0){
                                $param .= ",'ethnic_affinity':[" . implode(",", $ethnic_affinity1Arr1) . "]";
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . implode(",", $ethnic_affinity1Arr1) . "]"; 
                            }
                        }
                        if(!empty($industries1Arr1)){
                            if($tempstr != 0){
                                $param .= ",'industries':[" . implode(",", $industries1Arr1) . "]";
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'industries':[" . implode(",", $industries1Arr1) . "]";  
                            }
                        }
                        if(!empty($politics1Arr1)){
                            if($tempstr != 0){
                                $param .= ",'politics':[" . implode(",", $politics1Arr1) . "]";
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'politics':[" . implode(",", $politics1Arr1) . "]";  
                            }
                        }
                        if(!empty($life_events1Arr1)){
                            if($tempstr != 0){
                                $param .= ",'life_events':[" . implode(",", $life_events1Arr1) . "]";
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'life_events':[" . implode(",", $life_events1Arr1) . "]";
                            }
                        }
                        if(!empty($behaviors1Arr1)){
                            if($tempstr != 0){
                                $param .= ",'behaviors':[" . implode(",", $behaviors1Arr1) . "]";
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'behaviors':[" . implode(",", $behaviors1Arr1) . "]";
                            }
                        }
                        $param .= "}";
                    }

                    $param .= ", 'flexible_spec':[";
                    if (!empty($finalTargetArr['interestsArr1']) || !empty($finalTargetArr['family_statuses1Arr']) || !empty($finalTargetArr['household_composition1Arr']) || !empty($finalTargetArr['generation1Arr']) || !empty($finalTargetArr['ethnic_affinity1Arr']) || !empty($finalTargetArr['industries1Arr']) || !empty($finalTargetArr['politics1Arr']) || !empty($finalTargetArr['life_events1Arr']) || !empty($finalTargetArr['behaviors1Arr'])) {
                        $param .= "{";
                        $tempstr = 0;
                        if (!empty($finalTargetArr['interestsArr1'])){
                            $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr1'])) . "]";
                            $tempstr = 1;
                        }
                        if(!empty($finalTargetArr['family_statuses1Arr'])){
                            if($tempstr != 0){
                                $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses1Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses1Arr'])) . "]";   
                            }
                        }
                        if(!empty($finalTargetArr['household_composition1Arr'])){
                            if($tempstr != 0){
                                $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition1Arr'])) . "]"; 
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition1Arr'])) . "]";
                            }
                        }
                        if(!empty($finalTargetArr['generation1Arr'])){
                            if($tempstr != 0){
                                $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation1Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation1Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['ethnic_affinity1Arr'])){
                            if($tempstr != 0){
                                $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity1Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity1Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['industries1Arr'])){
                            if($tempstr != 0){
                                $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries1Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries1Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['politics1Arr'])){
                            if($tempstr != 0){
                                $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics1Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics1Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['life_events1Arr'])){
                            if($tempstr != 0){
                                $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events1Arr'])) . "]";  
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events1Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['behaviors1Arr'])){
                            if($tempstr != 0){
                                $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors1Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors1Arr'])) . "]";
                            }
                        }
                        $param .= "}";
                    }
                    if (!empty($finalTargetArr['interestsArr2']) || !empty($finalTargetArr['family_statuses2Arr']) || !empty($finalTargetArr['household_composition2Arr']) || !empty($finalTargetArr['generation2Arr']) || !empty($finalTargetArr['ethnic_affinity2Arr']) || !empty($finalTargetArr['industries2Arr']) || !empty($finalTargetArr['politics2Arr']) || !empty($finalTargetArr['life_events2Arr']) || !empty($finalTargetArr['behaviors2Arr'])) {
                        $param .= ",{";
                        $tempstr = 0;
                        if (!empty($finalTargetArr['interestsArr2'])){
                            $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr2'])) . "]";
                            $tempstr = 1;
                        }
                        if(!empty($finalTargetArr['family_statuses2Arr'])){
                            if($tempstr != 0){
                                $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses2Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses2Arr'])) . "]";   
                            }
                        }
                        if(!empty($finalTargetArr['household_composition2Arr'])){
                            if($tempstr != 0){
                                $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition2Arr'])) . "]"; 
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition2Arr'])) . "]";
                            }
                        }
                        if(!empty($finalTargetArr['generation2Arr'])){
                            if($tempstr != 0){
                                $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation2Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation2Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['ethnic_affinity2Arr'])){
                            if($tempstr != 0){
                                $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity2Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity2Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['industries2Arr'])){
                            if($tempstr != 0){
                                $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries2Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries2Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['politics2Arr'])){
                            if($tempstr != 0){
                                $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics2Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics2Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['life_events2Arr'])){
                            if($tempstr != 0){
                                $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events2Arr'])) . "]";  
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events2Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['behaviors2Arr'])){
                            if($tempstr != 0){
                                $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors2Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors2Arr'])) . "]";
                            }
                        }
                        $param .= "}";
                    }
                    if (!empty($finalTargetArr['interestsArr3']) || !empty($finalTargetArr['family_statuses3Arr']) || !empty($finalTargetArr['household_composition3Arr']) || !empty($finalTargetArr['generation3Arr']) || !empty($finalTargetArr['ethnic_affinity3Arr']) || !empty($finalTargetArr['industries3Arr']) || !empty($finalTargetArr['politics3Arr']) || !empty($finalTargetArr['life_events3Arr']) || !empty($finalTargetArr['behaviors3Arr'])) {
                        $param .= ",{";
                        $tempstr = 0;
                        if (!empty($finalTargetArr['interestsArr3'])){
                            $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr3'])) . "]";
                            $tempstr = 1;
                        }
                        if(!empty($finalTargetArr['family_statuses3Arr'])){
                            if($tempstr != 0){
                                $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses3Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses3Arr'])) . "]";   
                            }
                        }
                        if(!empty($finalTargetArr['household_composition3Arr'])){
                            if($tempstr != 0){
                                $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition3Arr'])) . "]"; 
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition3Arr'])) . "]";
                            }
                        }
                        if(!empty($finalTargetArr['generation3Arr'])){
                            if($tempstr != 0){
                                $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation3Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation3Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['ethnic_affinity3Arr'])){
                            if($tempstr != 0){
                                $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity3Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity3Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['industries3Arr'])){
                            if($tempstr != 0){
                                $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries3Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries3Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['politics3Arr'])){
                            if($tempstr != 0){
                                $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics3Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics3Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['life_events3Arr'])){
                            if($tempstr != 0){
                                $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events3Arr'])) . "]";  
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events3Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['behaviors3Arr'])){
                            if($tempstr != 0){
                                $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors3Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors3Arr'])) . "]";
                            }
                        }
                        $param .= "}";
                    }
                    if (!empty($finalTargetArr['interestsArr4']) || !empty($finalTargetArr['family_statuses4Arr']) || !empty($finalTargetArr['household_composition4Arr']) || !empty($finalTargetArr['generation4Arr']) || !empty($finalTargetArr['ethnic_affinity4Arr']) || !empty($finalTargetArr['industries4Arr']) || !empty($finalTargetArr['politics1Arr']) || !empty($finalTargetArr['life_events4Arr']) || !empty($finalTargetArr['behaviors4Arr'])) {
                        $param .= "{";
                        $tempstr = 0;
                        if (!empty($finalTargetArr['interestsArr4'])){
                            $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr4'])) . "]";
                            $tempstr = 1;
                        }
                        if(!empty($finalTargetArr['family_statuses4Arr'])){
                            if($tempstr != 0){
                                $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses4Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses4Arr'])) . "]";   
                            }
                        }
                        if(!empty($finalTargetArr['household_composition4Arr'])){
                            if($tempstr != 0){
                                $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition4Arr'])) . "]"; 
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition4Arr'])) . "]";
                            }
                        }
                        if(!empty($finalTargetArr['generation4Arr'])){
                            if($tempstr != 0){
                                $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation4Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation4Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['ethnic_affinity4Arr'])){
                            if($tempstr != 0){
                                $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity4Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity4Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['industries4Arr'])){
                            if($tempstr != 0){
                                $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries4Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries4Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['politics4Arr'])){
                            if($tempstr != 0){
                                $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics4Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics4Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['life_events4Arr'])){
                            if($tempstr != 0){
                                $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events4Arr'])) . "]";  
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events4Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['behaviors4Arr'])){
                            if($tempstr != 0){
                                $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors4Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors4Arr'])) . "]";
                            }
                        }
                        $param .= "}";
                    }
                    if (!empty($finalTargetArr['interestsArr5']) || !empty($finalTargetArr['family_statuses5Arr']) || !empty($finalTargetArr['household_composition5Arr']) || !empty($finalTargetArr['generation5Arr']) || !empty($finalTargetArr['ethnic_affinity5Arr']) || !empty($finalTargetArr['industries5Arr']) || !empty($finalTargetArr['politics5Arr']) || !empty($finalTargetArr['life_events5Arr']) || !empty($finalTargetArr['behaviors5Arr'])) {
                        $param .= "{";
                        $tempstr = 0;
                        if (!empty($finalTargetArr['interestsArr5'])){
                            $param .= "'interests':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['interestsArr5'])) . "]";
                            $tempstr = 1;
                        }
                        if(!empty($finalTargetArr['family_statuses5Arr'])){
                            if($tempstr != 0){
                                $param .= ",'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses5Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'family_statuses':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['family_statuses5Arr'])) . "]";   
                            }
                        }
                        if(!empty($finalTargetArr['household_composition5Arr'])){
                            if($tempstr != 0){
                                $param .= ",'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition5Arr'])) . "]"; 
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'household_composition':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['household_composition5Arr'])) . "]";
                            }
                        }
                        if(!empty($finalTargetArr['generation5Arr'])){
                            if($tempstr != 0){
                                $param .= ",'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation5Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'generation':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['generation5Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['ethnic_affinity5Arr'])){
                            if($tempstr != 0){
                                $param .= ",'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity5Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['ethnic_affinity5Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['industries5Arr'])){
                            if($tempstr != 0){
                                $param .= ",'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries5Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'industries':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['industries5Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['politics5Arr'])){
                            if($tempstr != 0){
                                $param .= ",'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics5Arr'])) . "]";   
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'politics':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['politics5Arr'])) . "]"; 
                            }
                        }
                        if(!empty($finalTargetArr['life_events5Arr'])){
                            if($tempstr != 0){
                                $param .= ",'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events5Arr'])) . "]";  
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'life_events':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['life_events5Arr'])) . "]";  
                            }
                        }
                        if(!empty($finalTargetArr['behaviors5Arr'])){
                            if($tempstr != 0){
                                $param .= ",'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors5Arr'])) . "]";  
                                $tempstr = 1;
                            }
                            else{
                                $param .= "'behaviors':[" . str_replace(array('$#$', 'life_events', 'family_statuses', 'politics', 'industries', 'ethnic_affinity', 'generation', 'household_composition', 'behaviors', 'interests'), '', implode(",", $finalTargetArr['behaviors5Arr'])) . "]";
                            }
                        }
                        $param .= "}";
                    }
                    
                    $param .= "]";
                }
                else{
                    //split
                    if (!empty($interestsArr11) || !empty($family_statuses1Arr1) || !empty($household_composition1Arr1) || !empty($generation1Arr1) || !empty($ethnic_affinity1Arr1) || !empty($industries1Arr1) || !empty($politics1Arr1) || !empty($life_events1Arr1) || !empty($behaviors1Arr1)) {
                        
                        $param .= ", 'exclusions':{";
                        $tempstr = 0;
                        if (!empty($finalArr[$i][11])) {
                            $param .= "'interests':[" . $finalArr[$i][11] . "]";
                            $tempstr = 1; 
                        }
                        if(!empty($finalArr[$i][13])){
                            if($tempstr != 0){
                                $param .= ",'family_statuses':[" . $finalArr[$i][13] . "]";
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'family_statuses':[" . $finalArr[$i][13] . "]";
                            }
                        }
                        if(!empty($finalArr[$i][14])){
                            if($tempstr != 0){
                                $param .= ",'household_composition':[" . $finalArr[$i][14] . "]";
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'household_composition':[" . $finalArr[$i][14] . "]";   
                            }
                        }
                        if(!empty($finalArr[$i][15])){
                            if($tempstr != 0){
                                $param .= ",'generation':[" . $finalArr[$i][15] . "]";
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'generation':[" . $finalArr[$i][15] . "]";
                            }
                        }
                        if(!empty($finalArr[$i][16])){
                            if($tempstr != 0){
                                $param .= ",'ethnic_affinity':[" . $finalArr[$i][16] . "]";
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'ethnic_affinity':[" . $finalArr[$i][16] . "]";   
                            }
                        }
                        if(!empty($finalArr[$i][17])){
                            if($tempstr != 0){
                                $param .= ",'industries':[" . $finalArr[$i][17] . "]";
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'industries':[" . $finalArr[$i][17] . "]";
                            }
                        }
                        if(!empty($finalArr[$i][18])){
                            if($tempstr != 0){
                                $param .= ",'politics':[" . $finalArr[$i][18] . "]";
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'politics':[" . $finalArr[$i][18] . "]";
                            }   
                        }
                        if(!empty($finalArr[$i][19])){
                            if($tempstr != 0){
                                $param .= ",'life_events':[" . $finalArr[$i][19] . "]";
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'life_events':[" . $finalArr[$i][19] . "]";
                            }
                        }
                        if(!empty($finalArr[$i][12])){
                            if($tempstr != 0){
                                $param .= ",'behaviors':[" . $finalArr[$i][12] . "]";
                                $tempstr = 1; 
                            }
                            else{
                                $param .= "'behaviors':[" . $finalArr[$i][12] . "]";
                            }
                        }
                        $param .= "}";
                    }

                    $param .= ", 'flexible_spec':[";
                    $param .= "{";
                    $tempExploadArr = explode('$#$', $finalArr[$i][1]);
                    $tempstr = 0;
                    if($tempExploadArr[0] == 'interests'){
                        $param .= "'interests':[" . $tempExploadArr[1]."]";
                        $tempstr = 1; 
                    }
                    else if($tempExploadArr[0] == 'behaviors'){
                        if($tempstr == 1){
                            $param .= ",'behaviors':[" . $tempExploadArr[1]."]";
                        }
                        else{
                            $param .= "'behaviors':[" . $tempExploadArr[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr[0] == 'household_composition'){
                        if($tempstr == 1){
                            $param .= ",'household_composition':[" . $tempExploadArr[1]."]";
                        }
                        else{
                            $param .= "'household_composition':[" . $tempExploadArr[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr[0] == 'generation'){
                        if($tempstr == 1){
                            $param .= ",'generation':[" . $tempExploadArr[1]."]";
                        }
                        else{
                            $param .= "'generation':[" . $tempExploadArr[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr[0] == 'ethnic_affinity'){
                        if($tempstr == 1){
                            $param .= ",'ethnic_affinity':[" . $tempExploadArr[1]."]";
                        }
                        else{
                            $param .= "'ethnic_affinity':[" . $tempExploadArr[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr[0] == 'industries'){
                        if($tempstr == 1){
                            $param .= ",'industries':[" . $tempExploadArr[1]."]";
                        }
                        else{
                            $param .= "'industries':[" . $tempExploadArr[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr[0] == 'politics'){
                        if($tempstr == 1){
                            $param .= ",'politics':[" . $tempExploadArr[1]."]";
                        }
                        else{
                            $param .= "'politics':[" . $tempExploadArr[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr[0] == 'family_statuses'){
                        if($tempstr == 1){
                            $param .= ",'family_statuses':[" . $tempExploadArr[1]."]";
                        }
                        else{
                            $param .= "'family_statuses':[" . $tempExploadArr[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr[0] == 'life_events'){
                        if($tempstr == 1){
                            $param .= ",'life_events':[" . $tempExploadArr[1]."]";
                        }
                        else{
                            $param .= "'life_events':[" . $tempExploadArr[1]."]";
                        }
                        $tempstr = 1;
                    }
                    $param .= "},{";
                    $tempExploadArr2 = explode('$#$', $finalArr[$i][2]);
                    $tempstr = 0;
                    if($tempExploadArr2[0] == 'interests'){
                        $param .= "'interests':[" . $tempExploadArr2[1]."]";
                        $tempstr = 1; 
                    }
                    else if($tempExploadArr2[0] == 'behaviors'){
                        if($tempstr == 1){
                            $param .= ",'behaviors':[" . $tempExploadArr2[1]."]";
                        }
                        else{
                            $param .= "'behaviors':[" . $tempExploadArr2[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr2[0] == 'household_composition'){
                        if($tempstr == 1){
                            $param .= ",'household_composition':[" . $tempExploadArr2[1]."]";
                        }
                        else{
                            $param .= "'household_composition':[" . $tempExploadArr2[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr2[0] == 'generation'){
                        if($tempstr == 1){
                            $param .= ",'generation':[" . $tempExploadArr2[1]."]";
                        }
                        else{
                            $param .= "'generation':[" . $tempExploadArr2[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr2[0] == 'ethnic_affinity'){
                        if($tempstr == 1){
                            $param .= ",'ethnic_affinity':[" . $tempExploadArr2[1]."]";
                        }
                        else{
                            $param .= "'ethnic_affinity':[" . $tempExploadArr2[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr2[0] == 'industries'){
                        if($tempstr == 1){
                            $param .= ",'industries':[" . $tempExploadArr2[1]."]";
                        }
                        else{
                            $param .= "'industries':[" . $tempExploadArr2[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr2[0] == 'politics'){
                        if($tempstr == 1){
                            $param .= ",'politics':[" . $tempExploadArr2[1]."]";
                        }
                        else{
                            $param .= "'politics':[" . $tempExploadArr2[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr2[0] == 'family_statuses'){
                        if($tempstr == 1){
                            $param .= ",'family_statuses':[" . $tempExploadArr2[1]."]";
                        }
                        else{
                            $param .= "'family_statuses':[" . $tempExploadArr2[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr2[0] == 'life_events'){
                        if($tempstr == 1){
                            $param .= ",'life_events':[" . $tempExploadArr2[1]."]";
                        }
                        else{
                            $param .= "'life_events':[" . $tempExploadArr2[1]."]";
                        }
                        $tempstr = 1;
                    }
                    $param .= "},{";
                    $tempExploadArr3 = explode('$#$', $finalArr[$i][3]);
                    $tempstr = 0;
                    if($tempExploadArr3[0] == 'interests'){
                        $param .= "'interests':[" . $tempExploadArr3[1]."]";
                        $tempstr = 1; 
                    }
                    else if($tempExploadArr3[0] == 'behaviors'){
                        if($tempstr == 1){
                            $param .= ",'behaviors':[" . $tempExploadArr3[1]."]";
                        }
                        else{
                            $param .= "'behaviors':[" . $tempExploadArr3[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr3[0] == 'household_composition'){
                        if($tempstr == 1){
                            $param .= ",'household_composition':[" . $tempExploadArr3[1]."]";
                        }
                        else{
                            $param .= "'household_composition':[" . $tempExploadArr3[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr3[0] == 'generation'){
                        if($tempstr == 1){
                            $param .= ",'generation':[" . $tempExploadArr3[1]."]";
                        }
                        else{
                            $param .= "'generation':[" . $tempExploadArr3[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr3[0] == 'ethnic_affinity'){
                        if($tempstr == 1){
                            $param .= ",'ethnic_affinity':[" . $tempExploadArr3[1]."]";
                        }
                        else{
                            $param .= "'ethnic_affinity':[" . $tempExploadArr3[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr3[0] == 'industries'){
                        if($tempstr == 1){
                            $param .= ",'industries':[" . $tempExploadArr3[1]."]";
                        }
                        else{
                            $param .= "'industries':[" . $tempExploadArr3[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr3[0] == 'politics'){
                        if($tempstr == 1){
                            $param .= ",'politics':[" . $tempExploadArr3[1]."]";
                        }
                        else{
                            $param .= "'politics':[" . $tempExploadArr3[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr3[0] == 'family_statuses'){
                        if($tempstr == 1){
                            $param .= ",'family_statuses':[" . $tempExploadArr3[1]."]";
                        }
                        else{
                            $param .= "'family_statuses':[" . $tempExploadArr3[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr3[0] == 'life_events'){
                        if($tempstr == 1){
                            $param .= ",'life_events':[" . $tempExploadArr3[1]."]";
                        }
                        else{
                            $param .= "'life_events':[" . $tempExploadArr3[1]."]";
                        }
                        $tempstr = 1;
                    }
                    $param .= "},{";
                    $tempExploadArr4 = explode('$#$', $finalArr[$i][4]);
                    $tempstr = 0;
                    if($tempExploadArr4[0] == 'interests'){
                        $param .= "'interests':[" . $tempExploadArr4[1]."]";
                        $tempstr = 1; 
                    }
                    else if($tempExploadArr4[0] == 'behaviors'){
                        if($tempstr == 1){
                            $param .= ",'behaviors':[" . $tempExploadArr4[1]."]";
                        }
                        else{
                            $param .= "'behaviors':[" . $tempExploadArr4[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr4[0] == 'household_composition'){
                        if($tempstr == 1){
                            $param .= ",'household_composition':[" . $tempExploadArr4[1]."]";
                        }
                        else{
                            $param .= "'household_composition':[" . $tempExploadArr4[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr4[0] == 'generation'){
                        if($tempstr == 1){
                            $param .= ",'generation':[" . $tempExploadArr4[1]."]";
                        }
                        else{
                            $param .= "'generation':[" . $tempExploadArr4[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr4[0] == 'ethnic_affinity'){
                        if($tempstr == 1){
                            $param .= ",'ethnic_affinity':[" . $tempExploadArr4[1]."]";
                        }
                        else{
                            $param .= "'ethnic_affinity':[" . $tempExploadArr4[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr4[0] == 'industries'){
                        if($tempstr == 1){
                            $param .= ",'industries':[" . $tempExploadArr4[1]."]";
                        }
                        else{
                            $param .= "'industries':[" . $tempExploadArr4[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr4[0] == 'politics'){
                        if($tempstr == 1){
                            $param .= ",'politics':[" . $tempExploadArr4[1]."]";
                        }
                        else{
                            $param .= "'politics':[" . $tempExploadArr4[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr4[0] == 'family_statuses'){
                        if($tempstr == 1){
                            $param .= ",'family_statuses':[" . $tempExploadArr4[1]."]";
                        }
                        else{
                            $param .= "'family_statuses':[" . $tempExploadArr4[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr4[0] == 'life_events'){
                        if($tempstr == 1){
                            $param .= ",'life_events':[" . $tempExploadArr4[1]."]";
                        }
                        else{
                            $param .= "'life_events':[" . $tempExploadArr4[1]."]";
                        }
                        $tempstr = 1;
                    }
                    $param .= "},{";
                    $tempExploadArr5 = explode('$#$', $finalArr[$i][5]);
                    $tempstr = 0;
                    if($tempExploadArr5[0] == 'interests'){
                        $param .= "'interests':[" . $tempExploadArr5[1]."]";
                        $tempstr = 1; 
                    }
                    else if($tempExploadArr5[0] == 'behaviors'){
                        if($tempstr == 1){
                            $param .= ",'behaviors':[" . $tempExploadArr5[1]."]";
                        }
                        else{
                            $param .= "'behaviors':[" . $tempExploadArr5[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr5[0] == 'household_composition'){
                        if($tempstr == 1){
                            $param .= ",'household_composition':[" . $tempExploadArr5[1]."]";
                        }
                        else{
                            $param .= "'household_composition':[" . $tempExploadArr5[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr5[0] == 'generation'){
                        if($tempstr == 1){
                            $param .= ",'generation':[" . $tempExploadArr5[1]."]";
                        }
                        else{
                            $param .= "'generation':[" . $tempExploadArr5[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr5[0] == 'ethnic_affinity'){
                        if($tempstr == 1){
                            $param .= ",'ethnic_affinity':[" . $tempExploadArr5[1]."]";
                        }
                        else{
                            $param .= "'ethnic_affinity':[" . $tempExploadArr5[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr5[0] == 'industries'){
                        if($tempstr == 1){
                            $param .= ",'industries':[" . $tempExploadArr5[1]."]";
                        }
                        else{
                            $param .= "'industries':[" . $tempExploadArr5[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr5[0] == 'politics'){
                        if($tempstr == 1){
                            $param .= ",'politics':[" . $tempExploadArr5[1]."]";
                        }
                        else{
                            $param .= "'politics':[" . $tempExploadArr5[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr5[0] == 'family_statuses'){
                        if($tempstr == 1){
                            $param .= ",'family_statuses':[" . $tempExploadArr5[1]."]";
                        }
                        else{
                            $param .= "'family_statuses':[" . $tempExploadArr5[1]."]";
                        }
                        $tempstr = 1;
                    }
                    else if($tempExploadArr5[0] == 'life_events'){
                        if($tempstr == 1){
                            $param .= ",'life_events':[" . $tempExploadArr5[1]."]";
                        }
                        else{
                            $param .= "'life_events':[" . $tempExploadArr5[1]."]";
                        }
                        $tempstr = 1;
                    }
                    $param .= "}";
                    $param .= "]";

                }
                #$param .= ", 'exclusions':{'life_events':[{'id':6003054185372}]}";
                #$param .= ", 'flexible_spec':[{'behaviors':[{'id':6002714895372}],'interests':[{'id':6003107902433},{'id':6003139266461}]},{'interests':[{'id':6003020834693}],'life_events':[{'id':6002714398172}]}]"

                if (!empty($Clevnt)) {
                    $param .= ",'custom_audiences':[" . $Clevnt . "]";
                }

                if (!empty($relationship_statuses)) {
                    $param .= ",'relationship_statuses':[" . implode(',', $relationship_statuses) . "]";
                }

                if (!empty($interested_in)) {
                    $param .= ",'interested_in':[" . implode(',', $interested_in) . "]";
                }

                if (!empty($lng_locales)) {
                    $param .= ",'locales':[" . $lng_locales . "]";
                }
                if (!empty($education_statuses)) {
                    $param .= ",'education_statuses':[" . implode(',', $education_statuses) . "]";
                }
                if (!empty($education_schools)) {
                    $param .= ",'education_schools':[" . $education_schools . "]";
                }
                if (!empty($education_majors)) {
                    $param .= ",'education_majors':[" . $education_majors . "]";
                }

                if (!empty($work_employers)) {
                    $param .= ",'work_employers':[" . $work_employers . "]";
                }

                if (!empty($work_positions)) {
                    $param .= ",'work_positions':[" . $work_positions . "]";
                }
                
                if (!in_array('placement', $split_adset)){
                    //print_r($page_types);exit;
                    $pub_pla = "";
						$dev_pla = "";
						$fbpos = "";
						$instapos = "";
						//print_r($page_types);
						//exit;
                        if (!empty($page_types[0])) {
							$pub_pla = "'facebook'";
							$dev_pla = "'desktop'";
							$fbpos = "'feed'";
                            //$param .= ",'device_platforms':['desktop'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                        }
                        if (!empty($page_types[1])) {
							if (strpos($pub_pla, 'facebook') !== false){
								
							}
							else{
								$pub_pla .= "'facebook'";
							}
							if ($dev_pla != ""){
								$dev_pla .= ",'mobile'";
							}
							else{
								$dev_pla = "'mobile'";
							}
							if (strpos($fbpos, 'feed') !== false){
								
							}
							else{
								$fbpos .= "'feed'";
							}
                            //$param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                        }
                        if (!empty($page_types[2])) {
							if($pub_pla == ""){
								$pub_pla .= "'facebook','audience_network'";
							}
							else if(strpos($pub_pla, 'facebook') !== false){
								$pub_pla .= ",'audience_network'";
							}
							if($dev_pla == ""){
								$dev_pla .= "'mobile'";
							}
							else if(strpos($dev_pla, 'mobile') !== false){
								
							}
							else{
								$dev_pla .= ",'mobile'";
							}
							
							if($fbpos == ""){
								$fbpos .= "'feed','instant_article'";
							}
							else if(strpos($fbpos, 'feed') !== false){
								$fbpos .= ",'instant_article'";
							}
							else{
								$fbpos .= ",'feed','instant_article'";
							}
							
                            //$param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed', 'instant_article']";
                        }
                        if (!empty($page_types[3])) {
							if (strpos($pub_pla, 'facebook') !== false){
								
							}
							else if($pub_pla == ""){
								$pub_pla .= "'facebook'";
							}
							else{
								$pub_pla .= ",'facebook'";
							}
							if($fbpos == ""){
								$fbpos .= "'right_hand_column'";
							}
							else{
								$fbpos .= ",'right_hand_column'";
							}
							
                            //$param .= ",'publisher_platforms':['facebook'],'facebook_positions':['right_hand_column']";
                        }
                        if (!empty($page_types[4]) && $optimize_for != "POST_ENGAGEMENT" && $optimize_for != "LEAD_GENERATION") {
							if($pub_pla == ""){
								$pub_pla .= "'instagram'";
							}
							else{
								$pub_pla .= ",'instagram'";
							}
                            //$param .= ",'publisher_platforms':['instagram']";
							$instapos = "'stream'";
                        }
						//$param .= ",";
						
						if($dev_pla != ""){
							$param .= ",'device_platforms':[".$dev_pla."]";
						}
						if($pub_pla != ""){
							$param .= ",'publisher_platforms':[".$pub_pla."]";
						}
						if($fbpos != ""){
							$param .= ",'facebook_positions':[".$fbpos."]";
						}
						if($instapos != ""){
							$param .= ",'instagram_positions':[".$instapos."]";
						}
                  /*  if (!empty($page_types[0])) {
                        $param .= ",'device_platforms':['desktop'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                    }
                    else if (!empty($page_types[1])) {
                        $param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                    }
                    else if (!empty($page_types[2])) {
                        $param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed', 'instant_article']";
                    }
                    else if (!empty($page_types[3])) {
                        $param .= ",'publisher_platforms':['facebook'],'facebook_positions':['right_hand_column']";
                    }
                    else if (!empty($page_types[4])) {
                        $param .= ",'publisher_platforms':['instagram']";
                    }
                    */
                }
                else{
                    if($finalArr[$i][6] == 'DNF'){
                        $param .= ",'device_platforms':['desktop'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                    }
                    else if($finalArr[$i][6] == 'MNF'){
                        $param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed']";
                    }
                    else if($finalArr[$i][6] == 'TPMS'){
                        $param .= ",'device_platforms':['mobile'],'publisher_platforms':['facebook'],'facebook_positions':['feed', 'instant_article']";
                    }
                    else if($finalArr[$i][6] == 'DRHS'){
                        $param .= ",'publisher_platforms':['facebook'],'facebook_positions':['right_hand_column']";
                    }
                    else if($finalArr[$i][6] == 'instagram'){
                        $param .= ",'publisher_platforms':['instagram']";
                    }
                }
               // print_r($finalArr[$i][6]); exit;
                $param .= "}";
            #echo "<pre>";print_r($param); echo "</pre>";
                //Write log
				/*$param = "optimize_for=PAGE_LIKES&targeting_spec={'age_min':18,'age_max':65+,'geo_locations': {'cities':[{'key':'1814658'}],'countries':['US']},'flexible_spec':[{'interests':[]}],'device_platforms':['desktop'],'publisher_platforms':['facebook'],'facebook_positions':['feed']}";*/
				
				/*{'age_min':18,'age_max':65+,'geo_locations': {'cities'
:[{'key':'1814658'}],'countries':['US']}, 'flexible_spec':[{'interests':[Array]}],'device_platforms'
:['desktop'],'publisher_platforms':['facebook'],'facebook_positions':['feed']}*/
				
				//$param = 'optimize_for=LINK_CLICKS&targeting_spec={"geo_locations":{"cities":[{"key":"1814658"}]},"age_min":18,"age_max":65+,"flexible_spec":[],"custom_audiences":[{"id":23842528174340160}]}';
				//$param = 'optimize_for=LINK_CLICKS&targeting_spec={"age_min":18,"age_max":65+,"flexible_spec":[],"custom_audiences":[{"id":23842528174340160}],"device_platforms":["desktop"],"publisher_platforms":["facebook"],"facebook_positions":["feed"]}';
                $this->writeLog('audience', $param);

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/act_" . $ad_account . "/reachestimate?" . $param . "&access_token=" . $this->access_token;
               // echo $url."<br>";

                $url = str_replace("  ", "%20", $url);
                $url = str_replace(" ", "%20", $url);
				$url = str_replace("Array", "", $url);
                $ch = curl_init($url);
                // echo "<br>".$url."<br>";
                // exit;
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result, true);
                // echo "<pre>";print_r($response);
                // exit;

                if (isset($response['data'])) {
                    //$audience = number_format($response['data']['users']) . " People";
                    $audience = number_format($response['data']['users']);
                    /*$tempStr1 .= '<tr>';
                    $tempStr1 .= '<td class="number">#'.($i+1).'</td>';
                    $tempStr1 .= '<td class="title">Estimated Audience Size</td>';
                    $tempStr1 .= '<td>'.$audience.'</td>';
                    $tempStr1 .= '</tr>';*/
                    $tempStr1 .= '<div class="ad-set">';
                    $tempStr1 .= '<div class="header d-flex flex-wrap align-items-end">';
                    $tempStr1 .= '<strong class="ad-set-titile">Adset #'.($i+1).'</strong>';
                    $tempStr1 .= '<div class="audience">';
                    $tempStr1 .= '<strong>'.$audience.'</strong>';
                    $tempStr1 .= '<span>People</span>';
                    $tempStr1 .= '</div>';
                    $tempStr1 .= '</div>';
                    $tempStr1 .= '</div>';
                    $totalAudience += $response['data']['users'];
                } else {
                    $audience = "Sorry! something went wrong";
                }
            }
        }
        //echo "junaid";
        //exit;
		$interestcount = $intarrcount;
        $finalArr1 = array();
        $finalstr = !empty($tempStr1) ? $tempStr1 : '';
        array_push($finalArr1, $finalstr);
        $sitePreArr['dataStr'] = $finalArr1;
        $sitePreArr['count'] = !empty($tempStr1) ? count($finalArr) : '0';
      
        $sitePreArr['totalAudience'] = !empty($tempStr1) ? $totalAudience : '0';
		$sitePreArr['locationcount'] = !empty($locationcount) ? $locationcount : '0';
		$sitePreArr['interestcount'] = !empty($interestcount) ? $interestcount : '0';
		$sitePreArr['placementcount'] = !empty($placementcount) ? $placementcount : '0';
		$sitePreArr['cacount'] = !empty($cacount) ? $cacount : '0';
        $data = json_encode($sitePreArr);
        echo $data;
        exit;
        //echo $str.'#@#@#@'.count($finalArr);exit;
    }

    function videoUploadOnFb($filePath, $account_id){
        #$filePath = 'http://localhost/tcm2/uploads/SampleVideo_1280x720_2mb.mp4';
        $url_arr = explode('/', $filePath);
        $ct = count($url_arr);
        $name = $url_arr[$ct - 1];
        $name_div = explode('.', $name);
        $ct_dot = count($name_div);
        $img_type = $name_div[$ct_dot - 1];
        $r =  FCPATH . "uploads/campaign/".$this->user_id."/" . $name;

        $video = new Advideo(null, "act_".$account_id);
        $video->{AdVideoFields::SOURCE} = $r;
        $video->create();
        return $video->id;
    }

    function fngetallvideos(){
        $userId = $_POST['userId'];
        $Id = $_POST['Id'];
        $adAccountId = $this->input->post('adAccountId');
        $adAccountId = 'act_' . $adAccountId;

        $str = '';
        $class = '';
        $whereArr = array('user_id' => $userId, 'adAccountId' => $this->input->post('adAccountId'));
        $userArr = $this->Spendcron_model->getData('user_ad_videos', $whereArr);

        if(!empty($_POST['campaignId']))
            $draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $_POST['campaignId']);
        
        $tempImg = array();
        if(!empty($draft_data)){
            $tempImg[] = $draft_data->adimage1; 
            $tempImg[] = $draft_data->adimage2; 
            $tempImg[] = $draft_data->adimage3; 
            $tempImg[] = $draft_data->adimage4; 
            $tempImg[] = $draft_data->adimage5; 
            $tempImg[] = $draft_data->product1image; 
            $tempImg[] = $draft_data->product2image; 
            $tempImg[] = $draft_data->product3image; 
        }

        if(!empty($userArr)){
            foreach ($userArr as $key => $value) {
                $class = '';
                if(in_array($value->img_hash, $tempImg)){
                    $class = 'imgSelect';
                }
               $str .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$value->video_id.'\', \''.$userId.'\', \''.$Id.'\', \''.$value->img_hash.'\')"><img id="'.$value->video_id.'-'.$Id.'"  class="img-responsive img-thumbnail video-thumb '.$class.'" src="'.$value->url.'" /></a></div>';
			          
            }
        }

        echo $str;exit;
    }

    function fngetallvideosLive(){
		//echo "manobilli"; exit;
        $userId = $_POST['userId'];
        $Id = $_POST['Id'];
        $Id = 'gallery';
        $adAccountId = $this->input->post('adAccountId');
        $adAccountId = 'act_' . $adAccountId;
		$pageID = $this->input->post('pageId');//'171962383153994';

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/advideos?fields=source,title,picture&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        
        $response = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($response);
        $str = '';
        $finalArr = array();
       // echo '<pre>';
		//print_r($response);
		//exit;
        if (isset($results->error)) {
            // $option = "<option value=''>No Record Found!</option>";
        } else {
            if ($results->data) {
                $vidlblcounter=0;
                foreach ($results->data as $result) {

                    $url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$result->id."/thumbnails?access_token=" . $this->access_token;
        
                    $ch = curl_init($url1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    
                    $response1 = curl_exec($ch);
                    curl_close($ch);
                    $results1 = json_decode($response1);
                    $imgArr = explode('/', $results1->data[0]->uri);

                    /*$imgApp1 = explode('?', end($imgArr));
                    $temp = $imgApp1[0];
                    $imgArr = explode('/', $result->picture);
                    $imgApp1 = explode('.', end($imgArr));
                    $temp = $imgApp1[0].rand();
                    $tempName = $imgApp1[0].'-'.$Id;*/
					
					if(!empty($_POST['campaignId']))
                    $draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $_POST['campaignId']);
                
                $tempImg = array();
                if(!empty($draft_data)){
                    $tempImg[] = $draft_data->adimage1; 
                    $tempImg[] = $draft_data->adimage2; 
                    $tempImg[] = $draft_data->adimage3; 
                    $tempImg[] = $draft_data->adimage4; 
                    $tempImg[] = $draft_data->adimage5; 
                    $tempImg[] = $draft_data->product1image; 
                    $tempImg[] = $draft_data->product2image; 
                    $tempImg[] = $draft_data->product3image; 
					$tempImg[] = $draft_data->fbimghash1;
					$tempImg[] = $draft_data->fbimghash2;
					$tempImg[] = $draft_data->fbimghash3;
					$tempImg[] = $draft_data->fbimghash4;
					$tempImg[] = $draft_data->fbimghash5;
                }
					
					//print_r($imgArr);echo "junaid";print_r($tempImg);echo "Nouman";echo $result->id; exit;
					
                    $class = '';
					$scriptcall = '';
					if(in_array($result->id, $tempImg)){
                        //$class = 'imgSelect';
						$scriptcall = '<script type="text/javascript">fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results1->data[0]->uri.'\');</script>';
						
                    }
                    /*if(in_array(end($imgArr), $tempImg)){
                        $class = 'imgSelect';
                    }*/
                    //$str .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.end($imgArr).'\')"><img id="'.$result->id.'-'.$Id.'"  class="img-responsive img-thumbnail video-thumb '.$class.'" src="'.$results1->data[0]->uri.'" /></a></div>';
					 //$str .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results1->data[0]->uri.'\')"><img id="'.$result->id.'-'.$Id.'"  class="img-responsive img-thumbnail video-thumb '.$class.'" src="'.$results1->data[0]->uri.'" /></a></div>'.$scriptcall;
					$vidlblcounter++; 
					$str .= '<div class="media-item '.$_POST['campaignId'].'">
															<input id="img-'.$vidlblcounter.'" type="checkbox">
															<label for="img-'.$vidlblcounter.'" onclick="fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results1->data[0]->uri.'\')">
																<span class="img-holder">
																	<img class="'.$Id.' video-thumb '.$class.'" src="'.$results1->data[0]->uri.'" alt="image description" id="'.$result->id.'-'.$Id.'">
																</span>
																
															</label>
														</div>';
                }
            }
            $flag = true;
            if(empty($results->paging->cursors->after)){
                $flag = false;
            }
            else{
                $nexturl = $url."&after=".$results->paging->cursors->after;
            }
            while ($flag) {
                $ch = curl_init($nexturl);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                
                $response = curl_exec($ch);
                curl_close($ch);
                $results = json_decode($response);
                if ($results->data) {
                    foreach ($results->data as $result) {

                        $url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$result->id."/thumbnails?access_token=" . $this->access_token;
        
                        $ch = curl_init($url1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        
                        $response1 = curl_exec($ch);
                        curl_close($ch);
                        $results1 = json_decode($response1);
                       
                        $imgArr = explode('/', $results1->data[0]->uri);
                        $imgApp1 = explode('.', end($imgArr));
                        $temp = $imgApp1[0].rand();
                        $tempName = $imgApp1[0].'-'.$Id;
                        
                        
                        /*$imgArr = explode('/', $result->picture);
                        $imgApp1 = explode('.', end($imgArr));
                        $temp = $imgApp1[0].rand();
                        $tempName = $imgApp1[0].'-'.$Id;*/

                        $imgArr = explode('/', $result->picture);
                        $imgApp1 = explode('.', end($imgArr));
                        $temp = $imgApp1[0].rand();
                        $tempName = $imgApp1[0].'-'.$Id;
						
						
						
						if(!empty($_POST['campaignId']))
                    $draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $_POST['campaignId']);
                
                $tempImg = array();
                if(!empty($draft_data)){
                    $tempImg[] = $draft_data->adimage1; 
                    $tempImg[] = $draft_data->adimage2; 
                    $tempImg[] = $draft_data->adimage3; 
                    $tempImg[] = $draft_data->adimage4; 
                    $tempImg[] = $draft_data->adimage5; 
                    $tempImg[] = $draft_data->product1image; 
                    $tempImg[] = $draft_data->product2image; 
                    $tempImg[] = $draft_data->product3image; 
					$tempImg[] = $draft_data->fbimghash1;
					$tempImg[] = $draft_data->fbimghash2;
					$tempImg[] = $draft_data->fbimghash3;
					$tempImg[] = $draft_data->fbimghash4;
					$tempImg[] = $draft_data->fbimghash5;
                }
					
					//print_r($imgArr);echo "junaid";print_r($tempImg);echo "Nouman";echo $result->id; exit;
					
                    $class = '';
					$scriptcall = '';
					if(in_array($result->id, $tempImg)){
                        //$class = 'imgSelect';
						$scriptcall = '<script type="text/javascript">fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results1->data[0]->uri.'\');</script>';
						
                    }
                        /*$class = '';
                        if(in_array(end($imgArr), $tempImg)){
                            $class = 'imgSelect';
                        }*/
                        //$str .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.end($imgArr).'\')"><img id="'.$result->id.'-'.$Id.'"  class="img-responsive video-thumb img-thumbnail '.$class.'" src="'.$results1->data[0]->uri.'" /></a></div>';
						//$str .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results1->data[0]->uri.'\')"><img id="'.$result->id.'-'.$Id.'"  class="img-responsive video-thumb img-thumbnail '.$class.'" src="'.$results1->data[0]->uri.'" /></a></div>'.$scriptcall;
						$str .= '<div class="media-item '.$_POST['campaignId'].'">
															<input id="img-01" type="checkbox">
															<label for="img-01" onclick="fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results1->data[0]->uri.'\')">
																<span class="img-holder">
																	<img class="'.$Id.' video-thumb '.$class.'" src="'.$results1->data[0]->uri.'" alt="image description" id="'.$result->id.'-'.$Id.'">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">IMAGE</span>
																	<time datetime="2017-06-01">01 Jun 2017</time>
																</span>
															</label>
														</div>';
                    }
                }
                if(empty($results->paging->cursors->after)){
                    $flag = false;
                }
                else{
                    $nexturl = $url."&after=".$results->paging->cursors->after;
                }
            }
        }

		//strat by junaid
		
		/*
		
		$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/171962383153994/videos?fields=source,title,picture&access_token=" . $this->access_token;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		
		
		$url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/457190207964542/thumbnails?access_token=" . $this->access_token;
        
                    $ch1 = curl_init($url1);
                    curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, true);
                    
                    $response1 = curl_exec($ch1);
                    curl_close($ch1);
                    $results1 = json_decode($response1);
		echo "<pre>";
		print_r($results1);
		echo "</pre>";			
		
		exit;
		
		*/
		
		$str1 = '';
		if(isset($pageID) && !empty($pageID)){
		
			$url2 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$pageID/videos?fields=source,title,picture&access_token=" . $this->access_token;
			
			$ch = curl_init($url2);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			
			$response2 = curl_exec($ch);
			curl_close($ch);
			$results2 = json_decode($response2);
			$finalArr2 = array();
// 			echo '<pre>';
// 			print_r($results2);
// 			exit;
			if (isset($results2->error)) {
				// $option = "<option value=''>No Record Found!</option>";
			} else {
				if ($results2->data) {
					foreach ($results2->data as $result2) {
	
						
						$url12 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$result2->id."/thumbnails?access_token=" . $this->access_token;
			
						$ch = curl_init($url12);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
						
						$response12 = curl_exec($ch);
						curl_close($ch);
						$results12 = json_decode($response12);
						
				// 		echo '<pre>';
				// 		print_r($results12);
				// 		exit;
						
						$imgArr2 = explode('/', $results12->data[0]->uri);
				// 		print_r($imgArr2);
				// 		exit;
	
						/*$imgApp1 = explode('?', end($imgArr));
						$temp = $imgApp1[0];
						$imgArr = explode('/', $result->picture);
						$imgApp1 = explode('.', end($imgArr));
						$temp = $imgApp1[0].rand();
						$tempName = $imgApp1[0].'-'.$Id;*/
						
						if(!empty($_POST['campaignId']))
						$draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $_POST['campaignId']);
					
					$tempImg = array();
					if(!empty($draft_data)){
						$tempImg[] = $draft_data->adimage1; 
						$tempImg[] = $draft_data->adimage2; 
						$tempImg[] = $draft_data->adimage3; 
						$tempImg[] = $draft_data->adimage4; 
						$tempImg[] = $draft_data->adimage5; 
						$tempImg[] = $draft_data->product1image; 
						$tempImg[] = $draft_data->product2image; 
						$tempImg[] = $draft_data->product3image; 
						$tempImg[] = $draft_data->fbimghash1;
						$tempImg[] = $draft_data->fbimghash2;
						$tempImg[] = $draft_data->fbimghash3;
						$tempImg[] = $draft_data->fbimghash4;
						$tempImg[] = $draft_data->fbimghash5;
					}
						
						//print_r($imgArr);echo "junaid";print_r($tempImg);echo "Nouman";echo $result->id; exit;
						
						$class = '';
						$scriptcall = '';
						if(in_array($result2->id, $tempImg)){
							//$class = 'imgSelect';
							$scriptcall = '<script type="text/javascript">fnSelectGalleryVideo(\''.$result2->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results12->data[0]->uri.'\');</script>';
							
						}
						/*if(in_array(end($imgArr), $tempImg)){
							$class = 'imgSelect';
						}*/
						//$str .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.end($imgArr).'\')"><img id="'.$result->id.'-'.$Id.'"  class="img-responsive img-thumbnail video-thumb '.$class.'" src="'.$results1->data[0]->uri.'" /></a></div>';
						//$str1 .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$result2->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results12->data[0]->uri.'\')"><img id="'.$result2->id.'-'.$Id.'"  class="img-responsive img-thumbnail video-thumb '.$class.'" src="'.$results12->data[0]->uri.'" /></a></div>'.$scriptcall;
						$str .= '<div class="media-item '.$_POST['campaignId'].'">
															<input id="img-'.$result2->id.'" type="checkbox">
															<label for="img-'.$result2->id.'" onclick="fnSelectGalleryVideo(\''.$result2->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results12->data[0]->uri.'\')">
																<span class="img-holder">
																	<img class="'.$Id.' video-thumb '.$class.'" src="'.$results12->data[0]->uri.'" alt="image description" id="'.$result2->id.'-'.$Id.'">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">IMAGE</span>
																	<time datetime="2017-06-01">01 Jun 2017</time>
																</span>
															</label>
														</div>';
					}
				}
				$flag2 = true;
				if(empty($results2->paging->cursors->after)){
					$flag2 = false;
				}
				else{
					$nexturl2 = $url2."&after=".$results2->paging->cursors->after;
				}
				while ($flag2) {
					$ch = curl_init($nexturl2);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
					
					$response2 = curl_exec($ch);
					curl_close($ch);
					$results2 = json_decode($response2);
					if ($results2->data) {
						foreach ($results2->data as $result2) {
	
							$url12 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$result2->id."/thumbnails?access_token=" . $this->access_token;
			
							$ch = curl_init($url12);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
							
							$response12 = curl_exec($ch);
							curl_close($ch);
							$results12 = json_decode($response12);
							$imgArr = explode('/', $results12->data[0]->uri);
							$imgApp12 = explode('.', end($imgArr));
							$temp = $imgApp12[0].rand();
							$tempName = $imgApp12[0].'-'.$Id;
							/*$imgArr = explode('/', $result->picture);
							$imgApp1 = explode('.', end($imgArr));
							$temp = $imgApp1[0].rand();
							$tempName = $imgApp1[0].'-'.$Id;*/
	
							$imgArr = explode('/', $result2->picture);
							$imgApp12 = explode('.', end($imgArr));
							$temp = $imgApp12[0].rand();
							$tempName = $imgApp12[0].'-'.$Id;
							
							
							
							if(!empty($_POST['campaignId']))
						$draft_data = $this->Campaigndarft_model->getCmpDraft($this->user_id, $_POST['campaignId']);
					
					$tempImg = array();
					if(!empty($draft_data)){
						$tempImg[] = $draft_data->adimage1; 
						$tempImg[] = $draft_data->adimage2; 
						$tempImg[] = $draft_data->adimage3; 
						$tempImg[] = $draft_data->adimage4; 
						$tempImg[] = $draft_data->adimage5; 
						$tempImg[] = $draft_data->product1image; 
						$tempImg[] = $draft_data->product2image; 
						$tempImg[] = $draft_data->product3image; 
						$tempImg[] = $draft_data->fbimghash1;
						$tempImg[] = $draft_data->fbimghash2;
						$tempImg[] = $draft_data->fbimghash3;
						$tempImg[] = $draft_data->fbimghash4;
						$tempImg[] = $draft_data->fbimghash5;
					}
						
						//print_r($imgArr);echo "junaid";print_r($tempImg);echo "Nouman";echo $result->id; exit;
						
						$class = '';
						$scriptcall = '';
						if(in_array($result2->id, $tempImg)){
							//$class = 'imgSelect';
							$scriptcall = '<script type="text/javascript">fnSelectGalleryVideo(\''.$result2->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results12->data[0]->uri.'\');</script>';
							
						}
							/*$class = '';
							if(in_array(end($imgArr), $tempImg)){
								$class = 'imgSelect';
							}*/
							//$str .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$result->id.'\', \''.$userId.'\', \''.$Id.'\', \''.end($imgArr).'\')"><img id="'.$result->id.'-'.$Id.'"  class="img-responsive video-thumb img-thumbnail '.$class.'" src="'.$results1->data[0]->uri.'" /></a></div>';
							$str1 .= '<div align="center" class="col-md-6 col-sm-6 '.$_POST['campaignId'].'"><a class="gallery-img" onclick="fnSelectGalleryVideo(\''.$result2->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results12->data[0]->uri.'\')"><img id="'.$result2->id.'-'.$Id.'"  class="img-responsive video-thumb img-thumbnail '.$class.'" src="'.$results12->data[0]->uri.'" /></a></div>'.$scriptcall;
							$str .= '<div class="media-item '.$_POST['campaignId'].'">
															<input id="img-01" type="checkbox">
															<label for="img-01" onclick="fnSelectGalleryVideo(\''.$result2->id.'\', \''.$userId.'\', \''.$Id.'\', \''.$results12->data[0]->uri.'\')">
																<span class="img-holder">
																	<img class="'.$Id.' video-thumb '.$class.'" src="'.$results12->data[0]->uri.'" alt="image description" id="'.$result2->id.'-'.$Id.'">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">IMAGE</span>
																	<time datetime="2017-06-01">01 Jun 2017</time>
																</span>
															</label>
														</div>';
						}
					}
					if(empty($results2->paging->cursors->after)){
						$flag2 = false;
					}
					else{
						$nexturl2 = $url2."&after=".$results2->paging->cursors->after;
					}
				}
			}
		
		}
		
		
		
		//end by junaid

        $str2 = $str1 . $str;
        
		echo $str2;exit;
    }

    function video(){
        $filePath = 'http://localhost/tcm2/uploads/SampleVideo_1280x720_2mb.mp4';
        $url_arr = explode('/', $filePath);
        $ct = count($url_arr);
        $name = $url_arr[$ct - 1];
        $name_div = explode('.', $name);
        $ct_dot = count($name_div);
        $img_type = $name_div[$ct_dot - 1];
        $r =  FCPATH . "uploads/campaign/".$this->user_id."/" . $name;
        $this->access_token = 'EAARCQQNbaOkBAEXoS18OLsakul5ZAGV05O47tQ9ZBacwtiVyFJhDKobeR5YciEChT3JZBbUaotgXzuHqjZAuZAy52LGMcUheCVKMommJBXrUIEZCbc2bHngSINMGj8phZASddCxcBj5zuqqqpSv6WMykCGxd5719XUZD';
        
        $adAccountId = "act_104413260013985";


        /*$ch = curl_init('https://graph.facebook.com/'.$this->config->item('facebook_graph_version').'/act_104413260013985/adcreatives');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $hasedImage1 = $this->getHashedImage('https://scontent.xx.fbcdn.net/t45.1600-4/16641356_23842549191480160_9054125367337418752_n.png');

        $data_array = array(
            'access_token' => $this->access_token,
            'name' => 'video test',
            'object_story_spec' => '{
                    "video_data":{
                        "call_to_action":{
                            "type":"LIKE_PAGE",
                            "value":{
                                "page":"171962383153994"
                            }
                        },
                    "description":"video message",
                    "image_url":"'.$hasedImage1.'",
                    "video_id": "217022578753052" 
                },
                "page_id":"171962383153994"
            }'
        );
        curl_setopt_array($ch, array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $data_array
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        $this->writeLog("websiteClicks_creative_ad", $data_array);
        $result = json_decode($response);
        echo "<PRE>";print_R($result);exit;*/

        /*$video = new Advideo(null, "act_104413260013985");
        $video->{AdVideoFields::SOURCE} = $r;
        #$video->{AdImageFields::FILENAME} = $this->getImageAbsloutePath($filePath);
        $video->create();
        echo "===>".$video->id;
        echo "<PRE>";print_R($video);*/

        
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/".$adAccountId."/advideos&access_token=" . $this->access_token;

        /*$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/
        261348307653812/thumbnails?access_token=" . $this->access_token;*/
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        
        $response = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($response);
        echo "<PRE>";print_R($results);exit;

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/261340534321256/thumbnails?access_token=" . $this->access_token;
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        
        $response = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($response);

        echo "<PRE>";print_R($results);exit;

        /*echo $url = "https://graph-video.facebook.com/v2.8/act_104413260013985/advideos?access_token=".$this->access_token."&source=@".$r;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch,CURLOPT_TIMEOUT,0);
        $response = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($response);
        echo "<PRE>";print_R($results);
        exit;*/
        return $video->id;
    }
}
//End of class