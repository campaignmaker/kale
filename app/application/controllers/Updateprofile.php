<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Updateprofile extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public $user_id;

    public function __construct() {
        parent::__construct();
        error_reporting(0);
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}
        
        $this->load->model('Users_model');
        $this->load->model('Upgrade_model');
        $this->load->library("availability_limit");
        $this->seo->SetValues('Title', "Facebook campaign profile");
        $this->seo->SetValues('Description', "Facebook campaign profile");
        $this->user_id = $this->session->userdata['logged_in']['id'];
    }

    public function index($cancel = NULL) {
        if ($this->user_id) {
            $data = new stdClass();
            $data->SubscriptionDetail = $this->Upgrade_model->userCurrentSubscriptionDetail($this->user_id);
            $data->Availability_limit = $this->availability_limit->getUsedLimit();
			

            if ($cancel != 'cancel') {

                // set validation rules
                $this->form_validation->set_rules('first_name', 'First Name', 'trim|xss_clean|required|min_length[2]');
                $this->form_validation->set_rules('last_name', 'Last Name', 'trim|xss_clean|required|min_length[2]');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|xss_clean|required');
                $this->form_validation->set_rules('country', 'Country', 'trim|xss_clean|required');

                if ($this->form_validation->run() === false) {
                    
                } else {

                    // set variables from the form
                    $user = array('first_name' => $this->input->post('first_name'), 'last_name' => $this->input->post('last_name'), 'phone' => $this->input->post('phone'), 'country' => $this->input->post('country'), 'company' => $this->input->post('company'));

                    $userId = $this->Users_model->edit_profile($user, $this->user_id);

                    if ($userId) {
                        $data->success = 'Profile update successfully.';
                    } else {
                        $data->error = 'There was a problem. Please try again.';
                    }
                }
            } else {
                list($data->success, $data->error) = $this->subscriptioncancel();
            }

            $data->user = $this->Users_model->get_user($this->user_id);
            $data->monthlySpend = $this->getMonthlySpend();
			$data->usersubscription = $this->Users_model->get_user_subscription($this->user_id);
		
            #echo "<PRE>";print_R($data);exit;
            $data->nextinvoicedate = $this->getinvoicedue();
            loadView('profileupdate', $data);
        } else {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
        }
    }
    public function cancelusersubscription(){
        try{
        $this->load->model('Users_model');
        include_once ($_SERVER['DOCUMENT_ROOT']."/aff/API/stripe/init.php");
        \Stripe\Stripe::setApiKey('sk_live_JEgEezB4NK9jSIEje15zkyn3');
        $this->user_id = $this->session->userdata['logged_in']['id'];
		$myuser = $this->Users_model->get_user($this->user_id);
		//sub_BnvHiU8jNJ73jI
		$subscription = \Stripe\Subscription::retrieve($myuser->stripe_subscription_id);
		$subscription->cancel();
		$this->Users_model->update_user_subid($this->user_id);
		redirect(site_url('/updateprofile'));
		//echo "<pre>";
		//print_r($subscription); echo "</pre>"; exit;
        } catch (Exception $e) {
            console.log($e->getMessage());
            $this->Users_model->update_user_subid($this->user_id);
            redirect(site_url('/updateprofile'));
    	  }
        
		
        redirect(site_url('/updateprofile'));
    }
    
//Samy Code Start - adding invoice due date

    public function getinvoicedue(){
        try{
            $this->load->model('Users_model');
            require_once(APPPATH.'libraries/stripe/Stripe.php');
            //include_once ($_SERVER['DOCUMENT_ROOT']."/aff/API/stripe/init.php");
            Stripe::setApiKey("sk_live_JEgEezB4NK9jSIEje15zkyn3");
            //\Stripe\Stripe::setApiKey('sk_live_JEgEezB4NK9jSIEje15zkyn3');
            $this->user_id = $this->session->userdata['logged_in']['id'];
            $myuser = $this->Users_model->get_user($this->user_id);
          
            $invoicedue = Stripe_Invoice::upcoming(array('customer' => $myuser->stripe_customer_id));
            //$invoicedue = \Stripe\Invoice::upcoming($myuser->stripe_customer_id);

            $mytimestamp = $invoicedue["date"];

            $mynextinvdate = gmdate("d/m/Y", $mytimestamp);
            
            if(!isset($mynextinvdate)){
                return "nothing";
            }
            return $mynextinvdate;
            
        } catch (Exception $e) {
           // print_r($e->getMessage()); exit;
            console.log($e->getMessage());
            //redirect(site_url('/updateprofile'));
        }
    

        //redirect(site_url('/updateprofile'));
        
    }

        //Samy code end
    
    
    function getMonthlySpend() {
        $where = array('user_id' => $this->user_id);
        $userSubscriptionArr = $this->Upgrade_model->getData('user_subscription', $where);
        
        //$billing_start_date = date('Y-m-01');//$userSubscriptionArr['0']['billing_start_date'];
        $billing_start_date = $userSubscriptionArr['0']['billing_start_date'];
        //$billing_end_date = date('Y').'-'.date('m').'-'.date('d');;//$userSubscriptionArr['0']['billing_end_date'];
        $billing_end_date = $userSubscriptionArr['0']['billing_end_date'];
        $status = $userSubscriptionArr['0']['status'];
        $packages_id = $userSubscriptionArr['0']['packages_id'];

        $where = array('user_id' => $this->user_id);
        $userAdsAccountArr = $this->Upgrade_model->getData('user_ad_account', $where);

        $where = array('id' => $packages_id);
        $packagesArr = $this->Upgrade_model->getData('packages', $where);
        $monthly_limit = $packagesArr[0]['monthly_limit'];

        $userAdsStr = '';
        if (!empty($userAdsAccountArr)) {
            foreach ($userAdsAccountArr as $k => $v) {
                $userAdsStr .= '"' . $v['ad_account_id'] . '",';
            }
            $userAdsStr = rtrim($userAdsStr, ',');
        }
        
        $where = array('user_id' => $this->session->userdata['logged_in']['id']);
        $whereCustom = '';
        if(!empty($userAdsStr)){
            $whereCustom = 'ad_account_id in ('.$userAdsStr.')';
        }
        $userSpendArr = $this->Upgrade_model->getDataTotalSpend('user_daily_spend', $where, $whereCustom);
        $totalAccountSpend = $userSpendArr[0]->spend;
            
        if($this->session->userdata['cur_currency'] == 'CO$'){
            $totalAccountSpend = $totalAccountSpend / 3000;
            /*$1 = 0.89 Euro
            $1 = 3,000 colombian pesos
            $1 = 1.30 Australian Dollar
            $1 = 1.30 Canadian Dollar
            $1 = 66.77 Indian Rupee
            $1 = 18.30 Mexican Pesos
            $1 = 0.77 GBP*/
        }
        else if($this->session->userdata['cur_currency'] == '£'){
            $totalAccountSpend = $totalAccountSpend / 0.89;
        }
        else if($this->session->userdata['cur_currency'] == 'AU$'){
            $totalAccountSpend = $totalAccountSpend / 1.30;
        }
        else if($this->session->userdata['cur_currency'] == 'CA$'){
            $totalAccountSpend = $totalAccountSpend / 1.30;
        }
        else if($this->session->userdata['cur_currency'] == 'INR'){
            $totalAccountSpend = $totalAccountSpend / 66.77;
        }
        else if($this->session->userdata['cur_currency'] == 'MAX$'){
            $totalAccountSpend = $totalAccountSpend / 18.30;
        }
        else if($CI->session->userdata['cur_currency'] == 'Rp'){
            $totalAccountSpend = $totalAccountSpend / 13037;
        }
            
        /*$where = array('user_id' => $this->user_id);
        if(!empty($userAdsStr)){
            $whereCustom = 'adaccountid in (' . $userAdsStr . ') and campaignstatus = "ACTIVE"';
        }
        else{
            $whereCustom = 'campaignstatus = "ACTIVE"';
        }
        $userCampaignArr = $this->Upgrade_model->getData('campaigns', $where, $whereCustom);

        $userCampaignStr = '';
        if (!empty($userCampaignArr)) {
            foreach ($userCampaignArr as $k1 => $v1) {
                $userCampaignStr .= '"' . $v1['id'] . '",';
            }
            $userCampaignStr = rtrim($userCampaignStr, ',');
        }

        $where = array('uid' => $this->user_id);
        //$whereCustom = 'campaign_id in ('.$userCampaignStr.')';
        if(!empty($userCampaignStr)){
            $whereCustom = 'campaign_id in (' . $userCampaignStr . ') and DATE_FORMAT(created_at, "%Y-%m-%d") BETWEEN "' . $billing_start_date . '" AND "' . $billing_end_date . '"';
        }
        else{
            $whereCustom = 'DATE_FORMAT(created_at, "%Y-%m-%d") BETWEEN "' . $billing_start_date . '" AND "' . $billing_end_date . '"';
        }
        $userBudgetArr = $this->Upgrade_model->getData('user_budget', $where, $whereCustom);
        #echo $this->db->last_query();exit;

        $totalAccountSpend = 0;
        if (!empty($userBudgetArr)) {
            $campaignWiseSpend = 0;
            foreach ($userBudgetArr as $k2 => $v2) {
                $startDate = strtotime($v2['start_date']);
                $endDate = strtotime(date('Y-m-d'));

                $days_between = ceil(abs($endDate - $startDate) / 86400);
                $campaignWiseSpend = $days_between * $v2['budgetamount'];
                $totalAccountSpend = $totalAccountSpend + $campaignWiseSpend;
            }
        }*/
        return $totalAccountSpend;
    }

    function subscriptioncancel() {

        $success = NULL;
        $error = NULL;
        try {

            $uSd = $this->Upgrade_model->userSubscriptionDetail($this->user_id);
            if ($uSd) {
                $result = Braintree_Subscription::cancel($uSd->subscription_id);
                if ($result->success) {
                    $this->Users_model->edit_profile(array('subscription_status' => 0), $this->user_id);
                    $success .= "<p>You have successfully Un-subscribed package</p>";
                } else {
                    foreach ($result->errors->deepAll() AS $err) {
                        $error .= "<p>" . $err->code . ": " . $err->message . "</p>";
                    }
                }
            } else {
                $error .= "<p>You have not subscribe any package</p>";
            }
        } catch (Exception $ex) {
            $error .= "<p>" . $ex . "<p>";
        }

        return array($success, $error);
    }
	
	public function sendemailaccountchange(){
		//print_r($this->input->post());
		//echo "junaid";
		try{
		$to = "samy@thecampaignmaker.com";
$subject = "REQUEST ACCOUNT CHANGE";

$message = "
<html>
<head>
<title>REQUEST ACCOUNT CHANGE</title>
</head>
<body>
<p>This email contains request for account change!</p>
<strong>Name :</strong>
<p>".$this->input->post('rcname')."</p>
<br>
<strong>Email :</strong>
<p>".$this->input->post('rcemail')."</p>
<br>
<strong>Message :</strong>
<p>".$this->input->post('rcmessage')."</p>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <'.$this->input->post('rcemail').'>' . "\r\n";
//$headers .= 'Cc: muhammadjunaid85@gmail.com' . "\r\n";
$headers .= 'Reply-To: '.$this->input->post('rcemail') . "\r\n";

mail($to,$subject,$message,$headers);
		
		}
		catch(Exception $ex){
			echo $ex->getMessage();
		}
		//exit;
		$this->session->set_flashdata('settingsmessage', 'y');
		redirect('updateprofile');
	}

}
