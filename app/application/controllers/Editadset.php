<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include_once (dirname(__FILE__) . "/createcampaign.php");

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Editadset extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->load->model('Users_model');
        $this->load->model('Userbudget_model');
        //$this->load->library('../controllers/createcampaign');

        $this->seo->SetValues('Title', "Adds");
        $this->seo->SetValues('Description', "Adds");
        $this->is_ajax = 0;
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}

        if (!empty($this->session->userdata['logged_in'])) {
            $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
            $this->user_id = $this->session->userdata['logged_in']['id'];
            $app_id = $this->config->item('facebook_app_id'); //"1648455885427227";
            $facebook_app_secret = $this->config->item('facebook_app_secret'); //"028ead291200f399ce7f1c8aa7232a31";
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
            redirect('/');
        }
    }

    public function index($addSetId = NULL) {

        $limit = "";
        $data = new stdClass();

        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $adAccountId = $this->input->post('adAccountId');
            $campaignId = $this->input->post('campaignId');
            $adsetId = $this->input->post('adsetId');
            $limit = $this->input->post('limit');

            $data->adAccountData = $this->getCurlAdset($campaignId, $adAccountId, $adsetId);
            $data->addAccountId = $adAccountId;
            $data->adaccounts = $data->adAccountData['response'];
            $data->adaccountsCount = $data->adAccountData['count'];
            $data->error = $data->adAccountData['error'];
            $data->success = $data->adAccountData['success'];

            echo $this->load->view('edit_adset', $data, true);
        } else {
            //$this->load->view("edit_adset");
			
			$adAccountId = $adAccountId;
            $campaignId = $campaignId;
            $adsetId = $addSetId;
            //$limit = $this->input->post('limit');

            $data->adAccountData = $this->getCurlAdset($adsetId);
            $data->addAccountId = $adAccountId;
            //$data->adaccounts = $data->adAccountData['response'];
            $data->adaccountsCount = $data->adAccountData['count'];
            $data->error = $data->adAccountData['error'];
            $data->success = $data->adAccountData['success'];
			/*echo "<pre>";
			print_r($data);
			echo "</pre>";*/
			//exit;
			loadView('edit_adset', $data);
        }
    }

    function getPrintResult($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }

    function getCurlAdset($adsetId) {
        
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/'.$adsetId.'/?fields=id,account_id,name,daily_budget,lifetime_budget,targeting&access_token=' . $this->access_token;
        //echo $url;exit;
        try {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if ($response) {
                $resultArray = array(
                    "count" => 1,
                    "response" => $response,
                    "error" => "",
                    "success" => ""
                );
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }
         } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }    

        /*$adSetFields = "id,account_id,name,effective_status,daily_budget,lifetime_budget,targeting";

        $resultArray = array();

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adsetId/?fields=$adSetFields" . "&access_token=" . $this->access_token;

        try {

           
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            //$this->getPrintResult($response);
            $dataArray = $this->getFinalResult($response, $adsetId);
            if ($response) {
                $resultArray = array(
                    "count" => 1,
                    "response" => $dataArray,
                    "error" => "",
                    "success" => ""
                );
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }*/
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getFinalResult($dataArray, $adsetId) {


        $returnRow = array();
        $interests_ids = array();
        $escapedArray = "";
        // $this->getPrintResult($dataArray);
        if (isset($dataArray['targeting'])) {
            //$this->getPrintResult($dataArray['targeting']);
            foreach ($dataArray['targeting']['interests'] as $val) {
                $interests_ids[] = array("id" => $val['id']);
            }
            foreach ($dataArray['targeting']['interests'] as $escapee) {

                $escapedArray = array("id" => $escapee['id'], "name" => $this->db->escape($escapee['name']));
            }
            // $this->getPrintResult($escapedArray);     
            $returnRow['targeting'] = array(
                "age_max" => $dataArray['targeting']['age_max'],
                "age_min" => $dataArray['targeting']['age_min'],
                "excluded_geo_locations" => isset($dataArray['targeting']['excluded_geo_locations']) ? $dataArray['targeting']['excluded_geo_locations'] : "",
                "genders" => $dataArray['targeting']['genders'],
                "geo_locations" => $dataArray['targeting']['geo_locations'],
                "interested_in" => $dataArray['targeting']['interested_in'],
                "interests_ids" => $interests_ids,
                "interests" => $escapedArray,
                "relationship_statuses" => $dataArray['targeting']['relationship_statuses'],
            );
        }

        $returnRow['adset'] = array(
            'adsetId' => $adsetId,
            'adsetName' => $dataArray['name'],
            'daily_budget' => $dataArray['daily_budget'],
            'lifetime_budget' => $dataArray['lifetime_budget']
        );
        //$this->getPrintResult($returnRow);
        return $returnRow;
    }

    function loadModal() {
        $data = new stdClass();
        $data->adset = 1;
        //var_dump($data);
        echo $this->load->view('modals_again', $data, true);
        //$this->load->view("modals", $data);
    }

    function loadScaleModal() {
        $data = new stdClass();
        $data->adset = 2;
        //var_dump($data);
        echo $this->load->view('modals_again', $data, true);
        //$this->load->view("modals", $data);
    }

    function saveAddSet() {

        $addsetId = $this->input->post('adsetID');
        //exit;
        $adsetName = $this->input->post('adsetName');

        $new_geo_locations = $this->getProperLocationArray(trim($this->input->post('new_geo_locations'), ","));
        $new_exclude_locations = trim($this->input->post('new_exclude_locations'), ",");

        $gender = $this->input->post('genders');
        $from = $this->input->post('from');
        $to = $this->input->post('to');


        if ($gender == "all") {
            $genders = "";
        } else {
            $genders = $gender;
        }

        $postFields = array(
            'access_token' => $this->access_token,
            'name' => $adsetName
        );

        $param = '';

        if ($this->input->post('new_geo_locations')) {
            $param .= '{"geo_locations":{' . $new_geo_locations . '}';
        }


        if ($this->input->post('new_exclude_locations')) {
            $param .= ",'excluded_geo_locations': {" . $new_exclude_locations . "}";
        }
        if ($this->input->post('genders')) {
            $param .= ",'genders':[" . $genders . "]";
        }
        if ($this->input->post('from')) {
            $param .= ",'age_min':" . $from;
        }
        if ($this->input->post('to')) {
            $param .= ",'age_max':" . $to;
        }

        $new_interests = trim($this->input->post('new_interests'), ",");        if ($this->input->post('new_interests')) {
            $param .= ",'interests':[" . $new_interests . "]";
        }

        $relationship_statuses = $this->input->post('relationship_statuses');
        if (!empty($relationship_statuses)) {
            $param .= ",'relationship_statuses':[" . implode(',', $relationship_statuses) . "]";
        }

        $interested_in = $this->input->post('interested_in');
        if (!empty($interested_in)) {
            $param .= ",'interested_in':[" . implode(',', $interested_in) . "]";
        }


        $param .= "}";
        //echo $param;
        //$this->getPrintResult($param);
        if (!empty($new_geo_locations)) {
            $postFields['targeting'] = $param;
        }
        try {

            $ch = curl_init($this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/" . $addsetId);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postFields
            ));

            $result = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($result, true);
           // $this->getPrintResult($response['error']);

            if (isset($response['error'])) {

                if (isset($response['error']['code']) && $response['error']['code'] == 100) {                    echo json_encode(array("code" => 200, "message" => "Required fields are empty."));
                    //echo json_encode(array("code" => 200, "message" => $response['error']['message']));
                } else {

                    echo json_encode(array("code" => 200, "message" => $response['error']['error_user_msg']));
                    //die("error111");
                }
            } else if (isset($response['success'])) {
                $this->saveAddSetDb($addsetId);
                echo json_encode(array("code" => 100, "message" => "Updated"));
                //die("error11");
            }
        } catch (Exception $ex) {

            echo json_encode(array("code" => 200, "message" => $ex->message()));
        }
    }

    public function adsetScale($adAccountId = NULL) {

        $limit = "";
        $data = new stdClass();

        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $adAccountId = $this->input->post('adAccountId');
            $campaignId = $this->input->post('campaignId');
            $adsetId = $this->input->post('adsetId');
            $limit = $this->input->post('limit');

            //$data->adAccountData = $this->getCurlAdset($campaignId, $adAccountId, $adsetId);
            $data->adAccountData = $this->getCurlAdset($adsetId);
            //print_r($data->adAccountData);exit;
            $data->addAccountId = $adAccountId;
            $data->adaccounts = $data->adAccountData['response'];
            $data->adaccountsCount = $data->adAccountData['count'];
            $data->error = $data->adAccountData['error'];
            $data->success = $data->adAccountData['success'];
            
            echo $this->load->view('edit_adsetscale', $data, true);
        } else {
            $this->load->view("edit_adsetscale");
        }
    }
    function saveAddSetScale() {
        $addsetId = $this->input->post('adsetId');
        $amount = $this->input->post('amount');
        $current_cur_code = $this->session->userdata('cur_code');
        if($current_cur_code == "VND" || $current_cur_code == "TWD" || $current_cur_code == "CLP" || $current_cur_code == "COP" || $current_cur_code == "CRC" || $current_cur_code == "HUF" || $current_cur_code == "ISK" || $current_cur_code == "IDR" || $current_cur_code == "JPY" || $current_cur_code == "KRW" || $current_cur_code == "PYG"){
            
        }
        else{
            $amount = $amount / 0.01;
        }
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $budget_type = $this->input->post('budget_type');
        $end = (new \DateTime($end_date))->format(\DateTime::ISO8601);
        //echo $addsetId."pqpqpqpq".$amount."pqpqpqpq".$this->access_token;exit;
        try {
            $ch = curl_init($this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/" . $addsetId);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            
            $data_array = array(
                'access_token' => $this->access_token,
                'daily_budget' => $amount,
                //'end_time' => $end
            );
            
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $data_array
            ));

            $result = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($result, true);
            if (isset($response['error'])) {
                if (isset($response['error']['message'])) {
                    echo json_encode(array("code" => 200, "message" => $response['error']['error_user_msg']));
                }
            }
            if (isset($response['success'])) {
                $this->saveAddSetDb($addsetId);
                echo json_encode(array("code" => 100, "message" => "Updated"));
            }
        } catch (Exception $ex) {

            echo json_encode(array("code" => 200, "message" => $ex->message()));        }
    }

    function getProperLocationArray($string) {

        $res = explode(",", trim(trim($string), ","));
        if (count($res) > 0) {

            foreach ($res as $rs) {

                $key_values = explode(':', $rs, 2);
                $key = trim($key_values[0]);
                $new_value = trim(trim(trim($key_values[1]), '['), ']');

                if (isset($array[$key])) {
                    if (!is_array($array[$key]))
                        $array[$key] = (array) $array[$key];
                    $array[$key][] = $new_value;
                } else {
                    $array[$key] = $new_value;
                }
            }

            $stri = '';
            foreach ($array as $key => $val) {
                if (is_array($val)) {
                    $rst = implode(",", $val);
                } else {
                    $rst = $val;
                }
                $stri .= $key . ':[' . $rst . "]" . ",";
            }
            return trim(trim($stri), ',');
        } else {
            return $string;
        }
    }

    function saveAddSetDb($adsetId) {

        $adSetFields = "id,account_id,name,effective_status,daily_budget,lifetime_budget,start_time,end_time,billing_event,targeting";
        $resultArray = "";

        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adsetId/?fields=$adSetFields" . "&access_token=" . $this->access_token;

        try {

            // exit;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);

            //$this->getPrintResult($response);

            $data = array(
                'adsetId' => $response['id'],
                'budget_type' => (isset($response['daily_budget']) && $response['daily_budget'] > 0) ? "daily_budget" : "lifetime_budget",
                'budgetamount' => (isset($response['daily_budget']) && $response['daily_budget'] > 0) ? $response['daily_budget'] / 0.01 : $response['lifetime_budget'] / 0.01,
                'start_date' => !empty($response['start_time']) ? $response['start_time'] : '',
                'end_date' => !empty($response['end_time']) ? $response['end_time'] : '',
                'billing_event' => isset($response['billing_event']) ? $response['billing_event'] : "",
            );
            $this->Userbudget_model->updateuserbudget($data);
            $resultArray = "100";
        } catch (Exception $ex) {
            $resultArray = "200";
        }
        return $resultArray;
    }

}

//end of class
