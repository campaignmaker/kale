<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Login extends CI_Controller {
	public $getpreviousurl;
	public function getgetpreviousurl() {
        return $this->getpreviousurl;
    }

    public function setgetpreviousurl($getpreviousurl) {
        $this->getpreviousurl = $getpreviousurl;
    }
    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
		//echo $this->session->userdata('last_page'); exit;
        error_reporting(E_ERROR);
        $this->load->model('Users_model');
        $this->seo->SetValues('Title', "Login");
        $this->seo->SetValues('Description', "Login");
        
         $this->load->helper(array('cookie'));
		 if($this->session->userdata('last_page'))
		 	$this->setgetpreviousurl($this->session->userdata('last_page'));
			
		 
    }

        public function index() {
          $cookie_name = 'wordpress_logged_in_'. md5(trim($this->config->item('site_url'),'/'));
		if( isset($_COOKIE[$cookie_name]) && !empty($_COOKIE[$cookie_name])){
				$user_data =  explode('|',$_COOKIE[$cookie_name]);
		    	$user_email = $this->Users_model->get_user_email_from_username($user_data[0]);
				$user_id = $this->Users_model->get_user_id_from_email($user_email);
				$user = $this->Users_model->get_user($user_id);
				$this->user_login_session($user);
			}
			
            if (!($this->session->userdata('logged_in'))) {
				
				
                $data = new stdClass();

                $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'required');

                if ($this->form_validation->run() == false) {

                    // validation not ok, send validation errors to the view
                    //$this->load->view('header');
                    $this->load->view('login', $data);
                } else {

                    // set variables from the form
                    $email = $this->input->post('email');
                    $password = $this->input->post('password');
                    $remember = $this->input->post('remember');
                    $user = $this->Users_model->login($email, $password);
                    if ($user) {
                        if (!($this->updateUserAccessToekn($user->id, $user->accesstoken))) {
                            $user->accesstoken = '';
                        }
                        /*if($remember == 1){
                            set_cookie('cookie_email','1','3600');
                            set_cookie('cookie_user',$user->id,'3600');
                        }*/
                        $this->user_login_session($user);
                    } else {

                        // login failed
                        $data->error = 'Wrong username or password.';
                        //$this->load->view('header');
                        $this->load->view('login', $data);
                    }
                }
            } else {
				$response = $this->Users_model->get_cancel_subscription($this->session->userdata['logged_in']['email']); 
				if($response == 'Active'){
			  redirect(site_url('/hello'));
				}else
				if($response == 'No Subscribe'){
					if($this->session->userdata['logged_in']['accesstoken'] == ''){
						header("Location:".$this->config->item('site_url').'connect-to-facebook/');
					}else{
					redirect($this->config->item('site_url').'one-step-closer/'); 
					 
					}
				}
			}
    }

    public function wpsuer(){
        $userId = base64_decode($this->uri->segment(3));
        $user = $this->Users_model->get_user($userId);
        $this->user_login_session($user);
    }	
	
	 public function wpsuerregister(){
        $userId = base64_decode($this->uri->segment(3));
        $user = $this->Users_model->get_user($userId);
        $this->user_login_sessionregister($user);
    }
	
	public function changeplan(){
        $plantype = $this->uri->segment(3);
        $user = $this->Users_model->upgrade_downgrade_plan($this->session->userdata['logged_in']['id'],$plantype);
		$useridchexp = (int)$this->session->userdata['logged_in']['id'];
		$usersubs = $this->Users_model->checktrilaexpired($useridchexp);
		$session_arraysub = array(
            'trialexpited' => (string) $usersubs
        );
		$this->session->set_userdata('user_subs', $session_arraysub);
        redirect(site_url('/updateprofile'));
    }
	
	public function refreshplan(){
		$useridchexp = (int)$this->session->userdata['logged_in']['id'];
		$usersubs = $this->Users_model->checktrilaexpired($useridchexp);
		$session_arraysub = array(
            'trialexpited' => (string) $usersubs
        );
		$this->session->set_userdata('user_subs', $session_arraysub);
        redirect(site_url('/updateprofile?showthanks=yes'));
    }

    public function user_login_session($user) {
        
       $session_array = array(
            'id' => (int) $user->id,
            'first_name' => (string) $user->first_name,
            'last_name' => (string) $user->last_name,
            'full_name' => (string) $user->first_name.' '.$user->last_name,
            'email' => (string) $user->email,
            'picture' => (string) $user->picture,
            'accesstoken' => (string) $user->accesstoken,
			'status' => (int) $user->disable_user,
            'logged_in' => (bool) true,
        );
		$useridchexp = (int) $user->id;
		$usersubs = $this->Users_model->checktrilaexpired($useridchexp);
		
		$session_arraysub = array(
            'trialexpited' => (string) $usersubs['exp'],
			'packgid' => $usersubs['pkgid'],
			'stripestatus' => $usersubs['stripestatus1'],
			'bill_start_date1' => $usersubs['bill_start_date'],
			'bill_end_date1' => $usersubs['bill_end_date']
        );
        $this->session->set_userdata('logged_in', $session_array);
        
		$this->session->set_userdata('user_subs', $session_arraysub);
		
		$this->Users_model->insert_behaviour_tracking($useridchexp);

		$response = $this->Users_model->get_cancel_subscription($this->session->userdata['logged_in']['email']); 
		if($response == 'Active'){
			  redirect(site_url('/hello'));
		}else
		if($response == 'No Subscribe'){
			if($this->session->userdata['logged_in']['accesstoken'] == ''){
				header("Location:".$this->config->item('site_url').'connect-to-facebook/');
			}else{
			redirect($this->config->item('site_url').'one-step-closer/'); }
		}elseif($response == 'Cancelled' || $response == 'Expired'){
			//redirect($this->config->item('site_url').'final-payment/');
			
		}
    }

    public function updateUserAccessToekn($uid, $accesstoken) {


        $fb = new Facebook([
            'app_id' => $this->config->item('facebook_app_id'),
            'app_secret' => $this->config->item('facebook_app_secret'),
            'default_graph_version' => $this->config->item('facebook_graph_version'),
        ]);

        if (isset($accesstoken) && $accesstoken != NULL) {

            try {
                // OAuth 2.0 client handler
                $oAuth2Client = $fb->getOAuth2Client();
                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($accesstoken);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                $error = $e->getMessage();
            } catch (Exception $e) {
                $error = $e->getMessage();
            }

            if (isset($error)) {
                $this->session->set_flashdata('error', $error);
                //print_r($error); die();
            }

            if (isset($longLivedAccessToken) && $longLivedAccessToken != NULL) {
                $this->Users_model->edit_profile(array('accesstoken' => $longLivedAccessToken), $uid);
                return true;
            } else { // Delete access token becuase its Not able to generate access token 
                $this->Users_model->edit_profile(array('accesstoken' => '', 'fbid' => ''), $uid);
                return false;
            }
        }
    }

}
