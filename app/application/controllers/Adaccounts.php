<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Adaccounts extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public $user_id;

    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
		  $this->load->model('Users_model');
        if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            header("Location:".$this->config->item('site_url'));die;
		}
        $response = $this->Users_model->get_cancel_subscription($this->session->userdata['logged_in']['email']);
		if($response == 'No Subscribe'){
		if($this->session->userdata['logged_in']['accesstoken'] == ''){
				header("Location:".$this->config->item('site_url').'connect-to-facebook/');
		}else{	redirect($this->config->item('site_url').'one-step-closer/'); }
		}elseif($response == 'Cancelled' || $response == 'Expired'){
			//redirect($this->config->item('site_url').'final-payment/');
		}
        
        $this->load->model('Users_model');
        $this->user_id = $this->session->userdata['logged_in']['id'];
        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
        $this->seo->SetValues('Title', "Adds");
        $this->seo->SetValues('Description', "Adds");
    }

    public function index() {
        if ($this->user_id) {
            $data = new stdClass();
            $data->adaccounts = $this->Users_model->getUserAddAccountId($this->user_id);
            loadView('adaccounts', $data);
        } else {
            redirect(site_url('/login'));
        }
    }

    public function update() {
        if ($this->user_id && $this->access_token) {

            $this->form_validation->set_rules('account_id_name[]', 'Ad Account', 'trim|required');

            if ($this->form_validation->run() == false) {
                $error = "Ad Account required! Try again";
                $this->get($error);
            } else {

                $accountIdNames = $this->input->post('account_id_name');
               

                if (isset($accountIdNames) && $accountIdNames != NULL) {

                    $this->Users_model->deleteAdAccount($this->user_id);

                    foreach ($accountIdNames as $accountIdName) {

                        $respons = explode('@IN@', $accountIdName);
                        $response = $this->Users_model->addAdAccountId(array('user_id' => $this->user_id, 'add_title' => $respons[1], 'ad_account_id' => $respons[0], 'status' => 1));
                    }
                }

                if ($response) {
                   //$this->session->set_flashdata('success', 'Account updated successfully.');
                    redirect(site_url('adaccounts'));
                } else {
                    $error = "Ad account(s) already taken by another user, contact chat support to share access.";
                    $this->get($error);
                }
            }
        } else {
            redirect('/');
        }
    }

    public function get($error = NULL) {

        if ($this->user_id && $this->access_token) {

            $data = new stdClass();

            try {
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/me?fields=adaccounts.limit(500){id,account_id,account_status,name}" . "&access_token=" . $this->access_token;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $result = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($result, true);

                if (isset($response['adaccounts']['data'])) {
                    foreach ($response['adaccounts']['data'] as $my_adaccount) {
                        if ($my_adaccount['account_status'] == 1) {
                            $myadaccounts[] = array('id' => $my_adaccount['id'], 'account_id' => $my_adaccount['account_id'], 'name' => $my_adaccount['name'], 'account_status' => $my_adaccount['account_status']);
                        }
                    }
                }
                if (isset($myadaccounts) && $myadaccounts != NULL) {
                    $data->adaccounts = $myadaccounts;
                } else {
                    $error = 'No ad account is associated with your facebook account. Please create an ad account first through your facebook account';
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
            }
            $data->error = $error;
            loadView('updateaccounts', $data);
        } else {
            redirect('/');
        }
    }

}
