<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Index extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->seo->SetValues('Title', "Facebook campaign");
        $this->seo->SetValues('Description', "Facebook campaign");
    }

    public function index() {
        $data = new stdClass();
        //add_css(array('thread.css'));
        $this->load->view('homepage/header');
        $this->load->view('homepage/index', $data);
        $this->load->view('homepage/footer');
    }

}
