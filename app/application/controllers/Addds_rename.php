<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Addds extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public $access_token;
    public $user_id;
    public $add_account_id;
    public $campaign_id;
    public $adds_id;
    public $add_title;
    public $is_ajax;

    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}
        $this->load->model('Users_model');
        $this->seo->SetValues('Title', "Adds");
        $this->seo->SetValues('Description', "Adds");
        $this->is_ajax = 0;


        if (!empty($this->session->userdata['logged_in'])) {
            $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
            $this->user_id = $this->session->userdata['logged_in']['id'];
            $this->app_id = $app_id = $this->config->item('facebook_app_id'); //"1648455885427227";
            $this->app_secret = $facebook_app_secret = $this->config->item('facebook_app_secret'); //"028ead291200f399ce7f1c8aa7232a31";
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
            redirect('login');
        }
        
        // User account spend limit reach
        if (userAccountSpendCheck()) {
            redirect('/updateprofile');
        }
        
        /*if (!empty($this->session->userdata('sDate')) && !empty($this->session->userdata('eDate'))) {
            $this->sDate = $this->session->userdata['sDate'];
            $this->eDate = $this->session->userdata['eDate'];
        }
        else{
            $this->sDate = date('Y-m-d', strtotime('-6 days'));//2016-06-07
            $this->eDate = date('Y-m-d');
            $this->session->set_userdata('sDate', $this->sDate);
            $this->session->set_userdata('eDate', $this->eDate);
        }*/
        if (!empty($this->session->userdata('dateval'))){
            $this->dateval = $this->session->userdata['dateval'];
        }
        else{
            $this->session->set_userdata('dateval', 'today');
            $this->dateval = $this->session->userdata['dateval'];
        }
    }

    public function index($adAccountId = NULL, $campaignId = NULL, $addSetId = NULL) {
        $limit = $this->dateval;//$this->sDate."#".$this->eDate;
        $data = new stdClass();
        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $adAccountId = $this->input->post('adAccountId');
            $campaignId = $this->input->post('campaignId');
            $addSetId = $this->input->post('addsetId');
            $limit = $this->input->post('limit');
            $this->session->set_userdata('dateval', $limit);
            //$dataArray = explode("#", $limit);
            //$this->session->set_userdata('sDate', $dataArray[0]);
            //$this->session->set_userdata('eDate', $dataArray[1]);
        }

        if (!empty($adAccountId) && !empty($campaignId) && !empty($addSetId)) {

            $this->setAddAccountId($adAccountId);
            $this->setcampaignId($campaignId);
            $this->setaddSetId($addSetId);
            $addset_name = $this->getOneCampaign($campaignId);
            $adds_name = $this->getOneAddset($addSetId);
            if (strpos($limit, '#') !== false) {
                $data->adAccountData = $this->getCurlAdddsDateRange($limit, $campaignId, $addSetId);
            } else {
                //$data->adAccountData = $this->getCurlCampaigns($limit, $campaignId, $addSetId);
                $data->adAccountData = $this->getCurlAdddsDateRange($limit, $campaignId, $addSetId);
            }
        } else {
            redirect('/reports');
        }
        $data->addAccountId = $this->getAddAccountId();
        $data->adaccounts = $data->adAccountData['response'];
        $data->adaccountsCount = $data->adAccountData['count'];
        $data->error = $data->adAccountData['error'];
        $data->success = $data->adAccountData['success'];
        $data->campaignId = $this->getCampaignId();
        $data->addsetId = $this->getaddSetId();
        $data->addsetName = $adds_name;
        $data->addset_name = $addset_name;
        $data->limit = $limit;

        $countryarray = $this->getAddsCountryChart_without_ajax($this->getCampaignId(),$this->getaddSetId(),$limit,"adddsReport");
        $data->adaccounts["country_total"] = $countryarray[0];
        $data->adaccounts["country_detail"] = $countryarray[1];

        $genderarray = $this->getAddsGender_without_ajax($this->getCampaignId(),$this->getaddSetId(),$limit,"adddsReport");
        $data->adaccounts["gender_total"] = $genderarray[0];
        $data->adaccounts["gender_detail"] = $genderarray[1];


        $agegrouparray = $this->getAddsChart_without_ajax($this->getCampaignId(),$this->getaddSetId(),$limit,"adddsReport");
        $data->adaccounts["agegroup_total"] = $agegrouparray[0];
        $data->adaccounts["agegroup_detail"] = $agegrouparray[1];

        $feedsarray = $this->getAddsPlacementData_without_ajax($this->getCampaignId(),$this->getaddSetId(),$limit,"adddsReport");
        $data->adaccounts["feeds_total"] = $feedsarray[0];
        $data->adaccounts["feeds_detail"] = $feedsarray[1];

        #echo "<PRE>";print_R($data);exit;
        if ($this->is_ajax == 1) {
            echo $this->load->view('ads_ajax_view', $data, true);
        } else {
            loadView('addds', $data);
        }
    }
    
    protected function getOneCampaign($campaignIdsArray) {
        try {
            $campaignObj = new Campaign($campaignIdsArray);
            $fields = array(
                CampaignFields::ID,
                CampaignFields::NAME
            );
            $campaignObj->read($fields);
            $resultArray = $campaignObj->{CampaignFields::NAME};
        } catch (Exception $e) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        return $resultArray;
    }

    /* From LIVE */

    protected function fbAdds($addSetId, $selected_limit) {
        try {
            $resultArray = $this->getAllAds($addSetId, $selected_limit);
        } catch (Exception $e) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        return $resultArray;
    }

    protected function getAllAds($addSetId, $selected_limit) {
        try {
            $addObj = new AdSet($addSetId);

            $fields = $this->getAdsFields();
            $insights = $this->getAdsInsightFields();
            $params = ""; //$this->getAdsParams($selected_limit);

            $response = $addObj->getAds($fields);
            // $this->getPrintResult($response);
            foreach ($response as $row) {
                try {
                    $adsetObj = new Adset($row->id);

                    $res = $this->get_hub_insight_per($adsetObj, $insights, $params);

                    foreach ($res as $rs) {

                        $insightsArray[] = array(
                            "account_id" => $rs->account_id,
                            "account_name" => $rs->account_name,
                            "action_values" => $rs->action_values,
                            "actions" => $rs->actions,
                            "actions_per_impression" => $rs->actions_per_impression,
                            "ad_id" => $rs->ad_id,
                            "ad_name" => $rs->ad_name,
                            "adset_id" => $rs->adset_id,
                            "adset_name" => $rs->adset_name,
                            "campaign_id" => $rs->campaign_id,
                            "campaign_name" => $rs->campaign_name,
                            "cost_per_action_type" => $rs->cost_per_action_type,
                            "cost_per_total_action" => $rs->cost_per_total_action,
                            "cost_per_unique_click" => $rs->cost_per_unique_click,
                            "cost_per_inline_link_click" => $rs->cost_per_inline_link_click,
                            "cost_per_inline_post_engagement" => $rs->cost_per_inline_post_engagement,
                            "cpm" => $rs->cpm,
                            "cpp" => $rs->cpp,
                            "ctr" => $rs->ctr,
                            "date_start" => $rs->date_start,
                            "date_stop" => $rs->date_stop,
                            "frequency" => $rs->frequency,
                            "impressions" => $rs->impressions,
                            "inline_link_clicks" => $rs->inline_link_clicks,
                            "inline_post_engagement" => $rs->inline_post_engagement,
                            "reach" => $rs->reach,
                            "spend" => $rs->spend,
                            "total_action_value" => $rs->total_action_value,
                            "total_actions" => $rs->total_actions,
                            "total_unique_actions" => $rs->total_unique_actions,
                            "unique_clicks" => $rs->unique_clicks,
                            "unique_ctr" => $rs->unique_ctr,
                            "website_ctr" => $rs->website_ctr
                        );
                    }
                    if ($insightsArray) {
                        $adsetDataArray[] = array(
                            "id" => $row->id,
                            "name" => $row->name,
                            "created_time" => $row->created_time,
                            "configured_status" => $row->configured_status,
                            "effective_status" => $row->effective_status,
                            "insights" => $insightsArray
                        );
                        $insightsArray = array();
                    }
                } catch (Exception $e) {
                    return $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $e->getMessage(),
                        "success" => ""
                    );
                }
            }
            if (count($adsetDataArray) > 1) {
                $finalArray = $this->getFinalResult($adsetDataArray);
                $resultArray = array(
                    "count" => 1,
                    "response" => $finalArray,
                    "error" => "",
                    "success" => ""
                );
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => "");
            }
        } catch (Exception $e) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        return $resultArray;
    }

    /* Privat functions */

    private function setAddAccountId($adAccountId) {
        $this->add_account_id = $adAccountId;
    }

    private function setcampaignId($campaignId) {
        $this->campaign_id = $campaignId;
    }

    private function setaddSetId($addSetId) {
        $this->adds_id = $addSetId;
    }

    //setaddSetId($addSetId)

    private function getAddAccountId() {
        return $this->add_account_id;
    }

    private function getCampaignId() {
        return $this->campaign_id;
    }

    private function getaddSetId() {
        return $this->adds_id;
    }

    /* End Private */

    function get_hub_insight_per($obj, $insights, $param) {
        try {

            return $response = $obj->getInsights($insights);
            // }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function getAdsFields($curl) {
        if ($curl == 1) {
            $fields = "id,account_id,name,bid_amount,budget_remaining,campaign_id,created_time,daily_budget,end_time,lifetime_budget,promoted_object,product_ad_behaviour,billing_event,configured_status,effective_status,targetingsentencelines{params,targetingsentencelines},targeting";
        } else {
            $fields = array(
                AdFields::ID,
                AdFields::ACCOUNT_ID,
                AdFields::BID_AMOUNT,
                AdFields::ADSET_ID,
                AdFields::CAMPAIGN_ID,
                // AdFields::conversion_specs,
                AdFields::CREATED_TIME,
                AdFields::AD_REVIEW_FEEDBACK,
                AdFields::NAME,
                AdFields::RTB_FLAG,
                AdFields::TARGETING,
                AdFields::TRACKING_SPECS,
                AdFields::UPDATED_TIME,
                // AdFields::VIEW_TAGS,
                AdFields::CREATIVE,
                AdFields::SOCIAL_PREFS,
                AdFields::FAILED_DELIVERY_CHECKS,
                AdFields::REDOWNLOAD,
                AdFields::ADLABELS,
                AdFields::ENGAGEMENT_AUDIENCE,
                AdFields::EXECUTION_OPTIONS
            );
        }
        return $fields;
    }

    function getAdsInsightFields() {
        return $fields = array(
            InsightsFields::ACCOUNT_ID,
            InsightsFields::IMPRESSIONS,
            InsightsFields::UNIQUE_CLICKS,
            InsightsFields::REACH,
            InsightsFields::INLINE_LINK_CLICKS,
            InsightsFields::COST_PER_INLINE_LINK_CLICK,
            InsightsFields::COST_PER_UNIQUE_CLICK
        );
    }

    function getAdsParams($type) {
        return $params = "'date_preset' => $type";
    }

    function getFinalResult($addSetDataArray, $campaignId, $addSetId) {
        $activeAddSet = 0;
        $inActiveAddSet = 0;
        $cost_per_unique_click = 0;
        $total_cost_per_inline_click = 0;
        $impressions = 0;
        $total_inline_clicks = 0;
        $reach = 0;
        $unique_clicks = 0;
        $unique_ctr = 0;
        $spent = 0;
        $actions = 0;
        $cost_per_total_action = 0;

        $inline_clicks = 0;
        $cost_per_inline_clicks = 0;
        $pendingAddSet = 0;
        $disapprovedAddSet = 0;
        $total = 0;
        $cost_per_conversion = 0;
        $conversion = 0;
        $countries = array();
        $intersts = array();
        $language = array();
        $gender = array('Male', 'female');
        $age = array();
        $behaviour = array();
        $customAudience = array();
        $demographic = array();
        $objective = $this->getCampaignObjective($campaignId);
        $addSetStatus = $this->getAddsetObjective($addSetId);
        $total1 = 0;
        $total2 = 0;
        $app_install = $leadgenother = $video_10_sec_watched_actions = $video_avg_percent_watched_actions = $offline_conversion = 0;
        $returnRow = array();
        // $this->getPrintResult($addSetDataArray);
        foreach ($addSetDataArray as $row) {
            $row['objective'] = $objective;
            if ($row['effective_status'] == "ACTIVE") {
                $activeAddSet++;
                $status = "Active";
            } else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {
                $inActiveAddSet++;
                $status = "Inactive";
            } else if ($row['effective_status'] == "PENDING_REVIEW") {
                $pendingAddSet++;
                $status = "In Review";
            } else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {
                $disapprovedAddSet++;
                $status = "Denied";
            }
            $total++;


            if (isset($row['insights'])) {
                $inline_post_engagement1 = $like1 = '0';
                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';
                // echo $row['name']."<br>";
                if (isset($row['insights']['data'][0]['actions'])) {
                    foreach ($row['insights']['data'][0]['actions'] as $rs) {

                        /*if ($row['insights']['data'][0]['clicks']) {
                            $inline_clicks = $row['insights']['data'][0]['clicks'];
                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];
                        }*/
						if ($row['insights']['data'][0]['inline_link_clicks']) {
                            $inline_clicks = $row['insights']['data'][0]['inline_link_clicks'];
                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['inline_link_clicks'];
                        }
                        if ($rs['action_type'] == "offsite_conversion") {
                            $conversion = $rs['value'];
                            $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];
                        } else {
                            $conversion = 0;
                            $cost_per_conversion = 0;
                        }
                        if($rs['action_type'] == 'page_engagement'){
                            if($rs['value']){
                                $total1++;
                                $inline_post_engagement1 = $rs['value'];
                            }
                            else{
                                $inline_post_engagement1 = 0;
                            }
                        }
                        if($rs['action_type'] == 'like'){
                            if($rs['value']){
                                $total2++;
                                $like1 = $rs['value'];
                            }
                            else{
                                $like1 = 0;
                            }
                        }
                        if($rs['action_type'] == 'app_install'){
                                if($rs['value']){
                                    $app_install = $rs['value'];
                                }
                                else{
                                    $app_install = 0;
                                }
                            }
                            if($rs['action_type'] == 'leadgen.other'){
                                if($rs['value']){
                                    $leadgenother = $rs['value'];
                                }
                                else{
                                    $leadgenother = 0;
                                }
                            }
                            if($rs['action_type'] == 'offline_conversion'){
                                if($rs['value']){
                                    $offline_conversion = $rs['value'];
                                }
                                else{
                                    $offline_conversion = 0;
                                }
                            }
                    }
                    foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {
                        if($rs['action_type'] == 'page_engagement'){
                            if($rs['value']){
                                $cost_per_inline_post_engagement1 = $rs['value'];
                            }
                            else{
                                $cost_per_inline_post_engagement1 = 0;
                            }
                        }
                        if($rs['action_type'] == 'like'){
                            if($rs['value']){
                                $cost_per_like1 = $rs['value'];
                            }
                            else{
                                $cost_per_like1 = 0;
                            }
                        }
                    }

                    foreach ($row['insights']['data'][0]['video_10_sec_watched_actions'] as $rs) {
                        if($rs['action_type'] == 'video_view'){
                            if($rs['value']){
                                $video_10_sec_watched_actions = $rs['value'];
                            }
                            else{
                                $video_10_sec_watched_actions = 0;
                            }
                        }
                    }
                    foreach ($row['insights']['data'][0]['video_avg_percent_watched_actions'] as $rs) {
                        if($rs['action_type'] == 'video_view'){
                            if($rs['value']){
                                $video_avg_percent_watched_actions = $rs['value'];
                            }
                            else{
                                $video_avg_percent_watched_actions = 0;
                            }
                        }
                    }
                    
                }
                $total_inline_clicks+=$inline_clicks;
                $total_cost_per_inline_click+=$cost_per_inline_clicks;
                
                $returnRow['adds'][] = array(
                    "adds_id" => $row['id'],
                    "adds_name" => $row['name'],
                    "adds_created_time" => $row['created_time'],
                    "adds_effective_status" => $status, //$row['effective_status'],
                    "adds_cost_per_unique_click" => $row['insights']['data'][0]['cost_per_unique_click'],
                    "adds_cost_per_inline_link_click" => $cost_per_inline_clicks,
                    "adds_impressions" => $row['insights']['data'][0]['impressions'],
                    "adds_cpm" => $row['insights']['data'][0]['cpm'],
                    "adds_inline_link_clicks" => $inline_clicks,
                    "adds_reach" => $row['insights']['data'][0]['reach'],
                    "adds_unique_clicks" => $row['insights']['data'][0]['unique_clicks'],
                    "adds_unique_ctr" => $row['insights']['data'][0]['ctr'],
                    "adds_spent" => $row['insights']['data'][0]['spend'],
                    "adds_actions" => $conversion,
                    "adds_cost_per_total_action" => $cost_per_conversion,
                    "adds_reach" => $row['insights']['data'][0]['reach'],
                    "adds_objective" => $row['objective'],
                    "inline_post_engagement" => $inline_post_engagement1,
                    "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement1,
                    "page_like" => $like1,
                    "cost_per_like1" => $cost_per_like1,
                );
                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];
                $impressions += $row['insights']['data'][0]['impressions'];
                $cpm += $row['insights']['data'][0]['cpm'];
                $reach += $row['insights']['data'][0]['reach'];
                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];
                $unique_ctr += $row['insights']['data'][0]['ctr'];
                $spent += $row['insights']['data'][0]['spend'];
                $actions += $conversion;
                $cost_per_total_action += $cost_per_conversion;
                $inline_post_engagement += $inline_post_engagement1;
                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;

                $page_like += $like1;
                $cost_per_like += $cost_per_like1;
                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            } else {

                $returnRow['adds'][] = array(
                    "adds_id" => $row['id'],
                    "adds_name" => $row['name'],
                    "adds_created_time" => $row['created_time'],
                    "adds_effective_status" => $status, //$row['effective_status'],
                    "adds_cost_per_unique_click" => 0,
                    "adds_cost_per_inline_link_click" => $cost_per_inline_clicks,
                    "adds_impressions" => 0,
                    "adds_cpm" => 0,
                    "adds_inline_link_clicks" => $inline_clicks,
                    "adds_reach" => 0,
                    "adds_unique_clicks" => 0,
                    "adds_unique_ctr" => 0,
                    "adds_spent" => 0,
                    "adds_actions" => 0,
                    "adds_cost_per_total_action" => 0,
                    "adds_reach" => 0,
                    "adds_objective" => $row['objective'],
                    "inline_post_engagement" => 0,
                    "cost_per_inline_post_engagement" => 0,
                    "page_like" => 0,
                    "cost_per_like1" => 0,
                );

                $cost_per_unique_click += 0;
                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];
                $impressions += 0;
                $cpm += 0;
                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];
                $reach += 0;
                $unique_clicks += 0;
                $unique_ctr += 0;
                $spent += 0;
                $actions += 0;
                $cost_per_total_action += 0;
                $inline_post_engagement += 0;
                $cost_per_inline_post_engagement += 0;

                $page_like += 0;
                $cost_per_like += 0;
                
                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            }
            // $this->getPrintResult($row['targetingsentencelines']['targetingsentencelines']);
            if (isset($row['targetingsentencelines'])) {

                foreach ($row['targetingsentencelines']['targetingsentencelines'] as $list) {
                    /* Countries */
                    if ($list['content'] == "Location - Living In:") {

                        foreach ($list['children'] as $ctry) {
                            $countries[] = $ctry;
                        }
                    }
                    /* Gender */
                    if ($list['content'] == "Gender:") {
                        foreach ($list['children'] as $genders) {
                            $gender[] = $genders;
                        }
                    }
                    /* Intersts */
                    if ($list['content'] == "Interests:") {

                        foreach ($list['children'] as $interst) {

                            $intersts[] = $interst;
                        }
                    }
                    /* Behavoiurs */
                    if ($list['content'] == "Behaviors:") {
                        //$this->getPrintResult($list['children']);
                        foreach ($list['children'] as $behaviours) {
                            $behaviour[] = $behaviours;
                        }
                    }
                    /* Behavoiurs */
                    if ($list['content'] == "Language:") {
                        foreach ($list['children'] as $lang) {
                            $language[] = $lang;
                        }
                    }
                    /* Custom Audience: */
                    if ($list['content'] == "Custom Audience:") {
                        foreach ($list['children'] as $customAud) {
                            $customAudience[] = $customAud;
                        }
                    }
                    /* Custom Audience: */
                    /* demographic  */
                    if ($list['content'] == "Parents:") {
                        foreach ($list['children'] as $demogr) {
                            $demographic[] = $demogr;
                        }
                    }
                    /* demographic  */
                    /* age  */
                    if ($list['content'] == "Age:") {
                        foreach ($list['children'] as $age1) {
                            $age[] = $age1;
                        }
                    }
                    /* age  */
                }
            }
        }
        if (count($countries) > 0) {
            $countries = array_unique($countries);
            $countries = implode(', ', $countries);
        } else {
            $country = "";
        }
        if (count($intersts) > 0) {
            $intersts = array_unique($intersts);
            $intersts = implode(', ', $intersts);
        } else {
            $intersts = "";
        }
        if (count($language) > 0) {
            $language = array_unique($language);
            $language = implode(', ', $language);
        } else {
            $language = "";
        }

        if (count($gender) > 0) {

            $gender = array_unique($gender);
            $gender = implode(', ', $gender);
        } else {
            $gender = "";
        }
        if (count($age) > 0) {

            $age = array_unique($age);
            $age = implode(', ', $age);
        } else {
            $age = "";
        }
        if (count($behaviour) > 0) {
            $behaviour = array_unique($behaviour);
            $behaviour = implode(', ', $behaviour);
        } else {
            $behaviour = "";
        }
        if (count($customAudience) > 0) {
            $customAudience = array_unique($customAudience);
            $customAudience = implode(', ', $customAudience);
        } else {
            $customAudience = "";
        }
        if (count($demographic) > 0) {
            $demographic = array_unique($demographic);
            $demographic = implode(', ', $demographic);
        } else {
            $demographic = "";
        }

        if ($unique_ctr > 0) {
            $unique_ctr = ($unique_ctr / $total);
        }
        if ($cost_per_total_action > 0) {
            $cost_per_total_action = ($spent / $actions);
        }
        if ($cost_per_inline_post_engagement > 0) {
            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);
        }
        if ($cost_per_like > 0) {
            $cost_per_like = ($cost_per_like / $total2);
        }
        $returnRow['adds_header'] = array(
            "adds_active" => $activeAddSet,
            "adds_inactive" => $inActiveAddSet,
            "adds_pending" => $pendingAddSet,
            "adds_disapproved" => $disapprovedAddSet,
            "cost_per_unique_click" => $cost_per_unique_click,
            "cost_per_inline_link_click" => $total_cost_per_inline_click,
            "impressions" => $impressions,
            "cpm" => $cpm,
            "inline_link_clicks" => $total_inline_clicks,
            "reach" => $reach,
            "unique_clicks" => $unique_clicks,
            "unique_ctr" => $unique_ctr,
            "spent" => $spent,
            "actions" => $actions,
            "cost_per_total_action" => $cost_per_total_action,
            "country" => $countries,
            "interests" => $intersts,
            "language" => $language,
            "gender" => $gender,
            "customAudience" => $customAudience,
            "demographic" => $demographic,
            "age" => $age,
            "behaviours" => $behaviour,
            "addSetStatus" => $addSetStatus,
            "inline_post_engagement" => $inline_post_engagement,
            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,
            "page_like" => $page_like,
            "cost_per_like" => $cost_per_like,
            "app_install" => $app_install,
            "leadgenother" => $leadgenother,
            "video_10_sec_watched_actions" => $video_10_sec_watched_actions,
            "video_avg_percent_watched_actions" => $video_avg_percent_watched_actions,
            "offline_conversion" => $offline_conversion
        );
        //$this->getPrintResult($returnRow);
        return $returnRow;
    }

    function getPrintResult($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }

    protected function getOneAddset($addSetId) {
        try {
            $addSetObj = new Ad($addSetId);
            // die("dd");
            // $addSetObj = new Adds($addSetId);

            $fields = array(
                AdFields::ID,
                AdFields::NAME
            );
            $addSetObj->read($fields);
            $resultArray = $addSetObj->{AdFields::NAME};
        } catch (Exception $e) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        return $resultArray;
    }

    function getAddSetParams($type) {
        return $params = ".date_preset($type)";
    }

    function getCurlCampaigns($limit, $campaignId, $addSetId) {
        $adAccountId = "";
        $date_preset = "";
        $resultArray = array();

        if (!empty($limit)) {
            $date_preset = $this->getAddSetParams($limit);
        } else {
            $date_preset = $this->getAddSetParams("last_7_days");
        }

        $adAccountId = "act_" . $this->getAddAccountId();
        $addSetFields = $this->getAdsFields(1);
        try {
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/ads?fields=insights$date_preset$addSetFields" . "&access_token=" . $this->access_token;

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            // $this->getPrintResult($response);
            if (isset($response['data'])) {
                $addSetDataArray = $this->getFinalResult($response['data'], $campaignId, $addSetId);
                if ($addSetDataArray) {
                    $resultArray = array(
                        "count" => 1,
                        "response" => $addSetDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => "No record found.",
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }

        return $resultArray;
    }

    function getCampaignObjective($campaignId) {
        $adAccountId = "act_" . $this->getAddAccountId();
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/?fields=objective&access_token=" . $this->access_token;
        // $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/customaudiences/?&access_token=" . $this->access_token;
        ///{ad_account_id}/customaudiences
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //  $this->getPrintResult($response);
        return $response['objective'];
    }

    function getCurlAdddsDateRange($limit, $campaignId, $addSetId) {
        $returnRow = array();
        $resultArray = array();

        $dataArray = explode("#", $limit);
        $addSetFields = $this->getAdsFields(1);
        
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/ads?fields=".$addSetFields.",insights.date_preset(".$limit."){inline_link_clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&access_token=" . $this->access_token;
        #echo $url;exit;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            if (isset($response['data'])) {
                $campaignDataArray = $this->getFinalResult($response['data']);
                if ($campaignDataArray) {
					
					$url4 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . '/?batch=['.urlencode('{ "method":"GET", "relative_url":"'.$addSetId.'?fields=insights.date_preset('.$limit.'){frequency,date_start}"},{ "method":"GET", "relative_url":"'.$addSetId.'/ads?fields='.$addSetFields.',insights.date_preset(today){spend}&limit=250"},{ "method":"GET", "relative_url":"'.$addSetId.'/ads?fields='.$addSetFields.',insights.date_preset(last_7d){spend}&limit=250"}').']&access_token=' . $this->access_token;
					
					 $ch4 = curl_init($url4);
                curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch4, CURLOPT_POST, 1);
                $result4 = curl_exec($ch4);
				//print_r($result);
                curl_close($ch4);
                $response4 = json_decode($result4, true);
                     /*$url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId?fields=insights.date_preset(".$limit.")%7Bfrequency%2Cdate_start%7D&access_token=" . $this->access_token;

                    $ch = curl_init($url1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response1 = json_decode($result, true);*/
					$response1 = json_decode($response4[0]['body'], true);
                    $campaignDataArray['adds_header']['newFrequ'] = $response1['insights']['data'][0]['frequency'];

                    $accountArr = $this->Users_model->getUserAddAccountName($this->getAddAccountId());
                    $campaignDataArray['adds_header']['accountName'] = $accountArr[0]->add_title;


                   /* $url2 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/ads?fields=".$addSetFields.",insights.date_preset(today){spend}&access_token=" . $this->access_token;

                    $ch = curl_init($url2);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result2 = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($result2, true);*/
                    
                    $response2 = json_decode($response4[1]['body'], true);
                    
                    
                    /* $url3 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/ads?fields=".$addSetFields.",insights.date_preset(last_7d){spend}&limit=250&access_token=" . $this->access_token;

                    $ch = curl_init($url3);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result3 = curl_exec($ch);
                    curl_close($ch);
                    $response3 = json_decode($result3, true);*/
                    $response3 = json_decode($response4[2]['body'], true);
                    $todayspend = $sevendayspend = 0;
                    
                    foreach ($response2['data'] as $row1) {
                        $todayspend += $row1['insights']['data'][0]['spend'];
                    }
                    /*if($response2['data'][0]['insights']['data'][0]['spend']){
                        $todayspend = $response2['data'][0]['insights']['data'][0]['spend'];
                    }
                    if($response3['data'][0]['insights']['data'][0]['spend']){
                        $sevendayspend = $response3['data'][0]['insights']['data'][0]['spend'];
                    }*/
                    foreach ($response3['data'] as $row2) {
                        $sevendayspend += $row2['insights']['data'][0]['spend'];
                    }
                    
                    $campaignDataArray['adds_header']['todayspend'] = $todayspend;
                    $campaignDataArray['adds_header']['sevendayspend'] = $sevendayspend;


                    $resultArray = array(
                        "count" => 1,
                        "response" => $campaignDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $error,
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => $error,
                    "success" => ""
                );
            }
            // $this->getPrintResult($response);
            /*if (isset($response['data'])) {
                $finalArr = array();
                foreach ($response['data'] as $campaignData){
                    $addsId = $campaignData['id'];
                    $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addsId/insights?fields=clicks,impressions,reach,actions,date_start,campaign_name,ctr,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective&time_range={'since':'".$dataArray[0]."','until':'".$dataArray[1]."'}&limit=250&access_token=" . $this->access_token;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response1 = json_decode($result, true);
                    $campaignData['insights']['data'] = $response1['data'];
                    $campaignData['insights']['paging'] = $response1['paging'];
                    array_push($finalArr, $campaignData);
                }
                $addSetDataArray = $this->getFinalResult($finalArr, $campaignId, $addSetId);
                if ($addSetDataArray) {
                    $resultArray = array(
                        "count" => 1,
                        "response" => $addSetDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => "No record found.",
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record found.",
                    "success" => ""
                );
            }*/
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getAddsGender() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
        }

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($genders['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($genders['gender'] == "female") {
                                $female = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "male") {
                                $male = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "unknown") {
                                $other = $feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($genders['gender'] == "female") {
                        $female = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "male") {
                        $male = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "unknown") {
                        $other = $genders[$pageName];
                    }
                }
            }
        }


        $response = array(
            "code" => 100,
            "male" => $male,
            "female" => $female,
            "other" => $other,
        );
        echo json_encode($response);
    }

    function getAddsGender_without_ajax($campaignId,$addSetId,$limit1,$pagename) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $campaignId;//$this->input->post('campaignId');
        $addSetId = $addSetId;//$this->input->post('addsetId');
        $limit = $limit1;//$this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7d&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
        }

        $pageName = !empty($pagename) ? $pagename : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($genders['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($genders['gender'] == "female") {
                                $female = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "male") {
                                $male = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "unknown") {
                                $other = $feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($genders['gender'] == "female") {
                        $female = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "male") {
                        $male = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "unknown") {
                        $other = $genders[$pageName];
                    }
                }
            }
        }


        $response = array(
            "code" => 100,
            "male" => $male,
            "female" => $female,
            "other" => $other,
        );
        $tempstr1 = array();
        $tempstr1[] = number_format($male+$female+$other);
        $tempstr1[] = '<li><span class="text-muted location">MEN</span> <span class="text-bold">'.number_format($male).'</span></li><li><span class="text-muted location">WOMEN</span> <span class="text-bold">'.number_format($female).'</span></li><li><span class="text-muted location">OTHER</span> <span class="text-bold">'.number_format($other).'</span></li>';
        return $tempstr1;
    }

    function getAddsGendervarious() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7d&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['gender']&access_token=" . $this->access_token;
        }

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($genders['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($genders['gender'] == "female") {
                                $female = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "male") {
                                $male = $feeds1['value'];
                                $total+=$feeds1['value'];
                            } else if ($genders['gender'] == "unknown") {
                                $other = $feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($genders['gender'] == "female") {
                        $female = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "male") {
                        $male = $genders[$pageName];
                        $total+=$genders[$pageName];
                    } else if ($genders['gender'] == "unknown") {
                        $other = $genders[$pageName];
                    }
                }
            }
        }


        $response = array(
            "code" => 100,
            "male" => $male,
            "female" => $female,
            "other" => $other,
        );
        $tempstr1 = array();
        $tempstr1[] = number_format($male+$female+$other);
        $tempstr1[] = '<li><span class="text-muted location">MEN</span> <span class="text-bold">'.number_format($male).'</span></li><li><span class="text-muted location">WOMEN</span> <span class="text-bold">'.number_format($female).'</span></li><li><span class="text-muted location">OTHER</span> <span class="text-bold">'.number_format($other).'</span></li>';

        echo json_encode($tempstr1);
        exit;
    }

    function getAddsChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();
        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $res[] = array(
                                "range" => $ages['age'],
                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                "impressions" => $feeds1['value'],
                                "gender" => $ages['gender']
                            );
                            $total += $ages['inline_link_clicks'];
                        }
                    }
                }
                else{
                    $res[] = array(
                        "range" => $ages['age'],
                        "inline_link_clicks" => $ages['inline_link_clicks'],
                        "impressions" => $ages[$pageName],
                        "gender" => $ages['gender']
                    );
                    $total += $ages['inline_link_clicks'];
                }
            }

            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }
        
        $tempArr = array();
        foreach ($resultArray as $k => $value) {
            $tempStr1 = '0';
            foreach ($value as $k1 => $value1) {
                $tempStr1 += $value1['impressions'];
            }
            $tempArr[$k] = $tempStr1;
        }


        if (!array_key_exists('18-24', $resultArray)) {
            $tempArr['18-24'] = 0;
        }
        if (!array_key_exists('25-34', $resultArray)) {
            $tempArr['25-34'] = 0;
        }
        if (!array_key_exists('35-44', $resultArray)) {
            $tempArr['35-44'] = 0;
        }
        if (!array_key_exists('45-54', $resultArray)) {
            $tempArr['45-54'] = 0;
        }
        if (!array_key_exists('55-64', $resultArray)) {
            $tempArr['55-64'] = 0;
        }
        if (!array_key_exists('65+', $resultArray)) {
            $tempArr['65+'] = 0;
        }
        
        foreach ($tempArr as $k => $value) {
            $tempStr .= '{
                            "y":"' . $k . '",
                            "value":' . $value . '
                         },';
        }
        $tempStr2 = rtrim($tempStr, ',');
        $tempstr1 = '[
            ' . $tempStr2 . '
            ]';
            
        echo $tempstr1;
        exit;
    }

    function getAddsChart_without_ajax($campaignId,$addSetId,$limit1,$pagename) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $campaignId;//$this->input->post('campaignId');
        $addSetId = $addSetId;//$this->input->post('addsetId');
        $limit = $limit1;//$this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();
        $pageName = !empty($pagename) ? $pagename : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $res[] = array(
                                "range" => $ages['age'],
                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                "impressions" => $feeds1['value'],
                                "gender" => $ages['gender']
                            );
                            $total += $ages['inline_link_clicks'];
                        }
                    }
                }
                else{
                    $res[] = array(
                        "range" => $ages['age'],
                        "inline_link_clicks" => $ages['inline_link_clicks'],
                        "impressions" => $ages[$pageName],
                        "gender" => $ages['gender']
                    );
                    $total += $ages['inline_link_clicks'];
                }
            }

            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }
        
        $tempArr = array();
        foreach ($resultArray as $k => $value) {
            $tempStr1 = '0';
            foreach ($value as $k1 => $value1) {
                $tempStr1 += $value1['impressions'];
            }
            $tempArr[$k] = $tempStr1;
        }


        if (!array_key_exists('18-24', $resultArray)) {
            $tempArr['18-24'] = 0;
        }
        if (!array_key_exists('25-34', $resultArray)) {
            $tempArr['25-34'] = 0;
        }
        if (!array_key_exists('35-44', $resultArray)) {
            $tempArr['35-44'] = 0;
        }
        if (!array_key_exists('45-54', $resultArray)) {
            $tempArr['45-54'] = 0;
        }
        if (!array_key_exists('55-64', $resultArray)) {
            $tempArr['55-64'] = 0;
        }
        if (!array_key_exists('65+', $resultArray)) {
            $tempArr['65+'] = 0;
        }
        
        foreach ($tempArr as $k => $value) {
            $agegroup_total += $value;
            $tempStr .= '<li><span class="text-muted location">'.$k.'</span> <span class="text-bold">'.number_format($value).'</span></li>';

        }
        $tempstr1 = array();
        $tempstr1[] = number_format($agegroup_total);
        $tempstr1[] = $tempStr;
        return $tempstr1;
    }

    function getAddsChartvarious() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['age','gender']&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();
        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $res[] = array(
                                "range" => $ages['age'],
                                "inline_link_clicks" => $ages['inline_link_clicks'],
                                "impressions" => $feeds1['value'],
                                "gender" => $ages['gender']
                            );
                            $total += $ages['inline_link_clicks'];
                        }
                    }
                }
                else{
                    $res[] = array(
                        "range" => $ages['age'],
                        "inline_link_clicks" => $ages['inline_link_clicks'],
                        "impressions" => $ages[$pageName],
                        "gender" => $ages['gender']
                    );
                    $total += $ages['inline_link_clicks'];
                }
            }

            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }
        
        $tempArr = array();
        foreach ($resultArray as $k => $value) {
            $tempStr1 = '0';
            foreach ($value as $k1 => $value1) {
                $tempStr1 += $value1['impressions'];
            }
            $tempArr[$k] = $tempStr1;
        }


        if (!array_key_exists('18-24', $resultArray)) {
            $tempArr['18-24'] = 0;
        }
        if (!array_key_exists('25-34', $resultArray)) {
            $tempArr['25-34'] = 0;
        }
        if (!array_key_exists('35-44', $resultArray)) {
            $tempArr['35-44'] = 0;
        }
        if (!array_key_exists('45-54', $resultArray)) {
            $tempArr['45-54'] = 0;
        }
        if (!array_key_exists('55-64', $resultArray)) {
            $tempArr['55-64'] = 0;
        }
        if (!array_key_exists('65+', $resultArray)) {
            $tempArr['65+'] = 0;
        }
        
         foreach ($tempArr as $k => $value) {
            $agegroup_total += $value;
            $tempStr .= '<li><span class="text-muted location">'.$k.'</span> <span class="text-bold">'.number_format($value).'</span></li>';

        }
        $tempstr1 = array();
        $tempstr1[] = number_format($agegroup_total);
        $tempstr1[] = $tempStr;
        echo json_encode($tempstr1);
        exit;
    }


    

    function toolStartStop() {


        $addsetId = $this->input->post('Id');
        $action = $this->input->post('action');


        $addset = new AdSet($addsetId);
        $res = "";
        if ($action == "ACTIVE") {


            $res = $addset->update(array(
                AdSet::STATUS_PARAM_NAME => AdSet::STATUS_ACTIVE,
            ));
        } else {

            $res = $addset->update(array(
                AdSet::STATUS_PARAM_NAME => AdSet::STATUS_PAUSED,
            ));
        }

        $res = $addset->update();
        $this->getPrintResult($res);
        echo "100";
    }

    function getAddsetObjective($campaignId) {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/?fields=effective_status,configured_status&access_token=" . $this->access_token;
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addsetId/?fields=objective,effective_status&access_token=" . $this->access_token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //   $this->getPrintResult($response);
        return $response['configured_status'];
    }

    function getAddsCountryChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
        }

        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $tempStr = '';
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $tempStr .= '{
                                            "label":"' . $result[0]->Country . '",
                                            "value":' . $feeds1['value'] . '
                                         },';
                        }
                    }
                }
                else{
                    $tempStr .= '{
                            "label":"' . $result[0]->Country . '",
                            "value":' . $ages[$pageName] . '
                         },';
                }
            }
        }

        $tempStr2 = rtrim($tempStr, ',');
        $tempstr1 = '[
            ' . $tempStr2 . '
            ]';
        echo $tempstr1;
        exit;
    }

    function getAddsCountryChart_without_ajax($campaignId,$addSetId,$limit1,$pagename) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $campaignId;//$this->input->post('campaignId');
        $addSetId = $addSetId;//$this->input->post('addsetId');
        $limit = $limit1;//$this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7d&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
        }

        $res = array();
        $resultArray = array();

        $pageName = !empty($pagename) ? $pagename : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $tempStr = '';
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $countrytotal += (int)$feeds1['value'];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                        }
                    }
                }
                else{
                    $countrytotal += (int)$ages[$pageName];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                }
            }
        }

        $tempstr1 = array();
        $tempstr1[] = number_format($countrytotal);
        $tempstr1[] = $tempStr;
        return $tempstr1;
    }

    function getAddsCountryChartvarious() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7d&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
        }

        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $tempStr = '';
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($ages['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $countrytotal += (int)$feeds1['value'];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($feeds1['value']).'</span></li>';
                        }
                    }
                }
                else{
                    $countrytotal += (int)$ages[$pageName];
                            $tempStr .= '<li><span class="text-muted location">'.strtoupper($result[0]->Country).' </span> <span class="text-bold">'.number_format($ages[$pageName]).'</span></li>';
                }
            }
        }

        $tempstr1 = array();
        $tempstr1[] = number_format($countrytotal);
        $tempstr1[] = $tempStr;
        echo json_encode($tempstr1);
        exit;
    }

    function getAddsTimeChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,reach,impressions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&sort=['reach_descending']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=30&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $time_4_am = strtotime('04:00:00');
        $time_8_am = strtotime('08:00:00');
        $time_12_pm = strtotime('12:00:00');
        $time_4_pm = strtotime('16:00:00');
        $time_8_pm = strtotime('20:00:00');
        $time_12_am = strtotime('23:59:59');

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $timeArr = explode(' - ', $ages['hourly_stats_aggregated_by_audience_time_zone']);
                $time = strtotime($timeArr[0]);
                if ($time_12_am > $time && $time_4_am >= $time) {
                    $post_12am_to4am[] = $ages;
                } else if ($time_4_am < $time && $time_8_am >= $time) {
                    $post_4am_to8am[] = $ages;
                } else if ($time_8_am < $time && $time_12_pm >= $time) {
                    $post_8am_to12pm[] = $ages;
                } else if ($time_12_pm < $time && $time_4_pm >= $time) {
                    $post_12pm_to4pm[] = $ages;
                } else if ($time_4_pm < $time && $time_8_pm >= $time) {
                    $post_4pm_to8pm[] = $ages;
                } else if ($time_8_pm < $time && $time_12_am >= $time) {
                    $post_8pm_to12am[] = $ages;
                }
            }
        }
        $count_12am_to4am = 0;
        if (is_array($post_12am_to4am)) {
            foreach ($post_12am_to4am as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_12am_to4am = $count_12am_to4am + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_12am_to4am = $count_12am_to4am + $post[$pageName];
                }
            }
        }
        $count_4am_to8am = 0;
        if (is_array($post_4am_to8am)) {
            foreach ($post_4am_to8am as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_4am_to8am = $count_4am_to8am + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_4am_to8am = $count_4am_to8am + $post[$pageName];
                }
            }
        }
        $count_8am_to12pm = 0;
        if (is_array($post_8am_to12pm)) {
            foreach ($post_8am_to12pm as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_8am_to12pm = $count_8am_to12pm + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_8am_to12pm = $count_8am_to12pm + $post[$pageName];
                }
            }
        }
        $count_12pm_to4pm = 0;
        if (is_array($post_12pm_to4pm)) {
            foreach ($post_12pm_to4pm as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_12pm_to4pm = $count_12pm_to4pm + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_12pm_to4pm = $count_12pm_to4pm + $post[$pageName];
                }
            }
        }
        $count_4pm_to8pm = 0;
        if (is_array($post_4pm_to8pm)) {
            foreach ($post_4pm_to8pm as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_4pm_to8pm = $count_4pm_to8pm + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_4pm_to8pm = $count_4pm_to8pm + $post[$pageName];
                }
            }
        }
        $count_8pm_to12am = 0;
        if (is_array($post_8pm_to12am)) {
            foreach ($post_8pm_to12am as $post) {
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($post['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            $count_8pm_to12am = $count_8pm_to12am + $feeds1['value'];
                        }
                    }
                }
                else{
                    $count_8pm_to12am = $count_8pm_to12am + $post[$pageName];
                }
            }
        }

        $tempStr .= '{
                        "y":"0-4",
                        "Impressions":' . $count_12am_to4am . '
                     },{
                        "y":"5-8",
                        "Impressions":' . $count_4am_to8am . '
                     },{
                        "y":"9-12",
                        "Impressions":' . $count_8am_to12pm . '
                     },{
                        "y":"13-16",
                        "Impressions":' . $count_12pm_to4pm . '
                     },{
                        "y":"17-20",
                        "Impressions":' . $count_4pm_to8pm . '
                     },{
                        "y":"21-24",
                        "Impressions":' . $count_8pm_to12am . '
                     }';

        $tempstr1 = '[
            ' . $tempStr . '
            ]';
        echo $tempstr1;
        exit;
    }

    function clickData(){
        #$addSetId = '23842522915290160';
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $addSetId = $this->input->post('addsetId');
        $campaignObj = new AdSet($addSetId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'lifetime',
            'time_increment' => 1,//all_days
        );

        $fields = array('inline_link_clicks','date_start','spend');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'inline_link_clicks' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $clicks = 0;
                if(!empty($value['inline_link_clicks'])){
                    $clicks = $value['inline_link_clicks'];
                }
                $tempStr .= '{
                            "year":"'.$value['date_start'].'",
                            "value":' . $clicks . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function getAddsEngagementData(){
        #$addSetId = '23842522915290160';
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $addSetId = $this->input->post('addsetId');
        $campaignObj = new AdSet($addSetId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));
        $this->date_to = date('Y-m-d');

        #$this->date_from = '2016-11-03';
        #$this->date_to = '2016-11-11';

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'lifetime',
            'time_increment' => 1,//all_days
        );

        $fields = array('actions','cost_per_action_type');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if($k1 == 'actions'){
                        $i=0;
                        foreach ($pt['actions'] as $key1 => $value1) {
                            if($value1['action_type'] == 'page_engagement'){
                                $arr[$k][$value1['action_type']] = $value1['value'];
                                $arr[$k]['cost_per_action_type'] = $pt['cost_per_action_type'][$i]['value'];
                            }
                            $i++;
                        }
                        $l++;
                    }
                    if ($k1 == 'date_start') {
                        $arr[$k]['date_start'] = $v1;
                        if (isset($kv_dates[$v1])) {
                            unset($kv_dates[$v1]);
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'page_engagement' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);

        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $page_engagement = 0;
                if(!empty($value['page_engagement'])){
                    $page_engagement = $value['page_engagement'];
                }
                $tempStr .= '{
                    "year":"'.$value['date_start'].'",
                    "value":' . $page_engagement . '
                 },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;
            exit;
        }
    }

    function getAddsPageLikeData(){
        #$addSetId = '23842522915290160';
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $addSetId = $this->input->post('addsetId');
        $campaignObj = new AdSet($addSetId);

        //$this->date_from = '2016-11-03';
        //$this->date_to = '2016-11-11';

        $this->date_from = date('Y-m-d', strtotime('-6 days'));
        $this->date_to = date('Y-m-d');

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'lifetime',
            'time_increment' => 1,//all_days
        );

        $fields = array('actions','cost_per_action_type');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if($k1 == 'actions'){
                        $i=0;
                        foreach ($pt['actions'] as $key1 => $value1) {
                            if($value1['action_type'] == 'like'){
                                $arr[$k]['like'] = $value1['value'];
                                $arr[$k]['cost_per_action_type'] = $pt['cost_per_action_type'][$i]['value'];
                            }
                            $i++;
                        }
                        $l++;
                    }
                    if ($k1 == 'date_start') {
                        $arr[$k]['date_start'] = $v1;
                        if (isset($kv_dates[$v1])) {
                            unset($kv_dates[$v1]);
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'like' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);

        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $like = 0;
                if(!empty($value['like'])){
                    $like = $value['like'];
                }
                $tempStr .= '{
                    "y":"'.$value['date_start'].'",
                    "a":' . $like . '
                 },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;
            exit;
        }
    }

    function getAddsConversionsData(){
       #$addSetId = '23842522915290160';
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $addSetId = $this->input->post('addsetId');
        $campaignObj = new AdSet($addSetId);

        //$this->date_from = '2016-11-03';
        //$this->date_to = '2016-11-11';

        $this->date_from = date('Y-m-d', strtotime('-6 days'));
        $this->date_to = date('Y-m-d');

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'lifetime',
            'time_increment' => 1,//all_days
        );

        $fields = array('actions','cost_per_action_type');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if($k1 == 'actions'){
                        $i=0;
                        foreach ($pt['actions'] as $key1 => $value1) {
                            if($value1['action_type'] == 'offsite_conversion'){
                                $arr[$k]['offsite_conversion'] = !empty($value1['value']) ? $value1['value'] : '0';
                                $arr[$k]['cost_per_action_type'] = !empty($pt['cost_per_action_type'][$i]['value']) ? $pt['cost_per_action_type'][$i]['value'] : '0.0';
                            }
                            $i++;
                        }
                        $l++;
                    }
                    if ($k1 == 'date_start') {
                        $arr[$k]['date_start'] = $v1;
                        if (isset($kv_dates[$v1])) {
                            unset($kv_dates[$v1]);
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'offsite_conversion' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);

        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $offsite_conversion = 0;
                if(!empty($value['offsite_conversion'])){
                    $offsite_conversion = $value['offsite_conversion'];
                }
                $tempStr .= '{
                    "year":"'.$value['date_start'].'",
                    "value":' . $offsite_conversion . '
                 },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;
            exit;
        }
    }

    function getAddsSpentData(){
        #$addSetId = '23842522915290160';
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $addSetId = $this->input->post('addsetId');
        $campaignObj = new AdSet($addSetId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'lifetime',
            'time_increment' => 1,//all_days
        );

        $fields = array('spend','date_start');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'spend' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $spend = 0;
                if(!empty($value['spend'])){
                    $spend = $value['spend'];
                }
                $tempStr .= '{
                            "year":"'.$value['date_start'].'",
                            "value":' . $spend . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function getAddsImpressionData(){
        #$addSetId = '23842522915290160';
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $addSetId = $this->input->post('addsetId');
        $campaignObj = new AdSet($addSetId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'lifetime',
            'time_increment' => 1,//all_days
        );

        $fields = array('impressions','date_start');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'impressions' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $impressions = 0;
                if(!empty($value['impressions'])){
                    $impressions = $value['impressions'];
                }
                $tempStr .= '{
                            "year":"'.$value['date_start'].'",
                            "value":' . $impressions . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function getAddsCtrData(){
        #$addSetId = '23842522915290160';
        Api::init($this->app_id, $this->app_secret, $this->access_token);
        $addSetId = $this->input->post('addsetId');
        $campaignObj = new AdSet($addSetId);

        $this->date_from = date('Y-m-d', strtotime('-6 days'));//'2017-02-02';
        $this->date_to = date('Y-m-d');//'2017-02-08';exit;

        $params = array(
            'time_range' => array(
                'since' => $this->date_from,
                'until' => $this->date_to,
            ),
            //'date_preset' => 'lifetime',
            'time_increment' => 1,//all_days
        );

        $fields = array('inline_link_click_ctr','date_start');
        $insights = $campaignObj->getInsights($fields, $params);
        $res = $insights->getObjects();
        $res = $this->asArray($res);
        $kv_dates = $this->fillDatesKeys($this->date_from, $this->date_to);
        $l = 0;
        if(!empty($res)){
            foreach ($res as $k => $pt) {
                foreach ($pt as $k1 => $v1) {
                    if (!empty($v1)) {
                        $arr[$k][$k1] = $v1;
                        $l++;
                        if ($k1 == 'date_start') {
                            if (isset($kv_dates[$v1])) {
                                unset($kv_dates[$v1]);
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($kv_dates as $k => $v) {
            $arr[] = ['date_start' => $k, 'date_stop' => $k, 'inline_link_click_ctr' => 0];
        }

        $graphArr = $this->orderByField($arr, 'date_start', $ascending = true);
        
        $tempstr1 = '';
        if(!empty($graphArr)){
            foreach ($graphArr as $key => $value) {
                $ctr = 0;
                if(!empty($value['inline_link_click_ctr'])){
                    $ctr = $value['inline_link_click_ctr'];
                }
                $tempStr .= '{
                            "year":"'.$value['date_start'].'",
                            "value":' . $ctr . '
                         },';
            }

            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
            
            echo $tempstr1;exit;
        }
        else{
            echo $tempstr1 = '[]';exit;
        }
    }

    function getAddsPlacementData() {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions','total_actions']&breakdowns=['publisher_platform','device_platform','platform_position']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
        }
        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);

        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
                /* if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['placement'] == "desktop_feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "instant_article") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_external_only") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "right_hand") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['placement'] == "desktop_feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "instant_article") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_external_only") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "right_hand") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }*/
				
				if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "instagram") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "audience_network") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "instagram") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "audience_network") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }
            }
            if ($desktop > 0) {
                $desktopPer = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobilePer = round(($mobile / $total) * 100);
            }
            if ($instant_article > 0) {
                $instant_articlePer = round(($instant_article / $total) * 100);
            }
            if ($mobile_external_only > 0) {
                $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
            }
            if ($right_hand > 0) {
                $right_handPer = round(($right_hand / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => !empty($desktop) ? $desktop : '0',
            "mobile" => !empty($mobile) ? $mobile : '0',
            "instant_article" => !empty($instant_article) ? $instant_article : '0',
            "mobile_external_only" => !empty($mobile_external_only) ? $mobile_external_only : '0',
            "right_hand" => !empty($right_hand) ? $right_hand : '0',
            "desktopPer" => !empty($desktopPer) ? $desktopPer.'%' : '0%',
            "mobilePer" => !empty($mobilePer) ? $mobilePer.'%' : '0%',
            "instant_articlePer" => !empty($instant_articlePer) ? $instant_articlePer.'%' : '0%',
            "mobile_external_onlyPer" => !empty($mobile_external_onlyPer) ? $mobile_external_onlyPer.'%' : '0%',
            "right_handPer" => !empty($right_handPer) ? $right_handPer.'%' : '0%',
            "total" => $total
        );
        echo json_encode($response);
    }

    function getAddsPlacementData_without_ajax($campaignId,$addSetId,$limit1,$pagename) {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $campaignId = $campaignId;//$this->input->post('campaignId');
        $addSetId = $addSetId;//$this->input->post('addsetId');
        $limit = $limit1;//$this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions','total_actions']&breakdowns=['publisher_platform','device_platform','platform_position']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
        }
        $res = array();
        $resultArray = array();

        $pageName = !empty($pagename) ? $pagename : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);

        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
                /* if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['placement'] == "desktop_feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "instant_article") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_external_only") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "right_hand") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['placement'] == "desktop_feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "instant_article") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_external_only") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "right_hand") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }*/
                
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "instagram") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "audience_network") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "instagram") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "audience_network") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }
            }
            if ($desktop > 0) {
                $desktopPer = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobilePer = round(($mobile / $total) * 100);
            }
            if ($instant_article > 0) {
                $instant_articlePer = round(($instant_article / $total) * 100);
            }
            if ($mobile_external_only > 0) {
                $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
            }
            if ($right_hand > 0) {
                $right_handPer = round(($right_hand / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => !empty($desktop) ? $desktop : '0',
            "mobile" => !empty($mobile) ? $mobile : '0',
            "instant_article" => !empty($instant_article) ? $instant_article : '0',
            "mobile_external_only" => !empty($mobile_external_only) ? $mobile_external_only : '0',
            "right_hand" => !empty($right_hand) ? $right_hand : '0',
            "desktopPer" => !empty($desktopPer) ? $desktopPer.'%' : '0%',
            "mobilePer" => !empty($mobilePer) ? $mobilePer.'%' : '0%',
            "instant_articlePer" => !empty($instant_articlePer) ? $instant_articlePer.'%' : '0%',
            "mobile_external_onlyPer" => !empty($mobile_external_onlyPer) ? $mobile_external_onlyPer.'%' : '0%',
            "right_handPer" => !empty($right_handPer) ? $right_handPer.'%' : '0%',
            "total" => $total
        );
        $tempstr1 = array();
        $tempstr1[] = number_format($total);
        if(empty($desktop)) $desktop=0;
        if(empty($mobile)) $mobile=0;
        if(empty($instant_article)) $instant_article=0;
        if(empty($mobile_external_only)) $mobile_external_only=0;
        if(empty($right_hand)) $right_hand=0;
        $tempstr1[] = '<li><span class="text-muted location">DESKTOP FEED</span> <span class="text-bold">'.number_format($desktop).'</span></li>
                                <li><span class="text-muted location">MOBILE FED</span> <span class="text-bold">'.number_format($mobile).'</span></li>
                                <li><span class="text-muted location">INSTAGRAM</span> <span class="text-bold">'.number_format($instant_article).'</span></li>
                                <li><span class="text-muted location">AUDIENCE</span> <span class="text-bold">'.number_format($mobile_external_only).'</span></li>
                                <li><span class="text-muted location">RIGHT SIDE</span> <span class="text-bold">'.number_format($right_hand).'</span></li>';
        return $tempstr1;
    }

    function getAddsPlacementDatavarious() {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions','total_actions']&breakdowns=['publisher_platform','device_platform','platform_position']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights?" . $date_preset . "fields=['clicks','impressions','reach','actions','date_start','campaign_name','ctr','call_to_action_clicks','cost_per_action_type','cost_per_unique_click','unique_clicks','spend','objective']&breakdowns=['publisher_platform','device_platform','platform_position']&access_token=" . $this->access_token;
        }
        $res = array();
        $resultArray = array();

        $pageName = !empty($_POST['pageName']) ? $_POST['pageName'] : 'impressions';
        if($pageName == 'adddsReport'){
            $pageName = 'impressions';
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);

        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
                /* if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['placement'] == "desktop_feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "instant_article") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "mobile_external_only") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['placement'] == "right_hand") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['placement'] == "desktop_feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "instant_article") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "mobile_external_only") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['placement'] == "right_hand") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }*/
                
                if($pageName == 'page_engagement' || $pageName == 'like' || $pageName == 'offsite_conversion'){
                    foreach ($feeds['actions'] as $feeds1) {
                        if ($feeds1['action_type'] == $pageName) {
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $desktop = $feeds1['value'];
                                $total += $feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                                $mobile = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "instagram") {
                                $instant_article = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['publisher_platform'] == "audience_network") {
                                $mobile_external_only = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                            if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                                $right_hand = $feeds1['value'];
                                $total +=$feeds1['value'];
                            }
                        }
                    }
                }
                else{
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $desktop = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "mobile" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "feed") {
                        $mobile = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "instagram") {
                        $instant_article = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['publisher_platform'] == "audience_network") {
                        $mobile_external_only = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                    if ($feeds['device_platform'] == "desktop" && $feeds['publisher_platform'] == "facebook" && $feeds['platform_position'] == "right_hand_column") {
                        $right_hand = $feeds[$pageName];
                        $total +=$feeds[$pageName];
                    }
                }
            }
            if ($desktop > 0) {
                $desktopPer = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobilePer = round(($mobile / $total) * 100);
            }
            if ($instant_article > 0) {
                $instant_articlePer = round(($instant_article / $total) * 100);
            }
            if ($mobile_external_only > 0) {
                $mobile_external_onlyPer = round(($mobile_external_only / $total) * 100);
            }
            if ($right_hand > 0) {
                $right_handPer = round(($right_hand / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => !empty($desktop) ? $desktop : '0',
            "mobile" => !empty($mobile) ? $mobile : '0',
            "instant_article" => !empty($instant_article) ? $instant_article : '0',
            "mobile_external_only" => !empty($mobile_external_only) ? $mobile_external_only : '0',
            "right_hand" => !empty($right_hand) ? $right_hand : '0',
            "desktopPer" => !empty($desktopPer) ? $desktopPer.'%' : '0%',
            "mobilePer" => !empty($mobilePer) ? $mobilePer.'%' : '0%',
            "instant_articlePer" => !empty($instant_articlePer) ? $instant_articlePer.'%' : '0%',
            "mobile_external_onlyPer" => !empty($mobile_external_onlyPer) ? $mobile_external_onlyPer.'%' : '0%',
            "right_handPer" => !empty($right_handPer) ? $right_handPer.'%' : '0%',
            "total" => $total
        );
         $tempstr1 = array();
        $tempstr1[] = number_format($total);
        if(empty($desktop)) $desktop=0;
        if(empty($mobile)) $mobile=0;
        if(empty($instant_article)) $instant_article=0;
        if(empty($mobile_external_only)) $mobile_external_only=0;
        if(empty($right_hand)) $right_hand=0;
        $tempstr1[] = '<li><span class="text-muted location">DESKTOP FEED</span> <span class="text-bold">'.number_format($desktop).'</span></li>
                                <li><span class="text-muted location">MOBILE FED</span> <span class="text-bold">'.number_format($mobile).'</span></li>
                                <li><span class="text-muted location">INSTAGRAM</span> <span class="text-bold">'.number_format($instant_article).'</span></li>
                                <li><span class="text-muted location">AUDIENCE</span> <span class="text-bold">'.number_format($mobile_external_only).'</span></li>
                                <li><span class="text-muted location">RIGHT SIDE</span> <span class="text-bold">'.number_format($right_hand).'</span></li>';
        echo json_encode($tempstr1);
        exit;
    }

    

    function setDateSession(){
        if ($this->input->post('ajax') == 1) {
            $this->is_ajax = 1;
            $limit = $this->input->post('limit');
            $this->session->set_userdata('dateval', $limit);
        }        
    }

    public function asArray($data) {
        foreach ($data as $k => $v)
            $data[$k] = $v->getData();
        return $data;
    }

    public static function fillDatesKeys($date_from, $date_to) {
        $kv = [];
        $date = $date_from;
        while ($date <= $date_to) {
            $kv[$date] = 0;
            $date = date("Y-m-d", strtotime($date) + 86400);
        }

        return $kv;
    }

    public static function orderByField($arr, $f, $ascending = true) {
        if (is_array($f)) {
            $f1 = $f[0];
            $ka = [];
            foreach ($arr as $el) {
                $f1v = $el[$f1];
                //hr($f1v);
                if (!isset($ka[$f1v]))
                    $ka[$f1v] = [];
                $ka[$f1v][] = $el;
            }

            //hre($ka);

            if ($ascending)
                ksort($ka);
            else
                krsort($ka);

            $arr = [];
            foreach ($ka as $els) {
                $f2 = $f[1];
                $els = $this->orderByField($els, $f2, $ascending);
                $arr = array_merge($arr, $els);
            }

            return $arr;
        }

        $kv = [];
        foreach ($arr as $k => $v)
            $kv[$k] = $v[$f];

        if ($ascending)
            asort($kv);
        else
            arsort($kv);

        $res = [];
        foreach ($kv as $k => $v)
            $res[] = $arr[$k];

        return $res;
    }
}
