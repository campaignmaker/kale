<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Flashsale extends CI_Controller {

    public function __construct() {
		 error_reporting(1);
       		parent::__construct();
		$this->load->model('Users_model');
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}

    }

    public function index() {

        $data = new stdClass();
        $this->load->view('flashsale');
		//loadView('trialactivate2', $data);
    }
	public function processpayment(){
		if(isset($_POST['action']) && $_POST['action'] == 'stripe') {
		  //echo "posted";
		  require_once(APPPATH.'libraries/stripe/Stripe.php');
		  //echo "posted and included";
		  $amount = base64_decode($_POST['amount']) * 100;
		  $token = $_POST['stripeToken'];
		  Stripe::setApiKey("sk_live_JEgEezB4NK9jSIEje15zkyn3");

		  try {

				$customer = Stripe_Customer::create(array(

					'card' => $token,
					'plan' => $_POST['selectedplan'],
					'email' => $this->session->userdata['logged_in']['email'],
					'description' => $this->session->userdata['logged_in']['first_name'],
					'metadata' => array("item_number" => $this->session->userdata['logged_in']['id'],"ip" => $_POST['idev_custom'])

				  )

				);


				$this->Users_model->update_stripe_customer($this->session->userdata['logged_in']['id'], $customer->id);
				$billingstatus = $this->Users_model->getbillingstartenddate($this->session->userdata['logged_in']['id']);
				if(isset($billingstatus) && $billingstatus != "dontupdatethis"){
				    $this->Users_model->update_billingdates($billingstatus['subscriptid']);
				    redirect(base_url().'login/user_login_sessionafterbilling');
				}
				// redirect on successful recurring payment setup
				redirect(base_url().'dashboard');



			  } catch (Exception $e) {

		        error_log($e->getMessage());
				// redirect on failure

				redirect(base_url().'flashsale');

			  }

		}
		else{
			redirect(base_url().'flashsale');
		}
	}
}

//End of class
