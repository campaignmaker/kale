<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Register extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->load->model('Users_model');
        $this->load->model('Upgrade_model');
        $this->load->library('getresponse');
        $this->seo->SetValues('Title', "Register");
        $this->seo->SetValues('Description', "Register");
    }

    public function index() {

        if (!($this->session->userdata('logged_in'))) {
            $data = new stdClass();

            // set validation rules
            $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|required|valid_email');
            $this->form_validation->set_rules('firstname', 'First Name', 'trim|xss_clean|required|min_length[4]');
            $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|required|min_length[6]');

            if ($this->form_validation->run() === false) {

                // validation not ok, send validation errors to the view
                //loadView('register', $data);
                $this->load->view('register', $data);
            } else {

                // set variables from the form
                $user['first_name'] = $this->input->post('firstname');
                $user['last_name'] = $this->input->post('lastname');
                $user['email'] = $this->input->post('email');
                $user['password'] = md5($this->input->post('password'));
                $user['hash'] = md5(uniqid(rand(), true));
                $user['status'] = 1;

                $userId = $this->Users_model->register($user);

                if ($userId) {

               
                    $sevenDay = date('Y-m-d', strtotime("+7 days"));

                    $subcription_data = array('user_id' => $userId, 'packages_id' => 1, 'status' => 'Active', 'billing_start_date' => "0000-00-00", 'billing_end_date' => "0000-00-00");
                    $this->Upgrade_model->userSubscription($subcription_data);
                    
                    
                    
                    $str .= "Hello and Welcome to the campaign maker family!";
					$str .= "\n";
					$str .= "\n";
					$str .= "Here are your login details:";
					$str .= "\n";
					$str .= "\n";
					$str .= "Email = " . $user['email'];
					$str .= "\n";
					$str .= "\n";
					//$str .= "Password = " . $Pwd;
					//$str .= "\n";
					//$str .= "\n";
					$str .= "Here is the login link: https://thecampaignmaker.com/ui/login";
					$str .= "\n";
					$str .= "\n";
					$str .= "If you have any questions, concerns or run into any issues, make sure you let us know by replying to this email anytime.";
					$str .= "\n";
					$str .= "\n";
					$str .= "I highly suggest you check out our detailed training videos before creating your first campaign here: http://thecampaignmaker.com/tcm/helps";
					$str .= "\n";
					$str .= "\n";
					$str .= "Happy Campaigning!";
					$str .= "\n";
					$str .= "Samy";
	
					$separator = md5(time());
	
					$eol = PHP_EOL;
					$headers = "";
					$headers = "From: The Campaign Maker <samy@thecampaignmaker.com>" . $eol;
					$headers .= "Reply-To: The Campaign Maker <samy@thecampaignmaker.com>" . $eol;
					$headers .= "MIME-Version: 1.0" . $eol;
					$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"";
					$msg = wordwrap($str, 70);
					//add_filter('wp_mail_from_name', 'The Campaign Maker');
					//wp_mail($Email, "TCM login detail", $msg, $headers);
					mail($user['email'],"TCM login detail",$msg, $headers);
                    
                    
                    
                    

                   /* $url = site_url("register/verify/" . $user['hash']);
                    $to = $user['email'];
                    $subject = 'Confirm Your Email To Activate Your Free 7-Day Trial';

                    $message = "<p>Hey " . ucfirst($user['first_name']) . ",</p><p>Thank you for signing up to The Campaign Maker!</p><p>You are almost ready to start your epic Facebook marketing journey with us.</p><p>Click the below link to officially start your completely free 7-day trial.</p><p><a href='" . $url . "'>Activate My Account</a><p>Please let us know if you have any questions or concerns about anything.</p><p>Welcome To The Family & Happy Campaigning,<br>The Campaign Maker Team</p>";
                    
                    sendEmail($to, $subject, $message);

                    $data->success = 'Account created successfully. Check your email for confirmation';
*/
                    //Save data into Get Response------------------------
                 //   $this->getresponse->addContact('pU8yj', $user['first_name'], $user['email'], 'insert', 0);
                    //------------------------------------------------------------------------------------
                    //Create user subcription 
                    // user creation ok
                    $this->form_validation->resetpostdata(true);
                    //loadView('register', $data);
                    header("Location: https://thecampaignmaker.com/ui/login/wpsuerregister/".base64_encode($userId));exit;
                    //$this->load->view('login2', $data);
                } else {

                    // user creation failed, this should never happen
                    $data->error = 'There was a problem creating your new account. Please try again.';
                    // send error to the view
                    $this->load->view('register', $data);
                    //loadView('register', $data);
                }
            }
        } else {
            redirect(site_url('dashboard'));
        }
    }

    function verify($hash = NULL) {

        if ($hash == NULL) {

            redirect('/');
        }

        // create the data object
        $data = new stdClass();

        $result = $this->Users_model->verifyEmailAddress($hash);

        if ($result) {
            $data->success = 'Email Verified Successfully! Please login';
        } else {
            $data->error = 'Sorry Unable to Verify Your Email!';
        }

        //$this->load->view('header');
        $this->load->view('login', $data);
        //$this->load->view('footer');
    }

}
