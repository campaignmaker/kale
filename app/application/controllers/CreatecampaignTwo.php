<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
//use FacebookAds\Api;
//use FacebookAds\Object\AdAccount;
//use FacebookAds\Object\Fields\AdAccountFields;
//use FacebookAds\Object\Campaign;
//use FacebookAds\Object\Fields\CampaignFields;
//use FacebookAds\Object\Values\AdObjectives;
//use FacebookAds\Object\TargetingSearch;
//use FacebookAds\Object\Search\TargetingSearchTypes;
//use FacebookAds\Object\TargetingSpecs;
//use FacebookAds\Object\Fields\TargetingSpecsFields;
//use FacebookAds\Object\AdImage;
//use FacebookAds\Object\Fields\AdImageFields;
//use FacebookAds\Object\AdCreative;
//use FacebookAds\Object\Fields\AdCreativeFields;
//use FacebookAds\Object\Fields\AdPreviewFields;
//use FacebookAds\Object\Values\AdFormats;
//use FacebookAds\Object\Fields\ObjectStory\LinkDataFields;
//use FacebookAds\Object\Fields\ObjectStorySpecFields;
//use FacebookAds\Object\ObjectStory\LinkData;
//use FacebookAds\Object\ObjectStorySpec;
//use FacebookAds\Object\Values\CallToActionTypes;
use FacebookAds\Api;
use FacebookAds\Object\AdImage;
use FacebookAds\Object\Fields\AdImageFields;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\ObjectStorySpecFields;
use FacebookAds\Object\Fields\ObjectStory\LinkDataFields;
use FacebookAds\Object\Fields\ObjectStory\AttachmentDataFields;
use FacebookAds\Object\CustomAudience;
use FacebookAds\Object\Fields\CustomAudienceFields;
use FacebookAds\Object\Values\CustomAudienceTypes;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\TargetingSpecs;
use FacebookAds\Object\Fields\TargetingSpecsFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Fields\AdGroupBidInfoFields;
use FacebookAds\Object\Values\BidTypes;
use FacebookAds\Object\AdGroup;
use FacebookAds\Object\Fields\AdGroupFields;
use FacebookAds\Object\Values\OptimizationGoals;
use FacebookAds\Object\Values\BillingEvents;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;

global $data;
$data = new stdClass();

class CreatecampaignTwo extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public $ad_account_id;

    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        global $fb;
        $this->seo->SetValues('Title', "Create Campaign");
        $this->seo->SetValues('Description', "Create Campaign");
        $this->load->model('Users_model');
        $this->load->model('Campaign_model');
        $this->load->model('Addesign_model');
        /*
         * 
         * Facebook Configuration-------------------- 
         * 
         */
        $fb = new Facebook([
            'app_id' => $this->config->item('facebook_app_id'),
            'app_secret' => $this->config->item('facebook_app_secret'),
            'default_graph_version' => $this->config->item('facebook_graph_version'),
        ]);
        $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
        $this->user_id = $this->session->userdata['logged_in']['id'];

        if (!empty($this->session->userdata['logged_in'])) {
            $app_id = $this->config->item('facebook_app_id');
            $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
            redirect('/');
        }
    }

    public function index() {
        global $data;
        add_css(array('asif_bootstrap.css', 'bootstrap-datepicker3.min.css', 'fileinput.css', 'jquery.tagsinput.css', 'chosen.css', 'jquery.tagit.css',
            'tagit.ui-zendesk.css', 'token-input.css', 'token-input-facebook.css'));
        add_js(array('jquery-ui.min.js', 'jquery.geocomplete.min.js', 'bootstrap-datepicker.min.js', 'fileinput.js', 'chosen.jquery.js',
            'jquery.tagsinput.min.js', 'tag-it.js', 'jquery.tokeninput.js'));
        add_css('asif.css');
        add_js('asif.js');
        //By Nadeem
        add_js(array('canvas-to-blob.min.js', 'fileinput.min.js'));
        add_css('fileinput.min.css');
        
        $ch = curl_init('https://graph.facebook.com/v2.5/act_303596569717773/adsets');
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'access_token' => 'CAARCQQNbaOkBABsz04KPg3rRR3h8aVlKyFwJXwIqakZAVC1PBRa82TvNZAizlgsuJTZAwK3HBEfUn9oZAk2UXzDzl1YLpWMOo0wnHAFDVDIdrfjxjMwtDZAuux5FXVJoU4OLyyo1BujzWyzWnZCJbiLDzZBifG76ZC97arO8ZCXhvcTaZA2Fr4OUjZC5liUWMDQM3RUZBDOYhgZAX9AjR4vUnRUhHpXjLO0TgJuwZD',
                'status' => 'PAUSED',
                'targeting' => '{"geo_locations":{"countries":["US"]}}',
                'campaign_id' => '6035947580810',
                'daily_budget' => 1000,
                'bid_amount' => 2,
                'billing_event' => 'IMPRESSIONS',
                'optimization_goal' => 'REACH',
                'name' => 'Kuti ADSet',
            )
        ));
        $response = curl_exec($ch);
        curl_close($ch);


        print_r($response); // {"id":"6036170795210"}
        echo "test";
        die();
        die("MAR GAYE");
        //$this->getAdpreivewAv();
        //
        //By Nadeem  
        /*
         * 
         * Get F.b Pages of current Usrr
         * 
         */
        //---------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------
        /*
         * 
         * Get User Ad Account---------------------------------------------
         * 
         */
        //echo BASEPATH ;
        $data->getAdAccountes = $this->Users_model->getUserAddAccountId($this->user_id);
//
        $account_id = "act_543816922330204";
        $page_id = "744539042303306";
        $pixel_id = "6046385201480";
        $filePath ="http://localhost/facebookcampaign/assets/campaign/14.png";
        $Img_hash = $this->generateCreativeImg("543816922330204", $filePath);
//die();
// Create Multi Product Ad Creative

//        $child_attachments = array();
//
//        for ($i = 1; $i <= 2; $i++) {
//            $child_attachments[] = array(
//                AttachmentDataFields::LINK => 'http://photos.vigyaan.com/africa' . $i,
//                AttachmentDataFields::IMAGE_HASH => $image[$i]->hash,
//            );
//        }

// The ObjectStorySpec helps bring some order to a complex
// API spec that assists with creating page posts inline.

//        $object_story_spec = array(
//            ObjectStorySpecFields::PAGE_ID => $page_id,
//            ObjectStorySpecFields::LINK_DATA => array(
//                LinkDataFields::MESSAGE => 'Check out some pictures from Africa',
//                LinkDataFields::LINK => 'http://photos.vigyaan.com',
//                LinkDataFields::CAPTION => 'photos.vigyaan.com',
//                LinkDataFields::CHILD_ATTACHMENTS => $child_attachments,
//        ));
//
//        $creative = new AdCreative(null, $account_id);
//
//        $creative->setData(array(
//            AdCreativeFields::NAME => 'Africa Creative 1',
//            AdCreativeFields::OBJECT_STORY_SPEC => $object_story_spec,
//        ));
//
//        $creative->create();
//        $creative_id = $creative->id;
//        echo 'Creative ID: ' . $creative_id . "<br />";

// Create Audience
// Learn more about custom audiences here
// https://developers.facebook.com/docs/marketing-api/custom-audience-website
//        $audience = new CustomAudience(null, $account_id);
//        $audience->setData(array(
//            CustomAudienceFields::NAME => 'f8 demo custom audience',
//            CustomAudienceFields::DESCRIPTION => 'people who visited our website',
//            CustomAudienceFields::SUBTYPE => 'WEBSITE',
//            CustomAudienceFields::RETENTION_DAYS => 30,
//            CustomAudienceFields::PREFILL => true,
//            CustomAudienceFields::RULE => array(
//                'url' => array(
//                    'i_contains' => ''
//                )),
//            'pixel_id' => $pixel_id,
//        ));
//
//        $audience->create();
        $audience_id = "6046386019280";
        echo "Audience ID: " . $audience_id . "<br />";

// Create a campaign
// A campaign can have one or more adsets

        $campaign = new Campaign(null, $account_id);
        $campaign->setData(array(
            CampaignFields::NAME => $this->generateRandomString() . 'Demo Campaign',
            CampaignFields::OBJECTIVE => "LINK_CLICKS",
            CampaignFields::EFFECTIVE_STATUS => Campaign::STATUS_PAUSED,
        ));

        $campaign->validate()->create();
        $campaign_id = $campaign->id;
        echo "Campaign ID: " . $campaign_id . "<br />";
        $inter = '6009248606271,6004139492506,6003334774656,1542695239333507';
        $inter = explode(",", $inter);
        
        $interest = array();
        $index = "id";
        foreach($inter as $val){
           array_push($interest, array('id' => $val));
        }
        $targeting = new TargetingSpecs();
        $targeting->{TargetingSpecsFields::CUSTOM_AUDIENCES} = array('id' => $audience_id);
        $targeting->{TargetingSpecsFields::INTERESTS} = $interest;
//        $targeting->{TargetingSpecsFields::AGE_MAX} = $interest;
//        $targeting->{TargetingSpecsFields::AGE_MIN} = $interest;
//        $targeting->{TargetingSpecsFields::EDUCATION_SCHOOLS} = $interest;
//        $targeting->{TargetingSpecsFields::EXCLUDED_GEO_LOCATIONS} = $interest;
//        $targeting->{TargetingSpecsFields::GENDERS} = $interest;
//        $targeting->{TargetingSpecsFields::RELATIONSHIP_STATUSES} = $interest;
//        $targeting->{TargetingSpecsFields::WORK_EMPLOYERS} = $interest;
//        $targeting->{TargetingSpecsFields::WORK_POSITIONS} = $interest;
        $adset = new AdSet(null, $account_id);
        $adset->setData(array(
            AdSetFields::NAME => 'My Adset' . $this->generateRandomString(),
            AdSetFields::CAMPAIGN_ID => $campaign->id,
            AdSetFields::EFFECTIVE_STATUS => AdSet::STATUS_ACTIVE,
            AdSetFields::DAILY_BUDGET => '150',
            AdSetFields::TARGETING => $targeting,
            AdSetFields::OPTIMIZATION_GOAL => OptimizationGoals::REACH,
            AdSetFields::BILLING_EVENT => BillingEvents::IMPRESSIONS,
            AdSetFields::BID_AMOUNT => 2,
            AdSetFields::START_TIME =>
            (new \DateTime("+1 week"))->format(\DateTime::ISO8601),
            AdSetFields::END_TIME =>
            (new \DateTime("+2 week"))->format(\DateTime::ISO8601),
        ));

        $adset->validate()->create();
        $adset_id = $adset->id;
        echo 'AdSet ID: ' . $adset_id . "<br />";

// Create an ad
// Each ad can only have one creative

        die();

        $ad = new Ad(null, $account_id);
        $ad->setData(array(
            AdFields::CREATIVE =>
            array('creative_id' => $creative->id),
            AdFields::NAME => 'My Ad' . $this->generateRandomString(),
            AdFields::ADSET_ID => $adset->id,
        ));

        $ad->create();
        echo 'Ad ID:' . $ad->id . "<br />";
        die();
        //----------Load View---------------
        loadView('createcampaigntwo', $data);
        //----------------------------------
    }

    public function generateRandomString($length = 10) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    private function getImageAbsloutePath($filePath){
        $url_arr = explode ('/', $filePath);
        $ct = count($url_arr);
        $name = $url_arr[$ct-1];
        $name_div = explode('.', $name);
        $ct_dot = count($name_div);
        $img_type = $name_div[$ct_dot -1];
        return FCPATH."assets/campaign/".$name;
    }
    private function getImgNameFrmUrl($filePath){
        $url_arr = explode ('/', $filePath);
        $ct = count($url_arr);
        $name = $url_arr[$ct-1];
        $name_div = explode('.', $name);
        $ct_dot = count($name_div);
        $img_type = $name_div[$ct_dot -1];
        return $name;
    }
    private function generateCreativeImg($ad_account,$filePath){
            $image = new AdImage(null, "act_".$ad_account);
            $image->{AdImageFields::FILENAME} = $this->getImageAbsloutePath($filePath);
            $image->create();
            return $image->hash;
    }
    private function create_cmp(){
        $campaign = new Campaign(null, $account_id);
        $campaign->setData(array(
            CampaignFields::NAME => $this->generateRandomString() . 'Demo Campaign',
            CampaignFields::OBJECTIVE => "LINK_CLICKS",
            CampaignFields::EFFECTIVE_STATUS => Campaign::STATUS_PAUSED,
        ));

        $campaign->validate()->create();
        $campaign_id = $campaign->id;
    }

}

//end of class
