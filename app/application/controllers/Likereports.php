<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Likereports extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public $access_token;
    public $user_id;
    public $add_account_id;
    public $add_title;
    public $is_ajax;
    public $limit;

    public function __construct() {
        error_reporting(0);
        parent::__construct();
        
        $this->load->model('Users_model');
        $this->seo->SetValues('Title', "Campaigns");
        $this->seo->SetValues('Description', "Reports");
        $this->is_ajax = 0;
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}
        // User subscriptions expire 
        if (userSubcriptionInvalid()) {
            redirect('/updateprofile');
        }
        // User account spend limit reach
        if (userAccountSpendCheck()) {
            redirect('/updateprofile');
        }
        #echo "<PRE>";print_r($this->session);exit;
        if (isset($this->session->userdata['logged_in']) && !empty($this->session->userdata['logged_in'])) {
            $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
            $this->user_id = $this->session->userdata['logged_in']['id'];
            $app_id = $this->config->item('facebook_app_id');
            $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
            redirect('/');
        }
        /*if (!empty($this->session->userdata('sDate')) && !empty($this->session->userdata('eDate'))) {
            $this->sDate = $this->session->userdata['sDate'];
            $this->eDate = $this->session->userdata['eDate'];
        }
        else{
            $this->sDate = date('Y-m-d', strtotime('-6 days'));//2016-06-07
            $this->eDate = date('Y-m-d');
            $this->session->set_userdata('sDate', $this->sDate);
            $this->session->set_userdata('eDate', $this->eDate);
        }*/
        if (!empty($this->session->userdata('dateval'))){
            $this->dateval = $this->session->userdata['dateval'];
        }
        else{
            $this->session->set_userdata('dateval', 'last_7_days');
            $this->dateval = $this->session->userdata['dateval'];
        }
       #echo $this->sDate."<br>".$this->sDate;exit;
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
    }

    public function index($adAccountId = NULL) {
        $limit = $this->dateval;//$this->sDate."#".$this->eDate;
        $data = new stdClass();
        $data->addAccountId = $adAccountId;
        /*$data->mobileData = $this->getCampaignFeeds("act_" .$adAccountId, $limit);
        $data->genderData = $this->getCampaignGendrs("act_" .$adAccountId, $limit);
        $data->spendAgeData = $this->getCampaignSpendChart("act_" .$adAccountId, $limit);
        $data->spendCountryData = $this->getCampaignSpendCountryChart("act_" .$adAccountId, $limit);
        $data->spendTimeData = $this->getCampaignSpendTimeChart("act_" .$adAccountId, $limit);*/
        $data->limit = $limit;
        #echo "<PRE>";print_R($data);exit;
        #echo $adAccountId;exit;
        loadView('likereports', $data);
    }
    
    function getCampaignChart($adAccountId, $limit) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        //$adAccountId = "act_" . $this->input->post('adAccountId');
        //$limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,spend&date_preset=lifetime&summary=['inline_link_clicks','like']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','like']&breakdowns=['age','gender']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','like']&breakdowns=['age','gender']&limit=250&access_token=" . $this->access_token;
        }
        
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $res[] = array(
                    "range" => $ages['age'],
                    "inline_link_clicks" => $ages['inline_link_clicks'],
                    "impressions" => $ages['impressions'],
                    "gender" => $ages['gender']
                );
                $total += $ages['inline_link_clicks'];
            }

            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];
                $resultArray1[$rs['gender']]['dataValue'] = round(($rs['inline_link_clicks'] / $total) * 100);

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }

        foreach ($resultArray as $k => $value) {
            $tempStr1 = '0';
            $tempStr2 = '0';
            $tempStr3 = '0';
            foreach ($value as $k1 => $value1) {
                if ($k1 == 'female') {
                    $tempStr1 = $value1['impressions'];
                }
                if ($k1 == 'male') {
                    $tempStr2 = $value1['impressions'];
                }
                if ($k1 == 'unknown') {
                    $tempStr3 = $value1['impressions'];
                }
            }
            $tempStr .= '{
                            "name1":"' . $k . '",
                            "value1":' . !empty($tempStr1) ? $tempStr1 : 0 . ', 
                            "value2":' . !empty($tempStr2) ? $tempStr2 : 0 . ', 
                            "value3":' . !empty($tempStr3) ? $tempStr3 : 0 . '
                         },';
        }
        $tempStr2 = rtrim($tempStr, ',');
        $tempstr1 = '[
            ' . $tempStr2 . '
            ]';
        echo $tempstr1;exit;
    }
    
    function getCampaignCountryChart($adAccountId, $limit) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        //$adAccountId = "act_" . $this->input->post('adAccountId');
        //$limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,spend&date_preset=lifetime&summary=['inline_link_clicks','like']&sort=['spend']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','like']&breakdowns=['country']&sort=['spend_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','like']&breakdowns=['country']&sort=['spend_descending']&limit=3&access_token=" . $this->access_token;
        }

        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                $tempStr .= '{
                            "name1":"' . $result[0]->Country . '",
                            "value1":' . !empty($ages['impressions']) ? $ages['impressions'] : 0 . '
                         },';
            }
        }

        $tempStr2 = rtrim($tempStr, ',');
        $tempstr1 = '[
            ' . $tempStr2 . '
            ]';
       echo $tempstr1;exit;
    }

    function getCampaignTimeChart($adAccountId, $limit) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        //$adAccountId = "act_" . $this->input->post('adAccountId');
        //$limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,reach,impressions&date_preset=lifetime&summary=['inline_link_clicks','reach','like']&sort=['reach_descending']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=30&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','like']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','like']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $time_4_am = strtotime('04:00:00');
        $time_8_am = strtotime('08:00:00');
        $time_12_pm = strtotime('12:00:00');
        $time_4_pm = strtotime('16:00:00');
        $time_8_pm = strtotime('20:00:00');
        $time_12_am = strtotime('23:59:59');

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $timeArr = explode(' - ', $ages['hourly_stats_aggregated_by_audience_time_zone']);
                $time = strtotime($timeArr[0]);
                if ($time_12_am > $time && $time_4_am >= $time) {
                    $post_12am_to4am[] = $ages;
                } else if ($time_4_am < $time && $time_8_am >= $time) {
                    $post_4am_to8am[] = $ages;
                } else if ($time_8_am < $time && $time_12_pm >= $time) {
                    $post_8am_to12pm[] = $ages;
                } else if ($time_12_pm < $time && $time_4_pm >= $time) {
                    $post_12pm_to4pm[] = $ages;
                } else if ($time_4_pm < $time && $time_8_pm >= $time) {
                    $post_4pm_to8pm[] = $ages;
                } else if ($time_8_pm < $time && $time_12_am >= $time) {
                    $post_8pm_to12am[] = $ages;
                }
            }
        }
        $count_12am_to4am = 0;
        if (is_array($post_12am_to4am)) {
            foreach ($post_12am_to4am as $post) {
                $count_12am_to4am = $count_12am_to4am + $post['impressions'];
            }
        }
        $count_4am_to8am = 0;
        if (is_array($post_4am_to8am)) {
            foreach ($post_4am_to8am as $post) {
                $count_4am_to8am = $count_4am_to8am + $post['impressions'];
            }
        }
        $count_8am_to12pm = 0;
        if (is_array($post_8am_to12pm)) {
            foreach ($post_8am_to12pm as $post) {
                $count_8am_to12pm = $count_8am_to12pm + $post['impressions'];
            }
        }
        $count_12pm_to4pm = 0;
        if (is_array($post_12pm_to4pm)) {
            foreach ($post_12pm_to4pm as $post) {
                $count_12pm_to4pm = $count_12pm_to4pm + $post['impressions'];
            }
        }
        $count_4pm_to8pm = 0;
        if (is_array($post_4pm_to8pm)) {
            foreach ($post_4pm_to8pm as $post) {
                $count_4pm_to8pm = $count_4pm_to8pm + $post['impressions'];
            }
        }
        $count_8pm_to12am = 0;
        if (is_array($post_8pm_to12am)) {
            foreach ($post_8pm_to12am as $post) {
                $count_8pm_to12am = $count_8pm_to12am + $post['impressions'];
            }
        }

        $tempStr .= '{
                        "name1":"0-4",
                        "value1":' . $count_12am_to4am . ',
                        "value2":"red"
                     },{
                        "name1":"5-8",
                        "value1":' . $count_4am_to8am . ',
                        "value2":"red"
                     },{
                        "name1":"9-12",
                        "value1":' . $count_8am_to12pm . ',
                        "value2":"red"
                     },{
                        "name1":"13-16",
                        "value1":' . $count_12pm_to4pm . ',
                        "value2":"red"
                     },{
                        "name1":"17-20",
                        "value1":' . $count_4pm_to8pm . ',
                        "value2":"red"
                     },{
                        "name1":"21-24",
                        "value1":' . $count_8pm_to12am . ',
                        "value2":"red"
                     }';

        $tempstr1 = '[
            ' . $tempStr . '
            ]';
        echo $tempstr1;exit;
    }
    
    function getCampaignFeeds($adAccountId, $limit) {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        //$adAccountId = "act_" . $this->input->post('adAccountId');
        //$limit = $this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&limit=250&access_token=" . $this->access_token;
        }
        
        $res = array();
        $resultArray = array();


        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);

        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
                if ($feeds['placement'] == "desktop_feed") {
                    $desktop = $feeds['inline_link_clicks'];
                    $total +=$feeds['inline_link_clicks'];
                }
                if ($feeds['placement'] == "mobile_feed") {
                    $mobile = $feeds['inline_link_clicks'];
                    $total +=$feeds['inline_link_clicks'];
                }
            }
            if ($desktop > 0) {
                $desktop = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobile = round(($mobile / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => $desktop,
            "mobile" => $mobile
        );
        
        echo json_encode($response);
        //$url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/adsets?name='My Ad Set'&optimization_goal=REACH&campaign_id=CAMPAIGN_ID&access_token=" . $this->access_token;
    }
    
    function getCampaignGendrs($adAccountId, $limit) {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        //$adAccountId = "act_" . $this->input->post('adAccountId');
        //$limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=250&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','reach']&breakdowns=['gender']&limit=250&access_token=" . $this->access_token;
            }
        } else {
            // $params = ".date_preset($type)";

            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/insights?" . $date_preset . "fields=['inline_link_clicks','reach']&breakdowns=['gender']&limit=250&access_token=" . $this->access_token;
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {

                if ($genders['gender'] == "female") {
                    $female = $genders['inline_link_clicks'];
                    $total+=$genders['inline_link_clicks'];
                } else if ($genders['gender'] == "male") {
                    $male = $genders['inline_link_clicks'];
                    $total+=$genders['inline_link_clicks'];
                } else if ($genders['gender'] == "unknown") {
                    $other = $genders['inline_link_clicks'];
                }
                // $total+=$genders['inline_link_clicks'];
            }
            if ($female > 0) {
                $female = ceil(($female / $total) * 100);
            }
            if ($male > 0) {
                $male = floor(($male / $total) * 100);
            }

            //$other = round(($other / $total) * 100);
        }


        $response = array(
            "code" => 100,
            "male" => $male,
            "female" => $female,
            "other" => $other,
        );
        echo json_encode($response);
    }
    
    function getAddSetFeeds() {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $campaignId = $this->input->post('campaignId');
        $limit = $this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&access_token=" . $this->access_token;
        }
        $res = array();
        $resultArray = array();
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);

        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
                if ($feeds['placement'] == "desktop_feed") {
                    $desktop = $feeds['inline_link_clicks'];
                    $total +=$feeds['inline_link_clicks'];
                }
                if ($feeds['placement'] == "mobile_feed") {
                    $mobile = $feeds['inline_link_clicks'];
                    $total +=$feeds['inline_link_clicks'];
                }
            }
            if ($desktop > 0) {
                $desktop = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobile = round(($mobile / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => $desktop,
            "mobile" => $mobile
        );
        echo json_encode($response);
    }
    
    function getAddSetGendrs() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach']&breakdowns=['gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach']&breakdowns=['gender']&access_token=" . $this->access_token;
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {

                if ($genders['gender'] == "female") {
                    $female = $genders['inline_link_clicks'];
                    $total+=$genders['inline_link_clicks'];
                } else if ($genders['gender'] == "male") {
                    $male = $genders['inline_link_clicks'];
                    $total+=$genders['inline_link_clicks'];
                } else if ($genders['gender'] == "unknown") {
                    $other = $genders['inline_link_clicks'];
                }
            }
            if ($female > 0) {
                $female = ceil(($female / $total) * 100);
            }
            if ($male > 0) {
                $male = floor(($male / $total) * 100);
            }

            //$other = round(($other / $total) * 100);
        }


        $response = array(
            "code" => 100,
            "male" => $male,
            "female" => $female,
            "other" => $other,
        );
        echo json_encode($response);
    }
    
    function getAddSetChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['age','gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['age','gender']&access_token=" . $this->access_token;
        }
        $res = array();
        $resultArray = array();
        #echo $url;exit;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $res[] = array(
                    "range" => $ages['age'],
                    "inline_link_clicks" => $ages['inline_link_clicks'],
                    "impressions" => $ages['impressions'],
                    "gender" => $ages['gender']
                );
                $total += $ages['inline_link_clicks'];
            }
            
            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];
                $resultArray1[$rs['gender']]['dataValue'] = round(($rs['inline_link_clicks'] / $total) * 100);

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }
        
        $tempstr1 = '[]';
        if(!empty($resultArray)){
            foreach ($resultArray as $k => $value) {
            $tempStr1 = '0';
            $tempStr2 = '0';
            $tempStr3 = '0';
            foreach ($value as $k1 => $value1) {
                if ($k1 == 'female') {
                    $tempStr1 = $value1['impressions'];
                }
                if ($k1 == 'male') {
                    $tempStr2 = $value1['impressions'];
                }
                if ($k1 == 'unknown') {
                    $tempStr3 = $value1['impressions'];
                }
            }
            $tempStr .= '{
                            "name1":"' . $k . '",
                            "value1":' . $tempStr1 . ', 
                            "value2":' . $tempStr2 . ', 
                            "value3":' . $tempStr3 . '
                         },';
        }
        }
        
        if(!empty($tempStr)){
            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
        }
        echo $tempstr1;
        exit;
    }
    
    function getAddSetCountryChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $tempStr = '';
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                $tempStr .= '{
                            "name1":"' . $result[0]->Country . '",
                            "value1":' . $ages['impressions'] . '
                         },';
            }
        }

        
        if(!empty($tempStr)){
            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
        }
        else{
            $tempstr1 = '[]';
        }
        echo $tempstr1;
        exit;
    }

    function getAddSetTimeChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights/?fields=inline_link_clicks,reach,impressions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&sort=['reach_descending']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=30&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','impressions']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','impressions']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $time_4_am = strtotime('04:00:00');
        $time_8_am = strtotime('08:00:00');
        $time_12_pm = strtotime('12:00:00');
        $time_4_pm = strtotime('16:00:00');
        $time_8_pm = strtotime('20:00:00');
        $time_12_am = strtotime('23:59:59');

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $timeArr = explode(' - ', $ages['hourly_stats_aggregated_by_audience_time_zone']);
                $time = strtotime($timeArr[0]);
                if ($time_12_am > $time && $time_4_am >= $time) {
                    $post_12am_to4am[] = $ages;
                } else if ($time_4_am < $time && $time_8_am >= $time) {
                    $post_4am_to8am[] = $ages;
                } else if ($time_8_am < $time && $time_12_pm >= $time) {
                    $post_8am_to12pm[] = $ages;
                } else if ($time_12_pm < $time && $time_4_pm >= $time) {
                    $post_12pm_to4pm[] = $ages;
                } else if ($time_4_pm < $time && $time_8_pm >= $time) {
                    $post_4pm_to8pm[] = $ages;
                } else if ($time_8_pm < $time && $time_12_am >= $time) {
                    $post_8pm_to12am[] = $ages;
                }
            }
        }
        $count_12am_to4am = 0;
        if (is_array($post_12am_to4am)) {
            foreach ($post_12am_to4am as $post) {
                $count_12am_to4am = $count_12am_to4am + $post['impressions'];
            }
        }
        $count_4am_to8am = 0;
        if (is_array($post_4am_to8am)) {
            foreach ($post_4am_to8am as $post) {
                $count_4am_to8am = $count_4am_to8am + $post['impressions'];
            }
        }
        $count_8am_to12pm = 0;
        if (is_array($post_8am_to12pm)) {
            foreach ($post_8am_to12pm as $post) {
                $count_8am_to12pm = $count_8am_to12pm + $post['impressions'];
            }
        }
        $count_12pm_to4pm = 0;
        if (is_array($post_12pm_to4pm)) {
            foreach ($post_12pm_to4pm as $post) {
                $count_12pm_to4pm = $count_12pm_to4pm + $post['impressions'];
            }
        }
        $count_4pm_to8pm = 0;
        if (is_array($post_4pm_to8pm)) {
            foreach ($post_4pm_to8pm as $post) {
                $count_4pm_to8pm = $count_4pm_to8pm + $post['impressions'];
            }
        }
        $count_8pm_to12am = 0;
        if (is_array($post_8pm_to12am)) {
            foreach ($post_8pm_to12am as $post) {
                $count_8pm_to12am = $count_8pm_to12am + $post['impressions'];
            }
        }

        $tempStr .= '{
                        "name1":"0-4",
                        "value1":' . $count_12am_to4am . ',
                        "value2":"red"
                     },{
                        "name1":"5-8",
                        "value1":' . $count_4am_to8am . ',
                        "value2":"red"
                     },{
                        "name1":"9-12",
                        "value1":' . $count_8am_to12pm . ',
                        "value2":"red"
                     },{
                        "name1":"13-16",
                        "value1":' . $count_12pm_to4pm . ',
                        "value2":"red"
                     },{
                        "name1":"17-20",
                        "value1":' . $count_4pm_to8pm . ',
                        "value2":"red"
                     },{
                        "name1":"21-24",
                        "value1":' . $count_8pm_to12am . ',
                        "value2":"red"
                     }';

        $tempstr1 = '[
            ' . $tempStr . '
            ]';
        echo $tempstr1;
        exit;
    }
    
    function getAdddsFeeds() {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&access_token=" . $this->access_token;
        }
        $res = array();
        $resultArray = array();
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);

        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
                if ($feeds['placement'] == "desktop_feed") {
                    $desktop = $feeds['inline_link_clicks'];
                    $total +=$feeds['inline_link_clicks'];
                }
                if ($feeds['placement'] == "mobile_feed") {
                    $mobile = $feeds['inline_link_clicks'];
                    $total +=$feeds['inline_link_clicks'];
                }
            }
            if ($desktop > 0) {
                $desktop = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobile = round(($mobile / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => $desktop,
            "mobile" => $mobile
        );
        echo json_encode($response);
    }
    
    function getAdddsGendrs() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach']&breakdowns=['gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach']&breakdowns=['gender']&access_token=" . $this->access_token;
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {

                if ($genders['gender'] == "female") {
                    $female = $genders['inline_link_clicks'];
                    $total+=$genders['inline_link_clicks'];
                } else if ($genders['gender'] == "male") {
                    $male = $genders['inline_link_clicks'];
                    $total+=$genders['inline_link_clicks'];
                } else if ($genders['gender'] == "unknown") {
                    $other = $genders['inline_link_clicks'];
                }
            }
            if ($female > 0) {
                $female = ceil(($female / $total) * 100);
            }
            if ($male > 0) {
                $male = floor(($male / $total) * 100);
            }

            //$other = round(($other / $total) * 100);
        }


        $response = array(
            "code" => 100,
            "male" => $male,
            "female" => $female,
            "other" => $other,
        );
        echo json_encode($response);
    }

    function getAdddsChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['age','gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['age','gender']&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $res[] = array(
                    "range" => $ages['age'],
                    "inline_link_clicks" => $ages['inline_link_clicks'],
                    "impressions" => $ages['impressions'],
                    "gender" => $ages['gender']
                );
                $total += $ages['inline_link_clicks'];
            }
            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];
                $resultArray1[$rs['gender']]['dataValue'] = round(($rs['inline_link_clicks'] / $total) * 100);

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }

        $tempstr1 = '[]';
        if(!empty($resultArray)){
            foreach ($resultArray as $k => $value) {
                $tempStr1 = '0';
                $tempStr2 = '0';
                $tempStr3 = '0';
                foreach ($value as $k1 => $value1) {
                    if ($k1 == 'female') {
                        $tempStr1 = $value1['impressions'];
                    }
                    if ($k1 == 'male') {
                        $tempStr2 = $value1['impressions'];
                    }
                    if ($k1 == 'unknown') {
                        $tempStr3 = $value1['impressions'];
                    }
                }
                $tempStr .= '{
                                "name1":"' . $k . '",
                                "value1":' . $tempStr1 . ', 
                                "value2":' . $tempStr2 . ', 
                                "value3":' . $tempStr3 . '
                             },';
            }
        }
        
        if(!empty($tempStr)){
            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
        }
        echo $tempstr1;
        exit;
    }
    
    function getAdddsCountryChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
        }

        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                $tempStr .= '{
                            "name1":"' . $result[0]->Country . '",
                            "value1":' . $ages['impressions'] . '
                         },';
            }
        }

        if(!empty($tempStr)){
            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
        }
        else{
            $tempstr1 = '[]';
        }
        echo $tempstr1;
        exit;
    }

    function getAdddsTimeChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/insights/?fields=inline_link_clicks,reach,impressions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&sort=['reach_descending']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=30&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','impressions']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','impressions']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $time_4_am = strtotime('04:00:00');
        $time_8_am = strtotime('08:00:00');
        $time_12_pm = strtotime('12:00:00');
        $time_4_pm = strtotime('16:00:00');
        $time_8_pm = strtotime('20:00:00');
        $time_12_am = strtotime('23:59:59');

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $timeArr = explode(' - ', $ages['hourly_stats_aggregated_by_audience_time_zone']);
                $time = strtotime($timeArr[0]);
                if ($time_12_am > $time && $time_4_am >= $time) {
                    $post_12am_to4am[] = $ages;
                } else if ($time_4_am < $time && $time_8_am >= $time) {
                    $post_4am_to8am[] = $ages;
                } else if ($time_8_am < $time && $time_12_pm >= $time) {
                    $post_8am_to12pm[] = $ages;
                } else if ($time_12_pm < $time && $time_4_pm >= $time) {
                    $post_12pm_to4pm[] = $ages;
                } else if ($time_4_pm < $time && $time_8_pm >= $time) {
                    $post_4pm_to8pm[] = $ages;
                } else if ($time_8_pm < $time && $time_12_am >= $time) {
                    $post_8pm_to12am[] = $ages;
                }
            }
        }
        $count_12am_to4am = 0;
        if (is_array($post_12am_to4am)) {
            foreach ($post_12am_to4am as $post) {
                $count_12am_to4am = $count_12am_to4am + $post['impressions'];
            }
        }
        $count_4am_to8am = 0;
        if (is_array($post_4am_to8am)) {
            foreach ($post_4am_to8am as $post) {
                $count_4am_to8am = $count_4am_to8am + $post['impressions'];
            }
        }
        $count_8am_to12pm = 0;
        if (is_array($post_8am_to12pm)) {
            foreach ($post_8am_to12pm as $post) {
                $count_8am_to12pm = $count_8am_to12pm + $post['impressions'];
            }
        }
        $count_12pm_to4pm = 0;
        if (is_array($post_12pm_to4pm)) {
            foreach ($post_12pm_to4pm as $post) {
                $count_12pm_to4pm = $count_12pm_to4pm + $post['impressions'];
            }
        }
        $count_4pm_to8pm = 0;
        if (is_array($post_4pm_to8pm)) {
            foreach ($post_4pm_to8pm as $post) {
                $count_4pm_to8pm = $count_4pm_to8pm + $post['impressions'];
            }
        }
        $count_8pm_to12am = 0;
        if (is_array($post_8pm_to12am)) {
            foreach ($post_8pm_to12am as $post) {
                $count_8pm_to12am = $count_8pm_to12am + $post['impressions'];
            }
        }

        $tempStr .= '{
                        "name1":"0-4",
                        "value1":' . $count_12am_to4am . ',
                        "value2":"red"
                     },{
                        "name1":"5-8",
                        "value1":' . $count_4am_to8am . ',
                        "value2":"red"
                     },{
                        "name1":"9-12",
                        "value1":' . $count_8am_to12pm . ',
                        "value2":"red"
                     },{
                        "name1":"13-16",
                        "value1":' . $count_12pm_to4pm . ',
                        "value2":"red"
                     },{
                        "name1":"17-20",
                        "value1":' . $count_4pm_to8pm . ',
                        "value2":"red"
                     },{
                        "name1":"21-24",
                        "value1":' . $count_8pm_to12am . ',
                        "value2":"red"
                     }';

        $tempstr1 = '[
            ' . $tempStr . '
            ]';
        echo $tempstr1;
        exit;
    }
    
    function getAddFeeds() {
        $desktop = 0;
        $mobile = 0;
        $other = 0;
        $total = 0;
        $dataArray = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $addId = $this->input->post('addId');
        $limit = $this->input->post('limit');
        $date_preset = "";

        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];


                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights/?fields=inline_link_clicks,cost_per_inline_link_click,total_actions&date_preset=lifetime&summary=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','total_actions']&breakdowns=['placement']&access_token=" . $this->access_token;
        }
        $res = array();
        $resultArray = array();
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);

        if (isset($response['data'])) {
            foreach ($response['data'] as $feeds) {
                if ($feeds['placement'] == "desktop_feed") {
                    $desktop = $feeds['inline_link_clicks'];
                    $total +=$feeds['inline_link_clicks'];
                }
                if ($feeds['placement'] == "mobile_feed") {
                    $mobile = $feeds['inline_link_clicks'];
                    $total +=$feeds['inline_link_clicks'];
                }
            }
            if ($desktop > 0) {
                $desktop = round(($desktop / $total) * 100);
            }
            if ($mobile > 0) {
                $mobile = round(($mobile / $total) * 100);
            }
        }
        $response = array(
            "code" => 100,
            "desktop" => $desktop,
            "mobile" => $mobile
        );
        echo json_encode($response);
    }
    
    function getAddGendrs() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $addId = $this->input->post('addId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights/?fields=inline_link_clicks,reach&date_preset=lifetime&summary=['inline_link_clicks','reach']&breakdowns=['gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','reach']&breakdowns=['gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','reach']&breakdowns=['gender']&access_token=" . $this->access_token;
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $genders) {

                if ($genders['gender'] == "female") {
                    $female = $genders['inline_link_clicks'];
                    $total+=$genders['inline_link_clicks'];
                } else if ($genders['gender'] == "male") {
                    $male = $genders['inline_link_clicks'];
                    $total+=$genders['inline_link_clicks'];
                } else if ($genders['gender'] == "unknown") {
                    $other = $genders['inline_link_clicks'];
                }
            }
            //echo $female."<br>";
            //echo $male;
            if ($female > 0) {
                $female = ceil(($female / $total) * 100);
            }
            if ($male > 0) {
                $male = floor(($male / $total) * 100);
            }

            //$other = round(($other / $total) * 100);
        }


        $response = array(
            "code" => 100,
            "male" => $male,
            "female" => $female,
            "other" => $other,
        );
        echo json_encode($response);
    }

    function getAddChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $addId = $this->input->post('addId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&breakdowns=['age','gender']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['age','gender']&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['age','gender']&access_token=" . $this->access_token;
        }
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $res[] = array(
                    "range" => $ages['age'],
                    "inline_link_clicks" => $ages['inline_link_clicks'],
                    "impressions" => $ages['impressions'],
                    "gender" => $ages['gender']
                );
                $total += $ages['inline_link_clicks'];
            }
            foreach ($res as $rs) {
                $resultArray1[$rs['gender']]['range'] = $rs['range'];
                $resultArray1[$rs['gender']]['gender'] = $rs['gender'];
                $resultArray1[$rs['gender']]['impressions'] = $rs['impressions'];
                $resultArray1[$rs['gender']]['dataValue'] = round(($rs['inline_link_clicks'] / $total) * 100);

                if (array_key_exists($rs['range'], $resultArray)) {
                    $resultArray[$rs['range']] = $resultArray1;
                } else {
                    $resultArray[$rs['range']] = array();
                    $resultArray[$rs['range']] = $resultArray1;
                }
            }
        }

       $tempstr1 = '[]';
        if(!empty($resultArray)){
            foreach ($resultArray as $k => $value) {
                $tempStr1 = '0';
                $tempStr2 = '0';
                $tempStr3 = '0';
                foreach ($value as $k1 => $value1) {
                    if ($k1 == 'female') {
                        $tempStr1 = $value1['impressions'];
                    }
                    if ($k1 == 'male') {
                        $tempStr2 = $value1['impressions'];
                    }
                    if ($k1 == 'unknown') {
                        $tempStr3 = $value1['impressions'];
                    }
                }
                $tempStr .= '{
                                "name1":"' . $k . '",
                                "value1":' . $tempStr1 . ', 
                                "value2":' . $tempStr2 . ', 
                                "value3":' . $tempStr3 . '
                             },';
            }
        }
        
        if(!empty($tempStr)){
            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
        }
        echo $tempstr1;
        exit;
    }
    
    function getAddCountryChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $addId = $this->input->post('addId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights/?fields=inline_link_clicks,impressions&date_preset=lifetime&summary=['inline_link_clicks','impressions']&sort=['impressions_descending']&breakdowns=['country']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=3&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','impressions']&breakdowns=['country']&sort=['impressions_descending']&limit=3&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $result = $this->Users_model->getCountryName($ages['country']);

                $tempStr .= '{
                            "name1":"' . $result[0]->Country . '",
                            "value1":' . $ages['impressions'] . '
                         },';
            }
        }

        if(!empty($tempStr)){
            $tempStr2 = rtrim($tempStr, ',');
            $tempstr1 = '[
                ' . $tempStr2 . '
                ]';
        }
        else{
            $tempstr1 = '[]';
        }
        echo $tempstr1;
        exit;
    }

    function getAddTimeChart() {
        $female = 0;
        $male = 0;
        $other = 0;
        $total = 0;
        $campaignId = $this->input->post('campaignId');
        $addSetId = $this->input->post('addsetId');
        $addId = $this->input->post('addId');
        $limit = $this->input->post('limit');
        $date_preset = "";
        $dataArray = "";
        if (!empty($limit)) {
            if (strpos($limit, '#') !== false) {
                $dataArray = explode("#", $limit);
                $dataArray[0];
                $dataArray[1];
                $date_preset = "date_preset=" . $limit . "&";

                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights/?fields=inline_link_clicks,reach,impressions&date_preset=lifetime&summary=['inline_link_clicks','reach','impressions']&sort=['reach_descending']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&time_range={'since':'$dataArray[0]','until':'$dataArray[1]'}&limit=30&access_token=" . $this->access_token;
            } else {
                $date_preset = "date_preset=" . $limit . "&";
                $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','impressions']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
            }
        } else {
            $date_preset = "date_preset=last_7_days&";
            $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/insights?" . $date_preset . "fields=['inline_link_clicks','reach','impressions']&breakdowns=['hourly_stats_aggregated_by_audience_time_zone']&sort=['reach_descending']&limit=30&access_token=" . $this->access_token;
        }
        #echo $url;exit;
        $res = array();
        $resultArray = array();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        $time_4_am = strtotime('04:00:00');
        $time_8_am = strtotime('08:00:00');
        $time_12_pm = strtotime('12:00:00');
        $time_4_pm = strtotime('16:00:00');
        $time_8_pm = strtotime('20:00:00');
        $time_12_am = strtotime('23:59:59');

        if (isset($response['data'])) {
            foreach ($response['data'] as $ages) {
                $timeArr = explode(' - ', $ages['hourly_stats_aggregated_by_audience_time_zone']);
                $time = strtotime($timeArr[0]);
                if ($time_12_am > $time && $time_4_am >= $time) {
                    $post_12am_to4am[] = $ages;
                } else if ($time_4_am < $time && $time_8_am >= $time) {
                    $post_4am_to8am[] = $ages;
                } else if ($time_8_am < $time && $time_12_pm >= $time) {
                    $post_8am_to12pm[] = $ages;
                } else if ($time_12_pm < $time && $time_4_pm >= $time) {
                    $post_12pm_to4pm[] = $ages;
                } else if ($time_4_pm < $time && $time_8_pm >= $time) {
                    $post_4pm_to8pm[] = $ages;
                } else if ($time_8_pm < $time && $time_12_am >= $time) {
                    $post_8pm_to12am[] = $ages;
                }
            }
        }
        $count_12am_to4am = 0;
        if (is_array($post_12am_to4am)) {
            foreach ($post_12am_to4am as $post) {
                $count_12am_to4am = $count_12am_to4am + $post['impressions'];
            }
        }
        $count_4am_to8am = 0;
        if (is_array($post_4am_to8am)) {
            foreach ($post_4am_to8am as $post) {
                $count_4am_to8am = $count_4am_to8am + $post['impressions'];
            }
        }
        $count_8am_to12pm = 0;
        if (is_array($post_8am_to12pm)) {
            foreach ($post_8am_to12pm as $post) {
                $count_8am_to12pm = $count_8am_to12pm + $post['impressions'];
            }
        }
        $count_12pm_to4pm = 0;
        if (is_array($post_12pm_to4pm)) {
            foreach ($post_12pm_to4pm as $post) {
                $count_12pm_to4pm = $count_12pm_to4pm + $post['impressions'];
            }
        }
        $count_4pm_to8pm = 0;
        if (is_array($post_4pm_to8pm)) {
            foreach ($post_4pm_to8pm as $post) {
                $count_4pm_to8pm = $count_4pm_to8pm + $post['impressions'];
            }
        }
        $count_8pm_to12am = 0;
        if (is_array($post_8pm_to12am)) {
            foreach ($post_8pm_to12am as $post) {
                $count_8pm_to12am = $count_8pm_to12am + $post['impressions'];
            }
        }

        $tempStr .= '{
                        "name1":"0-4",
                        "value1":' . $count_12am_to4am . ',
                        "value2":"red"
                     },{
                        "name1":"5-8",
                        "value1":' . $count_4am_to8am . ',
                        "value2":"red"
                     },{
                        "name1":"9-12",
                        "value1":' . $count_8am_to12pm . ',
                        "value2":"red"
                     },{
                        "name1":"13-16",
                        "value1":' . $count_12pm_to4pm . ',
                        "value2":"red"
                     },{
                        "name1":"17-20",
                        "value1":' . $count_4pm_to8pm . ',
                        "value2":"red"
                     },{
                        "name1":"21-24",
                        "value1":' . $count_8pm_to12am . ',
                        "value2":"red"
                     }';

        $tempstr1 = '[
            ' . $tempStr . '
            ]';
        echo $tempstr1;
        exit;
    }
}