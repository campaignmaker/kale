<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
use FacebookAds\Api;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdUser;
use Facebook\Facebook;
use Facebook\FacebookApp;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\AdObjectives;
use FacebookAds\Object\Insights;
use FacebookAds\Object\Fields\InsightsFields;
use FacebookAds\Object\Values\InsightsPresets;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Values\InsightsActionBreakdowns;
use FacebookAds\Object\Values\InsightsBreakdowns;
use FacebookAds\Object\Values\InsightsLevels;
use FacebookAds\Object\Values\InsightsIncrements;
use FacebookAds\Object\Values\InsightsOperators;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class Spendreports extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * 
     * @return void
     */
    public $access_token;
    public $user_id;
    public $add_account_id;
    public $add_title;
    public $is_ajax;
    public $limit;

    public function __construct() {
        error_reporting(0);
        parent::__construct();
        
        $this->load->model('Users_model');
        $this->seo->SetValues('Title', "Campaigns");
        $this->seo->SetValues('Description', "Reports");
        $this->is_ajax = 0;
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}
        // User subscriptions expire 
        if (userSubcriptionInvalid()) {
            redirect('/updateprofile');
        }
        // User account spend limit reach
        if (userAccountSpendCheck()) {
            redirect('/updateprofile');
        }
        
        if (isset($this->session->userdata['logged_in']) && !empty($this->session->userdata['logged_in'])) {
            $this->access_token = $this->session->userdata['logged_in']['accesstoken'];
            $this->user_id = $this->session->userdata['logged_in']['id'];
            $this->app_id = $app_id = $this->config->item('facebook_app_id');
            $this->app_secret = $facebook_app_secret = $this->config->item('facebook_app_secret');
            Api::init($app_id, $facebook_app_secret, $this->access_token);
        } else {
            redirect('/');
        }
        
        if (!empty($this->session->userdata('dateval'))){
            $this->dateval = $this->session->userdata['dateval'];
        }
        else{
            $this->session->set_userdata('dateval', 'today');
            $this->dateval = $this->session->userdata['dateval'];
        }
        $this->reportType = base64_decode($this->uri->segment('3'));
        $this->adAccountId = $this->uri->segment('4');
        $this->campaignId = $this->uri->segment('5');
        $this->addSetId = $this->uri->segment('6');
        $this->addId = $this->uri->segment('7');
    }

    public function index() {

        $limit = $this->dateval;//$this->sDate."#".$this->eDate;
        $data = new stdClass();
        $data->addAccountId = $this->adAccountId;
        if(!empty($this->addId)){
            $data->reportType1 = 'adReport';
            $data->reportType = $this->reportType;
            $data->adAccountData = $this->getCurlAddDateRange($limit, $this->campaignId, $this->addSetId, $this->addId);
        }
        else if(!empty($this->addSetId)){
            $data->reportType1 = 'adddsReport';
            $data->reportType = $this->reportType;
            $data->adAccountData = $this->getCurlAdddsDateRange($limit, $this->campaignId, $this->addSetId);
        }
        else if(!empty($this->campaignId)){
            $data->reportType1 = 'addSetReport';
            $data->reportType = $this->reportType;
            $data->adAccountData = $this->getCurlAddSetDateRange($limit, $this->campaignId);
        }
        else{
            $data->reportType1 = 'campaignsReport';
            $data->reportType = $this->reportType;
            $data->adAccountData = $this->getCurlCampaignsDateRange_new($limit);
        }
        $data->limit = $limit;
        $data->adaccounts = $data->adAccountData['response'];
        $data->adaccountsCount = $data->adAccountData['count'];
        $data->error = $data->adAccountData['error'];
        $data->success = $data->adAccountData['success'];
        $data->campaignId = $this->campaignId;
        $data->addsetId = $this->addSetId;
        $data->addId = $this->addId;
        #echo "<PRE>";print_R($data);exit;
        loadView('spendreports', $data);
    }

    function getCurlAddDateRange($limit, $campaignId, $addSetId, $addId) {

        $cost_per_unique_click = 0;
        $total_cost_per_inline_click = 0;
        $impressions = 0;
        $total_inline_clicks = 0;
        $reach = 0;
        $unique_clicks = 0;
        $unique_ctr = 0;
        $spent = 0;
        $actions = 0;
        $cost_per_total_action = 0;
        $cost_per_conversion = 0;
        $inline_clicks = 0;
        $cost_per_inline_clicks = 0;
        $total = 0;
        $conversion = 0;

        $returnRow = array();
        $resultArray = array();

        $dataArray = explode("#", $limit);
        $adFields = $this->getAdFields(1, $ajax);
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId/?fields=configured_status,effective_status,creative,insights.date_preset(".$limit."){clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}" . "&access_token=" . $this->access_token;
        #echo $url;exit;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            #echo "<PRE>";print_R($response);exit;
            // $this->getPrintResult($response);
            if (isset($response)) {
                $addSetDataArray = $this->getFinalResult1($response, $campaignId, $ajax, $addId);
                if ($addSetDataArray) {
                     $url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addId?fields=insights.date_preset(".$limit.")%7Bfrequency%2Cdate_start%7D&access_token=" . $this->access_token;

                    $ch = curl_init($url1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response1 = json_decode($result, true);
                    $addSetDataArray['campaign_header']['newFrequ'] = $response1['insights']['data'][0]['frequency'];

                    $accountArr = $this->Users_model->getUserAddAccountName($this->adAccountId);
                    $addSetDataArray['campaign_header']['accountName'] = $accountArr[0]->add_title;

                    $resultArray = array(
                        "count" => 1,
                        "response" => $addSetDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => "No record found.",
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => "No record within date.",
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getFinalResult1($addSetDataArray, $campaignId, $ajax, $addId) {

        $cost_per_unique_click = 0;
        $total_cost_per_inline_click = 0;
        $impressions = 0;
        $total_inline_clicks = 0;
        $reach = 0;
        $unique_clicks = 0;
        $unique_ctr = 0;
        $spent = 0;
        $actions = 0;
        $cost_per_total_action = 0;
        $total1 = 0;
        $total2 = 0;
        $total3 = 0;

        $inline_clicks = 0;
        $cost_per_inline_clicks = 0;
        $desktop_url = "";
        $mobile_url = "";
        $total = 0;
        $cost_per_conversion = 0;
        $conversion = 0;


        $countries = array();
        $intersts = array();
        $language = array();
        $gender = array();
        $behavoiurs = array();

        $objective = $this->getCampaignObjective($campaignId);
        // $this->getPrintResult($addSetDataArray);
        
        $returnRow = array();
        //$this->getPrintResult($desktop_url);
        if (isset($addSetDataArray['insights'])) {
            $inline_post_engagement1 = $like1 = '0';
            $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';
            foreach ($addSetDataArray['insights']['data'] as $row) {
                $total++;

                if (isset($row)) {
                    if (isset($row['actions'])) {
                        foreach ($row['actions'] as $rs) {
                            if ($row['clicks']) {
                                $inline_clicks = $row['clicks'];
                                $cost_per_inline_clicks = $row['spend'] / $row['clicks'];
                            }
                            if ($rs['action_type'] == "offsite_conversion") {
                                 $total3++;
                                $conversion = $rs['value'];
                                //$cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];
                            } else {
                                $conversion = 0;
                                //$cost_per_conversion = 0;
                            }
                            if($rs['action_type'] == 'page_engagement'){
                                if($rs['value']){
                                    $total1++;
                                    $inline_post_engagement1 = $rs['value'];
                                }
                                else{
                                    $inline_post_engagement1 = 0;
                                }
                            }
                            if($rs['action_type'] == 'like'){
                                if($rs['value']){
                                    $total2++;
                                    $like1 = $rs['value'];
                                }
                                else{
                                    $like1 = 0;
                                }
                            }


                        }
                        foreach ($row['cost_per_action_type'] as $rs) {
                            if($rs['action_type'] == 'page_engagement'){
                                if($rs['value']){
                                    $cost_per_inline_post_engagement1 = $rs['value'];
                                }
                                else{
                                    $cost_per_inline_post_engagement1 = 0;
                                }
                            }
                            if($rs['action_type'] == 'like'){
                                if($rs['value']){
                                    $cost_per_like1 = $rs['value'];
                                }
                                else{
                                    $cost_per_like1 = 0;
                                }
                            }
                            if ($rs['action_type'] == "offsite_conversion") {
                                $cost_per_conversion = $rs['value'];
                            } else {
                                $cost_per_conversion = 0;
                            }
                        }
                        $total_inline_clicks+=$inline_clicks;
                        $total_cost_per_inline_click+=$cost_per_inline_clicks;
                        if ($ajax != 1) {
                            
                        }

                        $returnRow['add'][] = array(
                            "add_objective" => $objective,
                            "configured_status" => $addSetDataArray['configured_status'],
                            "effective_status" => $addSetDataArray['effective_status'],
                        );
                        $cost_per_unique_click += $row['cost_per_unique_click'];
                        $impressions += $row['impressions'];
                        $cpm += $row['cpm'];
                        $reach += $row['reach'];
                        $unique_clicks += $row['unique_clicks'];
                        $unique_ctr += $row['ctr'];
                        $spent += $row['spend'];
                        $actions += $conversion;
                        $cost_per_total_action += $cost_per_conversion;
                        $inline_post_engagement += $inline_post_engagement1;
                        $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;

                        $page_like += $like1;
                        $cost_per_like += $cost_per_like1;
                
                        //Reset   
                        $inline_clicks = '--';
                        $cost_per_inline_clicks = '--';
                    }
                } else {

                    $returnRow['addsets'][] = array(
                        "add_objective" => $objective
                    );

                    $cost_per_unique_click += 0;
                    $impressions += 0;
                    $cpm += 0;
                    $reach += 0;
                    $unique_clicks += 0;
                    $unique_ctr += 0;
                    $spent += 0;
                    $actions += 0;
                    $cost_per_total_action += 0;

                    //Reset   
                    $inline_clicks = '--';
                    $cost_per_inline_clicks = '--';
                }
            }
        } else {

            $returnRow['addsets'][] = array(
                "add_objective" => $objective
            );

            $cost_per_unique_click += 0;
            $impressions += 0;
            $cpm += 0;
            $reach += 0;
            $unique_clicks += 0;
            $unique_ctr += 0;
            $spent += 0;
            $actions += 0;
            $cost_per_total_action += 0;
            $inline_post_engagement += 0;
            $cost_per_inline_post_engagement += 0;

            $page_like += 0;
            $cost_per_like += 0;

            //Reset   
            $inline_clicks = '--';
            $cost_per_inline_clicks = '--';
        }

        if ($ajax != 1) {

            if (isset($addSetDataArray['targetingsentencelines'])) {

                foreach ($addSetDataArray['targetingsentencelines']['targetingsentencelines'] as $list) {
                    /* Countries */
                    if ($list['content'] == "Location - Living In:") {

                        foreach ($list['children'] as $ctry) {
                            $countries[] = $ctry;
                        }
                    }
                    /* Gender */
                    if ($list['content'] == "Gender:") {
                        foreach ($list['children'] as $genders) {

                            $gender[] = $genders;
                        }
                    }
                    /* Intersts */
                    if ($list['content'] == "Interests:") {

                        foreach ($list['children'] as $interst) {

                            $intersts[] = $interst;
                        }
                    }
                    /* Behavoiurs */
                    if ($list['content'] == "Behaviors:") {
                        //$this->getPrintResult($list['children']);
                        foreach ($list['children'] as $behaviou) {
                            $behavoiurs[] = $behaviou;
                        }
                    }
                    /* Behavoiurs */
                    if ($list['content'] == "Language:") {
                        foreach ($list['children'] as $lang) {
                            $language[] = $lang;
                        }
                    }
                }
            }
            if (count($countries) > 0) {
                $countries = array_unique($countries);
                $countries = implode(', ', $countries);
            } else {
                $country = "";
            }
            if (count($intersts) > 0) {
                $intersts = array_unique($intersts);
                $intersts = implode(', ', $intersts);
            } else {
                $intersts = "";
            }
            if (count($language) > 0) {
                $language = array_unique($language);
                $language = implode(', ', $language);
            } else {
                $language = "";
            }
            if (count($gender) > 0) {

                $gender = array_unique($gender);
                $gender = implode(', ', $gender);
            } else {
                $gender = "";
            }
            if (count($behavoiurs) > 0) {
                $behavoiurs = array_unique($behavoiurs);
                $behavoiurs = implode(', ', $behavoiurs);
            } else {
                $behavoiurs = "";
            }
        }
        if ($unique_ctr > 0) {
            $unique_ctr = ($unique_ctr / $total);
        }
        if ($total_cost_per_inline_click > 0) {
            //echo $total_cost_per_inline_click."<br>";
            //echo $total;exit;
            $total_cost_per_inline_click = ($total_cost_per_inline_click / $total);
        }
        if ($cost_per_total_action > 0) {
            $cost_per_total_action = ($cost_per_total_action / $total2);
        }
        if ($cost_per_inline_post_engagement > 0) {
            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);
        }
        if ($cost_per_like > 0) {
            $cost_per_like = ($cost_per_like / $total2);
        }
        $returnRow['campaign_header'] = array(
            "cost_per_unique_click" => $cost_per_unique_click,
            "cost_per_inline_link_click" => $total_cost_per_inline_click,
            "impressions" => $impressions,
            "cpm" => $cpm,
            "inline_link_clicks" => $total_inline_clicks,
            "reach" => $reach,
            "unique_clicks" => $unique_clicks,
            "unique_ctr" => $unique_ctr,
            "spent" => $spent,
            "actions" => $actions,
            "cost_per_total_action" => $cost_per_total_action,
            "desktop_view" => $desktop_url,
            "mobile_view" => $mobile_url,
            "configured_status" => $addSetDataArray['configured_status'],
            "effective_status" => $addSetDataArray['effective_status'],
            "country" => $countries,
            "interests" => $intersts,
            "language" => $language,
            "gender" => $gender,
            "behaviours" => $behavoiurs,
            "inline_post_engagement" => $inline_post_engagement,
            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,
            "page_like" => $page_like,
            "cost_per_like" => $cost_per_like,
        );
        // $this->getPrintResult($returnRow);
        return $returnRow;
    }

    function getAdFields($curl, $ajax) {
        if ($curl == 1 && $ajax) {
            $fields = ",id,account_id,name,bid_amount,campaign_id,created_time,configured_status,effective_status";
        } else if ($curl == 1) {
            $fields = ",id,account_id,name,bid_amount,campaign_id,created_time,configured_status,effective_status,creative,targetingsentencelines{params,targetingsentencelines},targeting";
        } else {
            $fields = array(
                AdFields::ID,
                AdFields::ACCOUNT_ID,
                AdFields::BID_AMOUNT,
                AdFields::ADSET_ID,
                AdFields::CAMPAIGN_ID,
                AdFields::CREATED_TIME,
                AdFields::AD_REVIEW_FEEDBACK,
                AdFields::NAME,
                AdFields::TRACKING_SPECS,
                AdFields::UPDATED_TIME,
                AdFields::CREATIVE,
                AdFields::ADLABELS,
            );
        }
        return $fields;
    }

    function getCurlAdddsDateRange($limit, $campaignId, $addSetId) {
        $returnRow = array();
        $resultArray = array();

        $dataArray = explode("#", $limit);
        $addSetFields = $this->getAdsFields(1);
        
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId/ads?fields=".$addSetFields.",insights.date_preset(".$limit."){clicks,impressions,reach,actions,date_start,campaign_name,ctr,cpm,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&access_token=" . $this->access_token;
        #echo $url;exit;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            if (isset($response['data'])) {
                $campaignDataArray = $this->getFinalResult2($response['data']);
                if ($campaignDataArray) {

                     $url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$addSetId?fields=insights.date_preset(".$limit.")%7Bfrequency%2Cdate_start%7D&access_token=" . $this->access_token;

                    $ch = curl_init($url1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response1 = json_decode($result, true);
                    $campaignDataArray['campaign_header']['newFrequ'] = $response1['insights']['data'][0]['frequency'];

                    $accountArr = $this->Users_model->getUserAddAccountName($this->adAccountId);
                    $campaignDataArray['campaign_header']['accountName'] = $accountArr[0]->add_title;

                    $resultArray = array(
                        "count" => 1,
                        "response" => $campaignDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $error,
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => $error,
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getFinalResult2($addSetDataArray, $campaignId, $addSetId) {
        $activeAddSet = 0;
        $inActiveAddSet = 0;
        $cost_per_unique_click = 0;
        $total_cost_per_inline_click = 0;
        $impressions = 0;
        $total_inline_clicks = 0;
        $reach = 0;
        $unique_clicks = 0;
        $unique_ctr = 0;
        $spent = 0;
        $actions = 0;
        $cost_per_total_action = 0;

        $inline_clicks = 0;
        $cost_per_inline_clicks = 0;
        $pendingAddSet = 0;
        $disapprovedAddSet = 0;
        $total = 0;
        $cost_per_conversion = 0;
        $conversion = 0;
        $countries = array();
        $intersts = array();
        $language = array();
        $gender = array('Male', 'female');
        $age = array();
        $behaviour = array();
        $customAudience = array();
        $demographic = array();
        $objective = $this->getCampaignObjective($campaignId);
        $addSetStatus = $this->getAddsetObjective($addSetId);
        $total1 = 0;
        $total2 = 0;
        $returnRow = array();
        // $this->getPrintResult($addSetDataArray);
        foreach ($addSetDataArray as $row) {
            $row['objective'] = $objective;
            if ($row['effective_status'] == "ACTIVE") {
                $activeAddSet++;
                $status = "Active";
            } else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {
                $inActiveAddSet++;
                $status = "Inactive";
            } else if ($row['effective_status'] == "PENDING_REVIEW") {
                $pendingAddSet++;
                $status = "In Review";
            } else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {
                $disapprovedAddSet++;
                $status = "Denied";
            }
            $total++;


            if (isset($row['insights'])) {
                $inline_post_engagement1 = $like1 = '0';
                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';
                // echo $row['name']."<br>";
                if (isset($row['insights']['data'][0]['actions'])) {
                    foreach ($row['insights']['data'][0]['actions'] as $rs) {

                        if ($row['insights']['data'][0]['clicks']) {
                            $inline_clicks = $row['insights']['data'][0]['clicks'];
                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];
                        }

                        if ($rs['action_type'] == "offsite_conversion") {
                            $conversion = $rs['value'];
                            $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];
                        } else {
                            $conversion = 0;
                            $cost_per_conversion = 0;
                        }
                        if($rs['action_type'] == 'page_engagement'){
                            if($rs['value']){
                                $total1++;
                                $inline_post_engagement1 = $rs['value'];
                            }
                            else{
                                $inline_post_engagement1 = 0;
                            }
                        }
                        if($rs['action_type'] == 'like'){
                            if($rs['value']){
                                $total2++;
                                $like1 = $rs['value'];
                            }
                            else{
                                $like1 = 0;
                            }
                        }
                    }
                    foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {
                        if($rs['action_type'] == 'page_engagement'){
                            if($rs['value']){
                                $cost_per_inline_post_engagement1 = $rs['value'];
                            }
                            else{
                                $cost_per_inline_post_engagement1 = 0;
                            }
                        }
                        if($rs['action_type'] == 'like'){
                            if($rs['value']){
                                $cost_per_like1 = $rs['value'];
                            }
                            else{
                                $cost_per_like1 = 0;
                            }
                        }
                    }
                    
                }
                $total_inline_clicks+=$inline_clicks;
                $total_cost_per_inline_click+=$cost_per_inline_clicks;
                
               
                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];
                $impressions += $row['insights']['data'][0]['impressions'];
                $cpm += $row['insights']['data'][0]['cpm'];
                $reach += $row['insights']['data'][0]['reach'];
                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];
                $unique_ctr += $row['insights']['data'][0]['ctr'];
                $spent += $row['insights']['data'][0]['spend'];
                $actions += $conversion;
                $cost_per_total_action += $cost_per_conversion;
                $inline_post_engagement += $inline_post_engagement1;
                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;

                $page_like += $like1;
                $cost_per_like += $cost_per_like1;
                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            } else {
                $cost_per_unique_click += 0;
                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];
                $impressions += 0;
                $cpm += 0;
                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];
                $reach += 0;
                $unique_clicks += 0;
                $unique_ctr += 0;
                $spent += 0;
                $actions += 0;
                $cost_per_total_action += 0;
                $inline_post_engagement += 0;
                $cost_per_inline_post_engagement += 0;

                $page_like += 0;
                $cost_per_like += 0;
                
                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            }
            // $this->getPrintResult($row['targetingsentencelines']['targetingsentencelines']);
            if (isset($row['targetingsentencelines'])) {

                foreach ($row['targetingsentencelines']['targetingsentencelines'] as $list) {
                    /* Countries */
                    if ($list['content'] == "Location - Living In:") {

                        foreach ($list['children'] as $ctry) {
                            $countries[] = $ctry;
                        }
                    }
                    /* Gender */
                    if ($list['content'] == "Gender:") {
                        foreach ($list['children'] as $genders) {
                            $gender[] = $genders;
                        }
                    }
                    /* Intersts */
                    if ($list['content'] == "Interests:") {

                        foreach ($list['children'] as $interst) {

                            $intersts[] = $interst;
                        }
                    }
                    /* Behavoiurs */
                    if ($list['content'] == "Behaviors:") {
                        //$this->getPrintResult($list['children']);
                        foreach ($list['children'] as $behaviours) {
                            $behaviour[] = $behaviours;
                        }
                    }
                    /* Behavoiurs */
                    if ($list['content'] == "Language:") {
                        foreach ($list['children'] as $lang) {
                            $language[] = $lang;
                        }
                    }
                    /* Custom Audience: */
                    if ($list['content'] == "Custom Audience:") {
                        foreach ($list['children'] as $customAud) {
                            $customAudience[] = $customAud;
                        }
                    }
                    /* Custom Audience: */
                    /* demographic  */
                    if ($list['content'] == "Parents:") {
                        foreach ($list['children'] as $demogr) {
                            $demographic[] = $demogr;
                        }
                    }
                    /* demographic  */
                    /* age  */
                    if ($list['content'] == "Age:") {
                        foreach ($list['children'] as $age1) {
                            $age[] = $age1;
                        }
                    }
                    /* age  */
                }
            }
        }
        if (count($countries) > 0) {
            $countries = array_unique($countries);
            $countries = implode(', ', $countries);
        } else {
            $country = "";
        }
        if (count($intersts) > 0) {
            $intersts = array_unique($intersts);
            $intersts = implode(', ', $intersts);
        } else {
            $intersts = "";
        }
        if (count($language) > 0) {
            $language = array_unique($language);
            $language = implode(', ', $language);
        } else {
            $language = "";
        }

        if (count($gender) > 0) {

            $gender = array_unique($gender);
            $gender = implode(', ', $gender);
        } else {
            $gender = "";
        }
        if (count($age) > 0) {

            $age = array_unique($age);
            $age = implode(', ', $age);
        } else {
            $age = "";
        }
        if (count($behaviour) > 0) {
            $behaviour = array_unique($behaviour);
            $behaviour = implode(', ', $behaviour);
        } else {
            $behaviour = "";
        }
        if (count($customAudience) > 0) {
            $customAudience = array_unique($customAudience);
            $customAudience = implode(', ', $customAudience);
        } else {
            $customAudience = "";
        }
        if (count($demographic) > 0) {
            $demographic = array_unique($demographic);
            $demographic = implode(', ', $demographic);
        } else {
            $demographic = "";
        }

        if ($unique_ctr > 0) {
            $unique_ctr = ($unique_ctr / $total);
        }
        if ($cost_per_total_action > 0) {
            $cost_per_total_action = ($spent / $actions);
        }
        if ($cost_per_inline_post_engagement > 0) {
            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);
        }
        if ($cost_per_like > 0) {
            $cost_per_like = ($cost_per_like / $total2);
        }
        $returnRow['campaign_header'] = array(
            "adds_active" => $activeAddSet,
            "adds_inactive" => $inActiveAddSet,
            "adds_pending" => $pendingAddSet,
            "adds_disapproved" => $disapprovedAddSet,
            "cost_per_unique_click" => $cost_per_unique_click,
            "cost_per_inline_link_click" => $total_cost_per_inline_click,
            "impressions" => $impressions,
            "impressions" => $cpm,
            "inline_link_clicks" => $total_inline_clicks,
            "reach" => $reach,
            "unique_clicks" => $unique_clicks,
            "unique_ctr" => $unique_ctr,
            "spent" => $spent,
            "actions" => $actions,
            "cost_per_total_action" => $cost_per_total_action,
            "country" => $countries,
            "interests" => $intersts,
            "language" => $language,
            "gender" => $gender,
            "customAudience" => $customAudience,
            "demographic" => $demographic,
            "age" => $age,
            "behaviours" => $behaviour,
            "addSetStatus" => $addSetStatus,
            "inline_post_engagement" => $inline_post_engagement,
            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,
            "page_like" => $page_like,
            "cost_per_like" => $cost_per_like,
        );
        //$this->getPrintResult($returnRow);
        return $returnRow;
    }

    function getAddsetObjective($campaignId) {
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/?fields=effective_status,configured_status&access_token=" . $this->access_token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //   $this->getPrintResult($response);
        return $response['configured_status'];
    }

    function getAdsFields($curl) {
        if ($curl == 1) {
            $fields = "id,account_id,name,bid_amount,budget_remaining,campaign_id,created_time,daily_budget,end_time,lifetime_budget,promoted_object,product_ad_behaviour,billing_event,configured_status,effective_status,targetingsentencelines{params,targetingsentencelines},targeting";
        } else {
            $fields = array(
                AdFields::ID,
                AdFields::ACCOUNT_ID,
                AdFields::BID_AMOUNT,
                AdFields::ADSET_ID,
                AdFields::CAMPAIGN_ID,
                // AdFields::conversion_specs,
                AdFields::CREATED_TIME,
                AdFields::AD_REVIEW_FEEDBACK,
                AdFields::NAME,
                AdFields::RTB_FLAG,
                AdFields::TARGETING,
                AdFields::TRACKING_SPECS,
                AdFields::UPDATED_TIME,
                // AdFields::VIEW_TAGS,
                AdFields::CREATIVE,
                AdFields::SOCIAL_PREFS,
                AdFields::FAILED_DELIVERY_CHECKS,
                AdFields::REDOWNLOAD,
                AdFields::ADLABELS,
                AdFields::ENGAGEMENT_AUDIENCE,
                AdFields::EXECUTION_OPTIONS
            );
        }
        return $fields;
    }

    function getCurlAddSetDateRange($limit, $campaignId) {
        $returnRow = array();
        $resultArray = array();

        $dataArray = explode("#", $limit);
        $addSetFields = $this->getAdsetFields(1);
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/adsets?fields=".$addSetFields.",insights.date_preset(".$limit."){clicks,impressions,reach,actions,date_start,campaign_name,ctr,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective}&access_token=" . $this->access_token;
        #echo $url;exit;
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            if (isset($response['data'])) {
                $campaignDataArray = $this->getFinalResult3($response['data'], $campaignId);
                if ($campaignDataArray) {

                    $url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId?fields=insights.date_preset(".$limit.")%7Bfrequency%2Cdate_start%7D&access_token=" . $this->access_token;

                    $ch = curl_init($url1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response1 = json_decode($result, true);
                    $campaignDataArray['campaign_header']['newFrequ'] = $response1['insights']['data'][0]['frequency'];

                    $accountArr = $this->Users_model->getUserAddAccountName($this->adAccountId);
                    $campaignDataArray['campaign_header']['accountName'] = $accountArr[0]->add_title;

                    $resultArray = array(
                        "count" => 1,
                        "response" => $campaignDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $error,
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => $error,
                    "success" => ""
                );
            }
        } 
        catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        
        return $resultArray;
    }

    function getAdsetFields($curl) {
        if ($curl == 1) {
            $fields = "id,account_id,name,bid_amount,budget_remaining,campaign_id,created_time,daily_budget,end_time,lifetime_budget,start_time,promoted_object,product_ad_behaviour,billing_event,objective,configured_status,effective_status,targetingsentencelines{params,targetingsentencelines},targeting";
        } else {
            $fields = array(
                AdSetFields::ID,
                AdSetFields::ACCOUNT_ID,
                AdSetFields::ADSET_SCHEDULE,
                AdSetFields::BID_AMOUNT,
                AdSetFields::BILLING_EVENT,
                AdSetFields::BUDGET_REMAINING,
                AdSetFields::CAMPAIGN_ID,
                AdSetFields::CREATED_TIME,
                AdSetFields::CREATIVE_SEQUENCE,
                AdSetFields::DAILY_BUDGET,
                AdSetFields::END_TIME,
                AdSetFields::IS_AUTOBID,
                AdSetFields::LIFETIME_BUDGET,
                AdSetFields::LIFETIME_IMPS,
                AdSetFields::NAME,
                AdSetFields::OPTIMIZATION_GOAL,
                AdSetFields::PACING_TYPE,
                AdSetFields::RF_PREDICTION_ID,
                AdSetFields::START_TIME,
                AdSetFields::UPDATED_TIME,
                AdSetFields::TARGETING,
                AdSetFields::PROMOTED_OBJECT,
                AdSetFields::ADLABELS,
                AdSetFields::PRODUCT_AD_BEHAVIOR,
            );
        }
        return $fields;
    }

    function getFinalResult3($addSetDataArray, $campaignId) {
        $activeAddSet = 0;
        $inActiveAddSet = 0;
        $cost_per_unique_click = 0;
        $total_cost_per_inline_click = 0;
        $impressions = 0;
        $total_inline_clicks = 0;
        $reach = 0;
        $unique_clicks = 0;
        $unique_ctr = 0;
        $spent = 0;
        $actions = 0;
        $cost_per_total_action = 0;

        $inline_clicks = 0;
        $cost_per_inline_clicks = 0;
        $pendingAddSet = 0;
        $disapprovedAddSet = 0;
        $total = 0;
        $cost_per_conversion = 0;
        $conversion = 0;
        $boy = 0;
        $girl = 0;
        $objectiveResponse = $this->getCampaignObjective($campaignId);
        $objective = $objectiveResponse['objective'];
        $campaignStatus = $objectiveResponse['effective_status'];
        // $gendersAvg = $this->getCampaignGendrs($campaignId);
        $total1 = 0;
        $total2 = 0;

        $returnRow = array();

        //  $this->getPrintResult($addSetDataArray);
        foreach ($addSetDataArray as $row) {
            $row['objective'] = $objective;
            if ($row['effective_status'] == "ACTIVE" || $row['effective_status'] == "PREAPPROVED") {
                $activeAddSet++;
                $status = "Active";
            } 
            else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {
                $inActiveAddSet++;
                $status = "Inactive";
            } 
            else if ($row['effective_status'] == "PENDING_REVIEW") {
                $pendingAddSet++;
                $status = "In Review";
            } 
            else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {
                $disapprovedAddSet++;
                $status = "Denied";
            }
            $total++;

            if (isset($row['insights'])) {
                $inline_post_engagement1 = $like1 = '0';
                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';
                if (isset($row['insights']['data'][0])) {
                    //echo $row['name']."<br>";
                    if(!empty($row['insights']['data'][0]['actions'])){
                        foreach ($row['insights']['data'][0]['actions'] as $rs) {

                            if ($row['insights']['data'][0]['clicks']) {
                                $inline_clicks = $row['insights']['data'][0]['clicks'];
                                $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];
                            }

                            if ($rs['action_type'] == "offsite_conversion") {
                                $conversion = $rs['value'];
                                $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];
                            } else {
                                $conversion = 0;
                                $cost_per_conversion = 0;
                            }
                            if($rs['action_type'] == 'page_engagement'){
                                if($rs['value']){
                                    $total1++;
                                    $inline_post_engagement1 = $rs['value'];
                                }
                                else{
                                    $inline_post_engagement1 = 0;
                                }
                            }
                            if($rs['action_type'] == 'like'){
                                if($rs['value']){
                                    $total2++;
                                    $like1 = $rs['value'];
                                }
                                else{
                                    $like1 = 0;
                                }
                            }
                        }
                    }
                    else{
                        $conversion = 0;
                        $cost_per_conversion = 0;
                        $like1 = 0;
                    }
                    if(!empty($row['insights']['data'][0]['cost_per_action_type'])){
                        foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {
                            if($rs['action_type'] == 'page_engagement'){
                                if($rs['value']){
                                    $cost_per_inline_post_engagement1 = $rs['value'];
                                }
                                else{
                                    $cost_per_inline_post_engagement1 = 0;
                                }
                            }
                            if($rs['action_type'] == 'like'){
                                if($rs['value']){
                                    $cost_per_like1 = $rs['value'];
                                }
                                else{
                                    $cost_per_like1 = 0;
                                }
                            }
                        }
                    }
                    else{
                        $cost_per_like1 = 0;
                        $cost_per_inline_post_engagement1 = 0;
                    }
                    
                }
                $total_inline_clicks+=$inline_clicks;
                $total_cost_per_inline_click+=$cost_per_inline_clicks;
                
                
                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];
                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];
                $impressions += $row['insights']['data'][0]['impressions'];
                $cpm += $row['insights']['data'][0]['cpm'];
                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];
                $reach += $row['insights']['data'][0]['reach'];
                $frequency += $row['insights']['data'][0]['frequency'];
                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];
                $unique_ctr += $row['insights']['data'][0]['ctr'];
                $spent += $row['insights']['data'][0]['spend'];
                $actions += $conversion;
                $cost_per_total_action += $cost_per_conversion;
                $inline_post_engagement += $inline_post_engagement1;
                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;

                $page_like += $like1;
                $cost_per_like += $cost_per_like1;

                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            } else {
                
                $cost_per_unique_click += 0;
                // $cost_per_inline_link_click += $row['insights']['data'][0]['cost_per_inline_link_click'];
                $impressions += 0;
                $cpm += 0;
                // $inline_link_clicks += $row['insights']['data'][0]['inline_link_clicks'];
                $reach += 0;
                $frequency += 0;
                $unique_clicks += 0;
                $unique_ctr += 0;
                $spent += 0;
                $actions += 0;
                $cost_per_total_action += 0;
                $inline_post_engagement += 0;
                $cost_per_inline_post_engagement += 0;

                $page_like += 0;
                $cost_per_like += 0;
                
                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            }
        }

        if ($unique_ctr > 0) {
            $unique_ctr = ($unique_ctr / $total);
        }
        if ($total_cost_per_inline_click > 0) {
            //echo $total_cost_per_inline_click."<br>";
            //echo $total;exit;
            $total_cost_per_inline_click = ($total_cost_per_inline_click / $total);
        }
        if ($cost_per_total_action > 0) {
            $cost_per_total_action = ($spent / $actions);
        }
        if ($cost_per_inline_post_engagement > 0) {
            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);
        }
        if ($cost_per_like > 0) {
            $cost_per_like = ($cost_per_like / $total2);
        }
        // echo $actions;
        $returnRow['campaign_header'] = array(
            "addset_active" => $activeAddSet,
            "addset_inactive" => $inActiveAddSet,
            "addset_pending" => $pendingAddSet,
            "addset_disapproved" => $disapprovedAddSet,
            "cost_per_unique_click" => $cost_per_unique_click,
            "cost_per_inline_link_click" => $total_cost_per_inline_click,
            "impressions" => $impressions,
            "cpm" => $cpm,
            "inline_link_clicks" => $total_inline_clicks,
            "reach" => $reach,
            "frequency" => $frequency,
            "unique_clicks" => $unique_clicks,
            "unique_ctr" => $unique_ctr,
            "spent" => $spent,
            "actions" => $actions,
            "cost_per_total_action" => $cost_per_total_action,
            "campaignStatus" => $campaignStatus,
            "inline_post_engagement" => $inline_post_engagement,
            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,
            "page_like" => $page_like,
            "cost_per_like" => $cost_per_like,
        );
        // $this->getPrintResult($returnRow);
        return $returnRow;
    }

    function getCampaignObjective($campaignId) {
        $adAccountId = "act_" . $this->adAccountId;
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$campaignId/?fields=objective,effective_status&access_token=" . $this->access_token;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        //$this->getPrintResult($response);
        return $response;
    }

    function getCurlCampaignsDateRange_new($limit) {

        $resultArray = array();
        $adAccountId = "act_" . $this->adAccountId;
        $campaignFields = $this->getCampaignFields(1);
        $dataArray = explode("#", $limit);
        $url = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId/campaigns?fields=insights.date_preset(".$limit."){clicks,impressions,reach,actions,date_start,campaign_name,ctr,call_to_action_clicks,cost_per_action_type,cost_per_unique_click,unique_clicks,spend,objective,frequency}&limit=250&access_token=" . $this->access_token;
        
        try {

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            $error = $response['error']['message'];
            
            if (isset($response['data'])) {
                $campaignDataArray = $this->getFinalResult($response['data']);
                if ($campaignDataArray) {

                    $url1 = $this->config->item('facebook_graph_url') . $this->config->item('facebook_graph_version') . "/$adAccountId?fields=insights.date_preset(".$limit.")%7Bfrequency%2Cdate_start%7D&access_token=" . $this->access_token;

                    $ch = curl_init($url1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response1 = json_decode($result, true);
                    $campaignDataArray['campaign_header']['newFrequ'] = $response1['insights']['data'][0]['frequency'];

                    $accountArr = $this->Users_model->getUserAddAccountName($this->adAccountId);
                    $campaignDataArray['campaign_header']['accountName'] = $accountArr[0]->add_title;
                    
                    $resultArray = array(
                        "count" => 1,
                        "response" => $campaignDataArray,
                        "error" => "",
                        "success" => ""
                    );
                } else {
                    $resultArray = array(
                        "count" => 0,
                        "response" => "",
                        "error" => $error,
                        "success" => ""
                    );
                }
            } else {
                $resultArray = array(
                    "count" => 0,
                    "response" => "",
                    "error" => $error,
                    "success" => ""
                );
            }
        } catch (Exception $ex) {
            $resultArray = array(
                "count" => 0,
                "response" => "",
                "error" => $e->getMessage(),
                "success" => ""
            );
        }
        #echo "<PRE>";print_R($resultArray);exit;
        //$this->getPrintResult($resultArray);
        return $resultArray;
    }

    function getCampaignFields($curl) {
        if ($curl == 1) {
            $fields = "id,account_id,name,objective,buying_type,created_time,start_time,stop_time,effective_status,currency,configured_status";
        } else {
            $fields = array(
                CampaignFields::ID,
                CampaignFields::ACCOUNT_ID,
                CampaignFields::NAME,
                CampaignFields::OBJECTIVE,
                CampaignFields::BUYING_TYPE,
                CampaignFields::PROMOTED_OBJECT,
                CampaignFields::ADLABELS,
                CampaignFields::CREATED_TIME,
                CampaignFields::START_TIME,
                CampaignFields::STOP_TIME,
                CampaignFields::UPDATED_TIME,
                CampaignFields::EFFECTIVE_STATUS,
                CampaignFields::STATUS_PAUSED,
            );
        }
        return $fields;
    }

    function getFinalResult($campaignDataArray) {
        #echo "<PRE>";print_R($campaignDataArray);exit;
        $activeCampaigns = 0;
        $inActiveCampaigns = 0;
        $cost_per_unique_click = 0;
        $total_cost_per_inline_click = 0;
        $impressions = 0;
        $total_inline_clicks = 0;
        $reach = 0;
        $unique_clicks = 0;
        $unique_ctr = 0;
        $spent = 0;
        $actions = 0;
        $cost_per_total_action = 0;

        $inline_clicks = 0;
        $cost_per_inline_clicks = 0;
        $pendingCampaigns = 0;
        $disapprovedCampaigns = 0;
        $total = 0;
        $total1 = 0;
        $total2 = 0;
        $conversion = 0;
        #echo "<PRE>";print_R($campaignDataArray);exit;
        $returnRow = array();
        foreach ($campaignDataArray as $row) {
            if ($row['effective_status'] == "ACTIVE" || $row['effective_status'] == "PREAPPROVED") {
                $activeCampaigns++;
                $status = "Active";
            } 
            else if ($row['effective_status'] == "PAUSED" || $row['effective_status'] == "CAMPAIGN_PAUSED" || $row['effective_status'] == "PENDING_BILLING_INFO" || $row['effective_status'] == "ADSET_PAUSED") {
                $inActiveCampaigns++;
                $status = "Inactive";
            } 
            else if ($row['effective_status'] == "PENDING_REVIEW") {
                $pendingCampaigns++;
                $status = "In Review";
            } 
            else if ($row['effective_status'] == "DISAPPROVED" || $row['effective_status'] == "DELETED" || $row['effective_status'] == "ARCHIVED") {
                $disapprovedCampaigns++;
                $status = "Denied";
            }
            $total++;
            
            if (isset($row['insights'])) {
                // echo $row['name']."<br>";
                $inline_post_engagement1 = $like1 = '0';
                $conversion = $cost_per_conversion = 0;
                $cost_per_inline_post_engagement1 = $cost_per_like1 = '0';
                if (isset($row['insights']['data'][0])) {
                    foreach ($row['insights']['data'][0]['actions'] as $rs) {
                        #echo "<PRE>";print_r($row['insights']);exit;
                        if ($row['insights']['data'][0]['clicks']) {
                            $inline_clicks = $row['insights']['data'][0]['clicks'];
                            $cost_per_inline_clicks = $row['insights']['data'][0]['spend'] / $row['insights']['data'][0]['clicks'];
                        }
                        if ($rs['action_type'] == "leadgen.other") {
                            if($rs['value']){
                                $conversion = $rs['value'];
                            }
                            else{
                                $conversion = 0;
                            }
                        } 
                        if ($rs['action_type'] == "offsite_conversion") {
                            if($rs['value']){
                                $conversion += $rs['value'];
                                $cost_per_conversion = $row['insights']['data'][0]['spend'] / $rs['value'];
                            }
                            else{
                                $conversion = 0;
                            $cost_per_conversion = 0;
                            }
                        } 
                        if($rs['action_type'] == 'page_engagement'){
                            if($rs['value']){
                                $total1++;
                                $inline_post_engagement1 = $rs['value'];
                            }
                            else{
                                $inline_post_engagement1 = 0;
                            }
                        }
                        if($rs['action_type'] == 'like'){
                            if($rs['value']){
                                $total2++;
                                $like1 = $rs['value'];
                            }
                            else{
                                $like1 = 0;
                            }
                        }
                    }
                    foreach ($row['insights']['data'][0]['cost_per_action_type'] as $rs) {
                        if($rs['action_type'] == 'page_engagement'){
                            if($rs['value']){
                                $cost_per_inline_post_engagement1 = $rs['value'];
                            }
                            else{
                                $cost_per_inline_post_engagement1 = 0;
                            }
                        }
                        if ($rs['action_type'] == "leadgen.other") {
                            if($rs['value']){
                                $cost_per_conversion = $rs['value'];
                            }
                            else{
                                $cost_per_conversion = 0;
                            }
                        } 
                        if($rs['action_type'] == 'like'){
                            if($rs['value']){
                                $cost_per_like1 = $rs['value'];
                            }
                            else{
                                $cost_per_like1 = 0;
                            }
                        }
                    }
                }
                
                $total_inline_clicks+=$inline_clicks;
                $total_cost_per_inline_click+=$cost_per_inline_clicks;
                $cost_per_unique_click += $row['insights']['data'][0]['cost_per_unique_click'];
                $impressions += $row['insights']['data'][0]['impressions'];
                $reach += $row['insights']['data'][0]['reach'];
                $frequency += $row['insights']['data'][0]['frequency'];
                $unique_clicks += $row['insights']['data'][0]['unique_clicks'];
                $unique_ctr += $row['insights']['data'][0]['ctr'];
                $spent += $row['insights']['data'][0]['spend'];
                $actions += $conversion;
                $cost_per_total_action += $cost_per_conversion;
                $inline_post_engagement += $inline_post_engagement1;
                $cost_per_inline_post_engagement += $cost_per_inline_post_engagement1;

                $page_like += $like1;
                $cost_per_like += $cost_per_like1;
                $cost_leadgen1 += $cost_leadgen;
                $leadgen1 += $leadgen;

                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            } 
            else {

                $cost_per_unique_click += 0;
                $impressions += 0;
                $reach += 0;
                $frequency += 0;
                $unique_clicks += 0;
                $unique_ctr += 0;
                $spent += 0;
                $actions += 0;
                $cost_per_total_action += 0;
                $inline_post_engagement += 0;
                $cost_per_inline_post_engagement += 0;

                $page_like += 0;
                $cost_per_like += 0;
                $cost_leadgen1 += 0;
                $leadgen1 += 0;
                
                //Reset   
                $inline_clicks = '--';
                $cost_per_inline_clicks = '--';
            }
        }
        if ($unique_ctr > 0) {
            $unique_ctr = ($unique_ctr / $total);
        }
        if ($total_cost_per_inline_click > 0) {
            $total_cost_per_inline_click = ($total_cost_per_inline_click / $total);
        }
        
        if ($cost_per_inline_post_engagement > 0) {
            $cost_per_inline_post_engagement = ($cost_per_inline_post_engagement / $total1);
        }
        if ($cost_leadgen1 > 0) {
            $cost_leadgen1 = ($cost_leadgen1 / $total3);
        }
        if ($cost_per_like > 0) {
            $cost_per_like = ($cost_per_like / $total2);
        }
        //echo $actions;
        if ($cost_per_total_action > 0) {
            $cost_per_total_action = ($spent / $actions);
        }
        $returnRow['campaign_header'] = array(
            "campaign_active" => $activeCampaigns,
            "campaign_inactive" => $inActiveCampaigns,
            "campaign_pending" => $pendingCampaigns,
            "campaign_disapproved" => $disapprovedCampaigns,
            "cost_per_unique_click" => $cost_per_unique_click,
            "cost_per_inline_link_click" => $total_cost_per_inline_click,
            "impressions" => $impressions,
            "inline_link_clicks" => $total_inline_clicks,
            "reach" => $reach,
            "frequency" => $frequency,
            "unique_clicks" => $unique_clicks,
            "unique_ctr" => $unique_ctr,
            "spent" => $spent,
            "actions" => $actions,
            "actions" => $actions,
            "cost_per_total_action" => $cost_per_total_action,
            "inline_post_engagement" => $inline_post_engagement,
            "cost_per_inline_post_engagement" => $cost_per_inline_post_engagement,
            "page_like" => $page_like,
            "cost_per_like" => $cost_per_like,
            "leadgen" => $leadgen1,
            "cost_leadgen" => $cost_leadgen1,
        );

        #echo "<PRE>";print_R($returnRow);exit;
        return $returnRow;
    }

    function sksort(&$array, $subkey = "id", $subkey2 = null, $sort_ascending = true) {
        if (count($array))
            $temp_array[key($array)] = array_shift($array);
        foreach ($array as $key => $val) {
            $offset = 0;
            $found = false;
            foreach ($temp_array as $tmp_key => $tmp_val) {
                if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                    $temp_array = array_merge(
                            (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));
                    $found = true;
                } elseif (!$found
                        and $subkey2 and strtolower($val[$subkey]) == strtolower($tmp_val[$subkey])
                        and strtolower($val[$subkey2]) > strtolower($tmp_val[$subkey2])) {
                    $temp_array = array_merge(
                            (array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset));
                    $found = true;
                }
                $offset++;
            }
            if (!$found)
                $temp_array = array_merge($temp_array, array($key => $val));
        }
        if ($sort_ascending)
            $array = array_reverse($temp_array);
        else
            $array = $temp_array;

        return $array;
    }

    public function asArray($data) {
        foreach ($data as $k => $v)
            $data[$k] = $v->getData();
        return $data;
    }

    public static function fillDatesKeys($date_from, $date_to) {
        $kv = [];
        $date = $date_from;
        while ($date <= $date_to) {
            $kv[$date] = 0;
            $date = date("Y-m-d", strtotime($date) + 86400);
        }

        return $kv;
    }

    public static function orderByField($arr, $f, $ascending = true) {
        if (is_array($f)) {
            $f1 = $f[0];
            $ka = [];
            foreach ($arr as $el) {
                $f1v = $el[$f1];
                //hr($f1v);
                if (!isset($ka[$f1v]))
                    $ka[$f1v] = [];
                $ka[$f1v][] = $el;
            }

            //hre($ka);

            if ($ascending)
                ksort($ka);
            else
                krsort($ka);

            $arr = [];
            foreach ($ka as $els) {
                $f2 = $f[1];
                $els = $this->orderByField($els, $f2, $ascending);
                $arr = array_merge($arr, $els);
            }

            return $arr;
        }

        $kv = [];
        foreach ($arr as $k => $v)
            $kv[$k] = $v[$f];

        if ($ascending)
            asort($kv);
        else
            arsort($kv);

        $res = [];
        foreach ($kv as $k => $v)
            $res[] = $arr[$k];

        return $res;
    }
}