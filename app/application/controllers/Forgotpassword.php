<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Index class.
 * 
 * @extends CI_Controller
 */
class Forgotpassword extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        error_reporting(E_ERROR);
        $this->load->model('Users_model');
        $this->seo->SetValues('Title', "Forgot password");
        $this->seo->SetValues('Description', "Forgot password");
    }

    public function index() {
        if (!($this->session->userdata('logged_in'))) {
            $data = new stdClass();

            // set validation rules
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');

            if ($this->form_validation->run() == false) {

                // validation not ok, send validation errors to the view
                $data->error1 = 'Wrong email address.';
                //$this->load->view('header');
                $this->load->view('login', $data);
            } else {

                // set variables from the form
                $email = $this->input->post('email');
                $user_id = $this->Users_model->forgetpassword($email);
                if ($user_id) {
                    $hash = md5($email . uniqid(rand(), true));
                    $update = $this->Users_model->update_user_hash($user_id, $hash);
                    if ($update) {

                        $user_data = array();
                        $user_data['first_name'] = ucfirst($this->Users_model->get_user($user_id)->first_name);
                        $url = site_url("forgotpassword/password/" . $hash);
                        $to = $email;
                        $subject = 'Password Reset Request';

                        $message = "<p>Hi " . ucfirst($this->Users_model->get_user($user_id)->first_name) . ",</p><p>Please click the link below to reset your password now.</p><p><a href='" . $url . "'>Reset Password</a><p>Thanks</p>";

                        sendEmail($to, $subject, $message);

                        $data->success1 = 'E-mail sent successfully. Check your email to reset password';

                        $this->form_validation->resetpostdata(true);
                        //$this->load->view('header');
                        $this->load->view('login', $data);
                    } else {
                        // login failed
                        $data->error1 = 'Something Wrong.';

                        // send error to the view
                        //$this->load->view('header');
                        $this->load->view('forgotpassword', $data);
                    }
                } else {

                    // login failed
                    $data->error1 = 'Wrong e-mail address.';

                    // send error to the view
                    //$this->load->view('header');
                    $this->load->view('login', $data);
                }
            }
        } else {
            redirect('/');
        }
    }

    function password($hash = NULL) {

        if ($hash == NULL) {

            redirect('/');
        }
        // create the data object
        $data = new stdClass();

        $id = $this->Users_model->CheckEmailAddress($hash);

        if ($id) {
            $data->hash = $hash;
            //$this->load->view('header');
            $this->load->view('updatepassword', $data);
        } else {
            $data->error1 = 'Sorry Unable to reset your password!';
            //$this->load->view('header');
            $this->load->view('login', $data);
        }
    }

    function update_password() {

        if ($this->session->userdata('logged_in')) {
            redirect('/');
        }

        // create the data object
        $data = new stdClass();

        // set validation rules
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('rpassword', 'Confirm Password', 'trim|required|min_length[8]|matches[password]');
        $this->form_validation->set_rules('hash', 'Hash', 'trim|required|min_length[8]',array('required'=>'It seems you are going to cheat!'));
                    
        $hash = $this->input->post('hash');
        $data->hash = $hash;
        if ($this->form_validation->run() == false) {

            //$this->load->view('header');
            $this->load->view('updatepassword', $data);
        } else {
            // set variables from the form
            $password = $this->input->post('password');
            $user_id = $this->Users_model->CheckEmailAddress($hash);
            if ($user_id && $hash) {

                $update = $this->Users_model->update_password($user_id, md5($password));
                if ($update) {

                    $data->success = 'Password Updated successfully. Please login with your new password';
                    // user creation ok
                    //$this->load->view('header');
                    $this->load->view('login', $data);
                } else {
                    // password update failed
                    $data->error = 'Some thing Wrong.';
                    //$this->load->view('header');
                    $this->load->view('updatepassword', $data);
                }
            } else {

                // login failed
                $data->error = 'Some thing Wrong.';
                //$this->load->view('header');
                $this->load->view('updatepassword', $data);
            }
        }
    }

}
