<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Special extends CI_Controller {
	
    public function __construct() {
		 error_reporting(1);
       parent::__construct();
		if (!isset($this->session->userdata['logged_in']) && empty($this->session->userdata['logged_in'])) {
			$this->session->set_userdata('last_page', current_url());
            redirect(site_url('/login'));
		}
     
    }

    public function index() {
		
        $data = new stdClass();
        
		loadView('special', $data);
    }

    
   
}

//End of class