<!DOCTYPE html>
<html>
<head>
	<!-- set the encoding of your site -->
	<meta charset="utf-8">
	<!-- set the viewport width and initial-scale on mobile devices  -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php SeoDetail('Title'); ?></title>
	<link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>/assets/img/favicon.ico">
	  <link href="https://fonts.googleapis.com/css?family=Heebo:400,500,700" rel="stylesheet">
  <!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>newlogin/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>newlogin/css/main.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
</head>
<body>
<!-- main container of all the page elements -->
	<div id="wrapper">
		<div class="w1">
<!-- header of the page -->
			<header id="header" class="d-md-flex justify-content-between align-items-center">
				<div class="logo-holder">
					<a class="logo" href="<?php echo $this->config->item('site_url'); ?>"><img src="<?php echo $this->config->item('assets'); ?>newlogin/images/logo.svg" alt="The Campaign Maker"></a>
				</div>
			</header>