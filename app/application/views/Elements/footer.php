<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<nav class="footer-nav">
								<ul class="d-flex flex-wrap justify-content-center justify-content-md-start text-center">
									<li class="footer-nav-item">
										<a href="<?php echo $this->config->item('site_url'); ?>blog/terms-of-service" class="footer-nav-link">Terms of Service</a>
									</li>
									<li class="footer-nav-item">
										<a href="<?php echo $this->config->item('site_url'); ?>blog" class="footer-nav-link">Privacy Policy</a>
									</li>
									<li class="footer-nav-item">
										<a href="<?php echo $this->config->item('site_url'); ?>blog" class="footer-nav-link">Blog</a>
									</li>
									<li class="footer-nav-item">
										<a href="#" class="footer-nav-link">About Us</a>
									</li>
									<li class="footer-nav-item">
										<a href="#" class="footer-nav-link">Help</a>
									</li>
								</ul>
							</nav>
						</div>
						<div class="col-md-4">
							<div class="made-with text-center text-md-right">
								<span>Made with &lt;3 <a href="#">by FUBSZ</a></span>
							</div>
						</div>
					</div>
				</div>
					</footer>
						
	 <script src="<?php echo $this->config->item('assets'); ?>newlogin/js/bootstrap.min.js"></script>
      <script src="<?php echo $this->config->item('assets'); ?>newlogin/js/jquery.main.js"></script>
	  </div>
	</div>
</body>
</html>