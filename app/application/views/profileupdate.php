<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<main id="main">

<!-- <?php if (trialExpired()) { ?>	
<div class="top-notification-box">
          <i class="icon-warning"></i>
<span><?= trialExpired(); ?></span> 

      <div class="gender-label" style="display: inline-flex;margin-left: 10px;margin-top: -7px;">
                              <span class="c active" onclick="showupgradepopup()">
                            
                            Upgrade Now 
</span>                        
</div> 
                           

                       
        </div>
        <?php } ?> -->
       
        <div class="settings-holder">
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-6">
                <form action="#">
                  <div class="settings-box">
                    <div class="setting-header text-center">
                      <h2>YOUR ACCOUNT INFORMATION</h2>
                    </div>
                    <div class="panel-group">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <strong class="panel-title">
                            <label for="fullName">FULL NAME</label>
                          </strong>
                        </div>
                        <div class="panel-collapse collapse in">
                          <div class="panel-body">
                            <div class="sk3 form-group">
                              <input id="fullName" type="text" placeholder="<?= $user->first_name . ' ' . $user->last_name; ?>" class="form-control" value="" readonly="readonly">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <strong class="panel-title">
                            <label for="email">EMAIL ADDRESS</label>
                          </strong>
                        </div>
                        <div class="panel-collapse collapse in">
                          <div class="panel-body">
                            <div class="sk3 form-group">
                              <input id="email" type="email" placeholder="<?= $user->email; ?>" class="form-control" value="" readonly="readonly">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <strong class="panel-title">
                            <label>ACCOUNT TYPE</label>
                          </strong>
                        </div>
                        <div class="panel-collapse collapse in">
                          <div class="panel-body">
                            <div class="gender-label">
                              <span class="a"><?php echo $SubscriptionDetail->title; ?></span>
                              <?php if(!empty($user->stripe_subscription_id)){?>
                               <span class="a red" id="cancelsubscrip">Cancel Subscription</span>
                              <?php } ?>
                             <!-- <span class="c active">Active</span> -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <strong class="panel-title">
                            <label for="billingEndDate">ACCOUNT END DATE</label>
                          </strong>
                        </div>
                        <div class="panel-collapse collapse in">
                          <div class="panel-body date-picker">
                            <div class="sk3 form-group">
                              <input id="billingEndDate" type="text" placeholder="<?php echo date("d/m/Y", strtotime($usersubscription->billing_end_date));  ?>" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                      </div>
                   <!--   <div class="panel panel-default">
                        <div class="panel-heading">
                          <strong class="panel-title">
                            <label for="billingEndDate">NEXT PAYMENT DATE</label>
                          </strong>
                        </div>
                        <div class="panel-collapse collapse in">
                          <div class="panel-body date-picker">
                            <div class="sk3 form-group">
                              <input id="billingEndDate" type="text" placeholder="<?php 
                              if($nextinvoicedate  != "nothing"){
                                echo $nextinvoicedate;
                                }
                                else{
                                    echo "No Payment Due";
                                }
                                ?>" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        
                      </div> -->
                    </div>
                  </div>
                </form>
              </div>
              <div class="col-sm-6 col-md-5 col-md-offset-1">

              <?php if($this->session->flashdata('settingsmessage') == 'y'){ ?>
              	<div class="top-notification-box">
                  <i class="icon-warning"></i>
                  <span class="text">Your Message Has Been Sent.</span>
                </div>
                <?php } ?>
                <form action="<?php echo base_url(); ?>Updateprofile/sendemailaccountchange" method="post">
                  <div class="settings-box">
                    <div class="setting-header text-center">
                      <h2>REQUEST ACCOUNT CHANGE</h2>
                    </div>
                    <div class="change-request-box">
                      <div class="form-group validation">
                        <input name="rcname" type="text" class="form-control" placeholder="Full name" required="required">
                        <span class="validate-info">*</span>
                      </div>
                      <div class="form-group validation">
                        <input name="rcemail" type="email" class="form-control" placeholder="Your Account Email" required="required">
                        <span class="validate-info">*</span>
                      </div>
                      <div class="validation">
                        <textarea name="rcmessage" class="form-control" rows="1" placeholder="Explain clearly what you would like to change" required="required"></textarea>
                        <span class="validate-info">*</span>
                      </div>
                      <div class="btn-wrap text-center">
                        <button type="submit" class="btn btn-success">Send Message</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
      <!-- Main Content Ends -->
    </div>
    <style>
    .gender-label .a.red{
        color: #cc0000;
        border: 1px solid #cc0000;
    
    }
    .gender-label span.red.active,
.gender-label span.red:hover {
	background: rgba(204, 0, 0, .2);
}
    </style>
    <script>
        jQuery("#cancelsubscrip").click(function(){
            window.location.href = "<?php echo base_url(); ?>Updateprofile/cancelusersubscription";
        }); 
    </script>