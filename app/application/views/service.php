<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');

?>
<style>
    p.error {
        color: red;
        font-weight: bold;
    }
    #loading_report{
        display:none;
        width:100%;
        height:100%; 
        background:#fff; 
        opacity:0.5; 
        text-align:center;
        top:0px;
        left:0px;
        position:fixed;
        padding-top:15%;
        z-index: 9999;
    }
    .form-control {
        box-shadow: none;
    }
</style>
<div id="loading_report">
    <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/loading.gif" alt="Loading..">
</div> 
<form id="service_form" action="" type="post">
    <main id="main_1">
        <div class="step-bar">
            <div class="holder d-flex flex-wrap">
                <div class="step active">
                    <strong class="step-name">Contact</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Campaign</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Offer Info</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Competition</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Restrictions</strong>
                </div>
            </div>
        </div>
        <div class="main-content s1">
            <div class="container">
                <div class="center-box s8">
                    <div class="holder s1">
                        <div class="frame s1">
                            <h2 class="screen-heading text-center">What Is the Best Way to Contact You?</h2>
                            <div class="form-wrap s1">
                                <div class="form-group s1">
                                    <input type="text" name="usernamae" value="<?php echo $session_data['first_name']; ?> <?php echo $session_data['last_name']; ?>" class="required form-control" placeholder="*USER NAME HERE*">
                                </div>
                                <div class="form-group s1">
                                    <input type="text" name="email" value="<?php echo $session_data['email']; ?>" class="required form-control" placeholder="*USER EMAIL HERE*">
                                </div>
                            </div>
                        </div>
                        <div class="btn-holder text-center">
                            <a href="javascript:void(null);" onclick="gonext('1')" class="btn btn-next">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
                                Next Step
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <main id="main_2" style="display:none">
        <div class="step-bar">
            <div class="holder d-flex flex-wrap">
                <div class="step done">
                    <strong class="step-name">Contact</strong>
                </div>
                <div class="step active">
                    <strong class="step-name">Campaign</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Offer Info</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Competition</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Restrictions</strong>
                </div>
            </div>
        </div>
        <div class="main-content s1">
            <div class="container">
                <div class="center-box s8">
                    <div class="holder s1">
                        <div class="frame s1">
                            <h2 class="screen-heading text-center">
                                What Campaign Should We Analyze for the Quote?
                                <span class="sub-heading">The campaign chosen will base the quote we provide, so the more data the campaign has the better results we can give you.
                                    <strong class="highlight-text">Pro tip:  The campaign should have a minimum of 5,000 impressions to be considered.</strong>
                                </span>
                            </h2>
                            <div class="form-wrap s1">
                                <div class="form-group s1">
                                    <div class="select-holder s2">
                                        <select class="required service_ad_account" name="add_account">
                                            <option>-select your ad account -</option>
                                            <?php if(!empty($adaccounts)): ?>
                                                <?php foreach($adaccounts as $key => $adacount): ?>
                                            <option data-id="<?php echo $adacount->ad_account_id; ?>" value="<?php echo $adacount->add_title; ?>"><?php echo $adacount->add_title; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group s1">
                                    <div class="select-holder s2">
                                        <select id="service_campaign" class="required" name="campaign_account">
                                            <!--<option value="">Choose Your Campaign</option>-->
                                            <?php foreach($campaigns_list as $key => $campaign){  ?>
                                            <option value="<?php echo $campaign;  ?>"><?php echo $campaign;  ?></option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-holder text-center">
                            <a href="javascript:void(null);" onclick="goprev('2')" class="btn btn-prev">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
                                Previous Step
                            </a>
                            <a href="javascript:void(null);" onclick="gonext('2')" class="btn btn-next">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
                                Next Step
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <main id="main_3" style="display:none">
        <div class="step-bar">
            <div class="holder d-flex flex-wrap">
                <div class="step done">
                    <strong class="step-name">Contact</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Campaign</strong>
                </div>
                <div class="step active">
                    <strong class="step-name">Offer Info</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Competition</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Restrictions</strong>
                </div>
            </div>
        </div>
        <div class="main-content s1">
            <div class="container">
                <div class="center-box s8">
                    <div class="holder s1">
                        <div class="frame s1">
                            <h2 class="screen-heading text-center">
                                Tell Us in Your Own Words What Your Campaign Is Offering.
                                <span class="sub-heading">This will help base our research process and will give us insight on your business.
                                    <strong class="highlight-text">Pro tip:  Try to explain your offer in no more than 20 words.</strong>
                                </span>
                            </h2>
                            <div class="form-wrap s1">
                                <div class="form-group s1">
                                    <textarea class="form-control required" name="offer_info" cols="30" rows="3" placeholder="type your offer information here.."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="btn-holder text-center">
                            <a href="javascript:void(null);" onclick="goprev('3')" class="btn btn-prev">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
                                Previous Step
                            </a>
                            <a href="javascript:void(null);" onclick="gonext('3')" class="btn btn-next">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
                                Next Step
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <main id="main_4" style="display:none">
        <div class="step-bar">
            <div class="holder d-flex flex-wrap">
                <div class="step done">
                    <strong class="step-name">Contact</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Campaign</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Offer Info</strong>
                </div>
                <div class="step active">
                    <strong class="step-name">Competition</strong>
                </div>
                <div class="step">
                    <strong class="step-name">Restrictions</strong>
                </div>
            </div>
        </div>
        <div class="main-content s1">
            <div class="container">
                <div class="center-box s8">
                    <div class="holder s1">
                        <div class="frame s1">
                            <h2 class="screen-heading text-center">
                                Who Are Your Biggest Competitors?
                                <span class="sub-heading">You can just write down the sites of your competitors.
                                    <strong class="highlight-text">Pro tip:  No explanation needed, we will find their FB pages and other social accounts.</strong>
                                </span>
                            </h2>
                            <div class="form-wrap s1">
                                <div class="form-group s1">
                                    <textarea class="form-control required" name="competitors_url" cols="30" rows="3" placeholder="type the URL's here.."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="btn-holder text-center">
                            <a href="javascript:void(null);" onclick="goprev('4')" class="btn btn-prev">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
                                Previous Step
                            </a>
                            <a href="javascript:void(null);" onclick="gonext('4')" class="btn btn-next">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
                                Next Step
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <main id="main_5" style="display:none">
        <div class="step-bar">
            <div class="holder d-flex flex-wrap">
                <div class="step done">
                    <strong class="step-name">Contact</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Campaign</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Offer Info</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Competition</strong>
                </div>
                <div class="step active">
                    <strong class="step-name">Restrictions</strong>
                </div>
            </div>
        </div>
        <div class="main-content s1">
            <div class="container">
                <div class="center-box s8">
                    <div class="holder s1">
                        <div class="frame s1">
                            <h2 class="screen-heading text-center">
                                Do You Have Any Special Restrictions We Should Follow?
                                <span class="sub-heading">Anything specific we should avoid or must include in your campaign.
                                    <strong class="highlight-text">Pro tip:  if you do not have any specific restrictions then type none.</strong>
                                </span>
                            </h2>
                            <div class="form-wrap s1">
                                <div class="form-group s1">
                                    <textarea class="form-control required" name="restrictions" cols="30" rows="3" placeholder="type your special restrictions here.."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="btn-holder text-center">
                            <a href="javascript:void(null);" onclick="return submit_service_quote();" class="btn btn-submit-quote">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-paper-plane.svg" alt="plane"></span>
                                SUBMIT QUOTE
                            </a>
                        </div>
                        <div class="btn-holder text-center">
                            <a href="javascript:void(null);" onclick="goprev('5')" class="btn btn-prev">
                                <span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
                                Previous Step
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <main id="main_6" style="display:none">
        <div class="step-bar">
            <div class="holder d-flex flex-wrap">
                <div class="step done">
                    <strong class="step-name">Contact</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Campaign</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Offer Info</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Competition</strong>
                </div>
                <div class="step done">
                    <strong class="step-name">Restrictions</strong>
                </div>
            </div>
        </div>
        <div class="main-content s1">
            <div class="container">
                <div class="center-box s10">
                    <div class="holder s1">
                        <div class="img-holder text-center">
                            <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-hat.svg" alt="hat">
                        </div>
                        <h2 class="screen-heading text-center">
                            QUOTE REQUESTED SUCCESSFULLY!
                            <span class="sub-heading">You can expect to recieve your quote within 1 business day to the email provided!</span>
                        </h2>
                        <div class="success-message-holder text-center">
                            <p>Did you know that our annual users get a special discount. <br class="d-none d-md-block">
                                More information provided in the quote.</p>
                            <p class="highlight-text">Make sure you add <a href="mailto:samy@thecampaignmaker.com">samy@thecampaignmaker.com</a> to your email contact list!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</form>    

<script type="text/javascript">

    function goprev(step) {

        var nextstep = parseInt(step) - 1;

        $('#main_' + step).hide();
        $('#main_' + nextstep).show();
    }

    function gonext(step) {
        k = 0;
        $('#main_' + step + ' .error').remove();
        $('#main_' + step + ' .required').each(function (i, e) {
            if (!$(e).val()) {
                $(this).parents('.form-group').append('<p class="error">please fill this field</p>');
                k++;
            }
        });
        if (k > 0) {
            return false;
        }
        var nextstep = parseInt(step) + 1;
        $('#main_' + step).hide();
        $('#main_' + nextstep).show();
    }
    function submit_service_quote() {
        var formData = new FormData($("#service_form")[0]);
        $.ajax({
            url: '<?php echo base_url(); ?>/service/submit_quote',
            type: 'POST',
            data: formData,
            datatype: 'json',
            beforeSend: function () {
                // do some loading options
                $('#loading_report').show();
            },
            success: function (data) {
                gonext('5');
                $('#loading_report').hide();
            },

            complete: function () {
                // success alerts.
                $('#loading_report').hide();
            },

            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#loading_report').hide();
            },
            cache: false,
            contentType: false,
            processData: false

        });

    }
    
    $(document).ready(function(){
       $(document).on('change', '.service_ad_account', function(){
           var ad_account_id = $('option:selected', this).attr('data-id');
           $.ajax({
                type: "POST",
                cache: false,
                url: "<?php echo base_url() . 'service/getCampaigns'; ?>/",
                dataType: "json",
                data: {
                    ad_account_id: ad_account_id,
                },
                beforeSend: function () {
                    $('#loading_report').show();
                    $('#service_campaign').empty().append('<option value="">-select campaign-</option>');
                },
                success: function (reponse) {
                    $.each(reponse, function (index, value) {
                        $('#service_campaign').append($('<option/>', { 
                            value: value.campaign_name,
                            text : value.campaign_name 
                        }));
                    });  
                    $('#loading_report').hide();
                },
                complete: function () {
                    // success alerts.
                    $('#loading_report').hide();
                },

                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                    $('#loading_report').hide();
                },
            });
       });
    });
</script>