<header>
    <script>
fbq('track', 'InitiateCheckout');
</script>

		
		<div class="container-fluid">
			<div class="row no-gutter">
				<div class="col-sm-6">
					<div class="blue">
						<div class="price-item">
							<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/blue.svg" alt="">
							<div class="period">Monthly</div>
							<p class="period-p">Includes everything you need to create a successful Facebook campaign.</p>
							<div class="price special"><span class="dollar">$ </span>39</div>
							<div class="price"><span class="dollar">$ </span>19 <span class="per">/Month</span></div>
							<div class="features">
								<ul>
									<li>Unlimited Split Testing</li>
									<li>Unlimited Ad Accounts</li>
									<li>Unlimited Ad Spend</li>
									<li>Advanced Report Dashboard</li>
									<li>Priority Support</li>
								</ul>
							</div>
							<div class="clearfix"></div>
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="monthlyformabc">
 <input type="hidden" name="cmd" value="_xclick-subscriptions" />
 <input type="hidden" name="business" value="samy@fubsz.com" />
 <input type="hidden" name="item_name" value="The Campaign Maker - Special Monthly Subscription" />
<input type="hidden" name="item_number" value="<?php echo $this->session->userdata['logged_in']['id']; ?>" />
 <input type="hidden" name="currency_code" value="USD" />
 <input type="hidden" name="return" value="https://thecampaignmaker.com/ui/login/refreshplan" />
 <input type="hidden" name="a3" value="19" />
 <input type="hidden" name="p3" value="1" />
 <input type="hidden" name="t3" value="M" />
 <input type="hidden" name="rm" value="2" />
 <input type="hidden" name="notify_url" value="https://thecampaignmaker.com/aff/connect/paypal_ipn_recurring.php" />
 <input type="hidden" name="custom" value="<?PHP echo $_SERVER['REMOTE_ADDR']; ?>" />
 <input type="hidden" name="src" value="1" />
 <input type="hidden" name="no_note" value="1" />
<input type="hidden" name="no_shipping" value="1">
<a href="javascript:{}" class="price-button" onclick="document.getElementById('monthlyformabc').submit();">Purchase</a>
<!-- <a href="#" class="price-button">Purchase</a> -->
 </form>
							
							<div class="bottom">This $0.63/day can prevent hundreds of dollars of wasted adspend</div>
						</div>
					</div>
				</div>	
				<div class="col-sm-6">
					<div class="black">
						<div class="price-item">
							<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/black.svg" alt="">
							<div class="period">Yearly</div>
							<p class="period-p">IPush your ROI even further by going yearly and save an additional 50%.</p>
							<div class="price special"><span class="dollar">$ </span>19</div>
							<div class="price"><span class="dollar">$ </span>9 <span class="per">/Month &nbsp;&nbsp;&nbsp;Paid Yearly</span></div>
							<div class="features">
								<ul>
									<li>Unlimited Split Testing</li>
									<li>Unlimited Ad Accounts</li>
									<li>Unlimited Ad Spend</li>
									<li>Advanced Report Dashboard</li>
									<li>Priority Support</li>
									<li>Invitation to Elite Facebook Group</li>
								</ul>
							</div>
							<div class="clearfix"></div>
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="yearslyform">
 <input type="hidden" name="cmd" value="_xclick-subscriptions" />
 <input type="hidden" name="business" value="samy@fubsz.com" />
 <input type="hidden" name="item_name" value="The Campaign Maker - Pro Yearly Subscription" />
 <input type="hidden" name="item_number" value="<?php echo $this->session->userdata['logged_in']['id']; ?>" />
 <input type="hidden" name="currency_code" value="USD" />
 <input type="hidden" name="return" value="https://thecampaignmaker.com/ui/login/refreshplan" />
 <input type="hidden" name="a3" value="108" />
 <input type="hidden" name="p3" value="1" />
 <input type="hidden" name="t3" value="Y" />
 <input type="hidden" name="rm" value="2" />
 <input type="hidden" name="notify_url" value="https://thecampaignmaker.com/aff/connect/paypal_ipn_recurring.php" />
 <input type="hidden" name="custom" value="<?PHP echo $_SERVER['REMOTE_ADDR']; ?>" />
 <input type="hidden" name="src" value="1" />
 <input type="hidden" name="no_note" value="1" />
<input type="hidden" name="no_shipping" value="1">
<a href="javascript:{}" class="price-button" onclick="document.getElementById('yearslyform').submit();">Purchase</a>

 </form>
							<!-- <a href="#" class="price-button">Purchase</a>-->
							<div class="bottom">This <span class="red">$0.29/day</span> can prevent hundreds of dollars of wasted adspend</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="offer">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35 42">
			<g fill="none" fill-rule="evenodd" opacity=".9">
				<path stroke="#FFF" stroke-width="4" d="M31.447 36.79c0 1.763-1.481 3.187-3.3 3.187H6.739c-1.825 0-3.299-1.43-3.299-3.186V18.84"/>
				<path fill="#FFF" d="M30.956 30.222v-4.474h-7.729V24.8h7.729v-5.958h.982v14.14h-.982v-1.81h-7.799v-.949h7.799zm-18.048-.352v-4.122H3.643V24.8h9.265v-6.1h.983v21.08h-.983v-8.96H3.797v-.95h9.111zm8.283 9.91h-.982V18.698h.982V39.78zM12.98 18.224h-.983v-4.949h.983v4.949zm9.125 0h-.983v-4.949h.983v4.949z"/>
				<path stroke="#FFF" stroke-width="4" d="M6.731 18.366h-4.71v-5.321h30.844v5.321H10.297m7.483-5.719l-9.245.398c-3.467 0-3.671-6.155.885-6.155 4.235 0 7.87 4.974 8.36 5.674.49-.7 4.125-5.674 8.36-5.674 4.556 0 4.352 6.155.885 6.155l-9.245-.398z"/>
				<path fill="#7DC855" d="M15.205 4.407l-.644.717-2.94-2.46.643-.718 2.94 2.46zm6.394.717l-.642-.718 2.948-2.46.643.718-2.949 2.46zM18.54 3.491h-.982V0h.982v3.491z"/>
			</g>
		</svg>
		<p>This Special Offer Ends in June 1st 2017</p>
	</div>
	<div class="price-slider">
		<div class="container">
			<div class="heading">You Can Be One Of Them</div>
			<div class="carousel owl-carousel">
				<div class="testimonial">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
							<svg xmlns="http://www.w3.org/2000/svg" width="32" height="27" viewBox="0 0 32 27">
								<path fill="#FFF" fill-rule="evenodd" d="M12.308 4.91c1.358 0 2.461-1.1 2.461-2.455A2.459 2.459 0 0 0 12.308 0C5.51 0 0 5.496 0 12.273v9.818A4.916 4.916 0 0 0 4.923 27h4.923a4.915 4.915 0 0 0 4.923-4.91v-4.908a4.915 4.915 0 0 0-4.923-4.91H4.923c0-4.066 3.306-7.363 7.385-7.363m14.769 7.364h-4.923c0-4.067 3.306-7.364 7.384-7.364 1.36 0 2.462-1.1 2.462-2.454A2.459 2.459 0 0 0 29.538 0c-6.796 0-12.307 5.496-12.307 12.273v9.818A4.916 4.916 0 0 0 22.154 27h4.923A4.915 4.915 0 0 0 32 22.09v-4.908a4.915 4.915 0 0 0-4.923-4.91" opacity=".203"/>
							</svg>
							<div class="text">I want to reiterate this software is awesome! In March it helped me achieve an ROAS of 321% increase and I'm closing out April at 600% on my ecommerce store. No, I'm not shitting you! I'm seriously impressed.</div>
							<div class="name">Lane W<span class="company">Shoe Conspiracy</span></div>
						</div>
					</div>
				</div>
				<div class="testimonial">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
							<svg xmlns="http://www.w3.org/2000/svg" width="32" height="27" viewBox="0 0 32 27">
								<path fill="#FFF" fill-rule="evenodd" d="M12.308 4.91c1.358 0 2.461-1.1 2.461-2.455A2.459 2.459 0 0 0 12.308 0C5.51 0 0 5.496 0 12.273v9.818A4.916 4.916 0 0 0 4.923 27h4.923a4.915 4.915 0 0 0 4.923-4.91v-4.908a4.915 4.915 0 0 0-4.923-4.91H4.923c0-4.066 3.306-7.363 7.385-7.363m14.769 7.364h-4.923c0-4.067 3.306-7.364 7.384-7.364 1.36 0 2.462-1.1 2.462-2.454A2.459 2.459 0 0 0 29.538 0c-6.796 0-12.307 5.496-12.307 12.273v9.818A4.916 4.916 0 0 0 22.154 27h4.923A4.915 4.915 0 0 0 32 22.09v-4.908a4.915 4.915 0 0 0-4.923-4.91" opacity=".203"/>
							</svg>
							<div class="text">I want to reiterate this software is awesome! In March it helped me achieve an ROAS of 321% increase and I'm closing out April at 600% on my ecommerce store. No, I'm not shitting you! I'm seriously impressed.</div>
							<div class="name">Lane W<span class="company">Shoe Conspiracy</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>