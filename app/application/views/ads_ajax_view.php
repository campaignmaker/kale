<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
if (isset($adaccountsCount) && $adaccountsCount == 1) {
    ?>
    <div class="col-md-8 p-rl-3 d-box campaign-head">
        <div class="row today">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard-stat blue-madison blue-madison1">
                    <div class="visual">
                        <i class="fa fa-usd" style="left:-10px;"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php
                            if ($adaccounts['adds_header']['spent'] > 0)
                            {
                                echo $this->session->userdata('cur_currency') . number_format($adaccounts['adds_header']['spent'], 2, '.', ',');
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . number_format($adaccounts['adds_header']['spent']);
                            }
                            ?>
                        </div>
                        <div class="desc">
                            TOTAL SPENT
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-r-5">
                <div class="dashboard-stat green-haze blue-madison2">
                    <div class="visual">
                        <i class="fa fa-mouse-pointer" style="left:-10px;"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo number_format($adaccounts['adds_header']['inline_link_clicks']); ?> <small>Clicks</small>
                        </div>
                        <div class="desc">
                            <?php
                            if ($adaccounts['adds_header']['spent'] > 0)
                            {
                                $cost_per_click = $adaccounts['adds_header']['spent'] / $adaccounts['adds_header']['inline_link_clicks'];
                                if ($cost_per_click > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . round(number_format($cost_per_click, 3, '.', ','),2);
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . round(number_format($cost_per_click),2);
                                }
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . $adaccounts['adds__header']['inline_link_clicks'];
                            }
                            ?> per click
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-rl-5">
                <div class="dashboard-stat c3-plum blue-madison3">
                    <div class="visual">
                        <i class="fa fa-thumbs-o-up"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo number_format($adaccounts['adds_header']['page_like']); ?> <small>Page Likes</small>
                        </div>
                        <div class="desc">
                            <?php
                            echo $this->session->userdata('cur_currency').round(($adaccounts['adds_header']['spent']/$adaccounts['adds_header']['page_like']), 2);
                            ?> per like
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-l-5">
                <div class="dashboard-stat green-haze blue-madison4">
                    <div class="visual">
                        <i class="fa fa-eye"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php
                            echo number_format($adaccounts['adds_header']['impressions']);
                            ?>
                        </div>
                        <div class="desc">
                            Impressions
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-r-5">
                <div class="dashboard-stat c3-plum blue-madison2">
                    <div class="visual">
                        <i class="fa fa-rocket" style="left:-30px;"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo number_format($adaccounts['adds_header']['actions']); ?> <small>Conversions</small>
                        </div>
                        <div class="desc">
                            <?php
                            if ($adaccounts['adds_header']['cost_per_total_action'] > 0)
                            {
                                echo $this->session->userdata('cur_currency') . number_format($adaccounts['adds_header']['cost_per_total_action'], 2, '.', ',');
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . number_format($adaccounts['adds_header']['cost_per_total_action']);
                            }
                            ?> per conversion
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-rl-5">
                <div class="dashboard-stat green-haze blue-madison3">
                    <div class="visual">
                        <i class="fa fa-comment-o fa-flip-horizontal"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo number_format($adaccounts['adds_header']['inline_post_engagement']); ?> <small>Engages</small>
                        </div>
                        <div class="desc">
                            <?php
                            echo $this->session->userdata('cur_currency').round(($adaccounts['adds_header']['spent']/$adaccounts['adds_header']['inline_post_engagement']),2);
                            ?> per engagement
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-l-5">
                <div class="dashboard-stat c3-plum blue-madison4">
                    <div class="visual">
                        <i class="fa fa-percent"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php
                                echo number_format((($adaccounts['adds_header']['inline_link_clicks']/$adaccounts['adds_header']['impressions'])*100), 2, '.', ',') . "%";
                            ?>
                        </div>
                        <div class="desc">
                            Click Through Rate
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 p-rl-5">
        <div class="portlet box blue-madison desktop-mobile1" style="margin-bottom:7px;">
            <div class="portlet-title">
                <div class="caption">
                    &nbsp; Mobile VS Desktop
                </div>
            </div>
            <div class="portlet-body d-m">
                <div class="desktop-mobile">
                    <div class="lapmobbox">
                        <div align="center" class="col-md-4 col-sm-4 desktop"> 
                            <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/laptop.png" alt="">
                            <h2 class="desktop-value">0%</h2>
                            <span class="desktop-name">Desktop</span>
                        </div>
                        <div class="col-md-4 col-sm-4 slider-avg">
                            <br />
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                                </div>
                            </div>
                        </div>
                        <div align="center" class="col-md-4 col-sm-4 mobile"> <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/smartphone.png" alt="">
                            <h2 class="mobile-value">0%</h2>
                            <span class="mobile-name">Mobile</span> 
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="portlet box  blue-madison male-female-div">
            <div class="portlet-title">
                <div class="caption">
                    &nbsp; Gender
                </div>
            </div>
            <div class="portlet-body d-m">
                <div class="male-female">
                    <div class="lapmobbox">
                        <div align="center" class="col-md-6 col-sm-6 col-xs-6 male"> 
                            <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/male.png" alt="">
                            <h2 class="male-value">0%</h2>
                        </div>
                        <div align="center" class="col-md-6 col-sm-6 col-xs-6 female"> <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/female.png" alt="">
                            <h2 class="female-value">0%</h2>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD STATS -->
    <div class="clearfix">
    </div>
    <div class="clearfix">
    </div>
    <div class="row" id="commongraph" >
        <div class="col-md-3 country-div">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        Country
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        <div id="country" class="chart"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 p-l-5">
            <div class="col-md-6 p-rl-5 age-div">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            Age
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div>
                            <div id="age" class="chart"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 p-rl-5 time-div">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            &nbsp; Time
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div>
                            <div id="time" class="chart"></div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="row">
        <div class="col-md-12">
        <!-- BEGIN table-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>Ads
                </div>
            </div>
            <div class="portlet-body">
                <div class="">
                    <table class="table table-striped table-bordered table-hover" id="sample_Ads">
                        <thead>
                            <tr>
                                <th style="width: 5%;">
                                    Status
                                </th>
                                <th style="width: 33%;">
                                    Name
                                </th>
                                <th style="width: 15%;">
                                    Clicks
                                </th>
                                <th style="width: 9%;">
                                    Conversions
                                </th>
                                <th style="width: 9%;">
                                    Page Likes
                                </th>
                                <th style="width: 9%;">
                                    Engagement
                                </th>
                                <th style="width: 5%;">
                                    Reach
                                </th>
                                <th style="width: 8%;">
                                    CTR
                                </th>
                                <th style="width: 8%;">
                                    Spent
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                if (isset($adaccounts['adds'])){ 
                                    foreach ($adaccounts['adds'] as $campaign){
                                        ?>
                                            <tr>
                                                <?php if($campaign['adds_effective_status'] == 'Active'){
                                                    ?>
                                                        <td align="center" class="pt-n">
                                                            <input type="checkbox" checked="" name="check-1" value="<?php echo $campaign['adds_id']; ?>" class="lcs_check" autocomplete="off" /> 
                                                        </td>
                                                    <?php
                                                }
                                                else if($campaign['adds_effective_status'] == 'In Review'){
                                                    ?>
                                                        <td align="center" class="pt-n inreview">
                                                            <div class="checker disabled">
                                                                <span>
                                                                    <div class="lcs_wrap">
                                                                        <div class="lcs_switch  lcs_off lcs_disabled lcs_checkbox_switch">
                                                                            <div class="lcs_cursor"></div>
                                                                            <div class="lcs_label lcs_label_on"><span class="fa fa-exclamation"></span></div>
                                                                            <div class="lcs_label lcs_label_off"><span class="fa fa-exclamation"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    <?php
                                                }
                                                else if($campaign['adds_effective_status'] == 'Denied'){
                                                    ?>
                                                        <td align="center" class="pt-n decline">
                                                            <div class="checker disabled">
                                                                <span>
                                                                    <div class="lcs_wrap">
                                                                        <div class="lcs_switch  lcs_off lcs_disabled lcs_checkbox_switch">
                                                                            <div class="lcs_cursor"></div>
                                                                            <div class="lcs_label lcs_label_on"><span class="fa fa-times"></span></div>
                                                                            <div class="lcs_label lcs_label_off"><span class="fa fa-times"></span></div>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    <?php
                                                }
                                                else{
                                                    ?>
                                                        <td align="center" class="pt-n">
                                                            <input type="checkbox" name="check-1" value="<?php echo $campaign['adds_id']; ?>" class="lcs_check" autocomplete="off" /> 
                                                        </td>
                                                    <?php
                                                } ?>
                                                <td style="text-align: left;">
                                                    <a href='<?php echo site_url('adone/' . $addAccountId . '/' . $campaignId . '/' . $addsetId . '/' . $campaign['adds_id']); ?>'>
                                                        <?php echo $campaign['adds_name'] ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <?php
                                                        if ($campaign['adds_inline_link_clicks'] > 0)
                                                        {
                                                            echo number_format($campaign['adds_inline_link_clicks']);
                                                        }
                                                        else
                                                        {
                                                            echo $campaign['adds_inline_link_clicks'];
                                                        }
                                                    ?> @ 
                                                    <?php
                                                        if ($campaign['adds_cost_per_inline_link_click'] == '--')
                                                        {
                                                            echo '--';
                                                        }
                                                        else
                                                        {
                                                            echo $this->session->userdata('cur_currency') . number_format($campaign['adds_cost_per_inline_link_click'], 2, '.', ',');
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo number_format($campaign['adds_actions']); ?>
                                                    @ 
                                                    <?php echo $this->session->userdata('cur_currency') . number_format($campaign['adds_cost_per_total_action'], 2, '.', ','); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if ($campaign['page_like'] > 0)
                                                        {
                                                            echo number_format($campaign['page_like']);
                                                        }
                                                        else
                                                        {
                                                            echo $campaign['page_like'];
                                                        }
                                                    ?> @ 
                                                    <?php
                                                        if ($campaign['cost_per_like1'] == '--')
                                                        {
                                                            echo '--';
                                                        }
                                                        else
                                                        {
                                                            echo $this->session->userdata('cur_currency') . number_format($campaign['cost_per_like1'], 2, '.', ',');
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if ($campaign['inline_post_engagement'] > 0)
                                                        {
                                                            echo number_format($campaign['inline_post_engagement']);
                                                        }
                                                        else
                                                        {
                                                            echo $campaign['inline_post_engagement'];
                                                        }
                                                    ?> @ 
                                                    <?php
                                                        if ($campaign['cost_per_inline_post_engagement'] == '--')
                                                        {
                                                            echo '--';
                                                        }
                                                        else
                                                        {
                                                            echo $this->session->userdata('cur_currency') . number_format($campaign['cost_per_inline_post_engagement'], 2, '.', ',');
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo number_format($campaign['adds_reach']); ?>
                                                </td>
                                                <td>
                                                    <?php echo number_format($campaign['adds_unique_ctr'], 2, '.', ','); ?>%
                                                </td>
                                                <td>
                                                    <b><?php echo $this->session->userdata('cur_currency') . number_format($campaign['adds_spent'], 2, '.', ','); ?></b>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END table-->
    </div>
    </div>

    <?php
} else if (isset($adaccountsCount) && $adaccountsCount == 2) {
    ?>
<div class="col-md-8 p-rl-3 d-box campaign-head">
        <div class="row today">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard-stat blue-madison blue-madison1">
                    <div class="visual">
                        <i class="fa fa-usd" style="left:-10px;"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php
                            if ($adaccounts['adds_header']['spent'] > 0)
                            {
                                echo $this->session->userdata('cur_currency') . number_format($adaccounts['adds_header']['spent'], 2, '.', ',');
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . number_format($adaccounts['adds_header']['spent']);
                            }
                            ?>
                        </div>
                        <div class="desc">
                            TOTAL SPENT
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-r-5">
                <div class="dashboard-stat green-haze blue-madison2">
                    <div class="visual">
                        <i class="fa fa-mouse-pointer" style="left:-10px;"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo number_format($adaccounts['adds_header']['inline_link_clicks']); ?> <small>Clicks</small>
                        </div>
                        <div class="desc">
                            <?php
                            if ($adaccounts['adds_header']['spent'] > 0)
                            {
                                $cost_per_click = $adaccounts['adds_header']['spent'] / $adaccounts['adds_header']['inline_link_clicks'];
                                if ($cost_per_click > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . round(number_format($cost_per_click, 3, '.', ','),2);
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . round(number_format($cost_per_click),2);
                                }
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . $adaccounts['adds__header']['inline_link_clicks'];
                            }
                            ?> per click
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-rl-5">
                <div class="dashboard-stat c3-plum blue-madison3">
                    <div class="visual">
                        <i class="fa fa-thumbs-o-up"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo number_format($adaccounts['adds_header']['page_like']); ?> <small>Page Likes</small>
                        </div>
                        <div class="desc">
                            <?php
                            echo $this->session->userdata('cur_currency').round(($adaccounts['adds_header']['spent']/$adaccounts['adds_header']['page_like']), 2);
                            ?> per like
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-l-5">
                <div class="dashboard-stat green-haze blue-madison4">
                    <div class="visual">
                        <i class="fa fa-eye"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php
                            echo number_format($adaccounts['adds_header']['impressions']);
                            ?>
                        </div>
                        <div class="desc">
                            Impressions
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-r-5">
                <div class="dashboard-stat c3-plum blue-madison2">
                    <div class="visual">
                        <i class="fa fa-rocket" style="left:-30px;"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo number_format($adaccounts['adds_header']['actions']); ?> <small>Conversions</small>
                        </div>
                        <div class="desc">
                            <?php
                            if ($adaccounts['adds_header']['cost_per_total_action'] > 0)
                            {
                                echo $this->session->userdata('cur_currency') . number_format($adaccounts['adds_header']['cost_per_total_action'], 2, '.', ',');
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . number_format($adaccounts['adds_header']['cost_per_total_action']);
                            }
                            ?> per conversion
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-rl-5">
                <div class="dashboard-stat green-haze blue-madison3">
                    <div class="visual">
                        <i class="fa fa-comment-o fa-flip-horizontal"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo number_format($adaccounts['adds_header']['inline_post_engagement']); ?> <small>Engages</small>
                        </div>
                        <div class="desc">
                            <?php
                            echo $this->session->userdata('cur_currency').round(($adaccounts['adds_header']['spent']/$adaccounts['adds_header']['inline_post_engagement']),2);
                            ?> per engagement
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-l-5">
                <div class="dashboard-stat c3-plum blue-madison4">
                    <div class="visual">
                        <i class="fa fa-percent"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php
                                echo number_format((($adaccounts['adds_header']['inline_link_clicks']/$adaccounts['adds_header']['impressions'])*100), 2, '.', ',') . "%";
                            ?>
                        </div>
                        <div class="desc">
                            Click Through Rate
                        </div>
                    </div>
                    <a class="more" href="#">
                        &nbsp;
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php
} else {
    ?>
    <div class="col-lg-12 m-t-10 messages">
        <?php if (isset($error) && !empty($error)) : ?>
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span>
                    <?php echo $error ?> </span>
            </div>

        <?php endif; ?>

        <?php if (isset($success) && !empty($success)) : ?>
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <span>
                    <?php echo $success; ?> </span>
            </div>

        <?php endif; ?>
    </div>
    <?php
}
 