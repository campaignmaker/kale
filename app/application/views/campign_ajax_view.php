<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
if (isset($adaccountsCount) && $adaccountsCount == 1) {
?>

    <div class="dashboard-holder js-masonry" data-masonry-options='{"columnWidth": 1, "itemSelector": ".data-show-box", "percentPosition": true }'>
        <div class="data-show-box">
            <div class="data-header">
              <strong class="title">CLICKS</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content graph-data">
              <div class="text-holder">
                <strong class="title"><?php echo number_format($adaccounts['campaign_header']['inline_link_clicks']); ?></strong>
                <span class="sub-text">
                    <?php
                    if ($adaccounts['campaign_header']['spent'] > 0)
                    {
                        $cost_per_click = $adaccounts['campaign_header']['spent'] / $adaccounts['campaign_header']['inline_link_clicks'];
                        if ($cost_per_click > 0)
                        {
                            echo $this->session->userdata('cur_currency') . round(number_format($cost_per_click, 3, '.', ','),2);
                        }
                        else
                        {
                            echo $this->session->userdata('cur_currency') . round(number_format($cost_per_click),2);
                        }
                    }
                    else
                    {
                        echo $this->session->userdata('cur_currency') . $adaccounts['campaign_header']['inline_link_clicks'];
                    }
                    ?> per click
                </span>
              </div>
              <div id="area-chart1" class="click-div"></div>
            </div>
        </div>
        <div class="data-show-box long">
            <div class="data-header">
              <strong class="title">TOTAL SPENT</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content graph-data">
              <div class="l-chart">
                <div class="aspect-ratio spent-div" id="chart">

                </div>
              </div>
            </div>
        </div>
        <div class="data-show-box">
            <div class="data-header">
              <strong class="title">ENGAGEMENT</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content graph-data">
              <div class="text-holder">
                <strong class="title"><?php echo number_format($adaccounts['campaign_header']['inline_post_engagement']); ?></strong>
                <span class="sub-text">
                    <?php
                        echo $this->session->userdata('cur_currency').round(($adaccounts['campaign_header']['spent']/$adaccounts['campaign_header']['inline_post_engagement']),2);
                    ?> per engagement
                </span>
              </div>
              <div id="area-chart2" class="engagement-div"></div>
            </div>
        </div>
        <div class="data-show-box xx-small-height">
            <div class="freq-box">
              <div class="ico-holder">
                <i class="icon-calendar"></i>
              </div>
              <div class="freq-desc">
                <strong class="title">FREQ</strong>
                <strong class="count"><?php echo number_format($adaccounts['campaign_header']['frequency'], 2); ?> Times</strong>
              </div>
            </div>
        </div>
        <div class="data-show-box long-height">
            <div class="data-header">
              <strong class="title">PLACEMENT</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content placement-div">
              <div class="progress-holder">
                <strong class="title-text"><span class="num" id="desktop">2 324</span> <span class="sub-text">Desktop News Feed</span></strong>
                <div class="progress-container">
                  <div class="progress progress-1">
                    <div class="progress-bar" id="desktopPer-1" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
                      <span class="sr-only">60% Complete</span>
                    </div>
                  </div>
                  <div class="progress-info" id="desktopPer">30%</div>
                </div>
              </div>
              <div class="progress-holder">
                <strong class="title-text"><span class="num" id="mobile">124</span> <span class="sub-text">Mobile News Feed</span></strong>
                <div class="progress-container">
                  <div class="progress progress-2">
                    <div class="progress-bar" id="mobilePer-1" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                      <span class="sr-only">60% Complete</span>
                    </div>
                  </div>
                  <div class="progress-info" id="mobilePer">20%</div>
                </div>
              </div>
              <div class="progress-holder">
                <strong class="title-text"><span class="num" id="right_hand">324</span> <span class="sub-text">Desktop RHS</span></strong>
                <div class="progress-container">
                  <div class="progress progress-3">
                    <div class="progress-bar" id="right_handPer-1" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                      <span class="sr-only">60% Complete</span>
                    </div>
                  </div>
                  <div class="progress-info" id="right_handPer">20%</div>
                </div>
              </div>
              <div class="progress-holder">
                <strong class="title-text"><span class="num" id="mobile_external_only">1 724</span> <span class="sub-text">3rd Party Mobile Sites</span></strong>
                <div class="progress-container">
                  <div class="progress progress-4">
                    <div class="progress-bar" id="mobile_external_onlyPer-1" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 55%">
                      <span class="sr-only">60% Complete</span>
                    </div>
                  </div>
                  <div class="progress-info" id="mobile_external_onlyPer">20%</div>
                </div>
              </div>
              <div class="progress-holder">
                <strong class="title-text"><span class="num" id="instant_article">1 724</span> <span class="sub-text">Instant Article</span></strong>
                <div class="progress-container">
                  <div class="progress progress-4">
                    <div class="progress-bar" id="instant_articlePer-1" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 55%">
                      <span class="sr-only">60% Complete</span>
                    </div>
                  </div>
                  <div class="progress-info" id="instant_articlePer">57%</div>
                </div>
              </div>
            </div>
        </div>
        <div class="data-show-box">
            <div class="data-header">
              <strong class="title">PAGE LIKES</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content graph-data">
              <div class="text-holder">
                <strong class="title"><?php echo number_format($adaccounts['campaign_header']['page_like']); ?></strong>
                <span class="sub-text">
                    <?php
                        echo $this->session->userdata('cur_currency').round(($adaccounts['campaign_header']['spent']/$adaccounts['campaign_header']['page_like']), 2);
                        ?> per like
                </span>
              </div>
              <div id="area-chart3" class="pagelike-div"></div>
            </div>
        </div>
        <div class="data-show-box long">
            <div class="data-header">
              <strong class="title">CTR &amp; IMPRESSIONS</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content">
              <div class="graph-data-row">
                <div class="data-holder impression-div" id="buyers1">
                  
                </div>
                <div class="data-info">
                  <i class="icon-share"></i>
                  <span class="num">
                    %<?php
                        echo number_format((($adaccounts['campaign_header']['inline_link_clicks']/$adaccounts['campaign_header']['impressions'])*100), 2, '.', ',');
                    ?>
                  </span>
                  <span class="sub-text">CTR</span>
                </div>
              </div>
              <div class="graph-data-row">
                <div class="data-holder ctr-div" id="buyers">
                  
                </div>
                <div class="data-info">
                  <i class="icon-user"></i>
                    <span class="num">
                        <?php
                        echo number_format($adaccounts['campaign_header']['impressions']);
                        ?>
                    </span>
                  <span class="sub-text">IMPRESSIONS</span>
                </div>
              </div>
            </div>
        </div>
        <div class="data-show-box">
            <div class="data-header">
              <strong class="title">CONVERSIONS</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content graph-data">
              <div class="text-holder">
                <strong class="title"><?php echo number_format($adaccounts['campaign_header']['actions']); ?></strong>
                <span class="sub-text">
                    <?php
                        if ($adaccounts['campaign_header']['cost_per_total_action'] > 0)
                        {
                            echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['cost_per_total_action'], 2, '.', ',');
                        }
                        else
                        {
                            echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['cost_per_total_action']);
                        }
                    ?> per conversion
                </span>
              </div>
              <div id="area-chart4" class="conversion-div"></div>
            </div>
        </div>
        <div class="data-show-box long-1">
            <div class="data-header">
              <strong class="title">AGE GROUPS</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class='data-content graph-data age age-div' id="morris-bar-chart">
              
            </div>
        </div>
        <div class="data-show-box x-small">
            <div class="data-header">
              <strong class="title">LOCATIONS</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content graph-data country-div" id="morris-donut-chart1">
              
            </div>
        </div>
        <div class="data-show-box x-small male-female-div">
            <div class="data-header">
              <strong class="title">GENDER</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content graph-data male-female cntntBoxs" id="morris-donut-chart">
              
            </div>
        </div>
        <div class="data-show-box bar-data">
            <div class="data-header">
              <strong class="title">TIME</strong>
              <a href="#" class="more-link"><i class="icon-more-button"></i></a>
            </div>
            <div class="data-content graph-data time-div cntntBoxs" id="morris-bar-chart-time">
              
            </div>
        </div>
    </div>
    <div class="container-fluid">
      <div class="detail-status-holder">
        <div class="table-holder">
          <form action="#">
            <table class="table account-data-table dt-responsive nowrap" id="datatable-responsive">
              <colgroup>
                <col class="col5">
                <col class="col6">
                <col class="col7">
                <col class="col8">
                <col class="col9">
                <col class="col10">
                <col class="col11">
                <col class="col12">
                <col class="col13">
                <col class="col14">
              </colgroup>
              <thead>
                <tr>
                  <th><span class="sort-tag"><span>STATUS</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>CAMPAIGN NAME</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>CLICKS</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>CONVERSIONS</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>ENGAGEMENT</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>PAGE LIKES</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>CTR</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>IMPRESSIONS</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>SPENT</span> <i class="icon-sort"></i></span></th>
                  <th><span class="sort-tag"><span>DATE</span> <i class="icon-sort"></i></span></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                    if (isset($adaccounts['campaigns'])){ 
                        foreach ($adaccounts['campaigns'] as $campaign){
                            ?>
                                <tr>
                                    <?php if($campaign['campaign_effective_status'] == 'Active'){
                                        ?>
                                            <td class="status">
                                                <label class="switch-button">
                                                    <input type="checkbox" checked name="check-1" class="lcs_check" value="<?php echo $campaign['campaign_id']; ?>">
                                                    <span class="fake-toggle">
                                                    <span class="switch">&nbsp;</span>
                                                </span>
                                                </label>
                                            </td>
                                        <?php
                                    }
                                    else if($campaign['campaign_effective_status'] == 'In Review'){
                                        ?>
                                            <td align="center" class="pt-n inreview">
                                                <div class="checker disabled">
                                                    <span>
                                                        <div class="lcs_wrap">
                                                            <div class="lcs_switch  lcs_off lcs_disabled lcs_checkbox_switch">
                                                                <div class="lcs_cursor"></div>
                                                                <div class="lcs_label lcs_label_on"><span class="fa fa-exclamation"></span></div>
                                                                <div class="lcs_label lcs_label_off"><span class="fa fa-exclamation"></span></div>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </div>
                                            </td>
                                        <?php
                                    }
                                    else if($campaign['campaign_effective_status'] == 'Denied'){
                                        ?>
                                            <td align="center" class="pt-n decline">
                                                <div class="checker disabled">
                                                    <span>
                                                        <div class="lcs_wrap">
                                                            <div class="lcs_switch  lcs_off lcs_disabled lcs_checkbox_switch">
                                                                <div class="lcs_cursor"></div>
                                                                <div class="lcs_label lcs_label_on"><span class="fa fa-times"></span></div>
                                                                <div class="lcs_label lcs_label_off"><span class="fa fa-times"></span></div>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </div>
                                            </td>
                                        <?php
                                    }
                                    else{
                                        ?>
                                            <td class="status">
                                                <label class="switch-button">
                                                    <input type="checkbox" name="check-1" class="lcs_check" value="<?php echo $campaign['campaign_id']; ?>">
                                                    <span class="fake-toggle">
                                                    <span class="switch">&nbsp;</span>
                                                </span>
                                                </label>
                                            </td>
                                        <?php
                                    } ?>
                                    <td class="title">
                                        <a href='<?php echo site_url('addset/' . $addAccountId . '/' . $campaign['campaign_id']); ?>'>
                                            <?php echo $campaign['campaign_name'] ?>
                                        </a>
                                    </td>
                                    <td>
                                        <?php
                                            if ($campaign['campaign_inline_link_clicks'] > 0)
                                            {
                                                echo number_format($campaign['campaign_inline_link_clicks']);
                                            }
                                            else
                                            {
                                                echo $campaign['campaign_inline_link_clicks'];
                                            }
                                        ?> @ 
                                        <?php
                                            if ($campaign['campaign_cost_per_inline_link_click'] == '--')
                                            {
                                                echo '--';
                                            }
                                            else
                                            {
                                                echo $this->session->userdata('cur_currency') . number_format($campaign['campaign_cost_per_inline_link_click'], 2, '.', ',');
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $campaign['campaign_actions'];
                                        ?>
                                        @ 
                                        <?php echo ($campaign['campaign_cost_per_total_action'] > 0) ? $this->session->userdata('cur_currency') . number_format($campaign['campaign_cost_per_total_action'], 2, '.', ',') : $this->session->userdata('cur_currency') . $campaign['campaign_cost_per_total_action']; ?>
                                    </td>
                                    <td>
                                        <?php
                                            if ($campaign['page_like'] > 0)
                                            {
                                                echo number_format($campaign['page_like']);
                                            }
                                            else
                                            {
                                                echo $campaign['page_like'];
                                            }
                                        ?> @ 
                                        <?php
                                            if ($campaign['cost_per_like1'] == '--')
                                            {
                                                echo '--';
                                            }
                                            else
                                            {
                                                echo $this->session->userdata('cur_currency') . number_format($campaign['cost_per_like1'], 2, '.', ',');
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if ($campaign['inline_post_engagement'] > 0)
                                            {
                                                echo number_format($campaign['inline_post_engagement']);
                                            }
                                            else
                                            {
                                                echo $campaign['inline_post_engagement'];
                                            }
                                        ?> @ 
                                        <?php
                                            if ($campaign['cost_per_inline_post_engagement'] == '--')
                                            {
                                                echo '--';
                                            }
                                            else
                                            {
                                                echo $this->session->userdata('cur_currency') . number_format($campaign['cost_per_inline_post_engagement'], 2, '.', ',');
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($campaign['campaign_unique_ctr'], 2, '.', ','); ?>%
                                    </td>
                                    <td>
                                        <?php echo number_format($campaign['campaign_impressions']); ?>
                                    </td>
                                    <td>
                                        <b><?php echo $this->session->userdata('cur_currency') . number_format($campaign['campaign_spent'], 2, '.', ','); ?></b>
                                    </td>
                                    <td>
                                        <?php echo date("d/m/y", strtotime($campaign['campaign_created_time'])); ?>
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                ?>
              </tbody>
            </table>
          </form>
        </div>
      </div>
    </div>

    <?php
} 
else if (isset($adaccountsCount) && $adaccountsCount == 2) {
    ?>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard-stat blue-madison blue-madison1">
            <div class="visual">
                <i class="fa fa-usd" style="left:-10px;"></i>
            </div>
            <div class="details">
                <div class="number">
    <?php
    if ($adaccounts['campaign_header']['spent'] > 0) {
        echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['spent'], 2, '.', ',');
    } else {
        echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['spent']);
    }
    ?>
                </div>
                <div class="desc">
                    TOTAL SPENT
                </div>
            </div>
            <a class="more" href="#">
                &nbsp;
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-r-5">
        <div class="dashboard-stat green-haze blue-madison2">
            <div class="visual">
                <i class="fa fa-mouse-pointer" style="left:-10px;"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php echo number_format($adaccounts['campaign_header']['inline_link_clicks']); ?> <small>Clicks</small>
                </div>
                <div class="desc">
    <?php
    if ($adaccounts['campaign_header']['spent'] > 0) {
        $cost_per_click = $adaccounts['campaign_header']['spent'] / $adaccounts['campaign_header']['inline_link_clicks'];
        if ($cost_per_click > 0) {
            echo $this->session->userdata('cur_currency') . number_format($cost_per_click, 3, '.', ',');
        } else {
            echo $this->session->userdata('cur_currency') . number_format($cost_per_click);
        }
    } else {
        echo $this->session->userdata('cur_currency') . $adaccounts['campaign_header']['inline_link_clicks'];
    }
    ?>/click
                </div>
            </div>
            <a class="more" href="#">
                &nbsp;
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-rl-5">
        <div class="dashboard-stat c3-plum blue-madison3">
            <div class="visual">
                <i class="fa fa-thumbs-o-up"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php echo number_format($adaccounts['campaign_header']['page_like']); ?> <small>Page Likes</small>
                </div>
                <div class="desc">
                    <?php
                    if ($adaccounts['campaign_header']['cost_per_like'] > 0) {
                        echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['cost_per_like'], 2, '.', ',');
                    } else {
                        echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['cost_per_like']);
                    }
                    ?> per like
                </div>
            </div>
            <a class="more" href="#">
                &nbsp;
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-l-5">
        <div class="dashboard-stat green-haze blue-madison4">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    echo number_format($adaccounts['campaign_header']['reach']);
                    ?>
                </div>
                <div class="desc">
                    Impression
                </div>
            </div>
            <a class="more" href="#">
                &nbsp;
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-r-5">
        <div class="dashboard-stat c3-plum blue-madison2">
            <div class="visual">
                <i class="fa fa-comments-o" style="left:-30px;"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php echo number_format($adaccounts['campaign_header']['actions']); ?> <small>Conversions</small>
                </div>
                <div class="desc">
                    <?php
                    if ($adaccounts['campaign_header']['cost_per_total_action'] > 0) {
                        echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['cost_per_total_action'], 2, '.', ',');
                    } else {
                        echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['cost_per_total_action']);
                    }
                    ?> per conversion
                </div>
            </div>
            <a class="more" href="#">
                &nbsp;
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-rl-5">
        <div class="dashboard-stat green-haze blue-madison3">
            <div class="visual">
                <i class="fa fa-group"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php echo number_format($adaccounts['campaign_header']['inline_post_engagement']); ?> <small>Engagements</small>
                </div>
                <div class="desc">
                    <?php
                    if ($adaccounts['campaign_header']['cost_per_inline_post_engagement'] > 0) {
                        echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['cost_per_inline_post_engagement'], 2, '.', ',');
                    } else {
                        echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['cost_per_inline_post_engagement']);
                    }
                    ?> per engagement
                </div>
            </div>
            <a class="more" href="#">
                &nbsp;
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-l-5">
        <div class="dashboard-stat c3-plum blue-madison4">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php
                    if ($adaccounts['campaign_header']['unique_ctr'] > 0) {
                        echo number_format($adaccounts['campaign_header']['unique_ctr'], 2, '.', ',') . "%";
                    } else {
                        echo number_format($adaccounts['campaign_header']['unique_ctr']) . "%";
                    }
                    ?>
                </div>
                <div class="desc">
                    CTR
                </div>
            </div>
            <a class="more" href="#">
                &nbsp;
            </a>
        </div>
    </div>

    <?php
} 
else {
    ?>
    <div class="col-lg-12 m-t-10 messages">
    <?php if (isset($error) && !empty($error)) : ?>
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span>
                        <?php echo $error ?> </span>
            </div>

                    <?php endif; ?>

    <?php if (isset($success) && !empty($success)) : ?>
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <span>
        <?php echo $success; ?> </span>
            </div>

    <?php endif; ?>
    </div>
<?php }
?>
