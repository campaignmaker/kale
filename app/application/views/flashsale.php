<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>The Campaign Maker Flash Sale</title>
<link rel="shortcut icon" href="https://thecampaignmaker.com/img/favicon.ico" type="image/x-icon">
    <!-- CSS Styles -->
    <link href="https://thecampaignmaker.com/css/plugins/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://thecampaignmaker.com/css/plugins/owl.carousel.min.css">
    <link rel="stylesheet" href="https://thecampaignmaker.com/css/plugins/owl.theme.default.min.css">
    <link href="https://thecampaignmaker.com/css/credit-custom.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://thecampaignmaker.com/css/credit-style.css">
    <script type="text/javascript" src="https://js.stripe.com/v1/"></script>

  <script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load("1418879561");

   heap.addUserProperties({'name': '<?= $session_data['full_name'] ?>','email': '<?= $session_data['email'] ?>'});

   heap.identify(<?= $session_data['email'] ?>);
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1352351844789066');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1352351844789066&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<script>
  window.intercomSettings = {
    app_id: "pio8n28y",
    name: "<?= $session_data['full_name'] ?>", // Full name
    email: "<?= $session_data['email'] ?>", // Email address

  };
  </script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/pio8n28y';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

</head>
<body>
    <script>
  fbq('track', 'AddPaymentInfo');
</script>

<!-- Menu Tab-->
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="https://thecampaignmaker.com/pricing.html">
                <img src="https://thecampaignmaker.com/img/Logo1.svg" class="img-responsive">
            </a>
        </div>
    </div>
</nav>
<!--Main Content 1-->

<div id="credit-content1" class="box-content-shadow">
    <div class="container">
        <div class="row">
            <p class="credit-headline text-center">
                <span>
                    <img src="https://thecampaignmaker.com/img/lightning.png" id="features-img">
                </span>
                <span id="star">Discount Offer for <?= @ucfirst($session_data['first_name']) ?></span>
            </p>



                    <script type="text/javascript" src="https://thecampaignmaker.com/aff/connect/stripe_ip.php"></script>

                        <div class="top-notification-box" id="successmsgBx1" style="display: none;">
                      <i class="icon-warning"></i>
                      <span class="text payment-errors"></span>
                    </div>

            <div class="offer-wrapper">
                <button id="button1" onclick="Text1()" class="content-img-holder btn-active">
                    <div class="offer-best-deal-wrapper">
                        <div class="content-holder-popular">
                            <img src="https://thecampaignmaker.com/img/popular.png" class="img-responsive"/>
                        </div>
                    </div>
                    <div class="offer-title-wrapper">
                        <div class="offer-info-logo">
                            <img src="https://thecampaignmaker.com/img/abtesting/content8-img2.png" class="img-responsive"/>
                        </div>
                        <div class="offer-info-data">
                            <p class="credit-headline color-blue">Yearly</p>
                            <h1><span class="color-red"><b>$114</b></span>/year</h1>
                        </div>
                    </div>
                    <div class="offer-info-description">
                        <p class="text23 text-left">Lock in this price for 1 year!</p>
                    </div>
                </button>
                <button id="button2" onclick="Text2()" class="content-img-holder">
                    <div class="offer-best-deal-wrapper">
                        <div class="content-holder-popular">
                            <img src="https://thecampaignmaker.com/img/popular.png" class="img-responsive"/>
                        </div>
                    </div>
                    <div class="offer-title-wrapper">
                        <div class="offer-info-logo">
                            <img src="https://thecampaignmaker.com/img/abtesting/content8-img2.png" class="img-responsive"/>
                        </div>
                        <div class="offer-info-data">
                            <p class="credit-headline color-blue">Monthly</p>
                            <h1><span class="color-red"><b>$19</b></span>/month</h1>
                        </div>
                    </div>
                    <div class="offer-info-description">
                        <p class="text23 text-left">Everything included..</p>
                    </div>
                </button>

            </div>


            <script>
                function Text1() {
                    $('#button2').removeClass('btn-active');
                    $('#button1').addClass('btn-active');

                    $('#div1').html('<h1 class="text-center">Save 50% and pay <span class="color-red"><b>$114</b></span>/year</h1>');
                    $('#selectedplan').val('50DiscountYearly');
                }

                function Text2() {
                    $('#button1').removeClass('btn-active');
                    $('#button2').addClass('btn-active');

                    $('#div1').html('<h1 class="text-center">Save 50% and pay <span class="color-red"><b>$19</b></span>/month</h1>');
                    $('#selectedplan').val('50DiscountMonthly');
                }
            </script>

            <div>
                <div id="div1">
                    <h1 class="text-center">Save 50% and pay <span class="color-red"><b>$114</b></span>/year</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-10">

                <div class="credit-form-wrapper">
                   <!--FORM BEGIN-->
                <form action="<?php echo base_url()."/flashsale/processpayment"; ?>" method="POST" id="stripe-payment-form">
                    <input type="hidden" name="idev_custom" id="idev_custom_x21" />
                    <input type="hidden" name="idev_custom" id="idev_custom_x21" />
                    <script type="text/javascript" src="https://thecampaignmaker.com/aff/connect/stripe_ip.php"></script>



                        <div class="card-back-side">
                            <div class="card-back-title">
                                <p>CREDIT CARD</p>
                            </div>
                            <div class="card-back-line"></div>
                            <div class="card-back-data" autocomplete="off">
                                <label for="">CCV</label>
                                <input type="text" autocomplete="off" class="card-cvc">
                            </div>
                        </div>
                        <div class="card-front-side">
                            <div class="card-front-logo">
                                <img src="https://thecampaignmaker.com/img/master.png" alt="">
                                <img src="https://thecampaignmaker.com/img/amex.png" alt="">
                                <img src="https://thecampaignmaker.com/img/visa.png" alt="">
                            </div>
                            <div class="card-front-chip">
                                <img src="https://thecampaignmaker.com/img/chip.png" alt="">
                            </div>
                            <div class="card-front-number" autocomplete="off">
                                <label for="">NUMBER</label>
                                <input type="text"class="card-numbertxt" autocomplete="off">
                            </div>
                            <div class="card-front-info">
                                <div class="card-info-name" autocomplete="off">
                                    <label for="">NAME</label>
                                    <input type="texttext" class="card-name" autocomplete="off">
                                </div>
                                <div class="card-info-date">
                                    <label for="">EXPIRE</label>
                                    <div class="select-wrapper">
                                        <select class="card-expiry-month">
                                            <option disabled="MM" value="MM" selected="MM">MM</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                        <select class="card-expiry-year">
                                            <option disabled="YYYY" value="YYYY" selected="YYYY">YYYY</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-3 col-sm-offset-1 col-sm-10">
                <div class="credit-advantages">
                    <h2 class="text-left"><span> <img src="https://thecampaignmaker.com/img/credit-icon1.png"></span>Unlimited Ad Accounts</h2>
                    <h2 class="text-left"><span> <img src="https://thecampaignmaker.com/img/credit-icon2.png"></span>Unlimited Ad Spend
                    </h2>
                    <h2 class="text-left"><span> <img src="https://thecampaignmaker.com/img/credit-icon3.png"></span>Access To All
                        Tools</h2>
                    <h2 class="text-left"><span> <img src="https://thecampaignmaker.com/img/credit-icon4.png"></span>Cancel Anytime</h2>

                </div>
            </div>
        </div>
    </div>

</div>
</div>

<!--Main Content 3-->
<div id="credit-content3" class="box-content-shadow">
    <div class="container">
        <div class="row">
            <div class="ccol-xs-12">
                <div class="container1">
                    <ul>
                        <li>
                            <input type="radio" id="f-option" name="selector" checked>
                            <label for="f-option">
                                <h3>
                                    I agree with the
                                    <b>Terms of Use</b>
                                </h3>
                            </label>
                            <div class="check"></div>
                        </li>
                    </ul>
                </div>
                        <input name="action" value="stripe" type="hidden">
                        <input name="redirect" value="" type="hidden">
                        <input name="amount" value="NDk=" type="hidden">
                        <input name="stripe_nonce" value="0dd70c1b2d" type="hidden">

                <div class="content-btn color-bg-violet">
                    <button onclick=“return validate();” type="submit" id="stripe-submit" style="background-color: #2c76bb ;border-color: #2c76bb ;border-width: 0px;">
                    <h2><span><img src="https://thecampaignmaker.com/img/lightning2.png"></span>Submit Order</h2></button>
                </div>
            </div>
        </div>

    </div>
    <input type="hidden" id= "selectedplan" name="selectedplan" value="50DiscountYearly">
    </form>
         <!--FORM END-->
    <!--Main Content 4-->

</div>
<!--Login Footer-->

<footer>
    <div id="credit-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-3 col-sm-4 col-sm-offset-2 col-xs-6">
                    <h4 class="text-center"><a href="http://blog.thecampaignmaker.com/terms-of-service/" class="text-decoration-none">Terms Of Use</a>
                    </h4>
                </div>
                <div class="col-md-3 3 col-sm-4 col-xs-6">
                    <h4 class="text-center"><a href="http://blog.thecampaignmaker.com/privacy-policy/" class="text-decoration-none">Privacy Policy</a></h4>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Javascripts -->
<script src="https://thecampaignmaker.com/js/plugins/jquery.min.js"></script>
<script src="https://thecampaignmaker.com/js/plugins/owl.carousel.min.js"></script>
<script src="https://thecampaignmaker.com/js/plugins/bootstrap.min.js"></script>
<script>
    //var vid = document.getElementById("content4-video");

    //document.getElementById("content4-video").addEventListener("click", function(){
    //	vid.play();
    //	vid.setAttribute("poster", "");
    //});
    //vid.onended = function(){
    //	vid.load();
    //	vid.setAttribute("poster", "img/credit/poster.png");
    //};

    $(document).ready(function () {

        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 10,
            nav: true,
            center: true,
            loop: true,
            dots: false,
            smartSpeed: 600,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        $(".owl-prev").html("<i class='arrow-left'></i>");
        $(".owl-next").html("<i class='arrow-right'></i>");
    });
</script>
<script>
  window.intercomSettings = {
    app_id: "pio8n28y"
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/pio8n28y';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

<script>
Stripe.setPublishableKey("pk_live_HA0omoOtinEyA28gk3DqVoMK");

function stripeResponseHandler(status, response) {

    if (response.error) {

    // show errors returned by Stripe

		jQuery("#successmsgBx1").show();
        jQuery(".payment-errors").html(response.error.message);
        $('#myModal').modal();

    // re-enable the submit button

    jQuery('#stripe-submit').attr("disabled", false);

    } 
        else{
    		jQuery("#successmsgBx1").hide();

            var form$ = jQuery("#stripe-payment-form");

            // token contains id, last4, and card type

            var token = response['id'];

            // insert the token into the form so it gets submitted to the server

            form$.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");

            // and submit

            form$.get(0).submit();
        }

    

}

jQuery(document).ready(function($) {

  $("#stripe-payment-form").submit(function(event) {

    // disable the submit button to prevent repeated clicks

    $('#stripe-submit').attr("disabled", "disabled");


    // send the card details to Stripe

   Stripe.createToken({

      number: $('.card-numbertxt').val(),

      name: $('.card-name').val(),

      cvc: $('.card-cvc').val(),

      exp_month: $('.card-expiry-month').val(),

      exp_year: $('.card-expiry-year').val()

    }, stripeResponseHandler);


    // prevent the form from submitting with the default action

    return false;

  });

});
</script>

<script>
function validate(){
$(“input”).filter(function () {

  return $.trim($(this).val()).length == 0

}).length == 0;
}
</script>
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error Message From Stripe</h4>
        </div>
        <div class="modal-body">
        <span class="text payment-errors"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>

</body>
</html>
