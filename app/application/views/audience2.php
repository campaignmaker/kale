<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-sm-9 col-md-10 col-lg-9 col-md-offset-1 displaynone tabcontent" id="step3">
  <div class="top-notification-box" id="msg_box3" style="display: none;">
      <div id="msg_err3"></div>
  </div>
  <div class="top-notification-box" id="successmsgBx3" style="display: none;">
      <div id="msg_suc3"></div>
  </div>
  <div class="main-content-wrapper">
    <div class="row">
      <div class="col-sm-11 col-md-5 col-lg-4">
        <div class="main-content-box">
          
            <!-- start new design -->
                          <div class="header-holder with-step">
                            <div class="text-wrap">
                              <span class="step">1</span>
                              <strong class="title">Setup Your Target Audience </strong>
                            </div>
                          </div>
                          <div class="audience-setup-holder">
                            <div class="panel-group">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <strong class="panel-title">
                                   <a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                    by location
                                  </a>
                                  </strong>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                  <div class="panel-body">
                                    <div class="sk3 form-group">
                                    <input type="text" id="locations_autocomplete_include" value="" name="locations_autocomplete_include" class="form-control" placeholder="Add a country, state/province, city or ZIP..."/>
                        <input id="new_geo_locations" type="hidden" name="new_geo_locations" value="<?php echo $Cmpdraft->new_geo_locations; ?>">
                        <input id="avoid_geo_locations" type="hidden" name="avoid_geo_locations" value="<?php echo $Cmpdraft->avoid_geo_locations; ?>">
                        <input id="locations_autocomplete_include_hidden" type="hidden" name="locations_autocomplete_include_hidden" value="<?php echo $Cmpdraft->includelocation; ?>">
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <?php 
                                $interestArr = explode('#$#', $Cmpdraft->interest);
                                $new_interestsArr = explode('#$#', $Cmpdraft->new_interests);
                              ?>
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <strong class="panel-title">
                                  <a role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="true">
                                    BY INTEREST, DEMOGRAPHIC OR BEHAVIOUR
                                  </a>
                                  </strong>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse in">
                                  <div class="panel-body">
                                    <div class="dib-block">
                                      <div class="sk3 text-center form-group">
                                      	<input type="text" id="interests1" name="interests[]" placeholder="INTEREST, DEMOGRAPHIC OR BEHAVIOUR" class="form-control"/>
                                        <input id="new_interests1" type="hidden" name="new_interests[]" value='<?php echo $new_interestsArr[0]; ?>'>
                                        <?php
                                            $intestertext = str_replace('"', "", $interestArr[0]);
                                            $intestertextfinal = str_replace("'", "", $intestertext);
                                        ?>
                                        <input id="new_interests_hidden1" type="hidden" name="new_interests_hidden[]" value="<?php echo $intestertextfinal; ?>">
                                        
                                      </div>
                                      <div class="portlet box blue-hoki location-audi displayNone" id="suggestion_data1" >
                                          <div class="portlet-body form ">
                                              <div  >
                                                  <br />
                                                  <div class="col-md-12">
                                                      <!--interest-->
                                                      <div class="form-group">
                                                          <label class="control-label">Suggestions</label>
                                                          <div>
                                                              <div class="tag-cloud">
                                                                <div class="location-label" id="location-label1">
                                                                  
                                                                </div>
                                                              </div>
                                                              <div style="clear: both;"></div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="clearfix"></div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="browse-btn-holder">
                                        <a class="btn-browse" href="#"><span class="icon-folder"></span>Browse</a>
                                      </div>
                                    </div>
                                    
                                  </div>
                                </div>
                              </div>


                              <div  id="include_fileds">
                                <?php 
                                  if(!empty($interestArr[0])){
                                    $k=2;
                                    for($i=1; $i<=count($interestArr)-1; $i++){
                                      ?>
                                        <div class="panel panel-default">
                                          <div class="panel-heading">
                                            <strong class="panel-title">
                                              <a role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="true">
                                                AND INTEREST, DEMOGRAPHIC OR BEHAVIOUR
                                              </a>
                                            </strong>
                                          </div>
                                          <div id="collapseFour" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                              <div class="sk3 text-center form-group">
                                                <input type="text" id="interests<?php echo $k; ?>" name="interests[]" placeholder="INTEREST, DEMOGRAPHIC OR BEHAVIOUR" class="form-control"/>
                                                <input id="new_interests<?php echo $k; ?>" type="hidden" name="new_interests[]" value='<?php echo $new_interestsArr[$i]; ?>'>
                                                <input id="new_interests_hidden<?php echo $k; ?>" type="hidden" name="new_interests_hidden[]" value='<?php echo $interestArr[$i]; ?>'>
                                              </div>

                                              <div class="portlet box blue-hoki location-audi displayNone" id="suggestion_data<?php echo $k; ?>" >
                                                  <div class="portlet-body form ">
                                                      <div  >
                                                          <br />
                                                          <div class="col-md-12">
                                                              <!--interest-->
                                                              <div class="form-group">
                                                                  <label class="control-label">Suggestions</label>
                                                                  <div>
                                                                      <div class="tag-cloud"><ul></ul></div>
                                                                      <div style="clear: both;"></div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="clearfix"></div>
                                                      </div>
                                                  </div>
                                              </div>
                                              
                                              
                                            </div>
                                          </div>
                                        </div>
                                      <?php
                                    }
                                  }
                                ?>
                              </div>
                              <div  id="exlude_fileds">
                                <div class="panel panel-default" id="excludeInterestDiv" style="<?php if(!empty($Cmpdraft->interest_exclude)){ ?>display: block;<?php }else{ ?>display: none;<?php } ?>">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="true">
                                        OR INTEREST, DEMOGRAPHIC OR BEHAVIOUR
                                      </a>
                                    </strong>
                                  </div>
                                  <div id="collapseFour" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                      <div class="sk3 text-center form-group">
                                        <a class="delete-include_exlude-field add-relation" style="float:right;"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" id="interests_exclude1" name="interests_exclude" placeholder="INTEREST, DEMOGRAPHIC OR BEHAVIOUR" class="form-control"/>
                                        <input id="new_interests_exclude1" type="hidden" name="new_interests_exclude" value='<?php echo $Cmpdraft->new_interests_exclude; ?>'>
                                        <input id="new_interests_exclude_hidden1" type="hidden" name="new_interests_exclude_hidden" value='<?php echo $Cmpdraft->interest_exclude; ?>'>
                                      </div>

                                      <div class="portlet box blue-hoki location-audi displayNone" id="suggestion_data_exclude1" >
                                          <div class="portlet-body form ">
                                              <div  >
                                                  <br />
                                                  <div class="col-md-12">
                                                      <!--interest-->
                                                      <div class="form-group">
                                                          <label class="control-label">Suggestions</label>
                                                          <div>
                                                              <div class="tag-cloud"><ul></ul></div>
                                                              <div style="clear: both;"></div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="clearfix"></div>
                                              </div>
                                          </div>
                                      </div>
                                      
                                     
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <input type="hidden" name="interestsCount" id="interestsCount" value="<?php echo !empty($interestsCount) ? count($interestsCount) : !empty($interestArr) ? count($interestArr) : 1; ?>">
                              <input type="hidden" name="excludeCount" id="excludeCount" value="1">



                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <strong class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#collapseSix" aria-expanded="true">
                                    BY PLACEMENT
                                  </a>
                                  </strong>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse in">
                                  <div class="panel-body placement">
                                    <div class="panel-group">
                                      <div class="panel panel-default placement-panel clearfix">
                                        <div class="clearfix">
                                          <div class="panel-heading inline">
                                            <a class="btn btn-custom automatic-placement<?php if(strpos($Cmpdraft->placement, 'desktopfeed') !== false && strpos($Cmpdraft->placement, 'mobilefeed') !== false && strpos($Cmpdraft->placement, 'mobileexternal') !== false && strpos($Cmpdraft->placement, 'rightcolumn') !== false && strpos($Cmpdraft->placement, 'instagram') !== false) {echo " active";} else {} if(!isset($Cmpdraft->adobjective)) echo " active";?>" data-toggle="collapse" href="#" onclick="fnSelectPlacementvalall()">Automatic Placement</a>
                                          </div>
                                          <div class="panel-heading inline">
                                            <a class="btn btn-custom manual-placement collapsed<?php if(strpos($Cmpdraft->placement, 'desktopfeed') !== false && strpos($Cmpdraft->placement, 'mobilefeed') !== false && strpos($Cmpdraft->placement, 'mobileexternal') !== false && strpos($Cmpdraft->placement, 'rightcolumn') !== false && strpos($Cmpdraft->placement, 'instagram') !== false) {} else { if(isset($Cmpdraft->adobjective)) { echo " active"; } }?>" data-toggle="collapse" href="#manualPalacement" aria-expanded="false">Manual Placement</a>
                                          </div>
                                        </div>
                                        <div id="manualPalacement" class="panel-collapse collapse<?php if(strpos($Cmpdraft->placement, 'desktopfeed') !== false && strpos($Cmpdraft->placement, 'mobilefeed') !== false && strpos($Cmpdraft->placement, 'mobileexternal') !== false && strpos($Cmpdraft->placement, 'rightcolumn') !== false && strpos($Cmpdraft->placement, 'instagram') !== false) {} else { if(isset($Cmpdraft->adobjective)) { echo " in";} }?>" <?php if(strpos($Cmpdraft->placement, 'desktopfeed') !== false && strpos($Cmpdraft->placement, 'mobilefeed') !== false && strpos($Cmpdraft->placement, 'mobileexternal') !== false && strpos($Cmpdraft->placement, 'rightcolumn') !== false && strpos($Cmpdraft->placement, 'instagram') !== false) {} else { if(isset($Cmpdraft->adobjective)) { echo "aria-expanded='true' style='height:auto;'"; } else { echo "aria-expanded='false' style='height: 0px;'"; } }?>>
                                          <div class="panel-body placement-wrap">
                                            <div class="placement-holder">
                                              <div class="title">Choose Your Manual Placements</div>
                                              <ul class="placement-list">
                                                <li>
                                                <a class="placement-item2 <?= (strpos($Cmpdraft->placement, 'desktopfeed') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet','desktopFeed')" id="desktopFeed">
                                                  <input type="hidden" name="page_types[]" id="desktop_news_feed" value="<?= (strpos($Cmpdraft->placement, 'desktopfeed') !== false) ? "desktopfeed" : "" ?>">Desktop NewsFeed
                                                </a>
                                                <!-- <a class="placement-item2" href="#">Desktop NewsFeed</a> --></li>
                                                <li>
                                                <a class="placement-item2 mobile <?= (strpos($Cmpdraft->placement, 'mobilefeed') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet', 'mobileFeed')" id="mobileFeed">
                                                  <input type="hidden" name="page_types[]" id="mobile_news_feed" value="<?= (strpos($Cmpdraft->placement, 'mobilefeed') !== false) ? "mobilefeed" : "" ?>">Mobile NewsFeed
                                                </a><!--<a class="placement-item2" href="#">Mobile NewsFeed</a>--></li>
                                                <li <?= "PAGE_LIKES" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?><?= "LEAD_GENERATION" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?><?= "POST_ENGAGEMENT" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?>>
                                                <a class="placement-item2 mobile <?= (strpos($Cmpdraft->placement, 'mobileexternal') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet', 'rdpartyFeed')" id="rdpartyFeed">
                                                  <input type="hidden" name="page_types[]" id="party_mobile_site" value="<?= (strpos($Cmpdraft->placement, 'mobileexternal') !== false) ? "mobileexternal" : "" ?>">Mobile 3rd Party
                                                </a><!--<a class="placement-item2" href="#">Mobile 3rd Party</a>--></li>
                                                <li <?= "LEAD_GENERATION" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?>>
                                                <a class="placement-item2 <?= (strpos($Cmpdraft->placement, 'rightcolumn') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet', 'desktopRHS')" id="desktopRHS">
                                                  <input type="hidden" name="page_types[]" id="desktop_right_hand_side" value="<?= (strpos($Cmpdraft->placement, 'rightcolumn') !== false) ? "rightcolumn" : "" ?>">Desktop Right Side
                                                </a>
                                                <!--<a class="placement-item2" href="#">Desktop Right Side</a>--></li>
                                                <li <?= "PAGE_LIKES" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?><?= "LEAD_GENERATION" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?><?= "POST_ENGAGEMENT" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?>>
                                                <a class="placement-item2 instagram <?= (strpos($Cmpdraft->placement, 'instagram') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet', 'instagramPla')" id="instagramPla">
                                              <input type="hidden" name="page_types[]" id="instagram_feed" value="<?= (strpos($Cmpdraft->placement, 'instagram') !== false) ? "instagram" : "" ?>">Instagram Feed
                                            </a>
                                    <!--<a class="placement-item2" href="#">Instagram Feed</a>--></li>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="more-options choosen">
                                  <div class="toggle-wrap">
                                    <label class="switch-button">
                                      <input type="checkbox" id="more-options">
                                      <span class="fake-toggle">
                                        <span class="switch">&nbsp;</span>
                                      </span>
                                    </label>
                                    <span class="label-text">More Targeting Options?</span>
                                  </div>
                                  <div class="btn-holder">
                                   <!-- <a href="<?php //echo $this->config->item('base_url'); ?>helps" class="btn-help"><span class="icon-video"></span>Need Help?</a> -->
                                    <a href="javascript:;" class="btn-help bmd-modalButton" data-toggle="modal" data-bmdSrc="https://www.youtube.com/embed/IrwttJN75Ig" data-bmdWidth="640" data-bmdHeight="480" data-target="#myModaliframe2"  data-bmdVideoFullscreen="true"><span class="icon-video"></span>Need Help?</a>
                    
                   <!-- <button type="button" class="bmd-modalButton" data-toggle="modal" data-bmdSrc="https://www.youtube.com/embed/cMNPPgB0_mU" data-bmdWidth="640" data-bmdHeight="480" data-target="#myModaliframe"  data-bmdVideoFullscreen="true">Youtube</button> -->
                    <div class="modal fade" id="myModaliframe2">
                        <div class="modal-dialog">
                            <div class="modal-content bmd-modalContent">
                
                                <div class="modal-body">
                          
                          <div class="close-button">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          </div>
                          <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" frameborder="0"></iframe>
                          </div>
                                </div>
                
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                                    
                                    
                                   </div>
                                </div>
                              </div>
                              <div class="more-option-holder">
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="true">
                                    BY GENDER
                                  </a>
                                    </strong>
                                  </div>
                                  <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body gender">
                                      <div class="gender-label">
                                        <span class="a <?= ($Cmpdraft->gender == '1') ? "active" : "" ?>" onclick="fnSelectval('1', 'genders', 'genderMen')" id="genderMen">Men Only</span>
                        <span class="b <?= ($Cmpdraft->gender == '2') ? "active" : "" ?>" onclick="fnSelectval('2', 'genders', 'genderWomen')" id="genderWomen">Women Only</span>
                        <span class="c <?= ($Cmpdraft->gender == '') ? "active" : "" ?>" onclick="fnSelectval('', 'genders', 'genderAll')" id="genderAll">Both</span>
                        <input  id="genders" name="genders" value=""  type="hidden">
                                        
                                        <!-- <span class="d">Split Test</span> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="true">
                                        BY AGE
                                      </a>
                                    </strong>
                                  </div>
                                  <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body age" id="ageyear_fileds">
                                      <?php 
                                          $fromArr = explode(',', $Cmpdraft->agegroupfrom);
                                          $toArr = explode(',', $Cmpdraft->agegroupto);
                                      ?>
                                      <?php 
                                      if(!empty($fromArr[0])){
                                          for($i=0; $i<=count($fromArr)-1; $i++){
                                            ?>
                                                <div class="age-group" style="clear:left;">
                                                  <div class="input-group spin1" id="spinner_from<?php echo $i+1; ?>">
                                                    <input type="text" placeholder="18" class="form-control" maxlength="3" onkeypress="return isNumber(event)"  placeholder="form..." maxlength="2" value="<?= $fromArr[$i] ?>"  max="64" min="13" name="form" id="form">
                                                    <div class="input-group-addon">Years</div>
                                                    <div class="spinner-buttons input-group-btn btn-group-vertical">
                                                        <button type="button" class="btn spinner-up btn-xs blue">
                                                            <i class="fa fa-angle-up"></i>
                                                        </button>
                                                        <button type="button" class="btn spinner-down btn-xs blue">
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                    </div>
                                                  </div>
                                                  <span class="to">
                                                    <i class="icon-share"></i>
                                                  </span>
                                                  <div class="input-group spin2" id="spinner_to<?php echo $i+1; ?>">
                                                    <input type="text" placeholder="65+" class="form-control" maxlength="3" name="to" id="to" placeholder="to..." onkeypress="return isNumber(event)" maxlength="2" min="14" max="65+" value="<?= $toArr[$i] ?>" max="65" min="13">
                                                    <div class="input-group-addon">Years</div>
                                                    <div class="spinner-buttons input-group-btn btn-group-vertical">
                                                        <button type="button" class="btn spinner-up btn-xs blue">
                                                            <i class="fa fa-angle-up"></i>
                                                        </button>
                                                        <button type="button" class="btn spinner-down btn-xs blue">
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                    </div>
                                                  </div>
                                                  <a href="javascript:void(0)" class="add-age" id="more_timetable_fields"><i class="icon-plus"></i></a>
                                                </div>
                                            <?php
                                          }
                                      }
                                      else{
                                      ?>
                                          <div class="age-group">
                                            <div class="input-group spin1" id="spinner11">
                                              <input type="text" placeholder="18" class="form-control spinner-input" maxlength="3" placeholder="form..." maxlength="2" value="<?= !empty($Cmpdraft->agegroupfrom) ? $Cmpdraft->agegroupfrom : '18' ?>"  max="64" min="13" name="form" id="form">
                                              <div class="input-group-addon">Years</div>
                                              <div class="spinner-buttons input-group-btn btn-group-vertical">
                                                  <button type="button" class="btn spinner-up btn-xs blue">
                                                      <i class="fa fa-angle-up"></i>
                                                  </button>
                                                  <button type="button" class="btn spinner-down btn-xs blue">
                                                      <i class="fa fa-angle-down"></i>
                                                  </button>
                                              </div>
                                            </div>
                                            <span class="to">
                                              <i class="icon-share"></i>
                                            </span>
                                            <div class="input-group spin2" id="spinner21">
                                              <input type="text" placeholder="65+" class="form-control spinner-input" maxlength="3" name="to" id="to" placeholder="to..." onkeypress="return isNumber(event)" maxlength="2" min="14" max="65" value="<?= !empty($Cmpdraft->agegroupto) ? $Cmpdraft->agegroupto : '65+' ?>">
                                              <div class="input-group-addon">Years</div>
                                              <div class="spinner-buttons input-group-btn btn-group-vertical">
                                                  <button type="button" class="btn spinner-up btn-xs blue">
                                                      <i class="fa fa-angle-up"></i>
                                                  </button>
                                                  <button type="button" class="btn spinner-down btn-xs blue">
                                                      <i class="fa fa-angle-down"></i>
                                                  </button>
                                              </div>
                                            </div>
                                            <a href="javascript:void(0)" class="add-age" id="more_timetable_fields"><i class="icon-plus"></i></a>
                                          </div>
                                      <?php }
                                      ?>
                                      <input type="hidden" name="adeyearcounter" id="adeyearcounter" value="<?php echo count($fromArr); ?>">
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseSeven" aria-expanded="true">
                                        BY RELATIONSHIP STATUS
                                      </a>
                                    </strong>
                                  </div>
                                  <div id="collapseSeven" class="panel-collapse collapse in">
                                    <div class="panel-body" id="relationship_fileds">
                                      <?php 
                                          $realationshipArr = explode(',', $Cmpdraft->realationship);
                                      ?>
                                      <?php 
                                        if(!empty($realationshipArr[0])){
                                          for($i=0; $i<=count($realationshipArr)-1; $i++){
                                            ?>
                                              <div class="select-wrap">
                                                <select id="relationship" name="relationship_statuses[]">
                                                  <option >Relationship Status</option>
                                                  <option value="6" <?php if($realationshipArr[$i] == 6) { echo 'selected'; }?>>All</option>
                                                  <option value="1" <?php if($realationshipArr[$i] == 1) { echo 'selected'; }?>>Single</option>
                                                  <option value="2" <?php if($realationshipArr[$i] == 2) { echo 'selected'; }?>> In a Relationship</option>
                                                  <option value="3" <?php if($realationshipArr[$i] == 3) { echo 'selected'; }?>>Engaged</option>
                                                  <option value="4" <?php if($realationshipArr[$i] == 4) { echo 'selected'; }?>>Married</option>
                                                  <option value="5" <?php if($realationshipArr[$i] == 5) { echo 'selected'; }?>>Not Specified</option>
                                                </select>
                                                <a href="javascript:void(0)" class="delete-relationship-field add-relation"><i class="fa fa-trash-o"></i></a>
                                              </div>
                                            <?php 
                                          }
                                        }
                                        else{
                                      ?>
                                      <div class="select-wrap">
                                        <select id="relationship" name="relationship_statuses[]">
                                          <option selected>Relationship Status</option>
                                          <option value="" selected>All</option>
                                          <option value="1">Single</option>
                                          <option value="2"> In a Relationship</option>
                                          <option value="3">Engaged</option>
                                          <option value="4">Married</option>
                                          <option value="5">Not Specified</option>
                                        </select>
                                        <a href="javascript:void(0)" class="add-relation" id="more_relationship_fields"><i class="icon-plus"></i></a>
                                      </div>
                                      <?php } ?>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseEight" aria-expanded="true">
                                        BY EDUCATION
                                      </a>
                                    </strong>
                                  </div>
                                  <div id="collapseEight" class="panel-collapse collapse in">
                                    <div class="panel-body" id="education_fileds">
                                      <?php 
                                          $educationArr = explode(',', $Cmpdraft->education);
                                      ?>
                                      <?php 
                                        if(!empty($educationArr[0])){
                                          for($i=0; $i<=count($educationArr)-1; $i++){
                                            ?>
                                              <div class="select-wrap">
                                                <select id="education" name="education_statuses[]">
                                                  <option value="" selected>Education Level</option>
                                                  <option value="14" <?php if($educationArr[$i] == 6) {echo 'selected'; }?>>Education Level</option>
                                                  <option value="1" <?php if($educationArr[$i] == 1) {echo 'selected'; }?>> High School</option>
                                                  <option value="2" <?php if($educationArr[$i] == 2) {echo 'selected'; }?>> Undergrad</option>
                                                  <option value="3" <?php if($educationArr[$i] == 3) {echo 'selected'; }?>> Alum</option>
                                                  <option value="4" <?php if($educationArr[$i] == 4) {echo 'selected'; }?>> High School Grad</option>
                                                  <option value="5" <?php if($educationArr[$i] == 5) {echo 'selected'; }?>> Some College</option>
                                                  <option value="6" <?php if($educationArr[$i] == 6) {echo 'selected'; }?>> Associate Degree</option>
                                                  <option value="7" <?php if($educationArr[$i] == 7) {echo 'selected'; }?>> In Grad School</option>
                                                  <option value="8" <?php if($educationArr[$i] == 8) {echo 'selected'; }?>> Some Grad School</option>
                                                  <option value="9" <?php if($educationArr[$i] == 9) {echo 'selected'; }?>> Master Degree</option>
                                                  <option value="10" <?php if($educationArr[$i] == 10) {echo 'selected'; }?>> Professional Degree</option>
                                                  <option value="11" <?php if($educationArr[$i] == 11) {echo 'selected'; }?>> Doctorate Degree</option>
                                                  <option value="12" <?php if($educationArr[$i] == 12) {echo 'selected'; }?>> Unspecified</option>
                                                  <option value="13" <?php if($educationArr[$i] == 13) {echo 'selected'; }?>> Some High School</option>
                                                </select>
                                                <a href="javascript:void(0)" class="delete-education-field add-relation"><i class="fa fa-trash-o"></i></a>
                                              </div>
                                            <?php 
                                          }
                                        }
                                        else{
                                      ?>
                                        <div class="select-wrap">
                                          <select id="education" name="education_statuses[]">
                                            <option value="" selected>Education Level</option>
                                            <option value="1"> High School</option>
                                            <option value="2"> Undergrad</option>
                                            <option value="3"> Alum</option>
                                            <option value="4"> High School Grad</option>
                                            <option value="5"> Some College</option>
                                            <option value="6"> Associate Degree</option>
                                            <option value="7"> In Grad School</option>
                                            <option value="8"> Some Grad School</option>
                                            <option value="9"> Master Degree</option>
                                            <option value="10"> Professional Degree</option>
                                            <option value="11"> Doctorate Degree</option>
                                            <option value="12"> Unspecified</option>
                                            <option value="13"> Some High School</option>
                                          </select>
                                          <a href="javascript:void(0)" class="add-relation" id="more_education_fields"><i class="icon-plus"></i></a>
                                        </div>
                                        <?php } ?>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseNine" aria-expanded="true">
                                        BY LANGUAGE
                                      </a>
                                    </strong>
                                  </div>
                                  <div id="collapseNine" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                      <div class="sk3 text-center form-group">
                                        <input type="text" id="locales_autocomplete" placeholder="LANGUAGE" name="locales_autocomplete" class="form-control"/>
                                        <input id="new_ad_langauage" type="hidden" name="new_ad_langauage" value="<?php echo $Cmpdraft->new_ad_langauage; ?>">
                                        <input id="new_ad_langauage_hidden" type="hidden" name="new_ad_langauage_hidden" value="<?php echo $Cmpdraft->language; ?>">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseTen" aria-expanded="true">
                                        BY WORKPLACE
                                      </a>
                                    </strong>
                                  </div>
                                  <?php 
                                  $employersStr = '';
                                  if(!empty($Cmpdraft->employers)){
                                      $employersArr = explode(',', $Cmpdraft->employers);
                                      for ($i=0; $i <= count($employersArr)-1 ; $i++) { 
                                          $employersStr .= '{"id":'.$employersArr[$i].'},';
                                      }
                                      $employersStr = rtrim($employersStr, ',');
                                  }
                                  ?>
                                  <div id="collapseTen" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                      <div class="sk3 text-center form-group">
                                        <input type="text" name="employers_autocomplete" placeholder="WORKPLACE" id="employers_autocomplete" class="form-control"/>
                                        <input id="new_work_employers" type="hidden" name="new_work_employers" value='<?php echo $employersStr; ?>'>
                                        <input id="new_work_employers_hidden" type="hidden" name="new_work_employers_hidden" value='<?php echo $Cmpdraft->new_work_employers; ?>'>
                                        <input id="new_work_employers_id_hidden" type="hidden" name="new_work_employers_id_hidden" value='<?php echo $Cmpdraft->new_work_id_employers; ?>'>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseeleven" aria-expanded="true">
                                        BY JOB TITLE
                                      </a>
                                    </strong>
                                  </div>
                                  <?php 
                                  $workpositionsStr = '';
                                  if(!empty($Cmpdraft->workpositions)){
                                      $workpositionsArr = explode(',', $Cmpdraft->workpositions);
                                      for ($i=0; $i <= count($workpositionsArr)-1 ; $i++) { 
                                          $workpositionsStr .= '{"id":'.$workpositionsArr[$i].'},';
                                      }
                                      $workpositionsStr = rtrim($workpositionsStr, ',');
                                  }
                                  ?>
                                  <div id="collapseeleven" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                      <div class="sk3 text-center form-group">
                                        <input type="text" name="job_autocomplete" id="job_autocomplete" placeholder="JOB TITLE" class="form-control"/>
                                        <input id="new_work_positions" type="hidden" name="new_work_positions" value='<?php echo $workpositionsStr; ?>'>
                                        <input id="new_work_positions_hidden" type="hidden" name="new_work_positions_hidden" value='<?php echo $Cmpdraft->new_work_positions; ?>'>
                                        <input id="new_work_positions_id_hidden" type="hidden" name="new_work_positions_id_hidden" value='<?php echo $Cmpdraft->new_work_id_positions; ?>'>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <strong class="panel-title">
                                      <a role="button" data-toggle="collapse" href="#collapseTwelve" aria-expanded="true">
                                        BY CUSTOM AUDIENCE
                                      </a>
                                    </strong>
                                  </div>
                                  <div id="collapseTwelve" class="panel-collapse collapse in">
                                    <div class="panel-body" id="custom_audience_fileds">
                                    <div class="select-wrap">
                                          <select id="mobile_devices" class="mobile_devices" name="custom_audiences[]">
                                          <option value="" selected>CUSTOM AUDIENCE</option>
                                          <?php if(!empty($customaudiences)){
                                                  foreach ($customaudiences->data as $key => $result) {
                                                    
                                                    ?>
                                                      <option <?php echo $sel; ?> value='<?php echo $result->id . '##' . $result->name; ?>'><?php echo $result->name;?></option>
                                                    <?php
                                                  }
                
                                                }?>
                                          </select>
                                          <a href="javascript:void(0)" class="add-relation" id="more_custom_fields"><i class="icon-plus"></i></a>
                                        </div>
                                      <?php 
                                          $custom_audienceArr = explode('@', $Cmpdraft->custom_audience);
                                      ?>
                                      <?php 
									  
                                        if(!empty($custom_audienceArr[1])){
											
                                          for($i=0; $i<=count($custom_audienceArr)-1; $i++){
											  if(!empty($custom_audienceArr[$i])){
                                          ?>
                                            <div class="select-wrap">
                                            
                                              <select id="mobile_devices" class="mobile_devices" name="custom_audiences[]">
                                                <option value="" selected>CUSTOM AUDIENCE</option>
                                                <?php if(!empty($customaudiences)){
                                                  foreach ($customaudiences->data as $key => $result) {
                                                    $sel = '';
													echo $result->id . "##" . $result->name;
													echo $custom_audienceArr[$i];
                                                    if($custom_audienceArr[$i] == $result->id . "##" . $result->name){
                                                      $sel = 'selected';
                                                    }
                                                    ?>
                                                      <option <?php echo $sel; ?> value='<?php echo $result->id . '##' . $result->name; ?>'><?php echo $result->name;?></option>
                                                    <?php
                                                  }
                
                                                }?>
                                              </select>
                                              <a href="javascript:void(0)" class="delete-custom-audience-field add-relation"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                          <?php 
											  }
                                          }
                                        }
                                        else{
                                      ?>
                                        <!--<div class="select-wrap">
                                          <select id="mobile_devices" class="mobile_devices" name="custom_audiences[]">
                                          <option value="" selected>CUSTOM AUDIENCE</option>
                                          </select>
                                          <a href="javascript:void(0)" class="add-relation" id="more_custom_fields"><i class="icon-plus"></i></a>
                                        </div>-->
                                      <?php } ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <!-- end new design -->
          
        </div>
      </div>      
     
      <div class="col-sm-11 col-md-6 col-md-offset-1 col-lg-5 col-lg-offset-2">
                      <div class="fixed-box">
                        <div class="setup-preview-box">
                          <div class="header-holder with-step">
                            <div class="text-wrap">
                              <span class="step">2</span>
                              <strong class="title">Split Test Your Target Audience</strong>
                            </div>
                            <div class="btn-refresh-holder">
                              <span class="btn-refresh" onclick="createAdSets()"><i class="icon-refresh"></i> Refresh</span>
                            </div>
                          </div>
                          <strong class="title-text">WHAT WOULD YOU LIKE TO SPLIT TEST?</strong>
                          
                          
                           <div class="check-items-wrap" style="opacity:0;height:0px;">
                              <div class="checkbox checkbox-success">
                                <input type="checkbox" name="split_adset[]" value="location" <?=(strpos($Cmpdraft->split_adset, 'location') !== false)?"checked":""?> id="locationcheck">
                                <label for="locationcheck">
                                  Location
                                </label>
                              </div>
                              <div class="checkbox checkbox-success">
                                <input type="checkbox" name="split_adset[]" value="genders" <?=(strpos($Cmpdraft->split_adset, 'genders') !== false)?"checked":""?> id="gendercheck">
                                <label for="gendercheck">
                                  Gender
                                </label>
                              </div>
                              <div class="checkbox checkbox-success">
                                <input type="checkbox" name="split_adset[]" value="interests" <?=(strpos($Cmpdraft->split_adset, 'interests') !== false)?"checked":""?> id="intrestcheck">
                                <label for="intrestcheck">
                                  Interest, demographic, behaviour
                                </label>
                              </div>
                              <div class="checkbox checkbox-success">
                                <input type="checkbox" name="split_adset[]" value="placement" <?=(strpos($Cmpdraft->split_adset, 'placement') !== false)?"checked":""?> id="placementcheck">
                                <label for="placementcheck">
                                  Placement
                                </label>
                              </div>
                              <div class="checkbox checkbox-success">
                                <input type="checkbox" name="split_adset[]" value="custom_audiences" <?=(strpos($Cmpdraft->split_adset, 'custom_audiences') !== false)?"checked":""?> id="caudiencecheck">
                                <label for="caudiencecheck">
                                  Custom Audience
                                </label>
                              </div> 
                            </div>
                          <?php //if($locattiionArr[0] != ""){ echo count($locattiionArr); } else { echo "0";}?>
                          <div class="selected-option">
                            <div class="holder">
                            <?php 
                            $locattiionArr = explode('$#$', $Cmpdraft->new_geo_locations);

                            if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                              <a class="btn-selected-option <?=(strpos($Cmpdraft->split_adset, 'location') !== false)?"selected":""?>" href="javascript:void(0)" onclick='activatesplitoption("locationcheck");' id="locationchecka">Location <span>(<span id="locationcounter">0</span> selected )</span></a>
                              <a class="btn-selected-option <?=(strpos($Cmpdraft->split_adset, 'genders') !== false)?"selected":""?>" href="javascript:void(0)" onclick='activatesplitoption("gendercheck");' id="genderchecka">Gender</a>
                              <a class="btn-selected-option <?=(strpos($Cmpdraft->split_adset, 'interests') !== false)?"selected":""?>" href="javascript:void(0)" onclick='activatesplitoption("intrestcheck");' id="intrestchecka">Interest, Demographic, Behaviour <span>(<span id="interestcounter">0</span> selected )</span></a>
                              <a class="btn-selected-option <?=(strpos($Cmpdraft->split_adset, 'placement') !== false)?"selected":""?>" href="javascript:void(0)" onclick='activatesplitoption("placementcheck");' id="placementchecka">Placement <span>(<span id="placementcounter">0</span> selected )</span></a>
                              <a class="btn-selected-option <?=(strpos($Cmpdraft->split_adset, 'custom_audiences') !== false)?"selected":""?>" href="javascript:void(0)" onclick='activatesplitoption("caudiencecheck");'id="caudiencechecka">Custom Audience<span>(<span id="custacounter">0</span> selected )</span></a>
                              <?php } else { ?>
                              <a class="btn-selected-option" href="javascript:void(0)" onclick='showupgradepopup();' id="locationchecka">Location <span>(0 selected)</span></a>
                              <a class="btn-selected-option" href="javascript:void(0)" onclick='showupgradepopup();' id="genderchecka">Gender <span>(0 selected)</span></a>
                              <a class="btn-selected-option" href="javascript:void(0)" onclick='showupgradepopup();' id="intrestchecka">Interest, Demographic, Behaviour <span>(0 selected)</span></a>
                              <a class="btn-selected-option" href="javascript:void(0)" onclick='showupgradepopup();' id="placementchecka">Placement <span>(0 selected)</span></a>
                              <a class="btn-selected-option" href="javascript:void(0)" onclick='showupgradepopup();'id="placementchecka">Custom Audience<span>(0 selected)</span></a>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                        <div class="setup-preview-box">
                          <div class="header-holder with-step refresh-header">
                            <div class="text-wrap">
                              <span class="step">3</span>
                              <strong class="title">Preview Your Audience Sizes</strong>
                            </div>
                            <div class="btn-refresh-holder">
                              <span class="btn-refresh" onclick="createAdSets()"><i class="icon-refresh"></i> Refresh</span>
                            </div>
                          </div>
                          <strong class="title-text"><span class="text">NUMBER OF ADSETS YOU WILL CREATE</span><span  class="count" id="audiencesCount">0 ADSETS</span></strong>
                          <strong class="title-text" style="display:none;"><span class="text">Estimate Audience Size</span><span  class="count" id="totalAudienceCount">0 people</span></strong>
                          <div id="feedloader_adset" class="feedloader text-center displayNone">
                                <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
                            </div>
                          <div class="table-responsive">
                            <table class="table">
                            <colgroup>
                              <col class="col1">
                              <col class="col2">
                              <col class="col3">
                            </colgroup>
                            <tbody id="audiencestableresult" style="display: none;">
                              
                            </tbody>
                          </table>
                          <div class="heart">
                            <div class="drft_msg" style="display: none;">The draft has been  saved successfully.</div>
                          </div>
                          <!-- <div class="heart">
                            <a href="#" class="btn btn-load"><i class="icon-pencil"></i> Load Template</a>
                            
                            <a href="javascript:void(0)" class="btn btn-save pull-right" onclick="ad_design_darafts('loader_drft2')"><i class="icon-heart"></i> Save as template for later use</a> 
                          </div> -->
                        </div>
                        <div class="adsets-status" id="adset_status">
                            Select at least one location OR custom audience to get started
                            <!--<span class="btn-refresh"><i class="icon-refresh"></i> Refresh</span>-->
                          </div>
                        </div>
                        <div class="button-main">
            <a class="btn btn-info" href="javascript:void(0)" onclick="jQuery('#tab_1_3').removeClass('active');jQuery('#tab_1_2').addClass('active');">Back to objective</a>
            <button class="btn btn-success pull-right" type="button" id="SaveAudienceId" onclick="ad_design_darafts('loader_drft2', 'direct');SaveAudience();">Go To Budgeting</button>
          </div>
                        <!-- <div class="button-main center">
                          <button class="btn btn-info" type="button">Continue to Next Step</button>
                        </div> -->
                      </div>
                    </div>
    </div>
  </div>
</div>