<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Scale AdSet
        <?php
        if (isset($adaccountsCount) && $adaccountsCount == 1) {
            echo $adaccounts['adset']['adsetName'];
        }
        ?>
    </h4>
</div>
<div class="modal-body adset-modal">
    <div class="row">
        <div class="col-lg-12 adset-modal">
            <form class="bs-example bs-example-form" data-example-id="simple-input-groups"> 
                <div class="form-group overflow-none">


                    <div class="col-md-8 form-inline">
                        <label class="budget1">Budget *</label>
                        <div class="form-group">
                            <div class="input-group"> 
                                <?php
                                $amount = 0;
                                $checked = "";
                                $current_cur_code = $this->session->userdata('cur_code');
                                /*if (isset($adaccounts['adset']['daily_budget']) && $adaccounts['adset']['daily_budget'] > 0) {
                                    $amount = $adaccounts['adset']['daily_budget'] / 100;
                                } else if (isset($adaccounts['adset']['lifetime_budget']) && $adaccounts['adset']['lifetime_budget'] > 0) {
                                    $amount = $adaccounts['adset']['lifetime_budget'] / 100;
                                }*/
                                if (isset($adaccounts['daily_budget']) && $adaccounts['daily_budget'] > 0) {
                                    if($current_cur_code == "VND" || $current_cur_code == "TWD" || $current_cur_code == "CLP" || $current_cur_code == "COP" || $current_cur_code == "CRC" || $current_cur_code == "HUF" || $current_cur_code == "ISK" || $current_cur_code == "IDR" || $current_cur_code == "JPY" || $current_cur_code == "KRW" || $current_cur_code == "PYG"){
                                        $amount = $adaccounts['daily_budget'];
                                    }
                                    else{
                                        $amount = $adaccounts['daily_budget'] / 100;
                                    }
                                } else if (isset($adaccounts['lifetime_budget']) && $adaccounts['lifetime_budget'] > 0) {
                                    if($current_cur_code == "VND" || $current_cur_code == "TWD" || $current_cur_code == "CLP" || $current_cur_code == "COP" || $current_cur_code == "CRC" || $current_cur_code == "HUF" || $current_cur_code == "ISK" || $current_cur_code == "IDR" || $current_cur_code == "JPY" || $current_cur_code == "KRW" || $current_cur_code == "PYG"){
                                        $amount = $adaccounts['lifetime_budget'];
                                    }
                                    else{
                                        $amount = $adaccounts['lifetime_budget'] / 100;
                                    }
                                }
                                //print_r($adaccounts);
                                ?>
                                <style>
                                    .input-group-addon1 {
                                        display: table-cell;
                                        min-width: 47px;
                                        padding-left: 15px;
                                        vertical-align: middle;
                                    }
                                </style>
                                <input type="number" onkeypress="return isNumber(event)" name="budgetAmount" value="<?php echo $amount; ?>" min="5" id="InputAmount" class="form-control">
                                <!--<span class="input-group-addon" id="basic-addon2"><i class="fa fa-dollar"></i> </span>-->
                                <span class="input-group-addon" id="basic-addon2"><?php echo $this->session->userdata('cur_currency'); ?> </span>
                                <?php //$current_cur_code = $this->session->userdata('cur_code'); ?>
                                <span class="input-group-addon1">per day</span> 
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-md-12 form-inline">
                        <label class="budget1"><p>Budget type</p></label>
                        <div class="list-btn list-radios">
                            <span class="btn-radio-checks">
                               
                                <input type="radio" <?php
                                if (isset($adaccounts['adset']['lifetime_budget']) && $adaccounts['adset']['lifetime_budget'] > 0) {
                                    echo 'checked="checked"';
                                }
                                ?> value="lifetime_budget" name="budget" id="budget1">
                                <label for="budget1" class="btn">Total</label>
                            </span>
                            <span class="btn-radio-checks">
                                <?php // var_dump($adaccounts['adset']['lifetime_budget']);?>
                                <input type="radio" <?php
                                if (isset($adaccounts['adset']['daily_budget']) && $adaccounts['adset']['daily_budget'] > 0) {
                                    echo 'checked="checked"';
                                }
                                ?> value="daily_budget" name="budget" id="budget2">
                                <label for="budget2" class="btn">Per day</label>
                            </span>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                            <p><label class="budget1">End Date </label></p> 
                            <div class="input-group input-xlarge date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                <input type="text" class="form-control" id="adset_end_date" name="adset_end_date" value="">
                            </div>
                    </div>-->
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-success" onclick="saveAdsetScale('<?php echo $adaccounts['id']; ?>');">Save</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>