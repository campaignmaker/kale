<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 3.0.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php SeoDetail('Title'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="<?php SeoDetail('Description'); ?>" name="description"/>
        <meta content="The Campaign Maker" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo $this->config->item('assets'); ?>global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>global/plugins/select2/select2-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo $this->config->item('assets'); ?>global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="<?php echo $this->config->item('assets'); ?>admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->item('assets'); ?>admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>/assets/img/favicon.ico">
    </head>
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="logo">
                        <a href="index.html">
                            <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/logo.png" alt=""/>
                        </a>
                    </div>
                </div>
                <div class="col-md-6  col-sm-6">
                    <div class="loginmenu">
                        <ul>
                            <li><a href="<?php echo base_url(); ?>">Tour</a></li>
                            <li><a href="<?php echo base_url().'pricing'; ?>">Pricing</a></li>
                            <li><a href="<?php echo base_url().'register'; ?>">Sign Up</a></li>
                            <li class="active"><a href="<?php echo base_url().'login'; ?>">Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGIN -->
        <div class="content" style="margin-top: 80px;margin-bottom: 120px;">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo base_url();?>forgotpassword/update-password" method="post">
                <h3 align="center" class="form-title">Enter new password</h3>
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span>
                            <?= validation_errors() ?> </span>
                    </div>
                <?php endif; ?>
                <?php if (isset($error)) : ?>
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span>
                            <?= $error ?> </span>
                    </div>
                <?php endif; ?>
                <?php if (isset($success)) : ?>
                    <div class="alert alert-success">
                        <button class="close" data-close="alert"></button>
                        <span>
                            <?= $success; ?> </span>
                    </div>
                <?php endif; ?>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Retype New Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="rpassword"/>
                    </div>
                </div>
                <input type="hidden" name="hash" value="<?=@$hash;?>" />
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn">
                        <i class="m-icon-swapleft"></i> Back </button>
                    <button type="submit" class="btn green pull-right">
                        Submit <i class="m-icon-swapright m-icon-white"></i>
                    </button>
                </div>
                <div class="forget-password">
                    <h4>Want to login?</h4>
                    <p>
                         <a href="<?php echo base_url().'login'; ?>" id="forget-password">
                            Login Now! </a>
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright">
            2016 &copy; The Campaign Maker.
        </div>
        <div class="footer_bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer_bottom_link">
                            <ul>
                                <li><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
                                <li><a href="mailto:samy@thecampaignmaker.com">Contact Us</a></li>
                                <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
                                <li><a href="<?php echo base_url(); ?>terms-of-services">Terms &amp; Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <!-- END COPYRIGHT -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
                <script src="<?php echo $this->config->item('assets'); ?>global/plugins/respond.min.js"></script>
                <script src="<?php echo $this->config->item('assets'); ?>global/plugins/excanvas.min.js"></script> 
                <![endif]-->
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo $this->config->item('assets'); ?>global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('assets'); ?>global/plugins/select2/select2.min.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo $this->config->item('assets'); ?>global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->item('assets'); ?>admin/pages/scripts/login.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Login.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>