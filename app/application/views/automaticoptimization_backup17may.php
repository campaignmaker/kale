<?php
	echo "<pre>";
	print_r($adruleslibrarydata);
	echo "</pre>";
    echo $adruleslibrarydata[0]['evaluation_spec'][4]['field'];
	exit;
?>
 <main id="main">
    <div class="dashboard-header">
      <ol class="breadcrumb">
        <li><a href="#">Automatic Rules</a></li>
        <?php if (isset($adaccountsCount) && $adaccountsCount > 1) { ?>

        <?php }else{?>
          <li class="active"><?php echo $accountName; ?></li>
        <?php } ?>
      </ol>
    </div>
    <div id="preloaderMain" style="display: none;">
            <div id="statusMain"><i class="fa fa-spinner fa-spin"></i></div>
        </div>
    

<?php if (isset($adaccountsCount) && $adaccountsCount > 1) { ?>
        <div class="container-fluid" style="margin-top:20px;">
          <div class="detail-status-holder">
            <div class="table-holder responsive">
              <form action="#">
                <table class="table account-data-table">
                  <colgroup>
                    <col class="col5">
                    <col class="col6">
                    <col class="col7">
                    <col class="col8">
                    <col class="col9">
                    <col class="col10">
                    <col class="col11">
                    <col class="col12">
                    <col class="col13">
                    <col class="col14">
                  </colgroup>
                  <thead>
                    <tr>
                      <th><span class="sort-tag"><span>Ad Account Name</span></span></th>
                      <th><span class="sort-tag"><span>ACTIONS</span></span></th>
                      <th><span class="sort-tag"><span>Ad Account ID</span></span></th>
                      <th><span class="sort-tag"><span>Status</span></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                        foreach ($adaccounts as $adaccount) {
                        ?>
                            <tr> 
                                <td><?php echo $adaccount->add_title; ?></td>
                                <td><a href="<?php echo site_url('automaticoptimization/' . $adaccount->ad_account_id); ?>">Add Automatic Optimization Rule</a></td>
                                <td><?php echo $adaccount->ad_account_id; ?></td>
                                <td><?php
                                    if ($adaccount->status == 1) {
                                        echo "active";
                                    } else {
                                        echo "Not active";
                                    }
                                    ?></td>
                                
                            </tr>
                    <?php
                        }
                    ?>
                  </tbody>
                </table>
              </form>
            </div>
          </div>
        </div>
    <?php }else{ ?>
<!--Content-->    
<div class="content" style="margin-top:20px;">
    <div class="row">

        <!--menu-->
        <div class="col-sm-4">
            <div class="menu">
                <div class="clearfix"></div>
                <h2 class="text-center"><span class="text-success"><?php 
				$activecount = 0;
				foreach ($adruleslibrarydata as $autoopti1) {
					if($autoopti1['status'] == "ENABLED"){
						$activecount++;
					}
				}
				echo $activecount; ?></span> Current Active Rules</h2>
        <?php if (isset($adruleslibrarydata) && $adaccountsCount < 2) { ?>        
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                
				<?php	foreach ($adruleslibrarydata as $autoopti) { ?>
                	<div class="panel panel-default" id="headingone<?php echo $autoopti['id']; ?>">
                        <div class="panel-heading" role="tab" >
                           
                            <input type="hidden" value="<?php echo $autoopti['id']; ?>" id="autocampid<?php echo $autoopti['id'] ; ?>" />
                                <?php 
                                    $campaignsid = '';
                                    if($autoopti['evaluation_spec']['filters'][3]['field'] == 'campaign.id'){
                                        $campaignsid = $autoopti['evaluation_spec']['filters'][3]['value'][0];
                                    }
                                   

                                ?>    
                                <input type="hidden" value="<?php echo $campaignsid; ?>" id="autocampcampid<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['evaluation_spec']['filters'][4]['field']; ?>" id="param1<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['evaluation_spec']['filters'][4]['operator']; ?>" id="param2<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['evaluation_spec']['filters'][4]['value']; ?>" id="param3<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['evaluation_spec']['filters'][5]['field']; ?>" id="param4<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['evaluation_spec']['filters'][5]['operator']; ?>" id="param5<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['evaluation_spec']['filters'][5]['value']; ?>" id="param6<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['evaluation_spec']['filters'][1]['value']; ?>" id="timeframe<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['name']; ?>" id="rulename<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['status']; ?>" id="isactive<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['schedule_spec']['schedule_type']; ?>" id="checkRule<?php echo $autoopti['id'] ; ?>" />
                                <input type="hidden" value="<?php echo $autoopti['execution_spec']['execution_type']; ?>" id="ruleMet<?php echo $autoopti['id'] ; ?>" />
                                
                            <label class="switch">
                            <input type="checkbox" <?php if($autoopti['status'] == "ENABLED") echo "checked"; ?> name="rulecheck-1" value="<?php echo $autoopti['id']; ?>" id="manobilli<?php echo $autoopti['id']; ?>" onchange="fnactiveinactive('<?php echo $autoopti['id']; ?>')">
                                <div class="slider round"></div>
                            </label>

                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#<?php echo $autoopti['id']; ?>" aria-expanded="true" aria-controls="<?php echo $autoopti['id']; ?>">
                                <h4 class="panel-title">
                                <?php echo $autoopti['name']; ?>
                                 <!-- <span id="try" class="glyphicon glyphicon-triangle-right pull-right" aria-hidden="true"></span> -->
                                </h4>
                            </a>
                        </div>
                        <div id="<?php echo $autoopti['id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingone<?php echo $autoopti['id']; ?>">
                            <div class="panel-body">
                                <ul>
                                    <li><?php 
									foreach ($adaccounts['campaigns'] as $campaign){
                                        if($campaignsid == $campaign['campaign_id']){
                                            echo $campaign['campaign_name'];
                                        }
                                    } 
									?></li>
                                    <li> Rule: <?php 
                                  echo $autoopti['evaluation_spec']['filters'][4]['field']." ".$autoopti['evaluation_spec']['filters'][4]['operator']." ".$autoopti['evaluation_spec']['filters'][4]['value']; 
                                  ?></li>
                                    <li>Rule: <?php 
                                  echo $autoopti['evaluation_spec']['filters'][5]['field']." ".$autoopti['evaluation_spec']['filters'][5]['operator']." ".$autoopti['evaluation_spec']['filters'][5]['value']; 
                                  ?></li>
                                    <li>Action: <?php 
                                  echo $autoopti['execution_spec']['execution_type']; 
                                  ?></li>
                                    <li>Timeframe: <?php echo $autoopti['evaluation_spec']['filters'][1]['value']; ?></li>
                                    <li>Check Rule: <?php echo $autoopti['schedule_spec']['schedule_type']; ?></li>
                                    <li><a href="javascript:void(0)" onclick='return edititcamp(<?php echo $autoopti['id']; ?>)'>Edit</a>
                                  <a href="javascript:void(0)" onclick="DeleteAutomaticoptimization(<?php echo $autoopti['id']; ?>)">Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                   

                </div>
                <?php } ?>
            </div>
        </div>
        <!--End menu-->

        <!--Filter-->
        <div class="col-sm-8">
            <div class="filter">

                <form class="form-horizontal" action="<?php echo base_url().'automaticoptimization/create_automaticoptimization'; ?>" method="post" name="create_creaautomopt_frm" id="create_creaautomopt_frm">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="campaignName" class="col-sm-5 col-lg-6 control-label">
                                    <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/crown.png" alt="crown" class="img-lebel">
                                    Campaign Name</label>
                                <div class="col-sm-6 col-lg-6">
                                    <select class="form-control" id="campaignsid" name="autoopti[campaignsid]" required="required" >
                                        <!-- <option>Campaign Name</option> -->
										  <?php  if (isset($adaccounts['campaigns'])){ 
                        
                                          foreach ($adaccounts['campaigns'] as $campaign){  ?>
                                            <option value="<?php echo $campaign['campaign_id']; ?>"><?php echo $campaign['campaign_name']; ?></option>  
                                          <?php  } } ?> 
                                    </select>
                                    <input type="hidden" id="addAccountId" name="autoopti[addAccountId]" value="<?php echo $addAccountId; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="form-group">

                                <label for="rule" class="col-sm-5 col-lg-4 control-label">
                                    <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/rule.png" alt="rule" class="img-lebel">
                                    Rule</label>
                                <div class="col-sm-6 col-lg-7">
                                    <select class="form-control" id="rule" name="rule" onchange="setoptmizedvalue(this.value)">
                                        <option value="0"> Create Your Own Rule </option>
                                        <option value="1"> Cost Per Click with Impressions </option>
                                        <option value="2"> Cost per Click with Spent </option>
                                        <option value="3"> Click Through Rate with Impressions </option>
                                        <option value="4"> Click Through Rate with Spent </option>
                                        <option value="5"> Lifetime Frequency Cap </option>
                                        <option value="6"> Daily Frequency Cap </option>
                                        <option value="7"> Relaxed Cost Per Result </option>
                                        <option value="8"> Aggressive Cost Per Result </option>
                                        <option value="9"> High Spend Low Results Notification </option>
                                        <!-- <option value="5"> Engagement with Impressions </option>
                                        <option value="6"> Engagement with Spent </option> -->
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>

                    <!--rule-->

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group text-left">
                                <label for="checkRule" class="col-sm-12 control-label">Check Rule</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="checkRule" name="autoopti[checkRule]">
                                        <option value="DAILY"> Daily </option>
                                        <option value="HOURLY"> Hourly </option>
                                        <option value="SEMI_HOURLY"> Semi Hourly </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group text-left">
                                <label for="ruleMet" class="col-sm-12 control-label">Action When Rule is Met</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="ruleMet" name="autoopti[ruleMet]">
                                        <option value="PAUSE"> Pause </option>
                                        <option value="NOTIFICATION"> Notification </option>                                        
                                    <!--    <option value="CHANGE_BUDGET"> Change Budget </option> -->
                                        <option value="ROTATE"> Rotate </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group text-left">
                                <label for="timeframe" class="col-sm-12 control-label">Timeframe To Read</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="timeframe" name="autoopti[timeframe]">
                                        <option value="TODAY"> Today </option>
                                        <option value="YESTERDAY"> Yesterday </option>
                                        <option value="LAST_3_DAYS"> Last 3 Days</option>
                                        <option value="LAST_7_DAYS"> Last 7 Days</option>
                                        <option value="LAST_14_DAYS"> Last 14 Days</option>
                                        <option value="LAST_30_DAYS"> Last 30 Days</option>
                                        <option value="LIFETIME"> Lifetime</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="rule1" class="col-sm-4 col-md-2 control-label">
                                    <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/flash.png" alt="flash"> Rule #1</label>
                                <div class="col-sm-6 col-md-3">
                                    <select class="form-control" id="acparam1" name="autoopti[param1]">
                                      <option value="impressions">Impression</option>
                                      <option value="unique_impressions">Unique Impressions</option>
                                      <option value="clicks">Clicks</option>
                                      <option value="unique_clicks">Unique Clicks</option>
                                      <option value="spent">Spent</option>
                                      <option value="result">Result</option>
                                      <option value="cpc">CPC</option>
                                      <option value="cpm">CPM</option>
                                      <option value="ctr">CTR</option>
                                      <option value="cpa">CPA</option>
                                      
                                      
                                      <option value="reach">Reach</option>
                                      <option value="frequency">Frequency</option>
                                      <option value="cost_per_link_click">Cost Per Link Click</option>
                                      <option value="cost_per_initiate_checkout_fb">Cost Per Initiate Checkout</option>
                                      <option value="cost_per_purchase_fb">Cost Per Purchase</option>
                                      <option value="cost_per_add_to_cart_fb">Cost Per Add To Cart</option>
                                      <option value="cost_per_lead_fb">Cost Per Lead</option>
                                      <option value="cost_per_add_payment_info_fb">Cost Per Add Payment Info</option>
                                      <option value="cost_per_complete_registration_fb">Cost Per Complete Registration</option>
                                      <option value="cost_per_add_to_wishlist_fb">Cost Per Add To Wishlist</option>
                                      <option value="cost_per_search_fb">Cost Per Search</option>
                                      <option value="cost_per_view_content_fb">Cost Per View Content</option>
                                      <option value="cost_per_mobile_app_install">Cost Per Mobile App Install</option>
                                      <option value="cost_per">Cost Per Result</option>
                                    </select>
                                    
                                </div>
                                <div class="clearfix visible-sm"></div>
                                <br class="visible-sm visible-xs">
                                <div class="col-sm-4 col-md-3">
                                    <select class="form-control" id="acparam2" name="autoopti[param2]">
                                      <option value="GREATER_THAN">Greater Than</option>
                                      <option value="LESS_THAN">Less Than</option>
                                      <option value="EQUAL">Equal</option>
                                      <option value="NOT_EQUAL">Not Equal</option>
                                      <option value="IN">In</option>
                                      <option value="CONTAIN">Contain</option>
                                      <option value="ANY">Any</option>
                                      <option value="NONE">None</option>
                                    </select>
                                </div>
                                <br class="visible-xs">
                                <div class="col-xs-6 col-sm-4 col-md-2">
                                    <input type="text" class="form-control" placeholder="1,000" value="1000" id="acparam3" name="autoopti[param3]">
                                </div>
                                <div>
                                    <!--<button class="btn-circle"><img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/circle-add.png" alt="circle-add"></button>-->
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="rule2" class="col-sm-4 col-md-2 control-label">
                                    <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/flash.png" alt="flash"> Rule #2
                                </label>
                                <div class="col-sm-6 col-md-3">
                                <select id="acparam4" name="autoopti[param4]" class="autoopvariable form-control">
                                  <option value="impressions">Impression</option>
                                      <option value="unique_impressions">Unique Impressions</option>
                                      <option value="clicks">Clicks</option>
                                      <option value="unique_clicks">Unique Clicks</option>
                                      <option value="spent">Spent</option>
                                      <option value="result">Result</option>
                                      <option value="cpc">CPC</option>
                                      <option value="cpm">CPM</option>
                                      <option value="ctr">CTR</option>
                                      <option value="cpa">CPA</option>
                                      <option value="reach">Reach</option>
                                      <option value="frequency">Frequency</option>
                                      <option value="cost_per_link_click" selected>Cost Per Link Click</option>
                                      <option value="cost_per_initiate_checkout_fb">Cost Per Initiate Checkout</option>
                                      <option value="cost_per_purchase_fb">Cost Per Purchase</option>
                                      <option value="cost_per_add_to_cart_fb">Cost Per Add To Cart</option>
                                      <option value="cost_per_lead_fb">Cost Per Lead</option>
                                      <option value="cost_per_add_payment_info_fb">Cost Per Add Payment Info</option>
                                      <option value="cost_per_complete_registration_fb">Cost Per Complete Registration</option>
                                      <option value="cost_per_add_to_wishlist_fb">Cost Per Add To Wishlist</option>
                                      <option value="cost_per_search_fb">Cost Per Search</option>
                                      <option value="cost_per_view_content_fb">Cost Per View Content</option>
                                      <option value="cost_per_mobile_app_install">Cost Per Mobile App Install</option>
                                      <option value="cost_per">Cost Per Result</option>
                                </select>
                                   
                                </div>
                                <div class="clearfix visible-sm"></div>
                                <br class="visible-sm visible-xs">
                                <div class="col-sm-4 col-md-3 ">
                                    <select  class="form-control" id="acparam5" name="autoopti[param5]">
                                      <option value="GREATER_THAN">Greater Than</option>
                                      <option value="LESS_THAN">Less Than</option>
                                      <option value="EQUAL">Equal</option>
                                      <option value="NOT_EQUAL">Not Equal</option>
                                      <option value="IN">In</option>
                                      <option value="CONTAIN">Contain</option>
                                      <option value="ANY">Any</option>
                                      <option value="NONE">None</option>
                                    </select>
                                </div>
                                <br class="visible-xs">
                                <div class="col-xs-6 col-sm-4 col-md-2">
                                	<input type="text" class="form-control" placeholder="5" value="5" id="acparam6" name="autoopti[param6]">
                                    
                                </div>
                                <div>
                                  <!--  <button class="btn-circle"><img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/circle-add.png" alt="circle-add"></button>
                                    <button class="btn-circle"><img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/circle-remove.png" alt="circle-add"></button>-->
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <!--End rule-->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="nameThisRule" class="col-xs-12 col-sm-4 col-md-3 col-lg-2 control-label">Name This Rule</label>
                                <div class="col-xs-11 col-sm-6 col-md-5 col-lg-4">
                                    <input type="text" class="form-control" id="rulename"
                                           name="autoopti[rulename]" placeholder="Rule Name" required="required">
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <input type="hidden" id="theidautocamp" name="theidautocamp" value="0" />
                    <input type="hidden" id="isactive" name="autoopti[isactive]" value="1" />
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3 col-md-7 col-md-offset-5 col-lg-5 col-lg-offset-7">
                            <div class="col-xs-6">
                               <!-- <button class="btn btn-default btn-rule"> <span class="glyphicon glyphicon-remove"></span> Reset Rule</button> -->
                            </div>
                            
                            <div class="col-xs-6">
                                <button type="submit" class="btn btn-success btn-rule btn-save" onclick='if(jQuery("#rulename").val() != ""){ jQuery("#adsetspineer").show(); }'>  <span class="glyphicon glyphicon-ok"></span>  Apply Rule </button>
                                <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/spinner.gif" height="67"
                                                        width="67" class="hidden-sm hidden-xs" style="display:none;" id="adsetspineer" />
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <!--End Filter-->

    </div>
</div>
<?php } ?>
<!--End Content-->
    <style>
	.jcf-select-autoopvariable{
		width:auto !important;
	}
	</style>
    <script type="text/javascript">
        function edititcamp(id){
             document.getElementById("theidautocamp").value = document.getElementById("autocampid"+id).value;
             document.getElementById("campaignsid").value = document.getElementById("autocampcampid"+id).value;
             document.getElementById("acparam1").value = document.getElementById("param1"+id).value;
             var s = document.getElementById("param2"+id).value;
             //$('#acparam2 option[value='+s+']').attr('selected','selected');
             document.getElementById("acparam2").value = document.getElementById("param2"+id).value;
             document.getElementById("acparam3").value = document.getElementById("param3"+id).value;
             document.getElementById("acparam4").value = document.getElementById("param4"+id).value;
             document.getElementById("acparam5").value = document.getElementById("param5"+id).value;
             document.getElementById("acparam6").value = document.getElementById("param6"+id).value;
             document.getElementById("timeframe").value = document.getElementById("timeframe"+id).value;
             document.getElementById("rulename").value = document.getElementById("rulename"+id).value;
			 document.getElementById("isactive").value = document.getElementById("isactive"+id).value;
             document.getElementById("checkRule").value = document.getElementById("checkRule"+id).value;
             document.getElementById("ruleMet").value = document.getElementById("ruleMet"+id).value;
             initCustomForms();
            return false;
        }
		function setoptmizedvalue(id){
			if(id == "1"){
			     document.getElementById("checkRule").value = "DAILY";
				 document.getElementById("ruleMet").value = "PAUSE";
				 document.getElementById("timeframe").value = "TODAY";
				 document.getElementById("acparam1").value = "impressions";
				 document.getElementById("acparam2").value = "GREATER_THAN";
				 document.getElementById("acparam3").value = "1000";
				 document.getElementById("acparam4").value = "cost_per_link_click";
				 document.getElementById("acparam5").value = "GREATER_THAN";
				 document.getElementById("acparam6").value = "0.50";
			}
			else if(id == "2"){
			     document.getElementById("checkRule").value = "DAILY";
				 document.getElementById("ruleMet").value = "PAUSE";
				 document.getElementById("timeframe").value = "TODAY";
				 document.getElementById("acparam1").value = "spent";
				 document.getElementById("acparam2").value = "GREATER_THAN";
				 document.getElementById("acparam3").value = "10";
				 document.getElementById("acparam4").value = "cost_per_link_click";
				 document.getElementById("acparam5").value = "GREATER_THAN";
				 document.getElementById("acparam6").value = "0.50";
			}
			else if(id == "3"){
			     document.getElementById("checkRule").value = "DAILY";
				 document.getElementById("ruleMet").value = "PAUSE";
				 document.getElementById("timeframe").value = "TODAY";
				 document.getElementById("acparam1").value = "impressions";
				 document.getElementById("acparam2").value = "GREATER_THAN";
				 document.getElementById("acparam3").value = "1000";
				 document.getElementById("acparam4").value = "ctr";
				 document.getElementById("acparam5").value = "LESS_THAN";
				 document.getElementById("acparam6").value = "1.5";
			}
			else if(id == "4"){
			     document.getElementById("checkRule").value = "DAILY";
				 document.getElementById("ruleMet").value = "PAUSE";
				 document.getElementById("timeframe").value = "TODAY";
				 document.getElementById("acparam1").value = "spent";
				 document.getElementById("acparam2").value = "GREATER_THAN";
				 document.getElementById("acparam3").value = "10";
				 document.getElementById("acparam4").value = "ctr";
				 document.getElementById("acparam5").value = "LESS_THAN";
				 document.getElementById("acparam6").value = "1.5";
			}
		/*	else if(id == "5"){
				 document.getElementById("acparam1").value = "impressions";
				 document.getElementById("acparam2").value = "GREATER_THAN";
				 document.getElementById("acparam3").value = "1000";
				 document.getElementById("acparam4").value = "3";
				 document.getElementById("acparam5").value = "1";
				 document.getElementById("acparam6").value = "0.50";
			}
			else if(id == "6"){
				 document.getElementById("acparam1").value = "2";
				 document.getElementById("acparam2").value = "1";
				 document.getElementById("acparam3").value = "10";
				 document.getElementById("acparam4").value = "3";
				 document.getElementById("acparam5").value = "1";
				 document.getElementById("acparam6").value = "0.50";
			}*/
			else if(id == "5"){
			    document.getElementById("checkRule").value = "DAILY";
				document.getElementById("ruleMet").value = "PAUSE";
				document.getElementById("timeframe").value = "LIFETIME";
			    document.getElementById("acparam1").value = "frequency";
				document.getElementById("acparam2").value = "GREATER_THAN";
				document.getElementById("acparam3").value = "5";
				document.getElementById("acparam4").value = "spent";
				document.getElementById("acparam5").value = "GREATER_THAN";
				document.getElementById("acparam6").value = "1";
			}
			else if(id == "6"){
			    document.getElementById("checkRule").value = "HOURLY";
				document.getElementById("ruleMet").value = "NOTIFICATION";
				document.getElementById("timeframe").value = "TODAY";
			    document.getElementById("acparam1").value = "frequency";
				document.getElementById("acparam2").value = "GREATER_THAN";
				document.getElementById("acparam3").value = "3";
				document.getElementById("acparam4").value = "spent";
				document.getElementById("acparam5").value = "GREATER_THAN";
				document.getElementById("acparam6").value = "1";
			}
			else if(id == "7"){
			    document.getElementById("checkRule").value = "DAILY";
				document.getElementById("ruleMet").value = "NOTIFICATION";
				document.getElementById("timeframe").value = "LAST_3_DAYS";
			    document.getElementById("acparam1").value = "cost_per";
				document.getElementById("acparam2").value = "GREATER_THAN";
				document.getElementById("acparam3").value = "10";
				document.getElementById("acparam4").value = "impressions";
				document.getElementById("acparam5").value = "GREATER_THAN";
				document.getElementById("acparam6").value = "1000";
			}
			else if(id == "8"){
			    document.getElementById("checkRule").value = "SEMI_HOURLY";
				document.getElementById("ruleMet").value = "PAUSE";
				document.getElementById("timeframe").value = "TODAY";
			    document.getElementById("acparam1").value = "cost_per";
				document.getElementById("acparam2").value = "GREATER_THAN";
				document.getElementById("acparam3").value = "10";
				document.getElementById("acparam4").value = "impressions";
				document.getElementById("acparam5").value = "GREATER_THAN";
				document.getElementById("acparam6").value = "1000";
			}
			else if(id == "9"){
			    document.getElementById("checkRule").value = "SEMI_HOURLY";
				document.getElementById("ruleMet").value = "NOTIFICATION";
				document.getElementById("timeframe").value = "LIFETIME";
			    document.getElementById("acparam1").value = "cost_per";
				document.getElementById("acparam2").value = "GREATER_THAN";
				document.getElementById("acparam3").value = "20";
				document.getElementById("acparam4").value = "spent";
				document.getElementById("acparam5").value = "GREATER_THAN";
				document.getElementById("acparam6").value = "1000";
			}
			else{
			    document.getElementById("checkRule").value = "DAILY";
				document.getElementById("ruleMet").value = "PAUSE";
				document.getElementById("timeframe").value = "TODAY";
				document.getElementById("acparam1").value = "impressions";
				document.getElementById("acparam2").value = "GREATER_THAN";
				document.getElementById("acparam3").value = "1000";
				document.getElementById("acparam4").value = "cpc";
				document.getElementById("acparam5").value = "GREATER_THAN";
				document.getElementById("acparam6").value = "5";
			}
		}
		function fnactiveinactive(id){
			var isChecked = $("#manobilli"+id).is(":checked");
			if (isChecked) {
				$.ajax({
					type: "POST",
					cache: false,
					url: "<?php echo base_url().'automaticoptimization/update_aoactivation'?>/",
					data: {
						isactive: 1,
						Id: id
					},
					beforeSend: function (){
						
					},
					success: function (html){
							window.location.reload();
						
					},
					async: true
				});
				
			} else {
				$.ajax({
					type: "POST",
					cache: false,
					url: "<?php echo base_url().'automaticoptimization/update_aoactivation'?>/",
					data: {
						isactive: 0,
						Id: id
					},
					beforeSend: function (){
						
					},
					success: function (html){
							window.location.reload();
						
					},
					async: true
				});
			}
             
        }
		
    </script>
   
  </main>