<?php
if($this->uri->segment(1) != 'upgradediscount'){
    if(($this->session->userdata['logged_in']['accesstoken'] == '' && $this->uri->segment(1) != 'dashboard')) {
		header("Location:".$this->config->item('site_url').'connect-to-facebook/');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <title><?php SeoDetail('Title'); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="description" content="<?php SeoDetail('Description'); ?>" />
    <meta name="author" content="The Campaign Maker"/>
    
    
    <link rel="icon" type="image/x-icon" href="<?php echo $this->config->item('assets'); ?>admin/layout/img/favicon.ico">
    
   
	<!-- include the site stylesheet -->
	<link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>optimize/css/main.css">

   <!-- <link href="https://fonts.googleapis.com/css?family=Heebo:400,500,700" rel="stylesheet">-->
   
    <link href="<?php echo $this->config->item('assets'); ?>newdesign/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link href="<?php echo $this->config->item('assets'); ?>newdesign/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>newdesign/css/daterangepicker.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>newdesign/css/morris.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets'); ?>newdesign/css/ring.css">
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>newdesign/css/style.css">
    <link href="<?php echo $this->config->item('assets'); ?>global/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>token-input-facebook.css" type="text/css" />
    <?php if ($this->uri->segment(1) == 'special' || $this->uri->segment(1) == 'trialend') { ?>
    <link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>newdesign/css/main.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
   
    <?php } ?>
	
    <style>
      
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
     <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/animationdata.js"></script>
     <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/spinner.js"></script>
     
     <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/analys.js"></script>
     <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/analysispdf.js"></script>
     
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
  
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/pdf/jquery-ui-1.8.17.custom.min.js"></script> 
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/pdf/jspdf.debug.js"></script> 
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/pdf/basic.js"></script> 
     <!--// TEST -->
<script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>
   <!--// TEST END -->
   
  
   

    <script>
        // animate_spinner();
         	 function animate_spinner(){
         	    
                var params = {
                    container: document.getElementById('lottie'),
                    renderer: 'html',
                    loop: true,
                    autoplay: true,
                    animationData: animationData
                };
        
                var anim;
                anim = lottie.loadAnimation(params);
          }

    
        var site_url = "<?php echo base_url(); ?>";
        var cur_currency = "<?php echo $this->session->userdata('cur_currency'); ?>";
        <?php if (!empty($this->session->userdata('sDate')) && !empty($this->session->userdata('eDate'))) { ?>
            var sDate = "<?php echo $this->session->userdata('sDate'); ?>";
            var eDate = "<?php echo $this->session->userdata('eDate'); ?>";
            var sDate1 = "<?php echo date('M d, Y', strtotime($this->session->userdata('sDate'))); ?>";
            var eDate1 = "<?php echo date('M d, Y', strtotime($this->session->userdata('eDate'))); ?>";
            var sDate2 = "<?php echo date('m/d/Y', strtotime($this->session->userdata('sDate'))); ?>";
            var eDate2 = "<?php echo date('m/d/Y', strtotime($this->session->userdata('eDate'))); ?>";
        <?php } else{ ?>
            var sDate = "<?php echo $this->session->userdata('sDate'); ?>";
            var eDate = "<?php echo $this->session->userdata('eDate'); ?>";
            var sDate1 = "<?php echo date('M d, Y', strtotime($this->session->userdata('sDate'))); ?>";
            var eDate1 = "<?php echo date('M d, Y', strtotime($this->session->userdata('sDate'))); ?>";
            var sDate2 = "<?php echo date('m/d/Y', strtotime($this->session->userdata('sDate'))); ?>";
            var eDate2 = "<?php echo date('m/d/Y', strtotime($this->session->userdata('eDate'))); ?>";
        <?php } ?>
    </script>
    <?php if ($this->uri->segment(1) == 'trialactivate' || $this->uri->segment(1) == 'upgrade' || $this->uri->segment(1) == 'trialactivate2') { ?>
      <script type="text/javascript" src="https://js.stripe.com/v1/"></script>
    <?php } ?>
    <script>
    
    
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-76300210-1', 'auto', {'allowLinker': true});
      ga('require', 'linker');
      ga('linker:autoLink', ['jvzoo.com'] );
      ga('send', 'pageview');

    </script>
    <script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
      heap.load("1418879561");
      heap.addUserProperties({email: "<?= $session_data['email'] ?>"});
      heap.identify(<?= $session_data['email'] ?>);
</script>
   
    <style type="text/css">
      #overlay {
          display: none;
          position: absolute;
          background: #000;
      }
      #img-load {
          position: absolute;
          margin: -50px 0 0 -40px;
      }
    </style>
    
    
  </head>
  <body>
       <?php  //$useraddaccountcounts = getUserAddAccountIdcount(); ?>	
      <?php
     
      if ($this->uri->segment(1) == 'reports' || $this->uri->segment(1) == 'automaticoptimization' || $this->uri->segment(1) == 'analysis' ){ ?>
      
    
                        <div  id="commonloaderoverlay" style="display:none" >
           
                                 <div class="frame" style="color:white;">
									<h2 class="screen-heading text-center">
										<span id="full_loading_heading"></span>
										<span class="sub-heading s1">
										Loading..
										</span>
									</h2>
									
									
										<div  class="loading" style="margin:auto;width:400px">
										    	<div id="common_lottie" style="width:400px;height:300px"></div>
										    
										</div>
									
								</div>
    </div>
    
    
   <?php } ?>
        
   <script>
     /*
          	common_animate_spinner();
          	 	setTimeout(function(){ 
	    $('#commonloaderoverlay').hide();
	 }, 3000);
	 */
	      function common_animate_spinner(){
         	   
                var params = {
                    container: document.getElementById('common_lottie'),
                    renderer: 'html',
                    loop: true,
                    autoplay: true,
                    animationData: animationData
                };
        
                var anim;
                anim = lottie.loadAnimation(params);
          }
          
        /* $(document).ready(function(){  
            
            /*
              var menu =  $(this).text();
            var addaccountcount = parseInt('<?php echo $useraddaccountcounts ?>');
           
            if(menu == 'Report' ){
                
                if(addaccountcount > 1){
                    $('.main_report').show();  
                }else{
                    $('#commonloaderoverlay').show();
                    common_animate_spinner();
                   
            	 setTimeout(function(){ 
            	  
            	    $('.main_report').show();  
            	    $('#commonloaderoverlay').hide();
            	 }, 3000);
                }
            }
            
            
            if(( menu == 'Analyze') ){
                
                if(addaccountcount > 1){
                    //$('#main_5').show();  
                     $('#main_1').show(); 
                    
                }else{
                    $('#commonloaderoverlay').show();
                    common_animate_spinner();
                  
                	 setTimeout(function(){ 
                	  
                	    $('#main_1').show();  
                	    $('#commonloaderoverlay').hide();
                	 }, 3000);
                    
                    
                }
            }
            
            
            if(( menu == 'Optimize') ){
                
                if(addaccountcount > 1){
                    
                    $('.optimization_rule_list').show(); 
                    
                }else{
                    
                   	   $('#commonloaderoverlay').show();
                       common_animate_spinner();
                       setTimeout(function(){ 
                           
                	   $('#commonloaderoverlay').hide();
                	   $('.optimization_rule_list').show();  
                	   
                	 }, 3000);
	
                    
                }
            }
            
            */
            
     /*
        
        $('#primary-nav .nav-item a').click(function(){
            
            var menu =  $(this).text();
            var addaccountcount = parseInt('<?php echo $useraddaccountcounts ?>');
           
            if(menu == 'Report' ){
                
                if(addaccountcount > 1){
                    $('.main_report').show();  
                }else{
                    $('#commonloaderoverlay').show();
                    common_animate_spinner();
                   
            	 setTimeout(function(){ 
            	  
            	    $('.main_report').show();  
            	    $('#commonloaderoverlay').hide();
            	 }, 3000);
                }
            }
            
            
            if(( menu == 'Analyze') ){
                
                if(addaccountcount > 1){
                    //$('#main_5').show();  
                     $('#main_1').show(); 
                    
                }else{
                    $('#commonloaderoverlay').show();
                    common_animate_spinner();
                  
                	 setTimeout(function(){ 
                	  
                	    $('#main_1').show();  
                	    $('#commonloaderoverlay').hide();
                	 }, 3000);
                    
                    
                }
            }
            
            
            if(( menu == 'Optimize') ){
                
                if(addaccountcount > 1){
                    
                    $('.optimization_rule_list').show(); 
                    
                }else{
                    
                   	   $('#commonloaderoverlay').show();
                       common_animate_spinner();
                       setTimeout(function(){ 
                          
                	   $('#commonloaderoverlay').hide();
                	   $('.optimization_rule_list').show();  
                	   
                	 }, 3000);
	
                    
                }
            }
            
        });
        
          
        });
          
        */  
          
      </script>
	