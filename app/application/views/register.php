<main id="main">
				<div class="main-content">
					<div class="container">
						<div class="alert alert-danger alert-dismissible" style="display:none;">
              </div>
						<div class="form-wrap">
							<div class="holder s1">
								<form class="mepr-signup-form mepr-form" method="post" action="<?php echo $this->config->item('site_url'); ?>register/#mepr_jump" novalidate="">
								   <input id="mepr_process_signup_form" name="mepr_process_signup_form" value="Y" type="hidden">
									<input id="mepr_product_id" name="mepr_product_id" value="189" type="hidden">
									<input id="transaction_product_id" name="transaction_product_id" value="189" type="hidden">
									<input id="transaction_product_id" name="transaction[product_id]" value="189" type="hidden">
									<input id="memberships" name="memberships" value="189" type="hidden">
									<input id="site_name" name="site_name" value="v4" type="hidden">
									<input id="email_validate" name="email_validate" value="0" type="hidden">
									<h2 class="form-title">REGISTER FOR YOUR TCM ACCOUNT</h2>
									<div class="form-group">
										<label for="name">First Name:</label>
										<input name="user_first_name" id="user_first_name" class="mepr-form-input form-control" value="" type="text">
									</div>
									<div class="form-group">
										<label for="name">Last Name:</label>
										 
										<input name="user_last_name" id="user_last_name" class="mepr-form-input form-control" value="" type="text">
									</div>
									<div class="form-group">
										<label for="email-address">Email Address:*</label>
										<input name="user_email" id="user_email" class="mepr-form-input form-control" value="" required="" type="email">
									</div>
									<div class="form-group">
										<label for="password">Password:*</label>
										<input name="mepr_user_password" id="mepr_user_password" class="mepr-form-input mepr-password form-control" value="" required="" type="password">
									</div>
										<div class="form-group">
										<label for="password">Password Confirmation:*</label>
										<input name="mepr_user_password_confirm" id="mepr_user_password_confirm" class="mepr-form-input mepr-password-confirm form-control valid" value="" required="" type="password">
									</div>
									<div class="btn-row">
										<div class="d-sm-flex align-items-center">
											<div class="col">
												<div class="btn-holder text-center text-sm-left">
												 <input class="mepr-submit btn btn-primary" value="Sign Up" type="button" onclick="checkvalidation()">
												
												</div>
											</div>
											<div class="option text-center">
												<span>OR</span>
											</div>
											<div class="col">
												<div class="btn-holder text-center text-sm-right">
													<a href="<?php echo $this->config->item('site_url'); ?>chklogin/fb-signupauthorization.php" class="btn btn-fb"><span class="icon-facebook"></span>REGISTER WITH FB</a>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="panel">
								<div class="panel-holder d-sm-flex justify-content-between align-items-center">
									<div class="link-holder text-center text-sm-left">
										<a href="#" class="link-s2">Already have an account?</a>
									</div>
									<div class="btn-holder text-center text-sm-right">
										<a href="<?php echo $base_url; ?>login" class="btn btn-secondary">LOGIN NOW</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			
	
	<script>

		function checkvalidation(){
			$('.alert-danger').hide();
			if($('#user_first_name').val()==''){				
				$('.alert-danger').html("You must enter Name.");
				$('.alert-danger').show();
				$('#user_first_name').focus();
			}else	if($('#user_login').val()==''){				
				$('.alert-danger').html("You must enter UserName.");
				$('.alert-danger').show();
				$('#user_login').focus();
			}else
				if($('#user_email').val()==''){				
				$('.alert-danger').html("You must enter a Email.");
				$('.alert-danger').show();
				$('#user_email').focus();
			}else
				if(!isEmail($('#user_email').val())){
					$('.alert-danger').html("Email must be a real and properly formatted email address");
				$('.alert-danger').show();
				$('#user_email').focus();
			}else
				if($('#email_validate').val()==0){
				$('.alert-danger').html("This email address has already been used. If you are an existing user, please Login to complete your purchase. You will be redirected back here to complete your sign-up afterwards.");
				$('.alert-danger').show();
				$('#user_email').focus();
			}else
				if($('#mepr_user_password').val()==''){
					$('.alert-danger').html("You must enter a Password.");
				$('.alert-danger').show();
				$('#mepr_user_password').focus();
			}else
				if($('#mepr_user_password_confirm').val()==''){
					$('.alert-danger').html("You must enter a Password Confirmation.");
				$('.alert-danger').show();
				$('#mepr_user_password_confirm').focus();
			}else
				if($('#mepr_user_password_confirm').val()!=$('#mepr_user_password').val()){
					$('.alert-danger').html("Password and Password Confirmation should be same.");
				$('.alert-danger').show();
				$('#mepr_user_password').focus();
			}else
			{
			/*	var url ="<?php echo base_url(); ?>Register/saveUser?email="+$('#user_email').val()+"&name="+$('#user_first_name').val()+"&password="+$('#mepr_user_password').val();
				$.ajax({url: url, success: function(result){
				if(result==1){*/
					$(".mepr-signup-form").submit();
				/*}
			}});*/
			}
		}
		
		
		
			function isEmail(email) {
				  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				  return regex.test(email);
			}
			$(document).ready(function(){
			$("#user_email").change(function(){	
			var email=$('#user_email').val();
			$.ajax({url: "<?php echo base_url(); ?>Register/checkEmail?email="+email, success: function(result){
				if(result>=1){
					$('.alert-danger').html("This email address has already been used. If you are an existing user, please Login to complete your purchase. You will be redirected back here to complete your sign-up afterwards.");
					$('.alert-danger').show();
					$('#email_validate').val('0');
					$('#user_email').focus();
				}else{
					$('#email_validate').val('1');
				}
			}});
			});
			});
	</script>
