<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');

if($this->uri->segment(1) == 'dashboard'){
?>
<style type="text/css">#wrapper{ background: #fff;
    min-height: 100%; } .btn-wrap{margin-bottom: 20px;}.w1 {
	min-height: 600px;
	}.content-wrap {margin-top: 50px;} </style>

        
      <?php if (isset($adaccounts) && $adaccounts != NULL) { ?>
      <div class="container-fluid">
      <?php } else { ?>  
      <div class="container">
      <?php } ?>  
       
                
        <?php if (isset($adaccounts) && $adaccounts != NULL) { ?>
            <?php
            $attributes = array('id' => 'adAccountForm');
            echo form_open('dashboard/ad-account', $attributes);
            ?>
                    <div class="ad-account-holder">
                        <div class="table-responsive">
                          <table class="table account-data-table">
                            <colgroup>
                              <col class="col1">
                              <col class="col2">
                              <col class="col3">
                              <col class="col4">
                            </colgroup>
                            <thead>
                              <tr>
                                <th><span class="sort-tag"><span>Choose</span> </span></th>
                                <th><span class="sort-tag"><span>AD ACCOUNT NAME</span></span></th>
                                <th><span class="sort-tag"><span>AD ACCOUNT ID</span></span></th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($adaccounts as $adaccount) { ?>

                                    <tr>
                                        <td><input type="checkbox" value="<?= $adaccount['account_id'] . "@IN@" . $adaccount['name']; ?>" name="account_id_name[]" /></td>
                                        <td class="title"><?= $adaccount['name']; ?></td>
                                        <td class="account-id"><?= $adaccount['account_id']; ?></td>
                                        <?php
                                            if ($adaccount['account_status'] == 1) {
                                                echo '<td class="status">ACTIVE</td>';
                                            } else {
                                                echo '<td class="status inactive">INACTIVE</td>';
                                            }
                                            ?>
                                    </tr>
                                <?php } ?>  
                            </tbody>
                          </table>
                        </div>
                      </div> 
                      <div class="btn-wrap text-center start-saving">
                        <input type="hidden" name="fbtoken" value="<?= $fbaccesstoken; ?>" />
                        <button class="btn btn-success" type="submit">Add Ad Account</button>
                      </div>   
                    <!-- Divider here -->               
            </form>
        <?php } else { ?>        
        <div class="login-box-wrap">
          <!--div class="top-logo">
            <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/logo-small.png" alt="logo">
          </div-->
          <div class="content-wrap text-center">
            <div class="header-box">
              <h1>Welcome to The Campaign Maker <?= @ucfirst($session_data['first_name']) ?>!</h1>
              <strong class="sub-heading">READY TO SUPER CHARGE YOUR <br>FACEBOOK CAMPAIGNS?</strong>
            </div>
            <div class="social-login">
			  <a href="<?= $loginurl ?>" class="btn btn-fb" id="fb_btn"><i class="icon-facebook"></i> Connect Your Facebook Account To Get Started</a>
             
            </div>
          </div>
        </div>
        <?php } ?>
		 <?php if ((isset($error) && $error != NULL) || (validation_errors())) { ?>
                    <?php if (validation_errors()) : ?>
                        <div class="top-notification-box">
                          <i class="icon-warning"></i>
                          <span class="text"><?= validation_errors() ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($error)) : ?>
                        <div class="top-notification-box">
                          <i class="icon-warning"></i>
                          <span class="text"><?= $error ?></span>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($success)) : ?>
                    <div class="top-notification-box">
                        <i class="icon-warning"></i>
                      <span class="text"><?= $success; ?></span>
                    </div>
                       
                    <?php endif; ?>
                
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="top-notification-box">
              <i class="icon-warning"></i>
              <span class="text"><?= $this->session->flashdata('error') ?></span>
            </div>
        <?php } ?>
      </div>
 </div>     

<?php } else { ?>
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('menu'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <div class="col-md-7  p-rl-3">
                        <h3 class="page-title">
                            Welcome, <?= @ucfirst($session_data['first_name']) ?>
                        </h3>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 startcamp_sec">

                    <?php if ((isset($error) && $error != NULL) || (validation_errors())) { ?>
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="start-saving">
                                <?php if (validation_errors()) : ?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span>
                                            <?= validation_errors() ?> </span>
                                    </div>

                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span>
                                            <?= $error ?> </span>
                                    </div>

                                <?php endif; ?>

                                <?php if (isset($success)) : ?>
                                    <div class="alert alert-success">
                                        <button class="close" data-close="alert"></button>
                                        <span>
                                            <?= $success; ?> </span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('error')) { ?>
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="start-saving">
                                <div class="alert alert-danger">
                                    <button class="close" data-close="alert"></button>
                                    <span>
                                        <?= $this->session->flashdata('error') ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="row">
                        <div class="col-md-12">
                            <div align="center" class="startcamp_title">
                                <h2>Connect your Facebook account to start optimizing<br /> <span>your campaigns!</span></h2>
                            </div>
                            <br />
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <?php if (isset($adaccounts) && $adaccounts != NULL) { ?>
                        <?php
                        $attributes = array('id' => 'adAccountForm');
                        echo form_open('dashboard/ad-account', $attributes);
                        ?>
                        
                                <div class="ad_accounts table-scrollable">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Choose</th>
                                                <th>Ad Account Name</th>
                                                <th>Ad Account ID</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($adaccounts as $adaccount) { ?>

                                                <tr>
                                                    <td><input type="checkbox" value="<?= $adaccount['account_id'] . "@IN@" . $adaccount['name']; ?>" name="account_id_name[]" /></td>
                                                    <td><?= $adaccount['name']; ?></td>
                                                    <td><?= $adaccount['account_id']; ?></td>
                                                    <td><?php
                                                        if ($adaccount['account_status'] == 1) {
                                                            echo "active";
                                                        } else {
                                                            echo "Not active";
                                                        }
                                                        ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-2 col-md-offset-5 start-saving">
                                    <input type="hidden" name="fbtoken" value="<?= $fbaccesstoken; ?>" />
                                    <input type="submit" class="btn btn-primary" value="Add Ad Account" />
                                </div>
                           
                        </form>
                    <?php } else { ?>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div align="center" class="first_campaign_btn">
                                        <a href="<?= $loginurl ?>" class="abtn azm-social azm-btn azm-pill azm-shadow-bottom azm-facebook"><i class="fa fa-facebook"></i> Add Facebook Account</a>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<?php } ?>
