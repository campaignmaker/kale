<div  id="preloaderMain" style="display:none;">
            <div id="statusMain" style="font-size:14px;color:white">
                <i class="fa fa-spinner fa-spin" style="font-size:48px;color:white"></i></br>
              <span>Updating your report dashboard, please wait..</span>
            </div>
    </div>

<!-- Main Content Starts -->
    <main id="main">
       
        
        
        <div class="breadcrumb-holder">
					<div class="container d-sm-flex align-items-center">
					    <?php if (isset($adaccountsCount) && $adaccountsCount == 1) { ?>
						<div class="select-holder">
						    <select class="tcmreportdd" name="changeDateRange" onchange="getAdd('<?php echo $addAccountId; ?>', '<?php echo $campaignId; ?>', '<?php echo $addsetId; ?>', '<?php echo $addId; ?>', this.value)">
                      
                              <option value="today" <?php if($limit == 'today') echo 'selected'; ?>>Today</option>
                              <option value="yesterday" <?php if($limit == 'yesterday') echo 'selected'; ?>>Yesterday</option>
                              <option value="last_3d" <?php if($limit == 'last_3d') echo 'selected'; ?>>Last 3 Days</option>
                              <option value="last_7d" <?php if($limit == 'last_7d') echo 'selected'; ?>>Last 7 Days</option>
                              <option value="last_14d" <?php if($limit == 'last_14d') echo 'selected'; ?>>Last 14 Days</option>
                              <option value="last_28d" <?php if($limit == 'last_28d') echo 'selected'; ?>>Last 28 Days</option>
                              <option value="last_30d" <?php if($limit == 'last_30d') echo 'selected'; ?>>Last 30 Days</option>
                              <option value="last_90d" <?php if($limit == 'last_90d') echo 'selected'; ?>>Last 90 Days</option>
                              <option value="this_month" <?php if($limit == 'this_month') echo 'selected'; ?>>This Month</option> 
                              <option value="last_month" <?php if($limit == 'last_month') echo 'selected'; ?>>Last Month</option>
                              <option value="this_quarter" <?php if($limit == 'this_quarter') echo 'selected'; ?>>This Quarter</option>                      
                              <option value="this_year" <?php if($limit == 'this_month') echo 'selected'; ?>>This Year</option> 
                              <option value="last_year" <?php if($limit == 'last_month') echo 'selected'; ?>>Last Year</option>
                              <option value="lifetime" <?php if($limit == 'lifetime') echo 'selected'; ?>>Lifetime</option>
                            </select>
						</div>
						<?php } ?>
						<ol class="breadcrumb">
						    
						    
							<li class="breadcrumb-item"><a href="<?php echo base_url().'reports'; ?>">Report</a></li>
							<li class="breadcrumb-item">
							    <?php if (isset($adaccountsCount) && $adaccountsCount == 1) { ?>
              <a href="<?php echo base_url().'reports/'.$addAccountId; ?>"><?php echo substr($adaccounts['add_header']['accountName'], 0, 19); ?></a>
              <?php } else { ?>
             <a href="#"><?php echo substr($adaccounts['add_header']['accountName'], 0, 9); ?></a>
              <?php } ?>
              </li> 
              <li class="breadcrumb-item"><a href="<?php echo base_url().'addset/'.$addAccountId.'/'.$campaignId; ?>"><?php echo substr($addset_name, 0, 19); ?></a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url().'addds/'.$addAccountId.'/'.$campaignId.'/'.$addsetId; ?>"><?php echo substr($addsetName, 0, 19); ?></a></li>
							  <li class="breadcrumb-item"><a href="#">  <?php echo substr($addName, 0, 19); ?></a></li>
						</ol>
					</div>
				</div>
				<div class="main-content s1">
					<div class="main-statistics">
						<ul class="statistics-list d-flex flex-wrap">
							<li class="d-flex align-items-center">
								<div class="icon-holder">
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-money.svg" alt="money">
								</div>
								<div class="text-holder">
									<?php
                                if(round($adaccounts['add_header']['spent']) < 1000){
                                    echo '<div class="value">';
                                                }else{
                                    echo '<div class="value spentli">';
                                                }
                          if ($adaccounts['add_header']['spent'] > 0) {
                              echo $this->session->userdata('cur_currency') . number_format($adaccounts['add_header']['spent'], 2, '.', ',');
                          } else {
                              echo "0";
                          }
                          ?></div>
									<span class="title">TOTAL SPENT</span>
								</div>
							</li>
							<li class="d-flex align-items-center">
								<div class="icon-holder">
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-paper-plane.svg" alt="money">
								</div>
								<div class="text-holder">
									<div class="value"><?php echo number_format($adaccounts['add_header']['actions']); ?></div>
									<span class="title">RESULTS</span>
								</div>
							</li>
							<li class="d-flex align-items-center">
								<div class="icon-holder">
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-calculator.svg" alt="money">
								</div>
								<div class="text-holder">
									<div class="value"><?php 
									if($adaccounts['add_header']['actions'] > 0) 
									echo $this->session->userdata('cur_currency') . number_format($adaccounts['add_header']['spent']/$adaccounts['add_header']['actions'], 2, '.', ',');
									else
									echo "0"; ?></div>
									<span class="title">COST PER RESULT</span>
								</div>
							</li>
							<li class="d-flex align-items-center">
								<div class="icon-holder">
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-megaphone.svg" alt="money">
								</div>
								<div class="text-holder">
									<div class="value"><?php 
									if ($adaccounts['add_header']['reach'] > 0) {
                             echo number_format($adaccounts['add_header']['reach']);
                          } else {
                              echo "0";
                          }
									?></div>
									<span class="title">REACH</span>
								</div>
							</li>
						</ul>
					</div>
					<div class="main-data">
						<div class="data-row open-close">
							<div class="frame d-flex flex-wrap align-items-center">
								<div class="data-holder d-flex flex-wrap align-items-center">
									<div class="column select-column">
										<div class="select-holder">
											<select class="ParamDD" onchange="getDetailData(this);">
												<option value="impressions">Impressions</option>
												<option value="inline_link_clicks">Clicks</option>
												<option value="page_engagement">Engagement</option>
												<option value="offsite_conversion">Conversion</option>
												<option value="like">Page Like</option>
												<option value="video_view">3-Second Video Views</option>
												<option value="app_install">App Installs</option>
                                                <option value="leadgen_other">Leads (Form)</option>
											    <option value="mobile_app_install">Mobile App Installs</option>
											    <option value="offsite_conversion_fb_pixel_add_payment_info">Adds Payment Info</option>
											    <option value="offsite_conversion_fb_pixel_add_to_cart">Adds To Cart</option>
											    <option value="offsite_conversion_fb_pixel_add_to_wishlist">Adds To Wishlist</option>
											    <option value="offsite_conversion_fb_pixel_complete_registration">Completed Registration</option>
											    <option value="offsite_conversion_fb_pixel_initiate_checkout">Initiates Checkout</option>
											    <option value="offsite_conversion_fb_pixel_lead">Leads</option>
											    <option value="offsite_conversion_fb_pixel_purchase">Purchases</option>
											    <option value="offsite_conversion_fb_pixel_search">Searches</option>
											    <option value="offsite_conversion_fb_pixel_view_content">Views Content</option>
											    <option value="offsite_conversion_key_page_view">Key Page Views</option>
											    <option value="onsite_conversion_messaging_first_reply">New Messaging Conversations</option> 
											</select>
										</div>
									</div>
									<div class="column firstval">
										<a class="tab-opener" href="#">
										    <?php
                                
                                if(round($adaccounts['add_header']['impressions']) < 1000){
                                    echo '<strong class="value">';
                                                }else{
                                    echo '<strong class="value spentli">';
                                                }
                            echo number_format($adaccounts['add_header']['impressions']);
                          ?></strong>
											<span class="title">Impressions</span>
										</a>
									</div>
									<div class="column secondval">
										<a class="tab-opener" href="#" >
											<strong class="value"><?php
                                echo $this->session->userdata('cur_currency'); ?><?php
                            //echo number_format($adaccounts['add_header']['cpm']);
							if($adaccounts['add_header']['impressions'] != '0'){
								$subcpc = $adaccounts['add_header']['impressions']/1000;
								echo round($adaccounts['add_header']['spent']/$subcpc,2);
							} ?></strong>
											<span class="title">CPM</span>
										</a>
									</div>
									<div class="column thirdval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php echo number_format($adaccounts['add_header']['newFrequ'], 2); ?></strong>
											<span class="title">Frequency</span>
										</a>
									</div>
									<div class="column btn-column">
										<a href="#" class="btn btn-primary opener" onclick="return GetBreakdownAdone(this);">BREAKDOWN</a>
									</div>
								</div>
								<div class="action">
									<a href="#" class="btn btn-add" onclick="return addnewanalytics(this);"><span class="icon-plus"></span></a>
								</div>
							</div>
							<div class="slide">
								<div class="breakdown-data d-flex flex-wrap">
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">GENDER</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="genderbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">AGE GROUPS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="agegroupbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">PLACEMENTS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="placementbody">
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="data-row open-close">
							<div class="frame d-flex flex-wrap align-items-center">
								<div class="data-holder d-flex flex-wrap align-items-center">
									<div class="column select-column">
										<div class="select-holder">
											<select class="ParamDD" onchange="getDetailData(this);">
											    <option value="inline_link_clicks">Clicks</option>
											    <option value="impressions">Impressions</option>
											    <option value="page_engagement">Engagement</option>
												<option value="offsite_conversion">Conversion</option>
												<option value="like">Page Like</option>
												<option value="video_view">3-Second Video Views</option>
												<option value="app_install">App Installs</option>
                                                <option value="leadgen_other">Leads (Form)</option>
											    <option value="mobile_app_install">Mobile App Installs</option>
											    <option value="offsite_conversion_fb_pixel_add_payment_info">Adds Payment Info</option>
											    <option value="offsite_conversion_fb_pixel_add_to_cart">Adds To Cart</option>
											    <option value="offsite_conversion_fb_pixel_add_to_wishlist">Adds To Wishlist</option>
											    <option value="offsite_conversion_fb_pixel_complete_registration">Completed Registration</option>
											    <option value="offsite_conversion_fb_pixel_initiate_checkout">Initiates Checkout</option>
											    <option value="offsite_conversion_fb_pixel_lead">Leads</option>
											    <option value="offsite_conversion_fb_pixel_purchase">Purchases</option>
											    <option value="offsite_conversion_fb_pixel_search">Searches</option>
											    <option value="offsite_conversion_fb_pixel_view_content">Views Content</option>
											    <option value="offsite_conversion_key_page_view">Key Page Views</option>
											    <option value="onsite_conversion_messaging_first_reply">New Messaging Conversations</option> 
											</select>
										</div>
									</div>
									<div class="column firstval">
										<a class="tab-opener" href="#">
											<?php 
                              if(round($adaccounts['add_header']['spent']) < 1000){
                                    echo '<strong class="value">';
                                                }else{
                                    echo '<strong class="value spentli">';
                                                }
                              echo number_format($adaccounts['add_header']['inline_link_clicks']); ?></strong>
											<span class="title">Clicks</span>
										</a>
									</div>
									<div class="column secondval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php
                            if ($adaccounts['add_header']['spent'] > 0)
                            {
                                $cost_per_click = $adaccounts['add_header']['spent'] / $adaccounts['add_header']['inline_link_clicks'];
                                if ($cost_per_click > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($cost_per_click, 2, '.', ',');
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($cost_per_click);
                                }
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . $adaccounts['add_header']['inline_link_clicks'];
                            }?></strong>
											<span class="title">Per Click</span>
										</a>
									</div>
									<div class="column thirdval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php if ($adaccounts['add_header']['impressions'] > 0)
                            {
                                $click_rate = ($adaccounts['add_header']['inline_link_clicks'] / $adaccounts['add_header']['impressions'])*100;
                                if ($click_rate > 0)
                                {
                                    echo round(number_format($click_rate, 3, '.', ','),2)."%";
                                }
                            }
                             ?></strong>
											<span class="title">Click Rate</span>
										</a>
									</div>
									<div class="column btn-column">
										<a href="#" class="btn btn-primary opener" onclick="return GetBreakdownAdone(this);">BREAKDOWN</a>
									</div>
								</div>
								<div class="action">
									<a href="#" class="btn btn-remove" onclick="return removenewanalytics(this);"><span class="icon-bin"></span></a>
								</div>
							</div>
							<div class="slide">
								<div class="breakdown-data d-flex flex-wrap">
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">GENDER</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="genderbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">AGE GROUPS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="agegroupbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">PLACEMENTS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="placementbody">
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="data-row open-close">
							<div class="frame d-flex flex-wrap align-items-center">
								<div class="data-holder d-flex flex-wrap align-items-center">
									<div class="column select-column">
										<div class="select-holder">
											<select class="ParamDD" onchange="getDetailData(this);">
											    <option value="page_engagement">Engagement</option>
											    <option value="impressions">Impressions</option>
												<option value="inline_link_clicks">Clicks</option>
											    <option value="offsite_conversion">Conversion</option>
												<option value="like">Page Like</option>
												<option value="video_view">3-Second Video Views</option>
												<option value="app_install">App Installs</option>
                                                <option value="leadgen_other">Leads (Form)</option>
											    <option value="mobile_app_install">Mobile App Installs</option>
											    <option value="offsite_conversion_fb_pixel_add_payment_info">Adds Payment Info</option>
											    <option value="offsite_conversion_fb_pixel_add_to_cart">Adds To Cart</option>
											    <option value="offsite_conversion_fb_pixel_add_to_wishlist">Adds To Wishlist</option>
											    <option value="offsite_conversion_fb_pixel_complete_registration">Completed Registration</option>
											    <option value="offsite_conversion_fb_pixel_initiate_checkout">Initiates Checkout</option>
											    <option value="offsite_conversion_fb_pixel_lead">Leads</option>
											    <option value="offsite_conversion_fb_pixel_purchase">Purchases</option>
											    <option value="offsite_conversion_fb_pixel_search">Searches</option>
											    <option value="offsite_conversion_fb_pixel_view_content">Views Content</option>
											    <option value="offsite_conversion_key_page_view">Key Page Views</option>
											    <option value="onsite_conversion_messaging_first_reply">New Messaging Conversations</option> 
											    
											</select>
										</div>
									</div>
									<div class="column firstval">
										<a class="tab-opener" href="#">
											<?php 
                                if(round($adaccounts['add_header']['spent']) < 1000){
                                    echo '<strong class="value">';
                                                }else{
                                    echo '<strong class="value spentli">';
                                                }
                                echo number_format($adaccounts['add_header']['inline_post_engagement']); ?></strong>
											<span class="title">Engages</span>
										</a>
									</div>
									<div class="column secondval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php
                                echo $this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['inline_post_engagement']),2);
                            ?></strong>
											<span class="title">Per Engage</span>
										</a>
									</div>
									<div class="column thirdval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php if ($adaccounts['add_header']['impressions'] > 0)
                            {
                                $inline_posteng = ($adaccounts['add_header']['inline_post_engagement'] / $adaccounts['add_header']['impressions'])*100;
                                if ($inline_posteng > 0)
                                {
                                    echo round(number_format($inline_posteng, 3, '.', ','),2)."%";
                                }
                            }
                             ?></strong>
											<span class="title">Engage Rate</span>
										</a>
									</div>
									<div class="column btn-column">
										<a href="#" class="btn btn-primary opener" onclick="return GetBreakdownAdone(this);">BREAKDOWN</a>
									</div>
								</div>
								<div class="action">
									<a href="#" class="btn btn-remove" onclick="return removenewanalytics(this);"><span class="icon-bin"></span></a>
								</div>
							</div>
							<div class="slide">
								<div class="breakdown-data d-flex flex-wrap">
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">GENDER</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="genderbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">AGE GROUPS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="agegroupbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">PLACEMENTS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="placementbody">
											
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="campaign-list">
					    <div class="ad-manage-block">
                            <div class="container-fluid">
                              <div class="row">
                                <div class="col-xs-12">
                                 <!-- <div class="btns-holder">
                                      <a class="btn btn-save" href="#"><i class="icon-heart"></i> <span>MODIFY AD</span></a> 
                                    <a class="btn btn-save" href="#"><i class="icon-heart"></i> <span>DUPLICATE AD</span></a>
                                    <?php 
                                      if($adaccounts['add'][0]['effective_status'] == 'ACTIVE'){
                                        ?>
                                          <a class="btn btn-save" href="javascript:void(0);" onclick="toolStartStop('PAUSED', 'adddsReport', '<?php echo $addId; ?>');"><i class="icon-heart"></i> <span>TURN OFF AD</span></a>
                                        <?php
                                      }
                                      else if($adaccounts['add'][0]['effective_status'] == 'PAUSED'){
                                        ?>
                                          <a class="btn btn-save" href="javascript:void(0);" onclick="toolStartStop('ACTIVE', 'adddsReport', '<?php echo $addId; ?>');"><i class="icon-heart"></i> <span>TURN ON AD</span></a>
                                        <?php
                                      }
                                      ?>
                                    <a class="btn btn-save" href="<?php echo base_url().'automaticoptimization'; ?>"><i class="icon-heart"></i> <span>SET AUTOMATION RULE</span></a>
                                  </div> -->
                                </div>
                              </div>
                              <div class="row same-height">
                                <div class="col-md-6">
                                  <div class="ad-live-prev height">
                                    <div class="header">
                                     <br>
                                      <center> Live Preview Of Your Desktop Ad </center>
                                    </div>
                                    <div class="ad-holder">
                                       <div id="ads_preview_desktopfeed" class="ads-preview-panel previews-desktopfeed" align="center">
                                            <iframe id="desktop-view" src="<?php echo $adaccounts['add_header']['desktop_view']; ?>" scrolling="no" frameBorder="0" style="width:503px !important; height: 579px;min-height:500px;margin:15px auto;" align="middle"></iframe>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="ad-live-prev height">
                                    <div class="header">
                                     <br>
                                      <center>Live Preview Of Your Mobile Ad</center>
                                    </div>
                                    <div class="ad-holder">
                                       <div id="ads_preview_mobilefeed" class="ads-preview-panel previews-desktopfeed" align="center">
                                            <iframe id="mobile-view" src="<?php echo $adaccounts['add_header']['mobile_view']; ?>"  scrolling="no" frameBorder="0" style="width:323px !important; height: 497px;min-height:500px;margin:15px auto;" align="middle"></iframe>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>	
					</div>
					
				</div>
				
										
				<input type="hidden" id="hidimpressions" name="hidimpressions" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['impressions']).'</strong><span class="title">Impressions</span></a>'; ?>'>
				<?php
				$cpm = '';
                 
                if($adaccounts['add_header']['impressions'] != '0'){
								$subcpc = $adaccounts['add_header']['impressions']/1000;
								$cpm = $this->session->userdata('cur_currency').round($adaccounts['add_header']['spent']/$subcpc,2);
							}    
    			 ?>
    			<input type="hidden" id="hid2impressions" name="hid2impressions" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$cpm.'</strong><span class="title">CPM</span></a>'; ?>'>
				<input type="hidden" id="hid3impressions" name="hid3impressions" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['newFrequ'], 2).'</strong><span class="title">Frequency</span></a>'; ?>'>
				
				
				
				<input type="hidden" id="hidinline_link_clicks" name="hidinline_link_clicks" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['inline_link_clicks']).'</strong><span class="title">Clicks</span></a>'; ?>'>
				<?php
				$perclick = '';
                    
    			if ($adaccounts['add_header']['spent'] > 0)
                            {
                                $cost_per_click = $adaccounts['add_header']['spent'] / $adaccounts['add_header']['inline_link_clicks'];
                                if ($cost_per_click > 0)
                                {
                                    $perclick = $this->session->userdata('cur_currency') . number_format($cost_per_click, 2, '.', ',');
                                }
                                else
                                {
                                    $perclick = $this->session->userdata('cur_currency') . number_format($cost_per_click);
                                }
                            }
                            else
                            {
                                $perclick = $this->session->userdata('cur_currency') . $adaccounts['add_header']['inline_link_clicks'];
                            } ?>
    			<input type="hidden" id="hid2inline_link_clicks" name="hid2inline_link_clicks" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$perclick.'</strong><span class="title">Per Click</span></a>'; ?>'>
				<input type="hidden" id="hid3inline_link_clicks" name="hid3inline_link_clicks" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.round(number_format($click_rate, 3, '.', ','),2).'%</strong><span class="title">Click Rate</span></a>'; ?>'>
				
				
				<input type="hidden" id="hidpage_engagement" name="hidpage_engagement" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['inline_post_engagement']).'</strong><span class="title">Engages</span></a>'; ?>'>
				<?php
				$eng_rate = '';
                    
    			if ($adaccounts['add_header']['impressions'] > 0)
                            {
                                $inline_posteng = ($adaccounts['add_header']['inline_post_engagement'] / $adaccounts['add_header']['impressions'])*100;
                                if ($inline_posteng > 0)
                                {
                                    $eng_rate = round(number_format($inline_posteng, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2page_engagement" name="hid2page_engagement" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['inline_post_engagement']),2).'</strong><span class="title">Per Engage</span></a>'; ?>'>
				<input type="hidden" id="hid3page_engagement" name="hid3page_engagement" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$eng_rate.'</strong><span class="title">Engage Rate</span></a>'; ?>'>

				
				<input type="hidden" id="hidlike" name="hidlike" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['page_like']).'</strong><span class="title">Page Likes</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['impressions'] > 0)
                            {
                                $page_like = ($adaccounts['add_header']['page_like'] / $adaccounts['add_header']['impressions'])*100;
                                if ($page_like > 0)
                                {
                                    $pg_rate = round(number_format($page_like, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2like" name="hid2like" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['page_like']), 2).'</strong><span class="title">Per Page Like</span></a>'; ?>'>
				<input type="hidden" id="hid3like" name="hid3like" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Page Like Rate</span></a>'; ?>'>

				<input type="hidden" id="hidoffsite_conversion" name="hidoffsite_conversion" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['actions']).'</strong><span class="title">Conversion</span></a>'; ?>'>
                <input type="hidden" id="hid2offsite_conversion" name="hid2offsite_conversion" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['actions']),2).'</strong><span class="title">Per Conversion</span></a>'; ?>'>
				<?php
				$con_rate = '';
				if ($adaccounts['add_header']['impressions'] > 0)
                            {
                                $offline_conversion = ($adaccounts['add_header']['actions'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offline_conversion > 0)
                                {
                                    $con_rate = round(number_format($offline_conversion, 3, '.', ','),2)."%";
                                }
								else{
									$con_rate = "0%";
								}
                            }
                             ?>
				<input type="hidden" id="hid3offsite_conversion" name="hid3offsite_conversion" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$con_rate.'</strong><span class="title">Conversion Rate</span></a>'; ?>'>
<!--NEW DATA POINTS HERE SAMY-->
			    	<input type="hidden" id="hidvideo_view" name="hidvideo_view" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['total_video_3_sec_views']).'</strong><span class="title">Video Views</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['total_video_3_sec_views'] > 0)
                            {
                                $total_video_3_sec_views = ($adaccounts['add_header']['total_video_3_sec_views'] / $adaccounts['add_header']['impressions'])*100;
                                if ($total_video_3_sec_views > 0)
                                {
                                    $pg_rate = round(number_format($total_video_3_sec_views, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2video_view" name="hid2video_view" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['total_video_3_sec_views']), 2).'</strong><span class="title">Per Video Views</span></a>'; ?>'>
				<input type="hidden" id="hid3video_view" name="hid3video_view" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Video Views Rate</span></a>'; ?>'>
			
		
			  <input type="hidden" id="hidapp_install" name="hidapp_install" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['app_install']).'</strong><span class="title">App Installs</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['app_install'] > 0)
                            {
                                $app_install = ($adaccounts['add_header']['app_install'] / $adaccounts['add_header']['impressions'])*100;
                                if ($app_install > 0)
                                {
                                    $pg_rate = round(number_format($app_install, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2app_install" name="hid2app_install" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['app_install']), 2).'</strong><span class="title">Per App Installs</span></a>'; ?>'>
				<input type="hidden" id="hid3app_install" name="hid3app_install" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">App Installs Rate</span></a>'; ?>'>
			
			  <input type="hidden" id="hidleadgen_other" name="hidleadgen_other" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['total_leadgen_other']).'</strong><span class="title">Leads (Form)</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['total_leadgen_other'] > 0)
                            {
                                $total_leadgen_other = ($adaccounts['add_header']['total_leadgen_other'] / $adaccounts['add_header']['impressions'])*100;
                                if ($total_leadgen_other > 0)
                                {
                                    $pg_rate = round(number_format($total_leadgen_other, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2leadgen_other" name="hid2leadgen_other" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['total_leadgen_other']), 2).'</strong><span class="title">Per Leads (Form)</span></a>'; ?>'>
				<input type="hidden" id="hid3leadgen_other" name="hid3leadgen_other" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Leads (Form) Rate</span></a>'; ?>'>
				
											
                     <input type="hidden" id="hidmobile_app_install" name="hidmobile_app_install" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['total_mobile_app_install']).'</strong><span class="title">Mobile App Installs</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['total_mobile_app_install'] > 0)
                            {
                                $total_mobile_app_install = ($adaccounts['add_header']['total_mobile_app_install'] / $adaccounts['add_header']['impressions'])*100;
                                if ($total_mobile_app_install > 0)
                                {
                                    $pg_rate = round(number_format($total_mobile_app_install, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2mobile_app_install" name="hid2mobile_app_install" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['total_mobile_app_install']), 2).'</strong><span class="title">Per Mobile App Installs</span></a>'; ?>'>
				<input type="hidden" id="hid3mobile_app_install" name="hid3mobile_app_install" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Mobile App Installs Rate</span></a>'; ?>'>                          
				 <input type="hidden" id="hidoffsite_conversion_fb_pixel_add_payment_info" name="hidoffsite_conversion_fb_pixel_add_payment_info" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_add_payment_info']).'</strong><span class="title">Adds Payment Info</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_add_payment_info'] > 0)
                            {
                                $offsite_conversion_fb_pixel_add_payment_info = ($adaccounts['add_header']['offsite_conversion_fb_pixel_add_payment_info'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_add_payment_info > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_add_payment_info, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_add_payment_info" name="hid2offsite_conversion_fb_pixel_add_payment_info" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_add_payment_info']), 2).'</strong><span class="title">Per Adds Payment Info</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_add_payment_info" name="hid3offsite_conversion_fb_pixel_add_payment_info" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Adds Payment Info Rate</span></a>'; ?>'>							   
				<input type="hidden" id="hidoffsite_conversion_fb_pixel_add_to_cart" name="hidoffsite_conversion_fb_pixel_add_to_cart" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_add_to_cart']).'</strong><span class="title">Adds To Cart</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_add_to_cart'] > 0)
                            {
                                $offsite_conversion_fb_pixel_add_to_cart = ($adaccounts['add_header']['offsite_conversion_fb_pixel_add_to_cart'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_add_to_cart > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_add_to_cart, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_add_to_cart" name="hid2offsite_conversion_fb_pixel_add_to_cart" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_add_to_cart']), 2).'</strong><span class="title">Per Adds To Cart</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_add_to_cart" name="hid3offsite_conversion_fb_pixel_add_to_cart" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Adds To Cart Rate</span></a>'; ?>'>							   
					
				<input type="hidden" id="hidoffsite_conversion_fb_pixel_add_to_wishlist" name="hidoffsite_conversion_fb_pixel_add_to_wishlist" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_add_to_wishlist']).'</strong><span class="title">Adds To Wishlist</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_add_to_wishlist'] > 0)
                            {
                                $offsite_conversion_fb_pixel_add_to_wishlist = ($adaccounts['add_header']['offsite_conversion_fb_pixel_add_to_wishlist'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_add_to_wishlist > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_add_to_wishlist, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_add_to_wishlist" name="hid2offsite_conversion_fb_pixel_add_to_wishlist" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_add_to_wishlist']), 2).'</strong><span class="title">Per Adds To Wishlist</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_add_to_wishlist" name="hid3offsite_conversion_fb_pixel_add_to_wishlist" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Adds To Wishlist Rate</span></a>'; ?>'>							
							
				<input type="hidden" id="hidoffsite_conversion_fb_pixel_complete_registration" name="hidoffsite_conversion_fb_pixel_complete_registration" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_complete_registration']).'</strong><span class="title">Completed Registration</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_complete_registration'] > 0)
                            {
                                $offsite_conversion_fb_pixel_complete_registration = ($adaccounts['add_header']['offsite_conversion_fb_pixel_complete_registration'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_complete_registration > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_complete_registration, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_complete_registration" name="hid2offsite_conversion_fb_pixel_complete_registration" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_complete_registration']), 2).'</strong><span class="title">Per Completed Registration</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_complete_registration" name="hid3offsite_conversion_fb_pixel_complete_registration" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Completed Registration Rate</span></a>'; ?>'>	
				
				<input type="hidden" id="hidoffsite_conversion_fb_pixel_initiate_checkout" name="hidoffsite_conversion_fb_pixel_initiate_checkout" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_initiate_checkout']).'</strong><span class="title">Initiates Checkout</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_initiate_checkout'] > 0)
                            {
                                $offsite_conversion_fb_pixel_initiate_checkout = ($adaccounts['add_header']['offsite_conversion_fb_pixel_initiate_checkout'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_initiate_checkout > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_initiate_checkout, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_initiate_checkout" name="hid2offsite_conversion_fb_pixel_initiate_checkout" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_initiate_checkout']), 2).'</strong><span class="title">Per Initiates Checkout</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_initiate_checkout" name="hid3offsite_conversion_fb_pixel_initiate_checkout" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Initiates Checkout Rate</span></a>'; ?>'>	
				
				<input type="hidden" id="hidoffsite_conversion_fb_pixel_lead" name="hidoffsite_conversion_fb_pixel_lead" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_lead']).'</strong><span class="title">Leads</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_lead'] > 0)
                            {
                                $offsite_conversion_fb_pixel_lead = ($adaccounts['add_header']['offsite_conversion_fb_pixel_lead'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_lead > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_lead, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_lead" name="hid2offsite_conversion_fb_pixel_lead" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_lead']), 2).'</strong><span class="title">Per Leads</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_lead" name="hid3offsite_conversion_fb_pixel_lead" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Leads Rate</span></a>'; ?>'>	

				<input type="hidden" id="hidoffsite_conversion_fb_pixel_purchase" name="hidoffsite_conversion_fb_pixel_purchase" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_purchase']).'</strong><span class="title">Purchases</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_purchase'] > 0)
                            {
                                $offsite_conversion_fb_pixel_purchase = ($adaccounts['add_header']['offsite_conversion_fb_pixel_purchase'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_purchase > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_purchase, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_purchase" name="hid2offsite_conversion_fb_pixel_purchase" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_purchase']), 2).'</strong><span class="title">Per Purchases</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_purchase" name="hid3offsite_conversion_fb_pixel_purchase" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Purchases Rate</span></a>'; ?>'>
				
				<input type="hidden" id="hidoffsite_conversion_fb_pixel_search" name="hidoffsite_conversion_fb_pixel_search" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_search']).'</strong><span class="title">Searches</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_search'] > 0)
                            {
                                $offsite_conversion_fb_pixel_search = ($adaccounts['add_header']['offsite_conversion_fb_pixel_search'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_search > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_search, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_search" name="hid2offsite_conversion_fb_pixel_search" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_search']), 2).'</strong><span class="title">Per Searches</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_search" name="hid3offsite_conversion_fb_pixel_search" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Searches Rate</span></a>'; ?>'>
				
				<input type="hidden" id="hidoffsite_conversion_fb_pixel_view_content" name="hidoffsite_conversion_fb_pixel_view_content" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_fb_pixel_view_content']).'</strong><span class="title">Views Content</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_fb_pixel_view_content'] > 0)
                            {
                                $offsite_conversion_fb_pixel_view_content = ($adaccounts['add_header']['offsite_conversion_fb_pixel_view_content'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_fb_pixel_view_content > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_fb_pixel_view_content, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_fb_pixel_view_content" name="hid2offsite_conversion_fb_pixel_view_content" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_fb_pixel_view_content']), 2).'</strong><span class="title">Per Views Content</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_fb_pixel_view_content" name="hid3offsite_conversion_fb_pixel_view_content" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Views Content Rate</span></a>'; ?>'>
				
				<input type="hidden" id="hidoffsite_conversion_key_page_view" name="hidoffsite_conversion_key_page_view" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['offsite_conversion_key_page_view']).'</strong><span class="title">Key Page Views</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['offsite_conversion_key_page_view'] > 0)
                            {
                                $offsite_conversion_key_page_view = ($adaccounts['add_header']['offsite_conversion_key_page_view'] / $adaccounts['add_header']['impressions'])*100;
                                if ($offsite_conversion_key_page_view > 0)
                                {
                                    $pg_rate = round(number_format($offsite_conversion_key_page_view, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2offsite_conversion_key_page_view" name="hid2offsite_conversion_key_page_view" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['offsite_conversion_key_page_view']), 2).'</strong><span class="title">Per Key Page Views</span></a>'; ?>'>
				<input type="hidden" id="hid3offsite_conversion_key_page_view" name="hid3offsite_conversion_key_page_view" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">Key Page Views Rate</span></a>'; ?>'>
				
				<input type="hidden" id="hidonsite_conversion_messaging_first_reply" name="hidonsite_conversion_messaging_first_reply" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.number_format($adaccounts['add_header']['onsite_conversion_messaging_first_reply']).'</strong><span class="title">New Messaging Conversations</span></a>'; ?>'>
				<?php
				$pg_rate = '';
                    
    			if ($adaccounts['add_header']['onsite_conversion_messaging_first_reply'] > 0)
                            {
                                $onsite_conversion_messaging_first_reply = ($adaccounts['add_header']['onsite_conversion_messaging_first_reply'] / $adaccounts['add_header']['impressions'])*100;
                                if ($onsite_conversion_messaging_first_reply > 0)
                                {
                                    $pg_rate = round(number_format($onsite_conversion_messaging_first_reply, 3, '.', ','),2)."%";
                                }
                            } ?>
    			<input type="hidden" id="hid2onsite_conversion_messaging_first_reply" name="hid2onsite_conversion_messaging_first_reply" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$this->session->userdata('cur_currency').round(($adaccounts['add_header']['spent']/$adaccounts['add_header']['onsite_conversion_messaging_first_reply']), 2).'</strong><span class="title">Per New Messaging Conversations</span></a>'; ?>'>
				<input type="hidden" id="hid3onsite_conversion_messaging_first_reply" name="hid3onsite_conversion_messaging_first_reply" value='<?php echo '<a class="tab-opener" href="#"><strong class="value">'.$pg_rate.'</strong><span class="title">New Messaging Conversations Rate</span></a>'; ?>'>
				<!--END DP -->
				
				
			<input type="hidden" id="addAccountId" name="addAccountId" value="<?php echo $addAccountId; ?>">
        <input type="hidden" id="campaignId" name="campaignId" value="<?php echo $campaignId ?>">
        <input type="hidden" id="addsetId" name="addsetId" value="<?php echo $addsetId ?>">
        <input type="hidden" id="addId" name="addId" value="<?php echo $addId ?>">
        <input type="hidden" id="pageName" name="pageName" value="adReport">
        <input type="hidden" id="dayLimit" name="dayLimit" value="<?php echo $this->session->userdata('dateval'); ?>">
        
      </main>