<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
<style>
.nav-link{
	color:#fff;
}
.readmore
{
	display:none;
}
.display-inline {
	display: inline !important;
}
</style>
<main id="main">
				<div class="main-content">
				
					<div class="container help-container">
						<div class="row">
							<div class="col-xl-3 col-md-4">
								<aside id="sidebar">
									<nav class="side-nav">
									<ul class="nav nav-tabs">
											<li class="side-nav-item active">
												<a data-toggle="tab" href="#menu1" class="side-nav-link">CREATION TOOL</a>
											</li>
											<li class="side-nav-item">
												<a data-toggle="tab" href="#menu2" class="side-nav-link">OPTIMIZATION TOOL</a>
											</li>
											<li class="side-nav-item">
												<a data-toggle="tab" href="#menu3" class="side-nav-link">ANALYSIS TOOL</a>
											</li>
											<li class="side-nav-item">
												<a data-toggle="tab" href="#menu4" class="side-nav-link">REPORTING TOOL</a>
											</li>
											<li class="side-nav-item">
												<a data-toggle="tab" href="#menu5" class="side-nav-link">BILLING</a>
											</li>
											<li class="side-nav-item">
												<a data-toggle="tab" href="#menu6" class="side-nav-link">AD ACCOUNTS</a>
											</li>
											<li class="side-nav-item">
												<a data-toggle="tab" href="#menu7" class="side-nav-link">DONE FOR YOU SERVICE</a>
											</li>
										</ul>
									</nav>
								</aside>
							</div>
							<div class="col-xl-7 col-md-8">
								<div id="content" class="tab-content">
								<div id="menu1" class="tab-pane in active">
									<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="article-image">
											<a href="#"><img class="img-fluid" src="<?php echo $this->config->item('assets'); ?>newdesign/images/img01.jpg" alt="image description" width="604" height="340"></a>
										</div>
										<div class="text-holder">
											<div>Article 1
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									
									</article>
									</div>
									<div id="menu2" class="tab-pane ">
									<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
											<!--	<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div> -->
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="article-image">
											<iframe width="560" height="315" src="https://www.youtube.com/embed/74onfxeIZL4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
										</div>
										<div class="text-holder">
										<div>Article 2
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="article-image">
											<a href="#"><img class="img-fluid" src="<?php echo $this->config->item('assets'); ?>newdesign/images/img01.jpg" alt="image description" width="604" height="340"></a>
										</div>
										<div class="text-holder">
										<div>Article 3
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									</div>
										<div id="menu3" class="tab-pane ">
									<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="text-holder">
											<div>Article 4
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="article-image">
											<a href="#"><img class="img-fluid" src="<?php echo $this->config->item('assets'); ?>newdesign/images/img01.jpg" alt="image description" width="604" height="340"></a>
										</div>
										<div class="text-holder">
											<div>Article 5
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									</div>
										<div id="menu4" class="tab-pane ">
										<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="text-holder">
											<div>Article 6
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									</div>
										<div id="menu5" class="tab-pane ">
										<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="text-holder">
											<div>Article 7
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									</div>
										<div id="menu6" class="tab-pane ">
										<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="article-image">
											<a href="#"><img class="img-fluid" src="<?php echo $this->config->item('assets'); ?>newdesign/images/img01.jpg" alt="image description" width="604" height="340"></a>
										</div>
										<div class="text-holder">
											<div>Article 8
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
										<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="text-holder">
										<div>Article 9
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									</div>
										<div id="menu6" class="tab-pane ">
										<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="text-holder">
											<div>Article 10
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									</div>
										<div id="menu7" class="tab-pane ">
										<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="article-image">
											<a href="#"><img class="img-fluid" src="<?php echo $this->config->item('assets'); ?>newdesign/images/img01.jpg" alt="image description" width="604" height="340"></a>
										</div>
										<div class="text-holder">
											<div>Article 11
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
										<article class="help-articale">
										<header class="articale-header">
											<div class="meta d-flex align-items-center">
												<div class="avatar">
													<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/avatar01.jpg" alt="image description" width="40" height="40">
												</div>
												<div class="meta-detail">
													<span><a href="#">Christine Curtis</a> in <a href="#">Design</a></span>
													<span class="publisted-date">Yesterday</span>
												</div>
											</div>
											<h2 class="article-heading"><a href="#">How to Create a Clicks to Website Campaign</a></h2>
										</header>
										<div class="text-holder">
											<div>Article 12
											<div class="readmore">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat… </div> <a href="javascript:void(0);" class="readmore_a">Read more</a></div>
										</div>
									</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</main>
    </div>
	<script>
$(document).ready(function(){
	$('.readmore_a' ).on( "click", function( event ) {
		$( event.target ).prev().toggleClass('display-inline');
		if($( event.target ).html()=="Read more"){
		$( event.target ).html('Read less');}else{
		$( event.target ).html('Read more'); }
	});
});
</script>