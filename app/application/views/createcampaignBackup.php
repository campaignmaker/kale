<div  id="preloaderMain" style="display:none;">
            <div id="statusMain" style="font-size:14px;color:white">
                <i class="fa fa-spinner fa-spin" style="font-size:48px;color:white"></i></br>
              <span></span>
            </div>
    </div>


<?php defined('BASEPATH') OR exit('No direct script access allowed');  ?>
	<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1198746903472361',
            xfbml: true,
            version: 'v2.6'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


	</script>
    <?php $email = $this->session->userdata['logged_in']['email']; ?>
    <form accept-charset="utf-8" method="post" enctype="multipart/form-data" name="create_creativeads_frm" id="create_creativeads_frm" action="https://localhost/fb_marketing/createcampaign">    
        <main class="main11 maincont">
				<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step active">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s1">
								<div class="step-holder d-flex flex-wrap">
									<div class="step active">
										<strong class="step-name">Name</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Ad Account</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step not_show">
										<strong class="step-name">Objective</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							
							<div class="top-notification-box" id="msg_box1" style="display: none;">
                                <i class="icon-warning"></i>
                                <span class="text" id="msg_err1">Please enter campaign name</span>
                            </div>
							
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Name Your Campaign
										<span class="sub-heading">
											Write the name of your campaign *only you will see it*
											<strong class="highlight-text">Pro tip:  Write the objective and date to keep track.</strong>
										</span>
									</h2>
									<div class="form-wrap s1">
											<div class="form-group">
											    <input onchange="setCampaignName()" name="campaign[compaingname]" id="compaingname" type="text" class="form-control campaign-name" placeholder="type your campaign name here.." value="<?= isset($Cmpdraft->campaignname) ? $Cmpdraft->campaignname : '' ?>">
											</div>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									
									<div class="btn-col">
										<a href="javascript:void(0)" onclick="ad_design_darafts('loader_drft2', 'direct');CreateCampaign('step11')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main12 maincont" style="display:none;">
				<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step active">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s1">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Name</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Ad Account</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Objective</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="top-notification-box" id="msg_box11" style="display: none;">
                                <i class="icon-warning"></i>
                                <span class="text" id="msg_err1">Please select ad account</span>
                            </div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose Your Billing Account
										<span class="sub-heading">
											The ad account you choose will be billed for the FB spend.
											<strong class="highlight-text">Pro tip:  make sure you have a valid payment method in your Facebook ad account.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
											<div class="form-group">
												<div class="select-holder s2">
												    <select class="ad_accounts_fields" id="adaccountid" name="campaign[adaccountid]"  onchange="get_fb_pages(this);">
                                                        <option value="">Choose Your Ad Account</option>
                          								                <?php foreach ($getAdAccountes as $AdAccounte) { ?>
                              
                                                              <option value="<?php echo $AdAccounte->ad_account_id ?>" <?= $AdAccounte->ad_account_id == $Cmpdraft->adaccount ? "selected" : "" ?>>
                                                                  <?php echo $AdAccounte->add_title ?>
                                                              </option>
                                                          <?php }
                                                          ?>
                                                      </select>
												
												</div>
											</div>
											<!--<div class="form-group">
												<div class="select-holder s2">
													<select>
														<option>Choose Your Ad Account</option>
														<option>Choose Your Ad Account 1</option>
														<option>Choose Your Ad Account 2</option>
													</select>
												</div>
											</div>-->
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main11').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" onclick="ad_design_darafts('loader_drft2', 'direct');CreateCampaign('step12')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main13 maincont" style="display:none;">
				<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step active">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s1">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Name</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Ad Account</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Objective</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose Your Campaign Objective
										<span class="sub-heading">
											Choosing the right objective will have a drastic impact on your campaign.
											<strong class="highlight-text">Pro tip:  choose an objective that you want to achieve.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
										
											<div class="form-group">
												<div class="select-holder s2">
												    
													<select onchange="setbyobjective(this);" id="objective_id">
													    <option value="">Choose a Campaign Objective</option>
														<option value="LINK_CLICKS" <?= "LINK_CLICKS" == $Cmpdraft->adobjective ? "selected" : "" ?>>Clicks to Website</option>
														<option value="CONVERSIONS" <?= "CONVERSIONS" == $Cmpdraft->adobjective ? "selected" : "" ?>>Conversions</option>
														<option value="LEAD_GENERATION" <?= "LEAD_GENERATION" == $Cmpdraft->adobjective ? "selected" : "" ?>>Lead Generation</option>
														<option value="PAGE_LIKES" <?= "PAGE_LIKES" == $Cmpdraft->adobjective ? "selected" : "" ?>>Page Likes</option>
													<!--	<option value="PRODUCT_CATALOG_SALES" <?= "PRODUCT_CATALOG_SALES" == $Cmpdraft->adobjective ? "selected" : "" ?>>Multi-Product</option> -->
														<option value="POST_ENGAGEMENT" <?= "POST_ENGAGEMENT" == $Cmpdraft->adobjective ? "selected" : "" ?>>Post Engagement</option>
													</select>
												</div>
											</div>
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main12').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="CreateCampaign('step13')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Go To Adcopy
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			
			<main class="main19 maincont adcopy" style="display:none;">
				 
				<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Load Your Saved Adcopy
										<span class="sub-heading">
											Load an adcopy you previously saved using TCM Or start from scratch.
											<strong class="highlight-text">Pro tip: TCM automatically saves after each step!</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
									    
											<div class="big-btn-holder">
												<div class="btn-row d-flex flex-wrap justify-content-center">
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom d-flex flex-wrap">
															<input type="radio" checked="" name="save" id="new-rule">
															<label for="new-rule" class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-megaphone-rounded.svg" alt="megaphone">
																</span>
																<span class="text" id="newadcopy">
																	<span class="data">New Adcopy</span>
																	<span class="label">Start from scratch.</span>
																</span>
															</label>
														</div>
													</div>
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom d-flex flex-wrap">
															<input type="radio"  name="save" id="load-rule">
															<label for="load-rule"  class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-save-rounded.svg" alt="save">
																</span>
																<span class="text data load_drft" id="<?php echo $userId; ?>">
																	<span class="data load_drft" >Load Adcopy</span>
																	<span class="label" >Can be edited.</span>
																</span>
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group s2">
												<div class="select-holder s2" id="adcopytemplateholder" style="display:none">
													<select id="adcopytemplate" >
														<option>Choose your ad template</option>
													
													</select>
												</div>
											</div>
										
											
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main19')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main19');" >
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</main>
		
			<main class="main20 maincont adcopy" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step active">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Facebook Page
										<span class="sub-heading">
											The Facebook page will represent your ad to users..
											<strong class="highlight-text">Pro tip:  having a well designed page profile picture will help your CTR.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
											<div class="form-group">
												<div class="select-holder s2">
													<input type="hidden" name="creatives[adaccountid][]" id="adaccountid_1" value="<?=isset($Cmpdraft->adaccount)?$Cmpdraft->adaccount:''?>" />
													<input type="hidden" name="creatives[user_id][]" id="user_id" value="<?=isset($userId)?$userId:''?>" />
							
                    
                                                    <select id="objectIdpage" name="creatives[object_id][]" onchange="get_pages_tabs();">
														<?php foreach ($getUsersPages as $page) { ?>
                            <option value="<?php echo $page['id'] ?>" <?= ($Cmpdraft->pageid == $page['id']) ? "selected" : ""; ?>><?php echo $page['name'] ?></option>
                                                    <?php } ?>
													</select>
												</div>
											</div>
									</div>
									
									
									<div class="form-wrap s2 select-wrap POST_ENGAGEMENT_OBJ displayNone">
											<div class="form-group">
												<div class="select-holder s2">
													<select name="creatives[story_id]" id="storyId" onchange="LoeadPreview();" class="thisispostselect">

													</select>
												</div>
											</div>
									</div>
									  <div id="leadform_wrapper" style="display:none">
									 	<h2 class="screen-heading text-center">
									     Select Your Lead Form
									     	<span class="sub-heading">
											The Facebook page will represent your ad to users..
											<strong class="highlight-text">Pro tip:  having a well designed page profile picture will help your CTR.</strong>
										</span>
									</h2>
									
									<div class="form-wrap s2">
											<div class="form-group">
												<div class="select-holder s2">
												     <select class="thisispostselect" name="creatives[lead_gen_form][]" id="leadGenForm">
                                                     <option value="">Please select Lead Gen form...</option>
                        
                                                     </select>
									</div>
									  </div>
									      </div>
									  </div>    
									
									
								</div>
							
								    
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main20')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main20');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			
			
			<main class="main21 maincont adcopy" style="display:none;">
				<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Headline
										<span class="sub-heading">
											This is typically the first thing people see in your ad.
											<strong class="highlight-text">Pro tip:  Write your core message here.</strong>
										</span>
									</h2>
									<div class="form-wrap s3" id="headline_fileds">
									    
											<div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col input-wrap">
													<div class="form-group mb-0">
														<input class="form-control addfield_textbox creatives_title1" type="text" placeholder="type your headline here.." name="creatives[title][]" onblur="stickyCounter(this.value, 'headline')"  value="<?= $Cmpdraft->headlines1 ?>">
													</div>
												</div>
												<div class="flex-col btn-wrap">
												    <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
												    <a href="#" class="btn btn-add add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" id="more_headline_fields">
														<span class="icon-holder icon-plus"></span>
													</a>
                                                      <?php } else { ?>
                                                      <a href="#" class="btn btn-add add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" onclick="showupgradepopup()">
														<span class="icon-holder icon-plus"></span>
													</a>
                                                     <?php } ?>
													
												</div>
											</div>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main21')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main21');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			
			
			
			<main class="main22 maincont adcopy" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Body Text
										<span class="sub-heading">
											People read this area when they want to learn more.
											<strong class="highlight-text">Pro tip:  write your keyfacts and end with CTA.</strong>
										</span>
									</h2>
									<div class="form-wrap s3" id="bodytext_fileds">
										
											<div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col input-wrap">
													<div class="form-group mb-0">
													    <textarea type="text" cols="30" rows="3" class="form-control addfield_textbox creatives_body1" name="creatives[body][]" onblur="stickyCounter(this.value, 'body')"   placeholder="type your body text here.."><?= $Cmpdraft->adbody1 ?></textarea>
														
													</div>
												</div>
												<div class="flex-col btn-wrap">
												    <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
												    <a href="#" class="btn btn-add add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" id="more_bodytext_fields">
														<span class="icon-holder icon-plus"></span>
													</a>
                                                      <?php } else { ?>
                                                      <a href="#" class="btn btn-add add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" onclick="showupgradepopup()">
														<span class="icon-holder icon-plus"></span>
													</a>
                                                     <?php } ?>
													
												</div>
											</div>
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main22')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main22');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main23 maincont adcopy" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Description
										<span class="sub-heading">
											People read this area last and may not be visible on mobile
											<strong class="highlight-text">Pro tip:  write a firm call to action message here.</strong>
										</span>
									</h2>
									<div class="form-wrap s3" id="description_fileds">
										    
										    <div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col input-wrap">
													<div class="form-group mb-0">
													    <textarea type="text" cols="30" rows="3" class="form-control addfield_textbox creatives_link_description1" name="creatives[link_description][]" onblur="stickyCounter(this.value, 'desc')"   placeholder="type your description here.."><?= $Cmpdraft->linkdescription1 ?></textarea>
														
													</div>
												</div>
												<div class="flex-col btn-wrap">
												    <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
												    <a href="#" class="btn btn-add add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" id="more_description_fields">
														<span class="icon-holder icon-plus"></span>
													</a>
                                                      <?php } else { ?>
                                                      <a href="#" class="btn btn-add add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" onclick="showupgradepopup()">
														<span class="icon-holder icon-plus"></span>
													</a>
                                                     <?php } ?>
													
												</div>
											</div>
										    
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main23')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main23');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main24 maincont adcopy" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Website URL
										<span class="sub-heading">
											Where do you want your ad traffic to go?
											<strong class="highlight-text">Pro tip:  TCM adds Google analytics tracking for you.</strong>
										</span>
									</h2>
									<div class="form-wrap s3" id="url_fileds">
										    
										    <div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col input-wrap">
													<div class="form-group mb-0">
													    <!--<input class="form-control addfield_textbox creatives_link_url1" type="text" name="creatives[link_url][]"   placeholder="your url must start with either http:// or https://" onchange="LoeadPreview();"  value="<?= $Cmpdraft->adurl1 ?>">-->
													     <!--<input class="form-control addfield_textbox creatives_link_url1" type="text" name="creatives[link_url][]"   placeholder="your url must start with either http:// or https://" onchange="LoeadPreview();"  value="<?php echo $this->config->item('base_url'); ?>">-->
													     <input class="form-control addfield_textbox creatives_link_url1" type="text" name="creatives[link_url][]"   placeholder="your url must start with either http:// or https://" onchange="LoeadPreview();"  value="">
													</div>
												</div>
												<div class="flex-col btn-wrap">
												    <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
												    <a href="#" class="btn btn-add add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" id="more_url_fields">
														<span class="icon-holder icon-plus"></span>
													</a>
                                                      <?php } else { ?>
                                                      <a href="#" class="btn btn-add add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" onclick="showupgradepopup()">
														<span class="icon-holder icon-plus"></span>
													</a>
                                                     <?php } ?>
													
												</div>
											</div>
										    
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main24')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main24');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
            <main class="main25 maincont adcopy" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose Your Ad Media
										<span class="sub-heading">
											To keep costs down, we do not store anything on our servers.
											<strong class="highlight-text">Pro tip:  In most cases Images perform better than Video.</strong>
										</span>
									</h2>
									<div class="media-library">
										<div class="header d-md-flex flex-wrap align-items-center">
											<span class="title">Media Library</span>
											<div class="media-btn-group d-flex flex-wrap align-items-center">
												<div class="btn-col">
													<!-- <input type="file"  id="file-1" class="inputfile" data-multiple-caption="{count} files selected" multiple />
													<label for="file-1"><span class="icon-upload"></span><span>Upload Image</span></label>-->

													<a href="javascript:void(0)" onclick="return openInNewTab('https://www.facebook.com/ads/manager/media/library/');" target="_blank" class="btn btn-save uploadbtn btn-upload"><span class="icon-upload"></span>Upload Image</a>
			
												</div>
												<div class="btn-col">
													<a href="javascript:void(0)" target="_blank" class="btn btn-upload uploadbtn" id="uplaodpagevid"><span class="icon-upload"></span>Upload Video</a>
												</div>
												<div class="btn-col">
													<a class="btn btn-refresh" id="getadcopyimage" href="javascript:void(0)" onclick="return fngetallimages('<?php echo $userId; ?>', 'gallery')"><span class="icon-refresh"></span>Refresh/Get Images</a>
												</div>
												<div class="btn-col">
													<a class="btn btn-refresh" id="getadcopyvideo" href="javascript:void(0)" onclick="return fngetallvideos('<?php echo $userId; ?>', 'gallery');"><span class="icon-refresh"></span>Refresh/Get Videos</a>
												</div>
											</div>
										</div>
										<input type="hidden" name="imgType" id="imgType" value="<?php echo !empty($Cmpdraft->imgType) ? $Cmpdraft->imgType : ''; ?>">
										<div id="uploadedImages">
										</div>
										<div class="form-wrap s3">
											
												<div class="media-holder">
													<div class="media-wrap d-sm-flex flex-wrap" id="gallery">
													<!--	<div class="media-item">
															<input id="img-01" type="checkbox" checked>
															<label for="img-01">
																<span class="img-holder">
																	<img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/img02.png" alt="image description">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">IMAGE</span>
																	<time datetime="2017-06-01">01 Jun 2017</time>
																</span>
															</label>
														</div>
														<div class="media-item">
															<input id="img-02" type="checkbox">
															<label for="img-02">
																<span class="img-holder">
																	<img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/img03.png" alt="image description">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">IMAGE</span>
																	<time datetime="2017-05-17">17 May 2017</time>
																</span>
															</label>
														</div>
														<div class="media-item">
															<input id="img-03" type="checkbox">
															<label for="img-03">
																<span class="img-holder">
																	<img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/img04.png" alt="image description">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">Video</span>
																	<time datetime="2017-05-30">17 May 2017</time>
																</span>
															</label>
														</div>
														<div class="media-item">
															<input id="img-04" type="checkbox">
															<label for="img-04">
																<span class="img-holder">
																	<img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/img02.png" alt="image description">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">IMAGE</span>
																	<time datetime="2017-06-01">01 Jun 2017</time>
																</span>
															</label>
														</div>
														<div class="media-item">
															<input id="img-05" type="checkbox">
															<label for="img-05">
																<span class="img-holder">
																	<img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/img03.png" alt="image description">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">IMAGE</span>
																	<time datetime="2017-05-17">17 May 2017</time>
																</span>
															</label>
														</div>
														<div class="media-item">
															<input id="img-06" type="checkbox">
															<label for="img-06">
																<span class="img-holder">
																	<img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/img04.png" alt="image description">
																</span>
																<span class="caption d-flex flex-wrap justify-content-between">
																	<span class="label">Video</span>
																	<time datetime="2017-05-30">17 May 2017</time>
																</span>
															</label>
														</div> -->
													</div>
												</div>
											
										</div>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main25');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main25');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main26 maincont adcopy" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Call To Action Button
										<span class="sub-heading">
											Choose one of Facebook’s Premade CTA buttons.
											<strong class="highlight-text">Pro tip:  choose one that works with your headline.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
										
											<div class="form-group">
												<div class="select-holder s2">
												    <select class="form-control call-to" name="creatives[call_to_action3][]" id="callToAction" onchange="LoeadPreview();">
												     <option value="LEARN_MORE"<?= $Cmpdraft->calltoaction == 'LEARN_MORE' ? "selected" : "" ?>>Learn more</option>
                                                      <option value="SHOP_NOW"  <?= $Cmpdraft->calltoaction == 'SHOP_NOW' ? "selected" : "" ?>>Shop now</option>
                                                      <option value="BOOK_TRAVEL"<?= $Cmpdraft->calltoaction == 'BOOK_TRAVEL' ? "selected" : "" ?>>Book now</option>
                                                      <option value="SIGN_UP"<?= $Cmpdraft->calltoaction == 'SIGN_UP' ? "selected" : "" ?>>Sign up</option>
                                                      <option value="DOWNLOAD"<?= $Cmpdraft->calltoaction == 'DOWNLOAD' ? "selected" : "" ?>>Download</option>
                                                      <option value="WATCH_MORE"<?= $Cmpdraft->calltoaction == 'WATCH_MORE' ? "selected" : "" ?>>Watch more</option>
                                                      <!--<option value="WATCH_VIDEO"<?= $Cmpdraft->calltoaction == 'WATCH_VIDEO' ? "selected" : "" ?>>Watch video</option>-->
                                                      <option value="LISTEN_MUSIC"<?= $Cmpdraft->calltoaction == 'LISTEN_MUSIC' ? "selected" : "" ?>>Listen Now</option>
                                                      <option value="INSTALL_APP"<?= $Cmpdraft->calltoaction == 'INSTALL_APP' ? "selected" : "" ?>>Install Now</option>
                                                      <option value="USE_APP"<?= $Cmpdraft->calltoaction == 'USE_APP' ? "selected" : "" ?>>Use App</option>
                                                      <option value="NO_BUTTON"<?= $Cmpdraft->calltoaction == 'NO_BUTTON' ? "selected" : "" ?>>No Button</option>
                                                    </select>
													
												</div>
											</div>
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main26');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main26');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main27 maincont adcopy" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Use a Published Page Post?
										<span class="sub-heading">
											You can use an already published post from your Facebook Page?ratch.
											<strong class="highlight-text">Pro tip:  using a published post can help accumulate likes and shares on one post.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										
											<div class="subscription">
												<div class="subscription-item d-flex align-items-center">
													<div class="subscription-controller">
														<label class="switch">
															<input type="checkbox" name="track_fb_pixel" id="checkbox3" value="Yes" <?php if($Cmpdraft->track_fb_pixel == 'Yes'){echo "checked";} ?>>
															<span class="slider"></span>
														</label>
													</div>
													<div class="subscription-detial">
														<strong class="title">Track your ads with Facebook pixels.</strong>
														<span class="info">You need to have Facebook pixels installed on your website.</span>
													</div>
												</div>
												<div class="subscription-item d-flex align-items-center">
													<div class="subscription-controller">
														<label class="switch">
															<input type="checkbox" name="track_google" id="checkbox4" value="Yes" <?php if($Cmpdraft->track_google == 'Yes'){echo "checked";} ?>>
															<span class="slider"></span>
														</label>
													</div>
													<div class="subscription-detial">
														<strong class="title">Track your ads with Google Analytics.</strong>
														<span class="info">You need to have Google analytics installed on your website.</span>
													</div>
												</div>
												
												
												
												  <div class="subscription-item  align-items-center" id="conv_pixel_wrapper" style="display:none"> 
    												
    													<div class="subscription-detial choose_conversion">
    														<strong class="title">Choose Your Conversion Pixel</strong>
    														<div class="select-holder s2">
    													    <select id="pixel_id1" class="page1 form-control" name="pixel_id1" >
                                                                  <option value="CONTENT_VIEW" <?= $Cmpdraft->conv_pixel == 'CONTENT_VIEW' ? "selected" : "" ?>>ViewContent</option>
                                                                  <option value="SEARCH" <?= $Cmpdraft->conv_pixel == 'SEARCH' ? "selected" : "" ?>>Search</option>
                                                                  <option value="ADD_TO_CART" <?= $Cmpdraft->conv_pixel == 'ADD_TO_CART' ? "selected" : "" ?>>AddToCart</option>
                                                                  <option value="ADD_TO_WISHLIST" <?= $Cmpdraft->conv_pixel == 'ADD_TO_WISHLIST' ? "selected" : "" ?>>AddToWishlist</option>
                                                                  <option value="INITIATED_CHECKOUT" <?= $Cmpdraft->conv_pixel == 'INITIATED_CHECKOUT' ? "selected" : "" ?>>InitiateCheckout</option>
                                                                  <option value="ADD_PAYMENT_INFO" <?= $Cmpdraft->conv_pixel == 'ADD_PAYMENT_INFO' ? "selected" : "" ?>>AddPaymentInfo</option>
                                                                  <option value="PURCHASE" <?= $Cmpdraft->conv_pixel == 'PURCHASE' ? "selected" : "" ?>>Purchase</option>
                                                                  <option value="LEAD" <?= $Cmpdraft->conv_pixel == 'LEAD' ? "selected" : "" ?>>Lead</option>
                                                                  <option value="COMPLETE_REGISTRATION" <?= $Cmpdraft->conv_pixel == 'COMPLETE_REGISTRATION' ? "selected" : "" ?>>CompleteRegistration</option>
                                                          </select>
                                                          </div>
    													</div>
												</div>
												
												
											</div>
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main27');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main27');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main28 maincont adcopy" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">Preview Your Ads</h2>
									<div class="ad-table d-flex flex-wrap">
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label" >Headlines</span>
												<span class="data" id="preview_headline">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label" >Media</span>
												<span class="data" id="preview_media">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label" >URL’s</span>
												<span class="data" id="preview_urls">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label" >Body Texts</span>
												<span class="data" id="preview_body_text">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label" >Description</span>
												<span class="data" id="preview_description">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell total d-flex flex-wrap">
												<span class="label" >Total Ads to Create</span>
												<span class="data" id="preview_ads_to_create">1</span>
											</div>
										</div>
									</div>
									
									<!-- <div class="button-main"> -->
                                      <?php 
                                          if(!empty($Cmpdraft->product1image)){
                                              $imgApp1 = explode('.', $Cmpdraft->product1image);
                                              $tempName1 = $imgApp1[0].'-gallery-p1';
                                          }
                                          if(!empty($Cmpdraft->product2image)){
                                              $imgApp2 = explode('.', $Cmpdraft->product2image);
                                              $tempName2 = $imgApp1[0].'-gallery-p2';
                                          }
                                          if(!empty($Cmpdraft->product3image)){
                                              $imgApp3 = explode('.', $Cmpdraft->product3image);
                                              $tempName3 = $imgApp1[0].'-gallery-p3';
                                          }
                                      ?>
                                      <input type="hidden" id="Product1ImgSet" name="Product1ImgSet" value="<?php echo !empty($tempName1) ? $tempName1 : ''?>">
                                      <input type="hidden" id="Product2ImgSet" name="Product2ImgSet" value="<?php echo !empty($tempName2) ? $tempName2 : ''?>">
                                      <input type="hidden" id="Product3ImgSet" name="Product3ImgSet" value="<?php echo !empty($tempName3) ? $tempName3 : ''?>">
                                      <input type="hidden" id="loadCampaing" name="loadCampaing" value="<?php echo !empty($Cmpdraft->ID) ? $Cmpdraft->ID : ''?>">
                                      <!--<button class="btn btn-success pull-right" type="button" onclick="ad_design_darafts('loader_drft2', 'direct');CreateCreativeAd();">Go to targeting</button> -->
                                   <!-- </div> -->
									
									<div id="uploadedImages-product">
                        <?php 
                          if (!empty($Cmpdraft->product1image)) { 
                            $adimagehashArr = explode('_', $Cmpdraft->adimagehash1);
                            if($adimagehashArr[1] == 'video'){
                              ?>
                                <input class="<?= $adimagehashArr[0] ?> " type="hidden" id="viewUrl1" name="product1_img_url" value="<?= $Cmpdraft->product1image ?>">
                                <input class="<?= $adimagehashArr[0] ?>" id="fbimghash1" type='hidden' name='fbimghash1' value='<?= $adimagehashArr[0] ?>' />
                              <?php
                            }
                            else{
                            $imgApp1 = explode('.', $Cmpdraft->product1image);
                          ?>
                            <input class="<?= $imgApp1[0].'-gallery-p1' ?> p1" type="hidden" id="viewUrl1" name="product1_img_url" value="https://fbcdn-creative-a.akamaihd.net/hads-ak-xtf1/t45.1600-4/<?= $Cmpdraft->product1image ?>">
                            <input class="<?= $imgApp1[0].'-gallery-p1' ?> p1" id="fbimghash1" type='hidden' name='fbimghash1' value='<?= $Cmpdraft->adimagehash1 ?>' />
                        <?php } }?>
                        <?php 
                          if (!empty($Cmpdraft->product2image)) { 
                            $adimagehashArr2 = explode('_', $Cmpdraft->adimagehash2);
                            if($adimagehashArr2[1] == 'video'){
                              ?>
                                <input class="<?= $adimagehashArr2[0] ?> " type="hidden" id="viewUrl2" name="product2_img_url" value="<?= $Cmpdraft->product2image ?>">
                                <input class="<?= $adimagehashArr2[0] ?>" id="fbimghash2" type='hidden' name='fbimghash2' value='<?= $adimagehashArr2[0] ?>' />
                              <?php
                            }
                            else{
                          $imgApp2 = explode('.', $Cmpdraft->product2image);?>
                            <input class="<?= $imgApp2[0].'-gallery-p2' ?> p2" type="hidden" id="viewUrl2" name="product2_img_url" value="https://fbcdn-creative-a.akamaihd.net/hads-ak-xtf1/t45.1600-4/<?= $Cmpdraft->product2image ?>">
                            <input class="<?= $imgApp2[0].'-gallery-p2' ?> p2" id="fbimghash2" type='hidden' name='fbimghash2' value='<?= $Cmpdraft->adimagehash2 ?>' />
                        <?php } } ?>
                        <?php 
                          if (!empty($Cmpdraft->product3image)) { 
                            $adimagehashArr3 = explode('_', $Cmpdraft->adimagehash3);
                            if($adimagehashArr3[1] == 'video'){
                              ?>
                                <input class="<?= $adimagehashArr3[0] ?> " type="hidden" id="viewUrl3" name="product3_img_url" value="<?= $Cmpdraft->product3image ?>">
                                <input class="<?= $adimagehashArr3[0] ?>" id="fbimghash3" type='hidden' name='fbimghash3' value='<?= $adimagehashArr3[0] ?>' />
                              <?php
                            }
                            else{
                            $imgApp3 = explode('.', $Cmpdraft->product3image);?>
                            <input class="<?= $imgApp3[0].'-gallery-p3' ?> p3" type="hidden" id="viewUrl3" name="product3_img_url" value="https://fbcdn-creative-a.akamaihd.net/hads-ak-xtf1/t45.1600-4/<?= $Cmpdraft->product3image ?>">
                            <input class="<?= $imgApp3[0].'-gallery-p3' ?> p3" id="fbimghash3" type='hidden' name='fbimghash3' value='<?= $Cmpdraft->adimagehash3 ?>' />
                        <?php }} ?>
                    </div>
									<div class="ad-preview-section">
										<div class="preview-controller d-sm-flex flex-wrap align-items-center">
											<div class="select-holder">
												<select onchange="return changePreview(this);">
													<option value="desktop">Desktop News Feed</option>
													<option value="mobile">Mobile News Feed</option>
												</select>
											</div>
										<div class="pagination d-flex flex-wrap align-items-center justify-content-between">
										       <input type="hidden" name="adsCounter" value="0" id="adsCounter"/>
                                               <input type="hidden" name="totalAdsCount" value="0" id="totalAdsCount"/>
												<div class="display-page">
													<span id="showAdTextMess">Showing Ad 1 of 4</span>
												</div>
												<div class="btn-holder d-flex flex-wrap justify-content-between">
												  
													<a href="javascript:void(0)" id="PreviousAdLink" class="btn btn-page-prev" onclick="fnLoadPreviousAd()"><span class="icon-angle-left"></span></a>
													<a href="javascript:void(0)" id="NextAdLink" class="btn btn-page-next" onclick="fnLoadNextAd()"><span class="icon-angle-right"></span></a>
												</div>
											</div> 
										</div>
										<div id="ads_preview_desktopfeed" class="ads-preview-panel previews-desktopfeed ad-preview" >
										    <?php if (!empty($Cmpdraft->desktoppreview)) { ?>
                                                <?php echo html_entity_decode(str_replace("and", "&", $Cmpdraft->desktoppreview)); ?>
                                            <?php } else { ?>
                                                <div class="preview-info-alert jsNoPreviewText">
                                                    <p class="tit">Preview not available</p>
                                                    <p>You must fill all required fields to see a preview of your ads.</p>
                                                </div><!-- /.preview-info-alert -->
                                                <div class="preview-info-alert preview-loading jsLoadingText hide">
                                                    <p class="tit">Loading preview...</p>
                                                </div><!-- /.preview-info -->
                        
                                                <div id="js_preview_carousel_desktopfeed" class="carousel slide" data-interval="false">
                                                    <ul class="fb-ads-list fb-list-mobile fb-list-desktopfeed carousel-inner"></ul>
                                                </div><!--/.carousel-->
                                            <?php } ?>
										<!--	<img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/img06.png" alt="image description"> -->
										</div>
										<div id="ads_preview_mobilefeed" class="ads-preview-panel previews-desktopfeed displayNone ad-preview">
										    <?php if (!empty($Cmpdraft->mobilepreview)) { ?>
                                                <?php echo html_entity_decode(str_replace("and", "&", $Cmpdraft->mobilepreview)); ?>
                                            <?php } else { ?>
                                                <div class="preview-info-alert jsNoPreviewText">
                                                    <p class="tit">Preview not available</p>
                                                    <p>You must fill all required fields to see a preview of your ads.</p>
                                                </div><!-- /.preview-info-alert -->
                                                <div class="preview-info-alert preview-loading jsLoadingText hide">
                                                    <p class="tit">Loading preview...</p>
                                                </div><!-- /.preview-info -->
                        
                                                <div id="js_preview_carousel_mobilefeed" class="carousel slide" data-interval="false">
                                                    <ul class="fb-ads-list fb-list-mobile fb-list-desktopfeed carousel-inner"></ul>
                                                </div><!--/.carousel-->
                                            <?php } ?>
										<!--	<img src="<?php //echo $this->config->item('assets'); ?>newdesign/images/img06.png" alt="image description"> -->
										</div>
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main28');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main28');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Go to Targeting
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			
			<main class="main30 maincont adcopy" style="display:none;">
				 
				<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step">
										<strong class="step-name">Page</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Headline</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Body Text</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Description</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">URL</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Media</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">CTA</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Tracking</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Load Your Saved Targeting
										<span class="sub-heading">
											Load a Targeting you previously saved using TCM Or start from scratch.
											<strong class="highlight-text">Pro tip: TCM automatically saves after each step!</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
									    
											<div class="big-btn-holder">
												<div class="btn-row d-flex flex-wrap justify-content-center">
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom d-flex flex-wrap">
															<input type="radio" checked="" name="save" id="new-rule">
															<label for="new-rule" class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-megaphone-rounded.svg" alt="megaphone">
																</span>
																<span class="text" id="newadcopy">
																	<span class="data">New Adcopy</span>
																	<span class="label">Start from scratch.</span>
																</span>
															</label>
														</div>
													</div>
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom d-flex flex-wrap">
															<input type="radio"  name="save" id="load-rule">
															<label for="load-rule"  class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-save-rounded.svg" alt="save">
																</span>
																<span class="text data load_drft" id="<?php echo $userId; ?>">
																	<span class="data load_drft" >Load Adcopy</span>
																	<span class="label" >Can be edited.</span>
																</span>
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group s2">
												<div class="select-holder s2" id="adcopytemplateholder" style="display:none">
													<select id="adcopytemplate" >
														<option>Choose your ad template</option>
													
													</select>
												</div>
											</div>
										
											
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="adcopygoprev('main19')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="adcopygonext('main19');" >
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</main>
			<main class="main31 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step active">
										<strong class="step-name">Custom Audience</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Location</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Gender</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Age</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">IDB</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Placement</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Advanced</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Custom Audience
										<span class="sub-heading">
											Target by custom audiences you already have in FB.
											<strong class="highlight-text">Pro tip:  retargeting an audience can yield a much higher ROI than normal targeting.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
										
											<div class="form-group main-catagory" id="custom_audience_fileds">
											    
											    	
											<div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col input-wrap" style="max-width: 405px;margin-right: 20px;">
													<div class="form-group mb-0">
														<div class="select-holder s2">
                                                          <select id="mobile_devices" class="mobile_devices" name="custom_audiences[]">
                                                          <option value="" selected>CUSTOM AUDIENCE</option>
                                                          <?php if(!empty($customaudiences)){
                                                                  foreach ($customaudiences->data as $key => $result) {
                                                                    
                                                                    ?>
                                                                      <option <?php echo $sel; ?> value='<?php echo $result->id . '##' . $result->name; ?>'><?php echo $result->name;?></option>
                                                                    <?php
                                                                  }
                                
                                                                }?>
                                                          </select>
                                                  
                                                        </div>
													</div>
												</div>
												<div class="flex-col btn-wrap">
													<a href="javascript:void(0)" id="more_custom_fields" class="btn btn-add add-relation">
														<span class="icon-holder icon-plus"></span>
													</a>
												</div>
											</div>	
											    
												<!--<label class="tag-label" for="main-catagory">Main</label>
												<input class="form-control" id="main-catagory" type="text" placeholder="Type your main custom audiences">-->
											
										
											
                                      <?php 
                                          $custom_audienceArr = explode('@', $Cmpdraft->custom_audience);
                                      ?>
                                      <?php 
									  
                                        if(!empty($custom_audienceArr[1])){
											
                                          for($i=0; $i<=count($custom_audienceArr)-1; $i++){
											  if(!empty($custom_audienceArr[$i])){
                                          ?>
                                          
                                          
                                            
                                          
                                          
                                          <div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
    											<div class="flex-col input-wrap" style="max-width: 405px;margin-right: 20px;">
    												<div class="form-group mb-0">
    													<div class="select-holder s2">
                                                          <select id="mobile_devices" class="mobile_devices" name="custom_audiences[]">
                                                          <option value="" selected>CUSTOM AUDIENCE</option>
                                                            <?php if(!empty($customaudiences)){
                                                              foreach ($customaudiences->data as $key => $result) {
                                                                $sel = '';
            													echo $result->id . "##" . $result->name;
            													echo $custom_audienceArr[$i];
                                                                if($custom_audienceArr[$i] == $result->id . "##" . $result->name){
                                                                  $sel = 'selected';
                                                                }
                                                                ?>
                                                                  <option <?php echo $sel; ?> value='<?php echo $result->id . '##' . $result->name; ?>'><?php echo $result->name;?></option>
                                                                <?php
                                                              }
                            
                                                            }?>
                                                          </select>
                                                  
                                                        </div>
    												</div>
    											</div>
    											<div class="flex-col btn-wrap">
    												<a href="javascript:void(0)" class="btn btn-add delete-custom-audience-field add-relation"><span class="icon-holder icon-bin"></span></a>
    											</div>
    										</div>
    										
                                          <?php 
											  }
                                          }
                                        }?>
                                            
                                        </div>
                                       
                                        
										<div class="checkbox-label">
											<div class="checkbox-label-holder">
												<input name="split_adset[]" value="custom_audiences" id="split-test" type="checkbox">
												<label for="split-test">Split test each audience on it’s own.</label>
											</div>
										</div>
											
										<!--	<div class="form-group exclude-catagory">
												<label class="tag-label" for="exclude-catagory">EXCLUDE</label>
												<input class="form-control" id="exclude-catagory" type="text" placeholder="Type custom audiences you want to exclude">
											</div>
											<div class="form-group include-catagory">
												<label class="tag-label" for="include-catagory">INCLUDE</label>
												<input class="form-control" id="include-catagory" type="text" placeholder="Type custom audiences you want to include">
											</div>
										-->
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="targetgoprev('main31')">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Back to Adcopy
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="targetgonext('main31');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main32 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Custom Audience</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Location</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Gender</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Age</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">IDB</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Placement</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Advanced</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Location
										<span class="sub-heading">
											Target users by Country, city, state or even address.
											<strong class="highlight-text">Pro tip:  split testing multiple countries can cut costs considerably.</strong>
										</span>
									</h2>
									<div class="form-wrap s1">
										
											<div class="form-group main-catagory">
												<label class="tag-label" for="main-catagory">Main</label>
												<div class="tagsinput">
												    
												    
												    <input type="text" id="locations_autocomplete_include" value="" name="locations_autocomplete_include" class="form-control" placeholder="Add a country, state/province, city or ZIP..."/>
                                                <input id="new_geo_locations" type="hidden" name="new_geo_locations" value="<?php echo $Cmpdraft->new_geo_locations; ?>">
                                                <input id="avoid_geo_locations" type="hidden" name="avoid_geo_locations" value="<?php echo $Cmpdraft->avoid_geo_locations; ?>">
                                                <input id="locations_autocomplete_include_hidden" type="hidden" name="locations_autocomplete_include_hidden" value="<?php echo $Cmpdraft->includelocation; ?>">
												    
												<!--	<span class="tag s1">
														Ottawa, Canada
														<span class="select-holder s5">
															<select>
																<option>Exact</option>
																<option>Exact 1</option>
																<option>Exact 2</option>
															</select>
														</span>
														<a href="#" class="remove">X</a>
													</span>
													<span class="typehead">
														<input type="text" placeholder="Type the locations you want to target" id="main-catagory">
													</span> -->
												</div>
												<div class="checkbox-label">
													<div class="checkbox-label-holder">
														<input name="split_adset[]" value="location" id="split-test-loc" type="checkbox">
														<label for="split-test-loc">Split test each location on it’s own.</label>
													</div>
												</div>
											</div>
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="targetgoprev('main32');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="targetgonext('main32');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main33 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Custom Audience</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Location</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Gender</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Age</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">IDB</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Placement</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Advanced</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Gender
										<span class="sub-heading">
											Choose which gender you want to target.
											<strong class="highlight-text">Pro tip:  If you are unsure which to target then target both.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
									
											<div class="gender">
												<div class="gender-holder d-flex flex-wrap">
													<div class="btn-radio-custom s3 gender-item d-flex flex-wrap">
														<input type="radio" name="gender" id="men" <?= ($Cmpdraft->gender == '1') ? "checked" : "" ?>>
														<label for="men" class="d-flex flex-wrap align-items-center" onclick="fnSelectval('1', 'genders', 'genderMen')">
															<span class="icon-holder">
																<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-boy-rounded-small.svg" alt="men">
															</span>
															<span class="text">
																<span class="data">MEN ONLY</span>
															</span>
														</label>
													</div>
													<div class="btn-radio-custom s3 gender-item d-flex flex-wrap">
														<input type="radio" name="gender" id="women" <?= ($Cmpdraft->gender == '2') ? "checked" : "" ?>>
														<label for="women" class="d-flex flex-wrap align-items-center" onclick="fnSelectval('2', 'genders', 'genderWomen')">
															<span class="icon-holder">
																<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-girl-rounded-small.svg" alt="girl">
															</span>
															<span class="text">
																<span class="data">WOMEN ONLY</span>
															</span>
														</label>
													</div>
													<div class="btn-radio-custom s3 gender-item d-flex flex-wrap">
													    
														<!--<input type="radio" name="gender" id="both" <?= ($Cmpdraft->gender == '') ? "checked='checked'" : "" ?>>-->
															<input type="radio" name="gender" id="both" checked="checked">
														<label for="both" class="d-flex flex-wrap align-items-center" onclick="">
															<span class="icon-holder">
																<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-target-rounded.svg" alt="target">
															</span>
															<span class="text">
																<span class="data">TARGET BOTH</span>
															</span>
														</label>
														
													</div>
													<div class="btn-radio-custom s3 gender-item d-flex flex-wrap">
														<input type="checkbox" name="split_adset[]" value="genders" id="gendercheck" style="display:none !important;">
														<input type="radio" name="gender" id="test-them">
														
														<label for="test-them" class="d-flex flex-wrap align-items-center" onclick='activatesplitoption("gendercheck");'>
															<span class="icon-holder">
																<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-acamulator-rounded.svg" alt="acamulator">
															</span>
															<span class="text">
																<span class="data">A/B TEST THEM</span>
															</span>
														</label>
													</div>
													
													<input  id="genders" name="genders" value=""  type="hidden">
												</div>
											</div>
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main32').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="$('.maincont').hide();$('.main34').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main34 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Custom Audience</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Location</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Gender</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Age</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">IDB</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Placement</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Advanced</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Age
										<span class="sub-heading">
											Choose which age group you want to target.
											<strong class="highlight-text">Pro tip:  If you are unsure which to target then target 18-65.</strong>
										</span>
									</h2>
									<div class="form-wrap s3" id="ageyear_fileds">
											
											
											
											<?php 
                                          $fromArr = explode(',', $Cmpdraft->agegroupfrom);
                                          $toArr = explode(',', $Cmpdraft->agegroupto);
                                      ?>
                                      <?php 
                                      if(!empty($fromArr[0])){
                                          for($i=0; $i<=count($fromArr)-1; $i++){
                                            ?>
                                            
                                            <div class="age-targeting d-md-flex flex-wrap justify-content-center align-items-center" id="">
	                                        <div class="age-holder">
												<div class="select-holder s4">
													<select name="form" id="form">
													    <?php 
													    $x = 18;
													    for ($x = 14; $x <= 64; $x++) {
													    ?>
														<option value="<?php echo $x; ?>" <?php echo ($fromArr[$i]==$x)?"selected":"";  ?> ><?php echo $x; ?> Years Old</option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="range-icon-holder">
												<div class="icon-wrap">
													<span class="icon-arrow-head-right"></span>
												</div>
											</div>
											<div class="age-holder">
												<div class="select-holder s4">
													<select name="to" id="to">
														<?php 
														$x = 14;
													    for ($x = 15; $x <= 65; $x++) {
													    ?>
														<option value="<?php echo $x; ?>" <?php echo ($fromArr[$i]==$x)?"selected":"";  ?>><?php echo $x; ?> Years Old</option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="btn-wrap">
											    
												<a href="javascript:void(0)" id="more_timetable_fields" class="btn btn-add btn-add-big add-age">
													<span class="icon-holder icon-plus"></span>
												</a>
											</div>
										</div>
										
										
										
										
                                                
                                            <?php
                                          }
                                      }
                                      else{
                                      ?>
                                      
                                      
                                      
                                        <div class="age-targeting d-md-flex flex-wrap justify-content-center align-items-center" id="">
	                                        <div class="age-holder">
												<div class="select-holder s4">
													<select name="form" id="form">
													    <?php 
													    $x = 14;
													    $defaultfromage= 18;
													    for ($x = 14; $x <= 64; $x++) {
													    ?>
														<option value="<?php echo $x; ?>"  <?= $defaultfromage == $x ? "selected" : "" ?>><?php echo $x; ?> Years Old</option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="range-icon-holder">
												<div class="icon-wrap">
													<span class="icon-arrow-head-right"></span>
												</div>
											</div>
											<div class="age-holder">
												<div class="select-holder s4">
													<select name="to" id="to">
														<?php 
														$x = 14;
														$defaulttoage= 65;
													    for ($x = 15; $x <= 65; $x++) {
													    ?>
														<option value="<?php echo $x; ?>" <?= $defaulttoage == $x ? "selected" : "" ?>><?php echo $x; ?> Years Old</option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="btn-wrap">
											    
												<a href="javascript:void(0)" id="more_timetable_fields" class="btn btn-add btn-add-big add-age">
													<span class="icon-holder icon-plus"></span>
												</a>
											</div>
										</div>
                                      <?php }
                                      ?>
                                      <input type="hidden" name="adeyearcounter" id="adeyearcounter" value="<?php echo count($fromArr); ?>">
											
											
											
											
											
											
											
											
											
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main33').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="$('.maincont').hide();$('.main35').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main35 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Custom Audience</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Location</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Gender</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Age</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">IDB</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Placement</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Advanced</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">Interest, Demographic OR Behaviour
										<span class="sub-heading">
											Target by Interest, Demographic OR Behaviour.
											<strong class="highlight-text">Pro tip:  retargeting an audience can yield a much higher ROI than normal targeting.</strong>
										</span>
									</h2>
									<div class="form-wrap s1">
									
										
										    <?php 
                                                    $interestArr = explode('#$#', $Cmpdraft->interest);
                                                    $new_interestsArr = explode('#$#', $Cmpdraft->new_interests);
                                              ?>
                              
                                    <div class="dib-block">
                                      
                                      
                                      
										
											<div class="form-group main-catagory">
												<label class="tag-label" for="main-catagory">Main</label>
												<div class="tagsinput">
													    <input type="text" id="interests1" name="interests[]" placeholder="Type the locations you want to target"/>
                                                        <input id="new_interests1" type="hidden" name="new_interests[]" value='<?php echo $new_interestsArr[0]; ?>'>
                                                        <?php
                                                            $intestertext = str_replace('"', "", $interestArr[0]);
                                                            $intestertextfinal = str_replace("'", "", $intestertext);
                                                        ?>
                                                        <input id="new_interests_hidden1" type="hidden" name="new_interests_hidden[]" value="<?php echo $intestertextfinal; ?>">
													
												</div>
												<div class="suggestion-box displayNone" id="suggestion_data1">
												</div>
												<div class="checkbox-label">
													<div class="checkbox-label-holder">
														<input name="split_adset[]" value="interests" id="intrestcheck" type="checkbox">
														<label for="intrestcheck">Split test each audience on it’s own.</label>
													</div>
												</div>
											</div>

											<div id="include_fileds">
											  <?php 
                                              if(!empty($interestArr[0])){
                                                $k=2;
                                                for($i=1; $i<=count($interestArr)-1; $i++){
                                               ?>  
    											<div class="form-group include-catagory">
    												<label class="tag-label" for="include-catagory">INCLUDE THE FOLLOWING</label>
    												<div class="tagsinput">
    													<span class="typehead">
    													    <a class="delete-include_exlude-field add-relation" style="float:right;"><i class="fa fa-trash-o"></i></a>
    													    <input type="text" id="interests<?php echo $k; ?>" name="interests[]" placeholder="INTEREST, DEMOGRAPHIC OR BEHAVIOUR" class="form-control"/>
                                                            <input id="new_interests<?php echo $k; ?>" type="hidden" name="new_interests[]" value='<?php echo $new_interestsArr[$i]; ?>'>
                                                            <input id="new_interests_hidden<?php echo $k; ?>" type="hidden" name="new_interests_hidden[]" value='<?php echo $interestArr[$i]; ?>'>
    													    
    														<!-- <input type="text" placeholder="Type custom audiences you want to include" id="include-catagory"> -->
    													</span>
    												</div>
    												<div class="suggestion-box displayNone" id="suggestion_data<?php echo $k; ?>">
												    </div>
    											</div>
    										<?php 
                                                }
                                              }
                                              ?>
    										</div>
    										
    										<div id="exlude_fileds">
    										    
    										    <div class="form-group exclude-catagory" id="excludeInterestDiv" style="<?php if(!empty($Cmpdraft->interest_exclude)){ ?>display: block;<?php }else{ ?>display: none;<?php } ?>">
    												<label class="tag-label" for="exclude-catagory">EXCLUDE</label>
    												<div class="tagsinput">
    													<span class="typehead">
    													    <a class="delete-include_exlude-field add-relation" style="float:right;"><i class="fa fa-trash-o"></i></a>
    													    <input type="text" id="interests_exclude1" name="interests_exclude" placeholder="INTEREST, DEMOGRAPHIC OR BEHAVIOUR" class="form-control"/>
                                                            <input id="new_interests_exclude1" type="hidden" name="new_interests_exclude" value='<?php echo $Cmpdraft->new_interests_exclude; ?>'>
                                                            <input id="new_interests_exclude_hidden1" type="hidden" name="new_interests_exclude_hidden" value='<?php echo $Cmpdraft->interest_exclude; ?>'>
    													</span>
    												</div>
    												<div class="suggestion-box displayNone" id="suggestion_data_exclude1">
												    </div>

    											</div>
    										</div>
    										<div class="setup-holder">
                                               <a href="javascript:void(0)" class="a more_Include_fields btn-setup-include" id="more_Include_fields"><label class="tag-label" for="include-catagory">setup INCLUDE targeting</label></a>
                                               <a href="javascript:void(0)" class="b more_Exclude_fields btn-setup-exclude" id="more_Exclude_fields"><label class="tag-label" for="exclude-catagory">setup EXCLUDE targeting</label></a>
                                             
                                            </div>
										
									</div>
								</div>
								<input type="hidden" name="interestsCount" id="interestsCount" value="<?php echo !empty($interestsCount) ? count($interestsCount) : !empty($interestArr) ? count($interestArr) : 1; ?>">
                                 <input type="hidden" name="excludeCount" id="excludeCount" value="1">
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main34').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="$('.maincont').hide();$('.main36').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main36 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Custom Audience</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Location</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Gender</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Age</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">IDB</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Placement</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Advanced</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Ad Placement
										<span class="sub-heading">
											Where do you want your ads to be seen?
											<strong class="highlight-text">Pro tip:  If you are unsure which to target then choose all of them.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										
											<div class="ad-placement">
												<div class="ad-placement-holder d-flex flex-wrap">
													<div class="ad-placement-item">
														<div class="btn-checkbox-custom d-flex flex-wrap">
															<input type="checkbox" <?= (strpos($Cmpdraft->placement, 'desktopfeed') !== false) ? "checked" : "" ?> onclick="fnSelectPlacementval('placementDataSet','desktopFeed')" id="desktopFeed">
															<label class=" d-flex flex-wrap align-items-center" for="desktopFeed">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-desktop-rounded.svg" alt="desktopFeed">
																</span>
																<span class="label-text">Desktop <br> News Feed</span>
															</label>
															<input type="hidden" name="page_types[]" id="desktop_news_feed" value="<?= (strpos($Cmpdraft->placement, 'desktopfeed') !== false) ? "desktopfeed" : "" ?>">
														</div>
													</div>
													<div class="ad-placement-item">
														<div class="btn-checkbox-custom d-flex flex-wrap">
															<input type="checkbox" <?= (strpos($Cmpdraft->placement, 'mobilefeed') !== false) ? "checked" : "" ?> onclick="fnSelectPlacementval('placementDataSet', 'mobileFeed')" id="mobileFeed">
															<label class=" d-flex flex-wrap align-items-center" for="mobileFeed">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-iphone.svg" alt="mobileFeed">
																</span>
																<span class="label-text">Mobile <br> News Feed</span>
															</label>
															<input type="hidden" name="page_types[]" id="mobile_news_feed" value="<?= (strpos($Cmpdraft->placement, 'mobilefeed') !== false) ? "mobilefeed" : "" ?>">
														</div>
													</div>
													<div class="ad-placement-item">
														<div class="btn-checkbox-custom d-flex flex-wrap">
															<input type="checkbox" <?= (strpos($Cmpdraft->placement, 'instagram') !== false) ? "checked" : "" ?> onclick="fnSelectPlacementval('placementDataSet', 'instagramPla')" id="instagramPla">
															<label class=" d-flex flex-wrap align-items-center" for="instagramPla">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-instagram-rounded.svg" alt="instagramPla">
																</span>
																<span class="label-text">Instagram <br> Feed</span>
															</label>
															<input type="hidden" name="page_types[]" id="instagram_feed" value="<?= (strpos($Cmpdraft->placement, 'instagram') !== false) ? "instagram" : "" ?>">
														</div>
													</div>
													<div class="ad-placement-item">
														<div class="btn-checkbox-custom d-flex flex-wrap">
															<input type="checkbox" <?= (strpos($Cmpdraft->placement, 'rightcolumn') !== false) ? "checked" : "" ?> onclick="fnSelectPlacementval('placementDataSet', 'desktopRHS')" id="desktopRHS">
															<label class=" d-flex flex-wrap align-items-center" for="desktopRHS">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-browser-rounded.svg" alt="desktopRHS">
																</span>
																<span class="label-text">Desktop <br> Right Side</span>
															</label>
															<input type="hidden" name="page_types[]" id="desktop_right_hand_side" value="<?= (strpos($Cmpdraft->placement, 'rightcolumn') !== false) ? "rightcolumn" : "" ?>">
														</div>
													</div>
													<div class="ad-placement-item">
														<div class="btn-checkbox-custom d-flex flex-wrap">
															<input type="checkbox" <?= (strpos($Cmpdraft->placement, 'mobileexternal') !== false) ? "checked" : "" ?> onclick="fnSelectPlacementval('placementDataSet', 'rdpartyFeed')" id="rdpartyFeed">
															<label class=" d-flex flex-wrap align-items-center" for="rdpartyFeed">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-android-rounded.svg" alt="rdpartyFeed">
																</span>
																<span class="label-text">Audience <br> Network</span>
															</label>
															<input type="hidden" name="page_types[]" id="party_mobile_site" value="<?= (strpos($Cmpdraft->placement, 'mobileexternal') !== false) ? "mobileexternal" : "" ?>">
														</div>
													</div>
													<!-- <div class="ad-placement-item">
														<div class="btn-checkbox-custom d-flex flex-wrap">
															<input type="checkbox" id="messenger">
															<label class=" d-flex flex-wrap align-items-center" for="messenger">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-speech-rounded.svg" alt="messenger">
																</span>
																<span class="label-text">Messenger <br> Home</span>
															</label>
														</div>
													</div> -->
												</div>
												<div class="checkbox-label">
    												<div class="checkbox-label-holder">
    													<input name="split_adset[]" value="placement" id="split-test-placement" type="checkbox">
    													<label for="split-test-placement">Split test each placement on it’s own.</label>
    												</div>
    											</div>
											</div>
											
											
											
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="targetgoprev('main36');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="targetgonext('main36');">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main37 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Custom Audience</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Location</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Gender</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Age</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">IDB</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Placement</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Advanced</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Advanced Targeting Options
										<span class="sub-heading">
											Target by language, job role or by education level.
											<strong class="highlight-text">Pro tip:  You can skip this page by clicking next.</strong>
										</span>
									</h2>
									<div class="form-wrap s1">
										
											
											    <div class="form-group main-catagory">
												<label class="tag-label" for="main-catagory">Language</label>
												<div class="tagsinput">
											    <input type="text" id="locales_autocomplete" placeholder="LANGUAGE" name="locales_autocomplete" class="form-control"/>
                                        <input id="new_ad_langauage" type="hidden" name="new_ad_langauage" value="<?php echo $Cmpdraft->new_ad_langauage; ?>">
                                        <input id="new_ad_langauage_hidden" type="hidden" name="new_ad_langauage_hidden" value="<?php echo $Cmpdraft->language; ?>">
												<!-- <input class="form-control" type="text" placeholder="Enter the languages you want to target">-->
											</div>
											</div>
											
											<?php 
                                  $workpositionsStr = '';
                                  if(!empty($Cmpdraft->workpositions)){
                                      $workpositionsArr = explode(',', $Cmpdraft->workpositions);
                                      for ($i=0; $i <= count($workpositionsArr)-1 ; $i++) { 
                                          $workpositionsStr .= '{"id":'.$workpositionsArr[$i].'},';
                                      }
                                      $workpositionsStr = rtrim($workpositionsStr, ',');
                                  }
                                  ?>
											<div class="form-group main-catagory">
												<label class="tag-label" for="main-catagory">Job Title</label>
											<div class="tagsinput">
											    <input type="text" name="job_autocomplete" id="job_autocomplete" placeholder="JOB TITLE" class="form-control"/>
                                                <input id="new_work_positions" type="hidden" name="new_work_positions" value='<?php echo $workpositionsStr; ?>'>
                                                <input id="new_work_positions_hidden" type="hidden" name="new_work_positions_hidden" value='<?php echo $Cmpdraft->new_work_positions; ?>'>
                                                <input id="new_work_positions_id_hidden" type="hidden" name="new_work_positions_id_hidden" value='<?php echo $Cmpdraft->new_work_id_positions; ?>'>
											<!--	<input class="form-control" type="text" placeholder="Enter the job roles you want to target"> -->
											</div>
											</div>
											
											
											<div class="form-group main-catagory">
											    
											  
											<div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
    											<div class="flex-col input-wrap" style="width:100%;">
    												<div class="form-group mb-0">
    												    <label class="tag-label" for="main-catagory">Education Level</label>
    													<div class="select-holder s2">
                                                          <select id="education" name="education_statuses[]">
                                                            <option value="" selected>Education Level</option>
                                                            <option value="1"> High School</option>
                                                            <option value="2"> Undergrad</option>
                                                            <option value="3"> Alum</option>
                                                            <option value="4"> High School Grad</option>
                                                            <option value="5"> Some College</option>
                                                            <option value="6"> Associate Degree</option>
                                                            <option value="7"> In Grad School</option>
                                                            <option value="8"> Some Grad School</option>
                                                            <option value="9"> Master Degree</option>
                                                            <option value="10"> Professional Degree</option>
                                                            <option value="11"> Doctorate Degree</option>
                                                            <option value="12"> Unspecified</option>
                                                            <option value="13"> Some High School</option>
                                                          </select>
                                                  
                                                        </div>
    												</div>
    											</div>
    											<!--<div class="flex-col btn-wrap">
    												<a href="javascript:void(0)" class="btn btn-add delete-custom-audience-field add-relation"><span class="icon-holder icon-bin"></span></a>
    											</div>-->
    										</div>
											
											</div>
											
										
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main36').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="createAdSets();$('.maincont').hide();$('.main38').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main38 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s2">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Custom Audience</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Location</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Gender</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Age</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">IDB</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Placement</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Advanced</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Preview</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">Preview Your Target Audience</h2>
									<div class="ad-table d-flex flex-wrap">
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label">A/B Custom Audiences</span>
												<span class="data" id="custacounter">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label">A/B IDB’s</span>
												<span class="data" id="interestcounter">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label">A/B Age Groups</span>
												<span class="data" id="agecounter" >1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label">A/B Locations</span>
												<span class="data" id="locationcounter">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label">A/B Placements</span>
												<span class="data" id="placementcounter">1</span>
											</div>
										</div>
										<div class="table-col d-flex flex-wrap">
											<div class="table-cell d-flex flex-wrap">
												<span class="label">A/B Gender</span>
												<span class="data" id="gendercounter">1</span>
											</div>
										</div>
									</div>
									<div class="target-audience">
										<div class="target-audience-holder d-flex flex-wrap" id="audiencestableresult">
											<!--<div class="ad-set">
												<div class="header d-flex flex-wrap align-items-end">
													<strong class="ad-set-titile">Adset #1</strong>
													<div class="audience">
														<strong>12,000,123</strong>
														<span>People</span>
													</div>
												</div>
												<div class="audience-wrap">
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Custom Audience</strong>
															<span class="audience-data">Lookalike audience 1</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Location</strong>
															<span class="audience-data">United States, Canada</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Gender</strong>
															<span class="audience-data">Men, Women</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Age</strong>
															<span class="audience-data">18 - 65+</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">IDB</strong>
															<span class="audience-data">Sketch Design</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Placement</strong>
															<span class="audience-data">MNF, DNF, Audience network..</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
												</div>
											</div>
											
											<div class="ad-set">
												<div class="header s1 d-flex flex-wrap align-items-end">
													<strong class="ad-set-titile">Adset #1</strong>
													<div class="audience">
														<strong>11,000,123</strong>
														<span>People</span>
													</div>
												</div>
												<div class="audience-wrap">
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Custom Audience</strong>
															<span class="audience-data">Lookalike audience 1</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Location</strong>
															<span class="audience-data">United States, Canada</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Gender</strong>
															<span class="audience-data">Men, Women</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Age</strong>
															<span class="audience-data">18 - 65+</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">IDB</strong>
															<span class="audience-data">Sketch Design</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Placement</strong>
															<span class="audience-data">MNF, DNF, Audience network..</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
												</div>
											</div>
											<div class="ad-set">
												<div class="header s2 d-flex flex-wrap align-items-end">
													<strong class="ad-set-titile">Adset #1</strong>
													<div class="audience">
														<strong>900,123</strong>
														<span>People</span>
													</div>
												</div>
												<div class="audience-wrap">
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Custom Audience</strong>
															<span class="audience-data">Lookalike audience 1</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Location</strong>
															<span class="audience-data">United States, Canada</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Gender</strong>
															<span class="audience-data">Men, Women</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Age</strong>
															<span class="audience-data">18 - 65+</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">IDB</strong>
															<span class="audience-data">Sketch Design</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
													<div class="audience-item d-flex flex-wrap">
														<div class="detail-holder">
															<strong class="audience-category">Placement</strong>
															<span class="audience-data">MNF, DNF, Audience network..</span>
														</div>
														<div class="action">
															<a href="#" class="btn-edit">Edit</a>
														</div>
													</div>
												</div>
											</div>-->
										</div>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main37').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="$('.maincont').hide();$('.main41').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Go To Budgeting
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main41 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s1">
								<div class="step-holder d-flex flex-wrap">
									<div class="step active">
										<strong class="step-name">Schedule</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Bid</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step not_show">
										<strong class="step-name">Budget</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Scheduling Your Campaign
										<span class="sub-heading">
											Choose between open ended or scheduled campaigns
											<strong class="highlight-text">Pro tip: if you don’t have a set timeline, choose daily budgets.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										
											<div class="big-btn-holder">
												<div class="btn-row d-flex flex-wrap justify-content-center">
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom s1 d-flex flex-wrap">
															<input type="radio" checked="checked" name="gender" id="daily">
															<label for="daily" class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-hourglass-rounded-big.svg" alt="hourglass">
																</span>
																<span class="text">
																	<span class="data">Daily Budget</span>
																	<span class="label">Open Ended</span>
																</span>
															</label>
														</div>
													</div>
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom s1 d-flex flex-wrap">
															<input type="radio" name="gender" id="lifetime">
															<label for="lifetime"  class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-timer-rounded.svg" alt="timer">
																</span>
																<span class="text">
																	<span class="data">Lifetime Budget</span>
																	<span class="label">COMING SOON</span>
																</span>
															</label>
														</div>
													</div>
												</div>
											</div>
										<!--	<div class="form-group s2 extra-mb">
												<div class="select-holder s2">
													<select>
														<option>Choose Your Campaign START Date</option>
														<option>Choose Your Campaign START Date 1</option>
														<option>Choose Your Campaign START Date 2</option>
													</select>
												</div>
											</div>
											<div class="form-group s2">
												<div class="select-holder s2">
													<select>
														<option>Choose Your Campaign END Date</option>
														<option>Choose Your Campaign END Date 1</option>
														<option>Choose Your Campaign END Date 2</option>
													</select>
												</div>
											</div> -->
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main38').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Back to Targeting
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="$('.maincont').hide();$('.main42').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main42 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s1">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Schedule</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Bid</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step">
										<strong class="step-name">Budget</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Bidding
										<span class="sub-heading">
											Choose between automated TCM bidding and manual bids based on your campaign objective.
											<strong class="highlight-text">Pro tip:  if you are not sure what to bid, then choose automatic</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										
											<div class="big-btn-holder">
												<div class="btn-row d-flex flex-wrap justify-content-center">
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom d-flex flex-wrap">
															<input type="radio" checked="" name="bidding" id="automatic">
															<label for="automatic" class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-magnet-rounded.svg" alt="magnet">
																</span>
																<span class="text">
																	<span class="data">Automated Bids</span>
																	<span class="label">Recommended</span>
																</span>
															</label>
														</div>
													</div>
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom d-flex flex-wrap">
															<input type="radio" name="bidding" id="manual">
															<label for="manual"  class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-tools-rounded.svg" alt="tools">
																</span>
																<span class="text">
																	<span class="data">Manual Bidding</span>
																	<span class="label">COMING SOON</span>
																</span>
															</label>
														</div>
													</div>
												</div>
											</div>
										<!--	<div class="form-group s1">
												<input class="form-control" type="text" placeholder="type your max bid per objective result.">
												<div class="form-feedback text-center">Your Account Currency is US Dollars</div>
											</div> -->
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main41').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="$('.maincont').hide();$('.main43').show();setbudget_count();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main43 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s12">
							<div class="sub-step-bar s1">
								<div class="step-holder d-flex flex-wrap">
									<div class="step done">
										<strong class="step-name">Schedule</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step done">
										<strong class="step-name">Bid</strong>
										<span class="icon-check"></span>
									</div>
									<div class="step active">
										<strong class="step-name">Budget</strong>
										<span class="icon-check"></span>
									</div>
								</div>
							</div>
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Budget
										<span class="sub-heading">
											Set how much you want to spend per day per adset for your campaign.
											<strong class="highlight-text">Pro tip:  suggested bid is 5 $USD per adset.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										
											<div class="big-btn-holder s1">
												<div class="btn-row d-flex flex-wrap justify-content-center">
													<div class="btn-frame">
														<div class="btn-info-display text-center">
															<span class="label">You Have</span>
															<strong class="data" ><span id="budget_adset">4 </span> Adsets</strong>
														</div>
													</div>
													<div class="btn-frame">
														<div class="btn-info-display text-center">
															<span class="label">Total Spend Per Day</span>
															<strong class="data" id="budget_total_spend_per_day">0</strong>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group s2">
												<input class="form-control" type="text" id="exampleInputAmount" min="1" value="<?= isset($Cmpdraft->budgetammount) ? $Cmpdraft->budgetammount : "5" ?>" name="budgetAmount" onkeypress="return isNumber(event)" onkeyup="fnGetadset();" placeholder="type your daily budget per adset">
												<div class="form-feedback text-center" id="current_user_currency">Your Account Currency is <?php echo $current_cur_currency.'('.$current_cur_code.')'; ?></div>
											</div>
										
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main42').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-next" onclick="$('.maincont').hide();$('.main51').show();set_review_count();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
									
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<main class="main51 maincont" style="display:none;">
			    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Objective</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Adcopy</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Targeting</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step done">
							<strong class="step-name">Budget</strong>
							<span class="icon-check"></span>
						</div>
						<div class="step active">
							<strong class="step-name">Review</strong>
							<span class="icon-check"></span>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<h2 class="screen-heading text-center">
									Review
									<span class="sub-heading">
										See what you have done so far.
										<strong class="highlight-text">Pro tip: The larger the amount of ads, the longer it will take.</strong>
									</span>
								</h2>
								<div class="ad-table d-flex flex-wrap justify-content-center">
									<div class="table-col d-flex flex-wrap">
										<div class="table-cell d-flex flex-wrap">
											<span class="label">ADS PER ADSET</span>
											<span class="data" id="review_ads">4</span>
										</div>
									</div>
									<div class="table-col d-flex flex-wrap">
										<div class="table-cell d-flex flex-wrap">
											<span class="label">ADSETS</span>
											<span class="data" id="review_adset">3</span>
										</div>
									</div>
									<div class="table-col d-flex flex-wrap">
										<div class="table-cell d-flex flex-wrap">
											<span class="label">TOTAL ADS</span>
											<span class="data" id="review_total_ads">12</span>
										</div>
									</div>
								</div>
								<div class="ad-table d-flex flex-wrap justify-content-center">
									<div class="table-col d-flex flex-wrap">
										<div class="table-cell d-flex flex-wrap">
											<span class="label">BUDGET PER ADSET</span>
											<span class="data" id="review_budget_per_adset">1</span>
										</div>
									</div>
									<div class="table-col d-flex flex-wrap">
										<div class="table-cell d-flex flex-wrap">
											<span class="label">BUDGET PER 1 AD</span>
											<span class="data" id="review_budget_per_ad">1</span>
										</div>
									</div>
								</div>
								<div class="ad-table d-flex flex-wrap justify-content-center">
									<div class="table-col d-flex flex-wrap">
										<div class="table-cell d-flex flex-wrap">
											<span class="label">TOTAL SPEND PER DAY</span>
											<span class="data" id="total_spend">1</span>
										</div>
									</div>
								</div>
								
								<!--<div class="btn-holder text-center">
									<a href="#" onclick="Proceed_last();" class="btn btn-launch lastSavedBtn">
										<span class="icon-holder">
											<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-rocket.svg" alt="rocket">
										</span>
										Launch The Campaign!
									</a>
								
								</div>-->
								
									<!---/// Test -->
									<div class="btn-holder d-flex flex-wrap justify-content-center custom-launch-button">
									<a href="#" onclick="Proceed_last();"  class="btn btn-launch lastSavedBtn">
									<span class="icon-holder">
											<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-rocket.svg" alt="rocket">
										</span>
											Launch The Campaign!
									</a>
								</div>
									<!---// Test End -->
								
									<div class="btn-holder text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(0)" class="btn btn-prev" onclick="$('.maincont').hide();$('.main43').show();">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
								</div>
								
								
								
									
									
								
								
								
								
							</div>
						</div>
					</div>
				</div>
				
			</main>
			<main class="main52 maincont" style="display:none;">
			  
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Publishing Your Campaign Now…
										<span class="sub-heading s1">
											PLEASE DO NOT CLOSE THIS PAGE OR CLICK THE BACK BUTTON
										</span>
									</h2>
									<div  class="loading" style="margin:auto;width:400px">
										    	<div id="lottie" style="width:400px;height:300px"></div>
										    
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</main>
			<main class="main53 maincont" style="display:none;">
			    
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s10">
							<div class="holder s1">
								<div class="img-holder crown-holder text-center">
									<img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-crown-big.svg" alt="crown">
								</div>
								<h2 class="screen-heading text-center">
									CAMPAIGN PUBLISHED SUCCESSFULLY!
									<span class="sub-heading">Your campaign is currently being reviewed by Facebook.</span>
								</h2>
								<div class="success-message-holder s1 text-center">
									<p class="highlight-text">In the mean time we suggest setting up optimization rules <br class="d-none d-md-block"> for your campaign to limit wasteful spending on high cost ads.</p>
									<div class="btn-holder">
										<a href="<?= site_url('automaticoptimization'); ?>" class="btn btn-optimization">
											<span class="icon-holder">
												<img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-paper-plane-big.svg" alt="paper plane">
											</span>
											<span class="primary-text">Optimization Tool</span>
											<span class="secondary-text">Setup automated rules.</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</main>
			
			
			
			
			
			<div class="modal fade publish-campaign-popup" id="publishCampaign" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="label-holder">
              <span class="publishing-label">Publishing...</span>
            </div>
            <h2>Publishing Your Campaign Now...</h2>
            <div class="step-holder">
              <div class="step step-1 has-dot active">
                <span class="info-text active">Uploading Your Campaign Data to Facebook</span>
              </div>
              <div class="step step-2 has-dot">
                <span class="info-text">Uploading Your Campaign Data to Facebook</span>
              </div>
              <div class="step step-3">
              </div>
            </div>
          </div>
          <div class="modal-body">
            <p>This will take a few minutes depending on how many ads you created.</p>
            <div id="floatingBarsG">
              <div class="blockG" id="rotateG_01"></div>
              <div class="blockG" id="rotateG_02"></div>
              <div class="blockG" id="rotateG_03"></div>
              <div class="blockG" id="rotateG_04"></div>
              <div class="blockG" id="rotateG_05"></div>
              <div class="blockG" id="rotateG_06"></div>
              <div class="blockG" id="rotateG_07"></div>
              <div class="blockG" id="rotateG_08"></div>
              <div class="blockG" id="rotateG_09"></div>
              <div class="blockG" id="rotateG_10"></div>
              <div class="blockG" id="rotateG_11"></div>
              <div class="blockG" id="rotateG_12"></div>
            </div>
            <div class="warrning-note">
              Please DO NOT click the back button or close the window while the campaign is being published.
            </div>
          </div>
        </div>
      </div>
    </div>          
                        <div class="modal fade publish-campaign-popup" id="campaignPublished" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <a href="#" class="modal-close close-at-top" data-dismiss="modal">Close</a>
        <div class="modal-content">
          <div class="modal-header">
            <div class="label-holder">
              <span class="publishing-label">YAY!</span>
            </div>
            <h2>Your Campaign Has Been Published!.</h2>
            <div class="step-holder">
              <div class="step step-1 has-dot done">
                <span class="info-text">Uploading Your Campaign Data to Facebook</span>
              </div>
              <div class="step step-2 has-dot active">
                <span class="info-text active">Being Reviewed By Facebook <small class="additional-info">*your campaign will start delivering as soon as Facebook approves it*</small></span>
              </div>
              <div class="step step-3">
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="option-row">
              <div class="col-left">
                <strong class="title">Create Another Campaign</strong>
                <span class="info-text">When 1 campaign is not enough.</span>
              </div>
              <div class="col-right">
                <a href="<?php echo site_url('createcampaign'); ?>" class="btn-icon"><span class="icon-holder"><span class="icon-pen"></span></span>Create Another One</a>
              </div>
            </div>
            <div class="option-row">
              <div class="col-left">
                <strong class="title">Setup Automatic Optimization Rules</strong>
                <span class="info-text">Make your campaigns work for you.</span>
              </div>
              <div class="col-right">
                <a href="<?php echo site_url('automaticoptimization'); ?>" class="btn-icon"><span class="icon-holder"><span class="icon-optimize"></span></span>Optimization Rules</a>
              </div>
            </div>
            <div class="option-row">
              <div class="col-left">
                <strong class="title">Access Your Reporting Dashboard</strong>
                <span class="info-text">See how hard your campaigns are working.</span>
              </div>
              <div class="col-right">
                <a href="<?php echo site_url("reports"); ?>" class="btn-icon"><span class="icon-holder"><span class="icon-note"></span></span>Report Dashboard</a>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            Have questions or concerns about anything? <a href="<?php echo $this->config->item('base_url'); ?>helps">Contact us now!</a>
          </div>
        </div>
      </div>
    </div>
			
	
			
			
          <input id="billing_event_1" type="hidden" value="IMPRESSIONS" name="billing_event">
          <input id="totalAdsCount1" type="hidden" value="0" name="totalAdsCount1">
          <input id="totalAdsetCount1" type="hidden" value="0" name="totalAdsetCount1">
          <input id="budgetDay" type="hidden" value="daily_budget" name="budget_type">
          <!--<input id="budgetDay" type="hidden" value="lifetime_budget" name="budget_type">-->
          <input type="hidden" name="compaingname_1" id="compaingname_1" value="<?= isset($Cmpdraft->campaignname) ? $Cmpdraft->campaignname : '' ?>" />
          <input type="hidden" name="adaccountid_2" id="adaccountid_2" value="<?= isset($Cmpdraft->adaccount) ? $Cmpdraft->adaccount : '' ?>" />
          <input type="hidden" name="db_addesign_id" id="db_addesign_id" value="" />
          <input id="old_behaviours" type="hidden" value="<?=isset($Cmpdraft->behaviours)?$Cmpdraft->behaviours:''?>" name="old_behaviours">
          <input id="old_demograph" type="hidden" value="<?=isset($Cmpdraft->demographic)?$Cmpdraft->demographic:''?>" name="old_demograph">
          <input type="hidden" name="campaignobjective" id="campaignobjective" value="<?=isset($Cmpdraft->adobjective)?$Cmpdraft->adobjective:'LINK_CLICKS'?>" />
          <input type="hidden" name="desktoppreview" id="desktoppreview" value="" />
          <input type="hidden" name="mobilepreview" id="mobilepreview" value="" />
          <input type="hidden" name="righthandpreview" id="righthandpreview" value="" />
          <?php 
          if(!empty($Cmpdraft->adobjective)){
            if($Cmpdraft->adobjective == 'LEAD_GENERATION'){
              $objectiveType = 'objective9';
            }
            else if($Cmpdraft->adobjective == 'CONVERSIONS'){
              $objectiveType = 'objective4';
            }
            else if($Cmpdraft->adobjective == 'LINK_CLICKS'){
              $objectiveType = 'objective1';
            }
            else if($Cmpdraft->adobjective == 'PAGE_LIKES'){
              $objectiveType = 'objective8';
            }
            else if($Cmpdraft->adobjective == 'PRODUCT_CATALOG_SALES'){
              $objectiveType = 'objective2';
            }
            else if($Cmpdraft->adobjective == 'POST_ENGAGEMENT'){
              $objectiveType = 'objective6';
            }
          }
          else{
            $objectiveType = 'objective1';
          }
          ?>
          <input type="hidden" name="objectiveType" id="objectiveType" value="<?php echo $objectiveType; ?>" />
          </form>
      <!-- Main Content Ends -->
    </div>
     <!-- Modal -->
    
	<script>
	
	 $('label[for="both"]').trigger('click');
	 $('label[for="daily"]').trigger('click');
	
	 
	 
	function opencampaigntab(evt, fbtabName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		//alert(fbtabName);
		if(fbtabName != 'step1'){
			document.getElementById("adcopy").classList.add("selected");
		}
		else{
			document.getElementById("adcopy").classList.remove("selected");
		}
		if(fbtabName != 'step1' && fbtabName != 'step2'){
			document.getElementById("targeting").classList.add("selected");
		}
		else{
			document.getElementById("targeting").classList.remove("selected");
		}
		if(fbtabName == 'step4' || fbtabName == 'step5'){
			document.getElementById("budget").classList.add("selected");
		}
		else{
			document.getElementById("budget").classList.remove("selected");
		}
		if(fbtabName == 'step5'){
			document.getElementById("review").classList.add("selected");
		}
		else{
			document.getElementById("review").classList.remove("selected");
		}
		document.getElementById(fbtabName).style.display = "block";
	}
	</script>    