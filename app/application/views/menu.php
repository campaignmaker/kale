<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="page-sidebar-wrapper">
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="false" data-auto-speed="200">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <br/>
            <li class="start <?php if($this->uri->segment(1) == 'reports') echo 'active ';?> ">
                <a href="<?= site_url('reports'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">Your Campaigns</span>
                    <?php if($this->uri->segment(1) == 'reports') echo '<span class="selected"></span> ';?>
                </a>
            </li>
            <li class=" <?php if($this->uri->segment(1) == 'createcampaign') echo 'active ';?>">
                <a href="<?= site_url('createcampaign'); ?>">
                    <i class="fa fa-bullhorn"></i>
                    <span class="title">Create Campaign</span>
                    <?php if($this->uri->segment(1) == 'createcampaign') echo '<span class="selected"></span> ';?>
                </a>
            </li>
            <li class=" <?php if($this->uri->segment(1) == 'Adaccounts') echo 'active ';?>">
                <a href="<?= site_url('Adaccounts'); ?>">
                    <i class="fa fa-magic"></i>
                    <span class="title">Ad Account</span>
                    <?php if($this->uri->segment(1) == 'Adaccounts') echo '<span class="selected"></span> ';?>
                </a>
            </li>
            <li class=" <?php if($this->uri->segment(1) == 'updateprofile') echo 'active ';?>">
                <a href="<?= site_url('updateprofile'); ?>">
                    <i class="fa fa-gears"></i>
                    <span class="title">Settings</span>
                    <?php if($this->uri->segment(1) == 'updateprofile') echo '<span class="selected"></span> ';?>
                </a>
            </li>
            <li class=" <?php if($this->uri->segment(1) == 'helps') echo 'active ';?>">
                <a href="<?= site_url('helps'); ?>">
                    <i class="fa fa-info-circle"></i>
                    <span class="title">Help</span>
                    <?php if($this->uri->segment(1) == 'helps') echo '<span class="selected"></span> ';?>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>