<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- DO NOT MODIFY -->
<!-- Quora Pixel Code (JS Helper) -->
<script>
!function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
qp('init', '6020c37571984bb1983fc4546f49070b');
qp('track', 'ViewContent');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/6020c37571984bb1983fc4546f49070b/pixel?tag=ViewContent&noscript=1"/></noscript>
<!-- End of Quora Pixel Code -->
<script>qp('track', 'Generic');</script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Activate Your Free Trial!</title>
<link rel="shortcut icon" href="https://thecampaignmaker.com/img/favicon.ico" type="image/x-icon">
    <!-- CSS Styles -->
    <link href="https://thecampaignmaker.com/css/plugins/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://thecampaignmaker.com/css/plugins/owl.carousel.min.css">
    <link rel="stylesheet" href="https://thecampaignmaker.com/css/plugins/owl.theme.default.min.css">
    <link href="https://thecampaignmaker.com/css/credit-custom.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://thecampaignmaker.com/css/credit-style.css">
    <script type="text/javascript" src="https://js.stripe.com/v1/"></script>
    
  <script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load("1418879561");
   
   heap.addUserProperties({'name': '<?= $session_data['full_name'] ?>','email': '<?= $session_data['email'] ?>'});
   
   heap.identify(<?= $session_data['email'] ?>);
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1352351844789066');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1352351844789066&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-76300210-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-76300210-1');
</script>

<script>
  window.intercomSettings = {
    app_id: "pio8n28y",
    name: "<?= $session_data['full_name'] ?>", // Full name
    email: "<?= $session_data['email'] ?>", // Email address
    
  };
  </script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/pio8n28y';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

</head>
<body>
    <script>
  fbq('track', 'AddPaymentInfo');
</script>

<!-- Menu Tab-->
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <img src="https://thecampaignmaker.com/img/Logo1.svg" class="img-responsive">
            </a>
        </div>
    </div>
</nav>
<!--Main Content 1-->

<div id="credit-content1" class="box-content-shadow">
    <div class="container">
        <div class="row">
            <p class="credit-headline text-center">
                <span>
                    <img src="https://thecampaignmaker.com/img/lightning.png" id="features-img">
                </span>
                <span id="star">Activate Your Free Trial</span>
            </p>
             
             
             
                    <script type="text/javascript" src="https://thecampaignmaker.com/aff/connect/stripe_ip.php"></script>
                        
                        <div class="top-notification-box" id="successmsgBx1" style="display: none;">
                      <i class="icon-warning"></i>
                      <span class="text payment-errors"></span>
                    </div>
                  
            <div class="offer-wrapper">
                <button id="button1" onclick="Text1()" class="content-img-holder btn-active">
                    <div class="offer-best-deal-wrapper">
                        <div class="content-holder-popular">
                            <img src="https://thecampaignmaker.com/img/popular.png" class="img-responsive"/>
                        </div>
                    </div>
                    <div class="offer-title-wrapper">
                        <div class="offer-info-logo">
                            <img src="https://thecampaignmaker.com/img/abtesting/content8-img2.png" class="img-responsive"/>
                        </div>
                        <div class="offer-info-data">
                            <p class="credit-headline color-blue">Yearly</p>
                            <h1><span class="color-red"><b>$19</b></span>/month</h1>
                        </div>
                    </div>
                    <div class="offer-info-description">
                        <p class="text23 text-left">Save over 50% by paying yearly..</p>
                    </div>
                </button>
                <button id="button2" onclick="Text2()" class="content-img-holder">
                    <div class="offer-best-deal-wrapper">
                        <div class="content-holder-popular">
                            <img src="https://thecampaignmaker.com/img/popular.png" class="img-responsive"/>
                        </div>
                    </div>
                    <div class="offer-title-wrapper">
                        <div class="offer-info-logo">
                            <img src="https://thecampaignmaker.com/img/abtesting/content8-img2.png" class="img-responsive"/>
                        </div>
                        <div class="offer-info-data">
                            <p class="credit-headline color-blue">Monthly</p>
                            <h1><span class="color-red"><b>$39</b></span>/month</h1>
                        </div>
                    </div>
                    <div class="offer-info-description">
                        <p class="text23 text-left">Everything included..</p>
                    </div>
                </button>

            </div>


            <script>
                function Text1() {
                    $('#button2').removeClass('btn-active');
                    $('#button1').addClass('btn-active');

                    $('#div1').html('<h1 class="text-center">FREE for 3 days then <span class="color-red"><b>$228</b></span>/year</h1>');
                    $('#selectedplan').val('Yearly3');
                }

                function Text2() {
                    $('#button1').removeClass('btn-active');
                    $('#button2').addClass('btn-active');

                    $('#div1').html('<h1 class="text-center">FREE for 3 days then <span class="color-red"><b>$39</b></span>/month</h1>');
                    $('#selectedplan').val('Monthly3');
                }
            </script>

            <div>
                <div id="div1">
                    <h1 class="text-center">FREE for 3 days then <span class="color-red"><b>$228</b></span>/year</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-10">
                
                <div class="credit-form-wrapper">
                   <!--FORM BEGIN-->
                    <form action="<?php echo base_url()."/trialactivate2/processpayment"; ?>" method="POST" id="stripe-payment-form" style="height:1px !important;padding:0px !important;">
                        <input type="hidden" name="idev_custom" id="idev_custom_x21" />
                        <input type="hidden" name="idev_custom" id="idev_custom_x21" />
                        <script type="text/javascript" src="https://thecampaignmaker.com/aff/connect/stripe_ip.php"></script>
                            
                        <input name="action" value="stripe" type="hidden">
                        <input name="redirect" value="" type="hidden">
                        <input name="amount" value="NDk=" type="hidden">
                        <input name="stripe_nonce" value="0dd70c1b2d" type="hidden">
                        <input type="hidden" id= "selectedplan" name="selectedplan" value="Yearly3">
                    </form>    

                    
                  
                </div>
             
            </div>
        </div>
        
        
        <!-- start new stripe -->
                    
                    <script src="https://js.stripe.com/v3/"></script>
<style>
.wrapper-outer{
    background-color: #f7f8f9;
}
.wrapper-inner{
    width: 670px;
    margin: 0 auto;
    height: 100%;
    
}
.wrapper-outer.*{
    font-size: 16px;
    font-variant: normal;
    padding: 0;
}
form{
    padding: 30px;
    height: 120px;
}
.form-row {
    width: 70%;
    float: left;
}
.wrapper-outer label{

    font-weight: 500;
    font-size: 14px;
    display: block;
    margin-bottom: 8px;
    color: #6b7c93;

}
.StripeElement {
  background-color: white;
  height: 40px;
  padding: 10px 12px;
  border-radius: 4px;
  border: 1px solid transparent;
  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
#card-errors {
    height: 20px;
    padding: 4px 0;
    color: #fa755a;
}
.wrapper-outer button {
    border: none;
    border-radius: 4px;
    outline: none;
    text-decoration: none;
    color: #fff;
    background: #32325d;
    white-space: nowrap;
    display: inline-block;
    height: 40px;
    line-height: 40px;
    padding: 0 14px;
    box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
    border-radius: 4px;
    font-size: 15px;
    font-weight: 600;
    letter-spacing: 0.025em;
    text-decoration: none;
    -webkit-transition: all 150ms ease;
    transition: all 150ms ease;
    float: left;
    margin-left: 12px;
    margin-top: 7px;
}
</style>

            <div class="wrapper-outer">
                <div class="wrapper-inner">
                    <form action="/charge" method="post" id="payment-form" style="width:100%;">
                       
                      <div class="form-row">
                        <label for="card-element">
                          Credit or debit card
                        </label>
                        <div id="card-element">
                          <!-- a Stripe Element will be inserted here. -->
                        </div>
                    
                        <!-- Used to display form errors -->
                        <div id="card-errors" role="alert"></div>
                      </div>
                    <br>
                      <button>Start Trial</button>
                    </form>
                </div>
            </div>    
       

<script>
// Create a Stripe client
//var stripe = Stripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
var stripe = Stripe('pk_live_HA0omoOtinEyA28gk3DqVoMK');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
        if(result.token.card.funding == 'prepaid'){
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = "Prepaid cards are not allowed.";
            
        }
        else{
        
            var form$ = jQuery("#stripe-payment-form");
    
            // token contains id, last4, and card type
    
            //var token = response['id'];
    
            // insert the token into the form so it gets submitted to the server
    
            form$.append("<input type='hidden' name='stripeToken' value='" + result.token.id + "'/>");
    
            // and submit
    
            form$.get(0).submit();
        }
        
        // Send the token to your server
        //stripeTokenHandler(result.token);
    }
  });
});
</script>
                    
                    <!-- end new stripe -->

       
    </div>

</div>
</div>

<!--Main Content 3-->

    
        
                
                        
         <!--FORM END-->
    <!--Main Content 4-->
      

<!--Login Footer-->

<footer>
    <div id="credit-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-3 col-sm-4 col-sm-offset-2 col-xs-6">
                    <h4 class="text-center"><a href="https://thecampaignmaker.com/blog/terms-of-service/" class="text-decoration-none">Terms Of Use</a>
                    </h4>
                </div>
                <div class="col-md-3 3 col-sm-4 col-xs-6">
                    <h4 class="text-center"><a href="https://thecampaignmaker.com/blog/privacy-policy/" class="text-decoration-none">Privacy Policy</a></h4>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Javascripts -->
<script src="https://thecampaignmaker.com/js/plugins/jquery.min.js"></script>
<script src="https://thecampaignmaker.com/js/plugins/owl.carousel.min.js"></script>
<script src="https://thecampaignmaker.com/js/plugins/bootstrap.min.js"></script>
<script>
    //var vid = document.getElementById("content4-video");

    //document.getElementById("content4-video").addEventListener("click", function(){
    //	vid.play();
    //	vid.setAttribute("poster", "");
    //});
    //vid.onended = function(){
    //	vid.load();
    //	vid.setAttribute("poster", "img/credit/poster.png");
    //};

    $(document).ready(function () {

        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 10,
            nav: true,
            center: true,
            loop: true,
            dots: false,
            smartSpeed: 600,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        $(".owl-prev").html("<i class='arrow-left'></i>");
        $(".owl-next").html("<i class='arrow-right'></i>");
    });
</script>
<script>
  window.intercomSettings = {
    app_id: "pio8n28y"
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/pio8n28y';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

<script>
/*Stripe.setPublishableKey("pk_live_HA0omoOtinEyA28gk3DqVoMK");

function stripeResponseHandler(status, response) {

    if (response.error) {

    // show errors returned by Stripe

		jQuery("#successmsgBx1").show();
        jQuery(".payment-errors").html(response.error.message);
        $('#myModal').modal();

    // re-enable the submit button

    jQuery('#stripe-submit').attr("disabled", false);

    } else {
        if(response['card']['funding'] == 'prepaid'){
            jQuery("#successmsgBx1").show();
            jQuery(".payment-errors").html("Prepaid cards are not allowed.");
        }
        else{
    		jQuery("#successmsgBx1").hide();
    
            var form$ = jQuery("#stripe-payment-form");
    
            // token contains id, last4, and card type
    
            var token = response['id'];
    
            // insert the token into the form so it gets submitted to the server
    
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
    
            // and submit
    
            form$.get(0).submit();
        }

    }

}*/

jQuery(document).ready(function($) {

 /* $("#stripe-payment-form").submit(function(event) {

    // disable the submit button to prevent repeated clicks

    $('#stripe-submit').attr("disabled", "disabled");


    // send the card details to Stripe

   Stripe.createToken({

      number: $('.card-numbertxt').val(),
      
      name: $('.card-name').val(),

      cvc: $('.card-cvc').val(),

      exp_month: $('.card-expiry-month').val(),

      exp_year: $('.card-expiry-year').val()

    }, stripeResponseHandler);


    // prevent the form from submitting with the default action

    return false;

  });

});*/
</script>

<script>
function validate(){
$(“input”).filter(function () {

  return $.trim($(this).val()).length == 0

}).length == 0;
}
</script>
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error Message From Stripe</h4>
        </div>
        <div class="modal-body">
        <span class="text payment-errors"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>

</body>
</html>