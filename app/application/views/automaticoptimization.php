<?php
/* optimization page */


   
// 	echo "<pre>";
// 	print_r($adruleslibrarydata);
// 	echo "</pre>";
	
	/*
	print_r($adruleslibrarydata);
    echo $adruleslibrarydata[0]['filters']['evaluation_spec'][4]['field'];
    */
	//exit;

	
?>
	<!-- main container of all the page elements -->
	
	
<!-- orignal content --->
 <main id="main">
    <!--<div class="dashboard-header">
      <ol class="breadcrumb">
        <li><a href="#">Automatic Rules</a></li>
        <?php if (isset($adaccountsCount) && $adaccountsCount > 1) { ?>

        <?php }else{?>
          <li class="active"><?php echo $accountName; ?></li>
        <?php } ?>
      </ol>
    </div>-->
    
    <!--<div id="preloaderMain" style="display: none;">-->
    <!--        <div id="statusMain"><i class="fa fa-spinner fa-spin"></i></div>-->
    <!--    </div>-->
    
    <div  id="preloaderMain" style="display:none;">
            <div id="statusMain" style="font-size:14px;color:white">
                <i class="fa fa-spinner fa-spin" style="font-size:48px;color:white"></i></br>
              <span>Setting up your rules dashboard…..</span>
            </div>
    </div>

<?php if (isset($adaccountsCount) && $adaccountsCount > 1) { ?>
        
        
        	<!--<main id="main">-->
        	<main id="main" class="main_add_account">
				<div class="main-content">
					<div class="container">
						<div class="center-box s6">
							<div class="holder s1">
								<h2 class="screen-heading text-center">Which ad account would you like to optimize?</h2>
								<div class="ad-account">
									<div class="table-holder">
										<table class="table-s4">
										    
										     <?php
										     
                        foreach ($adaccounts as $keytemp => $adaccount) {
                            
                            if($keytemp ==0){
                                $checked = "checked";
                            }else{
                                $checked = "";
                            }
                        ?>
                             
										    
											<tr>
												<td>
														<input id="ad-account-<?php echo $adaccount->ad_account_id; ?>" type="radio" name="ad-account" <?php echo $checked;  ?> value="<?php echo $adaccount->ad_account_id; ?>" >
														<label for="ad-account-<?php echo $adaccount->ad_account_id; ?>"></label>
												</td>
												<td><?php echo $adaccount->add_title; ?></td>
												<td><?php echo $adaccount->ad_account_id; ?></td>
											</tr>
											
							 <?php
                        }
                    ?>				
										
										</table>
									</div>
									<div class="btn-holder text-center">
										<a id="use_add_account" class="btn btn-primary" href="javascript:void(null)">Use Ad Account</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
        
        
        
    <?php }else{ ?>
<!--Content-->    
	<main id="main" class="optimization_rule_list" >
				<div class="main-content s1">
					<div class="campaign-list s1">
						<header class="d-sm-flex flex-wrap align-items-center">
							<strong class="title">Optimization Rules List</strong>
							<div class="btn-holder">
								<a href="javascript:void(null);" class="btn btn-create-new-rules" id="create_new_rule">
									<span class="icon-holder">
										<img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-pencil.svg" alt="pencil">
									</span>
									CREATE NEW RULE
								</a>
							</div>
						</header>
						<div class="table-holder">
							<table class="table-s2" cellpadding="5" cellspacing="5">
								<thead>
									<tr>
										<th>STATUS</th>
										<th>Rule Name</th>
										<th>Campaign Name</th>
										<th>Frequency</th>
										<!-- <th>Date</th> -->
										<th>Primary Rule</th>
										<th>Secondary Rule</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								    <?php	foreach ($adruleslibrarydata as $autoopti) { ?>
								    
								    <?php
								    
								    if($autoopti[status] == 'ENABLED'){
								     $checked = 'checked="true"';
								    }else{
								         $checked = '';
								    }
								    
								    ?>
									<tr class="open-close highlight-data">
	
									<td title="STATUS">
														<label class="switch">
															<input class="rule-status" type="checkbox" <?php echo $checked;?> data="<?php echo $autoopti['id']; ?>" >
															<span class="slider"></span>
														</label>
									</td>
									<td title="Rule Name"> <?php echo $autoopti['name'];?> </td>
									<td title="Campaign Name">
													    <?php echo $campaigns_list[$autoopti['evaluation_spec']['filters'][3]['value'][0]]; ?>
									</td>
									<td title="Frequency">
													    <?php // echo $autoopti['schedule_spec']['schedule_type'];
													   echo  ucwords(strtolower(implode(' ',explode('_',$autoopti['schedule_spec']['schedule_type']))));
													    ?>
									</td>
												
								<!--	<td title="DATE STARTED"> 
													<?php echo date("Y/m/d", strtotime($autoopti['updated_time'])); 	?>
									</td> -->
													
									<td title="Primary Rule" > 
														<?php
													$field = ucfirst(implode(' ',explode('_',$autoopti['evaluation_spec']['filters'][4]['field'])));
													$operator = ucfirst(strtolower(implode(' ',explode('_',$autoopti['evaluation_spec']['filters'][4]['operator']))));
													$tempvalue = (float)$autoopti['evaluation_spec']['filters'][4]['value'];
													
													 if( $field == 'Spent' || strpos( $field, 'Cost' ) !== false) {
													$value = $tempvalue /100;
													 }else{
													   	$value = $tempvalue;  
													 }
													
													echo $field.' '.$operator.' '.$value;
												
													?>
									</td>
									<td title="Secondary Rule" > 
														<?php
													$field = ucfirst(implode(' ',explode('_',$autoopti['evaluation_spec']['filters'][5]['field'])));
													$operator = ucfirst(strtolower(implode(' ',explode('_',$autoopti['evaluation_spec']['filters'][5]['operator']))));
													
													$tempvalue = (float)$autoopti['evaluation_spec']['filters'][5]['value'];
													
													if( $field == 'Spent' || strpos( $field, 'Cost' ) !== false) {
													  $value = $tempvalue/100;
													 }else{
													  $value = $tempvalue;  
													 }
													
													echo $field.' '.$operator.' '.$value;
												
													?>
												
												
									</td>
									<td>
													    <!--<span class="delete-rule btn" data="<?php echo $autoopti['id']; ?>"> Delete </span>-->
													    
													    <button type="button" class="delete-rule btn " data="<?php echo $autoopti['id']; ?>"  data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Deleting..">Delete</button>
									</td>
	
									</tr>
									
								 <?php 	}  ?>	
								</tbody>
							</table>
						</div>
							<header class="d-sm-flex flex-wrap align-items-center">
							
						</header>
					</div>
					<!--<div class="pagination-bar">
						<div class="holder">
							<div class="container d-sm-flex flex-wrap align-items-center">
								<div class="display-pagination">Showing Campaigns (1-10) of 100</div>
								<div class="dispaly-per-page d-flex flex-wrap align-items-center justify-content-center justify-content-sm-end">
									<div class="text">Campaigns per page:</div>
									<div class="select-holder">
										<select>
											<option>10</option>
											<option>20</option>
											<option>30</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>-->
				</div>
			</main>
			
			
	
<?php } ?>

<main id="full_loading" style="display:none">
			    <div class="step-bar">
				
				</div>
				<div  class="main-content s1" >
				    	
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										<span id="full_loading_heading"></span>
										Starting The Optimization Tool
										<span class="sub-heading s1">
											PLEASE DO NOT CLOSE THIS PAGE OR CLICK THE BACK BUTTON
										</span>
									</h2>
									
									
										<div  class="loading" style="margin:auto;width:400px">
										    	<div id="lottie" style="width:400px;height:300px"></div>
										    
										</div>
									
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				</main>
        

<!--End Content-->

<!--<div id="preloaderMain" style="display: none;">-->
<!--            <div id="statusMain"><i class="fa fa-spinner fa-spin"></i>-->
<!--           <span>Setting up your rules dashboard now…..</span>-->
<!--            </div>-->
            
        </div>
        
   
        
        
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<script src="<?php echo $this->config->item('assets'); ?>/optimize/js/jquery.main.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Montserrat:400,500,600']
			}
		});
	</script>



    <style>
	.jcf-select-autoopvariable{
		width:auto !important;
	}
	</style>
	
	
	
    <script type="text/javascript">
    
    <?php if (isset($adaccountsCount) && $adaccountsCount > 1) { ?>
    // 	 $('.optimization_rule_list').show(); 
    
    
	<?php }else{ ?>
	
// 	  $('#full_loading').show();
// 	  animate_spinner();
	
// 	 setTimeout(function(){ 
// 	    $('.optimization_rule_list').show();  
// 	    $('#full_loading').hide();
	    
// 	 }, 3000);
	

	<?php } ?>
    
    
    
    
    
    
        function edititcamp(id){
             document.getElementById("theidautocamp").value = document.getElementById("autocampid"+id).value;
             document.getElementById("campaignsid").value = document.getElementById("autocampcampid"+id).value;
             document.getElementById("acparam1").value = document.getElementById("param1"+id).value;
             var s = document.getElementById("param2"+id).value;
             //$('#acparam2 option[value='+s+']').attr('selected','selected');
             document.getElementById("acparam2").value = document.getElementById("param2"+id).value;
             document.getElementById("acparam3").value = document.getElementById("param3"+id).value;
             document.getElementById("acparam4").value = document.getElementById("param4"+id).value;
             document.getElementById("acparam5").value = document.getElementById("param5"+id).value;
             document.getElementById("acparam6").value = document.getElementById("param6"+id).value;
             document.getElementById("timeframe").value = document.getElementById("timeframe"+id).value;
             document.getElementById("rulename").value = document.getElementById("rulename"+id).value;
			 document.getElementById("isactive").value = document.getElementById("isactive"+id).value;
             document.getElementById("checkRule").value = document.getElementById("checkRule"+id).value;
             document.getElementById("ruleMet").value = document.getElementById("ruleMet"+id).value;
             initCustomForms();
            return false;
        }
		





$('.delete-rule').click(function(){
    
   
     var id = $(this).attr('data');
     var element = $(this);
     element.button('loading');
   
    var url = site_url + 'automaticoptimization/deleteap/'+id;
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            success: function (result) {
            
                element.button('reset');
                element.parent().parent().hide();
            },
            async: false
        });
    }, 2000);
    return false;
    
    
    
});

		
        
        
        $('#use_add_account').click(function(){
            //$('#preloaderMain').show();
            $('.main_add_account').hide();
             $('#full_loading').show();
             $('#full_loading_heading').html('Setting Optimization Rule List Dashboard');
             animate_spinner();
           var addaccountID =  $("input[name='ad-account']:checked").val();
        
           if(addaccountID != "" && typeof addaccountID !== 'undefined'){
           window.location = '<?php echo base_url(); ?>automaticoptimization/'+addaccountID;
           } else{
               return false;
           }
        });
        
        
         $('#create_new_rule').click(function(){
            //$('#preloaderMain').show();
            $('.optimization_rule_list').hide();
             $('#full_loading').show();
             $('#full_loading_heading').html('Setting up Create New Rule');
             animate_spinner();
           var addaccountID =  '<?php echo $addAccountId; ?>';
       
           if(addaccountID != "" && typeof addaccountID !== 'undefined'){
           window.location = '<?php echo base_url(); ?>automaticoptimization/createrule/'+addaccountID;
           } else{
               return false;
           }
        });
        
      
        
        
			$('.rule-status').click(function(){
		    
		    var id = $(this).attr('data');
			var isChecked = $(this).is(":checked") ? 1 : 0;
		
		
				$.ajax({
					type: "POST",
					cache: false,
					url: "<?php echo base_url().'automaticoptimization/update_aoactivation'?>/",
					data: {
						isactive: isChecked,
						Id: id
					},
					beforeSend: function (){
						
					},
					success: function (html){
							
					},
					async: true
				});
		
    
		
		});
		
		
		
// 		$('.btn').on('click', function() {
//             var $this = $(this);
//           $this.button('loading');
//             setTimeout(function() {
//               $this.button('reset');
//           }, 8000);
//      });

		
		
		
		
    </script>
   
  </main>