<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>

<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('menu'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <div class="col-md-7  p-rl-3">
                        <h3 class="page-title">
                           
                        </h3>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="row">
                    <center><h1>About Us</h1> </center>
                    <br>
                    <p>The Campaign Maker is a web based Facebook marketing platform that is designed specifically for Facebook marketers who want to automate and simplify their facebook marketing tasks. 
                    </p>
                    <p>Setting up a proper Facebook campaign can take hours to split test and optimize, while using The Campaign Maker we have been able to drastically reduce the time spent on setting up highly targeted and result oriented campaigns.
                    </p><p>
                        In addition to the speed and autonomous features of the campaign maker is the simplistic report that declutters and focuses on certain metrics that would make or break your campaign. This simplistic report is called the snapshot report that has proven to be extremely valuable to brand new marketers that would otherwise be confused and mislead with other marketing platforms.
                    </p><p>
                        The Campaign Maker is developed and managed by a Florida based Facebook marketing agency by the name of FUBSZ LLC which has managed 200+ clients and 2,000+ campaigns in 2015 alone.
                    </p><p>
                        Since Facebook is the next biggest potential search engine of the world Facebook marketing strategies are going to be the needs of the coming era. What we present is an easy and time-saving solution to the Facebook marketing dilemma.
                    </p><p>
                        The multiple features that the campaign maker offers distinguishes it from Facebook’s native marketing platform. These include the  interest search engine, automated split testing and the convenient campaign maker tools offered in the reporting section.
                    </p>
                    <p>
                        What  we offer is an effective, simple and economical solution to the tedious and tiring procedure of Facebook marketing which not only ensures properly made campaigns but is also time-saving.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>