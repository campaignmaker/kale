<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('menu'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <div class="col-md-7  p-rl-3">
                        <h3 class="page-title">
                            Like Reports
                        </h3>
                    </div>
                    <!--<div class="col-md-5 p-rl-3">
                        <div class="upgrade-now">
                            <span>Your trial will expire in 3 day(s)</span> <button class="btn btn-sm blue"><i class="fa fa-level-up"></i> Upgrade Now </button>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>

                    <div class="col-md-9 col-sm-8 p-rl-3">   

                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo base_url() . 'reports'; ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="<?php echo base_url() . 'reports'; ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Like Reports</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3 col-sm-4 p-rl-5">
                        <div class="form-group m-t-15">
                            <select class="form-control" name="changeDateRange" onchange="getCampaign('<?php echo $addAccountId; ?>', this.value)">
                                <option value="">Select Duration</option>
                                <option value="today" <?php if ($limit == 'today') echo 'selected'; ?>>Today</option>
                                <option value="yesterday" <?php if ($limit == 'yesterday') echo 'selected'; ?>>Yesterday</option>
                                <option value="last_3_days" <?php if ($limit == 'last_3_days') echo 'selected'; ?>>Last 3 Days</option>
                                <option value="this_week" <?php if ($limit == 'this_week') echo 'selected'; ?>>This Week</option>
                                <option value="last_week" <?php if ($limit == 'last_week') echo 'selected'; ?>>Last Week</option>
                                <option value="last_7_days" <?php if ($limit == 'last_7_days') echo 'selected'; ?>>Last 7 Days</option>
                                <option value="last_14_days" <?php if ($limit == 'last_14_days') echo 'selected'; ?>>Last 14 Days</option>
                                <option value="last_30_days" <?php if ($limit == 'last_30_days') echo 'selected'; ?>>Last 30 Days</option>
                                <option value="last_90_days" <?php if ($limit == 'last_90_days') echo 'selected'; ?>>Last 90 Days</option>
                                <option value="this_month" <?php if ($limit == 'this_month') echo 'selected'; ?>>This Month</option> 
                                <option value="last_month" <?php if ($limit == 'last_month') echo 'selected'; ?>>Last Month</option>
                                <option value="last_3_months" <?php if ($limit == 'last_3_months') echo 'selected'; ?>>Last 3 Month</option>
                                <option value="lifetime" <?php if ($limit == 'lifetime') echo 'selected'; ?>>Lifetime</option>
                            </select>
                        </div>
                    </div>

                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- BEGIN DASHBOARD STATS -->
            <div class="campaigns">
                <div class="col-md-12 p-rl-5">
                    <div class="col-md-6">
                        <div class="portlet box blue-madison desktop-mobile1" style="margin-bottom:7px;">
                            <div class="portlet-title">
                                <div class="caption">
                                    &nbsp; Mobile VS Desktop
                                </div>
                            </div>
                            <div class="portlet-body d-m">
                                <div class="desktop-mobile">
                                    <div class="lapmobbox">
                                        <div align="center" class="col-md-4 col-sm-4 desktop"> 
                                            <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/laptop.png" alt="">
                                            <h2 class="desktop-value"><?php echo $mobileData['desktop']; ?>%</h2>
                                            <span class="desktop-name">Desktop</span>
                                        </div>
                                        <div class="col-md-4 col-sm-4 slider-avg">
                                            <br />
                                            <div class="progress progress-striped active">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                                                </div>
                                            </div>
                                        </div>
                                        <div align="center" class="col-md-4 col-sm-4 mobile"> <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/smartphone.png" alt="">
                                            <h2 class="mobile-value"><?php echo $mobileData['mobile']; ?>%</h2>
                                            <span class="mobile-name">Mobile</span> 
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="portlet box  blue-madison male-female-div">
                            <div class="portlet-title">
                                <div class="caption">
                                    &nbsp; Gender
                                </div>
                            </div>
                            <div class="portlet-body d-m">
                                <div class="male-female">
                                    <div class="lapmobbox">
                                        <div align="center" class="col-md-6 col-sm-6 col-xs-6 male"> 
                                            <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/male.png" alt="">
                                            <h2 class="male-value"><?php echo $genderData['male']; ?>%</h2>
                                        </div>
                                        <div align="center" class="col-md-6 col-sm-6 col-xs-6 female"> <img src="<?php echo $this->config->item('assets'); ?>admin/layout/img/female.png" alt="">
                                            <h2 class="female-value"><?php echo $genderData['female']; ?>%</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END DASHBOARD STATS -->
                <div class="clearfix"></div>
                <div class="clearfix"></div>
                <div class="row" id="commongraph" >
                    <div class="col-md-3 country-div">
                        <div class="portlet box blue-hoki">
                            <div class="portlet-title">
                                <div class="caption">
                                    Country
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div>
                                    <div id="country" class="chart"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 p-l-5">
                        <div class="col-md-6 p-rl-5 age-div">
                            <div class="portlet box blue-hoki">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Age
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div>
                                        <div id="age" class="chart"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 p-rl-5 time-div">
                            <div class="portlet box blue-hoki">
                                <div class="portlet-title">
                                    <div class="caption">
                                        &nbsp; Time
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div>
                                        <div id="time" class="chart"></div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <input type="hidden" id="addAccountId" name="addAccountId" value="<?php echo $addAccountId; ?>">
            <input type="hidden" id="pageName" name="pageName" value="campaignsReport">
            <!--<input type="hidden" id="dayLimit" name="dayLimit" value="<?php //echo $this->session->userdata('sDate').'#'.$this->session->userdata('eDate');  ?>">-->
            <input type="hidden" id="dayLimit" name="dayLimit" value="<?php echo $this->session->userdata('dateval'); ?>">

        </div>
    </div>
    <!-- END CONTENT -->
</div>
