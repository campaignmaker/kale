<div class="main-statistics">
						<ul class="statistics-list d-flex flex-wrap">
							<li class="d-flex align-items-center">
								<div class="icon-holder">
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-money.svg" alt="money">
								</div>
								<div class="text-holder">
									<?php
                                if(round($adaccounts['campaign_header']['spent']) < 1000){
                                    echo '<div class="value">';
                                                }else{
                                    echo '<div class="value spentli">';
                                                }
                          if ($adaccounts['campaign_header']['spent'] > 0) {
                              echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['spent'], 2, '.', ',');
                          } else {
                              echo "0";
                          }
                          ?></div>
									<span class="title">TOTAL SPENT</span>
								</div>
							</li>
							<li class="d-flex align-items-center">
								<div class="icon-holder">
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-paper-plane.svg" alt="money">
								</div>
								<div class="text-holder">
									<div class="value"><?php echo number_format($adaccounts['campaign_header']['actions']); ?></div>
									<span class="title">ACTIONS</span>
								</div>
							</li>
							<li class="d-flex align-items-center">
								<div class="icon-holder">
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-calculator.svg" alt="money">
								</div>
								<div class="text-holder">
									<div class="value"><?php 
									if($adaccounts['campaign_header']['actions'] > 0) 
									echo $this->session->userdata('cur_currency') . number_format($adaccounts['campaign_header']['spent']/$adaccounts['campaign_header']['actions'], 2, '.', ',');
									else
									echo "0"; ?></div>
									<span class="title">COST PER ACTION</span>
								</div>
							</li>
							<li class="d-flex align-items-center">
								<div class="icon-holder">
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-megaphone.svg" alt="money">
								</div>
								<div class="text-holder">
									<div class="value"><?php 
									if ($adaccounts['campaign_header']['reach'] > 0) {
                             echo number_format($adaccounts['campaign_header']['reach']);
                          } else {
                              echo "0";
                          }
									?></div>
									<span class="title">REACH</span>
								</div>
							</li>
						</ul>
					</div>
					<div class="main-data">
						<div class="data-row open-close">
							<div class="frame d-flex flex-wrap align-items-center">
								<div class="data-holder d-flex flex-wrap align-items-center">
									<div class="column select-column">
										<div class="select-holder">
											<select class="ParamDD" onchange="getDetailData(this);">
												<option value="impressions">Impressions</option>
												<option value="inline_link_clicks">Clicks</option>
												<option value="page_engagement">Engagement</option>
												<option value="offsite_conversion">Conversion</option>
												<option value="like">Page Like</option>
												<option value="video_view">3-Second Video Views</option>
												<option value="app_install">App Installs</option>
                                                <option value="leadgen_other">Leads (Form)</option>
											    <option value="mobile_app_install">Mobile App Installs</option>
											    <option value="offsite_conversion_fb_pixel_add_payment_info">Adds Payment Info</option>
											    <option value="offsite_conversion_fb_pixel_add_to_cart">Adds To Cart</option>
											    <option value="offsite_conversion_fb_pixel_add_to_wishlist">Adds To Wishlist</option>
											    <option value="offsite_conversion_fb_pixel_complete_registration">Completed Registration</option>
											    <option value="offsite_conversion_fb_pixel_initiate_checkout">Initiates Checkout</option>
											    <option value="offsite_conversion_fb_pixel_lead">Leads</option>
											    <option value="offsite_conversion_fb_pixel_purchase">Purchases</option>
											    <option value="offsite_conversion_fb_pixel_search">Searches</option>
											    <option value="offsite_conversion_fb_pixel_view_content">Views Content</option>
											    <option value="offsite_conversion_key_page_view">Key Page Views</option>
											    <option value="onsite_conversion_messaging_first_reply">New Messaging Conversations</option>
											</select>
										</div>
									</div>
									<div class="column firstval">
										<a class="tab-opener" href="#">
										    <?php
                                
                                if(round($adaccounts['campaign_header']['impressions']) < 1000){
                                    echo '<strong class="value">';
                                                }else{
                                    echo '<strong class="value spentli">';
                                                }
                            echo number_format($adaccounts['campaign_header']['impressions']);
                          ?></strong>
											<span class="title">Impressions</span>
										</a>
									</div>
									<div class="column secondval">
										<a class="tab-opener" href="#" >
											<strong class="value"><?php
                                echo $this->session->userdata('cur_currency'); ?><?php
                            //echo number_format($adaccounts['campaign_header']['cpm']);
							if($adaccounts['campaign_header']['impressions'] != '0'){
								$subcpc = $adaccounts['campaign_header']['impressions']/1000;
								echo round($adaccounts['campaign_header']['spent']/$subcpc,2);
							} ?></strong>
											<span class="title">CPM</span>
										</a>
									</div>
									<div class="column thirdval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php echo number_format($adaccounts['campaign_header']['newFrequ'], 2); ?></strong>
											<span class="title">Frequency</span>
										</a>
									</div>
									<div class="column btn-column">
										<a href="#" class="btn btn-primary opener" onclick="return GetBreakdown(this);" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading.." >Breakdown</a>
									</div>
								</div>
								<div class="action">
									<a href="#" class="btn btn-add" onclick="return addnewanalytics(this);"><span class="icon-plus"></span></a>
								</div>
							</div>
							<div class="slide">
								<div class="breakdown-data d-flex flex-wrap">
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">GENDER</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="genderbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">AGE GROUPS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="agegroupbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">PLACEMENTS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="placementbody">
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="data-row open-close">
							<div class="frame d-flex flex-wrap align-items-center">
								<div class="data-holder d-flex flex-wrap align-items-center">
									<div class="column select-column">
										<div class="select-holder">
											<select class="ParamDD" onchange="getDetailData(this);">
											    <option value="inline_link_clicks">Clicks</option>
											    <option value="impressions">Impressions</option>
											    <option value="page_engagement">Engagement</option>
												<option value="offsite_conversion">Conversion</option>
												<option value="like">Page Like</option>
												<option value="video_view">3-Second Video Views</option>
												<option value="app_install">App Installs</option>
                                                <option value="leadgen_other">Leads (Form)</option>
											    <option value="mobile_app_install">Mobile App Installs</option>
											    <option value="offsite_conversion_fb_pixel_add_payment_info">Adds Payment Info</option>
											    <option value="offsite_conversion_fb_pixel_add_to_cart">Adds To Cart</option>
											    <option value="offsite_conversion_fb_pixel_add_to_wishlist">Adds To Wishlist</option>
											    <option value="offsite_conversion_fb_pixel_complete_registration">Completed Registration</option>
											    <option value="offsite_conversion_fb_pixel_initiate_checkout">Initiates Checkout</option>
											    <option value="offsite_conversion_fb_pixel_lead">Leads</option>
											    <option value="offsite_conversion_fb_pixel_purchase">Purchases</option>
											    <option value="offsite_conversion_fb_pixel_search">Searches</option>
											    <option value="offsite_conversion_fb_pixel_view_content">Views Content</option>
											    <option value="offsite_conversion_key_page_view">Key Page Views</option>
											    <option value="onsite_conversion_messaging_first_reply">New Messaging Conversations</option>
											</select>
										</div>
									</div>
									<div class="column firstval">
										<a class="tab-opener" href="#">
											<?php 
                              if(round($adaccounts['campaign_header']['spent']) < 1000){
                                    echo '<strong class="value">';
                                                }else{
                                    echo '<strong class="value spentli">';
                                                }
                              echo number_format($adaccounts['campaign_header']['inline_link_clicks']); ?></strong>
											<span class="title">Clicks</span>
										</a>
									</div>
									<div class="column secondval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php
                            if ($adaccounts['campaign_header']['spent'] > 0)
                            {
                                $cost_per_click = $adaccounts['campaign_header']['spent'] / $adaccounts['campaign_header']['inline_link_clicks'];
                                if ($cost_per_click > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($cost_per_click, 2, '.', ',');
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($cost_per_click);
                                }
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . $adaccounts['campaign_header']['inline_link_clicks'];
                            }?></strong>
											<span class="title">Per Click</span>
										</a>
									</div>
									<div class="column thirdval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php if ($adaccounts['campaign_header']['impressions'] > 0)
                            {
                                $click_rate = ($adaccounts['campaign_header']['inline_link_clicks'] / $adaccounts['campaign_header']['impressions'])*100;
                                if ($click_rate > 0)
                                {
                                    echo round(number_format($click_rate, 3, '.', ','),2)."%";
                                }
                            }
                             ?></strong>
											<span class="title">Click Rate</span>
										</a>
									</div>
									<div class="column btn-column">
										<button  class="btn btn-primary opener" onclick="return GetBreakdown(this);" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading..">Breakdown</button>
									</div>
								</div>
								<div class="action">
									<a href="#" class="btn btn-remove" onclick="return removenewanalytics(this);"><span class="icon-bin"></span></a>
								</div>
							</div>
							<div class="slide">
								<div class="breakdown-data d-flex flex-wrap">
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">GENDER</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="genderbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">AGE GROUPS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="agegroupbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">PLACEMENTS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="placementbody">
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="data-row open-close">
							<div class="frame d-flex flex-wrap align-items-center">
								<div class="data-holder d-flex flex-wrap align-items-center">
									<div class="column select-column">
										<div class="select-holder">
											<select class="ParamDD" onchange="getDetailData(this);">
											    <option value="page_engagement">Engagement</option>
											    <option value="impressions">Impressions</option>
												<option value="inline_link_clicks">Clicks</option>
											    <option value="offsite_conversion">Conversion</option>
												<option value="like">Page Like</option>
												<option value="video_view">3-Second Video Views</option>
												<option value="app_install">App Installs</option>
                                                <option value="leadgen_other">Leads (Form)</option>
											    <option value="mobile_app_install">Mobile App Installs</option>
											    <option value="offsite_conversion_fb_pixel_add_payment_info">Adds Payment Info</option>
											    <option value="offsite_conversion_fb_pixel_add_to_cart">Adds To Cart</option>
											    <option value="offsite_conversion_fb_pixel_add_to_wishlist">Adds To Wishlist</option>
											    <option value="offsite_conversion_fb_pixel_complete_registration">Completed Registration</option>
											    <option value="offsite_conversion_fb_pixel_initiate_checkout">Initiates Checkout</option>
											    <option value="offsite_conversion_fb_pixel_lead">Leads</option>
											    <option value="offsite_conversion_fb_pixel_purchase">Purchases</option>
											    <option value="offsite_conversion_fb_pixel_search">Searches</option>
											    <option value="offsite_conversion_fb_pixel_view_content">Views Content</option>
											    <option value="offsite_conversion_key_page_view">Key Page Views</option>
											    <option value="onsite_conversion_messaging_first_reply">New Messaging Conversations</option>
											</select>
										</div>
									</div>
									<div class="column firstval">
										<a class="tab-opener" href="#">
											<?php 
                                if(round($adaccounts['campaign_header']['spent']) < 1000){
                                    echo '<strong class="value">';
                                                }else{
                                    echo '<strong class="value spentli">';
                                                }
                                echo number_format($adaccounts['campaign_header']['inline_post_engagement']); ?></strong>
											<span class="title">Engages</span>
										</a>
									</div>
									<div class="column secondval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php
                                echo $this->session->userdata('cur_currency').round(($adaccounts['campaign_header']['spent']/$adaccounts['campaign_header']['inline_post_engagement']),2);
                            ?></strong>
											<span class="title">Per Engage</span>
										</a>
									</div>
									<div class="column thirdval">
										<a class="tab-opener" href="#">
											<strong class="value"><?php if ($adaccounts['campaign_header']['impressions'] > 0)
                            {
                                $inline_posteng = ($adaccounts['campaign_header']['inline_post_engagement'] / $adaccounts['campaign_header']['impressions'])*100;
                                if ($inline_posteng > 0)
                                {
                                    echo round(number_format($inline_posteng, 3, '.', ','),2)."%";
                                }
                            }
                             ?></strong>
											<span class="title">Engage Rate</span>
										</a>
									</div>
									<div class="column btn-column">
										<a href="#" class="btn btn-primary opener" onclick="return GetBreakdown(this);" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading..">Breakdown</a>
									</div>
								</div>
								<div class="action">
									<a href="#" class="btn btn-remove" onclick="return removenewanalytics(this);"><span class="icon-bin"></span></a>
								</div>
							</div>
							<div class="slide">
								<div class="breakdown-data d-flex flex-wrap">
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">GENDER</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="genderbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">AGE GROUPS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="agegroupbody">
												
											</tbody>
										</table>
									</div>
									<div class="table-holder">
										<table class="table-s1">
											<thead>
												<tr>
													<th class="highlight">PLACEMENTS</th>
													<th>TOTAL</th>
												</tr>
											</thead>
											<tbody class="placementbody">
											
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="campaign-list">
						<header class="d-flex flex-wrap align-items-center justify-content-between">
							<div class="campaign-category d-flex flex-wrap align-items-center justify-content-center justify-content-md-start">
								<strong class="title">Campaign List</strong>
								<div class="select-holder">
									<select onchange="getAICampaign(this.value)">
										<option value="active">Active Campaigns</option>
										<option value="inactive" <?php echo $this->session->userdata('camptype') == "inactive"?"selected":""; ?>>Inactive Campaigns</option>
									</select>
								</div>
							</div>
							<div class="btn-holder">
								<a href="<?php echo site_url('createcampaign'); ?>" class="btn btn-primary">New Campaign</a>
								<!-- <a href="#" class="btn btn-primary">Export Table</a> -->
								<a href="<?php echo site_url('analysis'); ?>" class="btn btn-primary">Compare</a>
							</div>
						</header>
						<div class="table-holder">
							<table class="table-s2" id="datatable-responsive">
								<colgroup>
									<col class="col1">
									<col class="col2">
									<col class="col3">
									<col class="col4">
									<col class="col5">
									<col class="col6">
								</colgroup>
								<thead>
									<tr>
										<th>STATUS</th>
										<th>CAMPAIGN NAME</th>
										<th>REACH</th>
										<th>TOTAL SPENT</th>
										<th>DATE STARTED</th>
										<th>ACTION</th>
									</tr>
								</thead>
								<tbody>
								    <?php 
						//echo "<pre>";
						//print_r($adaccounts['campaigns']);
						//echo "</pre>";
						
						    $activeorinactive = "";
						    if ($this->session->userdata('camptype') == "inactive"){
                                $activeorinactive = "inactive";
                            }
                            else{
                                $activeorinactive = "active";
                            }
					//	echo $activeorinactive;
                            if (isset($adaccounts['campaigns'])){ 
                                foreach ($adaccounts['campaigns'] as $campaign){
                                    $uniqueId = 'lcs_check_'.rand();
                                    if($campaign['campaign_effective_status'])
                                    if($activeorinactive == 'active'){
                                        if($campaign['campaign_effective_status'] == 'Active'){
                                    
                                    ?>
                                        
									<tr class="open-close">
										<td colspan="6">
											<table>
											    <colgroup>
													<col class="col1">
													<col class="col2">
													<col class="col3">
													<col class="col4">
													<col class="col5">
													<col class="col6">
												</colgroup>
												<tr class="highlight-data">
												    <?php if($campaign['campaign_effective_status'] == 'Active'){
                                                        ?>
                                                        <td title="STATUS">
    														<label class="switch">
    															<input type="checkbox"checked name="check-1" value="<?php echo $campaign['campaign_id']; ?>" id="<?php echo $uniqueId; ?>" onchange="fnChangeCheck('<?php echo $uniqueId; ?>', '<?php echo $campaign['campaign_id']; ?>')">
    															<span class="slider"></span>
    														</label>
    													</td>
                                                          
                                                        <?php
                                                    }
                                                    else if($campaign['campaign_effective_status'] == 'In Review'){
                                                        ?>
                                                        <td title="STATUS">
    														<label class="switch">
    															<span class="slider">In Review</span>
    														</label>
    													</td>
                                                            
                                                        <?php
                                                    }
                                                    else if($campaign['campaign_effective_status'] == 'Denied'){
                                                        ?>
                                                        <td title="STATUS">
    														<label class="switch">
    															<span class="slider"> Denied</span>
    														</label>
    													</td>
                                                            
                                                        <?php
                                                    }
                                                    else{
                                                        ?>
                                                        <td title="STATUS">
    														<label class="switch">
    															<input type="checkbox" name="check-1" class="lcs_check" value="<?php echo $campaign['campaign_id']; ?>" id="<?php echo $uniqueId; ?>" onchange="fnChangeCheck('<?php echo $uniqueId; ?>', '<?php echo $campaign['campaign_id']; ?>')">
    															<span class="slider"></span>
    														</label>
    													</td>
                                                        <?php
                                                    } ?>
													
													<td class="Campaign Name">
                                                        <a href='<?php echo site_url('addset/' . $addAccountId . '/' . $campaign['campaign_id']); ?>'>
                                                            <?php echo $campaign['campaign_name'] ?>
                                                        </a>
                                                    </td>
													<td title="Reach"><?php echo number_format($campaign['reach'], 0, '.', ','); ?></td>
													<td title="TOTAL SPENT"><?php echo $this->session->userdata('cur_currency') . number_format($campaign['campaign_spent'], 2, '.', ','); ?></td>
													<td title="DATE STARTED"><?php echo date("d/m/y", strtotime($campaign['campaign_created_time'])); ?></td>
													<td title="action">
														<a class="opener" href="#">
															<span class="close-text">Breakdown</span>
															<span class="open-text">Hide</span>
														</a>
													</td>
												</tr>
												<tr>
													<td colspan="6" class="slide-content">
														<div class="slide">
															<div class="table-row d-flex flex-wrap">
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-marketing.svg" alt="marketing">
																					</div>
																					<span class="text">Impressions</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['campaign_impressions']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPM</span>
																				</div>
																			</td>
																			<td><?php
                                                                                echo $this->session->userdata('cur_currency');
                                                							if($campaign['campaign_impressions'] != '0'){
                                                								$subcpc = $campaign['campaign_impressions']/1000;
                                                								echo round($campaign['campaign_spent']/$subcpc,2);
                                                							} ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-file-sharing.svg" alt="Frequency">
																					</div>
																					<span class="text">Frequency</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['frequency'], 2); ?>%</td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-click.png" alt="click">
																					</div>
																					<span class="text">Clicks</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['campaign_inline_link_clicks']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPC</span>
																				</div>
																			</td>
																			<td>
																			    <?php
																			     if ($campaign['campaign_spent'] > 0)
                            {
                                $cost_per_click = $campaign['campaign_spent'] / $campaign['campaign_inline_link_clicks'];
                                if ($cost_per_click > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($cost_per_click, 2, '.', ',');
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($cost_per_click);
                                }
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . "0";
                            }
                            ?>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="conversion">
																					</div>
																					<span class="text">CTR</span>
																				</div>
																			</td>
																			<td>
																			    <?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $click_rate = ($campaign['campaign_inline_link_clicks'] / $campaign['campaign_impressions'])*100;
                                if ($click_rate > 0)
                                {
                                    echo round(number_format($click_rate, 3, '.', ','),2)."%";
                                }
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-engagement.png" alt="engagement">
																					</div>
																					<span class="text">Engagement</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['inline_post_engagement']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPE</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['inline_post_engagement']),2);
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="Engage Rate">
																					</div>
																					<span class="text">Engage Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $inline_posteng = ($campaign['inline_post_engagement'] / $campaign['campaign_impressions'])*100;
                                if ($inline_posteng > 0)
                                {
                                    echo round(number_format($inline_posteng, 3, '.', ','),2)."%";
                                }
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-page-like.png" alt="page-like">
																					</div>
																					<span class="text">Page Likes</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['page_like']);?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPPL</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['page_like']), 2);
                                ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="Engage Rate">
																					</div>
																					<span class="text">Like Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $page_like = ($campaign['page_like'] / $campaign['campaign_impressions'])*100;
                                if ($page_like > 0)
                                {
                                    echo round(number_format($page_like, 3, '.', ','),2)."%";
                                }
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
															</div>
															<div class="table-row d-flex flex-wrap">
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-phone.svg" alt="phone">
																					</div>
																					<span class="text">App Installs</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['app_install']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPAI</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['app_install']),2);
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-file-sharing.svg" alt="Frequency">
																					</div>
																					<span class="text">Install Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $app_install = ($campaign['app_install'] / $campaign['campaign_impressions'])*100;
                                if ($app_install > 0)
                                {
                                    echo round(number_format($app_install, 3, '.', ','),2)."%";
                                }
								else{
									echo "0%";
								}
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-social-networks.svg" alt="social network">
																					</div>
																					<span class="text">(Form) Leads</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['leadgen']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPFL</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['leadgen']),2);
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="conversion">
																					</div>
																					<span class="text">Lead Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $leadgenother = ($campaign['leadgen'] / $campaign['campaign_impressions'])*100;
                                if ($leadgenother > 0)
                                {
                                    echo round(number_format($leadgenother, 3, '.', ','),2)."%";
                                }
								else{
									echo "0%";
								}
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-view.png" alt="view">
																					</div>
																					<span class="text">10s Views</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['video_10_sec_watched_actions']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPV</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['video_10_sec_watched_actions']),2);
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="Engage Rate">
																					</div>
																					<span class="text">View Rate</span>
																				</div>
																			</td>
																			<td><?php echo round(number_format($campaign['video_avg_percent_watched_actions'], 3, '.', ','),2)."%"; ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-tree.png" alt="tree">
																					</div>
																					<span class="text">Conversions</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['campaign_actions']);?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPConv</span>
																				</div>
																			</td>
																			<td><?php
                                if ($campaign['campaign_cost_per_total_action'] > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($campaign['campaign_cost_per_total_action'], 2, '.', ',');
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($campaign['campaign_cost_per_total_action']);
                                }
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="Engage Rate">
																					</div>
																					<span class="text">Conv Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $web_conv = ($campaign['campaign_actions'] / $campaign['campaign_impressions'])*100;
                                if ($web_conv > 0)
                                {
                                    echo round(number_format($web_conv, 3, '.', ','),2)."%";
                                }
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<?php
                                        }
                                    }
                                    elseif($activeorinactive == 'inactive'){
                                        if($campaign['campaign_effective_status'] != 'Active'){?>
                                       <tr class="open-close">
										<td colspan="6">
											<table>
											    <colgroup>
													<col class="col1">
													<col class="col2">
													<col class="col3">
													<col class="col4">
													<col class="col5">
													<col class="col6">
												</colgroup>
												<tr class="highlight-data">
												    <?php if($campaign['campaign_effective_status'] == 'Active'){
                                                        ?>
                                                        <td title="STATUS">
    														<label class="switch">
    															<input type="checkbox"checked name="check-1" value="<?php echo $campaign['campaign_id']; ?>" id="<?php echo $uniqueId; ?>" onchange="fnChangeCheck('<?php echo $uniqueId; ?>', '<?php echo $campaign['campaign_id']; ?>')">
    															<span class="slider"></span>
    														</label>
    													</td>
                                                          
                                                        <?php
                                                    }
                                                    else if($campaign['campaign_effective_status'] == 'In Review'){
                                                        ?>
                                                        <td title="STATUS">
    														<label class="switch">
    															<span class="slider">In Review</span>
    														</label>
    													</td>
                                                            
                                                        <?php
                                                    }
                                                    else if($campaign['campaign_effective_status'] == 'Denied'){
                                                        ?>
                                                        <td title="STATUS">
    														<label class="switch">
    															<span class="slider"> Denied</span>
    														</label>
    													</td>
                                                            
                                                        <?php
                                                    }
                                                    else{
                                                        ?>
                                                        <td title="STATUS">
    														<label class="switch">
    															<input type="checkbox" name="check-1" class="lcs_check" value="<?php echo $campaign['campaign_id']; ?>" id="<?php echo $uniqueId; ?>" onchange="fnChangeCheck('<?php echo $uniqueId; ?>', '<?php echo $campaign['campaign_id']; ?>')">
    															<span class="slider"></span>
    														</label>
    													</td>
                                                        <?php
                                                    } ?>
													
													<td class="Campaign Name">
                                                        <a href='<?php echo site_url('addset/' . $addAccountId . '/' . $campaign['campaign_id']); ?>'>
                                                            <?php echo $campaign['campaign_name'] ?>
                                                        </a>
                                                    </td>
													<td title="Reach"><?php echo number_format($campaign['reach'], 0, '.', ','); ?></td>
													<td title="TOTAL SPENT"><?php echo $this->session->userdata('cur_currency') . number_format($campaign['campaign_spent'], 2, '.', ','); ?></td>
													<td title="DATE STARTED"><?php echo date("d/m/y", strtotime($campaign['campaign_created_time'])); ?></td>
													<td title="action">
														<a class="opener" href="#">
															<span class="close-text">Breakdown</span>
															<span class="open-text">Hide</span>
														</a>
													</td>
												</tr>
												<tr>
													<td colspan="6" class="slide-content">
														<div class="slide">
															<div class="table-row d-flex flex-wrap">
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-marketing.svg" alt="marketing">
																					</div>
																					<span class="text">Impressions</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['campaign_impressions']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPM</span>
																				</div>
																			</td>
																			<td><?php
                                                                                echo $this->session->userdata('cur_currency');
                                                							if($campaign['campaign_impressions'] != '0'){
                                                								$subcpc = $campaign['campaign_impressions']/1000;
                                                								echo round($campaign['campaign_spent']/$subcpc,2);
                                                							} ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-file-sharing.svg" alt="Frequency">
																					</div>
																					<span class="text">Frequency</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['frequency'], 2); ?>%</td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-click.png" alt="click">
																					</div>
																					<span class="text">Clicks</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['campaign_inline_link_clicks']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPC</span>
																				</div>
																			</td>
																			<td>
																			    <?php
																			     if ($campaign['campaign_spent'] > 0)
                            {
                                $cost_per_click = $campaign['campaign_spent'] / $campaign['campaign_inline_link_clicks'];
                                if ($cost_per_click > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($cost_per_click, 2, '.', ',');
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($cost_per_click);
                                }
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . "0";
                            }
                            ?>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="conversion">
																					</div>
																					<span class="text">CTR</span>
																				</div>
																			</td>
																			<td>
																			    <?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $click_rate = ($campaign['campaign_inline_link_clicks'] / $campaign['campaign_impressions'])*100;
                                if ($click_rate > 0)
                                {
                                    echo round(number_format($click_rate, 3, '.', ','),2)."%";
                                }
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-engagement.png" alt="engagement">
																					</div>
																					<span class="text">Engagement</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['inline_post_engagement']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPE</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['inline_post_engagement']),2);
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="Engage Rate">
																					</div>
																					<span class="text">Engage Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $inline_posteng = ($campaign['inline_post_engagement'] / $campaign['campaign_impressions'])*100;
                                if ($inline_posteng > 0)
                                {
                                    echo round(number_format($inline_posteng, 3, '.', ','),2)."%";
                                }
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-page-like.png" alt="page-like">
																					</div>
																					<span class="text">Page Likes</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['page_like']);?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPPL</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['page_like']), 2);
                                ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="Engage Rate">
																					</div>
																					<span class="text">Like Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $page_like = ($campaign['page_like'] / $campaign['campaign_impressions'])*100;
                                if ($page_like > 0)
                                {
                                    echo round(number_format($page_like, 3, '.', ','),2)."%";
                                }
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
															</div>
															<div class="table-row d-flex flex-wrap">
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-phone.svg" alt="phone">
																					</div>
																					<span class="text">App Installs</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['app_install']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPAI</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['app_install']),2);
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-file-sharing.svg" alt="Frequency">
																					</div>
																					<span class="text">Install Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $app_install = ($campaign['app_install'] / $campaign['campaign_impressions'])*100;
                                if ($app_install > 0)
                                {
                                    echo round(number_format($app_install, 3, '.', ','),2)."%";
                                }
								else{
									echo "0%";
								}
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-social-networks.svg" alt="social network">
																					</div>
																					<span class="text">(Form) Leads</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['leadgen']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPFL</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['leadgen']),2);
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="conversion">
																					</div>
																					<span class="text">Lead Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $leadgenother = ($campaign['leadgen'] / $campaign['campaign_impressions'])*100;
                                if ($leadgenother > 0)
                                {
                                    echo round(number_format($leadgenother, 3, '.', ','),2)."%";
                                }
								else{
									echo "0%";
								}
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-view.png" alt="view">
																					</div>
																					<span class="text">10s Views</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['video_10_sec_watched_actions']); ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPV</span>
																				</div>
																			</td>
																			<td><?php
                                echo $this->session->userdata('cur_currency').round(($campaign['campaign_spent']/$campaign['video_10_sec_watched_actions']),2);
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="Engage Rate">
																					</div>
																					<span class="text">View Rate</span>
																				</div>
																			</td>
																			<td><?php echo round(number_format($campaign['video_avg_percent_watched_actions'], 3, '.', ','),2)."%"; ?></td>
																		</tr>
																	</table>
																</div>
																<div class="table-holder">
																	<table class="table-s3">
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-tree.png" alt="tree">
																					</div>
																					<span class="text">Conversions</span>
																				</div>
																			</td>
																			<td><?php echo number_format($campaign['campaign_actions']);?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cell d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-coin.png" alt="coin">
																					</div>
																					<span class="text">CPConv</span>
																				</div>
																			</td>
																			<td><?php
                                if ($campaign['campaign_cost_per_total_action'] > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($campaign['campaign_cost_per_total_action'], 2, '.', ',');
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($campaign['campaign_cost_per_total_action']);
                                }
                            ?></td>
																		</tr>
																		<tr>
																			<td>
																				<div class="table-cel d-flex align-items-center">
																					<div class="icon-holder">
																						<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-conversion.svg" alt="Engage Rate">
																					</div>
																					<span class="text">Conv Rate</span>
																				</div>
																			</td>
																			<td><?php if ($campaign['campaign_impressions'] > 0)
                            {
                                $web_conv = ($campaign['campaign_actions'] / $campaign['campaign_impressions'])*100;
                                if ($web_conv > 0)
                                {
                                    echo round(number_format($web_conv, 3, '.', ','),2)."%";
                                }
                            }
                             ?></td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<?php
                                }
                            }
                                }
                            }
                        ?>
								</tbody>
							</table>
						</div>
					</div>
					
			