<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-sm-8 col-md-10 col-lg-9 col-md-offset-1 displaynone tabcontent" id="step5">
  <div class="top-notification-box" id="msg_box303" style="display: none;">
      <div id="msg_err303"></div>
  </div>
  <div class="top-notification-box" id="successmsgBx303" style="display: none;">
      <div id="msg_suc303"></div>
  </div>
  <div class="main-content-wrapper">
    <div class="row">
      <div class="col-md-6">
        <div class="main-content-box inner">
          
            <div class="header-holder">
              <div class="btn-item review selected">
                <span>
                  <i class="icon-profile"></i>
                  <span class="text">Review</span>
                </span>
              </div>
              <div class="text-wrap">
                <strong class="title">Review &amp; Publish</strong>
              </div>
            </div>
            <div class="audience-setup-holder inner">
              <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <span>SET YOUR DAILY SPEND PER ADSET</span>
                    </strong>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body spend">
                      <div class="spend-set-holder">
                        <div class="review-box text-center">
                          <span class="review-title">EST. TOTAL SPEND PER DAY</span>
                          <span class="price" id="totalDailySpend-last"><?php echo $this->session->userdata('cur_currency'); ?>20</span>
                          <span class="divider">&nbsp;</span>
                          <span class="review-text" id="adsetSpend-last"><?php echo $this->session->userdata('cur_currency'); ?>5 X 4 ADSETS</span>
                        </div>
                        <div class="review-box text-center">
                          <span class="review-title">TOTAL ADS YOU WILL CREATE</span>
                          <span class="price" id='totalAdCount-last'>12</span>
                          <span class="divider">&nbsp;</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="setup-preview-box">
                <strong class="title-text"><span class="text">NUMBER OF ADSETS YOU WILL CREATE</span><span  class="count" id="audiencesCount-last">0 ADSETS</span></strong>
                <div class="table-responsive">
                  <table class="table">
                    <colgroup>
                      <col class="col1">
                      <col class="col2">
                      <col class="col3">
                    </colgroup>
                    <tbody id="audiencestableresult-last" style="display: none;">
                      
                    </tbody>
                  </table>
                  <div class="heart">
                    <a href="#" class="btn btn-save pull-right"><i class="icon-heart"></i> Save as template for later use</a>
                  </div>
                </div>
              </div>
            </div>
        
        </div>
        <div class="button-main">
          <a class="btn btn-info lastSavedBtn" href="javascript:void(0)" onclick="jQuery('#tab_1_5').removeClass('active');jQuery('#tab_1_4').addClass('active');">Back to budget</a>
          <button class="btn btn-success pull-right long lastSavedBtn" type="button" onclick="Proceed_last();">Publish The Campaign</button>
          
        </div>
      </div>
      <div class="col-md-6">
        <div class="ads-box">
          <div class="ads-header">
            <div class="ads-count POST_ENGAGEMENT_OBJ_Hide" id="showAdTextMess-last"><span>Showing ad #1 of 20 ads</span></div>
            <div class="ads-control POST_ENGAGEMENT_OBJ_Hide">
              <a href="javascript:void(0)" id="PreviousAdLink-last" class="btn-prev" onclick="fnLoadPreviousAd()">Prev</a>
              <a href="javascript:void(0)" id="NextAdLink-last" class="btn-next" onclick="fnLoadNextAd()"><span class="text">Next Ad</span></a>
            </div>
          </div>
          <div class="preview-btn">
            <a href="javascript:void(0)" onclick="jQuery('#ads_preview_mobilefeed2').hide();jQuery('#ads_preview_desktopfeed2').show();" class="btn desktop-preview"><i class="icon-calendar"></i><span>Desktop Preview</span></a>
            <a href="javascript:void(0)" onclick="jQuery('#ads_preview_desktopfeed2').hide();jQuery('#ads_preview_mobilefeed2').show();" class="btn mobile-preview pull-right"><i class="icon-calendar"></i><span>Mobile Preview</span></a>
          </div>
          <div class="admock">
            <div id="feedloader_12" class="feedloader displayNone">
                <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
            </div>
            <div id="ads_preview_desktopfeed2" class="ads-preview-panel previews-desktopfeed" align="center">
               <div class="preview-info-alert jsNoPreviewText">
                    <p class="tit">Preview not available</p>
                    <p>You must fill all required fields to see a preview of your ads.</p>
                </div><!-- /.preview-info-alert -->
                <div class="preview-info-alert preview-loading jsLoadingText hide">
                    <p class="tit">Loading preview...</p>
                </div><!-- /.preview-info -->

                <div id="js_preview_carousel_desktopfeed" class="carousel slide" data-interval="false">
                    <ul class="fb-ads-list fb-list-mobile fb-list-desktopfeed carousel-inner"></ul>
                </div><!--/.carousel-->
            </div><!-- /.ads-preview-panel -->
            
            <div id="feedloader_22" class="feedloader displayNone">
                <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
            </div>
            <div id="ads_preview_mobilefeed2" class="ads-preview-panel previews-desktopfeed displayNone" align="center">
                <div class="preview-info-alert jsNoPreviewText">
                    <p class="tit">Preview not available</p>
                    <p>You must fill all required fields to see a preview of your ads.</p>
                </div><!-- /.preview-info-alert -->
                <div class="preview-info-alert preview-loading jsLoadingText hide">
                    <p class="tit">Loading preview...</p>
                </div><!-- /.preview-info -->

                <div id="js_preview_carousel_mobilefeed" class="carousel slide" data-interval="false">
                    <ul class="fb-ads-list fb-list-mobile fb-list-desktopfeed carousel-inner"></ul>
                </div><!--/.carousel-->
            </div><!-- /.ads-preview-panel -->
            <!--<img src="images/facebook.png" alt="image description">-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade publish-campaign-popup" id="publishCampaign" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="label-holder">
              <span class="publishing-label">Publishing...</span>
            </div>
            <h2>Publishing Your Campaign Now...</h2>
            <div class="step-holder">
              <div class="step step-1 has-dot active">
                <span class="info-text active">Uploading Your Campaign Data to Facebook</span>
              </div>
              <div class="step step-2 has-dot">
                <span class="info-text">Uploading Your Campaign Data to Facebook</span>
              </div>
              <div class="step step-3">
              </div>
            </div>
          </div>
          <div class="modal-body">
            <p>This will take a few minutes depending on how many ads you created.</p>
            <div id="floatingBarsG">
              <div class="blockG" id="rotateG_01"></div>
              <div class="blockG" id="rotateG_02"></div>
              <div class="blockG" id="rotateG_03"></div>
              <div class="blockG" id="rotateG_04"></div>
              <div class="blockG" id="rotateG_05"></div>
              <div class="blockG" id="rotateG_06"></div>
              <div class="blockG" id="rotateG_07"></div>
              <div class="blockG" id="rotateG_08"></div>
              <div class="blockG" id="rotateG_09"></div>
              <div class="blockG" id="rotateG_10"></div>
              <div class="blockG" id="rotateG_11"></div>
              <div class="blockG" id="rotateG_12"></div>
            </div>
            <div class="warrning-note">
              Please DO NOT click the back button or close the window while the campaign is being published.
            </div>
          </div>
        </div>
      </div>
    </div>          
                        <div class="modal fade publish-campaign-popup" id="campaignPublished" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <a href="#" class="modal-close close-at-top" data-dismiss="modal">Close</a>
        <div class="modal-content">
          <div class="modal-header">
            <div class="label-holder">
              <span class="publishing-label">YAY!</span>
            </div>
            <h2>Your Campaign Has Been Published!.</h2>
            <div class="step-holder">
              <div class="step step-1 has-dot done">
                <span class="info-text">Uploading Your Campaign Data to Facebook</span>
              </div>
              <div class="step step-2 has-dot active">
                <span class="info-text active">Being Reviewed By Facebook <small class="additional-info">*your campaign will start delivering as soon as Facebook approves it*</small></span>
              </div>
              <div class="step step-3">
              </div>
            </div>
          </div>
          <div class="modal-body">
            <div class="option-row">
              <div class="col-left">
                <strong class="title">Create Another Campaign</strong>
                <span class="info-text">When 1 campaign is not enough.</span>
              </div>
              <div class="col-right">
                <a href="<?php echo site_url('createcampaign'); ?>" class="btn-icon"><span class="icon-holder"><span class="icon-pen"></span></span>Create Another One</a>
              </div>
            </div>
            <div class="option-row">
              <div class="col-left">
                <strong class="title">Setup Automatic Optimization Rules</strong>
                <span class="info-text">Make your campaigns work for you.</span>
              </div>
              <div class="col-right">
                <a href="<?php echo site_url('automaticoptimization'); ?>" class="btn-icon"><span class="icon-holder"><span class="icon-optimize"></span></span>Optimization Rules</a>
              </div>
            </div>
            <div class="option-row">
              <div class="col-left">
                <strong class="title">Access Your Reporting Dashboard</strong>
                <span class="info-text">See how hard your campaigns are working.</span>
              </div>
              <div class="col-right">
                <a href="<?php echo site_url("reports"); ?>" class="btn-icon"><span class="icon-holder"><span class="icon-note"></span></span>Report Dashboard</a>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            Have questions or concerns about anything? <a href="<?php echo $this->config->item('base_url'); ?>helps">Contact us now!</a>
          </div>
        </div>
      </div>
    </div>