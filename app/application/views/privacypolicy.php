<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('menu'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <div class="col-md-7  p-rl-3">
                        <h3 class="page-title">
                            Privacy Policy
                        </h3>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="row">
                    
                    <p>
                        <strong>1. Introduction</strong>
                    </p>
                    <p>
                        1.1 The Campaign Maker is committed to safeguarding the privacy of thecampaignmaker.com visitors; in this policy The Campaign Maker explain how The
                        Campaign Maker will treat your personal information.
                    </p>
                    <p>
                        1.2 The Campaign Maker will ask you to consent to our use of cookies in accordance with the terms of this policy when you first visit thecampaignmaker.com.
                        / By using thecampaignmaker.com and agreeing to this policy, you consent to our use of cookies in accordance with the terms of this policy.
                    </p>
                    <p>
                        <strong>2. Collecting personal information</strong>
                    </p>
                    <p>
                        2.1 The Campaign Maker may collect, store and use the following kinds of personal information:
                    </p>
                    <p>
                        (a) information about your computer and about your visits to and use of this website (including your IP address, geographical location, browser type and
                        version, operating system, referral source, length of visit, page views and website navigation paths);
                    </p>
                    <p>
                        (b) information that you provide to us when registering with thecampaignmaker.com (including your name, email address, country);
                    </p>
                    <p>
                        (c) information that you provide when completing your profile on thecampaignmaker.com (by connecting your Facebook account to the software which will
                        collect your name, email, country, occupation and will grant you access to the ad accounts you manage and Facebook pages you manage.);
                    </p>
                    <p>
                        (d) information that you provide to us for the purpose of subscribing to our email notifications and/or newsletters (including your name and email
                        address);
                    </p>
                    <p>
                        (e) information that you provide to us when using the services on thecampaignmaker.com, or that is generated in the course of the use of those services
                        (including the timing, frequency and pattern of service use;
                    </p>
                    <p>
                        (f) information relating to any purchases you make of our SaaS or any other transactions that you enter into through thecampaignmaker.com (including your
                        name, address, telephone number, email address and card details;
                    </p>
                    <p>
                        (g) information that you post to thecampaignmaker.com for publication on the internet (including your campaigns, adsets and ads via the marketing
                        platform);
                    </p>
                    <p>
                        (h) information contained in or relating to any communications that you send to us or send through thecampaignmaker.com (including the communication
                        content and meta data associated with the communication);
                    </p>
                    <p>
                        (i) any other personal information that you choose to send to us; and
                    </p>
                    <p>
                        (j) information for retargeting via retargeting softwares such as Facebook marketing, Google ads, etc.
                    </p>
                    <p>
                        2.2 Before you disclose to us the personal information of another person, you must obtain that person's consent to both the disclosure and the processing
                        of that personal information in accordance with this policy.
                    </p>
                    <p>
                        <strong>3. Using your personal information</strong>
                    </p>
                    <p>
                        3.1 Personal information submitted to us through thecampaignmaker.com will be used for the purposes specified in this policy or on the relevant pages of
                        the website.
                    </p>
                    <p>
                        3.2 The Campaign Maker may use your personal information to:
                    </p>
                    <p>
                        (a) administer thecampaignmaker.com and business;
                    </p>
                    <p>
                        (b) personalise thecampaignmaker.com for you;
                    </p>
                    <p>
                        (c) enable your use of the services available on thecampaignmaker.com;
                    </p>
                    <p>
                        (d) to managing accounts through thecampaignmaker.com;
                    </p>
                    <p>
                        (e) supply to you services purchased through thecampaignmaker.com;
                    </p>
                    <p>
                        (f) send statements, invoices and payment reminders to you, and collect payments from you;
                    </p>
                    <p>
                        (g) send you non-marketing commercial communications;
                    </p>
                    <p>
                        (h) send you email notifications that you have specifically requested;
                    </p>
                    <p>
                        (i) send you our email newsletter, if you have requested it (you can inform us at any time if you no longer require the newsletter);
                    </p>
                    <p>
                        (j) send you marketing communications relating to our business or the businesses of carefully-selected third parties which The Campaign Maker think may be
                        of interest to you, by post or, where you have specifically agreed to this, by email or similar technology (you can inform us at any time if you no longer
                        require marketing communications);
                    </p>
                    <p>
                        (k) provide third parties with statistical information about our users (but those third parties will not be able to identify any individual user from that
                        information);
                    </p>
                    <p>
                        (l) deal with enquiries and complaints made by or about you relating to thecampaignmaker.com;
                    </p>
                    <p>
                        (m) keep thecampaignmaker.com secure and prevent fraud;
                    </p>
                    <p>
                        (n) verify compliance with the terms and conditions governing the use of thecampaignmaker.com (including monitoring private messages sent through
                        thecampaignmaker.com private messaging service);
                    </p>
                    <p>
                        3.3 If you submit personal information for publication on thecampaignmaker.com, The Campaign Maker will publish and otherwise use that information in
                        accordance with the license you grant to us.
                    </p>
                    <p>
                        3.4 The Campaign Maker will not, without your express consent, supply your personal information to any third party for the purpose of their or any other
                        third party's direct marketing.
                    </p>
                    <p>
                        3.5 All thecampaignmaker.com financial transactions are handled through our payment services provider, PayPal and Braintree. The Campaign Maker will share
                        information with our payment services provider only to the extent necessary for the purposes of processing payments you make via thecampaignmaker.com,
                        refunding such payments and dealing with complaints and queries relating to such payments and refunds.
                    </p>
                    <p>
                        <strong>4. Disclosing personal information</strong>
                    </p>
                    <p>
                        4.1 The Campaign Maker may disclose your personal information to any of our employees, officers, insurers, professional advisers, agents, suppliers or
                        subcontractors insofar as reasonably necessary for the purposes set out in this policy.
                    </p>
                    <p>
                        4.2 The Campaign Maker may disclose your personal information to any member of our group of companies (this means our subsidiaries, our ultimate holding
                        company and all its subsidiaries) insofar as reasonably necessary for the purposes set out in this policy.
                    </p>
                    <p>
                        4.3 The Campaign Maker may disclose your personal information:
                    </p>
                    <p>
                        (a) to the extent that The Campaign Maker is required to do so by law;
                    </p>
                    <p>
                        (b) in connection with any ongoing or prospective legal proceedings;
                    </p>
                    <p>
                        (c) in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing
                        credit risk);
                    </p>
                    <p>
                        (d) to the purchaser (or prospective purchaser) of any business or asset that The Campaign Maker are (or are contemplating) selling; and
                    </p>
                    <p>
                        (e) to any person who The Campaign Maker reasonably believes may apply to a court or other competent authority for disclosure of that personal information
                        where, in our reasonable opinion, such court or authority would be reasonably likely to order disclosure of that personal information.
                    </p>
                    <p>
                        4.4 Except as provided in this policy, The Campaign Maker will not provide your personal information to third parties.
                    </p>
                    <p>
                        <strong>5. International data transfers</strong>
                    </p>
                    <p>
                        5.1 Information that The Campaign Maker collect may be stored and processed in and transferred between any of the countries in which The Campaign Maker
                        operate in order to enable us to use the information in accordance with this policy.
                    </p>
                    <p>
                        5.2 Personal information that you publish on thecampaignmaker.com or submit for publication on thecampaignmaker.com may be available, via the internet,
                        around the world. The Campaign Maker cannot prevent the use or misuse of such information by others.
                    </p>
                    <p>
                        5.3 You expressly agree to the transfers of personal information described in this Section 5.
                    </p>
                    <p>
                        <strong>6. Retaining personal information</strong>
                    </p>
                    <p>
                        6.1 This Section 6 sets out our data retention policies and procedure, which are designed to help ensure that The Campaign Maker comply with our legal
                        obligations in relation to the retention and deletion of personal information.
                    </p>
                    <p>
                        6.2 Personal information that The Campaign Maker process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or
                        those purposes.
                    </p>
                    <p>
                        6.3 Notwithstanding the other provisions of this Section 6, The Campaign Maker will retain documents (including electronic documents) containing personal
                        data:
                    </p>
                    <p>
                        (a) to the extent that The Campaign Maker is required to do so by law;
                    </p>
                    <p>
                        (b) if The Campaign Maker believe that the documents may be relevant to any ongoing or prospective legal proceedings; and
                    </p>
                    <p>
                        (c) in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing
                        credit risk).
                    </p>
                    <p>
                        <strong>7. Security of your personal information</strong>
                    </p>
                    <p>
                        7.1 The Campaign Maker will take reasonable technical and organizational precautions to prevent the loss, misuse or alteration of your personal
                        information.
                    </p>
                    <p>
                        7.2 The Campaign Maker will store all the personal information you provide on our secure (password- and firewall-protected) servers.
                    </p>
                    <p>
                        7.3 You acknowledge that the transmission of information over the internet is inherently insecure, and The Campaign Maker cannot guarantee the security of
                        data sent over the internet.
                    </p>
                    <p>
                        7.4 You are responsible for keeping the password you use for accessing thecampaignmaker.com confidential; The Campaign Maker will not ask you for your
                        password (except when you log in to thecampaignmaker.com).
                    </p>
                    <p>
                        <strong>8. Amendments</strong>
                    </p>
                    <p>
                        8.1 The Campaign Maker may update this policy from time to time by publishing a new version on thecampaignmaker.com.
                    </p>
                    <p>
                        8.2 You should check this page occasionally to ensure you are happy with any changes to this policy.
                    </p>
                    <p>
                        <strong>9. Your rights</strong>
                    </p>
                    <p>
                        9.1 The Campaign Maker may withhold personal information that you request to the extent permitted by law.
                    </p>
                    <p>
                        9.2 You may instruct us at any time not to process your personal information for marketing purposes.
                    </p>
                    <p>
                        9.3 In practice, you will usually either expressly agree in advance to our use of your personal information for marketing purposes, or The Campaign Maker
                        will provide you with an opportunity to opt out of the use of your personal information for marketing purposes.
                    </p>
                    <p>
                        <strong>10. Third party websites</strong>
                    </p>
                    <p>
                        10.1 Thecampaignmaker.com includes hyperlinks to, and details of, third party websites.
                    </p>
                    <p>
                        10.2 The Campaign Maker has no control over, and is not responsible for, the privacy policies and practices of third parties.
                    </p>
                    <p>
                        <strong>11. Updating information</strong>
                    </p>
                    <p>
                        11.1 Please let us know if the personal information that The Campaign Maker hold about you needs to be corrected or updated.
                    </p>
                    <p>
                        <strong>12. Cookies</strong>
                    </p>
                    <p>
                        12.1 Thecampaignmaker.com uses cookies.
                    </p>
                    <p>
                        12.2 A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the
                        browser. The identifier is then sent back to the server each time the browser requests a page from the server.
                    </p>
                    <p>
                        12.3 Cookies may be either "persistent" cookies or "session" cookies: a persistent cookie will be stored by a web browser and will remain valid until its
                        set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when
                        the web browser is closed.
                    </p>
                    <p>
                        12.4 Cookies do not typically contain any information that personally identifies a user, but personal information that The Campaign Maker store about you
                        may be linked to the information stored in and obtained from cookies.
                    </p>
                    <p>
                        12.5 The Campaign Maker use both retargeting and marketing on thecampaignmaker.com.
                    </p>
                    <p>
                        12.6 The names of the cookies that The Campaign Maker use on thecampaignmaker.com, and the purposes for which they are used, are set out below:
                    </p>
                    <p>
                        (a) The Campaign Maker use both retargeting and marketing on thecampaignmaker.com for Adwords retarget / Facebook retarget / Conversion tracking / Adroll
                        retarget;
                    </p>
                    <p>
                        12.7 Most browsers allow you to refuse to accept cookies; for example:
                    </p>
                    <p>
                        (a) in Internet Explorer (version 10) you can block cookies using the cookie handling override settings available by clicking "Tools", "Internet Options",
                        "Privacy" and then "Advanced";
                    </p>
                    <p>
                        (b) in Firefox (version 24) you can block all cookies by clicking "Tools", "Options", "Privacy", selecting "Use custom settings for history" from the
                        drop-down menu, and unticking "Accept cookies from sites"; and
                    </p>
                    <p>
                        (c) in Chrome (version 29), you can block all cookies by accessing the "Customise and control" menu, and clicking "Settings", "Show advanced settings" and
                        "Content settings", and then selecting "Block sites from setting any data" under the "Cookies" heading.
                    </p>
                    <p>
                        12.8 Blocking all cookies will have a negative impact upon the usability of many websites.
                    </p>
                    <p>
                        12.9 If you block cookies, you will not be able to use all the features on thecampaignmaker.com.
                    </p>
                    <p>
                        12.10 You can delete cookies already stored on your computer; for example:
                    </p>
                    <p>
                        (a) in Internet Explorer (version 10), you must manually delete cookie files (you can find instructions for doing so at
                        http://support.microsoft.com/kb/278835);
                    </p>
                    <p>
                        (b) in Firefox (version 24), you can delete cookies by clicking "Tools", "Options" and "Privacy", then selecting "Use custom settings for history",
                        clicking "Show Cookies", and then clicking "Remove All Cookies"; and
                    </p>
                    <p>
                        (c) in Chrome (version 29), you can delete all cookies by accessing the "Customise and control" menu, and clicking "Settings", "Show advanced settings" and
                        "Clear browsing data", and then selecting "Delete cookies and other site and plug-in data" before clicking "Clear browsing data".
                    </p>
                    <p>
                        12.11 Deleting cookies will have a negative impact on the usability of many websites.
                    </p>
                    <p>
                        <strong>13. Our details</strong>
                    </p>
                    <p>
                        13.1 This website is owned and operated by FUBSZ LLC.
                    </p>
                    <p>
                        13.2 The Campaign Maker is registered in Florida, USA and our registered office is at 1175 Highway A1a, Satellite Beach, FL.
                    </p>
                    <p>
                        13.3 Our principal place of business is at 1175 Highway A1a #603, Satellite Beach, FL.
                    </p>
                    <p>
                        13.4 You can contact us by writing to the business address given above, by using thecampaignmaker.com contact form, by email to	<a href="mailto:samy@thecampaignmaker.com">samy@thecampaignmaker.com</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>