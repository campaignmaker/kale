<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/57acdf3f1aad34d80cf3d3d2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<?php $session_data = $this->session->userdata('logged_in'); 
?>
<?php $re = $this->session->userdata['logged_in']['id'].'_user'; ?>
<div class="page-container">
    <?php 
    if ($this->session->userdata('logged_in')) {
    $this->load->view('menu'); 
    }
    ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 col-sm-12 ads-box">
                    <h3 class="page-title">Pick your plan</h3>
                    <hr>
                    <?php if (($this->session->flashdata('subcriptionExpire'))) : ?>
                        <div class="margin20 alert alert-danger">
                            <button class="close" data-close="alert"></button>
                            <span>
                                <?= $this->session->flashdata('subcriptionExpire') ?> </span>
                        </div>

                    <?php endif; ?>
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <ul class="nav nav-pills">

                            <li class="active">
                                <a href="<?= site_url('pricing'); ?>">Monthly </a>
                            </li>
                            <li >
                                <a href="<?= site_url('pricing/yearly'); ?>"><span class="yeraly">Yearly</span>
                                    (2 months free!)</a>
                            </li>
                        </ul>
                    </div>
                    <!--Facebook Pages-->
                    <div class="portlet box blue-hoki camp-name">

                        <div class="portlet-title">

                            <div class="caption"> <i class="fa fa-bullhorn"></i> All Plans Include These Awesome Features: </div>

                            <div class="tools"> <a href="javascript:;" class="collapse"> </a> </div>

                        </div>

                        <div class="portlet-body form">

                            <div class="form-horizontal form-row-sepe">

                                <div class="form-body">

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="info-box-price darkgreen"> <span class="info-box-price-icon"><img src="<?php echo $this->config->item('assets'); ?>1-a.png"></span>

                                            <div class="info-box-content-price"> <span class="info-box-number-price">Conversion Tracking</span> </div>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="info-box-price darkblue"> <span class="info-box-price-icon"><img src="<?php echo $this->config->item('assets'); ?>2-a.png"></span>

                                            <div class="info-box-content-price"> <span class="info-box-number-price">Powerful Interest Search Engine</span> </div>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="info-box-price darkred"> <span class="info-box-price-icon"><img src="<?php echo $this->config->item('assets'); ?>3-a.png"></span>

                                            <div class="info-box-content-price"> <span class="info-box-number-price">Epic Adcopy Split Testing</span> </div>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="info-box-price darkyellow"> <span class="info-box-price-icon"><img src="<?php echo $this->config->item('assets'); ?>4-a.png"></span>

                                            <div class="info-box-content-price"> <span class="info-box-number-price">Automated Targeting Split Test </span> </div>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="info-box-price darkgreen"> <span class="info-box-price-icon"><img src="<?php echo $this->config->item('assets'); ?>8-a.png"></span>

                                            <div class="info-box-content-price"> <span class="info-box-number-price">Advanced Targeting For Experts</span> </div>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="info-box-price darkblue"> <span class="info-box-price-icon"><img src="<?php echo $this->config->item('assets'); ?>7-a.png"></span>

                                            <div class="info-box-content-price"> <span class="info-box-number-price">Google Analytics Integration</span> </div>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="info-box-price darkred"> <span class="info-box-price-icon"><img src="<?php echo $this->config->item('assets'); ?>6-a.png"></span>

                                            <div class="info-box-content-price"> <span class="info-box-number-price">Simple & Extensive Reporting</span> </div>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="info-box-price darkyellow"> <span class="info-box-price-icon"><img src="<?php echo $this->config->item('assets'); ?>5-a.png"></span>

                                            <div class="info-box-content-price"> <span class="info-box-number-price">Epic Campaign Maker Tools</span> </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="clearfix"></div>

                            </div>

                        </div>

                    </div>

                    <!--URLs-->

                    <!--The Adcopy -->
                    <div class="tab-content">
                        <div class="portlet box blue-hoki choose-camp-sec tab-pane fade active in" id="tab_1">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Pricing Tables
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row margin-bottom-40">
                                    <!-- Pricing -->
                                    <div class="col-md-3">
                                        <div class="pricing hover-effect">
                                            <div class="pricing-head">
                                                <h3>Noob Campaigner <span>
                                                </h3>
                                                <h4><i>$</i>19
                                                    <span>
                                                        Per Month </span>
                                                </h4>
                                            </div><div class="pricing-footer">
                                                <p>
                                                    Ideal for getting started with Facebook Ads on a budget.
                                                </p>
                                                <p class="pricing-rate">
                                                    Adspend up to <span class="pricing-rate1">$1,000</span> per month.
                                                </p>
                                            </div>
                                            <ul class="pricing-content list-unstyled">
                                                <li>
                                                    <i class="fa fa-tags"></i> Unlimited Facebook Ad Accounts 
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Unlimited Split Testing
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Epic Reporting Structure
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Extensive Conversion Tracking
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Easy To Use Platform
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Online Support in 48h
                                                </li>
                                            </ul>
                                            <div class="pricing-footer">
                                                <?php if(!empty($session_data)){ ?>
                                                <a href="https://www.jvzoo.com/b/0/225863/99?user_id=<?php echo $this->curl->AsciiToHex($re); ?>&packages_id=<?php echo $this->curl->AsciiToHex('8')?>" class="btn yellow-crusta">
                                                    Sign Up <i class="m-icon-swapright m-icon-white"></i>
                                                </a>
                                                <?php }else{ ?>
                                                    <a href="<?= site_url('login'); ?>" class="btn yellow-crusta">
                                                        Sign Up <i class="m-icon-swapright m-icon-white"></i>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="pricing hover-effect">
                                            <div class="pricing-head">
                                                <h3>Casual Campaigner <span>
                                                </h3>
                                                <h4><i>$</i>29
                                                    <span>
                                                        Per Month </span>
                                                </h4>
                                            </div>
                                            <div class="pricing-footer">
                                                <p>
                                                    Ideal for the casual marketer.
                                                </p>
                                                <p class="pricing-rate">
                                                    Adspend up to <span class="pricing-rate1">$3,000</span> per month.
                                                </p>
                                            </div>
                                            <ul class="pricing-content list-unstyled">
                                                <li>
                                                    <i class="fa fa-tags"></i> Unlimited Facebook Ad Accounts 
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Unlimited Split Testing
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Epic Reporting Structure
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Extensive Conversion Tracking
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Easy To Use Platform
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Online Support in 48h
                                                </li>
                                            </ul>
                                            <div class="pricing-footer">
                                                <?php if(!empty($session_data)){ ?>
                                                <a href="https://www.jvzoo.com/b/0/226006/99?user_id=<?php echo $this->curl->AsciiToHex($re); ?>&packages_id=<?php echo $this->curl->AsciiToHex('2')?>" class="btn yellow-crusta">
                                                    Sign Up <i class="m-icon-swapright m-icon-white"></i>
                                                </a>
                                                <?php }else{ ?>
                                                    <a href="<?= site_url('login'); ?>" class="btn yellow-crusta">
                                                        Sign Up <i class="m-icon-swapright m-icon-white"></i>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="pricing hover-effect">
                                            <div class="pricing-head">
                                                <h3>Advanced Campaigner 
                                                </h3>
                                                <h4><i>$</i>49
                                                    <span>
                                                        Per Month </span>
                                                </h4>
                                            </div>
                                            <div class="pricing-footer">
                                                <p>
                                                    Grow Plan as you increase your spending and need more power.
                                                </p>
                                                <p class="pricing-rate">
                                                    Adspend up to <span class="pricing-rate1">$10,000</span> per month. 
                                                </p>
                                            </div>
                                            <ul class="pricing-content list-unstyled">
                                                <li>
                                                    <i class="fa fa-tags"></i> Unlimited Facebook Ad Accounts
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Unlimited Split Testing 
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Epic Reporting Structure
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Extensive Conversion Tracking
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Easy To Use Platform
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Online Support in 24h
                                                </li>
                                            </ul>
                                            <div class="pricing-footer">
                                                <?php if(!empty($session_data)){ ?>
                                                <a href="https://www.jvzoo.com/b/0/226008/99?user_id=<?php echo $this->curl->AsciiToHex($re); ?>&packages_id=<?php echo $this->curl->AsciiToHex('3')?>" class="btn yellow-crusta">
                                                    Sign Up <i class="m-icon-swapright m-icon-white"></i>
                                                </a>
                                                <?php }else{ ?>
                                                    <a href="<?= site_url('login'); ?>" class="btn yellow-crusta">
                                                        Sign Up <i class="m-icon-swapright m-icon-white"></i>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="pricing hover-effect">
                                            <div class="pricing-head">
                                                <h3>Master Campaigner 
                                                </h3>
                                                <h4><i>$</i>99
                                                    <span>
                                                        Per Month </span>
                                                </h4>
                                            </div>
                                            <div class="pricing-footer">
                                                <p>
                                                    For the master campaigner! Campaign as much as you want with no limits!
                                                </p>
                                                <p class="pricing-rate">
                                                    Adspend up to <span class="pricing-rate1">$100,000</span> per month.
                                                </p>
                                            </div>
                                            <ul class="pricing-content list-unstyled">
                                                <li>
                                                    <i class="fa fa-tags"></i> Unlimited Facebook Ad Accounts
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Unlimited Split Testing
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Epic Reporting Structure
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Extensive Conversion Tracking
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Easy To Use Platform
                                                </li>
                                                <li>
                                                    <i class="fa fa-tags"></i> Online Support in 12h
                                                </li>
                                            </ul>
                                            <div class="pricing-footer">
                                                
                                                <?php if(!empty($session_data)){ ?>
                                                <a href="https://www.jvzoo.com/b/0/226010/99?user_id=<?php echo $this->curl->AsciiToHex($re); ?>&packages_id=<?php echo $this->curl->AsciiToHex('4')?>" class="btn yellow-crusta">
                                                    Sign Up <i class="m-icon-swapright m-icon-white"></i>
                                                </a>
                                                <?php }else{ ?>
                                                    <a href="<?= site_url('login'); ?>" class="btn yellow-crusta">
                                                        Sign Up <i class="m-icon-swapright m-icon-white"></i>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//End Pricing -->
                                </div>
                            </div>        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>