<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
<style>
    .quote-information .label {
        color: #9fa9ba;
        font-weight: 400;
        padding: 0px;
        text-align: left;
        font-size: inherit;
        line-height: inherit;
    }
    p.error {
        color: red;
        font-weight: bold;
    }
    #loading_report{
        display:none;
        width:100%;
        height:100%; 
        background:#fff; 
        opacity:0.5; 
        text-align:center;
        top:0px;
        left:0px;
        position:fixed;
        padding-top:15%;
        z-index: 9999;
    }
    .form-control {
        box-shadow: none;
    }
</style>
<main id="main_7">
    <div class="main-content s1">
        <div class="container">
            <div class="center-box s11">
                <div class="holder s2">
                    <h2 class="screen-heading text-center">COST PER CLICK QUOTE GUARANTEE</h2>
                    <div class="plan-holder s1 text-center">
                        <div class="plan-row d-flex flex-wrap justify-content-center">
                            <div class="plan d-flex flex-wrap">
                                <a href="#" class="link-box d-flex flex-wrap align-items-center">
                                    <div class="holder">
                                        <strong class="price">7 Days</strong>
                                        <span class="paid-title">Timeframe</span>
                                        <span class="plan-title">Only Active Days are Counted</span>
                                    </div>
                                </a>
                            </div>
                            <div data-amount="0.22" class="plan d-flex flex-wrap">
                                <a href="#" class="link-box d-flex flex-wrap align-items-center">
                                    <div class="holder">
                                        <strong class="price">$0.22</strong>
                                        <span class="paid-title">Cost Per Click</span>
                                        <span class="plan-title">Guaranteed Rate</span>
                                    </div>
                                </a>
                            </div>
                            <div data-amount="35" class="plan d-flex flex-wrap">
                                <a href="#" class="link-box d-flex flex-wrap align-items-center">
                                    <div class="holder">
                                        <strong class="price">$35</strong>
                                        <span class="paid-title">Daily Budget</span>
                                        <span class="plan-title">Req. Budget For Campaign</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="quote-information">
                        <h2 class="quote-heading text-center">Quote Information</h2>
                        <div class="item-holder d-flex flex-wrap">
                            <div class="quote-item d-flex flex-wrap">
                                <div class="item-wrap d-flex flex-wrap align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-badge-rounded.svg" alt="badge">
                                    </div>
                                    <div class="text">
                                        <span class="data">Samy Punani</span>
                                        <span class="label">Name</span>
                                    </div>
                                </div>
                            </div>
                            <div class="quote-item d-flex flex-wrap">
                                <div class="item-wrap d-flex flex-wrap align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-letter-rounded.svg" alt="letter">
                                    </div>
                                    <div class="text">
                                        <span class="data">Dummy.Email@gmail.com</span>
                                        <span class="label">Account Email</span>
                                    </div>
                                </div>
                            </div>
                            <div class="quote-item d-flex flex-wrap">
                                <div class="item-wrap d-flex flex-wrap align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-crown-rounded.svg" alt="crown">
                                    </div>
                                    <div class="text">
                                        <span class="data">212333997549</span>
                                        <span class="label">Ad Account ID</span>
                                    </div>
                                </div>
                            </div>
                            <div class="quote-item d-flex flex-wrap">
                                <div class="item-wrap d-flex flex-wrap align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-megaphone-rounded.svg" alt="megaphone">
                                    </div>
                                    <div class="text">
                                        <span class="data">9988747291</span>
                                        <span class="label">Original Campaign ID</span>
                                    </div>
                                </div>
                            </div>
                            <div class="quote-item d-flex flex-wrap">
                                <div class="item-wrap d-flex flex-wrap align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pencil-rounded.svg" alt="pencil">
                                    </div>
                                    <div class="text">
                                        <span class="data">March 5th 2018</span>
                                        <span class="label">Quote Created On</span>
                                    </div>
                                </div>
                            </div>
                            <div class="quote-item d-flex flex-wrap">
                                <div class="item-wrap d-flex flex-wrap align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-hourglass-rounded.svg" alt="hourglass">
                                    </div>
                                    <div class="text">
                                        <span class="data">March 7th 2018</span>
                                        <span class="label">Quote Valid Until</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="other-info">
                            <div class="info-box">
                                <div class="info-detail">
                                    <strong class="title">Information About User Campaign</strong>
                                    <p>We offer something someting, and its really cool. We are looking to expand our audience to another vertical. Our best ad did so and so.</p>
                                </div>
                            </div>
                            <div class="info-box">
                                <div class="info-detail">
                                    <strong class="title">Competitors Information</strong>
                                    <p>We offer something someting, and its really cool. We are looking to expand our audience to another vertical. Our best ad did so and so.</p>
                                </div>
                            </div>
                            <div class="info-box">
                                <div class="info-detail">
                                    <strong class="title">Special Restrictions Indicated By User</strong>
                                    <p>We offer something someting, and its really cool. We are looking to expand our audience to another vertical. Our best ad did so and so.</p>
                                </div>
                            </div>
                            <div class="info-box s1">
                                <div class="info-detail">
                                    <strong class="title">Terms of Guarantee</strong>
                                    <ul class="custom-list">
                                        <li>Timeframe days are counted only for days the campaign is 100% active and running.</li>
                                        <li>The campaign must run uninterrupted in any way during the timeframe mentioned above.</li>
                                        <li>The campaign budget must not be altered in any way during the timeframe mentioned above.</li>
                                        <li>The campaign/adsets/ads must not be edited in any way during the timeframe mentioned above.</li>
                                    </ul>
                                    <p>If every single ad within the campaign has a higher cost per click AND has exceeded the timeframe in this quote AND all terms mentioned above are met, then you will be entitled to a full refund of the service cost.</p>
                                    <div class="highlight-text">
                                        <p>Basically, let the campaign run without touching it for the days mentioned and if the CPC is higher in all ads than mentioned then you get a refund for the service.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="center-box s11">
                <div class="holder s2">
                    <h2 class="screen-heading s1 text-center">
                        Choose Your Service Type and Approve for Us to Get Started
                        <span class="sub-heading">You're in good hands..</span>
                    </h2>
                    <div class="plan-holder text-center">
                        <div class="plan-row d-flex flex-wrap">
                            <div data-amount="200" class="plan d-flex flex-wrap select_plan">
                                <div class="btn-radio-custom s4 d-flex flex-wrap">
                                    <input type="radio" name="service" id="campaign">
                                    <label class="d-flex flex-wrap align-items-center" for="campaign">
                                        <span class="radio-text-holder">
                                            <span class="plan-title">Campaign Setup</span>
                                            <strong class="price">$200</strong>
                                            <span class="paid-title">Excludes Retargeting</span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div data-amount="300" class="plan d-flex flex-wrap select_plan">
                                <div class="btn-radio-custom s4 d-flex flex-wrap">
                                    <input type="radio" checked="" name="service" id="retargeting">
                                    <label class="d-flex flex-wrap align-items-center" for="retargeting">
                                        <span class="radio-text-holder">
                                            <span class="plan-title">Campaign + Retargeting</span>
                                            <strong class="price">$300</strong>
                                            <span class="paid-title">Includes Retargeting</span>
                                            <span class="offer">Recommended for Ecommerce</span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="features">
                        <h2 class="feature-heading">What's Included</h2>
                        <div class="holder s1">
                            <div class="item-holder d-flex flex-wrap">
                                <div class="feature-item d-flex flex-wrap">
                                    <div class="item-wrap d-flex flex-wrap align-items-center">
                                        <div class="icon-holder">
                                            <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-crown-rounded.svg" alt="crown">
                                        </div>
                                        <div class="text">
                                            <span>Expert Campaign Setup</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="feature-item d-flex flex-wrap">
                                    <div class="item-wrap d-flex flex-wrap align-items-center">
                                        <div class="icon-holder">
                                            <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-megaphone-rounded.svg" alt="megaphone">
                                        </div>
                                        <div class="text">
                                            <span>Saved Inside Your Account</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="feature-item d-flex flex-wrap">
                                    <div class="item-wrap d-flex flex-wrap align-items-center">
                                        <div class="icon-holder">
                                            <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-heart-rounded.svg" alt="heart">
                                        </div>
                                        <div class="text">
                                            <span>Premium Support</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="feature-item d-flex flex-wrap">
                                    <div class="item-wrap d-flex flex-wrap align-items-center">
                                        <div class="icon-holder">
                                            <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-gift-rounded.svg" alt="gift">
                                        </div>
                                        <div class="text">
                                            <span>Facebook Policy Safe</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="feature-item d-flex flex-wrap">
                                    <div class="item-wrap d-flex flex-wrap align-items-center">
                                        <div class="icon-holder">
                                            <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-dimond-rounded.svg" alt="dimond">
                                        </div>
                                        <div class="text">
                                            <span>Backed by Guarantee</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="feature-item d-flex flex-wrap">
                                    <div class="item-wrap d-flex flex-wrap align-items-center">
                                        <div class="icon-holder">
                                            <img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-trophy-rounded.svg" alt="trophy">
                                        </div>
                                        <div class="text">
                                            <span>Delivery Within 3 Biz Days</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
             
       <div class="cell example example4">
            <form id="payment-form">
              <div id="example4-paymentRequest">
                <!--Stripe paymentRequestButton Element inserted here-->
              </div>
			   
              <input id="example4-name" data-tid="elements_examples.form.name_placeholder" type="hidden" value="<?php echo $this->session->userdata['logged_in']['first_name']." ".$this->session->userdata['logged_in']['last_name']; ?>">
          
          </fieldset>
              <fieldset>
			    <input name="amount" id="amount_charged" value="300" type="hidden">
                <legend class="card-only" data-tid="elements_examples.form.pay_with_card">Pay with card</legend>
                <legend class="payment-request-available" data-tid="elements_examples.form.enter_card_manually">Or enter card details</legend>
                <div class="container">
                  <div id="example4-card"></div>
                  <button type="submit" data-tid="elements_examples.form.donate_button">Pay $<span id="paymentfld">300</span></button>
                </div>
              </fieldset>
              <div class="error" role="alert"><svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
                  <path class="base" fill="#000" d="M8.5,17 C3.80557963,17 0,13.1944204 0,8.5 C0,3.80557963 3.80557963,0 8.5,0 C13.1944204,0 17,3.80557963 17,8.5 C17,13.1944204 13.1944204,17 8.5,17 Z"/>
                  <path class="glyph" fill="#FFF" d="M8.5,7.29791847 L6.12604076,4.92395924 C5.79409512,4.59201359 5.25590488,4.59201359 4.92395924,4.92395924 C4.59201359,5.25590488 4.59201359,5.79409512 4.92395924,6.12604076 L7.29791847,8.5 L4.92395924,10.8739592 C4.59201359,11.2059049 4.59201359,11.7440951 4.92395924,12.0760408 C5.25590488,12.4079864 5.79409512,12.4079864 6.12604076,12.0760408 L8.5,9.70208153 L10.8739592,12.0760408 C11.2059049,12.4079864 11.7440951,12.4079864 12.0760408,12.0760408 C12.4079864,11.7440951 12.4079864,11.2059049 12.0760408,10.8739592 L9.70208153,8.5 L12.0760408,6.12604076 C12.4079864,5.79409512 12.4079864,5.25590488 12.0760408,4.92395924 C11.7440951,4.59201359 11.2059049,4.59201359 10.8739592,4.92395924 L8.5,7.29791847 L8.5,7.29791847 Z"/>
                </svg>
                <span class="message"></span></div>
				 
            </form>
			  <div class="success">
              <div class="icon">
                <svg width="84px" height="84px" viewBox="0 0 84 84" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
                  <circle class="border" cx="42" cy="42" r="40" stroke-linecap="round" stroke-width="4" stroke="#000" fill="none"/>
                  <path class="checkmark" stroke-linecap="round" stroke-linejoin="round" d="M23.375 42.5488281 36.8840688 56.0578969 64.891932 28.0500338" stroke-width="4" stroke="#000" fill="none"/>
                </svg>
              </div>
              <h3 class="title" data-tid="elements_examples.success.title">Payment successful</h3>
              <p class="message"><span data-tid="elements_examples.success.message"> </span><span class="token" ></span><br>We will start working on your new campaign ASAP!</p>
            
            </div>
          <!--div class="info text-center">
                            <p>Your credit card will be charged $<amount>300</amount></p>
                        </div-->

           
          </div>
        
                </div>
            </div>
        </div>
    </div>
</main>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="<?php echo $this->config->item('assets'); ?>stripe/js/index.js" data-rel-js=""></script>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets'); ?>stripe/css/base.css" data-rel-css="">
  <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('assets'); ?>stripe/css/example4.css" data-rel-css="">
<script>
var payamount =  300;
$(document).ready(function(){
       $(document).on('click', '.select_plan', function(){
           var amount = $(this).attr('data-amount');
           $('#paymentfld').html(amount);
           $('#amount_charged').val(amount);
		   
}); 
}); 
(function() {
  "use strict";

  var elements = stripe.elements({
    fonts: [
      {
        cssSrc: "https://rsms.me/inter/inter-ui.css"
      }
    ],
    locale: window.__exampleLocale
  });

  var card = elements.create("card", {
    style: {
      base: {
        color: "#32325D",
        fontWeight: 500,
        fontFamily: "Inter UI, Open Sans, Segoe UI, sans-serif",
        fontSize: "16px",
        fontSmoothing: "antialiased",

        "::placeholder": {
          color: "#CFD7DF"
        }
      },
      invalid: {
        color: "#E25950"
      }
    }
  });

  card.mount("#example4-card");

  var paymentRequest = stripe.paymentRequest({
    country: "US",
    currency: "usd",
    total: {
      amount: payamount,
      label: "Total"
    }
  });
  paymentRequest.on("token", function(result) {
    var example = document.querySelector(".example4");
    example.querySelector(".token").innerText = result.token.id;
    example.classList.add("submitted");
   result.complete("success");
  });

  var paymentRequestElement = elements.create("paymentRequestButton", {
    paymentRequest: paymentRequest,
    style: {
      paymentRequestButton: {
        type: "donate"
      }
    }
  });

  paymentRequest.canMakePayment().then(function(result) {
    if (result) {
      document.querySelector(".example4 .card-only").style.display = "none";
      document.querySelector(
        ".example4 .payment-request-available"
      ).style.display =
        "block";
      paymentRequestElement.mount("#example4-paymentRequest");
    }
  });

  registerElements([card, paymentRequestElement], "example4");
})();
     
</script>