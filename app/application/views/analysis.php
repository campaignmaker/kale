<style>
#main_6 #heading_report {
    position: relative;
    width: 100%;
    z-index: -1;
	margin-bottom: 21px;
	margin-top: -20px;
	display: none;
}
.table-report thead th:first-child {
	color: #0294ff;
}
.table-report thead th {
	color: #4a4a4a;
	border-top: 1px solid #e6eaee;
	border-bottom: 1px solid #e6eaee;
	border-left: 1px solid #e6eaee;
	border-right: 1px solid #e6eaee;
}
 .table-report .highlight-data > td{
	 color: #4a4a4a;
	border-bottom: 1px solid #e6eaee;
	border-left: 1px solid #e6eaee;
	border-right: 1px solid #e6eaee;
 }
 #myImg{ max-height: 160px;max-width: 500px;}
.analysis-report .specified{ font-family: Montserrat, Arial, Helvetica, "Helvetica Neue", sans-serif; }
#loading_report{display:none;width:100%; height:100%; background:#fff; opacity:0.5; text-align:center;top:0px;left:0px;position:fixed;padding-top:15%;z-index: 9999;
}
</style>
<link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>newdesign/css/report.css">
<div id="loading_report">
<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/loading.gif" alt="Loading..">
</div> 
<input type="hidden" id="current_curr" value="<?php echo $cur_currency ?>" />
	<div  id="analysing" style="display:none;">
         <div id="statusMain"><i class="fa fa-spinner fa-spin" style="font-size:48px;color:white"> </i></div>
    </div>
    <p id="bypassme"></p>
	<?php if (isset($adaccountsCount) && $adaccountsCount > 1) { ?>
        	<main id="main" class="analyz_add_account">
				<div class="main-content">
					<div class="container">
						<div class="center-box s6">
							<div class="holder s1">
								<h2 class="screen-heading text-center">Which ad account would you like to analyze?</h2>
								<div class="ad-account">
									<div class="table-holder">
										<table class="table-s4">
										    
										     <?php
										     
                        foreach ($adaccounts as $keytemp => $adaccount) {
                            
                            if($keytemp ==0){
                                $checked = "checked";
                            }else{
                                $checked = "";
                            }
                        ?>
                             
										    
											<tr>
												<td>
														<input id="ad-account-<?php echo $adaccount->ad_account_id; ?>" type="radio" name="ad-account" <?php echo $checked;  ?> value="<?php echo $adaccount->ad_account_id; ?>" >
														<label for="ad-account-<?php echo $adaccount->ad_account_id; ?>"></label>
												</td>
												<td><?php echo $adaccount->add_title; ?></td>
												<td><?php echo $adaccount->ad_account_id; ?></td>
											</tr>
											
							 <?php
                        }
                    ?>				
					</table>
									</div>
									<div class="btn-holder text-center">
										<a id="use_add_account" class="btn btn-primary" href="javascript:void(null)">Use Ad Account</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			
			
			<main id="main_5" style="display:none">
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Starting The Analysis Tool 
										<span class="sub-heading s1">
											PLEASE DO NOT CLOSE THIS PAGE OR CLICK THE BACK BUTTON
										</span>
									</h2>
								
									
									<div  class="loading" style="margin:auto;width:400px">
										   <div id="lottie" style="width:400px;height:300px"></div>
										</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</main>
        
        
    <?php }else{ ?>
	
	<!-- Step 1 Start Here  -->
	<main id="main_1" >
				<div class="step-bar s1">
					<div class="holder d-flex flex-wrap">
						<div class="step active">
							<strong class="step-name">Level</strong>
						</div>
						<div class="step">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step">
							<strong class="step-name">Focus</strong>
						</div>
						<div class="step">
							<strong class="step-name">Measure</strong>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										What Level Do You Want To Analyze?
										<span class="sub-heading">
											Analyze either campaign, adset or ad levels.
											<strong class="highlight-text">Pro tip:  Adset level gives you the most accurate data to improve.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										<form action="#" class="form">
											<div class="big-btn-holder">
												<div class="btn-row d-flex flex-wrap justify-content-center">
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom s2 d-flex flex-wrap">
															<input type="radio" name="analyze" id="overview" value="campaign">
															<label for="overview" class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-globe-rounded.svg" alt="globe">
																</span>
																<span class="text">
																	<span class="data">Campaign Level</span>
																	<span class="label">Overview</span>
																</span>
															</label>
														</div>
													</div>
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom s2 d-flex flex-wrap">
															<input type="radio" checked="" name="analyze"  id="recommended" value="adset">
															<label for="recommended"  class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-statistic-rounded.svg" alt="statistic">
																</span>
																<span class="text">
																	<span class="data" >Adset Level</span>
																	<span class="label">Recommended</span>
																</span>
															</label>
														</div>
													</div>
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom s2 d-flex flex-wrap">
															<input type="radio" name="analyze" id="detailed" value="ad">
															<label for="detailed"  class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-molecula-rounded.svg" alt="molecula">
																</span>
																<span class="text">
																	<span class="data">Ad Level</span>
																	<span class="label">Detailed</span>
																</span>
															</label>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										 <a href="javascript:void(null);" onclick="gonext('1')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main> 
    <!-- Step 1 End Here  -->
    
    <!-- Step 2 Start Here  -->
    	<main id="main_2" style="display:none">
				<div class="step-bar s1">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name done">Level</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step">
							<strong class="step-name">Focus</strong>
						</div>
						<div class="step">
							<strong class="step-name">Measure</strong>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										What Time Frame Would You Like To Analyze?
										<span class="sub-heading">
											Specify the time frame you want the anlaysis to be based on.
											<strong class="highlight-text">Pro tip:  analyzing by lifetime gives you the largest group of data to review.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
										<form action="#" class="form">
											<div class="form-group">
												<div class="select-holder s2">
													<select id="timeframe" name="autoopti[timeframe]" >
													 
														<option value="today">Today</option>
														  <option value="yesterday">Yesterday</option>
														  <option value="last_3d">Last 3 Days</option>
														  <option value="last_7d">Last 7 Days</option>
														  <option value="last_14d">Last 14 Days</option>
														  <option value="last_28d">Last 28 Days</option>
														  <option value="last_30d">Last 30 Days</option>
														  <option value="last_90d">Last 90 Days</option>
														  <option value="this_month">This Month</option> 
														  <option value="last_month">Last Month</option>
														  <option value="this_quarter">This Quarter</option>                      
														  <option value="this_year">This Year</option> 
														  <option value="last_year">Last Year</option>
														  <option value="lifetime" selected="">Lifetime</option>
                                                        
													</select>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="goprev('2')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
									 <a href="javascript:void(null);" onclick="gonext('2')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
    <!-- Step 2 End Here  -->
 
     <!-- Step 3 Start Here  -->
     
       <div id="main_3" style="display:none">
       
     	<main id="main_3_ad" style="display:none">
				<div class="step-bar s1">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name done">Level</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Focus</strong>
						</div>
						<div class="step">
							<strong class="step-name">Measure</strong>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose The <span class="highlight-text">Ad</span> You Want To Analyze
										<span class="sub-heading">
											Choose the main Ad you want to use for the report.
											<strong class="highlight-text">Pro tip:  you can click the + button to compare.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										<form action="#" class="form">
											<div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col select-wrap">
													<div class="form-group mb-0" id="adgroup">
													    
														<div class="select-holder s2">
															<select class="ad" name="autoopti[ad][]">
																<option value="">Go Back to Ad</option>
																
															</select>
															<a href="javascript:void(null);" class="btn btn-compare" id="add-more-ads">
														<span class="icon-holder icon-plus"></span>
														Compare
													</a>
														</div>
														
													</div>
												</div>
											
											</div>
										</form>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="goprev('3')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
									<a href="javascript:void(null);" onclick="gonext('3')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		 <main id="main_3_adset" style="display:none">
				<div class="step-bar s1">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name done">Level</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Focus</strong>
						</div>
						<div class="step">
							<strong class="step-name">Measure</strong>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose The <span class="highlight-text">Adset</span> You Want To Analyze
										<span class="sub-heading">
											Choose the main Adset you want to use for the report.
											<strong class="highlight-text">Pro tip:  you can click the + button to compare.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										<form action="#" class="form">
											<div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col select-wrap">
													<div class="form-group mb-0" id="adsetgroup">
														<div class="select-holder s2">
															<select class="adset" name="autoopti[adset][]" onchange="getCampaignAd(this.value)">
															     <option value="">Go Back to Campaign</option>
												
															</select>
																<a href="javascript:void(null);" class="btn btn-compare" id="ad-more-adset">
														<span class="icon-holder icon-plus" ></span>
														Compare
													</a>
															
														</div>
													</div>
												</div>
												
											</div>
										</form>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="goprev('3')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="gonext('3')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		<main id="main_3_campaign"  >
				<div class="step-bar s1">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Level</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Focus</strong>
						</div>
						<div class="step">
							<strong class="step-name">Measure</strong>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose The <span class="highlight-text">Campaign</span> You Want To Analyze
										<span class="sub-heading">
											Choose the main campaign you want to use for the report.
											<strong class="highlight-text">Pro tip:  you can click the + button to compare.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										<form action="#" class="form">
											<div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col select-wrap">
													<div class="form-group mb-0" id="compaignGroup">
														<div class="select-holder s2">
														   
														   
														  <select class="form-control campaignName"  name="autoopti[campaignsid][]" required="required" onchange="getCampaignAsdsets(this.value)">
                                                         <option value="0">Select Campaign</option>
                                                         
                                                         <?php /* ?>
                                                          <?php  if (isset($adaccounts['campaigns'])){ 
                                                           
                                                          foreach ($adaccounts['campaigns'] as $campaign){  ?>
                                                            <option value="<?php echo $campaign['campaign_id']."|||||".$campaign['campaign_objective']; ?>"><?php echo $campaign['campaign_name']; ?></option>  
                                                          <?php  } } ?> 
                                                          
                                                          <?php */ ?>
                                                          
                                                          
                                                           <?php  if (isset($adaccounts['campaigns'])){ 
                                                           
                                                          foreach ($adaccounts['campaigns'] as $campaign){  ?>
                                                            <option value="<?php echo $campaign['campaign_id']; ?>"><?php echo $campaign['campaign_name']; ?></option>  
                                                            
                                                          <?php  } } ?> 
                                                          
                                                          
                                                       </select>
														
														<a href="javascript:void(null);" class="btn btn-compare custom-compare-button" id="ad-more-compaign">
														<span class="icon-holder icon-plus" ></span>
														Compare
													   </a>
															
															
														</div>
													</div
													
													
													 
												</div>
												<div class="flex-col btn-wrap">
												      
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
									<a href="javascript:void(null);" onclick="goprev('3')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="gonext('3')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			
	   </div>		
     <!-- Step 3 End Here  -->
     
     <!-- Step 4 Start Here  -->
     <main id="main_4" style="display:none">
				<div class="step-bar s1">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name done">Level</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Focus</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Measure</strong>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										What Data Points Do You Want To Measure?
										<span class="sub-heading">
											Choose up to 5 data points to analyze in your report.
											<strong class="highlight-text">Pro tip:  focus on data points such as CTR, total spent and engagement that show how your money is spent.</strong>
										</span>
									</h2>
									<div class="form-wrap s3">
										<form action="#" class="form">
											<div class="form-element-row d-flex flex-wrap justify-content-center align-items-center">
												<div class="flex-col select-wrap">
													<div class="form-group mb-0" id="datapointgroup">
														<div class="select-holder s2">
															<select  name="autoopti[datapoint][]" class="datapoint">
															    <option  value="" >Choose a data point</option>
															    <option value="link_click">Clicks to Website</option>
                                                                <option value="offsite_conversion.fb_pixel_lead">Leads (FB pixel)</option>
                                                        		<option value="post_engagement">Post Engagement</option>
                                                        		<option value="like">Page Likes</option>
                                                        		<option value="offsite_conversion.add_to_cart">Adds to Cart (FB pixel)</option>
                                                        		<option value="offsite_conversion.checkout">Checkouts (FB pixel)</option>
                                                        		<option value="offsite_conversion.fb_pixel_add_payment_info">Adds Payment Info (FB pixel)</option>
                                                        		<option value="offsite_conversion.fb_pixel_add_to_cart">Adds To Cart (FB pixel)</option>
                                                        		<option value="offsite_conversion.fb_pixel_add_to_wishlist">Adds To Wishlist (FB pixel)</option>
                                                        		<option value="offsite_conversion.fb_pixel_complete_registration">Completed Registration (FB pixel)</option>
                                                        		<option value="offsite_conversion.fb_pixel_initiate_checkout">Initiates Checkout (FB pixel)</option>
                                                        		<option value="offsite_conversion.fb_pixel_purchase">Purchases (FB pixel)</option>
                                                        		<option value="offsite_conversion.fb_pixel_search">Search (FB pixel)</option>
                                                        		<option value="offsite_conversion.fb_pixel_view_content">Views Content (FB pixel)</option>
                                                        		<option value="offsite_conversion.key_page_view">Key Page Views (FB pixel)</option>
                                                        		<option value="onsite_conversion.messaging_first_reply">New Messaging Conversations</option>
                                                        		<option value="app_install">App Installs</option>
                                                        		<option value="leadgen.other">Leads (Form)</option>
                                                        		<option value="video_10_sec_watched_actions">10-Second Video Views</option>
                                                        		<option value="video_p25_watched_actions">25% Video Watched</option>
                                                        		<option value="video_view">3-Second Video Views</option>
                                                        		<option value="mobile_app_install">Mobile App Installs</option>

															</select>
															
																<a href="javascript:void(null);" class="btn btn-add custom-more-datapoint" id="ad-more-datapoint">
														<span class="icon-holder icon-plus" ></span>
													</a>
															
															
														</div>
													</div>
												
												</div>
												<div class="flex-col btn-wrap">
													
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="btn-holder d-flex flex-wrap justify-content-center">
									<a href="javascript:void(null);" onclick="startAnalysis()" class="btn btn-start-analyzing">
										<span class="icon-holder">
											<img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-eraser.svg" alt="eraser">
										</span>
										Start Analyzing!
									</a>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
									<a href="javascript:void(null);" onclick="goprev('4')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
     <!-- Step 4 End Here  -->
     
      <!-- Step 5 Start Here  -->
       	<main id="main_5" style="display:none">
			 <!--	<div class="step-bar s1">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Level</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Focus</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Measure</strong>
						</div>
					</div> 
				</div> -->
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Generating Your Analysis Report
										<span class="sub-heading s1">
											PLEASE DO NOT CLOSE THIS PAGE OR CLICK THE BACK BUTTON
										</span>
									</h2>
								
									
									<div  class="loading" style="margin:auto;width:400px">
										   <div id="lottie" style="width:400px;height:300px"></div>
										</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</main>
     <!-- Step 5 End Here  -->
     
     
      <!-- Step 6 Start Here  -->
    	<main id="main_6" style="display:none">
				<div class="page-heading">
					<div class="holder d-md-flex flex-wrap justify-content-between align-items-center">
						<h1>Analysis Report</h1>
						<div class="btn-holder flex-wrap d-flex">
						
							<!--div class="btn-col">
								<input type="file" id="uploadlogo" class="inputfile"  onchange="previewFile()"/>
								<label for="uploadlogo"><span class="icon-upload"></span><span>Upload Logo</span></label>
								

								</a>
						</div--> 
						<div class="btn-col">
								
								<?php if($sub_id=='316'){ ?><a href="JavaScript:void(null)" id="download_report" class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><?php }else{ ?>
								<a href="JavaScript:void(null)" id="download_report" class="btn btn-primary" onclick="generatepdf()" >
								<?php } ?>
									<span class="icon-holder">
									
										<img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-save.svg" alt="save">
									</span>
									DOWNLOAD REPORT
								</a>
								
							</div> 
							<div class="btn-col">
								<a href="<?php echo base_url().'analysis/'.$addAccountId; ?>" class="btn btn-primary" id="start_over">
									<span class="icon-holder">
										<img src="<?php echo $this->config->item('assets'); ?>analyz/images/icon-pencil.svg" alt="pencil">
									</span>
									START OVER
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="main-content s1">
				
					<div class="analysis-report" id="analysis-report" style="background:#eff3f6;">
					<div class="page-heading" id="heading_report">
					<div class="holder d-md-flex flex-wrap justify-content-between align-items-center">
						<h1>Analysis Report</h1>
					<div class="logo-holder">
					<a class="logo"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/logo.svg" alt="Choose Ad Account" id="myImg"></a>
					</div>
					<h2>Analyzed on <?php echo date("Y/m/d"); ?></h2>	
					</div>
					</div>
				
				
						<div class="report-row" >
							<header class="header-section text-center">
								<strong class="title">First let’s see how much was spent. </strong>
							</header>
							<div class="table-holder" >
								<table class="table-report" id="spendTable">
								
								</table>
							</div>
						</div>
						<div class="report-row" >
							<header class="header-section text-center">
								<strong class="title">Next let’s see how many people saw it in the time frame <span class="specified">specified.</span></strong>
							</header>
							<div class="table-holder">
								<table class="table-report" id="timeframeReport" >
								
								</table>
							</div>
						</div>
						<div class="report-row" >
							<header class="header-section text-center">
								<strong class="title">Now let’s compare the data you chose earlier.</strong>
							</header>
							
						<div id="compareTable">
						
						</div>	
						
							
						</div>
					</div>
					
					<div  id="analysis-report-pdf" style="display:none;background: rgb(255, 255, 255);padding: 18px 15px 0px;color: black;" class="analysis-report">
					    
					<div class="report-row" style="margin-bottom:0px;">
							<header class="header-section text-center">
								<strong class="title">First let’s see how much was spent.</strong>
							</header>
							<div class="table-holder" >
								<table class="table-report" id="spendTablepdf">
								
								</table>
							</div>
						</div>
							<div class="report-row" style="margin-bottom:0px;">
							<header class="header-section text-center">
								<strong class="title">Next let’s see how many people saw it in the time frame specified.</strong>
							</header>
							<div class="table-holder">
								<table class="table-report" id="timeframeReportpdf" >
								
								</table>
							</div>
						</div>
						<div class="report-row" style="margin-bottom:0px;">
							<header class="header-section text-center">
								<strong class="title">Now let’s compare the data you chose earlier.</strong>
							</header>
							<div class="table-holder">
						<div id="compareTablepdf">
						
						</div>	
						</div>	
							
						</div>
					</div>
				</div>
			</main>
     <!-- Step 6 End Here  -->
     

    <div id="preloaderMain" style="display: none;">
            <div id="statusMain"><i class="fa fa-spinner fa-spin"></i></div>
        </div>
 <?php } ?>   

<!-- <?php if (isset($adaccountsCount) && $adaccountsCount > 1) { ?>
        <div class="container-fluid" style="margin-top:20px;">
          <div class="detail-status-holder">
            <div class="table-holder responsive">
              <form action="#">
                <table class="table account-data-table">
                  <colgroup>
                    <col class="col5">
                    <col class="col6">
                    <col class="col7">
                    <col class="col8">
                    <col class="col9">
                    <col class="col10">
                    <col class="col11">
                    <col class="col12">
                    <col class="col13">
                    <col class="col14">
                  </colgroup>
                  <thead>
                    <tr>
                      <th><span class="sort-tag"><span>Ad Account Name</span></span></th>
                      <th><span class="sort-tag"><span>ACTIONS</span></span></th>
                      <th><span class="sort-tag"><span>Ad Account ID</span></span></th>
                      <th><span class="sort-tag"><span>Status</span></span></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                        foreach ($adaccounts as $adaccount) {
                        ?>
                            <tr> 
                                <td><?php echo $adaccount->add_title; ?></td>
                                <td><a href="<?php echo site_url('analysis/' . $adaccount->ad_account_id); ?>">See Analysis</a></td>
                                <td><?php echo $adaccount->ad_account_id; ?></td>
                                <td><?php
                                    if ($adaccount->status == 1) {
                                        echo "active";
                                    } else {
                                        echo "Not active";
                                    }
                                    ?></td>
                                
                            </tr>
                    <?php
                        }
                    ?>
                  </tbody>
                </table>
              </form>
            </div>
          </div>
        </div>
    <?php }else{ ?> -->
    
<?php /*  ?>

<!--Content-->

<div class="content" style="margin-top:20px;">
    	<!--Analysis-start-->

        <div class="container-fluid">
            <div class="analysis-start">
                <form>

                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-lg-4 campaign">
                            <div class="form-group form-group-lg">
                                <label for="campaignName">Choose a Campaign</label>
                                <select class="form-control" id="campaignName" name="autoopti[campaignsid]" required="required" onchange="getCampaignAsdsets(this.value)">
                                     <option value="0">Select Campaign</option>
                                      <?php  if (isset($adaccounts['campaigns'])){ 
                                       
                                      foreach ($adaccounts['campaigns'] as $campaign){  ?>
                                        <option value="<?php echo $campaign['campaign_id']."|||||".$campaign['campaign_objective']; ?>"><?php echo $campaign['campaign_name']; ?></option>  
                                      <?php  } } ?> 
                                </select>
                                
                                <!--<span class="glyphicon glyphicon-arrow-right"></span>-->

                                <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/arrow-icon.png" height="14"
                                                        width="40" class="hidden-sm hidden-xs"/>
                                <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/spinner.gif" height="67"
                                                        width="67" class="hidden-sm hidden-xs" style="display:none;" id="adsetspineer" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-4 adest">
                            <div class="form-group form-group-lg">
                                <label for="adestName">Choose an Adset</label>
                                <select class="form-control" id="adestName">
                                    <option value="0">Select Adset</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-4">
                            <button class="btn btn-primary" onclick="return startAnalysis()">Start Analysis</button>
                        </div>
                    </div>
                </form>
            </div>
            
            	<!--Analysing-->

            <div class="analysing" id="analysing" style="display:none;">
                <p>Analyzing Your Adset</p>
                <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/loading.gif" height="84" width="84"/>
            </div>
            
            <div class="analysing" id="analysing1" style="display:none;">
                <p>Creating An Optimized Version Of This Adset</p>
                <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/loading.gif" height="84" width="84"/>
            </div>

            <!--End Analysing-->
                
            <!--Analysis Done   -->

        <div class="analysis-done" id="analysis-done"  style="display:none;">
            <!--Head-->
            <div class="head-analysis table-responsive">
                <table class="table">
                    <thead>
                    <tr id="adsettotals">
                        <td><img src="../../assets/newdesign/images/total-result.png"
                                 style="margin-right: 2px"/>
                            <span class="text-primary">1,049</span>
                            <br class="hidden-lg">Total Results</td>
                        <td><img src="../../assets/newdesign/images/employee.png"/>
                            <span class="text-primary">$0.69</span>
                            <br class="hidden-lg">Cost Per Result</td>
                        <td><img src="../../assets/newdesign/images/percentage.png" style="margin-top: -5px"/>
                            <span class="text-primary">0.64%</span>
                            <br class="hidden-lg">Result Rate</td>
                        <td><img src="../../assets/newdesign/images/money-bag.png"/>
                            <span class="text-primary">$719.50</span>
                            <br class="hidden-lg">Total Spent</td>
                        <td><img src="../../assets/newdesign/images/best.png"/>
                            Best Performing</td>
                    </tr>
                    </thead>
                </table>
            </div>
            <!--End Head-->

            <!--AGE GROUP ANALYSIS-->
            <div class="heading">AGE GROUP ANALYSIS</div>
            <div class="table-responsive">
                <table class="table table-hover" id="analysistbl1" width="100%">
                    <thead>
                    <tr>
                        <th>
                            
                                AGE GROUP
                                <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                RESULTS <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                COST PER RESULT <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                RESULT RATE <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                           
                            TOTAL SPENT <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                           
                        </th>
                    </tr>
                    </thead>
                    <tbody id="agegrouptotals">
                    <tr>
                        <td><img src="../../assets/newdesign/images/n18.png" height="18" width="18"/>
                            18 - 24</td>
                        <td>89</td>
                        <td>$0.67</td>
                        <td class="text-success">0.45 %</td>
                        <td>$60.3</td>
                    </tr>
                    <tr>
                        <td><img src="../../assets/newdesign/images/n25.png" height="18" width="18"/>
                            25 - 34</td>
                        <td>200</td>
                        <td>$0.73</td>
                        <td class="text-success">0.47 %</td>
                        <td>$146.32</td>
                    </tr>
                    <tr>
                        <td><img src="../../assets/newdesign/images/n35.png" height="18" width="18"/>
                            35 - 44</td>
                        <td>248</td>
                        <td>$0.74</td>
                        <td class="text-success">0.56 %</td>
                        <td>$183.43</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../../assets/newdesign/images/n45.png" height="18" width="18"/>
                            45 - 54
                            <img src="../../assets/newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>
                        </td>
                        <td>275</td>
                        <td>$0.69</td>
                        <td class="text-success">0.80 %</td>
                        <td>$189.26</td>
                    </tr>
                    <tr>
                        <td><img src="../../assets/newdesign/images/n55.png" height="18" width="18"/>
                            55 - 65</td>
                        <td>237</td>
                        <td>$0.59</td>
                        <td class="text-success">1.13 %</td>
                        <td>$140.46</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!--End AGE GROUP ANALYSIS-->

            <!--GENDER ANALYSIS-->
            <div class="heading">GENDER ANALYSIS</div>
            <div class="table-responsive">
                <table class="table table-hover" id="analysistbl2" width="100%">
                    <thead>
                    <tr>
                        <th>
                            
                                GENDER
                                <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                RESULTS <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                COST PER RESULT <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                RESULT RATE <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                           
                            TOTAL SPENT <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                           
                        </th>
                    </tr>
                    </thead>
                    <tbody id="gendertotals">
                    <tr>
                        <td><img src="../../assets/newdesign/images/nMen.png" height="15" width="15"/>
                            Men</td>
                        <td>62</td>
                        <td>$0.93</td>
                        <td class="text-success">0.33 %</td>
                        <td>$57.87</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../../assets/newdesign/images/nWomen.png" height="15" width="15"/>
                            Women
                            <img src="../../assets/newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>
                        </td>
                        <td>983</td>
                        <td>$0.67</td>
                        <td class="text-success">0.69 %</td>
                        <td>$657.91</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!--End GENDER ANALYSIS-->

            <!--AD PLACEMENT ANALYSIS-->
            <div class="heading">AD PLACEMENT ANALYSIS</div>
            <div class="table-responsive">
                <table class="table table-hover" id="analysistbl3" width="100%">
                    <thead>
                    <tr>
                        <th>
                            
                                PLACEMENT
                                <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                RESULTS <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                COST PER RESULT <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                RESULT RATE <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                           
                            TOTAL SPENT <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                           
                        </th>
                    </tr>
                    </thead>
                    <tbody id="adplacementtotals">
                    <tr>
                        <td><img src="../../assets/newdesign/images/nDesktop.png" height="15" width="15"/>
                            Desktop Newsfeed</td>
                        <td>30</td>
                        <td>$0.87</td>
                        <td class="text-success">0 %</td>
                        <td>$25.98</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../../assets/newdesign/images/nMobile.png" height="15" width="15"/>
                            Mobile Newsfeed
                            <img src="../../assets/newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>
                        </td>
                        <td>1,026</td>
                        <td>$0.68</td>
                        <td class="text-success">0%</td>
                        <td>$25.98</td>
                    </tr>
                    <tr>
                        <td><img src="../../assets/newdesign/images/nInstagram.png" height="15" width="15"/>
                            Instagram Feed</td>
                        <td>0</td>
                        <td>$695.86</td>
                        <td class="text-success">0%</td>
                        <td>$0</td>
                    </tr>
                    <tr>
                        <td><img src="../../assets/newdesign/images/nUser.png" height="15" width="15"/>
                            Audience Network</td>
                        <td>0</td>
                        <td>$0</td>
                        <td class="text-success">0%</td>
                        <td>$0</td>
                    </tr>
                    <tr>
                        <td><img src="../../assets/newdesign/images/nComputer.png" height="15" width="15"/>
                            Desktop Right Side</td>
                        <td>0</td>
                        <td>$0</td>
                        <td class="text-success">0%</td>
                        <td>$0</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!--End AD PLACEMENT ANALYSIS-->

            <!--LOCATION ANALYSIS-->
            <div class="heading">LOCATION ANALYSIS</div>
            <div class="table-responsive">
                <table class="table table-hover" id="analysistbl4" width="100%">
                    <thead>
                    <tr>
                        <th>
                            
                                COUNTRY
                                <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                RESULTS <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                COST PER RESULT <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                            
                                RESULT RATE <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                            
                        </th>
                        <th>
                           
                            TOTAL SPENT <img src="<?php echo $this->config->item('assets');?>newdesign/images/arrow-up.png" height="12"
                                         width="6"/>
                           
                        </th>
                    </tr>
                    </thead>
                    <tbody id="countrytotals">
                    <tr>
                        <td>
                            <img src="../../assets/newdesign/images/nFolder.png" height="12" width="15"/>
                            USA
                        </td>
                        <td>10,021</td>
                        <td>$0.212</td>
                        <td class="text-success">1.21%</td>
                        <td>$100</td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../../assets/newdesign/images/nFolder.png" height="12" width="15"/>
                            UK
                            <img src="../../assets/newdesign/images/best.png" height="18" width="18" style="margin-left: 14px"/>
                        </td>
                        <td>11,211</td>
                        <td>$0.232</td>
                        <td class="text-success">1.21%</td>
                        <td>$100</td>
                    </tr>
                    <tr>
                        <td><img src="../../assets/newdesign/images/nFolder.png" height="12" width="15"/>
                            Canada</td>
                        <td>2,120</td>
                        <td>$0.321</td>
                        <td class="text-success">1.21%</td>
                        <td>$100</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!--End LOCATION ANALYSIS-->

            <!--Analysis Buttons-->
            <div class="analysis-btns">
                <div class="btn-adest">
                    <button class="btn btn-danger" onclick="return clearAnalysis()">ANALYZE ANOTHER ADSET</button>
                </div>

                <div class="btn-creat-adest">
                    <button class="btn btn-primary" onclick="return optimizeVersion()">CREATE AN OPTIMIZED VERSION OF THIS ADSET</button>
                </div>

                <div class="btn-report">    
                    <button class="btn btn-success" onclick="return generatePDF()">EXPORT THIS REPORT</button>
                </div>
                <div class="clearfix"></div>
                <!--End Analysis Buttons-->

            </div>
        </div>

        <!--End Analysis Done   -->     
        </div>

        <!--Analysis-start-->
</div>

<?php */  ?>

<?php } ?>

 
<!--End Content-->
    <style>
	.jcf-select-autoopvariable{
		width:auto !important;
	}
	</style>
    <script type="text/javascript">
    
      <?php if (isset($adaccountsCount) && $adaccountsCount > 1) { ?>
// 	 $('#main_5').show();  
	
	<?php }else{ ?>
	
// 	  $('#main_5').show();
// 	  animate_spinner();
// 	 setTimeout(function(){ 
	  
// 	    $('#main_1').show();  
	   
// 	    $('#main_5').hide();
// 	 }, 3000);
	
	<?php } ?>
    
    
    
    
    
    function goprev(step){
		   
		     var nextstep = parseInt(step)-1;
		     
		      //if(step == '3'){
		          
		      //$('#main_'+step+'_ad').hide();
		      //$('#main_'+step+'_adset').hide();
		      //$('#main_'+step+'_campaign').hide();
		      
		      //}
		      
		    $('#main_'+step).hide();
            $('#main_'+nextstep).show();
		}
    
    	function gonext(step){
		    
		    var nextstep = parseInt(step)+1;
		    var analyze_level =  $("input[name='analyze']:checked").val();
		    
		    if(nextstep == '3'){
		        $('#main_3_campaign').show();
		        $('#main_3_adset').hide();
		         $('#main_3_ad').hide();
		         
		         
		          if(analyze_level == 'campaign'){
		         $('#ad-more-compaign').show();
		        $('#ad-more-adset').hide();
		         $('#add-more-ads').hide();
		            
		        }else if(analyze_level == 'adset'){
		             $('#ad-more-compaign').hide();
		        $('#ad-more-adset').show();
		         $('#add-more-ads').hide();
		        }else{
		             $('#ad-more-compaign').hide();
		        $('#ad-more-adset').hide();
		         $('#add-more-ads').show();
		            
		        }
		    }
		    
		    $('#main_'+step).hide();
            $('#main_'+nextstep).show();
		}
		
		
		$('.campaignName').change(function(){
		   
         var nextstep = 3;
         var analyze_level =  $("input[name='analyze']:checked").val();
         
         var compaignName = $(this).val();
         
         if(compaignName != ""){
         
          if(analyze_level == 'adset' || analyze_level == 'ad'){
              
               $('#main_3_campaign').hide();
               $('#main_'+nextstep+'_adset').show();
               
          }
          
		}
		
		
     });
		
		
		
			$('.adset').change(function(){
			    
			 
            var nextstep = 3;
            var analyze_level =  $("input[name='analyze']:checked").val();
            
            var adsetName = $(this).val();
      
        if(adsetName !=''){
          if(analyze_level == 'ad'){
              
               $('#main_3_adset').hide();
               $('#main_'+nextstep+'_ad').show();
               
          }
        }else{
            
            $('#main_3_adset').hide();
               $('#main_3_campaign').show();
            
        }
         
        
      });
		
		
	$('.ad').change(function(){
	   
            var nextstep = 3;
            var analyze_level =  $("input[name='analyze']:checked").val();
            
            var adName = $(this).val();
      
        if(adName ==''){
           $('#main_3_adset').show();
           $('#main_3_ad').hide();
        }
        
      });
		
		
		
    
    
          $('#use_add_account').click(function(){
              
              
            $('.analyz_add_account').hide();
            $('#main_5').show();
            
            animate_spinner();
            
           var addaccountID =  $("input[name='ad-account']:checked").val();
        
           if(addaccountID != "" && typeof addaccountID !== 'undefined'){
           window.location = '<?php echo base_url(); ?>analysis/'+addaccountID;
           } else{
               return false;
           }
        });
    
    
    
    
    function startAnalysis(){
      var validationstr = "";
      if($('#campaignName').val() == "0"){
        validationstr += "Please Select Campaign";
      }
      if($('#adestName').val() == "0"){
        validationstr += "\n Please Select Adset";
      }
   
      
      
      var analyze_level =  $("input[name='analyze']:checked").val();
      var adsets = new Array() ;
      var datapoints = new Array() ;
      
      
      if(analyze_level=='adset'){
          
          $('.adset').each(function(){
          var datadset =  $(this).val();
          var datasetkey = $(this).find(":selected").text();
          adsets.push({name:datasetkey,id:datadset});
      });  
       
          
      }else if(analyze_level=='ad'){
          
          $('.ad').each(function(){
            var datadset =  $(this).val();
            //adsets.push(datadset);
             var datasetkey = $(this).find(":selected").text();
          adsets.push({name:datasetkey,id:datadset});
          });  
          
          
      }else{
          
          $('.campaignName').each(function(){
            var datadset =  $(this).val();
            //adsets.push(datadset);
             var datasetkey = $(this).find(":selected").text();
            //  alert(datasetkey);
            //  alert(datadset);
          adsets.push({name:datasetkey,id:datadset});
         
          }); 
          
      }
      
     //alert(adsets);
      
       $('.datapoint').each(function(){
           
        var datap =  $(this).val();
        datapoints.push(datap);
          
      });
      
     
      if(validationstr == ""){
       
      
        var timeFrame =  $('#timeframe').val();
        
        
            $('#main_4').hide();
            $('#main_5').show();
            
            animate_spinner();
                
            //alert($adsets);
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "<?php echo base_url().'analysis/getCurlAdsetlifetime'?>/",
            dataType: "json",
            data: {
              analyze_level: analyze_level,  
              addSetId: adsets,
              timeFrame:timeFrame,
              datapoints:datapoints
            },
            beforeSend: function (){
              
            },
            success: function (reponse){
                
            var hits =  reponse['peopleSaw'];
            var spend =  reponse['spend'];
            var datatocompair =  reponse['datatocompair'];
             
            // console.log(datatocompair);
            var hitsTable = gethitTable(hits);
            var spendTable = spendtTable(spend);
            var compareTable = getcomparedata(datatocompair);

           	var columncounttimeframe = 0;
			$.each(hits, function(index, serverdata) {
				 columncounttimeframe++;
			});	
		
		
			var columncountspendTable = 0
			$.each(hits, function(index, serverdata) {
				 columncountspendTable++;
			});	
        
             $('#timeframeReport').html('');
             $('#timeframeReport').html(hitsTable);
             $('#timeframeReport').addClass('column'+columncounttimeframe);
             
             $('#spendTable').html('');
             $('#spendTable').html(spendTable);
             $('#spendTable').addClass('column'+columncountspendTable);
             
             $('#compareTable').html('');
             $('#compareTable').html(compareTable);
             
             
            var hitsTablepdf = gethitTablepdf(hits);
            var spendTablepdf = spendtTablepdf(spend);
            var compareTablepdf = getcomparedatapdf(datatocompair);
           
        
             $('#timeframeReportpdf').html('');
             $('#timeframeReportpdf').html(hitsTablepdf);
             
             $('#spendTablepdf').html('');
             $('#spendTablepdf').html(spendTablepdf);
             
             $('#compareTablepdf').html('');
             $('#compareTablepdf').html(compareTablepdf); 
             
             
           
            $('#main_5').hide();
            $('#main_6').show();
                
            
            },
            error: function (){
              alert('Some Error Occurd');
              //window.location.reload();
            }
            //async: true
          });
        
        //location.href = "<?php echo base_url().'analysis/getCurlAdsetlifetime'; ?>";
        
      }
      else{
        alert(validationstr);
      }
      return false;
    }
		function getCampaignAsdsets(campaignids){
		    
             $('#analysing').show();
           
              var campaignid1 = campaignids.split("|||||");
              var campaignid = campaignid1[0];
        
              
              if(campaignid != "0"){
                  $.ajax({
                    type: "POST",
                    cache: false,
                    url: "<?php echo base_url().'analysis/getCampaignAsdsets'?>/",
                    data: {
                      campaignid: campaignid
                    },
                    beforeSend: function (){
                      
                    },
                    success: function (htmlret){
                    
                      
                      	setTimeout(function(){ 
                      	    
                      	    $('.adset').html(htmlret);
                            $('#analysing').hide();
                         
                      	},300);
                    },
                    async: true
                  });
              }
              else{
              
                $("#adestName").html('<option value="0">Select Adest</option>');
              }
		}
		
		
		
			function getCampaignAd(adset){
     
             $('#analysing').show();
              $('#adsetspineer').show();
              if(adset != "0"){
                  $.ajax({
                    type: "POST",
                    cache: false,
                    url: "<?php echo base_url().'analysis/getCampaignAd'?>/",
                    data: {
                      adset: adset
                    },
                    beforeSend: function (){
                      
                    },
                    success: function (htmlret){
                     
                      	setTimeout(function(){ 
                      	    
                      	   $('.ad').html(htmlret);
                            $('#analysing').hide();
                         
                      	},300);
                      
                    },
                    async: true
                  });
              }
              else{
                $('#adsetspineer').hide();
                $("#adestName").html('<option value="0">Select Adest</option>');
              }
		}

      



        function clearAnalysis(){
            $('#analysis-done').hide();
            $('#analysing').hide();
            $('#campaignName').val("0");            
            $("#adestName").html('<option value="0">Select Adest</option>'); 

            return false;            
        }
		
		function optimizeVersion(){
			/* var finatxt;
            var person = prompt("Enter how much you want the new optimized adset to spend per day", "10");

            if (person == null || person == "" || isNaN(person) || person == "0") {
                finatxt = "0";
            } else {
                finatxt = person;                
            }
            */
            
            if(confirm("Your New Optimized Adset Will Be Published With a Daily Budget of "+$('#pubcamptext').val()) == true){

			    $('#publishCampaign').modal('show');
			
			//return false;
			
			//$('#analysing1').show();
    			setTimeout(function(){ 
                    $.ajax({
                    type: "POST",
                    cache: false,
                    url: "<?php echo base_url().'analysis/copyAdsets'?>/",
                    data: {
                      addSetId: $('#adestName').val(),
                      agerange: $('#bestagerange').val().replace("+", ""),
                      gender: $('#bestgender').val(),
                      adPlacement: $('#bestadplacement').val()
                    },
                    beforeSend: function (){
                      
                    },
                    success: function (htmlret){
                      //$('#analysing1').hide();
        			  $('#publishCampaign').modal('hide');
        			  $('#campaignPublished').modal('show');
        
                    },
        			error: function (data) {
        				$('#publishCampaign').modal('hide');
        			},
                    async: true
                  });
        		  }, 9000);
            }

            return false;            
        }
        

        function generatePDF(){
            var campaignid1 = $('#campaignName').val().split("|||||");
            var campaignobj = campaignid1[1];
            var baseURL;
            baseURL = "<?php echo base_url(); ?>";
            
            var finalurl = baseURL+'analysis/generateanalysispdf?camptitle='+document.getElementById("campaignName").options[document.getElementById('campaignName').selectedIndex].text+'&addSetId='+ $('#adestName').val()+'&campaignObjectivce='+campaignobj;
            window.open(
              finalurl,
              '_blank' // <- This is what makes it open in a new window.
            );


           /* window.location.href = baseURL+'analysis/generateanalysispdf?camptitle='+document.getElementById("campaignName").options[document.getElementById('campaignName').selectedIndex].text+'&addSetId='+ $('#adestName').val()+'&campaignObjectivce='+campaignobj;*/
          
            return false;            
        }
        
		
    </script>
    
    <script>
    $(document).ready(function() {
        
    var max_fields_limit_x      = 3; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    
    
    var max_fields_limit_y      = 3; //set limit for maximum input fields
    var y = 1; //initialize counter for text box
    
    
    var max_fields_limit_z      = 3; //set limit for maximum input fields
    var  z= 1; //initialize counter for text box
    
    var max_fields_limit_d      = 3; //set limit for maximum input fields
    var d = 1; //initialize counter for text box
    
    $('#ad-more-compaign').click(function(e){ //click event on add more fields button having class add_more_button
    
        e.preventDefault();
        if(x < max_fields_limit_x){  
            x++; 
            
			$("#compaignGroup .select-holder:first-child" ).clone().appendTo("#compaignGroup");	
			
			if(x==1){
			     $("#compaignGroup .select-holder:first-child").append('<a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-holder icon-minus" ></span>Remove</a>');
			}else{
			    
			     $("#compaignGroup .select-holder:last-child").append('<a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-holder icon-minus" ></span>Remove</a>');  
			}
			
			  }
    });  
    
    $('#compaignGroup').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent().remove(); x--;
    });
    
    
     $('#ad-more-adset').click(function(e){ //click event on add more fields button having class add_more_button
    
         e.preventDefault();
        
        
        if(y < max_fields_limit_y){  
            y++; 
            
			$("#adsetgroup .select-holder:first-child" ).clone().appendTo("#adsetgroup");	
			
			if(y==1){
			     $("#adsetgroup .select-holder:first-child").append('<a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-holder icon-minus" ></span>Remove</a>');
			}else{
			    
			     $("#adsetgroup .select-holder:last-child").append('<a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-holder icon-minus" ></span>Remove</a>');  
			}
			
			  }
        
        
        
        
    });  
    
    $('#adsetgroup').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent().remove(); y--;
    });
    
    
     $('#add-more-ads').click(function(e){ //click event on add more fields button having class add_more_button
    
        e.preventDefault();
    
       
        if(z < max_fields_limit_z){  
            z++; 
            
			$("#adgroup .select-holder:first-child" ).clone().appendTo("#adgroup");	
			
			if(z==1){
			     $("#adgroup .select-holder:first-child").append('<a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-holder icon-minus" ></span>Remove</a>');
			}else{
			    
			     $("#adgroup .select-holder:last-child").append('<a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-holder icon-minus" ></span>Remove</a>');  
			}
			
			  }
       
       
       
       
    });  
    
    $('#adgroup').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent().remove(); z--;
    });
    
    
    $('#ad-more-datapoint').click(function(e){ //click event on add more fields button having class add_more_button
    
        e.preventDefault();
        // if(x < max_fields_limit){ 
        //     x++; 
        //     $('#datapointgroup').append('<div class="select-holder s2"><select id="ad'+x+'"><option value="">Go Back to Adset</option></select><a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-minus" ></span>Remove</a></div>'); //add input field
        // }
        
        
         if(d < max_fields_limit_d){  
            d++; 
            
			$("#datapointgroup .select-holder:first-child" ).clone().appendTo("#datapointgroup");	
			
			if(d==1){
			     $("#datapointgroup .select-holder:first-child").append('<a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-holder icon-minus" ></span>Remove</a>');
			}else{
			    
			     $("#datapointgroup .select-holder:last-child").append('<a href="#" class="btn btn-compare remove_field" style="margin-left:10px;float:right;"><span class="icon-holder icon-minus" ></span>Remove</a>');  
			}
			
			  }
    });  
    
    
    $('#datapointgroup').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent().remove(); d--;
    });
    
    
    
   
   $('.analysis-report').on("click",".opener", function(e){
   
    var href = $(this).attr('href');
    var temphref = href.split('-');
    var selectedhref = temphref[2];
    var parentthis = $(this);
    
    parentthis.parent().parent().children().children().each(function(){
         $(this).removeClass('activebreakdown');
    });
    
   
    
    parentthis.parent().parent().parent().parent().next().children().children().each(function(){
        
        var id = $(this).attr('id');
        var temp = id.split('-');
        var selectedid = temp[2];
        if(selectedid == selectedhref){ 
            if($(this).css('display') == 'none'){
                $(this).show(); 
               parentthis.addClass('activebreakdown');   
            }else{     
               $(this).hide();    
            }
        }else{
            
          $(this).hide();  
           $(this).removeClass('activebreakdown');
        }
    });
   });
});
</script>

	<script>

//   function animate_spinner(){
//         var params = {
//             container: document.getElementById('lottie'),
//             renderer: 'html',
//             loop: true,
//             autoplay: true,
//             animationData: animationData
//         };

//         var anim;
//         anim = lottie.loadAnimation(params);
//  }
 
 function generatepdf(){
	 $('#heading_report').show();
    var divHeight = $('#analysis-report').height();	
	var divWidth = $('#analysis-report').width();
	var ratio = divHeight / divWidth;
	
	 html2canvas($('#analysis-report'), {
     height: divHeight,
     width: divWidth,
     onrendered: function(canvas) {
          var image = canvas.toDataURL("image/jpeg");
		  $('#heading_report').hide();
		  $('#myModal').modal('hide');
          var doc = new jsPDF('p', 'px', [divHeight, divWidth]); 
          var width = doc.internal.pageSize.width;    
          var height = doc.internal.pageSize.height;
          height = ratio * width;
          doc.addImage(image, 'JPEG', 0, 0, width-10, height-10);
          doc.save('analysis.pdf'); 
		}
		});
   }
   function previewFile(){
		var file_data = $('#uploadlogo').prop('files')[0];
        var form_data = new FormData();  // Create a FormData object
        form_data.append('file', file_data);  // Append all element in FormData  object
        form_data.append('adAccountId', <?php echo $adAccountId; ?>);  // Append all element in FormData  object
        $.ajax({
                url: "<?php echo base_url().'analysis/uploadlogo'?>/",
                dataType    : 'text',           // what to expect back from the PHP script, if anything
                cache       : false,
                contentType : false,
                processData : false,
                data        : form_data,                         
                type        : 'post',
				 beforeSend: function() {
				 $('#loading_report').show();
				},
                success     : function(output){
                   var reader  = new FileReader();
				   var preview = document.getElementById('myImg'); //selects the query named img
				   var file    = document.querySelector('input[type=file]').files[0]; //sames as here
				   if(output=="Logo uploaded! Click Generate PDF to see new report."){
						reader.onloadend = function () {
						preview.src = reader.result;
					}
					 if (file) {
					   reader.readAsDataURL(file); //reads the data as a URL
				   } else {
					   preview.src = "";
				   }
					}
					$('#loading_report').hide();
					alert(output); 
                },
				complete: function() {
					$('#loading_report').hide();
				}
         });
  }
</script>
<script src="http://html2canvas.hertzen.com/dist/html2canvas.js"></script> 
<div class="modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
         
          <h4 class="modal-title">Downaload Report</h4>
		   <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>You can add your logo in PDF.</p>
		  	<div class="btn-col">
								<input type="file" id="uploadlogo" class="inputfile"  onchange="previewFile()"/>
								<label for="uploadlogo"><span class="icon-upload"></span><span>Upload Logo</span></label>
								

								</a>
						</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success" onclick="generatepdf()">Generate PDF</button>
        </div>
      </div>
    </div>
  </div>
    
 