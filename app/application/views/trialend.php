<header>
	
		
		<div class="container-fluid">
			<div class="row no-gutter">
				<div class="col-sm-6">
					<div class="blue">
						<div class="price-item">
							<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/blue.svg" alt="">
							<div class="period">Monthly</div>
							<p class="period-p">Includes everything you need to create a successful Facebook campaign.</p>
							<div class="price"><span class="dollar">$ </span>39 <span class="per">/Month</span></div>
							<div class="features">
								<ul>
									<li>Unlimited Split Testing</li>
									<li>Unlimited Ad Accounts</li>
									<li>Unlimited Ad Spend</li>
									<li>Advanced Report Dashboard</li>
									<li>Priority Support</li>
								</ul>
							</div>
							<div class="clearfix"></div>
                            <a href="<?php echo base_url()."upgrade?plan=monthly"; ?>" class="price-button">Purchase</a>
						<!--	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="monthlyformabc">
 <input type="hidden" name="cmd" value="_xclick-subscriptions" />
 <input type="hidden" name="business" value="samy@fubsz.com" />
 <input type="hidden" name="item_name" value="The Campaign Maker - Pro Monthly Subscription" />
<input type="hidden" name="item_number" value="<?php //echo $this->session->userdata['logged_in']['id']; ?>" />
 <input type="hidden" name="currency_code" value="USD" />
 <input type="hidden" name="return" value="https://thecampaignmaker.com/ui/login/refreshplan" />
 <input type="hidden" name="a3" value="39" />
 <input type="hidden" name="p3" value="1" />
 <input type="hidden" name="t3" value="M" />
 <input type="hidden" name="rm" value="2" />
 <input type="hidden" name="notify_url" value="https://thecampaignmaker.com/aff/connect/paypal_ipn_recurring.php" />
 <input type="hidden" name="custom" value="<?PHP //echo $_SERVER['REMOTE_ADDR']; ?>" />
 <input type="hidden" name="src" value="1" />
 <input type="hidden" name="no_note" value="1" />
<input type="hidden" name="no_shipping" value="1">
<a href="javascript:{}" class="price-button" onclick="document.getElementById('monthlyformabc').submit();">Purchase</a>

 </form> -->
							<div class="bottom">This $1.3/day can prevent hundreds of dollars of wasted adspend</div>
						</div>
					</div>
				</div>	
				<div class="col-sm-6">
					<div class="black">
						<div class="price-item">
							<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/black.svg" alt="">
							<div class="period">Yearly</div>
							<p class="period-p">IPush your ROI even further by going yearly and save an additional 50%.</p>
							<div class="price"><span class="dollar">$ </span>19 <span class="per">/Month &nbsp;&nbsp;&nbsp;Paid Yearly</span></div>
							<div class="features">
								<ul>
									<li>Unlimited Split Testing</li>
									<li>Unlimited Ad Accounts</li>
									<li>Unlimited Ad Spend</li>
									<li>Advanced Report Dashboard</li>
									<li>Priority Support</li>
									<li>Invitation to Elite Facebook Group</li>
								</ul>
							</div>
							<div class="clearfix"></div>
                            <a href="<?php echo base_url()."upgrade?plan=yearly"; ?>" class="price-button">Purchase</a>
							<!--<form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="yearslyform">
 <input type="hidden" name="cmd" value="_xclick-subscriptions" />
 <input type="hidden" name="business" value="samy@fubsz.com" />
 <input type="hidden" name="item_name" value="The Campaign Maker - Pro Yearly Subscription" />
 <input type="hidden" name="item_number" value="<?php //echo $this->session->userdata['logged_in']['id']; ?>" />
 <input type="hidden" name="currency_code" value="USD" />
 <input type="hidden" name="return" value="https://thecampaignmaker.com/ui/login/refreshplan" />
 <input type="hidden" name="a3" value="348" />
 <input type="hidden" name="p3" value="1" />
 <input type="hidden" name="t3" value="Y" />
 <input type="hidden" name="rm" value="2" />
 <input type="hidden" name="notify_url" value="https://thecampaignmaker.com/aff/connect/paypal_ipn_recurring.php" />
 <input type="hidden" name="custom" value="<?PHP //echo $_SERVER['REMOTE_ADDR']; ?>" />
 <input type="hidden" name="src" value="1" />
 <input type="hidden" name="no_note" value="1" />
<input type="hidden" name="no_shipping" value="1">
<a href="javascript:{}" class="price-button" onclick="document.getElementById('yearslyform').submit();">Purchase</a>

 </form>-->
							<div class="bottom">This $0.62/day can prevent hundreds of dollars of wasted adspend</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="price-slider">
		<div class="container">
			<div class="heading">You Can Be One Of Them</div>
			<div class="carousel owl-carousel">
				<div class="testimonial">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
							<svg xmlns="http://www.w3.org/2000/svg" width="32" height="27" viewBox="0 0 32 27">
								<path fill="#FFF" fill-rule="evenodd" d="M12.308 4.91c1.358 0 2.461-1.1 2.461-2.455A2.459 2.459 0 0 0 12.308 0C5.51 0 0 5.496 0 12.273v9.818A4.916 4.916 0 0 0 4.923 27h4.923a4.915 4.915 0 0 0 4.923-4.91v-4.908a4.915 4.915 0 0 0-4.923-4.91H4.923c0-4.066 3.306-7.363 7.385-7.363m14.769 7.364h-4.923c0-4.067 3.306-7.364 7.384-7.364 1.36 0 2.462-1.1 2.462-2.454A2.459 2.459 0 0 0 29.538 0c-6.796 0-12.307 5.496-12.307 12.273v9.818A4.916 4.916 0 0 0 22.154 27h4.923A4.915 4.915 0 0 0 32 22.09v-4.908a4.915 4.915 0 0 0-4.923-4.91" opacity=".203"/>
							</svg>
							<div class="text">I want to reiterate this software is awesome! In March it helped me achieve an ROAS of 321% increase and I'm closing out April at 600% on my ecommerce store. No, I'm not shitting you! I'm seriously impressed.</div>
							<div class="name">Lane W<span class="company">Shoe Conspiracy</span></div>
						</div>
					</div>
				</div>
				<div class="testimonial">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
							<svg xmlns="http://www.w3.org/2000/svg" width="32" height="27" viewBox="0 0 32 27">
								<path fill="#FFF" fill-rule="evenodd" d="M12.308 4.91c1.358 0 2.461-1.1 2.461-2.455A2.459 2.459 0 0 0 12.308 0C5.51 0 0 5.496 0 12.273v9.818A4.916 4.916 0 0 0 4.923 27h4.923a4.915 4.915 0 0 0 4.923-4.91v-4.908a4.915 4.915 0 0 0-4.923-4.91H4.923c0-4.066 3.306-7.363 7.385-7.363m14.769 7.364h-4.923c0-4.067 3.306-7.364 7.384-7.364 1.36 0 2.462-1.1 2.462-2.454A2.459 2.459 0 0 0 29.538 0c-6.796 0-12.307 5.496-12.307 12.273v9.818A4.916 4.916 0 0 0 22.154 27h4.923A4.915 4.915 0 0 0 32 22.09v-4.908a4.915 4.915 0 0 0-4.923-4.91" opacity=".203"/>
							</svg>
							<div class="text">I want to reiterate this software is awesome! In March it helped me achieve an ROAS of 321% increase and I'm closing out April at 600% on my ecommerce store. No, I'm not shitting you! I'm seriously impressed.</div>
							<div class="name">Lane W<span class="company">Shoe Conspiracy</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>