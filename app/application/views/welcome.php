<?php ?>
<main id="main">
    <div class="main-content">
        <div class="container">
            <div class="welcome-screen">
                <div class="holder">
                    <h2 class="screen-heading">Hey <?php echo $this->session->userdata['logged_in']['first_name']; ?>! <br> What would you like to do?</h2>
                    <div class="item-holder d-flex flex-wrap">
                        <div class="cta-holder d-flex">
                            <a class="btn-cta d-flex justify-content-between align-items-center shadow-red" href="<?= site_url('createcampaign'); ?>">
                                <div class="detail d-flex align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-megaphone.svg" alt="megaphone">
                                    </div>
                                    <div class="text">
                                        <strong class="title">Create a New Campaign</strong>
                                        <span class="tool-info">Using The Creation Tool</span>
                                    </div>
                                </div>
                                <div class="arrow-holder">
                                    <span class="icon-angle-right"></span>
                                </div>
                            </a>
                        </div>
                        <div class="cta-holder d-flex">
                            <a class="btn-cta d-flex justify-content-between align-items-center shadow-blue-gary" href="<?= site_url('reports'); ?>">
                                <div class="detail d-flex align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-monitor.svg" alt="monitor">
                                    </div>
                                    <div class="text">
                                        <strong class="title">Open Your Report Dashboard</strong>
                                        <span class="tool-info">Using The Reporting Tool</span>
                                    </div>
                                </div>
                                <div class="arrow-holder">
                                    <span class="icon-angle-right"></span>
                                </div>
                            </a>
                        </div>
                        <div class="cta-holder d-flex">
                            <a class="btn-cta d-flex justify-content-between align-items-center shadow-blue" href="<?= site_url('analysis'); ?>">
                                <div class="detail d-flex align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-statistic.svg" alt="statistic">
                                    </div>
                                    <div class="text">
                                        <strong class="title">Analyze an Old Adset</strong>
                                        <span class="tool-info">Using The Analysis Tool</span>
                                    </div>
                                </div>
                                <div class="arrow-holder">
                                    <span class="icon-angle-right"></span>
                                </div>
                            </a>
                        </div>
                        <div class="cta-holder d-flex">
                            <a class="btn-cta d-flex justify-content-between align-items-center shadow-green" href="<?= site_url('automaticoptimization'); ?>">
                                <div class="detail d-flex align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-tools.svg" alt="tools">
                                    </div>
                                    <div class="text">
                                        <strong class="title">Setup an Optimization Rule</strong>
                                        <span class="tool-info">Using The Optimization Tool</span>
                                    </div>
                                </div>
                                <div class="arrow-holder">
                                    <span class="icon-angle-right"></span>
                                </div>
                            </a>
                        </div>
                        <div class="cta-holder d-flex">
                            <a class="btn-cta d-flex justify-content-between align-items-center shadow-purpule" href="<?= site_url('helps'); ?>">
                                <div class="detail d-flex align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-crown.svg" alt="crown">
                                    </div>
                                    <div class="text">
                                        <strong class="title">Get Expert Help</strong>
                                        <span class="tool-info">Done For You Premium Service</span>
                                    </div>
                                </div>
                                <div class="arrow-holder">
                                    <span class="icon-angle-right"></span>
                                </div>
                            </a>
                        </div>
                        <div class="cta-holder d-flex">
                            <a class="btn-cta d-flex justify-content-between align-items-center shadow-yellow" href="<?= site_url('helps'); ?>">
                                <div class="detail d-flex align-items-center">
                                    <div class="icon-holder">
                                        <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-lamp.svg" alt="lamp">
                                    </div>
                                    <div class="text">
                                        <strong class="title">Go to The Help Section</strong>
                                        <span class="tool-info">Walkthrough Videos</span>
                                    </div>
                                </div>
                                <div class="arrow-holder">
                                    <span class="icon-angle-right"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>