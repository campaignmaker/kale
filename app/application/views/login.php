<main id="main">

				<div class="main-content">

					<div class="container">

						<div class="form-wrap">

							<div class="holder">

								<!--form name="mepr_loginform" id="mepr_loginform" class="mepr-form" action="<?php //echo $this->config->item('site_url');?>wp-login.php" method="post"-->
								
								<form class="form s1 login-form" action="<?php echo base_url();?>login" method="post">
								  <div class="mp-form-row mepr_username form-group">

									<div class="mp-form-label">

									  	<label for="log">Email Address</label>

									 </div>

									<!--input type="text" name="log" class="form-control" id="user_login" value=""-->
									<input class="form-control" id="email" name="email" type="email" required>

								  </div>

								  <div class="mp-form-row mepr_password form-group">

									<div class="mp-form-label">

									  <label for="pwd">Password</label>

									 </div>

									<!--input type="password" name="pwd" class="form-control" id="user_pass" value=""-->
									<input class="form-control" id="pwd" name="password" required type="password">
								  </div>

								  	<!--<div>

									<label><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> </label>

								  </div>-->

								  <div class="mp-spacer">&nbsp;</div>

								  <div class="d-sm-flex">

									<div class="">

										<div class="submit">

										<!--input type="submit" name="wp-submit" id="wp-submit" class="button-primary mepr-share-button btn btn-primary" value="Log In"-->
										
										<input type="submit" class="button-primary btn btn-primary" value="Log In">
										
										<!--input type="hidden" name="redirect_to" value="<?php //echo $this->config->item('site_url'); ?>v4/reports">

										<input type="hidden" name="mepr_process_login_form" value="true">

										<input type="hidden" name="mepr_is_login_page" value="true"-->

										</div>

									</div>

									<div class="col">

										<div class="btn-holder text-center text-sm-right">

										  <a href="<?php echo $this->config->item('site_url'); ?>chklogin/fb-signupauthorization.php" class="btn btn-fb"><span class="icon-facebook"></span>LOGIN WITH FB</a>

										</div>

									</div>

						          </div>

								  <div class="mepr-login-actions">

									  <a class="link-s1" href="<?php echo $this->config->item('site_url'); ?>login/?action=forgot_password">Forgot Password</a>

								  </div>

								</form>

							</div>

							<div class="panel">

								<div class="panel-holder d-sm-flex justify-content-between align-items-center">

									<div class="link-holder text-center text-sm-left">

										<a href="#" class="link-s2">Don’t have an account?</a>

									</div>

									<div class="btn-holder text-center text-sm-right">

										<a href="<?php echo base_url();?>register" class="btn btn-secondary">REGISTER NOW</a>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</main>

			

	

