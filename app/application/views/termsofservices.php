<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('menu'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <div class="col-md-7  p-rl-3">
                        <h3 class="page-title">
                            Terms of Service
                        </h3>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="row">
                    <p>
                        <strong>1. Introduction</strong>
                    </p>
                    <p>
                        1.1 Welcome to thecampaignmaker.com. The Campaign Maker is web based Facebook marketing software; it basically connects to the Facebook ad account and
                        helps you create more in depth campaigns than normal.
                    </p>
                    <p>
                        1.2 These terms and conditions shall govern your use of thecampaignmaker.com.
                    </p>
                    <p>
                        1.3 By using thecampaignmaker.com, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part
                        of these terms and conditions, you must not use thecampaignmaker.com.
                    </p>
                    <p>
                        1.4 If you register with thecampaignmaker.com, submit any material to thecampaignmaker.com or use any of thecampaignmaker.com services, The Campaign Maker
                        will ask you to expressly agree to these terms and conditions.
                    </p>
                    <p>
                        1.5 You must be at least 13 years of age to use thecampaignmaker.com; and by using thecampaignmaker.com or agreeing to these terms and conditions, you
                        warrant and represent to us that you are at least 13 years of age.
                    </p>
                    <p>
                        1.6 Thecampaignmaker.com uses cookies; by using thecampaignmaker.com or agreeing to these terms and conditions, you consent to our use of cookies in
                        accordance with the terms of our privacy policy.
                    </p>
                    <p>
                        <strong>2. Copyright notice</strong>
                    </p>
                    <p>
                        2.1 Copyright (c) 2015 FUBSZ LLC.
                    </p>
                    <p>
                        2.2 Subject to the express provisions of these terms and conditions:
                    </p>
                    <p>
                        (a) we, together with our licensors, own and control all the copyright and other intellectual property rights in thecampaignmaker.com and the material on
                        thecampaignmaker.com; and
                    </p>
                    <p>
                        (b) All the copyright and other intellectual property rights in thecampaignmaker.com and the material on thecampaignmaker.com are reserved.
                    </p>
                    <p>
                        <strong>3. Licence to use website</strong>
                    </p>
                    <p>
                        3.1 You may:
                    </p>
                    <p>
                        (a) View pages from thecampaignmaker.com in a web browser;
                    </p>
                    <p>
                        (b) Download pages from thecampaignmaker.com for caching in a web browser;
                    </p>
                    <p>
                        (c) Print pages from thecampaignmaker.com;
                    </p>
                    <p>
                        (d) Stream audio and video files from thecampaignmaker.com; and
                    </p>
                    <p>
                        (e) Use thecampaignmaker.com services by means of a web browser,
                    </p>
                    <p>
                        Subject to the other provisions of these terms and conditions.
                    </p>
                    <p>
                        3.2 Except as expressly permitted by Section 4.1 or the other provisions of these terms and conditions, you must not download any material from
                        thecampaignmaker.com or save any such material to your computer.
                    </p>
                    <p>
                        3.3 You may only use thecampaignmaker.com for your own personal and business purposes, and you must not use thecampaignmaker.com for any other purposes.
                    </p>
                    <p>
                        3.4 Except as expressly permitted by these terms and conditions, you must not edit or otherwise modify any material on thecampaignmaker.com.
                    </p>
                    <p>
                        3.5 Unless you own or control the relevant rights in the material, you must not:
                    </p>
                    <p>
                        (a) Republish material from thecampaignmaker.com (including republication on another website);
                    </p>
                    <p>
                        (b) Sell, rent or sub-license material from thecampaignmaker.com;
                    </p>
                    <p>
                        (c) Show any material from thecampaignmaker.com in public;
                    </p>
                    <p>
                        (d) Exploit material from thecampaignmaker.com for a commercial purpose; or
                    </p>
                    <p>
                        (e) Redistribute material from thecampaignmaker.com.
                    </p>
                    <p>
                        3.6 Notwithstanding Section 4.5, you may redistribute our newsletter in print and electronic form to any person.
                    </p>
                    <p>
                        3.7 The Campaign Maker reserve the right to restrict access to areas of thecampaignmaker.com, or indeed our whole website, at our discretion; you must not
                        circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on thecampaignmaker.com.
                    </p>
                    <p>
                        <strong>4. Acceptable use</strong>
                    </p>
                    <p>
                        4.1 You must not:
                    </p>
                    <p>
                        (a) Use thecampaignmaker.com in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability
                        or accessibility of the website;
                    </p>
                    <p>
                        (b) Use thecampaignmaker.com in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or
                        harmful purpose or activity;
                    </p>
                    <p>
                        (c) use thecampaignmaker.com to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware,
                        computer virus, Trojan horse, worm, keystroke logger, root kit or other malicious computer software;
                    </p>
                    <p>
                        (d) Conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data
                        harvesting) on or in relation to thecampaignmaker.com without our express written consent;
                    </p>
                    <p>
                        (e) Access or otherwise interact with thecampaignmaker.com using any robot, spider or other automated means;
                    </p>
                    <p>
                        (f) Violate the directives set out in the robots.txt file for thecampaignmaker.com; or
                    </p>
                    <p>
                        (g) Use data collected from thecampaignmaker.com for any direct marketing activity (including without limitation email marketing, SMS marketing,
                        telemarketing and direct mailing).
                    </p>
                    <p>
                        4.2 You must not use data collected from thecampaignmaker.com to contact individuals, companies or other persons or entities.
                    </p>
                    <p>
                        4.3 You must ensure that all the information you supply to us through thecampaignmaker.com, or in relation to thecampaignmaker.com, is true, accurate,
                        current, complete and non-misleading.
                    </p>
                    <p>
                        <strong> </strong>
                    </p>
                    <p>
                        <strong>5. Registration and accounts</strong>
                    </p>
                    <p>
                        5.1 To be eligible for an individual account on thecampaignmaker.com under this Section 6, you must be at least 13 years of age.
                    </p>
                    <p>
                        5.2 You may register for an account with thecampaignmaker.com by completing and submitting the account registration form on thecampaignmaker.com, and
                        clicking on the verification link in the email that the website will send to you.
                    </p>
                    <p>
                        5.3 You must notify us in writing immediately if you become aware of any unauthorised use of your account.
                    </p>
                    <p>
                        5.4 You must not use any other person's account to access the website, unless you have that person's express permission to do so.
                    </p>
                    <p>
                        <strong>6. User IDs and passwords</strong>
                    </p>
                    <p>
                        6.1 If you register for an account with thecampaignmaker.com, The Campaign Maker will provide you with / you will be asked to choose a user ID and
                        password.
                    </p>
                    <p>
                        6.2 Your user ID must not be liable to mislead and must comply with the content rules set out in Section 10; you must not use your account or user ID for
                        or in connection with the impersonation of any person.
                    </p>
                    <p>
                        6.3 You must keep your password confidential.
                    </p>
                    <p>
                        6.4 You must notify us in writing immediately if you become aware of any disclosure of your password.
                    </p>
                    <p>
                        6.5 You are responsible for any activity on thecampaignmaker.com arising out of any failure to keep your password confidential, and may be held liable for
                        any losses arising out of such a failure.
                    </p>
                    <p>
                        <strong>7. Cancellation and suspension of account</strong>
                    </p>
                    <p>
                        7.1 The Campaign Maker may:
                    </p>
                    <p>
                        (a) Suspend your account;
                    </p>
                    <p>
                        (b) Cancel your account; and/or
                    </p>
                    <p>
                        (c) Edit your account details,
                    </p>
                    <p>
                        At any time in our sole discretion without notice or explanation.
                    </p>
                    <p>
                        7.2 You may cancel your account on thecampaignmaker.com using your account control panel on the website.
                    </p>
                    <p>
                        <strong>8.Payment; Taxes.</strong>
                    </p>
                    <p>
                        8.1 The Campaign Maker will charge any subscription fees set forth on the Service pricing webpage (“Order�?) http://TheCampaignMaker.com/pricing on the Effective Date, and thereafter on each monthly “anniversary�? thereof.  Subscription fee charges for months following the Effective Date will be charged to the credit card provided to The Campaign Maker by User. The Campaign Maker will send User a confirmation email receipt upon each credit card charge to the email address provided by User.  All payments shall be non-refundable, paid in US dollars and via credit card in accordance with instructions provided by The Campaign Maker.  User will provide a valid credit card and an email address for charge receipts upon registration, and shall update such information from time to time as necessary to ensure that such information is at all times accurate.  If the credit card information User has provided is incorrect or incomplete, or The Campaign Maker is otherwise unable to complete a credit card transaction due to User’s error or omission, the account will be shut down within 5 days. The Campaign Maker currently uses Braintree Payments to process credit card charges.  Braintree Payment’s terms of service and privacy policy can be found here https://www.braintreepayments.com/landing/gateway-terms-of-service  and here https://www.braintreepayments.com/privacy . User shall pay a late fee on all amounts not paid at the rate of one and one-half percent (1.5%) per month or the maximum amount permitted by law, whichever is less. The Campaign Maker reserves the right to change the Subscription Fees for any Subscription Level at any time upon thirty (30) days prior notice to User and to change payment processors at any time. When running a Facebook campaign through the Service, you must associate a Facebook account including a Facebook approved payment method for paying all Facebook media costs.
                    </p>
                    <p>
                        8.2 User may choose to upgrade to a more-expensive or downgrade to a less expensive subscription level at any time during the term of this Agreement.  Upgrade/downgrade pricing may be found on the Order webpage http://TheCampaignMaker.com/pricing
                    </p>
                    <p>
                        8.3 If this Agreement is terminated by either party for any reason other than User’s breach, the user’s account will remain active until the end of the current billing cycle. The Campaign Maker will then stop charging the user and the account will be terminated within 5 days from the end of the billing cycle.
                    </p>
                    <p>
                        8.4 User shall bear and be responsible for any applicable federal, state, local and foreign taxes, duties, tariffs, levies, withholdings and similar assessments (including without limitation, sales taxes, use taxes and value added taxes) relating to the subject matter hereunder, excluding taxes based upon The Campaign Maker’s net income. 
                    </p>
                    <p>
                        <strong>9. Your content: licence</strong>
                    </p>
                    <p>
                        9.1 In these terms and conditions, "your content" means all works and materials (including without limitation text, graphics, images, audio material, video
                        material, audio-visual material, scripts, software and files) that you submit to us or thecampaignmaker.com for storage or publication on, processing by,
                        or transmission via, thecampaignmaker.com.
                    </p>
                    <p>
                        9.2 You grant to us a worldwide, irrevocable, non-exclusive, royalty-free licence to use, reproduce, store, adapt, publish, translate and distribute your
                        content in any existing or future media / reproduce, store and publish your content on and in relation to this website and any successor website /
                        reproduce, store and, with your specific consent, publish your content on and in relation to this website.
                    </p>
                    <p>
                        9.3 You grant to us the right to sub-license the rights licensed under Section 8.2.
                    </p>
                    <p>
                        9.4 You grant to us the right to bring an action for infringement of the rights licensed under Section 8.2.
                    </p>
                    <p>
                        9.5 You hereby waive all your moral rights in your content to the maximum extent permitted by applicable law; and you warrant and represent that all other
                        moral rights in your content have been waived to the maximum extent permitted by applicable law.
                    </p>
                    <p>
                        9.6 You may edit your content to the extent permitted using the editing functionality made available on thecampaignmaker.com.
                    </p>
                    <p>
                        9.7 Without prejudice to our other rights under these terms and conditions, if you breach any provision of these terms and conditions in any way, or if The
                        Campaign Maker reasonably suspect that you have breached these terms and conditions in any way, The Campaign Maker may delete, unpublished or edit any or
                        all of your content.
                    </p>
                    <p>
                        <strong>10. Your content: rules</strong>
                    </p>
                    <p>
                        10.1 You warrant and represent that your content will comply with these terms and conditions.
                    </p>
                    <p>
                        10.2 Your content must not be illegal or unlawful, must not infringe any person's legal rights, and must not be capable of giving rise to legal action
                        against any person (in each case in any jurisdiction and under any applicable law).
                    </p>
                    <p>
                        10.3 Your content, and the use of your content by us in accordance with these terms and conditions, must not:
                    </p>
                    <p>
                        (a) Be libellous or maliciously false;
                    </p>
                    <p>
                        (b) Be obscene or indecent;
                    </p>
                    <p>
                        (c) infringe any copyright, moral right, database right, trade mark right, design right, right in passing off, or other intellectual property right;
                    </p>
                    <p>
                        (d) Infringe any right of confidence, right of privacy or right under data protection legislation;
                    </p>
                    <p>
                        (e) Constitute negligent advice or contain any negligent statement;
                    </p>
                    <p>
                        (f) Constitute an incitement to commit a crime, instructions for the commission of a crime or the promotion of criminal activity;
                    </p>
                    <p>
                        (g) Be in contempt of any court, or in breach of any court order;
                    </p>
                    <p>
                        (h) Be in breach of racial or religious hatred or discrimination legislation;
                    </p>
                    <p>
                        (I) am blasphemous;
                    </p>
                    <p>
                        (j) Be in breach of official secrets legislation;
                    </p>
                    <p>
                        (k) Be in breach of any contractual obligation owed to any person;
                    </p>
                    <p>
                        (l) Depict violence, in an explicit, graphic or gratuitous manner;
                    </p>
                    <p>
                        (m) Be pornographic, lewd, suggestive or sexually explicit;
                    </p>
                    <p>
                        (n) Be untrue, false, inaccurate or misleading;
                    </p>
                    <p>
                        (o) consist of or contain any instructions, advice or other information which may be acted upon and could, if acted upon, cause illness, injury or death,
                        or any other loss or damage;
                    </p>
                    <p>
                        (p) Constitute spam;
                    </p>
                    <p>
                        (q) Be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory; or
                    </p>
                    <p>
                        (r) Cause annoyance, inconvenience or needless anxiety to any person.
                    </p>
                    <p>
                        <strong>11. Limited warranties</strong>
                    </p>
                    <p>
                        11.1 The Campaign Maker does not warrant or represent:
                    </p>
                    <p>
                        (a) The completeness or accuracy of the information published on thecampaignmaker.com;
                    </p>
                    <p>
                        (b) That the material on the website is up to date; or
                    </p>
                    <p>
                        (c) That the website or any service on the website will remain available.
                    </p>
                    <p>
                        11.2 The Campaign Maker reserve the right to discontinue or alter any or all of thecampaignmaker.com services, and to stop publishing thecampaignmaker.com,
                        at any time in our sole discretion without notice or explanation; and save to the extent expressly provided otherwise in these terms and conditions, you
                        will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if The Campaign Maker stop
                        publishing the website.
                    </p>
                    <p>
                        11.3 To the maximum extent permitted by applicable law and subject to Section 11.1, The Campaign Maker excludes all representations and warranties relating
                        to the subject matter of these terms and conditions, thecampaignmaker.com and the use of thecampaignmaker.com.
                    </p>
                    <p>
                        <strong>12. Limitations and exclusions of liability</strong>
                    </p>
                    <p>
                        12.1 Nothing in these terms and conditions will:
                    </p>
                    <p>
                        (a) Limit or exclude any liability for death or personal injury resulting from negligence;
                    </p>
                    <p>
                        (b) Limit or exclude any liability for fraud or fraudulent misrepresentation;
                    </p>
                    <p>
                        (c) Limit any liabilities in any way that is not permitted under applicable law; or
                    </p>
                    <p>
                        (d) Exclude any liabilities that may not be excluded under applicable law.
                    </p>
                    <p>
                        12.2 The limitations and exclusions of liability set out in this Section 12 and elsewhere in these terms and conditions:
                    </p>
                    <p>
                        (a) Are subject to Section 11.1; and
                    </p>
                    <p>
                        (b) Govern all liabilities arising under these terms and conditions or relating to the subject matter of these terms and conditions, including liabilities
                        arising in contract, in tort (including negligence) and for breach of statutory duty.
                    </p>
                    <p>
                        12.3 To the extent that thecampaignmaker.com and the information and services on thecampaignmaker.com are provided free of charge, The Campaign Maker will
                        not be liable for any loss or damage of any nature.
                    </p>
                    <p>
                        12.4 The Campaign Maker will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.
                    </p>
                    <p>
                        12.5 The Campaign Maker will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income,
                        revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.
                    </p>
                    <p>
                        12.6 The Campaign Maker will not be liable to you in respect of any loss or corruption of any data, database or software.
                    </p>
                    <p>
                        12.7 The Campaign Maker will not be liable to you in respect of any special, indirect or consequential loss or damage.
                    </p>
                    <p>
                        12.8 You accept that The Campaign Maker have an interest in limiting the personal liability of our officers and employees and, having regard to that
                        interest, you acknowledge that The Campaign Maker are a limited liability entity; you agree that you will not bring any claim personally against our
                        officers or employees in respect of any losses you suffer in connection with the website or these terms and conditions (this will not, of course, limit or
                        exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).
                    </p>
                    <p>
                        <strong>13. Breaches of these terms and conditions</strong>
                    </p>
                    <p>
                        13.1 Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if The Campaign Maker
                        reasonably suspect that you have breached these terms and conditions in any way, The Campaign Maker may:
                    </p>
                    <p>
                        (a) Send you one or more formal warnings;
                    </p>
                    <p>
                        (b) Temporarily suspend your access to thecampaignmaker.com;
                    </p>
                    <p>
                        (c) Permanently prohibit you from accessing thecampaignmaker.com;
                    </p>
                    <p>
                        (d) Block computers using your IP address from accessing thecampaignmaker.com;
                    </p>
                    <p>
                        (e) Contact any or all your internet service providers and request that they block your access to thecampaignmaker.com;
                    </p>
                    <p>
                        (f) Commence legal action against you, whether for breach of contract or otherwise; and/or
                    </p>
                    <p>
                        (g) Suspend or delete your account on thecampaignmaker.com.
                    </p>
                    <p>
                        13.2 Where the Campaign Maker suspend or prohibit or block your access to thecampaignmaker.com or a part of thecampaignmaker.com, you must not take any
                        action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).
                    </p>
                    <p>
                        <strong>14. Variation</strong>
                    </p>
                    <p>
                        14.1 The Campaign Maker may revise these terms and conditions from time to time.
                    </p>
                    <p>
                        14.2 The revised terms and conditions shall apply to the use of thecampaignmaker.com from the date of publication of the revised terms and conditions on
                        the website, and you hereby waive any right you may otherwise have to be notified of, or to consent to, revisions of these terms and conditions. / The
                        Campaign Maker will give you written notice of any revision of these terms and conditions, and the revised terms and conditions will apply to the use of
                        thecampaignmaker.com from the date that The Campaign Maker give you such notice; if you do not agree to the revised terms and conditions, you must stop
                        using thecampaignmaker.com.
                    </p>
                    <p>
                        14.3 If you have given your express agreement to these terms and conditions, The Campaign Maker will ask for your express agreement to any revision of
                        these terms and conditions; and if you do not give your express agreement to the revised terms and conditions within such period as The Campaign Maker may
                        specify, The Campaign Maker will disable or delete your account on the website, and you must stop using the website.
                    </p>
                    <p>
                        <strong>15. Assignment</strong>
                    </p>
                    <p>
                        15.1 You hereby agree that The Campaign Maker may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms and
                        conditions.
                    </p>
                    <p>
                        15.2 You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these
                        terms and conditions.
                    </p>
                    <p>
                        <strong>16. Severability</strong>
                    </p>
                    <p>
                        16.1 If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other
                        provisions will continue in effect.
                    </p>
                    <p>
                        16.2 If any unlawful and/or unenforceable provision of these terms and conditions would be lawful or enforceable if part of it were deleted, that part will
                        be deemed to be deleted, and the rest of the provision will continue in effect.
                    </p>
                    <p>
                        <strong>17. Third party rights</strong>
                    </p>
                    <p>
                        17.1 These terms and conditions are for our benefit and your benefit, and these terms and conditions are not intended to benefit or be enforceable by any
                        third party.
                    </p>
                    <p>
                        17.2 The exercise of the parties' rights under these terms and conditions is not subject to the consent of any third party.
                    </p>
                    <p>
                        <strong>18. Entire agreement</strong>
                    </p>
                    <p>
                        18.1 Subject to Section 11.1, these terms and conditions, together with our privacy policy, shall constitute the entire agreement between you and us in
                        relation to your use of thecampaignmaker.com and shall supersede all previous agreements between you and us in relation to your use of
                        thecampaignmaker.com.
                    </p>
                    <p>
                        <strong>19. Law and jurisdiction</strong>
                    </p>
                    <p>
                        19.1 These terms and conditions shall be governed by and construed in accordance with Florida law.
                    </p>
                    <p>
                        19.2 Any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the courts of Florida, USA.
                    </p>
                    <p>
                        <strong>20. Our details</strong>
                    </p>
                    <p>
                        20.1 This website is owned and operated by FUBSZ LLC.
                    </p>
                    <p>
                        20.2 The Campaign Maker is registered in Florida, USA and our registered office is at 1175 Highway A1a, Satellite Beach, FL.
                    </p>
                    <p>
                        20.3 Our principal place of business is at 1175 Highway A1a #603, Satellite Beach, FL.
                    </p>
                    <p>
                        20.4 You can contact us by writing to the business address given above, by using thecampaignmaker.com contact form, by email to    <a href="mailto:Samy@thecampaignmaker.com">Samy@thecampaignmaker.com</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>