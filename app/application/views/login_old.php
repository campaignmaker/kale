<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="<?php SeoDetail('Description'); ?>" name="description"/>
  <meta content="The Campaign Maker" name="author"/>
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php SeoDetail('Description'); ?></title>

    <style>body { opacity: 0; overflow-x: hidden; } html { background-color: #fff; }</style>
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?php echo $this->config->item('assets'); ?>newdesign/css/main.min.css">
  <meta name="theme-color" content="#000">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#000">
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-status-bar-style" content="#000">


  
<link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>/assets/img/favicon.ico">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

    </head>
<body class="sign-page login-page">
     <div class="login-screen">
    <div class="container">
      <h1>Welcome Back to <br> The Campaign Maker</h1>
      <p>The greatest mistake you can make in life is continually <br class="hidden-xs">fearing that you'll make one.</p>
      <div class="btn-wrap">
        <a href="https://thecampaignmaker.com/chklogin/fb-authorization.php" class="button-login">Login Now With Facebook</a>
        <p>Don’t have an account? <a href="#">Sign Up Here &#10095;</a></p>
      </div>
    </div>
  </div>

  <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/scripts.min.js"></script>
    </body>
    </html>
