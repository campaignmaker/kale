<?php
/* add new rules page */

?>
	<!-- main container of all the page elements -->
	<?php //if($step == '1' || $step == ''){  ?>

    </style>
	<div id="wrapper">
		<div class="w1">
			<!-- header of the page -->
		<form id="add_rule" action="/app/automaticoptimization/create_automaticoptimization" class="form" method="post">
			
			<main id="main_1" >
				<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step active">
							<strong class="step-name">Name</strong>
						</div>
						<div class="step">
							<strong class="step-name">Campaign</strong>
						</div>
						<div class="step">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step">
							<strong class="step-name">Rule</strong>
						</div>
						<div class="step">
							<strong class="step-name">Action</strong>
						</div>
						<div class="step">
							<strong class="step-name">Freq</strong>
						</div>
					</div>
				</div>
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Name Your Optimization Rule
										<span class="sub-heading">
											This will help you find rules quicker!
											<strong class="highlight-text">Pro tip:  this will only be seen by you.</strong>
										</span>
									</h2>
									<div class="form-wrap s1">
										<!--<form id="rule_name" action="/v4/automaticoptimization/createrule" class="form" method="post">-->
											<div class="form-group">
											    <!--<input type="hidden" value="1" name="step" />-->
												<input type="text" class="form-control" name="ruleName" placeholder="type your rule name here….">
											</div>
											
											
										<!--</form>-->
									</div>
								</div>
								<div class="btn-holder text-center">
									<a href="javascript:void(null);" onclick="gonext('1')" class="btn btn-next">
										<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-next-pointer.svg" alt="pointer"></span>
										Next Step
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			
	
	
	<?php //}  ?>

	   	<?php // if($step == '2'){  ?>
		
			<main id="main_2" style="display:none">
			    
			    	<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Name</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Campaign</strong>
						</div>
						<div class="step">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step">
							<strong class="step-name">Rule</strong>
						</div>
						<div class="step">
							<strong class="step-name">Action</strong>
						</div>
						<div class="step">
							<strong class="step-name">Freq</strong>
						</div>
					</div>
				</div>
			
				<div class="main-content s1">
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose The Campaign You Want To Optimize
										<span class="sub-heading">
											Specify the campaign you want the following rules applied to.
											<strong class="highlight-text">Pro tip: the rules will be applied to the individual ads inside the campaign to provide the best results.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
										<!--<form id="Campaign_form" action="" class="form">-->
											<div class="form-group">
											     <!--<input type="hidden" value="2" name="step" />-->
												<div class="select-holder s2">
													<select name="compaign">
														<!--<option value="">Choose Your Campaign</option>-->
														<?php foreach($campaigns_list as $key => $temp){  ?>
														<option value="<?php echo $key;  ?>"><?php echo $temp;  ?></option>
													
														<?php } ?>
													</select>
												</div>
											</div>
										<!--</form>-->
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="goprev('2')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="gonext('2')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		<?php // }  ?>
	
	   <?php //if($step == '3'){  ?>
	   
	    <main id="main_3" style="display:none">
	       	<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Name</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Campaign</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step">
							<strong class="step-name">Rule</strong>
						</div>
						<div class="step">
							<strong class="step-name">Action</strong>
						</div>
						<div class="step">
							<strong class="step-name">Freq</strong>
						</div>
					</div>
				</div>
		<div  class="main-content s1" >
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose The Time Frame You Want Applied
										<span class="sub-heading">
											How far back do you want to read the data? for example today will only read today's data.
											<strong class="highlight-text">Pro tip: the higher the timeframe (lifetime) the more aggressive the rules will be applied.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
										<!--<form id="time_frame_form" action="" class="form">-->
											<div class="form-group">
											     <!--<input type="hidden" value="3" name="step" />-->
												<div class="select-holder s2">
													<select name="timeFrame">
														<!--<option value="">Choose Your Time Frame</option>-->
														<option value="TODAY"> Today </option>
                                                        <option value="YESTERDAY"> Yesterday </option>
                                                        <option value="LAST_3_DAYS"> Last 3 Days</option>
                                                        <option value="LAST_7_DAYS"> Last 7 Days</option>
                                                        <option value="LAST_14_DAYS"> Last 14 Days</option>
                                                        <option value="LAST_30_DAYS"> Last 30 Days</option>
                                                        <option value="LIFETIME"> Lifetime</option>
													</select>
												</div>
											</div>
										<!--</form>-->
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="goprev('3')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="gonext('3')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	   </main>		
				<?php //}  ?>
				
			<?php //if($step == '4'){  ?>	
				<main id="main_4" style="display:none">
				 	<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Name</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Campaign</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Rule</strong>
						</div>
						<div class="step">
							<strong class="step-name">Action</strong>
						</div>
						<div class="step">
							<strong class="step-name">Freq</strong>
						</div>
					</div>
				</div>   
				<div   class="main-content s1" >
					<div class="container">
						<div class="center-box s9">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Setup Your Optimization Rules
										<span class="sub-heading">
											Choose between one of many data points you can base your rules on.
											<strong class="highlight-text">Pro tip: impressions of 5,000 or more is the best initial rule.</strong>
										</span>
									</h2>
									<div class="form-wrap s5">
										<!--<form id="optimize_rule_form" action="" class="form">-->
										     <!--<input type="hidden" value="4" name="step" />-->
										     
										     <input type="hidden" id="theidautocamp" name="theidautocamp" value="0">
										     <input type="hidden" id="addAccountId" name="addAccountId" value="<?php echo $addAccountId; ?>">
										     
											<div class="big-btn-holder">
												<div class="btn-row d-flex flex-wrap justify-content-center">
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom d-flex flex-wrap" id="add_new_rule">
															<input type="radio" name="optimize" id="new-rule">
															<label for="new-rule" class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-megaphone-rounded.svg" alt="megaphone">
																</span>
																<span class="text">
																	<span id="add_new_rule" class="data">New Rule</span>
																	<span class="label">Start from scratch.</span>
																</span>
															</label>
														</div>
													</div>
													<div class="btn-frame d-flex flex-wrap">
														<div class="btn-radio-custom d-flex flex-wrap" id="load_existing_rule">
															<input type="radio"  name="optimize" id="load-rule">
															<label for="load-rule"  class="d-flex flex-wrap align-items-center">
																<span class="icon-holder">
																	<img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-save-rounded.svg" alt="save">
																</span>
																<span class="text">
																	<span id="load_existing_rule" class="data">Load Rule</span>
																	<span class="label">Can be edited.</span>
																</span>
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group s2">
												<div class="select-holder s2" style="display:none">
													<select name="rule_template" onchange="setoptmizedvalue(this.value)" id="load_template_rule">
														<option value="" >Choose your rule template</option>
    													<option value="1"> Cost Per Click with Impressions </option>
                                                        <option value="2"> Cost per Click with Spent </option>
                                                        <option value="3"> Click Through Rate with Impressions </option>
                                                        <option value="4"> Click Through Rate with Spent </option>
                                                        <option value="5"> Lifetime Frequency Cap </option>
                                                        <!-- <option value="6"> Daily Frequency Cap </option> -->
                                                        <option value="7"> Relaxed Cost Per Result </option>
                                                        <option value="8"> Aggressive Cost Per Result </option>
                                                        <option value="9"> High Spend Low Results Notification </option>
													</select>
												</div>
											</div>
										<!--</form>-->
									</div>
									<div class="rules-section" style="display:none">
										<div class="rule-row">
											<div class="rule-header">
												<strong class="rule-title">Rule #1</strong>
											</div>
											<div class="rule-body">
												<div class="rule-frame d-flex flex-wrap">
													<div class="form-group">
														<div class="select-holder s3">
															<select name="rules[datapoint][]">
																 <option  value="" >Choose a data point</option>
																 <option value="impressions">Impression</option>
                                                                  <option value="unique_impressions">Unique Impressions</option>
                                                                  <option value="clicks">Clicks</option>
                                                                  <option value="unique_clicks">Unique Clicks</option>
                                                                  <option value="spent">Spent</option>
                                                                  <option value="result">Result</option>
                                                                  <option value="cpc">Cost Per Click</option>
                                                                  <option value="cpm">Cost Per Mille</option>
                                                                  <option value="ctr">Click Through Rate</option>
                                                                  <option value="cpa">Cost Per Action</option>
                                                                  
                                                                  
                                                                  <option value="reach">Reach</option>
                                                                  <option value="frequency">Frequency</option>
                                                                  <option value="cost_per_link_click">Cost Per Link Click</option>
                                                                  <option value="cost_per_initiate_checkout_fb">Cost Per Initiate Checkout</option>
                                                                  <option value="cost_per_purchase_fb">Cost Per Purchase</option>
                                                                  <option value="cost_per_add_to_cart_fb">Cost Per Add To Cart</option>
                                                                  <option value="cost_per_lead_fb">Cost Per Lead</option>
                                                                  <option value="cost_per_add_payment_info_fb">Cost Per Add Payment Info</option>
                                                                  <option value="cost_per_complete_registration_fb">Cost Per Complete Registration</option>
                                                                  <option value="cost_per_add_to_wishlist_fb">Cost Per Add To Wishlist</option>
                                                                  <option value="cost_per_search_fb">Cost Per Search</option>
                                                                  <option value="cost_per_view_content_fb">Cost Per View Content</option>
                                                                  <option value="cost_per_mobile_app_install">Cost Per Mobile App Install</option>
                                                                  <option value="cost_per">Cost Per Result</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<div class="select-holder s3">
															<select name="rules[operator][]">
																  <option  value="" >Choose an operator</option>
																  <option value="GREATER_THAN">Greater Than</option>
                                                                  <option value="LESS_THAN">Less Than</option>
                                                             <!-- <option value="EQUAL">Equal</option>
                                                                  <option value="NOT_EQUAL">Not Equal</option>
                                                                  <option value="IN">In</option>
                                                                  <option value="CONTAIN">Contain</option>
                                                                  <option value="ANY">Any</option>
                                                                  <option value="NONE">None</option> -->
																
															</select>
														</div>
													</div>
													<div class="form-group">
														<input class="form-control s1" name="rules[fieldvalue][]" type="number" placeholder="Enter a value">
													</div>
												</div>
											</div>
										</div>
										<div class="rule-row">
											<div class="rule-header">
												<strong class="rule-title">AND MUST MEET RULE #2</strong>
											</div>
											<div class="rule-body">
												<div class="rule-frame d-flex flex-wrap">
													<div class="form-group">
														<div class="select-holder s3">
															<select name="rules[datapoint][]">
																 <option  value="" >Choose a data point</option>
															     <option value="impressions">Impression</option>
                                                                  <option value="unique_impressions">Unique Impressions</option>
                                                                  <option value="clicks">Clicks</option>
                                                                  <option value="unique_clicks">Unique Clicks</option>
                                                                  <option value="spent">Spent</option>
                                                                  <option value="result">Result</option>
                                                                  <option value="cpc">Cost Per Click</option>
                                                                  <option value="cpm">Cost Per Mille</option>
                                                                  <option value="ctr">Click Through Rate</option>
                                                                  <option value="cpa">Cost Per Action</option>
                                                                  <option value="reach">Reach</option>
                                                                  <option value="frequency">Frequency</option>
                                                                  <option value="cost_per_link_click">Cost Per Link Click</option>
                                                                  <option value="cost_per_initiate_checkout_fb">Cost Per Initiate Checkout</option>
                                                                  <option value="cost_per_purchase_fb">Cost Per Purchase</option>
                                                                  <option value="cost_per_add_to_cart_fb">Cost Per Add To Cart</option>
                                                                  <option value="cost_per_lead_fb">Cost Per Lead</option>
                                                                  <option value="cost_per_add_payment_info_fb">Cost Per Add Payment Info</option>
                                                                  <option value="cost_per_complete_registration_fb">Cost Per Complete Registration</option>
                                                                  <option value="cost_per_add_to_wishlist_fb">Cost Per Add To Wishlist</option>
                                                                  <option value="cost_per_search_fb">Cost Per Search</option>
                                                                  <option value="cost_per_view_content_fb">Cost Per View Content</option>
                                                                  <option value="cost_per_mobile_app_install">Cost Per Mobile App Install</option>
                                                                  <option value="cost_per">Cost Per Result</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<div class="select-holder s3">
															<select name="rules[operator][]">
																<option  value="" >Choose an operator</option>
																<option value="GREATER_THAN">Greater Than</option>
                                                                  <option value="LESS_THAN">Less Than</option>
                                                            <!--  <option value="EQUAL">Equal</option>
                                                                  <option value="NOT_EQUAL">Not Equal</option>
                                                                  <option value="IN">In</option>
                                                                  <option value="CONTAIN">Contain</option>
                                                                  <option value="ANY">Any</option>
                                                                  <option value="NONE">None</option> -->
															</select>
														</div>
													</div>
													<div class="form-group">
														<input class="form-control s1" name="rules[fieldvalue][]" type="number" placeholder="Enter a value">
													</div>
												</div>
											</div>
										</div>
										<a href="#" class="btn-add-rule"><span class="icon-plus"></span></a>
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="goprev('4')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="gonext('4')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</main>
				<?php //}  ?>
				<?php //if($step == '5'){  ?>
				
				 <main id="main_5" style="display:none">
					    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Name</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Campaign</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Rule</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Action</strong>
						</div>
						<div class="step">
							<strong class="step-name">Freq</strong>
						</div>
					</div>
				</div>
				 <div  class="main-content s1" >
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Choose The Action You Want When The Rules Are Met.
										<span class="sub-heading">
											Choose the action you want to happen when the previous rule conditions are met.
											<strong class="highlight-text">Pro tip: the action will be applied to the individual ad not the campaign or adset.</strong>
										</span>
									</h2>
									<div class="form-wrap s2">
										<!--<form id="optimize_action_form" action="" class="form">-->
											<div class="form-group">
												<div class="select-holder s2">
												    
												 
												    
													<select name="actions" >
														<!--<option value="">Choose Your Action</option>-->
														<option value="PAUSE"> Pause Ad </option>
                                                    <option value="NOTIFICATION"> Notify Me </option>                                        
                                             <!-- <option value="ROTATE"> Rotate </option> -->
													</select>
													
													
													
												</div>
											</div>
										<!--</form>-->
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="goprev('5')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="gonext('5')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</main>
				<?php //} ?>
				
				<?php //if($step == '6'){  ?>
				
				 <main id="main_6" style="display:none">
				     	<div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Name</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Campaign</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Rule</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Action</strong>
						</div>
						<div class="step active">
							<strong class="step-name">Freq</strong>
						</div>
					</div>
				</div>
				<div  class="main-content s1" >
				    
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										How Often Do You Want To Check The Campaign?
										<span class="sub-heading">
											Specify how often you want TCM to check your campaign for the conditions mentioned.
											<strong class="highlight-text">Pro tip: campaigns with over $100/day generally choose hourly. </strong>
										</span>
									</h2>
									<div class="form-wrap s2">
										<!--<form id="frequency_form" action="" class="form">-->
											<div class="form-group">
												<div class="select-holder s2">
													<select name="frequency" >
														<!--<option value="">Choose Your Frequency</option>-->
													<option value="DAILY"> Every 24 Hours </option>
                                                    <option value="HOURLY"> Every 60 Minutes </option>
                                                    <option value="SEMI_HOURLY"> Every 30 Minutes </option>
													</select>
												</div>
											</div>
										<!--</form>-->
									</div>
								</div>
								<div class="btn-holder s2 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="goprev('6')" class="btn btn-prev">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-prev-pointer.svg" alt="pointer"></span>
											Previous Step
										</a>
									</div>
									<div class="btn-col">
										<a href="javascript:void(null);" onclick="submitform('add_rule','6')" class="btn btn-next">
											<span class="icon-holder"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-pointer.svg" alt="pointer"></span>
											Next Step
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</main>
				<?php //}  ?>
				
			
				
			
		</form>	
		
		
			<?php //if($step == '7'){  ?>
			<main id="main_7" style="display:none">
			    <div class="step-bar">
					<!--<div class="holder d-flex flex-wrap">-->
					<!--	<div class="step done">-->
					<!--		<strong class="step-name">Name</strong>-->
					<!--	</div>-->
					<!--	<div class="step done">-->
					<!--		<strong class="step-name">Campaign</strong>-->
					<!--	</div>-->
					<!--	<div class="step done">-->
					<!--		<strong class="step-name">Time</strong>-->
					<!--	</div>-->
					<!--	<div class="step done">-->
					<!--		<strong class="step-name">Rule</strong>-->
					<!--	</div>-->
					<!--	<div class="step done">-->
					<!--		<strong class="step-name">Action</strong>-->
					<!--	</div>-->
					<!--	<div class="step done">-->
					<!--		<strong class="step-name">Freq</strong>-->
					<!--	</div>-->
					<!--</div>-->
				</div>
				<div  class="main-content s1" >
				    	
					<div class="container">
						<div class="center-box s8">
							<div class="holder s1">
								<div class="frame">
									<h2 class="screen-heading text-center">
										Creating Your Optimization Rule Now…
										<span class="sub-heading s1">
											PLEASE DO NOT CLOSE THIS PAGE OR CLICK THE BACK BUTTON
										</span>
									</h2>
									
									
										<div  class="loading" style="margin:auto;width:400px">
										    	<div id="lottie" style="width:400px;height:300px"></div>
										    
										</div>
									
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				</main>
				<?php //}  ?>
		
			<?php //if($step == '8'){  ?>
					<main id="main_8" style="display:none">
					    <div class="step-bar">
					<div class="holder d-flex flex-wrap">
						<div class="step done">
							<strong class="step-name">Name</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Campaign</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Time</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Rule</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Action</strong>
						</div>
						<div class="step done">
							<strong class="step-name">Freq</strong>
						</div>
					</div>
				</div>
				<div  class="main-content s1" >
				    	
					<div class="container">
						<div class="center-box s10">
							<div class="holder s1">
								<div class="img-holder text-center">
									<img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-hat.svg" alt="hat">
								</div>
								<h2 class="screen-heading text-center">
									RULE CREATED SUCCESSFULLY!
									<span class="sub-heading">The Campaign Maker will now continously optimize your campaign!</span>
								</h2>
								<div class="btn-holder s3 text-center d-flex flex-wrap justify-content-center">
									<div class="btn-col">
										<a href="<?php echo base_url().'automaticoptimization/'.$addAccountId; ?>" class="btn btn-primary btn-lg">SEE YOUR ACTIVE RULES</a>
									</div>
									<div class="btn-col">
										<a href="<?php echo base_url().'automaticoptimization/createrule/'.$addAccountId; ?>" class="btn btn-primary btn-lg">CREATE ANOTHER RULE</a>
									</div>
								</div>
								<div class="rate-section text-center">
									<strong class="title">How Would You Rate Our Optimization Tool?</strong>
									<div class="rating-holder d-flex flex-row-reverse justify-content-center">
										<input value="5" name="rating" id="star5" type="radio">
										<label for="star5" title="5 stars"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-star-outline.svg" alt="star"></label>
										<input value="4" name="rating" id="star4" type="radio">
										<label for="star4" title="4 stars"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-star-outline.svg" alt="star"></label>
										<input value="3" name="rating" id="star3" type="radio">
										<label for="star3" title="3 stars"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-star-outline.svg" alt="star"></label>
										<input value="2" name="rating" id="star2" type="radio">
										<label for="star2" title="2 stars"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-star-outline.svg" alt="star"></label>
										<input value="1" name="rating" id="star1" type="radio">
										<label for="star1" title="1 star"><img src="<?php echo $this->config->item('assets'); ?>optimize/images/icon-star-outline.svg" alt="star"></label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</main>
				<?php //}  ?>
		
	</div>
	</div>
	
	
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<script src="<?php echo $this->config->item('assets'); ?>/optimize/js/jquery.main.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Montserrat:400,500,600']
			}
		});
		
		function submitform(formid,step){
		   
              var nextstep = parseInt(step)+1;
              var nextnextstep = nextstep +1;
              
    		    $('#main_'+step).hide();
                $('#main_'+nextstep).show();
		          animate_spinner();
		             
                 var form = $('#'+formid);
                

                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('/automaticoptimization/create_automaticoptimization'); ?>",
                    data: form.serialize(), 
                    dataType: "json",
                    success: function(data){
                    
                   setTimeout(function(){ 
                       
                       if(data.id !== undefined ){
                     $('#main_'+nextstep).hide();
                      $('#main_'+nextnextstep).show();
                   }else{
                       alert("Some Error Occured Please Try again .");
                        $('#main_'+nextstep).hide();
                        $('#main_'+step).show();
                       
                   }
                   
                   }, 10000);
                   
                   
                  		  
                    },
                    error: function() {
                        alert("Error posting feed.");
                    }
               });


		    
		    
		    
		    
		}
		
		function goprev(step){
		   
		     var nextstep = parseInt(step)-1;
		    $('#main_'+step).hide();
            $('#main_'+nextstep).show();
		}
		
		function gonext(step){
		    
		    if(step=='1'){
		        
		       var rulename =  $('input[name="ruleName"]').val();
		       
		       if(rulename ==""){
		           alert('Please Enter Rule Name');
		           return false;
		       }
		      
		    }
		    
		    if(step == '4'){
		       
		        var fiel4validation  = true;
		          $('.rule-row').each(function(){
		        
                   var datapoint = $(this).find('select[name="rules[datapoint][]"]').val();
        		   var operator  = $(this).find('select[name="rules[operator][]"]').val();    
        		   var fieldvalue = $(this).find('input[name="rules[fieldvalue][]"]').val();  
        		   
        		   if(datapoint== "" || operator=="" || fieldvalue == "" ){
        		       
        		       fiel4validation  = false;
        		   }
		        
		        
		        
		    });
		    
		    if(fiel4validation == false){
		            
		        alert('Please check rule any Parameter is missing');
		        return false;
		        }
		        
		        
		    }
		    
		    var nextstep = parseInt(step)+1;
		    $('#main_'+step).hide();
            $('#main_'+nextstep).show();
		}
		
		
		
		
		
		
		
		$('#load_existing_rule').click(function(){
		    
		     $('select[name="rule_template"]').parent().show();
             $('.rules-section').show();
		   
		});
		
		
		$('#add_new_rule').click(function(){
		    
		    $('.rule-row').each(function(){
		        
           $(this).find('select[name="rules[datapoint][]"]').val('');
		   $(this).find('select[name="rules[operator][]"]').val('');    
		   $(this).find('input[name="rules[fieldvalue][]"]').val('');  
		    
		    });
		    
		     $('select[name="rule_template"]').parent().hide();
            $('.rules-section').show();
		    
		    
		});
		
	
	
	   $('#load_template_rule').change(function(){
	       
	       var index = $(this).val();
	       var data = {"rule":{
"1":{
"1":{"datapoint":"impressions","type":"GREATER_THAN","fieldvalue":1000},
"2":{"datapoint":"cost_per_link_click","type":"GREATER_THAN","fieldvalue":0.50}
},
"2":{
"1":{"datapoint":"spent","type":"GREATER_THAN","fieldvalue":10},
"2":{"datapoint":"cost_per_link_click","type":"GREATER_THAN","fieldvalue":0.50}
},

"3":{
"1":{"datapoint":"impressions","type":"GREATER_THAN","fieldvalue":1000},
"2":{"datapoint":"ctr","type":"LESS_THAN","fieldvalue":1.5}
},
"4":{
"1":{"datapoint":"spent","type":"GREATER_THAN","fieldvalue":10},
"2":{"datapoint":"ctr","type":"LESS_THAN","fieldvalue":1.5}
},
"5":{
"1":{"datapoint":"frequency","type":"GREATER_THAN","fieldvalue":5},
"2":{"datapoint":"spent","type":"GREATER_THAN","fieldvalue":1},
},
"6":{
"1":{"datapoint":"frequency","type":"GREATER_THAN","fieldvalue":3},
"1":{"datapoint":"spent","type":"GREATER_THAN","fieldvalue":1},
},
"7":{
"1":{"datapoint":"cost_per","type":"GREATER_THAN","fieldvalue":10},
"2":{"datapoint":"impressions","type":"GREATER_THAN","fieldvalue":1000}
},
"8":{
"1":{"datapoint":"cost_per","type":"GREATER_THAN","fieldvalue":10},
"2":{"datapoint":"impressions","type":"GREATER_THAN","fieldvalue":1000}
},
"9":{
"1":{"datapoint":"cost_per","type":"GREATER_THAN","fieldvalue":20},
"2":{"datapoint":"spent","type":"GREATER_THAN","fieldvalue":1000}
}
}};



	       
	       var servers =  data['rule'][index];

 $.each(servers, function(key, value) {
      // alert(value.type);
        if(key == "1"){
           $('.rule-row:eq(0)').find('select[name="rules[datapoint][]"]').val(value.datapoint);
		   $('.rule-row:eq(0)').find('select[name="rules[operator][]"]').val(value.type);    
		   $('.rule-row:eq(0)').find('input[name="rules[fieldvalue][]"]').val(value.fieldvalue);
        }
        if(key == "2"){
           $('.rule-row:eq(1)').find('select[name="rules[datapoint][]"]').val(value.datapoint);
		   $('.rule-row:eq(1)').find('select[name="rules[operator][]"]').val(value.type);    
		   $('.rule-row:eq(1)').find('input[name="rules[fieldvalue][]"]').val(value.fieldvalue);
        }
     
 });
	       
	   });

		
	</script>
	
	
	<script>
	var params = {
        container: document.getElementById('lottie'),
        renderer: 'html',
        loop: true,
        autoplay: true,
        animationData: animationData
    };

    var anim;
 
    anim = lottie.loadAnimation(params);
//   function animate_spinner(){
//     var params = {
//         container: document.getElementById('lottie'),
//         renderer: 'html',
//         loop: true,
//         autoplay: true,
//         animationData: animationData
//     };

//     var anim;

//     anim = lottie.loadAnimation(params);
//  }
</script>
	
