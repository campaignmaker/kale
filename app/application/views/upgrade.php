<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
    <div id="preloaderMain" style="display: none;">
                <div id="statusMain"><i class="fa fa-spinner fa-spin"></i></div>
                <div id="lastStepMess" style="display: none;">Please do not close this page or click the back button while the campaign is uploading</div>
            </div>
        <div class="trial-body">
    <div class="left-trial">
      <div class="trial-quote">
        <div class="trial-quote-title">YOU’RE IN GOOD HANDS</div>
        <div class="trial-quote-text">I want to reiterate this software is awesome! In March it helped me achieve a ROAS of 321% increase and I'm closing out April at 600% on my ecommerce store. No, I'm not shitting you! I'm seriously impressed.</div>
        <div class="trial-quote-name">LANE W.<span>Shoe Conspiracy</span></div>
      </div>
      <div class="trial-quote small-quote">
        <div class="trial-quote-title">DAILY QUOTE</div>
        <div class="trial-quote-text">If you do what you’ve always done, you’ll get what you’ve always gotten. </div>
        <div class="trial-quote-name">TONY ROBBINS<span>Speaker</span></div>
      </div>
      <div class="trial-quote small-quote">
        <div class="trial-quote-title">YOUR ACCOUNT MANAGER</div>
        <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/trial-photo.jpg" alt="" class="trial-quote-photo">
        <div class="trial-quote-name">SAMY <br>ZABARAH<span>Facebook Ads Expert <br>Since 2011</span></div>
        <div class="trial-quote-a">
          <a href="mailto:samy@thecampaignmaker.com">Email Me</a><a href="skype:facebook.adz?add">Skype Me</a>
        </div>
      </div>
    </div>
    <div class="right-trial">
      <div class="activate-title">Reactivate Your Account</div>
      <div class="top-notification-box" id="successmsgBx1" style="display: none;">
                      <i class="icon-warning"></i>
                      <span class="text payment-errors"></span>
                    </div>
      <form class="form-activate" action="<?php echo base_url()."/upgrade/processpayment"; ?>" method="POST" id="stripe-payment-form">
          <input type="hidden" name="idev_custom" id="idev_custom_x21" />
<script type="text/javascript" src="https://thecampaignmaker.com/aff/connect/stripe_ip.php"></script>
        <div class="card-info">
          <label class="card-number valid">
            <span class="label">Credit Card Number</span>
            <input type="text" class="card-numbertxt" autocomplete="off">
          </label>
          <div class="label-div">
            <span class="label">Expiry Date</span>
            <div class="card-date">
              <select class="card-expiry-month">
                <option value="1">01</option>
                <option value="2">02</option>
                <option value="3">03</option>
                <option value="4">04</option>
                <option value="5">05</option>
                <option value="6">06</option>
                <option value="7">07</option>
                <option value="8">08</option>
                <option value="9">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
            <div class="card-date">
              <select class="card-expiry-year">
                <option value="2017">2017</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
                <option value="2027">2027</option>
                
              </select>
            </div>
          </div>
          <label class="card-code">
            <span class="label">CVV Code</span>
            <input type="text" autocomplete="off" class="card-cvc">
          </label>
        </div>

        <div class="radio-wrap">
          <div class="radio-title">Choose your account type</div>
          <div class="radio">
            <input type="radio" id="r1" name="selectedplan" value="UpgradeMonthly" <?php if($_GET['plan'] == "monthly" || $_GET['plan'] == "") echo 'checked="checked"'; ?> />
            <label for="r1"><span class="span-radio"></span>Monthly Account ($39/Month)<span class="free">You will be charged $39 per month.</span></label>
          </div>
          <div class="radio">
            <input type="radio" id="r2" name="selectedplan" value="UpgradeYearly" <?php if($_GET['plan'] == "yearly") echo 'checked="checked"'; ?> />
            <label for="r2"><span class="span-radio"></span>Annual Account ($19/Month)<span class="free">You will be charged $228 per year.</span></label>
          </div>
        </div>
        <input name="action" value="stripe" type="hidden">
                        <input name="redirect" value="" type="hidden">
                        <input name="amount" value="NDk=" type="hidden">
                        <input name="stripe_nonce" value="0dd70c1b2d" type="hidden">
                      
        <div class="card-button">
          <button type="submit" id="stripe-submit">ACTIVATE MY ACCOUNT</button>
        </div>
      </form>
      <div class="bottom-stripe">
        <p>By signing up you agree to our Terms of Service</p>
        <div class="poweredby"></div>
      </div>
    </div>
  </div>
<!-- Main Content Starts -->
     <!-- main id="main">
        <div class="help-page-holder">
          <div class="container">
            <div class="settings-box">
              <div class="setting-header text-center">
                <h2>ENTER YOUR CREDIT CARD INFO TO ACTIVATE TRIAL!</h2>
              </div>
              <div class="help-videos-holder">
                	<form action="<?php //echo base_url()."/trialactivate/processpayment"; ?>" method="POST" id="stripe-payment-form">
                        <div class="form-row">
                            <label>Card Number</label>
                            <input size="20" autocomplete="off" class="card-number" type="text" style="color:#000;">
                        </div>
                        <div class="form-row">
                            <label>CVC</label>
                            <input size="4" autocomplete="off" class="card-cvc" type="text" style="color:#000;">
                        </div>
                        <div class="form-row">
                            <label>Expiration (MM/YYYY)</label>
                            <input size="2" class="card-expiry-month" type="text" style="color:#000;">
                            <span> / </span>
                            <input size="4" class="card-expiry-year" type="text" style="color:#000;">
                        </div>
                                    <div class="form-row" style="display:none;">
                            <label>Payment Type:</label>
                            <input name="recurring" value="no"  type="radio"><span>One time payment</span>
                            <input name="recurring" value="yes" checked="checked" type="radio"><span>Recurring monthly payment</span>
                        </div>
                                    <input name="action" value="stripe" type="hidden">
                        <input name="redirect" value="" type="hidden">
                        <input name="amount" value="NDk=" type="hidden">
                        <input name="stripe_nonce" value="0dd70c1b2d" type="hidden">
                        <button type="submit" id="stripe-submit" class="btn btn-success">Submit Payment</button>
                    </form>
                    <div class="payment-errors"></div>
              </div>
            </div>
          </div>
        </div>
      </main>-->
      <!-- Main Content Ends -->
    </div>
<script>
Stripe.setPublishableKey("pk_live_HA0omoOtinEyA28gk3DqVoMK");

function stripeResponseHandler(status, response) {

    if (response.error) {

    // show errors returned by Stripe

		jQuery("#successmsgBx1").show();
        jQuery(".payment-errors").html(response.error.message);

    // re-enable the submit button

    jQuery('#stripe-submit').attr("disabled", false);

    } else {
		jQuery("#successmsgBx1").hide();

        var form$ = jQuery("#stripe-payment-form");
        var url = form$.attr( 'action' );

        // token contains id, last4, and card type

        var token = response['id'];

        // insert the token into the form so it gets submitted to the server

        form$.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
        jQuery("#preloaderMain").show();
        // and submit
        jQuery.ajax({
           type: "POST",
           url: url,
           data: jQuery("#stripe-payment-form").serialize(), // serializes the form's elements.
           success: function(data)
           {
               setTimeout(function () { window.location.href = "<?php echo base_url().'login/refreshplan'; ?>" }, 20000);
               //alert("junaid"); // show response from the php script.
           },
           error: function(jqXHR, textStatus, errorThrown) {
                //alert("Nouman");
                setTimeout(function () { window.location.href = "<?php echo base_url().'login/refreshplan'; ?>" }, 20000);
            }
         });
        return false;
        //form$.get(0).submit();

    }

}

jQuery(document).ready(function($) {

  $("#stripe-payment-form").submit(function(event) {

    // disable the submit button to prevent repeated clicks

    $('#stripe-submit').attr("disabled", "disabled");


    // send the card details to Stripe

    Stripe.createToken({

      number: $('.card-numbertxt').val(),

      cvc: $('.card-cvc').val(),

      exp_month: $('.card-expiry-month').val(),

      exp_year: $('.card-expiry-year').val()

    }, stripeResponseHandler);



    // prevent the form from submitting with the default action

    return false;

  });

});
</script>    