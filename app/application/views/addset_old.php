<!-- Main Content Starts -->
<main id="main">
<style>
      .jcf-select-drop{
          width:200px !important;
      }
      
      </style>
    <div class="dashboard-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().'reports'; ?>">Dashboard</a></li>
            <li>
              <?php if (isset($adaccountsCount) && $adaccountsCount == 1) { ?>
              <a href="<?php echo base_url().'reports/'.$addAccountId; ?>"><?php echo substr($adaccounts['addset_header']['accountName'], 0, 19); ?></a>
              <?php } else { ?>
              <?php echo substr($adaccounts['addset_header']['accountName'], 0, 19); ?>
              <?php } ?>
            </li>
            <li class="active">
              <?php
                  echo "<a href='javascript:void(0)'>". substr($campaignName, 0, 19) . " </a>";
              ?>
            </li>
        </ol>
        <?php if (isset($adaccountsCount) && $adaccountsCount == 1) { ?>
            <a class="btn btn-date" id="reportrange">
              <strong class="ico-holder"><i class="icon-calendar"></i></strong>
              <span class="text range-data">
                <select class="form-control tcmreportdd" name="changeDateRange" onchange="getAddSets('<?php echo $addAccountId; ?>', '<?php echo $campaignId; ?>', this.value)">
                 
                  <option value="today" <?php if($limit == 'today') echo 'selected'; ?>>Today</option>
                      <option value="yesterday" <?php if($limit == 'yesterday') echo 'selected'; ?>>Yesterday</option>
                      <option value="last_3d" <?php if($limit == 'last_3d') echo 'selected'; ?>>Last 3 Days</option>
                      <option value="last_7d" <?php if($limit == 'last_7d') echo 'selected'; ?>>Last 7 Days</option>
                      <option value="last_14d" <?php if($limit == 'last_14d') echo 'selected'; ?>>Last 14 Days</option>
                      <option value="last_28d" <?php if($limit == 'last_28d') echo 'selected'; ?>>Last 28 Days</option>
                      <option value="last_30d" <?php if($limit == 'last_30d') echo 'selected'; ?>>Last 30 Days</option>
                      <option value="last_90d" <?php if($limit == 'last_90d') echo 'selected'; ?>>Last 90 Days</option>
                      <option value="this_month" <?php if($limit == 'this_month') echo 'selected'; ?>>This Month</option> 
                      <option value="last_month" <?php if($limit == 'last_month') echo 'selected'; ?>>Last Month</option>
                      <option value="this_quarter" <?php if($limit == 'this_quarter') echo 'selected'; ?>>This Quarter</option>                      
                      <option value="this_year" <?php if($limit == 'this_month') echo 'selected'; ?>>This Year</option> 
                      <option value="last_year" <?php if($limit == 'last_month') echo 'selected'; ?>>Last Year</option>
                      <option value="lifetime" <?php if($limit == 'lifetime') echo 'selected'; ?>>Lifetime</option>
                </select>
              </span>
            </a>
            <style>
        						.jcf-select-tcmreportdd{
					width:auto !important;
				}
                </style>
        <?php } ?>
    </div>
    <!-- END PAGE HEADER-->
    <?php if (isset($adaccountsCount) && $adaccountsCount == 1) { ?>
        <!-- BEGIN DASHBOARD STATS -->
        <div class="page-content campaigns">
            


                                  <!--Content-->

<div class="content">
    <div class="row">
        <div class="container-fluid">

        <!--Account-->

        <div class="menu account">

            <!--First row-->

            <div class="col-sm-4 col-md-3 col-lg-55">
                <div class="panel panel-default" id="heading1">
                    <div class="panel-heading" role="tab" >
                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#clkWeb" aria-expanded="true" aria-controls="clkWeb">
                            <h4 class="panel-title">
                                CLICKS TO WEBSITE
                                <span class="glyphicon" aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="clkWeb" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="heading1">
                        <div class="panel-body">
                            <ul>
                                <li><?php echo number_format($adaccounts['addset_header']['inline_link_clicks']); ?></li>
                                <li><span class="text-bold"> <?php
                            if ($adaccounts['addset_header']['spent'] > 0)
                            {
                                $cost_per_click = $adaccounts['addset_header']['spent'] / $adaccounts['addset_header']['inline_link_clicks'];
                                if ($cost_per_click > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . round(number_format($cost_per_click, 3, '.', ','),2);
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . round(number_format($cost_per_click),2);
                                }
                            }
                            else
                            {
                                echo $this->session->userdata('cur_currency') . $adaccounts['addset_header']['inline_link_clicks'];
                            }
                            ?> </span> <span class="text-muted"> PER CLICK </span> </li>
                                <li><span class="text-bold" id="clickrate"> <?php if ($adaccounts['addset_header']['impressions'] > 0)
                            {
                                $click_rate = ($adaccounts['addset_header']['inline_link_clicks'] / $adaccounts['addset_header']['impressions'])*100;
                                if ($click_rate > 0)
                                {
                                    echo round(number_format($click_rate, 3, '.', ','),2)."%";
                                }
                            }
                             ?> </span> <span class="text-muted"> Click rate </span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 col-lg-55">
                <div class="panel panel-default" id="heading2">
                    <div class="panel-heading" role="tab" >
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#webConversion" aria-expanded="false" aria-controls="webConversion">
                            <h4 class="panel-title">
                                WEBSITE CONVERSIONS
                                <span class="glyphicon" ria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>


                    <div id="webConversion" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="heading2">
                        <div class="panel-body">
                            <ul>
                                <li><?php echo number_format($adaccounts['addset_header']['actions']); ?></li>
                                <li><span class="text-bold"> 
                                <?php
                                if ($adaccounts['addset_header']['cost_per_total_action'] > 0)
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($adaccounts['addset_header']['cost_per_total_action'], 2, '.', ',');
                                }
                                else
                                {
                                    echo $this->session->userdata('cur_currency') . number_format($adaccounts['addset_header']['cost_per_total_action']);
                                }
                            ?> </span> <span class="text-muted"> PER CONVERSION </span></li>
                                <li><span class="text-bold"> <?php if ($adaccounts['addset_header']['impressions'] > 0)
                            {
                                $web_conv = ($adaccounts['addset_header']['actions'] / $adaccounts['addset_header']['impressions'])*100;
                                if ($web_conv > 0)
                                {
                                    echo round(number_format($web_conv, 3, '.', ','),2)."%";
                                }
                            }
                             ?> </span> <span class="text-muted">CONVERSION RATE</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 col-lg-55">
                <div class="panel panel-default" id="heading3">
                    <div class="panel-heading" role="tab" >
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#totalSpent" aria-expanded="false" aria-controls="totalSpent">
                            <h4 class="panel-title">
                                TOTAL SPENT
                                <span class="glyphicon " aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="totalSpent" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="heading3">
                        <div class="panel-body">
                            <ul>
                                <li><?php
                          if ($adaccounts['addset_header']['spent'] > 0) {
                              echo $this->session->userdata('cur_currency') . number_format($adaccounts['addset_header']['spent'], 2, '.', ',');
                          } else {
                              echo $this->session->userdata('cur_currency') . number_format($adaccounts['addset_header']['spent']);
                          }
                          ?></li>
                                <!-- <li> <span class="text-bold"> --> <?php
                          /*if ($adaccounts['addset_header']['todayspend'] > 0) {
                              echo $this->session->userdata('cur_currency') . number_format($adaccounts['addset_header']['todayspend'], 2, '.', ',');
                          } else {
                              echo $this->session->userdata('cur_currency') . number_format($adaccounts['addset_header']['todayspend']);
                          }*/
                          ?> <!-- </span> <span class="text-muted"></span></li>
                                <li><span class="text-bold"> --> <?php
                         /* if ($adaccounts['addset_header']['sevendayspend'] > 0) {
                              echo $this->session->userdata('cur_currency') . number_format($adaccounts['addset_header']['sevendayspend'], 2, '.', ',');
                          } else {
                              echo $this->session->userdata('cur_currency') . number_format($adaccounts['addset_header']['sevendayspend']);
                          }*/
                          ?> <!-- </span> <span class="text-muted"></span></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm"></div>

            <div class="col-sm-4 col-md-3 col-lg-55">
                <div class="panel panel-default" id="heading4">
                    <div class="panel-heading" role="tab">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#imperssion" aria-expanded="false" aria-controls="imperssion">
                            <h4 class="panel-title">
                                IMPRESSIONS
                                <span class="glyphicon " aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="imperssion" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="heading4">
                        <div class="panel-body">
                            <ul>
                                <li> <?php
                            echo number_format($adaccounts['addset_header']['impressions']);
                          ?>
                        </li>
                                <li><span class="text-bold"> $<?php
                            //echo number_format($adaccounts['addset_header']['cpm']);

                                                if($adaccounts['addset_header']['impressions'] != '0'){
                $subcpc = $adaccounts['addset_header']['impressions']/1000;
                echo round($adaccounts['addset_header']['spent']/$subcpc,2);
              }
                          ?>
                             </span> <span class="text-muted"> PER 1k IMPRESSIONS </span> </li>
                                <li><span class="text-bold"> <?php echo number_format($adaccounts['addset_header']['newFrequ'], 2); ?> </span> <span class="text-muted"> FREQUENCY RATE </span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-md"></div>

            <div class="col-sm-4 col-md-3 col-lg-55">
                <div class="panel panel-default" id="heading5">
                    <div class="panel-heading" role="tab">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#postEng" aria-expanded="false" aria-controls="postEng">
                            <h4 class="panel-title">
                                POST ENGAGEMENTS
                                <span class="glyphicon " aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="postEng" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="heading5">
                        <div class="panel-body">
                            <ul>
                                <li><?php echo number_format($adaccounts['addset_header']['inline_post_engagement']); ?></li>
                                <li><span class="text-bold"><?php
                                echo $this->session->userdata('cur_currency').round(($adaccounts['addset_header']['spent']/$adaccounts['addset_header']['inline_post_engagement']),2);
                            ?> </span> <span class="text-muted">PER ENGAGEMENT</span></li>
                                <li><span class="text-bold"><?php if ($adaccounts['addset_header']['impressions'] > 0)
                            {
                                $inline_posteng = ($adaccounts['addset_header']['inline_post_engagement'] / $adaccounts['addset_header']['impressions'])*100;
                                if ($inline_posteng > 0)
                                {
                                    echo round(number_format($inline_posteng, 3, '.', ','),2)."%";
                                }
                            }
                             ?> </span> <span class="text-muted">Engagement Rate</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-lg"></div>

            <!--End First row-->


            <!--Second row-->

            <div class="col-sm-4 col-md-3 col-lg-55">
                <div class="panel panel-default" id="heading6">
                    <div class="panel-heading" role="tab" >
                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#likes" aria-expanded="true" aria-controls="likes">
                            <h4 class="panel-title">
                                page likes
                                <span class="glyphicon glyphicon-triangle-bottom pull-right" aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="likes" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading6">
                        <div class="panel-body">
                            <ul>
                                <li><?php echo number_format($adaccounts['addset_header']['page_like']); ?></li>
                                <li><span class="text-bold"><?php
                                echo $this->session->userdata('cur_currency').round(($adaccounts['addset_header']['spent']/$adaccounts['addset_header']['page_like']), 2);
                                ?></span> <span class="text-muted">PER PAGE LIKE</span></li>
                                <li><span class="text-bold"><?php if ($adaccounts['addset_header']['impressions'] > 0)
                            {
                                $page_like = ($adaccounts['addset_header']['page_like'] / $adaccounts['addset_header']['impressions'])*100;
                                if ($page_like > 0)
                                {
                                    echo round(number_format($page_like, 3, '.', ','),2)."%";
                                }
                            }
                             ?></span> <span class="text-muted"> PAGE LIKE RATE</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm"></div>

            <div class="col-sm-4 col-md-3 col-lg-55">
                <div class="panel panel-default" id="heading7">
                    <div class="panel-heading" role="tab" >
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#facebookLeads" aria-expanded="false" aria-controls="facebookLeads">
                            <h4 class="panel-title">
                                FACEBOOK LEADS
                                <span class="glyphicon glyphicon-triangle-bottom pull-right" aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="facebookLeads" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                        <div class="panel-body">
                            <ul>
                                <li><?php echo number_format($adaccounts['addset_header']['leadgenother']); ?></li>
                                <li><span class="text-bold"><?php
                                echo $this->session->userdata('cur_currency').round(($adaccounts['addset_header']['spent']/$adaccounts['addset_header']['leadgenother']),2);
                            ?> </span> <span class="text-muted">PER LEAD </span></li>
                                <li><span class="text-bold"><?php if ($adaccounts['addset_header']['impressions'] > 0)
                            {
                                $leadgenother = ($adaccounts['addset_header']['leadgenother'] / $adaccounts['addset_header']['impressions'])*100;
                                if ($leadgenother > 0)
                                {
                                    echo round(number_format($leadgenother, 3, '.', ','),2)."%";
                                }
                else{
                  echo "0%";
                }
                            }
                             ?> </span> <span class="text-muted"> LEAD RATE </span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 col-lg-55">
                <div class="panel panel-default" id="heading8">
                    <div class="panel-heading" role="tab" >
                        <a class="collapsed" role="button" data-toggle="collapse"
                           data-parent="#accordion"
                           href="#appInstall" aria-expanded="false" aria-controls="appInstall">
                            <h4 class="panel-title">
                                APP INSTALLS
                                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="appInstall" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading8">
                        <div class="panel-body">
                            <ul>
                                <li><?php echo number_format($adaccounts['addset_header']['app_install']); ?></li>
                                <li><span class="text-bold"><?php
                                echo $this->session->userdata('cur_currency').round(($adaccounts['addset_header']['spent']/$adaccounts['addset_header']['app_install']),2);
                            ?> </span> <span class="text-muted"> PER APP INSTALL </span></li>
                                <li><span class="text-bold"><?php if ($adaccounts['addset_header']['impressions'] > 0)
                            {
                                $app_install = ($adaccounts['addset_header']['app_install'] / $adaccounts['addset_header']['impressions'])*100;
                                if ($app_install > 0)
                                {
                                    echo round(number_format($app_install, 3, '.', ','),2)."%";
                                }
                else{
                  echo "0%";
                }
                            }
                             ?> </span> <span class="text-muted">  INSTALL RATE </span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-md"></div>

            <div class="col-sm-4 col-md-3 col-md-offset-3 col-lg-offset-0 col-lg-55">
                <div class="panel panel-default" id="heading9">
                    <div class="panel-heading" role="tab">
                        <a class="collapsed" role="button" data-toggle="collapse"
                           data-parent="#accordion"
                           href="#videoViews" aria-expanded="false" aria-controls="videoViews">
                            <h4 class="panel-title">
                                10 SECOND VIDEO VIEWS
                                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="videoViews" class="panel-collapse collapse"
                         role="tabpanel" aria-labelledby="heading9">
                        <div class="panel-body">
                            <ul>
                                <li><?php echo number_format($adaccounts['addset_header']['video_10_sec_watched_actions']); ?></li>
                                <li><span class="text-bold"><?php
                                echo $this->session->userdata('cur_currency').round(($adaccounts['addset_header']['spent']/$adaccounts['addset_header']['video_10_sec_watched_actions']),2);
                            ?> </span> <span class="text-muted"> PER 10s VIEW </span></li>
                                <li><span class="text-bold"><?php echo round(number_format($adaccounts['addset_header']['video_avg_percent_watched_actions'], 3, '.', ','),2)."%"; ?> </span> <span class="text-muted"> AVG % WATCHED </span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm"></div>

            <div class="col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-0 col-lg-55">
                <div class="panel panel-default" id="heading10">
                    <div class="panel-heading" role="tab">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#oflline" aria-expanded="false" aria-controls="oflline">
                            <h4 class="panel-title">
                                OFFLINE CONVERSIONS
                                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="oflline" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading10">
                        <div class="panel-body">
                            <ul>
                                <li><?php echo number_format($adaccounts['addset_header']['offline_conversion']); ?></li>
                                <li><span class="text-bold"><?php
                                echo $this->session->userdata('cur_currency').round(($adaccounts['addset_header']['spent']/$adaccounts['addset_header']['offline_conversion']),2);
                            ?></span> <span class="text-muted">PER CONVERSION </span></li>
                                <li><span class="text-bold"><?php if ($adaccounts['addset_header']['impressions'] > 0)
                            {
                                $offline_conversion = ($adaccounts['addset_header']['offline_conversion'] / $adaccounts['addset_header']['impressions'])*100;
                                if ($offline_conversion > 0)
                                {
                                    echo round(number_format($offline_conversion, 3, '.', ','),2)."%";
                                }
                else{
                  echo "0%";
                }
                            }
                             ?></span> <span class="text-muted">PAGE LIKE RATE </span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <!--End second row-->

            <!--Third-->
            <div class="col-sm-4 col-md-3">
              <div class="data-content graph-data country-divvar">
                      
                    </div>
                <div class="panel panel-default ddsection" id="heading11">
                    <div class="panel-heading" role="tab" >
                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#locations" aria-expanded="true" aria-controls="locations">
                            <h4 class="panel-title text-left">
                                LOCATIONS <span class="text-lowercase">by</span>
                                <span class="glyphicon glyphicon-triangle-bottom pull-right" aria-hidden="true"></span>
                            </h4>
                        </a>
                        <select class="form-control jcf-hidden" name="locationdd" onchange="getAddSetCountryChartvarious(this.value)">
                      
                      <option value="clicks" selected="">Clicks</option>
                      <option value="impressions">Impressions</option>
                      <option value="spend">Total Spent</option>
                      <option value="page_engagement">Engagement</option>
                      <option value="like">Page Like</option>
                      <option value="ctr">CTR</option>
                      <option value="offsite_conversion">Conversion</option>
                      
                      <option value="offsite_conversion.fb_pixel_add_payment_info">Adds Payment Info</option>
                      <option value="offsite_conversion.fb_pixel_add_to_cart">Adds To Cart</option>
                      <option value="offsite_conversion.fb_pixel_add_to_wishlist">Adds To Wishlist</option>
                      <option value="offsite_conversion.fb_pixel_complete_registration">Completed Registration</option>
                      <option value="offsite_conversion.fb_pixel_custom">Custom pixel events defined by the advertiser</option>
                      <option value="offsite_conversion.fb_pixel_initiate_checkout">Initiates Checkout</option>
                      <option value="offsite_conversion.fb_pixel_lead">Leads</option>
                      <option value="offsite_conversion.fb_pixel_purchase">Purchases</option>
                      <option value="offsite_conversion.fb_pixel_search">Search</option>
                      <option value="offsite_conversion.fb_pixel_view_content">Views Content</option>
                      <option value="offsite_conversion.key_page_view">Key Page Views</option>
                      <option value="offsite_conversion.lead">Offsite Conversion Leads</option>
                      <option value="offsite_conversion.other">Other Website Conversions</option>
                      <option value="offsite_conversion.registration">Registrations</option>
                      
                    </select>
                    </div>
                    
                    <div id="locations" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading11">
                        <div class="panel-body">
                            <ul id="locationmain">
                                <li><?php if(!empty($adaccounts['country_total'])) echo $adaccounts['country_total'];?></li>
                                <?php if(!empty($adaccounts['country_detail'])) echo $adaccounts['country_detail'];?>
                               
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
            <div class="data-content graph-data gender-divvar">
                      
                    </div>
                <div class="panel panel-default ddsection" id="heading12">
                    <div class="panel-heading" role="tab" >
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#gender" aria-expanded="false" aria-controls="gender">
                            <h4 class="panel-title text-left" style="padding-left: 61px;">
                                GENDER <span class="text-lowercase">by</span>
                                <span class="glyphicon glyphicon-triangle-bottom pull-right" aria-hidden="true"></span>
                            </h4>
                        </a>
                        <select class="form-control jcf-hidden" name="genderdd" onchange="getAddSetGendervarious(this.value)">
                      
                      <option value="clicks" selected="">Clicks</option>
                      <option value="impressions">Impressions</option>
                      <option value="spend">Total Spent</option>
                      <option value="page_engagement">Engagement</option>
                      <option value="like">Page Like</option>
                      <option value="ctr">CTR</option>
                      <option value="offsite_conversion">Conversion</option>
                      
                      <option value="offsite_conversion.fb_pixel_add_payment_info">Adds Payment Info</option>
                      <option value="offsite_conversion.fb_pixel_add_to_cart">Adds To Cart</option>
                      <option value="offsite_conversion.fb_pixel_add_to_wishlist">Adds To Wishlist</option>
                      <option value="offsite_conversion.fb_pixel_complete_registration">Completed Registration</option>
                      <option value="offsite_conversion.fb_pixel_custom">Custom pixel events defined by the advertiser</option>
                      <option value="offsite_conversion.fb_pixel_initiate_checkout">Initiates Checkout</option>
                      <option value="offsite_conversion.fb_pixel_lead">Leads</option>
                      <option value="offsite_conversion.fb_pixel_purchase">Purchases</option>
                      <option value="offsite_conversion.fb_pixel_search">Search</option>
                      <option value="offsite_conversion.fb_pixel_view_content">Views Content</option>
                      <option value="offsite_conversion.key_page_view">Key Page Views</option>
                      <option value="offsite_conversion.lead">Offsite Conversion Leads</option>
                      <option value="offsite_conversion.other">Other Website Conversions</option>
                      <option value="offsite_conversion.registration">Registrations</option>
                      
                    </select>
                    </div>
                    <div id="gender" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading12">
                        <div class="panel-body">
                            <ul id="gendermain">
                                <li><?php if(!empty($adaccounts['gender_total'])) echo $adaccounts['gender_total'];?></li>
                                <?php if(!empty($adaccounts['gender_detail'])) echo $adaccounts['gender_detail'];?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
            <div class="data-content graph-data agegroup-divvar">
                      
                    </div>
                <div class="panel panel-default ddsection" id="heading13">
                    <div class="panel-heading" role="tab" >
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#ageGroup" aria-expanded="false" aria-controls="ageGroup">
                            <h4 class="panel-title text-left" style="padding-left: 27px;">
                                AGE GROUPS <span class="text-lowercase">by</span>
                                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                            </h4>
                        </a>
                        <select class="form-control jcf-hidden" name="agegroupdd" onchange="getAddSetChartvarious(this.value)">
                      
                      <option value="clicks" selected="">Clicks</option>
                      <option value="impressions">Impressions</option>
                      <option value="spend">Total Spent</option>
                      <option value="page_engagement">Engagement</option>
                      <option value="like">Page Like</option>
                      <option value="ctr">CTR</option>
                      <option value="offsite_conversion">Conversion</option>
                      
                      <option value="offsite_conversion.fb_pixel_add_payment_info">Adds Payment Info</option>
                      <option value="offsite_conversion.fb_pixel_add_to_cart">Adds To Cart</option>
                      <option value="offsite_conversion.fb_pixel_add_to_wishlist">Adds To Wishlist</option>
                      <option value="offsite_conversion.fb_pixel_complete_registration">Completed Registration</option>
                      <option value="offsite_conversion.fb_pixel_custom">Custom pixel events defined by the advertiser</option>
                      <option value="offsite_conversion.fb_pixel_initiate_checkout">Initiates Checkout</option>
                      <option value="offsite_conversion.fb_pixel_lead">Leads</option>
                      <option value="offsite_conversion.fb_pixel_purchase">Purchases</option>
                      <option value="offsite_conversion.fb_pixel_search">Search</option>
                      <option value="offsite_conversion.fb_pixel_view_content">Views Content</option>
                      <option value="offsite_conversion.key_page_view">Key Page Views</option>
                      <option value="offsite_conversion.lead">Offsite Conversion Leads</option>
                      <option value="offsite_conversion.other">Other Website Conversions</option>
                      <option value="offsite_conversion.registration">Registrations</option>
                      
                    </select>
                    </div>
                    <div id="ageGroup" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading13">
                        <div class="panel-body">
                            <ul id="agegroupmain">
                                <li><?php if(!empty($adaccounts['agegroup_total'])) echo $adaccounts['agegroup_total'];?></li>
                                <?php if(!empty($adaccounts['agegroup_detail'])) echo $adaccounts['agegroup_detail'];?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm"></div>
<div class="data-content graph-data placement-divvar">
                      
                    </div>
            <div class="col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-0">
                <div class="panel panel-default ddsection" id="heading14">
                    <div class="panel-heading" role="tab">
                        <a class="collapsed" role="button" data-toggle="collapse"
                           data-parent="#accordion"
                           href="#placement" aria-expanded="false" aria-controls="placement">
                            <h4 class="panel-title text-left" style="padding-left: 27px;">
                                PLACEMENTS <span class="text-lowercase">by</span>
                                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                            </h4>
                        </a>
                        <select class="form-control jcf-hidden" name="placementdd" onchange="getAddSetPlacementDatavarious(this.value)">
                      <option value="clicks" selected="">Clicks</option>
                      <option value="impressions">Impressions</option>
                      
                      <option value="spend">Total Spent</option>
                      <option value="page_engagement">Engagement</option>
                      <option value="like">Page Like</option>
                      <option value="ctr">CTR</option>
                      <option value="offsite_conversion">Conversion</option>
                      
                      <option value="offsite_conversion.fb_pixel_add_payment_info">Adds Payment Info</option>
                      <option value="offsite_conversion.fb_pixel_add_to_cart">Adds To Cart</option>
                      <option value="offsite_conversion.fb_pixel_add_to_wishlist">Adds To Wishlist</option>
                      <option value="offsite_conversion.fb_pixel_complete_registration">Completed Registration</option>
                      <option value="offsite_conversion.fb_pixel_custom">Custom pixel events defined by the advertiser</option>
                      <option value="offsite_conversion.fb_pixel_initiate_checkout">Initiates Checkout</option>
                      <option value="offsite_conversion.fb_pixel_lead">Leads</option>
                      <option value="offsite_conversion.fb_pixel_purchase">Purchases</option>
                      <option value="offsite_conversion.fb_pixel_search">Search</option>
                      <option value="offsite_conversion.fb_pixel_view_content">Views Content</option>
                      <option value="offsite_conversion.key_page_view">Key Page Views</option>
                      <option value="offsite_conversion.lead">Offsite Conversion Leads</option>
                      <option value="offsite_conversion.other">Other Website Conversions</option>
                      <option value="offsite_conversion.registration">Registrations</option>

                      
                    </select>
                        
                    </div>
                    <div id="placement" class="panel-collapse collapse"
                         role="tabpanel" aria-labelledby="heading14">
                        <div class="panel-body">
                            <ul id="placementmain">
                                <li><?php if(!empty($adaccounts['feeds_total'])) echo $adaccounts['feeds_total'];?></li>
                                <?php if(!empty($adaccounts['feeds_detail'])) echo $adaccounts['feeds_detail'];?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!--End Third-->

        </div>

        <!--End Account-->

        </div>

        

    </div>
</div>

<!--End Content-->



            <div class="container-fluid">
              <div class="detail-status-holder">
                <div class="table-holder">
                  <form action="#">
                    <table class="table account-data-table dt-responsive nowrap" id="datatable-responsive">
                      <colgroup>
                        <col class="col5">
                        <col class="col6">
                        <col class="col7">
                        <col class="col8">
                        <col class="col9">
                        <col class="col10">
                        <col class="col11">
                        <col class="col12">
                        <col class="col13">
                        <col class="col14">
                      </colgroup>
                      <thead>
                        <tr>
                          <th><span class="sort-tag"><span>STATUS</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>NAME</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>CLICKS</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>CONVERSIONS</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>ENGAGEMENT</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>PAGE LIKES</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>CTR</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>IMPRESSIONS</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>SPENT</span> <i class="icon-sort"></i></span></th>
                          <th><span class="sort-tag"><span>Daily Budget</span> <i class="icon-sort"></i></span></th>
                          <!-- <th><span class="sort-tag"><span>EDIT</span> <i class="icon-sort"></i></span></th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                            if (isset($adaccounts['addsets'])){ 
                                foreach ($adaccounts['addsets'] as $campaign){
                                    $uniqueId = 'lcs_check_'.rand();
                                    ?>
                                        <tr>
                                            <?php if($campaign['addset_effective_status'] == 'Active'){
                                                ?>
                                                    <td class="status">
                                                        <label class="switch-button">
                                                            <input type="checkbox" checked name="check-1" class="lcs_check" value="<?php echo $campaign['addset_id']; ?>" id="<?php echo $uniqueId; ?>" onchange="fnChangeCheck('<?php echo $uniqueId; ?>', '<?php echo $campaign['addset_id']; ?>')">
                                                            <span class="fake-toggle">
                                                            <span class="switch">&nbsp;</span>
                                                        </span>
                                                        </label>
                                                    </td>
                                                <?php
                                            }
                                            else if($campaign['addset_effective_status'] == 'In Review'){
                                                ?>
                                                    <td align="center" class="pt-n inreview">
                                                        <div class="checker disabled">
                                                            <span>
                                                                <div class="lcs_wrap">
                                                                    <div class="lcs_switch  lcs_off lcs_disabled lcs_checkbox_switch">
                                                                        <div class="lcs_cursor"></div>
                                                                        <div class="lcs_label lcs_label_on"><span class="fa fa-exclamation"></span></div>
                                                                        <div class="lcs_label lcs_label_off"><span class="fa fa-exclamation"></span></div>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </td>
                                                <?php
                                            }
                                            else if($campaign['addset_effective_status'] == 'Denied'){
                                                ?>
                                                    <td align="center" class="pt-n decline">
                                                        <div class="checker disabled">
                                                            <span>
                                                                <div class="lcs_wrap">
                                                                    <div class="lcs_switch  lcs_off lcs_disabled lcs_checkbox_switch">
                                                                        <div class="lcs_cursor"></div>
                                                                        <div class="lcs_label lcs_label_on"><span class="fa fa-times"></span></div>
                                                                        <div class="lcs_label lcs_label_off"><span class="fa fa-times"></span></div>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </td>
                                                <?php
                                            }
                                            else{
                                                ?>
                                                    <td class="status">
                                                        <label class="switch-button">
                                                            <input type="checkbox" name="check-1" class="lcs_check" value="<?php echo $campaign['addset_id']; ?>" id="<?php echo $uniqueId; ?>" onchange="fnChangeCheck('<?php echo $uniqueId; ?>', '<?php echo $campaign['addset_id']; ?>')">
                                                            <span class="fake-toggle">
                                                            <span class="switch">&nbsp;</span>
                                                        </span>
                                                        </label>
                                                    </td>
                                                <?php
                                            } ?>
                                            <td class="title">
                                                <a href='<?php echo site_url('addds/' . $addAccountId . '/' . $campaignId . '/' . $campaign['addset_id']); ?>'>
                                                    <?php echo $campaign['addset_name'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <?php
                                                    if ($campaign['addset_inline_link_clicks'] > 0)
                                                    {
                                                        echo number_format($campaign['addset_inline_link_clicks']);
                                                    }
                                                    else
                                                    {
                                                        echo $campaign['addset_inline_link_clicks'];
                                                    }
                                                ?> @ 
                                                <?php
                                                    if ($campaign['addset_cost_per_inline_link_click'] == '--')
                                                    {
                                                        echo '--';
                                                    }
                                                    else
                                                    {
                                                        echo $this->session->userdata('cur_currency') . number_format($campaign['addset_cost_per_inline_link_click'], 2, '.', ',');
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo number_format($campaign['addset_actions']); ?>
                                                @ 
                                                <?php echo $this->session->userdata('cur_currency') . number_format($campaign['addset_cost_per_total_action'], 2, '.', ','); ?>
                                            </td>
                                            <td>
                                                <?php
                                                    if ($campaign['inline_post_engagement'] > 0)
                                                    {
                                                        echo number_format($campaign['inline_post_engagement']);
                                                    }
                                                    else
                                                    {
                                                        echo $campaign['inline_post_engagement'];
                                                    }
                                                ?> @ 
                                                <?php
                                                    if ($campaign['cost_per_inline_post_engagement'] == '--')
                                                    {
                                                        echo '--';
                                                    }
                                                    else
                                                    {
                                                        echo $this->session->userdata('cur_currency') . number_format($campaign['cost_per_inline_post_engagement'], 2, '.', ',');
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                               <?php
                                                    if ($campaign['page_like'] > 0)
                                                    {
                                                        echo number_format($campaign['page_like']);
                                                    }
                                                    else
                                                    {
                                                        echo $campaign['page_like'];
                                                    }
                                                ?> @ 
                                                <?php
                                                    if ($campaign['cost_per_like1'] == '--')
                                                    {
                                                        echo '--';
                                                    }
                                                    else
                                                    {
                                                        echo $this->session->userdata('cur_currency') . number_format($campaign['cost_per_like1'], 2, '.', ',');
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo number_format((($campaign['addset_inline_link_clicks']/$campaign['addset_impressions'])*100), 2, '.', ',');
												// echo number_format($campaign['addset_unique_ctr'], 2, '.', ','); ?>%
                                            </td>
                                            <td>
                                                <?php echo number_format($campaign['addset_impressions']); ?>
                                            </td>
                                            <td>
                                                <b><?php echo $this->session->userdata('cur_currency') . number_format($campaign['addset_spent'], 2, '.', ','); ?></b>
                                            </td>
                                            <td>
                                                <?php
                                                if (($campaign['addset_daily_budget']) > 0) {

                                                    echo "<a href='javascript:void(0)' onclick='scaleCampaign(".$addAccountId.",\"".$campaignId."\",\"".$campaign['addset_id']."\")'>".$this->session->userdata('cur_currency') . ($campaign['addset_daily_budget'] * 0.01)."</a>";
                                                } else {
                                                     $now = strtotime($campaign['addset_end_time']) . "<br>";

                                                     $your_date = strtotime($campaign['addset_start_time']) . "<br>";
                                                    $datediff = $now - $your_date;
                                                    $days = floor($datediff / (60 * 60 * 24));


                                                     $daily_budget = $campaign['addset_lifetime_budget'] / 100;
                                                     echo "<a href='javascript:void(0)' onclick='scaleCampaign(".$addAccountId.",\"".$campaignId."\",\"".$campaign['addset_id']."\")'>".$this->session->userdata('cur_currency') . number_format($daily_budget/$days, 2, '.', ',')."</a>";
                                                }
                                                ?>
                                            </td>
                                            
                                        </tr>
                                    <?php
                                }
                            }
                        ?>
                      </tbody>
                    </table>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <input type="hidden" id="addAccountId" name="addAccountId" value="<?php echo $addAccountId; ?>">
        <input type="hidden" id="campaignId" name="campaignId" value="<?php echo $campaignId ?>">
        <input type="hidden" id="pageName" name="pageName" value="addSetReport">
        <input type="hidden" id="pageName1" name="pageName1" value="addSetReport">
        <input type="hidden" id="dayLimit" name="dayLimit" value="<?php echo $this->session->userdata('dateval'); ?>">
    <?php } ?>
</main>