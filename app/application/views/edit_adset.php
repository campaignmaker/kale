  <?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1198746903472361',
            xfbml: true,
            version: 'v2.6'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


	</script>
    <pre>
    <?php 
    print_r($adAccountData);
    ?>
  </pre>
    <form accept-charset="utf-8" method="post" enctype="multipart/form-data" name="create_creativeads_frm" id="create_creativeads_frm" action="https://localhost/fb_marketing/createcampaign">    
        <main id="main">
        	<div id="preloaderMain" style="display: none;">
                <div id="statusMain"><i class="fa fa-spinner fa-spin"></i></div>
                <div id="lastStepMess" style="display: none;">Please do not close this page or click the back button while the campaign is uploading</div>
            </div>
            <div class="container-fluid container-with-sidenav">
              <div class="row">
                
                <div class="tab-content">
                  
                  <div id="tab_1_2" class="tab-pane">
                    <?php //$this->load->view('addesign'); ?>
                  </div>
                  <div id="tab_1_1" class="tabcontent active col-md-12 col-md-offset-1">
                    			<!-- start audience -->
                  <div class="col-sm-12 col-md-12 col-lg-12 tabcontent" id="step3">
  <div class="top-notification-box" id="msg_box3" style="display: none;">
      <div id="msg_err3"></div>
  </div>
  <div class="top-notification-box" id="successmsgBx3" style="display: none;">
      <div id="msg_suc3"></div>
  </div>
  <div class="main-content-wrapper" style="padding-top:20px;">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="main-content-box">
          
            <div class="header-holder">
              <div class="btn-item targeting selected">
                <span>
                  <i class="icon-star-box"></i>
                  <span class="text">Targeting</span>
                </span>
              </div>
              <div class="text-wrap">
                <strong class="title">Audience Setup</strong>
              </div>
            </div>
            <div class="audience-setup-holder">
              <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                        by location
                      </a>
                    </strong>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="sk3 form-group">
                        <input type="text" id="locations_autocomplete_include" value="" name="locations_autocomplete_include" class="form-control" placeholder="Add a country, state/province, city or ZIP..."/>
                        <input id="new_geo_locations" type="hidden" name="new_geo_locations" value="">
                        <input id="avoid_geo_locations" type="hidden" name="avoid_geo_locations" value="">
                        <input id="locations_autocomplete_include_hidden" type="hidden" name="locations_autocomplete_include_hidden" value="">
                      </div>
                      <!--<div class="location-label">
                        <span>design <i class="icon-close"></i></span>
                        <span>marketing <i class="icon-close"></i></span>
                        <span>coding <i class="icon-close"></i></span>
                      </div>-->
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="true">
                        BY GENDER
                      </a>
                    </strong>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body gender">
                      <div class="gender-label">
                        <span class="a <?= ($Cmpdraft->gender == '1') ? "active" : "" ?>" onclick="fnSelectval('1', 'genders', 'genderMen')" id="genderMen">Men</span>
                        <span class="b <?= ($Cmpdraft->gender == '2') ? "active" : "" ?>" onclick="fnSelectval('2', 'genders', 'genderWomen')" id="genderWomen">Women</span>
                        <span class="c <?= ($Cmpdraft->gender == '') ? "active" : "" ?>" onclick="fnSelectval('', 'genders', 'genderAll')" id="genderAll">All</span>
                        <input  id="genders" name="genders" value=""  type="hidden">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="true">
                        BY AGE
                      </a>
                    </strong>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body age" id="ageyear_fileds">
                      <?php 
                          $fromArr = explode(',', $Cmpdraft->agegroupfrom);
                          $toArr = explode(',', $Cmpdraft->agegroupto);
                      ?>
                      <?php 
                      if(!empty($fromArr[0])){
                          for($i=0; $i<=count($fromArr)-1; $i++){
                            ?>
                                <div class="age-group" style="clear:left;">
                                  <div class="input-group spin1" id="spinner_from<?php echo $i+1; ?>">
                                    <input type="text" placeholder="18" class="form-control" maxlength="3" onkeypress="return isNumber(event)"  placeholder="form..." maxlength="2" value="<?= $fromArr[$i] ?>"  max="64" min="13" name="form" id="form">
                                    <div class="input-group-addon">Years</div>
                                    <div class="spinner-buttons input-group-btn btn-group-vertical">
                                        <button type="button" class="btn spinner-up btn-xs blue">
                                            <i class="fa fa-angle-up"></i>
                                        </button>
                                        <button type="button" class="btn spinner-down btn-xs blue">
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                    </div>
                                  </div>
                                  <span class="to">
                                    <i class="icon-share"></i>
                                  </span>
                                  <div class="input-group spin2" id="spinner_to<?php echo $i+1; ?>">
                                    <input type="text" placeholder="65+" class="form-control" maxlength="3" name="to" id="to" placeholder="to..." onkeypress="return isNumber(event)" maxlength="2" min="14" max="65+" value="<?= $toArr[$i] ?>" max="65" min="13">
                                    <div class="input-group-addon">Years</div>
                                    <div class="spinner-buttons input-group-btn btn-group-vertical">
                                        <button type="button" class="btn spinner-up btn-xs blue">
                                            <i class="fa fa-angle-up"></i>
                                        </button>
                                        <button type="button" class="btn spinner-down btn-xs blue">
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                    </div>
                                  </div>
                                  <a href="javascript:void(0)" class="add-age" id="more_timetable_fields"><i class="icon-plus"></i></a>
                                </div>
                            <?php
                          }
                      }
                      else{
                      ?>
                          <div class="age-group">
                            <div class="input-group spin1" id="spinner11">
                              <input type="text" placeholder="18" class="form-control spinner-input" maxlength="3" placeholder="form..." maxlength="2" value="<?= !empty($Cmpdraft->agegroupfrom) ? $Cmpdraft->agegroupfrom : '18' ?>"  max="64" min="13" name="form" id="form">
                              <div class="input-group-addon">Years</div>
                              <div class="spinner-buttons input-group-btn btn-group-vertical">
                                  <button type="button" class="btn spinner-up btn-xs blue">
                                      <i class="fa fa-angle-up"></i>
                                  </button>
                                  <button type="button" class="btn spinner-down btn-xs blue">
                                      <i class="fa fa-angle-down"></i>
                                  </button>
                              </div>
                            </div>
                            <span class="to">
                              <i class="icon-share"></i>
                            </span>
                            <div class="input-group spin2" id="spinner21">
                              <input type="text" placeholder="65+" class="form-control spinner-input" maxlength="3" name="to" id="to" placeholder="to..." onkeypress="return isNumber(event)" maxlength="2" min="14" max="65" value="<?= !empty($Cmpdraft->agegroupto) ? $Cmpdraft->agegroupto : '65+' ?>">
                              <div class="input-group-addon">Years</div>
                              <div class="spinner-buttons input-group-btn btn-group-vertical">
                                  <button type="button" class="btn spinner-up btn-xs blue">
                                      <i class="fa fa-angle-up"></i>
                                  </button>
                                  <button type="button" class="btn spinner-down btn-xs blue">
                                      <i class="fa fa-angle-down"></i>
                                  </button>
                              </div>
                            </div>
                            <a href="javascript:void(0)" class="add-age" id="more_timetable_fields"><i class="icon-plus"></i></a>
                          </div>
                      <?php }
                      ?>
                      <input type="hidden" name="adeyearcounter" id="adeyearcounter" value="<?php echo count($fromArr); ?>">
                    </div>
                  </div>
                </div>
                <?php 
                  $interestArr = explode('#$#', $Cmpdraft->interest);
                  $new_interestsArr = explode('#$#', $Cmpdraft->new_interests);
                ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="true">
                        BY INTEREST, DEMOGRAPHIC OR BEHAVIOUR
                      </a>
                    </strong>
                  </div>
                  <div id="collapseFour" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="sk3 text-center form-group">
                        <input type="text" id="interests1" name="interests[]" placeholder="INTEREST, DEMOGRAPHIC OR BEHAVIOUR" class="form-control"/>
                        <input id="new_interests1" type="hidden" name="new_interests[]" value='<?php echo $new_interestsArr[0]; ?>'>
                        <input id="new_interests_hidden1" type="hidden" name="new_interests_hidden[]" value='<?php echo $interestArr[0]; ?>'>
                      </div>

                      <div class="portlet box blue-hoki location-audi displayNone" id="suggestion_data1" >
                          <div class="portlet-body form ">
                              <div  >
                                  <br />
                                  <div class="col-md-12">
                                      <!--interest-->
                                      <div class="form-group">
                                          <label class="control-label">Suggestions</label>
                                          <div>
                                              <div class="tag-cloud">
                                                <div class="location-label" id="location-label1">
                                                  
                                                </div>
                                              </div>
                                              <div style="clear: both;"></div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="clearfix"></div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="target">
                        <span class="title">Advanced Targeting</span>
                        <a href="javascript:void(0)" class="a more_Include_fields " id="more_Include_fields">Include</a>
                        <a href="javascript:void(0)" class="b more_Exclude_fields" id="more_Exclude_fields">Exclude</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div  id="include_fileds">
                  <?php 
                    if(!empty($interestArr[0])){
                      $k=2;
                      for($i=1; $i<=count($interestArr)-1; $i++){
                        ?>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <strong class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="true">
                                  AND INTEREST, DEMOGRAPHIC OR BEHAVIOUR
                                </a>
                              </strong>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse in">
                              <div class="panel-body">
                                <div class="sk3 text-center form-group">
                                  <input type="text" id="interests<?php echo $k; ?>" name="interests[]" placeholder="INTEREST, DEMOGRAPHIC OR BEHAVIOUR" class="form-control"/>
                                  <input id="new_interests<?php echo $k; ?>" type="hidden" name="new_interests[]" value='<?php echo $new_interestsArr[$i]; ?>'>
                                  <input id="new_interests_hidden<?php echo $k; ?>" type="hidden" name="new_interests_hidden[]" value='<?php echo $interestArr[$i]; ?>'>
                                </div>

                                <div class="portlet box blue-hoki location-audi displayNone" id="suggestion_data<?php echo $k; ?>" >
                                    <div class="portlet-body form ">
                                        <div  >
                                            <br />
                                            <div class="col-md-12">
                                                <!--interest-->
                                                <div class="form-group">
                                                    <label class="control-label">Suggestions</label>
                                                    <div>
                                                        <div class="tag-cloud"><ul></ul></div>
                                                        <div style="clear: both;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="target">
                                  <span class="title">Advanced Targeting</span>
                                  <a href="javascript:void(0)" class="a more_Include_fields " id="more_Include_fields">Include</a>
                                  <a href="javascript:void(0)" class="b more_Exclude_fields" id="more_Exclude_fields">Exclude</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php
                      }
                    }
                  ?>
                </div>
                <div  id="exlude_fileds">
                  <div class="panel panel-default" id="excludeInterestDiv" style="<?php if(!empty($Cmpdraft->interest_exclude)){ ?>display: block;<?php }else{ ?>display: none;<?php } ?>">
                    <div class="panel-heading">
                      <strong class="panel-title">
                        <a role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="true">
                          OR INTEREST, DEMOGRAPHIC OR BEHAVIOUR
                        </a>
                      </strong>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse in">
                      <div class="panel-body">
                        <div class="sk3 text-center form-group">
                          <a class="delete-include_exlude-field add-relation" style="float:right;"><i class="fa fa-trash-o"></i></a>
                          <input type="text" id="interests_exclude1" name="interests_exclude" placeholder="INTEREST, DEMOGRAPHIC OR BEHAVIOUR" class="form-control"/>
                          <input id="new_interests_exclude1" type="hidden" name="new_interests_exclude" value='<?php echo $Cmpdraft->new_interests_exclude; ?>'>
                          <input id="new_interests_exclude_hidden1" type="hidden" name="new_interests_exclude_hidden" value='<?php echo $Cmpdraft->interest_exclude; ?>'>
                        </div>

                        <div class="portlet box blue-hoki location-audi displayNone" id="suggestion_data_exclude1" >
                            <div class="portlet-body form ">
                                <div  >
                                    <br />
                                    <div class="col-md-12">
                                        <!--interest-->
                                        <div class="form-group">
                                            <label class="control-label">Suggestions</label>
                                            <div>
                                                <div class="tag-cloud"><ul></ul></div>
                                                <div style="clear: both;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="target">
                          <span class="title">Advanced Targeting</span>
                          <a href="javascript:void(0)" class="a more_Include_fields " id="more_Include_fields">Include</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="interestsCount" id="interestsCount" value="<?php echo !empty($interestsCount) ? count($interestsCount) : !empty($interestArr) ? count($interestArr) : 1; ?>">
                <input type="hidden" name="excludeCount" id="excludeCount" value="1">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseSix" aria-expanded="true">
                        BY PLACEMENT
                      </a>
                    </strong>
                  </div>
                  <!-- started by junaid -->
                  <div id="collapseSix" class="panel-collapse collapse in">
                                  <div class="panel-body placement">
                                    <div class="panel-group">
                                      <div class="panel panel-default placement-panel clearfix">
                                        <div class="clearfix">
                                          <div class="panel-heading inline">
                                            <a class="btn btn-custom automatic-placement" data-toggle="collapse" href="#" onclick="fnSelectPlacementvalall()">Automatic Placement</a>
                                          </div>
                                          <div class="panel-heading inline">
                                            <a class="btn btn-custom manual-placement collapsed" data-toggle="collapse" href="#manualPalacement" aria-expanded="false">Manual Placement</a>
                                          </div>
                                        </div>
                                        <div id="manualPalacement" class="panel-collapse collapse">
                                          <div class="panel-body placement-wrap">
                                            <div class="placement-holder">
                                              <div class="title">Choose Your Manual Placements</div>
                                              <ul class="placement-list">
                                                <li>
                                                <a class="placement-item2 <?= (strpos($Cmpdraft->placement, 'desktopfeed') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet','desktopFeed')" id="desktopFeed">
                                                  <input type="hidden" name="page_types[]" id="desktop_news_feed" value="<?= (strpos($Cmpdraft->placement, 'desktopfeed') !== false) ? "desktopfeed" : "" ?>">Desktop NewsFeed
                                                </a>
                                                <!-- <a class="placement-item2" href="#">Desktop NewsFeed</a> --></li>
                                                <li>
                                                <a class="placement-item2 mobile <?= (strpos($Cmpdraft->placement, 'mobilefeed') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet', 'mobileFeed')" id="mobileFeed">
                                                  <input type="hidden" name="page_types[]" id="mobile_news_feed" value="<?= (strpos($Cmpdraft->placement, 'mobilefeed') !== false) ? "mobilefeed" : "" ?>">Mobile NewsFeed
                                                </a><!--<a class="placement-item2" href="#">Mobile NewsFeed</a>--></li>
                                                <li <?= "PAGE_LIKES" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?><?= "LEAD_GENERATION" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?><?= "POST_ENGAGEMENT" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?>>
                                                <a class="placement-item2 mobile <?= (strpos($Cmpdraft->placement, 'mobileexternal') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet', 'rdpartyFeed')" id="rdpartyFeed">
                                                  <input type="hidden" name="page_types[]" id="party_mobile_site" value="<?= (strpos($Cmpdraft->placement, 'mobileexternal') !== false) ? "mobileexternal" : "" ?>">Mobile 3rd Party
                                                </a><!--<a class="placement-item2" href="#">Mobile 3rd Party</a>--></li>
                                                <li <?= "LEAD_GENERATION" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?>>
                                                <a class="placement-item2 <?= (strpos($Cmpdraft->placement, 'rightcolumn') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet', 'desktopRHS')" id="desktopRHS">
                                                  <input type="hidden" name="page_types[]" id="desktop_right_hand_side" value="<?= (strpos($Cmpdraft->placement, 'rightcolumn') !== false) ? "rightcolumn" : "" ?>">Desktop Right Side
                                                </a>
                                                <!--<a class="placement-item2" href="#">Desktop Right Side</a>--></li>
                                                <li <?= "PAGE_LIKES" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?><?= "LEAD_GENERATION" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?><?= "POST_ENGAGEMENT" == $Cmpdraft->adobjective ? 'style="display:none;"' : "" ?>>
                                                <a class="placement-item2 instagram <?= (strpos($Cmpdraft->placement, 'instagram') !== false) ? "active" : "" ?>" onclick="fnSelectPlacementval('placementDataSet', 'instagramPla')" id="instagramPla">
                                              <input type="hidden" name="page_types[]" id="instagram_feed" value="<?= (strpos($Cmpdraft->placement, 'instagram') !== false) ? "instagram" : "" ?>">Instagram Feed
                                            </a>
                        						<!--<a class="placement-item2" href="#">Instagram Feed</a>--></li>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                  <!-- ended by junaid -->
                  
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseSeven" aria-expanded="true">
                        BY RELATIONSHIP STATUS
                      </a>
                    </strong>
                  </div>
                  <div id="collapseSeven" class="panel-collapse collapse in">
                    <div class="panel-body" id="relationship_fileds">
                      <?php 
                          $realationshipArr = explode(',', $Cmpdraft->realationship);
                      ?>
                      <?php 
                        if(!empty($realationshipArr[0])){
                          for($i=0; $i<=count($realationshipArr)-1; $i++){
                            ?>
                              <div class="select-wrap">
                                <select id="relationship" name="relationship_statuses[]">
                                  <option >Relationship Status</option>
                                  <option value="6" <?php if($realationshipArr[$i] == 6) { echo 'selected'; }?>>All</option>
                                  <option value="1" <?php if($realationshipArr[$i] == 1) { echo 'selected'; }?>>Single</option>
                                  <option value="2" <?php if($realationshipArr[$i] == 2) { echo 'selected'; }?>> In a Relationship</option>
                                  <option value="3" <?php if($realationshipArr[$i] == 3) { echo 'selected'; }?>>Engaged</option>
                                  <option value="4" <?php if($realationshipArr[$i] == 4) { echo 'selected'; }?>>Married</option>
                                  <option value="5" <?php if($realationshipArr[$i] == 5) { echo 'selected'; }?>>Not Specified</option>
                                </select>
                                <a href="javascript:void(0)" class="delete-relationship-field add-relation"><i class="fa fa-trash-o"></i></a>
                              </div>
                            <?php 
                          }
                        }
                        else{
                      ?>
                      <div class="select-wrap">
                        <select id="relationship" name="relationship_statuses[]">
                          <option selected>Relationship Status</option>
                          <option value="" selected>All</option>
                          <option value="1">Single</option>
                          <option value="2"> In a Relationship</option>
                          <option value="3">Engaged</option>
                          <option value="4">Married</option>
                          <option value="5">Not Specified</option>
                        </select>
                        <a href="javascript:void(0)" class="add-relation" id="more_relationship_fields"><i class="icon-plus"></i></a>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseEight" aria-expanded="true">
                        BY EDUCATION
                      </a>
                    </strong>
                  </div>
                  <div id="collapseEight" class="panel-collapse collapse in">
                    <div class="panel-body" id="education_fileds">
                      <?php 
                          $educationArr = explode(',', $Cmpdraft->education);
                      ?>
                      <?php 
                        if(!empty($educationArr[0])){
                          for($i=0; $i<=count($educationArr)-1; $i++){
                            ?>
                              <div class="select-wrap">
                                <select id="education" name="education_statuses[]">
                                  <option value="" selected>Education Level</option>
                                  <option value="14" <?php if($educationArr[$i] == 6) {echo 'selected'; }?>>Education Level</option>
                                  <option value="1" <?php if($educationArr[$i] == 1) {echo 'selected'; }?>> High School</option>
                                  <option value="2" <?php if($educationArr[$i] == 2) {echo 'selected'; }?>> Undergrad</option>
                                  <option value="3" <?php if($educationArr[$i] == 3) {echo 'selected'; }?>> Alum</option>
                                  <option value="4" <?php if($educationArr[$i] == 4) {echo 'selected'; }?>> High School Grad</option>
                                  <option value="5" <?php if($educationArr[$i] == 5) {echo 'selected'; }?>> Some College</option>
                                  <option value="6" <?php if($educationArr[$i] == 6) {echo 'selected'; }?>> Associate Degree</option>
                                  <option value="7" <?php if($educationArr[$i] == 7) {echo 'selected'; }?>> In Grad School</option>
                                  <option value="8" <?php if($educationArr[$i] == 8) {echo 'selected'; }?>> Some Grad School</option>
                                  <option value="9" <?php if($educationArr[$i] == 9) {echo 'selected'; }?>> Master Degree</option>
                                  <option value="10" <?php if($educationArr[$i] == 10) {echo 'selected'; }?>> Professional Degree</option>
                                  <option value="11" <?php if($educationArr[$i] == 11) {echo 'selected'; }?>> Doctorate Degree</option>
                                  <option value="12" <?php if($educationArr[$i] == 12) {echo 'selected'; }?>> Unspecified</option>
                                  <option value="13" <?php if($educationArr[$i] == 13) {echo 'selected'; }?>> Some High School</option>
                                </select>
                                <a href="javascript:void(0)" class="delete-education-field add-relation"><i class="fa fa-trash-o"></i></a>
                              </div>
                            <?php 
                          }
                        }
                        else{
                      ?>
                        <div class="select-wrap">
                          <select id="education" name="education_statuses[]">
                            <option value="" selected>Education Level</option>
                            <option value="1"> High School</option>
                            <option value="2"> Undergrad</option>
                            <option value="3"> Alum</option>
                            <option value="4"> High School Grad</option>
                            <option value="5"> Some College</option>
                            <option value="6"> Associate Degree</option>
                            <option value="7"> In Grad School</option>
                            <option value="8"> Some Grad School</option>
                            <option value="9"> Master Degree</option>
                            <option value="10"> Professional Degree</option>
                            <option value="11"> Doctorate Degree</option>
                            <option value="12"> Unspecified</option>
                            <option value="13"> Some High School</option>
                          </select>
                          <a href="javascript:void(0)" class="add-relation" id="more_education_fields"><i class="icon-plus"></i></a>
                        </div>
                        <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseNine" aria-expanded="true">
                        BY LANGUAGE
                      </a>
                    </strong>
                  </div>
                  <div id="collapseNine" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="sk3 text-center form-group">
                        <input type="text" id="locales_autocomplete" placeholder="LANGUAGE" name="locales_autocomplete" class="form-control"/>
                        <input id="new_ad_langauage" type="hidden" name="new_ad_langauage" value="<?php echo $Cmpdraft->new_ad_langauage; ?>">
                        <input id="new_ad_langauage_hidden" type="hidden" name="new_ad_langauage_hidden" value="<?php echo $Cmpdraft->language; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseTen" aria-expanded="true">
                        BY WORKPLACE
                      </a>
                    </strong>
                  </div>
                  <?php 
                  $employersStr = '';
                  if(!empty($Cmpdraft->employers)){
                      $employersArr = explode(',', $Cmpdraft->employers);
                      for ($i=0; $i <= count($employersArr)-1 ; $i++) { 
                          $employersStr .= '{"id":'.$employersArr[$i].'},';
                      }
                      $employersStr = rtrim($employersStr, ',');
                  }
                  ?>
                  <div id="collapseTen" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="sk3 text-center form-group">
                        <input type="text" name="employers_autocomplete" placeholder="WORKPLACE" id="employers_autocomplete" class="form-control"/>
                        <input id="new_work_employers" type="hidden" name="new_work_employers" value='<?php echo $employersStr; ?>'>
                        <input id="new_work_employers_hidden" type="hidden" name="new_work_employers_hidden" value='<?php echo $Cmpdraft->new_work_employers; ?>'>
                        <input id="new_work_employers_id_hidden" type="hidden" name="new_work_employers_id_hidden" value='<?php echo $Cmpdraft->new_work_id_employers; ?>'>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseeleven" aria-expanded="true">
                        BY JOB TITLE
                      </a>
                    </strong>
                  </div>
                  <?php 
                  $workpositionsStr = '';
                  if(!empty($Cmpdraft->workpositions)){
                      $workpositionsArr = explode(',', $Cmpdraft->workpositions);
                      for ($i=0; $i <= count($workpositionsArr)-1 ; $i++) { 
                          $workpositionsStr .= '{"id":'.$workpositionsArr[$i].'},';
                      }
                      $workpositionsStr = rtrim($workpositionsStr, ',');
                  }
                  ?>
                  <div id="collapseeleven" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="sk3 text-center form-group">
                        <input type="text" name="job_autocomplete" id="job_autocomplete" placeholder="JOB TITLE" class="form-control"/>
                        <input id="new_work_positions" type="hidden" name="new_work_positions" value='<?php echo $workpositionsStr; ?>'>
                        <input id="new_work_positions_hidden" type="hidden" name="new_work_positions_hidden" value='<?php echo $Cmpdraft->new_work_positions; ?>'>
                        <input id="new_work_positions_id_hidden" type="hidden" name="new_work_positions_id_hidden" value='<?php echo $Cmpdraft->new_work_id_positions; ?>'>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseTwelve" aria-expanded="true">
                        BY CUSTOM AUDIENCE
                      </a>
                    </strong>
                  </div>
                  <div id="collapseTwelve" class="panel-collapse collapse in">
                    <div class="panel-body" id="custom_audience_fileds">
                      <?php 
                          $custom_audienceArr = explode('@', $Cmpdraft->custom_audience);
                      ?>
                      <?php 
                        if(!empty($custom_audienceArr[0])){
                          for($i=0; $i<=count($custom_audienceArr)-1; $i++){
                          ?>
                            <div class="select-wrap">
                              <select id="mobile_devices" class="mobile_devices" name="custom_audiences[]">
                                <option value="" selected>CUSTOM AUDIENCE</option>
                                <?php if(!empty($customaudiences)){
                                  foreach ($customaudiences->data as $key => $result) {
                                    $sel = '';
                                    if($custom_audienceArr[$i] == $result->id . "##" . $result->name){
                                      $sel = 'selected';
                                    }
                                    ?>
                                      <option <?php echo $sel; ?> value='<?php echo $result->id . '##' . $result->name; ?>'><?php echo $result->name;?></option>
                                    <?php
                                  }

                                }?>
                              </select>
                              <a href="javascript:void(0)" class="delete-custom-audience-field add-relation"><i class="fa fa-trash-o"></i></a>
                            </div>
                          <?php 
                          }
                        }
                        else{
                      ?>
                        <div class="select-wrap">
                          <select id="mobile_devices" class="mobile_devices" name="custom_audiences[]">
                          <option value="" selected>CUSTOM AUDIENCE</option>
                          </select>
                          <a href="javascript:void(0)" class="add-relation" id="more_custom_fields"><i class="icon-plus"></i></a>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
        </div>
      </div>
      
    </div>
  </div>
</div>
<!-- end audience -->
                 	</div>
                  <div id="tab_1_4" class="tab-pane">
                    <?php //$this->load->view('budget_bidding'); ?>
                  </div>
                  <div id="tab_1_5" class="tab-pane">
                    <?php //$this->load->view('publishLastStep'); ?>         
                  </div>
                </div>  
              </div>
            </div>
          </main>
          <input id="billing_event_1" type="hidden" value="IMPRESSIONS" name="billing_event">
          <input id="totalAdsCount1" type="hidden" value="0" name="totalAdsCount1">
          <input id="totalAdsetCount1" type="hidden" value="0" name="totalAdsetCount1">
          <input id="budgetDay" type="hidden" value="daily_budget" name="budget_type">
          <!--<input id="budgetDay" type="hidden" value="lifetime_budget" name="budget_type">-->
          <input type="hidden" name="compaingname_1" id="compaingname_1" value="<?= isset($Cmpdraft->campaignname) ? $Cmpdraft->campaignname : '' ?>" />
          <input type="hidden" name="adaccountid_2" id="adaccountid_2" value="<?= isset($Cmpdraft->adaccount) ? $Cmpdraft->adaccount : '' ?>" />
          <input type="hidden" name="db_addesign_id" id="db_addesign_id" value="" />
          <input id="old_behaviours" type="hidden" value="<?=isset($Cmpdraft->behaviours)?$Cmpdraft->behaviours:''?>" name="old_behaviours">
          <input id="old_demograph" type="hidden" value="<?=isset($Cmpdraft->demographic)?$Cmpdraft->demographic:''?>" name="old_demograph">
          <input type="hidden" name="campaignobjective" id="campaignobjective" value="<?=isset($Cmpdraft->adobjective)?$Cmpdraft->adobjective:'LINK_CLICKS'?>" />
          <input type="hidden" name="desktoppreview" id="desktoppreview" value="" />
          <input type="hidden" name="mobilepreview" id="mobilepreview" value="" />
          <input type="hidden" name="righthandpreview" id="righthandpreview" value="" />
          <?php 
          if(!empty($Cmpdraft->adobjective)){
            if($Cmpdraft->adobjective == 'LEAD_GENERATION'){
              $objectiveType = 'objective9';
            }
            else if($Cmpdraft->adobjective == 'CONVERSIONS'){
              $objectiveType = 'objective4';
            }
            else if($Cmpdraft->adobjective == 'LINK_CLICKS'){
              $objectiveType = 'objective1';
            }
            else if($Cmpdraft->adobjective == 'PAGE_LIKES'){
              $objectiveType = 'objective8';
            }
            else if($Cmpdraft->adobjective == 'PRODUCT_CATALOG_SALES'){
              $objectiveType = 'objective2';
            }
            else if($Cmpdraft->adobjective == 'POST_ENGAGEMENT'){
              $objectiveType = 'objective6';
            }
          }
          else{
            $objectiveType = 'objective1';
          }
          ?>
          <input type="hidden" name="objectiveType" id="objectiveType" value="<?php echo $objectiveType; ?>" />
    </form>      
      <!-- Main Content Ends -->
    </div>
     <!-- Modal -->
    
	<script>
	function opencampaigntab(evt, fbtabName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		//alert(fbtabName);
		if(fbtabName != 'step1'){
			document.getElementById("adcopy").classList.add("selected");
		}
		else{
			document.getElementById("adcopy").classList.remove("selected");
		}
		if(fbtabName != 'step1' && fbtabName != 'step2'){
			document.getElementById("targeting").classList.add("selected");
		}
		else{
			document.getElementById("targeting").classList.remove("selected");
		}
		if(fbtabName == 'step4' || fbtabName == 'step5'){
			document.getElementById("budget").classList.add("selected");
		}
		else{
			document.getElementById("budget").classList.remove("selected");
		}
		if(fbtabName == 'step5'){
			document.getElementById("review").classList.add("selected");
		}
		else{
			document.getElementById("review").classList.remove("selected");
		}
		document.getElementById(fbtabName).style.display = "block";
	}
	</script>    