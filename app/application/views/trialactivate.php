<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
<script>
  fbq('track', 'AddPaymentInfo');
</script>
 <script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load("1418879561");
</script>

<style>.async-hide { opacity: 0 !important} </style>
<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-55MG9MN':true});</script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76300210-1', 'auto');
  ga('require', 'GTM-55MG9MN');
  ga('send', 'pageview');

</script>
<script src="//load.sumome.com/" data-sumo-site-id="96d95b15a936537ec626b4a6b83f28eeadd638f83c1fb1910116d8e466d758cc" async="async"></script>

        <div class="trial-body">
    <div class="left-trial">
      <div class="trial-quote">
        <div class="trial-quote-title">YOU’RE IN GOOD HANDS</div>
        <div class="trial-quote-text">I want to reiterate this software is awesome! In March it helped me achieve a ROAS of 321% increase and I'm closing out April at 600% on my ecommerce store. No, I'm not shitting you! I'm seriously impressed.</div>
        <div class="trial-quote-name">LANE W.<span>Shoe Conspiracy</span></div>
      </div>
      <div class="trial-quote small-quote">
        <div class="trial-quote-title">DAILY QUOTE</div>
        <div class="trial-quote-text">If you do what you’ve always done, you’ll get what you’ve always gotten. </div>
        <div class="trial-quote-name">TONY ROBBINS<span>Speaker</span></div>
      </div>
      <div class="trial-quote small-quote">
        <div class="trial-quote-title">YOUR ACCOUNT MANAGER</div>
        <img src="<?php echo $this->config->item('assets'); ?>newdesign/images/trial-photo.jpg" alt="" class="trial-quote-photo">
        <div class="trial-quote-name">SAMY <br>ZABARAH<span>Facebook Ads Expert <br>Since 2011</span></div>
        <div class="trial-quote-a">
          <a href="mailto:samy@thecampaignmaker.com">Email Me</a><a href="skype:facebook.adz?add">Skype Me</a>
        </div>
      </div>
    </div>
    <div class="right-trial">
      <div class="activate-title">Activate Your Free Trial</div>
      <div class="top-notification-box" id="successmsgBx1" style="display: none;">
                      <i class="icon-warning"></i>
                      <span class="text payment-errors"></span>
                    </div>
      <form class="form-activate" action="<?php echo base_url()."/trialactivate/processpayment"; ?>" method="POST" id="stripe-payment-form">
          <input type="hidden" name="idev_custom" id="idev_custom_x21" />
<script type="text/javascript" src="https://thecampaignmaker.com/aff/connect/stripe_ip.php"></script>
        <div class="card-info">
          <label class="card-number valid">
            <span class="label">Credit Card Number</span>
            <input type="text" class="card-numbertxt" autocomplete="off">
          </label>
          <div class="label-div">
            <span class="label">Expiry Date</span>
            <div class="card-date">
              <select class="card-expiry-month">
                <option value="1">01</option>
                <option value="2">02</option>
                <option value="3">03</option>
                <option value="4">04</option>
                <option value="5">05</option>
                <option value="6">06</option>
                <option value="7">07</option>
                <option value="8">08</option>
                <option value="9">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
            <div class="card-date">
              <select class="card-expiry-year">
                <option value="2017">2017</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
                <option value="2027">2027</option>
                <option value="2028">2028</option>
                <option value="2029">2029</option>
              </select>
            </div>
          </div>
          <label class="card-code">
            <span class="label">CVV Code</span>
            <input type="text" autocomplete="off" class="card-cvc">
          </label>
        </div>

        <div class="radio-wrap">
          <div class="radio-title">Choose your account type</div>
          <div class="radio">
            <input type="radio" id="r1" name="selectedplan" value="Monthly3" checked="checked" />
            <label for="r1"><span class="span-radio"></span>Monthly Account ($39/Month)<span class="free">Free for 3 days then $39 per month.</span></label>
          </div>
          <div class="radio">
            <input type="radio" id="r2" name="selectedplan" value="Yearly3" />
            <label for="r2"><span class="span-radio"></span>Annual Account ($19/Month)<span class="free">Free for 3 days then $228 per year.</span></label>
          </div>
        </div>
        <input name="action" value="stripe" type="hidden">
                        <input name="redirect" value="" type="hidden">
                        <input name="amount" value="NDk=" type="hidden">
                        <input name="stripe_nonce" value="0dd70c1b2d" type="hidden">
                        
        <div class="card-button">
          <button type="submit" id="stripe-submit">START MY TRIAL</button>
        </div>
      </form>
      <div class="bottom-stripe">
        <p>By signing up you agree to our Terms of Service</p>
        <div class="poweredby"></div>
      </div>
    </div>
  </div>
<!-- Main Content Starts -->
     <!-- main id="main">
        <div class="help-page-holder">
          <div class="container">
            <div class="settings-box">
              <div class="setting-header text-center">
                <h2>ENTER YOUR CREDIT CARD INFO TO ACTIVATE TRIAL!</h2>
              </div>
              <div class="help-videos-holder">
                	<form action="<?php //echo base_url()."/trialactivate/processpayment"; ?>" method="POST" id="stripe-payment-form">
                        <div class="form-row">
                            <label>Card Number</label>
                            <input size="20" autocomplete="off" class="card-number" type="text" style="color:#000;">
                        </div>
                        <div class="form-row">
                            <label>CVC</label>
                            <input size="4" autocomplete="off" class="card-cvc" type="text" style="color:#000;">
                        </div>
                        <div class="form-row">
                            <label>Expiration (MM/YYYY)</label>
                            <input size="2" class="card-expiry-month" type="text" style="color:#000;">
                            <span> / </span>
                            <input size="4" class="card-expiry-year" type="text" style="color:#000;">
                        </div>
                                    <div class="form-row" style="display:none;">
                            <label>Payment Type:</label>
                            <input name="recurring" value="no"  type="radio"><span>One time payment</span>
                            <input name="recurring" value="yes" checked="checked" type="radio"><span>Recurring monthly payment</span>
                        </div>
                                    <input name="action" value="stripe" type="hidden">
                        <input name="redirect" value="" type="hidden">
                        <input name="amount" value="NDk=" type="hidden">
                        <input name="stripe_nonce" value="0dd70c1b2d" type="hidden">
                        <button type="submit" id="stripe-submit" class="btn btn-success">Submit Payment</button>
                    </form>
                    <div class="payment-errors"></div>
              </div>
            </div>
          </div>
        </div>
      </main>-->
      <!-- Main Content Ends -->
    </div>
<script>
Stripe.setPublishableKey("pk_live_HA0omoOtinEyA28gk3DqVoMK");

function stripeResponseHandler(status, response) {

    if (response.error) {

    // show errors returned by Stripe

		jQuery("#successmsgBx1").show();
        jQuery(".payment-errors").html(response.error.message);

    // re-enable the submit button

    jQuery('#stripe-submit').attr("disabled", false);

    } else {
        if(response['card']['funding'] == 'prepaid'){
            jQuery("#successmsgBx1").show();
            jQuery(".payment-errors").html("Prepaid cards are not allowed.");
        }
        else{
    		jQuery("#successmsgBx1").hide();
    
            var form$ = jQuery("#stripe-payment-form");
    
            // token contains id, last4, and card type
    
            var token = response['id'];
    
            // insert the token into the form so it gets submitted to the server
    
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
    
            // and submit
    
            form$.get(0).submit();
        }

    }

}

jQuery(document).ready(function($) {

  $("#stripe-payment-form").submit(function(event) {

    // disable the submit button to prevent repeated clicks

    $('#stripe-submit').attr("disabled", "disabled");


    // send the card details to Stripe

    Stripe.createToken({

      number: $('.card-numbertxt').val(),

      cvc: $('.card-cvc').val(),

      exp_month: $('.card-expiry-month').val(),

      exp_year: $('.card-expiry-year').val()

    }, stripeResponseHandler);



    // prevent the form from submitting with the default action

    return false;

  });

});
</script>    