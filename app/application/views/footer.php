
<!-- Footer Section -->

    <footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<nav class="footer-nav">
								<ul class="d-sm-flex justify-content-center justify-content-md-start text-center">
									<li class="footer-nav-item">
										<a href="https://thecampaignmaker.com/blog/terms-of-service/" target="_blank" class="footer-nav-link">Terms of Service</a>
									</li>
									<li class="footer-nav-item">
										<a href="https://www.iubenda.com/privacy-policy/95975692" target="_blank" class="footer-nav-link">Privacy Policy</a>
									</li>
									<li class="footer-nav-item">
										<a href="https://thecampaignmaker.com/blog/" class="footer-nav-link">Blog</a>
									</li>
									<li class="footer-nav-item">
										<a href="http://fubsz.com" target="_blank" class="footer-nav-link">About Us</a>
									</li>
									
								</ul>
							</nav>
						</div>
						<div class="col-md-4">
							<div class="made-with text-center text-md-right">
								<span>Made with &lt;3 <a href="#">by FUBSZ</a></span>
							</div>
						</div>
					</div>
				</div>
			</footer>
    </div>
</div>
<!-- model start -->    
    <div class="modal fade upgrade-popup" id="upgradePopup" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <a href="#" class="modal-close" data-dismiss="modal"><i class="icon-close"></i></a>
        <div class="modal-content">
          <div class="modal-body">
              <div class="flex-box">
                <div class="plan-select-box">
                  <div class="logo"><a href="#"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/logo-small-01.png" height="71" width="71" alt="logo"></a></div>
                  <div class="text-holder">
                    <h2>Upgrade Your Account</h2>
                    <p>Unlock the full potential of The Campaign Maker with these epic features by upgrading or keep using the basic features by downgrading to a free forever account.</p>
                  </div>
                  <div class="table-holder">
                    <table class="table plan-table">
                      <colgroup>
                        <col class="col1">
                        <col class="col2">
                        <col class="col3">
                        <col class="col4">
                      </colgroup>
                      <thead>
                        <tr>
                          <th>PLAN FEATURES</th>
                          <th>
                            <label class="custom-radio"><input type="radio" name="plan" onclick="handleplanClick(this);" value="free"><span class="fake-radio"></span><span class="fake-label">Free Forever</span></label>
                          </th>
                          <th>
                            <label class="custom-radio"><input type="radio" name="plan" onclick="handleplanClick(this);" value="monthly"><span class="fake-radio"></span><span class="fake-label">PRO Monthly</span></label>
                          </th>
                          <th>
                            <label class="custom-radio"><input type="radio" name="plan" checked onclick="handleplanClick(this);" value="yearly"><span class="fake-radio"></span><span class="fake-label">PRO Yearly <span class="save-info">Save $96</span></span></label>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Reporting Dashboard</td>
                          <th>BASIC</th>
                          <th>DETAILED</th>
                          <th>DETAILED</th>
                        </tr>
                        <tr>
                          <td>Campaign Creation</td>
                          <th>LIMITED</th>
                          <th>UNLIMITED</th>
                          <th>UNLIMITED</th>
                        </tr>
                        <tr>
                          <td>Automatic Optimization Rules</td>
                          <td><span class="no-option"></span></td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                        </tr>
                        <tr>
                          <td>Save &amp; Load Full Campaigns</td>
                          <td><span class="no-option"></span></td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                        </tr>
                        <tr>
                          <td>Split Test Ads &amp; Targeting</td>
                          <td><span class="no-option"></span></td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                        </tr>
                        <tr>
                          <td>Invitation to Join Private Facebook Group</td>
                          <td><span class="no-option"></span></td>
                          <td><span class="no-option"></span></td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                        </tr>
                        <tr>
                          <td>Customer Support</td>
                          <th>72 HOURS</th>
                          <th>24 HOURS</th>
                          <th>12 HOURS</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <span class="question-help-info"><i class="icon-question"></i><span>Questions or Concerns? Click the help button in the menu above.</span></span>
                </div>
                <div class="account-summary">
                  <div class="summary-header">
                    <span class="account-title">Account Upgrade</span>
                    <span class="sub-title">Summary</span>
                  </div>
                  <table class="table">
                    <tbody>
                      <tr>
                        <td>Plan</td>
                        <td id="account_plan">Yearly</td>
                      </tr>
                      <tr>
                        <td>Monthly price</td>
                        <td id="monthly_price">$29/mo</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="summary-footer">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>Total Price</td>
                          <td id="total_price">$348/yr</td>
                        </tr>
                      </tbody>
                    </table>
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="monthlyform" style="display:none;">
 <input type="hidden" name="cmd" value="_xclick-subscriptions" />
 <input type="hidden" name="business" value="samy@fubsz.com" />
 <input type="hidden" name="item_name" value="The Campaign Maker - Pro Monthly Subscription" />
<input type="hidden" name="item_number" value="<?php echo $this->session->userdata['logged_in']['id']; ?>" />
 <input type="hidden" name="currency_code" value="USD" />
 <input type="hidden" name="return" value="https://thecampaignmaker.com/ui/login/refreshplan" />
 <input type="hidden" name="a3" value="39" />
 <input type="hidden" name="p3" value="1" />
 <input type="hidden" name="t3" value="M" />
 <input type="hidden" name="rm" value="2" />
 <input type="hidden" name="notify_url" value="https://thecampaignmaker.com/aff/connect/paypal_ipn_recurring.php" />
 <input type="hidden" name="custom" value="<?PHP echo $_SERVER['REMOTE_ADDR']; ?>" />
 <input type="hidden" name="src" value="1" />
 <input type="hidden" name="no_note" value="1" />
<input type="hidden" name="no_shipping" value="1">
<button class="btn btn-info btn-upgrade">Upgrade now</button>
 </form>
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="yearlyform">
 <input type="hidden" name="cmd" value="_xclick-subscriptions" />
 <input type="hidden" name="business" value="samy@fubsz.com" />
 <input type="hidden" name="item_name" value="The Campaign Maker - Pro Yearly Subscription" />
 <input type="hidden" name="item_number" value="<?php echo $this->session->userdata['logged_in']['id']; ?>" />
 <input type="hidden" name="currency_code" value="USD" />
 <input type="hidden" name="return" value="https://thecampaignmaker.com/ui/login/refreshplan" />
 <input type="hidden" name="a3" value="348" />
 <input type="hidden" name="p3" value="1" />
 <input type="hidden" name="t3" value="Y" />
 <input type="hidden" name="rm" value="2" />
 <input type="hidden" name="notify_url" value="https://thecampaignmaker.com/aff/connect/paypal_ipn_recurring.php" />
 <input type="hidden" name="custom" value="<?PHP echo $_SERVER['REMOTE_ADDR']; ?>" />
 <input type="hidden" name="src" value="1" />
 <input type="hidden" name="no_note" value="1" />
<input type="hidden" name="no_shipping" value="1">
<button class="btn btn-info btn-upgrade">Upgrade now</button>
 </form>
                    <form action="#">
                    <a class="btn btn-info btn-upgrade" id="freeform" href="<?php echo $this->config->item('base_url'); ?>login/changeplan/free"  style="display:none;margin-left:auto;margin-right:auto;">Downgrade</a>
                    </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <span class="esc-info">Press <strong>Esc</strong> to close</span>
      </div>
    </div>
    <style>
	.upgrade-popup .account-summary {
    background: rgba(0, 0, 0, 0) url("<?php echo $this->config->item('assets'); ?>newdesign/images/bg-account.png") no-repeat scroll 0 0 / cover ;
    
}
	</style>
<!-- model end --> 

<!--thank you modal-->

<div class="modal fade thankyou-popup" id="thankyouPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <a href="#" class="modal-close" data-dismiss="modal"><i class="icon-close"></i></a>
          <div class="modal-content">
            <div class="modal-header">
              <h2>Thank You For Your Continued Support!</h2>
              <p>Your account is now upgraded</p>
            </div>
            <div class="modal-body">
                <div class="btn-row">
                  <a href="<?php echo base_url(); ?>createcampaign" class="btn btn-info long" data-toggle="modal" data-target="#campaignPublished">Create New Campaign</a>
                </div>
                <div class="btn-row">
                  <a href="<?php echo base_url(); ?>reports" class="btn btn-info darken long" data-toggle="modal" data-target="#publishCampaign">Access Your Reports</a>
                </div>
                <div class="btn-row">
                  <a href="<?php echo base_url(); ?>automaticoptimization" class="btn btn-success long" data-toggle="modal" data-target="#publishCampaign">Setup Optimization Rules</a>
                </div>
            </div>
            <div class="modal-footer">
              <p>Have questions or concerns about anything? We are alway here to help make this the best experience for you! &nbsp;<a href="<?php echo base_url(); ?>helps">Contact us now.</a></p>
            </div>
          </div>
        </div>
      </div>
<!--Thank you Modal End -->


<!--  special offer model start -->    
    <div class="modal fade upgrade-popup" id="upgradePopupmonthly" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <a href="#" class="modal-close" data-dismiss="modal"><i class="icon-close"></i></a>
        <div class="modal-content">
          <div class="modal-body">
              <div class="flex-box">
                <div class="plan-select-box">
                  <div class="logo"><a href="#"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/logo-small-01.png" height="71" width="71" alt="logo"></a></div>
                  <div class="text-holder">
                    <h2>Upgrade Your Account</h2>
                    <p>Unlock the full potential of The Campaign Maker with these epic features by upgrading or keep using the basic features by downgrading to a free forever account.</p>
                  </div>
                  <div class="table-holder">
                    <table class="table plan-table">
                      <colgroup>
                        <col class="col1">
                        <col class="col2">
                        
                      </colgroup>
                      <thead>
                        <tr>
                          <th>PLAN FEATURES</th>
                          
                          <th>
                            <label class="custom-radio"><span class="fake-label">PRO Monthly</span></label>
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Reporting Dashboard</td>
                          
                          <th>DETAILED</th>
                        </tr>
                        <tr>
                          <td>Campaign Creation</td>
                         
                          <th>UNLIMITED</th>
                        </tr>
                        <tr>
                          <td>Automatic Optimization Rules</td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                        </tr>
                        <tr>
                          <td>Save &amp; Load Full Campaigns</td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                        </tr>
                        <tr>
                          <td>Split Test Ads &amp; Targeting</td>
                          <td><span class="option-available"><i class="icon-check"></i></span></td>
                        </tr>
                        <tr>
                          <td>Invitation to Join Private Facebook Group</td>
                          <td><span class="no-option"></span></td>
                        </tr>
                        <tr>
                          <td>Customer Support</td>
                        
                          <th>24 HOURS</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <span class="question-help-info"><i class="icon-question"></i><span>Questions or Concerns? Click the help button in the menu above.</span></span>
                </div>
                <div class="account-summary">
                  <div class="summary-header">
                    <span class="account-title">Account Upgrade</span>
                    <span class="sub-title">Summary</span>
                  </div>
                  <table class="table">
                    <tbody>
                      <tr>
                        <td>Plan</td>
                        <td id="account_plan">Monthly</td>
                      </tr>
                      <tr>
                        <td>Monthly price</td>
                        <td id="monthly_price">$39/mo</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="summary-footer">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>Total Price</td>
                          <td id="total_price">$39/yr</td>
                        </tr>
                      </tbody>
                    </table>
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="monthlyformmonthly">
 <input type="hidden" name="cmd" value="_xclick-subscriptions" />
 <input type="hidden" name="business" value="samy@fubsz.com" />
 <input type="hidden" name="item_name" value="The Campaign Maker - Pro Monthly Subscription" />
<input type="hidden" name="item_number" value="<?php echo $this->session->userdata['logged_in']['id']; ?>" />
 <input type="hidden" name="currency_code" value="USD" />
 <input type="hidden" name="return" value="https://thecampaignmaker.com/ui/login/refreshplan" />
 <input type="hidden" name="a3" value="39" />
 <input type="hidden" name="p3" value="1" />
 <input type="hidden" name="t3" value="M" />
 <input type="hidden" name="rm" value="2" />
 <input type="hidden" name="notify_url" value="https://thecampaignmaker.com/aff/connect/paypal_ipn_recurring.php" />
 <input type="hidden" name="custom" value="<?PHP echo $_SERVER['REMOTE_ADDR']; ?>" />
 <input type="hidden" name="src" value="1" />
 <input type="hidden" name="no_note" value="1" />
<input type="hidden" name="no_shipping" value="1">
<button class="btn btn-info btn-upgrade">Upgrade now</button>
 </form>
                    
                  </div>
                </div>
              </div>
          </div>
        </div>
        <span class="esc-info">Press <strong>Esc</strong> to close</span>
      </div>
    </div>
    <style>
	.upgrade-popup .account-summary {
    background: rgba(0, 0, 0, 0) url("<?php echo $this->config->item('assets'); ?>newdesign/images/bg-account.png") no-repeat scroll 0 0 / cover ;
    
}
	.editadset{
		background-color: #26c1c9;
		color: #fff !important;
		padding: 5px 15px;
		text-decoration: none !important;
	}
	</style>
    
<!-- special offer model end --> 

    
<?php $this->load->view('modals'); ?>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/bootstrap.min.js"></script>
    <?php //if($this->uri->segment(1) != 'trialactivate' && $this->uri->segment(1) != 'upgrade' && $this->uri->segment(1) != 'automaticoptimization' && $this->uri->segment(1) != 'analysis' && $this->uri->segment(1) != 'trialactivate2') { ?>
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/jquery.main.js"></script>
    
    <!--<script src="<?php echo $this->config->item('assets'); ?>newlogin/js/jquery.main.js"></script>-->
    
    <?php //} ?>
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/moment.min.js"></script>
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/dataTables.bootstrap.min.js"></script>
    
    <!--<script src="<?php echo $this->config->item('assets'); ?>newdesign/js/datepicker/daterangepicker.js"></script>-->
    <script type="text/javascript" src="<?php echo $this->config->item('assets'); ?>global/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->item('assets'); ?>newdesign/js/bootstrap-datepicker.js"></script>
    
    <script src="<?php echo $this->config->item('assets'); ?>global/plugins/dropzone/dropzone.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tokeninput/1.7.0/jquery.tokeninput.js"></script>
    <!-- <script type="text/javascript" src="<?php //echo $this->config->item('assets'); ?>jquery.tokeninput.js"></script> -->
    <?php if ($this->uri->segment(1) == 'createcampaign' || $this->uri->segment(1) == 'automaticoptimization') { ?>
      <script src="<?php echo $this->config->item('assets'); ?>createcampaign-new.js" type="text/javascript"></script>
    <?php } ?>
    <?php  if($this->uri->segment(1) == 'editadset'){ ?>
      <script src="<?php echo $this->config->item('assets'); ?>editadset-new.js" type="text/javascript"></script>    
    <?php } ?>
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/raphael-min.js"></script>
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/morris.min.js"></script>
    <?php if ($this->uri->segment(1) == 'special' || $this->uri->segment(1) == 'trialend' || $this->uri->segment(1) == 'trialactivate' || $this->uri->segment(1) == 'upgrade' || $this->uri->segment(1) == 'trialactivate2') { ?>
    <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/scripts.min.js"></script>
    <?php } ?>
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.10/webfont.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
	<script>
	
	
		WebFont.load({
			google: {
				families: ['Montserrat:400,500,600']
			}
		});
	</script>

    <script type="text/javascript">
      jQuery(window).load(function(){
        
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();
        
        

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        //$('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

       //$('#datatable-responsive').DataTable();

        TableManageButtons.init();
        //FormDropzone.init();
      });
      
      

      var FormDropzone = function () {
        return {
          //main function to initiate the module
          init: function () {
            Dropzone.options.myDropzone = {
                //maxFiles: 1,
                init: function () {
                    this.on("addedfile", function (file, responseText) {
                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;
                        // Listen to the click event
                        removeButton.addEventListener("click", function (e) {
                            // Make sure the button click doesn't submit the form:
                            console.log(e);
                            console.log(file.name);
                            console.log(responseText);
                            e.preventDefault();
                            e.stopPropagation();
                            var fname = file.name;
                            var fnameArr = fname.split('.');
                            var fname1 = fnameArr[0]+fnameArr[1]
                            jQuery('.' + fname1).remove();
                            stickyCounter('', '');
                            // Remove the file preview.
                            _this.removeFile(file);
                              
                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.
                        });
                        
                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });
                      
                    this.on("maxfilesexceeded", function(file) { this.removeFile(file); });
                },
                accept: function (file, done) {
                      var re = /(?:\.([^.]+))?$/;
                      var ext = re.exec(file.name)[1];
                      ext = ext.toUpperCase();
                      if (ext == "JPG" || ext == "JPEG" || ext == "PNG" || ext == "GIF" || ext == "BMP" || ext == "3GP" || ext == "AVI" || ext == "DAT" || ext == "F4V" || ext == "FLV" || ext == "M4V" || ext == "MKV" || ext == "MOV" || ext == "MP4" || ext == "WMV" || ext == "TOD" || ext == "QT" || ext == "VOB" || ext == "MPEG" || ext == "MPEG4")
                      {
                          done();
                      } else {
                          done("Please select only supported picture files.");
                      }
                },
                success: function (file, response) {
                      console.log(file);
                      obj = JSON.parse(response);
                      if(obj.p == 1){
                          var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="product1_img_url" id="product1_img_url">';
                          str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                          jQuery('#uploadedImages-product').append(str);
                      }
                      else if(obj.p == 2){
                          var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="product2_img_url" id="product2_img_url">';
                          str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                          jQuery('#uploadedImages-product').append(str);
                      }
                      else if(obj.p == 3){
                          var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="product3_img_url" id="product3_img_url">';
                          str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                          jQuery('#uploadedImages-product').append(str);
                      }
                      else{
                        if($('#imgType').val() == 'video'){
                          jQuery('#uploadedImages').html('');
                        }
                        var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="creatives[viewUrl][]" id="viewUrl1">';
                        str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages').append(str);
                      }
                      stickyCounter(obj.filename, 'img');
                      LoeadPreview();
                      jQuery('.dz-message').css('opacity', 1);
                    }  
                }
            }
        };
      }();
      var FormDropzone1 = function () {
          return {
              //main function to initiate the module
              init: function () {
                  Dropzone.options.myDropzone1 = {
                      maxFiles: 1,
                      init: function () {
                          this.on("addedfile", function (file, responseText) {
                              // Create the remove button
                              var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                              // Capture the Dropzone instance as closure.
                              var _this = this;
                              // Listen to the click event
                              removeButton.addEventListener("click", function (e) {
                                  // Make sure the button click doesn't submit the form:
                                  e.preventDefault();
                                  e.stopPropagation();
                                  stickyCounter();
                                  // Remove the file preview.
                                  _this.removeFile(file);
                                  // If you want to the delete the file on the server as well,
                                  // you can do the AJAX request here.
                              });

                              // Add the button to the file preview element.
                              file.previewElement.appendChild(removeButton);
                          });
                          
                          this.on("maxfilesexceeded", function(file) { this.removeFile(file); });
                      },
                      accept: function (file, done) {
                          var re = /(?:\.([^.]+))?$/;
                          var ext = re.exec(file.name)[1];
                          ext = ext.toUpperCase();
                          if (ext == "JPG" || ext == "JPEG" || ext == "PNG" || ext == "GIF" || ext == "BMP" || ext == "3GP" || ext == "AVI" || ext == "DAT" || ext == "F4V" || ext == "FLV" || ext == "M4V" || ext == "MKV" || ext == "MOV" || ext == "MP4" || ext == "WMV" || ext == "TOD" || ext == "QT" || ext == "VOB" || ext == "MPEG" || ext == "MPEG4")
                          {
                              done();
                          } else {
                              done("Please select only supported picture files.");
                          }
                      },
                      success: function (file, response) {
                          console.log(file);
                          obj = JSON.parse(response);
                          if(obj.p == 1){
                              var str = '<input type="hidden" value="' + site_url + 'uplaods/campaign/'+obj.userId+'/' + obj.filename + '" name="product1_img_url" id="product1_img_url">';
                              str += '<input type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                              jQuery('#uploadedImages-product').append(str);
                              $('#totalAdsCount1').val('4');
                          }
                          else if(obj.p == 2){
                              var str = '<input type="hidden" value="' + site_url + 'uplaods/campaign/'+obj.userId+'/' + obj.filename + '" name="product2_img_url" id="product2_img_url">';
                              str += '<input type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                              jQuery('#uploadedImages-product').append(str);
                              $('#totalAdsCount1').val('4');
                          }
                          else if(obj.p == 3){
                              var str = '<input type="hidden" value="' + site_url + 'uplaods/campaign/'+obj.userId+'/' + obj.filename + '" name="product3_img_url" id="product3_img_url">';
                              str += '<input type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                              jQuery('#uploadedImages-product').append(str);
                              $('#totalAdsCount1').val('4');
                          }
                          else if(obj.p == 'video'){
                              jQuery('#uploadedImages').html('');
                              var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="creatives[viewUrl][]" id="product3_video_url">';
                              str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                              jQuery('#uploadedImages').append(str);
                              stickyCounter('', '');
                          }
                          else{
                              var str = '<input type="hidden" value="' + site_url + 'uplaods/campaign/'+obj.userId+'/' + obj.filename + '" name="creatives[viewUrl][]" id="viewUrl1">';
                              str += '<input type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                              jQuery('#uploadedImages').append(str);

                          }
                          LoeadPreview();
                          jQuery('.dz-message').css('opacity', 1);
                      }
                  }
              }
          };
      }();
      jQuery(document).ready(function () {
        FormDropzone.init();
        FormDropzone1.init();
     });
    </script>

    <script>
      jQuery(window).load(function(){
        $(".dataTables_length").prependTo($("#datatable-responsive_filter"));
        $('#datatable-responsive_filter').prependTo($('.data .box_title'));
      });

      $(document).ready(function() {
        if ($(window).width() < 992) {
          $(".go_there1").prependTo($(".come_here1"));
		  
		 
               // $('#tableData').paging({limit:10});
            
        }
	    //$('#upgradePopup').modal('show');
    
      });
      jQuery(window).load(function(){
        $('#spinner11').spinner({value:18, min:18, max:65, step:1});
        $('#spinner21').spinner({value:65, min:18, max:65, step:1});
        
        for(k=2; k<=20; k++){
            $('#spinner_from'+k).spinner({value:18, min:18, max:65, step:1});
            $('#spinner_to'+k).spinner({value:65, min:18, max:65, step:1});
        }
    });
		function handleplanClick(myRadio) {
			if(myRadio.value == "monthly"){
				document.getElementById("account_plan").innerHTML = 'Monthly';
				document.getElementById("monthly_price").innerHTML = '$39/mo';
				document.getElementById("total_price").innerHTML = '$39/mo';
				//document.getElementById("upgradebtn").innerHTML = 'Upgrade now';
				document.getElementById("monthlyform").style.display = 'block';
				document.getElementById("yearlyform").style.display = 'none';
				document.getElementById("freeform").style.display = 'none';	
			}
			if(myRadio.value == "yearly"){
				document.getElementById("account_plan").innerHTML = 'Yearly';
				document.getElementById("monthly_price").innerHTML = '$29/mo';
				document.getElementById("total_price").innerHTML = '$348/mo';
				//document.getElementById("upgradebtn").innerHTML = 'Upgrade now';
				document.getElementById("yearlyform").style.display = 'block';
				document.getElementById("monthlyform").style.display = 'none';
				document.getElementById("freeform").style.display = 'none';	
			}
			if(myRadio.value == "free"){
				document.getElementById("account_plan").innerHTML = 'FREE';
				document.getElementById("monthly_price").innerHTML = 'FREE';
				document.getElementById("total_price").innerHTML = 'FREE';
				document.getElementById("freeform").style.display = 'inline-block';
				document.getElementById("monthlyform").style.display = 'none';
				document.getElementById("yearlyform").style.display = 'none';	
				//document.getElementById("upgradebtn").innerHTML = 'Downgrade';
			}
		}
		
    </script>
    <script>
      var date = new Date();
      date.setDate(date.getDate()-1);
      jQuery(window).load(function(){
        jQuery( "#inline-datepicker" ).datepicker({
          weekStart: 1,
          todayHighlight: true,
          startDate: date,
          toggleActive: true
        }).on('changeDate', function(e){
          $('#adset_start_date').val(e.format('dd/mm/yyyy'))
        });

      });
	  function upgradeplan(){
		  if($('input[name="plan"]:checked').val() == "free"){
			  window.location='<?php echo $this->config->item('base_url'); ?>login/changeplan/free';
		  }
		  if($('input[name="plan"]:checked').val() == "monthly"){
			  window.location='<?php echo $this->config->item('base_url'); ?>login/changeplan/monthly';
		  }
		  if($('input[name="plan"]:checked').val() == "yearly"){
			  window.location='<?php echo $this->config->item('base_url'); ?>login/changeplan/yearly';
		  }
	  
	  }
	  function showupgradepopup(){
	  	$('#upgradePopup').modal('show');
	  }

	  function fneditadset(adsetid){
		  alert(adsetid);
		  $('#editadset').modal('show');
	  }
    </script>
	
<?php if($this->session->userdata['user_subs']['trialexpited'] == "expired") { ?>

    <script>
    $(document).ready(function() {
        $('#upgradePopup').modal('show');
    });
	 </script>
<?php } ?>
<?php if($_GET['specialoffer'] == "yes"){ ?>

    <script>
    $(document).ready(function() {
        $('#upgradePopupmonthly').modal('show');
    });
	 </script>
<?php } ?>     
<?php if($_GET['showthanks'] == "yes"){ ?>
	<script>
    $(document).ready(function() {
        $('#thankyouPopup').modal('show');
    });
	 </script>
     <?php } ?>
    <?php if ($this->uri->segment(1) == 'reports' || $this->uri->segment(1) == 'addset' || $this->uri->segment(1) == 'addds' || $this->uri->segment(1) == 'adone' || $this->uri->segment(1) == 'spendreports') { ?>
    <script src="<?php echo $this->config->item('assets'); ?>general-new.js" type="text/javascript"></script>
    <?php }?>
    <?php if($Cmpdraft->imgType == 'video'){?>
      <script type="text/javascript"> jQuery('#uploadedImages').html(''); fngetallvideos('<?php echo $userId; ?>', 'gallery-video');$('.upld-video').slideToggle(300);
    $('.upld-img').hide(300);
    $('.upld-lib').hide(300);
    $('#imgType').val('video');</script>
    <?php } ?>
    <?php if($Cmpdraft->imgType == 'image'){ ?>
    <?php if($Cmpdraft->adobjective == "PRODUCT_CATALOG_SALES") { ?>
       <script type="text/javascript">
       jQuery('#uploadedImages').html('');
       //jQuery('#uploadedImages-product').html('');
       
         <?php //if($Cmpdraft->adimagehash1 != "") { ?>
              fngetallimages('<?php echo $userId; ?>', 'gallery-p1'); $('.upld-lib-p1').slideToggle(300);
             $('.upld-img-p1').hide(300);
             $('.upld-video-p1').hide(300);
             $('#imgType').val('image'); 
         <?php //} ?>
         <?php //if($Cmpdraft->adimagehash2 != "") { ?>
              fngetallimages('<?php echo $userId; ?>', 'gallery-p2'); $('.upld-lib-p2').slideToggle(300);
             $('.upld-img-p2').hide(300);
             $('.upld-video-p2').hide(300);
             $('#imgType').val('image'); 
         <?php //} ?>
         <?php //if($Cmpdraft->adimagehash3 != "") { ?>
              fngetallimages('<?php echo $userId; ?>', 'gallery-p3'); $('.upld-lib-p3').slideToggle(300);
             $('.upld-img-p3').hide(300);
             $('.upld-video-p3').hide(300);
             $('#imgType').val('image'); 
         <?php //} ?>
       </script>
    <?php } else { ?>
    <script type="text/javascript"> jQuery('#uploadedImages').html(''); fngetallimages('<?php echo $userId; ?>', 'gallery'); $('.upld-lib').slideToggle(300);
    $('.upld-img').hide(300);
    $('.upld-video').hide(300);
    $('#imgType').val('image'); </script>
    <? } } ?> 
    <?php 
      $interestArr = explode('#$#', $Cmpdraft->interest);
      if(!empty($interestArr[0])){
        $k=2;
        for($i=1; $i<=count($interestArr)-1; $i++){
          ?>
            <script type="text/javascript">addIntrest('<?php echo $k; ?>');</script>
          <?php
        }
      }
    ?>
    <?php
      if(!empty($Cmpdraft->pageid)){
        ?><script type="text/javascript">get_pages_tabs();</script><?php
      }
      ?>
      <?php if(!isset($Cmpdraft->adobjective)){ ?>
      <script type="text/javascript">fnSelectPlacementvalall();</script>
      <?php } ?>
      
          <!-- <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/animationdata.js"></script>
      <script src="<?php echo $this->config->item('assets'); ?>newdesign/js/spinner.js"></script> -->
  </body>
</html>
