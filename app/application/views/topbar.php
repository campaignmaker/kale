<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
?>
<!-- BEGIN HEADER -->
<!-- DO NOT MODIFY -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1352351844789066');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1352351844789066&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Intercom -->
<script>
  window.intercomSettings = {
    app_id: "pio8n28y",
    name: "<?= $session_data['full_name'] ?>", // Full name
    email: "<?= $session_data['email'] ?>", // Email address
    
  };
  </script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/pio8n28y';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
     
<div id="wrapper">
    <div class="w1">
    <!-- BEGIN HEADER INNER -->
    <header id="header" class="d-md-flex justify-content-between align-items-center">
		<div class="logo-holder">
		    <a class="logo" href="<?= site_url('hello') ?>"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/logo.svg" alt="Choose Ad Account"></a>
		</div>
		<a href="#" class="nav-opener">
			<span class="bar"></span>
			<span class="bar"></span>
			<span class="bar"></span>
		</a>
		<nav id="primary-nav">
			<div class="nav-holder">
				<div class="logo-holder d-md-none">
					<a class="logo" href="#"><img class="img-fluid" src="<?php echo $this->config->item('assets'); ?>newdesign/images/logo.svg" alt="The Campaign Maker"></a>
				</div>
				<?php if ($this->session->userdata('logged_in')) { ?>
    				<ul class="d-md-flex">
    				    <?php if($this->uri->segment(1) != 'trialactivate') {?>
						 <?php if($this->uri->segment(1) != 'dashboard') {?>
              	        <?php if($this->session->userdata['logged_in']['status'] == 1){ ?>
              	        
    					<li class="nav-item <?php if($this->uri->segment(1) == 'reports') echo 'active ';?>">
    						<a class="nav-link" href="<?= site_url('reports'); ?>">Report</a>
    					</li>
    					<li class="nav-item <?php if($this->uri->segment(1) == 'createcampaign') echo 'active ';?>">
    						<a class="nav-link" href="<?= site_url('createcampaign'); ?>">Create</a>
    					</li>
    					<?php } ?>
    					<li class="nav-item <?php if($this->uri->segment(1) == 'analysis') echo 'active ';?>">
    					    <?php if($this->session->userdata['user_subs']['packgid'] == '8'){ ?>
                            	<a class="nav-link" href="#" onclick="showupgradepopup()">Analyze</a>
                            <?php } else { ?>
                                <a class="nav-link" href="<?= site_url('analysis'); ?>">Analyze</a>
                            <?php } ?>
    						
    					</li>
    					<li class="nav-item <?php if($this->uri->segment(1) == 'automaticoptimization') echo 'active ';?>">
    					    <?php if($this->session->userdata['user_subs']['packgid'] == '8'){ ?>
                            	<a class="nav-link" href="#" onclick="showupgradepopup()">Optimize</a>
                            <?php } else { ?>
                                <a class="nav-link" href="<?= site_url('automaticoptimization'); ?>">Optimize</a>
                            <?php } ?>
    					</li>
    					<?php if($this->session->userdata['logged_in']['status'] == 1){ ?>
                        <li class="nav-item <?php if($this->uri->segment(1) == 'helps') echo 'active ';?>">
                            <a class="nav-link" href="https://thecampaignmaker.com/help/" target="_blank">Help</a>
                        </li>
                        <?php } ?>
    					
    					<?php if($this->session->userdata['logged_in']['status'] == 1){ ?>
                        <li class="nav-item <?php if($this->uri->segment(1) == 'adaccounts') echo 'active ';?>">
                            <a class="nav-link" href="<?= site_url('adaccounts'); ?>">Settings</a>
                        </li>
                        <?php } ?>
                        <?php } ?>
    					<?php if ($this->session->userdata('logged_in')) { ?>
    					<li class="nav-item log-out">
    						<a class="nav-link" href="<?= site_url('logout') ?>"><span class="icon-log-out"></span></a>
    					</li>
    					<?php } } ?>
    				</ul>
				<?php } ?>
			</div>
		</nav>
	</header>
	