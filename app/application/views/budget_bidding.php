<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-sm-8 col-md-10 col-lg-9 col-md-offset-1 displaynone tabcontent" id="step4">
  <div class="top-notification-box" id="msg_box4" style="display: none;">
      <div id="msg_err4"></div>
  </div>
  <div class="top-notification-box" id="successmsgBx4" style="display: none;">
      <div id="msg_suc4"></div>
  </div>
  <div class="main-content-wrapper">
    <div class="row">
      <div class="col-sm-11 col-md-5 col-lg-4">
        <div class="main-content-box inner">
          
            <div class="header-holder">
              <div class="btn-item budget selected">
                <span>
                  <i class="icon-bar-chart"></i>
                  <span class="text">Budget</span>
                </span>
              </div>
              <div class="text-wrap">
                <strong class="title">Budget Setup</strong>
              </div>
            </div>
            <div class="audience-setup-holder inner">
              <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <span>SET YOUR DAILY SPEND PER ADSET</span>
                    </strong>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body spend">
                      <div class="spend-set-holder">
                        <div class="input-group">
                          <div class="input-group-addon">
                              <?php echo $this->session->userdata('cur_currency'); ?>
                          </div>
                          <input type="text" id="exampleInputAmount" min="1" value="<?= isset($Cmpdraft->budgetammount) ? $Cmpdraft->budgetammount : "5" ?>" name="budgetAmount" onkeypress="return isNumber(event)" onkeyup="fnGetadset();" class="form-control">
                        </div>
                        <div class="review-box text-center">
                          <span class="review-title">EST. TOTAL SPEND PER DAY</span>
                          <span class="price" id="totalDailySpend"><?php echo $this->session->userdata('cur_currency'); ?>20</span>
                          <span class="divider">&nbsp;</span>
                          <span class="review-text" id="adsetSpend"><?php echo $this->session->userdata('cur_currency'); ?>5 X 4 ADSETS</span>
                          <p id="adsetCounts" style="display: none;"></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <span>WHAT DAYS OF THE WEEK SHOULD THE CAMPAIGN RUN?</span>
                    </strong>
                  </div>
                  <div id="collapseDays" class="panel-collapse collapse in">
                    <div class="panel-body spend">
                      <div class="inline-checkbox <?php echo $Cmpdraft->campaign_day; ?>">
                        <?php 
                          if(!empty($Cmpdraft->campaign_day)){ 
                            $dayArr = explode(',', $Cmpdraft->campaign_day);
                          ?>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?php if(in_array('1', $dayArr)){ echo 'checked'; } ?> id="mon" value="1" name="day[]">
                              <label for="mon">
                                Mon
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?= (strpos($Cmpdraft->campaign_day, '2') !== false) ? "checked" : "" ?> id="tues" value="2" name="day[]">
                              <label for="tues">
                                Tues
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?php if(in_array('3', $dayArr)){ echo 'checked'; } ?> id="wed" value="3" name="day[]">
                              <label for="wed">
                                Wed
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?= (strpos($Cmpdraft->campaign_day, '4') !== false) ? "checked" : "" ?> id="thur" value="4" name="day[]">
                              <label for="thur">
                                Thur
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?= (strpos($Cmpdraft->campaign_day, '5') !== false) ? "checked" : "" ?> id="fri" value="5" name="day[]">
                              <label for="fri">
                                Fri
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?= (strpos($Cmpdraft->campaign_day, '6') !== false) ? "checked" : "" ?> id="sat" value="6" name="day[]">
                              <label for="sat">
                                Sat
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?= (strpos($Cmpdraft->campaign_day, '0') !== false) ? "checked" : "" ?> id="sun" value="0" name="day[]">
                              <label for="sun">
                                Sun
                              </label>
                            </div>
                          <?php 
                          } 
                          else{
                          ?>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" checked="" id="mon" value="1" name="day[]">
                              <label for="mon">
                                Mon
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" checked="" id="tues" value="2" name="day[]">
                              <label for="tues">
                                Tues
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" checked="" id="wed" value="3" name="day[]">
                              <label for="wed">
                                Wed
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" checked="" id="thur" value="4" name="day[]">
                              <label for="thur">
                                Thur
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" checked="" id="fri" value="5" name="day[]">
                              <label for="fri">
                                Fri
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" checked="" id="sat" value="6" name="day[]">
                              <label for="sat">
                                Sat
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" checked="" id="sun" value="0" name="day[]">
                              <label for="sun">
                                Sun
                              </label>
                            </div>
                          <?php } ?>  
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <span>WHAT HOURS OF THE DAY SHOULD THE CAMPAIGN RUN?</span>
                    </strong>
                  </div>
                  <div id="collapseNumRabge" class="panel-collapse collapse in">
                    <div class="panel-body spend">
                      <div class="inline-checkbox">
                        <?php 
                          if(!empty($Cmpdraft->campaign_time)){ 
                            $timeArr = explode(',', $Cmpdraft->campaign_time);
                          ?>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?php if(in_array('1', $timeArr)){ echo 'checked'; } ?> id="one4" value="1" name="time[]">
                              <label for="one4">
                                1-4
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?php if(in_array('2', $timeArr)){ echo 'checked'; } ?> id="five8" value="2" name="time[]">
                              <label for="five8">
                                5-8
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?php if(in_array('3', $timeArr)){ echo 'checked'; } ?> id="nine12" value="3" name="time[]">
                              <label for="nine12">
                                9-12
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?php if(in_array('4', $timeArr)){ echo 'checked'; } ?> id="thir16" value="4" name="time[]">
                              <label for="thir16">
                                13-16
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?php if(in_array('5', $timeArr)){ echo 'checked'; } ?> id="seven20" value="5" name="time[]">
                              <label for="seven20">
                                17-20
                              </label>
                            </div>
                            <div class="checkbox checkbox-success">
                              <input type="checkbox" <?php if(in_array('6', $timeArr)){ echo 'checked'; } ?> id="twentyone2" value="6" name="time[]">
                              <label for="twentyone2">
                                21-24
                              </label>
                            </div>
                            <?php }
                            else{
                              ?>
                                <div class="checkbox checkbox-success">
                                  <input type="checkbox" checked="" id="one4" value="1" name="time[]">
                                  <label for="one4">
                                    1-4
                                  </label>
                                </div>
                                <div class="checkbox checkbox-success">
                                  <input type="checkbox" checked="" id="five8" value="2" name="time[]">
                                  <label for="five8">
                                    5-8
                                  </label>
                                </div>
                                <div class="checkbox checkbox-success">
                                  <input type="checkbox" checked="" id="nine12" value="3" name="time[]">
                                  <label for="nine12">
                                    9-12
                                  </label>
                                </div>
                                <div class="checkbox checkbox-success">
                                  <input type="checkbox" checked="" id="thir16" value="4" name="time[]">
                                  <label for="thir16">
                                    13-16
                                  </label>
                                </div>
                                <div class="checkbox checkbox-success">
                                  <input type="checkbox" checked="" id="seven20" value="5" name="time[]">
                                  <label for="seven20">
                                    17-20
                                  </label>
                                </div>
                                <div class="checkbox checkbox-success">
                                  <input type="checkbox" checked="" id="twentyone2" value="6" name="time[]">
                                  <label for="twentyone2">
                                    21-24
                                  </label>
                                </div>
                              <?php
                            }
                            ?>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="adset_start_date" id="adset_start_date" value="<?php echo date('m/d/Y'); ?>">
                <!--<div class="panel panel-default">
                  <div class="panel-heading">
                    <strong class="panel-title">
                      <a role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="true">
                        CHOOSE YOUR CAMPAIGN START DATE
                      </a>
                    </strong>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body calendar">
                      <div id="inline-datepicker">
                        <input type="hidden" name="adset_start_date" id="adset_start_date" value="<?php echo date('d/m/Y'); ?>">
                      </div>
                    </div>
                  </div>
                </div>-->
              </div>
            </div>
        </div>
        <div class="button-main add">
          <a class="btn btn-info" href="javascript:void(0)" onclick="jQuery('#tab_1_4').removeClass('active');jQuery('#tab_1_3').addClass('active');">Back to targeting</a>
          <button class="btn btn-success pull-right" type="button" id='Savebudgetbtn' onclick="ad_design_darafts('loader_drft2', 'direct');Savebudget();">Go To Review</button>
        </div>
      </div>
    </div>
  </div>
</div>