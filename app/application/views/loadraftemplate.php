<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="modal fade loadcampaign-popup" id="loadCampaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <a href="#" class="modal-close" data-dismiss="modal"><i class="icon-close"></i></a>
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-logo">
          <a href="#"><img src="<?php echo $this->config->item('assets'); ?>newdesign/images/logo-small-01.png" height="63" width="63" alt="logo"></a>
        </div>
        <div class="header-desc">
          <strong class="modal-title">LOAD YOUR CAMPAIGN</strong>
          <span class="campaign-info"><?php echo !empty($getcmpdrfts) ? count($getcmpdrfts) : ''; ?> Campaigns Available</span>
        </div>
      </div>
      <form action="#" class="campaign-search">
        <div class="sk3">
            <input type="hidden" name="userId-1" id="userId-1" value="<?php echo $user_id; ?>">
          <input type="search" name="searchKeyword" id="searchKeyword" class="form-control" placeholder="Search for your campaign" onkeyup="fnSearchLoadDraftCam()">
        </div>
      </form>
      <div class="modal-body" id="loadCampaignBody">
        <div class="campaigns-list">
            <?php foreach ($getcmpdrfts as $getcmpdrft) { ?>
                <div class="campaign-item" id="row_<?= $getcmpdrft->ID ?>">
                    <div class="campaign-left">
                      <strong class="campaign-name"><a href="#"><?= $getcmpdrft->campaignname ?></a></strong>
                      <div class="meta">
                        <span class="ico-holder"><i class="icon-single-user"></i></span>
                        <span class="campaign-category"><?= str_replace('_', ' ', $getcmpdrft->adobjective) ?></span>
                        <span class="time">created on <time datetime="2012-12-12"><?= date('m/d/Y', strtotime($getcmpdrft->CreationTime)) ?></time></span>
                      </div>
                    </div>
                    <a href="<?php echo site_url('createcampaign/' . $getcmpdrft->ID) . '/'; ?>" class="btn btn-load-campaign">LOAD CAMPAIGN</a>
                    <a id="<?php echo $getcmpdrft->ID; ?>" href="javascript:void(0)" class="btn btn-delete-campaign" onclick="deletedraftcampaign(<?php echo $getcmpdrft->ID; ?>);">DELETE</a>
                </div>
            <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
                