<?php defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');
if(strtolower($this->uri->segment(1)) == 'adaccounts'){
?>
    <main id="main">
       
				<div class="main-content">
					<div class="container">
        
        <!--<div class="container-fluid">-->
          
          <div class="ad-accout-settings">
							<!-- for tabset, add .tabset on tab-nav 
							<ul class="tab-nav d-flex flex-wrap justify-content-center">
								<li class="d-flex flex-wrap justify-content-center">
									<a class="d-sm-flex flex-wrap align-items-center" data-toggle="tab" href="#tab1-0">
										<span class="icon-holder icon-profile-settings"></span>
										<span class="text">Profile settings</span>
									</a>
								</li>
								<li class="active d-flex flex-wrap justify-content-center">
									<a class="d-sm-flex flex-wrap align-items-center" data-toggle="tab" href="#tab2-0">
										<span class="icon-holder icon-team"></span>
										<span class="text">Ad Account Settings</span>
									</a>
								</li>
							</ul>-->
							<!-- for tab, add .tab-content on .center-box -->
							<div class="center-box s7 tab-content">
								<!-- for tab, add appropriate id on .holder -->
								<div class="holder tab-pane fade in active" id="tab2-0">
									<h2 class="screen-heading text-center">Your Connected Ad Accounts</h2>
									
									<div class="ad-account s1">
										<div class="table-holder">
											<table class="table-s5">
											     <?php if (isset($adaccounts) && $adaccounts != NULL) {
											    
											     ?>  
											     
											 
											  
											     
											     <?php  foreach ($adaccounts as $adaccount) {  
											     
											      if($keytemp ==0){
                                                        $checked = "checked";
                                                    }else{
                                                        $checked = "";
                                                    }
											     
											     ?>
												<tr>
												<!--<td> <input id="ad-account-<?php echo $adaccount->ad_account_id; ?>" style="display:block" type="radio" name="ad-account" <?php echo $checked;  ?> value="<?php echo $adaccount->ad_account_id; ?>" ><td>   -->
												<td><?php echo $adaccount->add_title; ?><td>
												<td><?php echo $adaccount->ad_account_id; ?></td>
												</tr>
									
												 <?php } ?>  
												<?php } ?>
											</table>
										</div>
										
										
										<div class="btn-holder text-center">
											<a class="btn btn-primary" href="<?= site_url("adaccounts/get"); ?>">Update Your Ad Accounts</a>
										</div>
									</div>
									
									 <?php /* if ($this->session->flashdata('success')) { ?>
                <div class="top-notification-box">
                  <i class="icon-warning"></i>
                  <span class="text"><?= $this->session->flashdata('success') ?></span>
                </div>
            <?php } */ ?>  
        
									<div class="highlight-info">
										<div class="info-holder">
											<div class="icon-holder">
												<img src="<?php echo $this->config->item('assets');?>newdesign/images/icon-heart.svg" alt="heart">
											</div>
											<div class="text">
												<span>Need more settings? check out your <a href="https://thecampaignmaker.com/devganesh/settings">Account Settings</a> </span>
											</div>
										</div>
									</div>
								</div>
								<!--<div class="holder profile-settings tab-pane fade" id="tab1-0">
									<div class="content-row">
										<h2 class="profile-heading">Profile Information</h2>
										<div class="item-holder d-flex flex-wrap">
											<div class="profile-item d-flex flex-wrap">
												<div class="item-wrap d-flex flex-wrap align-items-center">
													<div class="icon-holder">
														<img src="<?php echo $this->config->item('assets');?>newdesign/images/icon-badge-rounded.svg" alt="badge">
													</div>
													<div class="text">
														<span class="data">Samy</span>
														<span class="label">First Name</span>
													</div>
												</div>
											</div>
											<div class="profile-item d-flex flex-wrap">
												<div class="item-wrap d-flex flex-wrap align-items-center">
													<div class="icon-holder">
														<img src="<?php echo $this->config->item('assets');?>newdesign/images/icon-crown-rounded.svg" alt="crown">
													</div>
													<div class="text">
														<span class="data">Zankaroont</span>
														<span class="label">Last Name</span>
													</div>
												</div>
											</div>
											<div class="profile-item d-flex flex-wrap">
												<div class="item-wrap d-flex flex-wrap align-items-center">
													<div class="icon-holder">
														<img src="<?php echo $this->config->item('assets');?>newdesign/images/icon-letter-rounded.svg" alt="letter">
													</div>
													<div class="text">
														<span class="data">Dummy.Email@gmail.com</span>
														<span class="label">Account Email</span>
													</div>
												</div>
											</div>
											<div class="profile-item d-flex flex-wrap">
												<div class="item-wrap d-flex flex-wrap align-items-center">
													<div class="icon-holder">
														<img src="<?php echo $this->config->item('assets');?>newdesign/images/icon-megaphone-rounded.svg" alt="megaphone">
													</div>
													<div class="text">
														<span class="data">Dummy.Email2@gmail.com</span>
														<span class="label">Facebook Email</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="content-row">
										<h2 class="profile-heading">Account Subscription</h2>
										<div class="item-holder d-flex flex-wrap with-label">
											<div class="profile-item d-flex flex-wrap">
												<div class="item-wrap d-flex flex-wrap align-items-center">
													<div class="icon-holder">
														<img src="<?php echo $this->config->item('assets');?>newdesign/images/icon-paper-plane-rounded.svg" alt="paper plane">
													</div>
													<div class="text">
														<span class="data">Master Account Monthly</span>
														<span class="label">Account Type</span>
													</div>
												</div>
											</div>
											<div class="profile-item d-flex flex-wrap tag-label-item">
												<div class="item-wrap d-flex flex-wrap align-items-center">
													<div class="icon-holder">
														<img src="<?php echo $this->config->item('assets');?>newdesign/images/icon-wallet-rounded.svg" alt="wallet">
													</div>
													<div class="text">
														<span class="data"><time datetime="2018-11-02">2/11/2018</time></span>
														<span class="label">Billing End Date</span>
													</div>
												</div>
												<div class="profile-item-info">
													<span>CANCEL SUBSCRIPTION</span>
												</div>
											</div>
										</div>
									</div>
									<div class="highlight-info">
										<div class="info-holder">
											<div class="icon-holder">
												<img src="<?php echo $this->config->item('assets');?>newdesign/images/icon-heart.svg" alt="heart">
											</div>
											<div class="text">
												<span>Need help? Click the chat icon on the right side </span>
											</div>
										</div>
									</div>
								</div> -->
							</div>
						</div>
          
           <!--</div>-->
           
           </div>
            </div>
        </div>
      </main>
      <!-- Main Content Ends -->
    </div>
<?php } else { ?>
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('menu'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <div class="col-md-7  p-rl-3">
                        <h3 class="page-title">
                            My Ad Accounts
                        </h3>
                    </div>
                    <!--<div class="col-md-5 p-rl-3">
                        <div class="upgrade-now">
                            <span>Your trial will expire in 3 day(s)</span> <button class="btn btn-sm blue"><i class="fa fa-level-up"></i> Upgrade Now </button>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo base_url() . 'reports'; ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="javascript:void(0)">My Ad Accounts</a>
                        </li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN table-->
                    <div class="portlet box blue-hoki">
                        <div class="portlet-title">
                            <div class="caption">
                                My Ad Accounts
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="">
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                                <?php } ?>
                                <?php if (isset($adaccounts) && $adaccounts != NULL) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px;">
                                                    Ad Account Name
                                                </th>
                                                <th>
                                                    Ad Account ID
                                                </th>
                                                <th>
                                                    Status
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($adaccounts as $adaccount) {
                                                ?>
                                                <tr> 
                                                    <td><?php echo $adaccount->add_title; ?></td>
                                                    <td><?php echo $adaccount->ad_account_id; ?></td>
                                                    <td><?php
                                                        if ($adaccount->status == 1) {
                                                            echo "active";
                                                        } else {
                                                            echo "Not active";
                                                        }
                                                        ?></td>
                                                    <td><a href="<?php echo site_url('reports/' . $adaccount->ad_account_id); ?>">View Reports</a></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                <?php } ?>
                                    <div class="clearfix"></div>
                                    <div class="col-md-2 col-md-offset-5 start-saving">
                                        <a class="btn btn-primary" href="<?= site_url("adaccounts/get"); ?>">Update Facebook Account(s)</a>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <!-- END table-->
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<?php } ?>