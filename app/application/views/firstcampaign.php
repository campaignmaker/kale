<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$session_data = $this->session->userdata('logged_in');

?>

 <!-- Main Content Starts -->
      <main id="main">
        <div class="container">
          <div class="welcome-dashboard-container">
            <div class="welcome-header text-center">
              <h1>Congrats, <?= @ucfirst($session_data['first_name']) ?></h1>
              <p>You’re ready to start saving time and money managing your new awesome Facebook Campaigns.</p>
            </div>
            <strong class="title-text">STEP 1 - WATCH THIS QUICK START VIDEO</strong>
            <div class="youtube-video">
              <iframe src="https://www.youtube.com/embed/2atoEPcAvec?rel=0&amp;showinfo=0" allowfullscreen></iframe>
            </div>
            <strong class="title-text">STEP 2 - DOWNLOAD THE COMPLETE FACEBOOK GUIDE EBOOK</strong>
            <div class="ebook-box">
              <a target="_blank" href="http://bit.ly/2n5bR4d" class="btn btn-info btn-small">DOWNLOAD EBOOK</a>
            </div>
            <strong class="title-text">STEP 3 - GET STARTED!</strong>
            <div class="get-started-box">
              <a href="<?php echo base_url(); ?>createcampaign" class="btn btn-danger">CREATE NEW CAMPAIGN</a>
              <span class="or-text">OR</span>
              <a href="<?php echo base_url(); ?>reports" class="btn btn-green">YOUR REPORTING DASHBOARD</a>
            </div>
          </div>
        </div>
      </main>
      <!-- Main Content Ends -->
    </div>


<!-- Google Code for TCM Registration Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 985336567;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "ea7PCPia2GkQ95Xs1QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/985336567/?label=ea7PCPia2GkQ95Xs1QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1352351844789066'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1352351844789066&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<script>
fbq('track', 'Lead');
</script>

<img src="https://thecampaignmaker.com/aff/sale.php?profile=44&idev_leadamt=0.01&idev_ordernum=1" style="height:0px; width:0px; border:0px;" />

 <script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load("1418879561");
</script>


