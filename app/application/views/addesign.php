<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="col-sm-8 col-md-10 col-lg-9 col-md-offset-1 displaynone tabcontent" id="step2">
      <div class="top-notification-box" id="msg_box2" style="display: none;">
          <div id="msg_err2"></div>
      </div>
      <div class="top-notification-box" id="successmsgBx2" style="display: none;">
          <div id="msg_suc2"></div>
      </div>
      <div class="main-content-wrapper">
        <div class="row">
          <div class="col-md-6">
            <div class="main-content-box inner">
                <div class="header-holder with-btn">
                  <div class="text-wrap">
                    <strong class="title">Setting Up Your Ads</strong>
                  </div>
                  <div class="btn-holder">
                <!-- <a href="<?php //echo $this->config->item('base_url'); ?>helps" class="btn-help"><span class="icon-video"></span>Need Help?</a> -->
                    <a href="javascript:;" class="btn-help bmd-modalButton" data-toggle="modal" data-bmdSrc="https://www.youtube.com/embed/uEP4HqFY_xc" data-bmdWidth="640" data-bmdHeight="480" data-target="#myModaliframe"  data-bmdVideoFullscreen="true"><span class="icon-video"></span>Need Help?</a>
                    
                   <!-- <button type="button" class="bmd-modalButton" data-toggle="modal" data-bmdSrc="https://www.youtube.com/embed/cMNPPgB0_mU" data-bmdWidth="640" data-bmdHeight="480" data-target="#myModaliframe"  data-bmdVideoFullscreen="true">Youtube</button> -->
                    <div class="modal fade" id="myModaliframe">
                        <div class="modal-dialog">
                            <div class="modal-content bmd-modalContent">
                
                                <div class="modal-body">
                          
                          <div class="close-button">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          </div>
                          <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" frameborder="0"></iframe>
                          </div>
                                </div>
                
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                  </div>
                </div>
                <div class="inputs-holder">
                  <strong id="titstep1" class="title with-bg">Step 1 - Choose Your Facebook Page</strong>
                  <div class="select-wrap">
                  	<input type="hidden" name="creatives[adaccountid][]" id="adaccountid_1" value="<?=isset($Cmpdraft->adaccount)?$Cmpdraft->adaccount:''?>" />
                    
                    <select id="objectIdpage" name="creatives[object_id][]" onchange="get_pages_tabs();">
                      <option value="">Choose Your Facebook Page</option>
                      <?php foreach ($getUsersPages as $page) { ?>
                            <option value="<?php echo $page['id'] ?>" <?= ($Cmpdraft->pageid == $page['id']) ? "selected" : ""; ?>><?php echo $page['name'] ?></option>
                        <?php } ?>
                    </select>
                  </div>
                  
                  <div class="select-wrap POST_ENGAGEMENT_OBJ displayNone ">
                  	<strong class="title with-bg">Step 2 - Choose Your Post</strong>
                    <select name="creatives[story_id]" id="storyId" onchange="LoeadPreview();" class="thisispostselect">

                    </select>
                  </div>
                  
                  <div class="input-wrap LINK_CLICKS_OBJ LINK_CLICKS_OBJ1 CONVERSIONS_OBJ LEAD_GENERATION_OBJ PAGE_LIKES_OBJ_Head" id="url_fileds">
                    <strong id="titstep2" class="title with-bg" <?= ("PAGE_LIKES" == $Cmpdraft->adobjective || "LEAD_GENERATION" == $Cmpdraft->adobjective) ? "style='display:none;'" : "" ?>>Step 2 - Enter Your Destination URL</strong>
                    <div class="form-group validation url_del_div">
                      <input type="text" class="form-control addfield_textbox" name="creatives[link_url][]" id='creatives_link_url1' placeholder="Target URL" onchange="LoeadPreview();"  onblur="stickyCounter(this.value, 'url')" value="<?= $Cmpdraft->adurl1 ?>">
                      <span class="validate-info">*</span>
                      <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                      <a href="#" class="add-input" id="more_url_fields"><i class="icon-plus"></i></a>
                      <?php } else { ?>
                      <a href="#" class="add-input" onclick="showupgradepopup()"><i class="icon-plus"></i></a>
                      <?php } ?>
                    </div>
                    <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                    	<?php if(!empty($Cmpdraft->adurl2)) { ?>
                        <div class="form-group validation url_del_div">
                            <input name="creatives[link_url][]" id="creatives_link_url2" onblur="stickyCounter(this.value, 'url')" class="form-control addfield_textbox" placeholder="Target URL" type="text" value="<?= $Cmpdraft->adurl2 ?>">
                            <a class="delete-url-field add-input">
                                <i class="fa fa-trash-o"></i>
                            </a>
                                <div class="clearfix">
                                </div>
                        </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->adurl3)) { ?>
                        <div class="form-group validation url_del_div">
                            <input name="creatives[link_url][]" id="creatives_link_url2" onblur="stickyCounter(this.value, 'url')" class="form-control addfield_textbox" placeholder="Target URL" type="text" value="<?= $Cmpdraft->adurl3 ?>">
                            <a class="delete-url-field add-input">
                                <i class="fa fa-trash-o"></i>
                            </a>
                                <div class="clearfix">
                                </div>
                        </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->adurl4)) { ?>
                        <div class="form-group validation url_del_div">
                            <input name="creatives[link_url][]" id="creatives_link_url2" onblur="stickyCounter(this.value, 'url')" class="form-control addfield_textbox" placeholder="Target URL" type="text" value="<?= $Cmpdraft->adurl4 ?>">
                            <a class="delete-url-field add-input">
                                <i class="fa fa-trash-o"></i>
                            </a>
                                <div class="clearfix">
                                </div>
                        </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->adurl5)) { ?>
                        <div class="form-group validation url_del_div">
                            <input name="creatives[link_url][]" id="creatives_link_url2" onblur="stickyCounter(this.value, 'url')" class="form-control addfield_textbox" placeholder="Target URL" type="text" value="<?= $Cmpdraft->adurl5 ?>">
                            <a class="delete-url-field add-input">
                                <i class="fa fa-trash-o"></i>
                            </a>
                                <div class="clearfix">
                                </div>
                        </div>
                        <?php } ?>
                    <?php } ?>
                    
                  </div>

                  <div class="input-wrap LINK_CLICKS_OBJ LINK_CLICKS_OBJ1 CONVERSIONS_OBJ LEAD_GENERATION_OBJ PAGE_LIKES_OBJ_Head">
                    <div class="form-group validation">
                      <input type="text" class="form-control" name="creatives[link_caption][]" id="creatives_link_caption"  
                                   value="<?= $Cmpdraft->displaylink ?>" placeholder="Display URL">
                      <span class="validate-info">*</span>
                    </div>
                  </div>

                  <div class="input-wrap PAGE_LIKES_OBJ_Head1 displayNone PAGE_LIKES_OBJ_Head" id="url_fileds">
                    <strong class="title with-bg" id="titstep31">Step 2 - Enter Your URL And Body Text</strong>
                    <div class="form-group validation url_del_div">
                      <input type="text" class="form-control addfield_textbox" name="products_link" id='products_link' placeholder="Insert URL and use + to test more...." value="<?= $Cmpdraft->productslink ?>">
                    </div>
                  </div>
                  
                  
                  <div class="input-wrap PAGE_LIKES_OBJ_Head PAGE_LIKES_OBJ_Head2" id="headline_fileds">
                    <strong class="title with-bg" id="titstep3"><?= ("LEAD_GENERATION" == $Cmpdraft->adobjective || "PAGE_LIKES" == $Cmpdraft->adobjective) ? "Step 2" : "Step 3" ?> - Write Your Adcopy</strong>
                    <div class="form-group validation headline_del_div">
                      <input type="text" placeholder="Headline" class="form-control addfield_textbox" name="creatives[title][]" onblur="stickyCounter(this.value, 'headline')" id="creatives_title1" value="<?= $Cmpdraft->headlines1 ?>">
                      <span class="validate-info">*</span>
                      <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                      <a href="#" class="add-input" id="more_headline_fields"><i class="icon-plus"></i></a>
                      <?php } else { ?>
                      <a href="#" class="add-input" onclick="showupgradepopup()"><i class="icon-plus"></i></a>
                      <?php } ?>
                    </div>
                    	<?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                    		<?php if(!empty($Cmpdraft->headlines2)) { ?>
                                <div class="form-group validation headline_del_div">
                                    <input placeholder="Headline" id="creatives_title1" onblur="stickyCounter(this.value, 'headline')" name="creatives[title][]" class="form-control addfield_textbox" type="text" value="<?= $Cmpdraft->headlines2 ?>">
                                    <a class="delete-headline-field add-input"><i class="fa fa-trash-o"></i></a>
                                    <div class="clearfix"></div>
                                </div>
                        	<?php } ?>
                            <?php if(!empty($Cmpdraft->headlines3)) { ?>
                                <div class="form-group validation headline_del_div">
                                    <input placeholder="Headline" id="creatives_title1" onblur="stickyCounter(this.value, 'headline')" name="creatives[title][]" class="form-control addfield_textbox" type="text" value="<?= $Cmpdraft->headlines3 ?>">
                                    <a class="delete-headline-field add-input"><i class="fa fa-trash-o"></i></a>
                                    <div class="clearfix"></div>
                                </div>
                        	<?php } ?>
                            <?php if(!empty($Cmpdraft->headlines4)) { ?>
                                <div class="form-group validation headline_del_div">
                                    <input placeholder="Headline" id="creatives_title1" onblur="stickyCounter(this.value, 'headline')" name="creatives[title][]" class="form-control addfield_textbox" type="text" value="<?= $Cmpdraft->headlines4 ?>">
                                    <a class="delete-headline-field add-input"><i class="fa fa-trash-o"></i></a>
                                    <div class="clearfix"></div>
                                </div>
                        	<?php } ?>
                            <?php if(!empty($Cmpdraft->headlines5)) { ?>
                                <div class="form-group validation headline_del_div">
                                    <input placeholder="Headline" id="creatives_title1" onblur="stickyCounter(this.value, 'headline')" name="creatives[title][]" class="form-control addfield_textbox" type="text" value="<?= $Cmpdraft->headlines5 ?>">
                                    <a class="delete-headline-field add-input"><i class="fa fa-trash-o"></i></a>
                                    <div class="clearfix"></div>
                                </div>
                        	<?php } ?>
                         <?php } ?>   
                  </div>
                  
                  <div class="input-wrap PAGE_LIKES_OBJ_Head2" id="bodytext_fileds">
                    <strong class="title with-bg" <?= "PAGE_LIKES" == $Cmpdraft->adobjective ? "" : "style='display:none;'" ?> id="titpagelikestep2">Step 2 - Write Your Adcopy</strong>
                    <div class="form-group validation bodytext_del_div">
                      <textarea type="text" class="form-control addfield_textbox" name="creatives[body][]" onblur="stickyCounter(this.value, 'body')" id="creatives_body1" placeholder="Body Text"><?= $Cmpdraft->adbody1 ?></textarea>
                      
                      <span class="validate-info">*</span>
                      <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                      <a href="#" class="add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" id="more_bodytext_fields"><i class="icon-plus"></i></a>
                      <?php } else { ?>
                      <a href="#" class="add-input PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1" onclick="showupgradepopup()"><i class="icon-plus"></i></a>
                      <?php } ?>
                    </div> 
                    <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                    	<?php if(!empty($Cmpdraft->adbody2)) { ?>
                    	<div class="form-group validation bodytext_del_div">
                        	<textarea placeholder="Body Text" id="creatives_body1" onblur="stickyCounter(this.value, 'body')" name="creatives[body][]" class="form-control addfield_textbox" type="text"><?= $Cmpdraft->adbody2 ?></textarea>
                            	<a class="delete-bodytext-field add-input"><i class="fa fa-trash-o"></i></a>
                                <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->adbody3)) { ?>
                    	<div class="form-group validation bodytext_del_div">
                        	<textarea placeholder="Body Text" id="creatives_body1" onblur="stickyCounter(this.value, 'body')" name="creatives[body][]" class="form-control addfield_textbox" type="text"><?= $Cmpdraft->adbody3 ?></textarea>
                            	<a class="delete-bodytext-field add-input"><i class="fa fa-trash-o"></i></a>
                                <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->adbody4)) { ?>
                    	<div class="form-group validation bodytext_del_div">
                        	<textarea placeholder="Body Text" id="creatives_body1" onblur="stickyCounter(this.value, 'body')" name="creatives[body][]" class="form-control addfield_textbox" type="text"><?= $Cmpdraft->adbody4 ?></textarea>
                            	<a class="delete-bodytext-field add-input"><i class="fa fa-trash-o"></i></a>
                                <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->adbody5)) { ?>
                    	<div class="form-group validation bodytext_del_div">
                        	<textarea placeholder="Body Text" id="creatives_body1" onblur="stickyCounter(this.value, 'body')" name="creatives[body][]" class="form-control addfield_textbox" type="text"><?= $Cmpdraft->adbody5 ?></textarea>
                            	<a class="delete-bodytext-field add-input"><i class="fa fa-trash-o"></i></a>
                                <div class="clearfix"></div>
                        </div>
                        <?php } ?>   
                    <?php } ?>                    
                  </div>

                  <div class="input-wrap PAGE_LIKES_OBJ_Head PAGE_LIKES_OBJ_Head2" id="description_fileds">
                    <div class="form-group validation description_del_div">
                      <textarea type="text" class="form-control addfield_textbox" name="creatives[link_description][]" onblur="stickyCounter(this.value, 'desc')" id="creatives_link_description1" placeholder="Description"><?= $Cmpdraft->linkdescription1 ?></textarea>
                      
                      <span class="validate-info">*</span>
                      <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                      <a href="#" class="add-input" id="more_description_fields"><i class="icon-plus"></i></a>
                      <?php } else { ?>
                      <a href="#" class="add-input" onclick="showupgradepopup()"><i class="icon-plus"></i></a>
                      <?php } ?>
                    </div>
                    <?php if($this->session->userdata['user_subs']['packgid'] != '8'){ ?>
                    	 <?php if(!empty($Cmpdraft->linkdescription2)) { ?>
                            <div class="form-group validation description_del_div">
                                <textarea onblur="stickyCounter(this.value, 'desc')" placeholder="Description" id="creatives_link_description1" name="creatives[link_description][]" class="form-control addfield_textbox" type="text"><?= $Cmpdraft->linkdescription2 ?></textarea>
                                    <a class="delete-description-field add-input"><i class="fa fa-trash-o"></i></a>
                                    <div class="clearfix"></div>
                            </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->linkdescription3)) { ?>
                            <div class="form-group validation description_del_div">
                                <textarea onblur="stickyCounter(this.value, 'desc')" placeholder="Description" id="creatives_link_description1" name="creatives[link_description][]" class="form-control addfield_textbox" type="text"><?= $Cmpdraft->linkdescription3 ?></textarea>
                                    <a class="delete-description-field add-input"><i class="fa fa-trash-o"></i></a>
                                    <div class="clearfix"></div>
                            </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->linkdescription4)) { ?>
                            <div class="form-group validation description_del_div">
                                <textarea onblur="stickyCounter(this.value, 'desc')" placeholder="Description" id="creatives_link_description1" name="creatives[link_description][]" class="form-control addfield_textbox" type="text"><?= $Cmpdraft->linkdescription4 ?></textarea>
                                    <a class="delete-description-field add-input"><i class="fa fa-trash-o"></i></a>
                                    <div class="clearfix"></div>
                            </div>
                        <?php } ?>
                        <?php if(!empty($Cmpdraft->linkdescription5)) { ?>
                            <div class="form-group validation description_del_div">
                                <textarea onblur="stickyCounter(this.value, 'desc')" placeholder="Description" id="creatives_link_description1" name="creatives[link_description][]" class="form-control addfield_textbox" type="text"><?= $Cmpdraft->linkdescription5 ?></textarea>
                                    <a class="delete-description-field add-input"><i class="fa fa-trash-o"></i></a>
                                    <div class="clearfix"></div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                  </div>

                  <div class="select-wrap PAGE_LIKES_OBJ_Head creatives_link_url_wrap PAGE_LIKES_OBJ_Head2">
                    <select class="form-control call-to" name="creatives[call_to_action][]" id="callToAction" onchange="LoeadPreview();">
                      <option value="SHOP_NOW"  <?= $Cmpdraft->calltoaction == 'SHOP_NOW' ? "selected" : "" ?>>Shop now</option>
                      <option value="BOOK_TRAVEL"<?= $Cmpdraft->calltoaction == 'BOOK_TRAVEL' ? "selected" : "" ?>>Book now</option>
                      <option value="LEARN_MORE"<?= $Cmpdraft->calltoaction == 'LEARN_MORE' ? "selected" : "" ?>>Learn more</option>
                      <option value="SIGN_UP"<?= $Cmpdraft->calltoaction == 'SIGN_UP' ? "selected" : "" ?>>Sign up</option>
                      <option value="DOWNLOAD"<?= $Cmpdraft->calltoaction == 'DOWNLOAD' ? "selected" : "" ?>>Download</option>
                      <option value="WATCH_MORE"<?= $Cmpdraft->calltoaction == 'WATCH_MORE' ? "selected" : "" ?>>Watch more</option>
                      <!--<option value="WATCH_VIDEO"<?= $Cmpdraft->calltoaction == 'WATCH_VIDEO' ? "selected" : "" ?>>Watch video</option>-->
                      <option value="LISTEN_MUSIC"<?= $Cmpdraft->calltoaction == 'LISTEN_MUSIC' ? "selected" : "" ?>>Listen Now</option>
                      <option value="INSTALL_APP"<?= $Cmpdraft->calltoaction == 'INSTALL_APP' ? "selected" : "" ?>>Install Now</option>
                      <option value="USE_APP"<?= $Cmpdraft->calltoaction == 'USE_APP' ? "selected" : "" ?>>Use App</option>
                      <option value="NO_BUTTON"<?= $Cmpdraft->calltoaction == 'NO_BUTTON' ? "selected" : "" ?>>No Button</option>
                    </select>
                  </div>

                  <div class="select-wrap form-group PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide callToAction3  PAGE_LIKES_OBJ_Head2" style="display:none;">
                
                    <select class="form-control call-to" name="creatives[call_to_action3][]" id="callToAction3" onchange="LoeadPreview();">
                      <option value="DOWNLOAD"<?= $Cmpdraft->calltoaction3 == 'DOWNLOAD' ? "selected" : "" ?>>Download</option>
                      <option value="GET_QUOTE"<?= $Cmpdraft->calltoaction3 == 'GET_QUOTE' ? "selected" : "" ?>>Get quote</option>
                      <option value="LEARN_MORE"<?= $Cmpdraft->calltoaction3 == 'LEARN_MORE' ? "selected" : "" ?>>Learn more</option>
                      <option value="SIGN_UP"<?= $Cmpdraft->calltoaction3 == 'SIGN_UP' ? "selected" : "" ?>>Sign up</option>
                      <option value="SUBSCRIBE"<?= $Cmpdraft->calltoaction3 == 'SUBSCRIBE' ? "selected" : "" ?>>Subscribe</option>
                      <option value="APPLY_NOW"<?= $Cmpdraft->calltoaction3 == 'APPLY_NOW' ? "selected" : "" ?>>Apply Now</option>
                      <option value="NO_BUTTON"<?= $Cmpdraft->calltoaction3 == 'NO_BUTTON' ? "selected" : "" ?>>No Button</option>
                    </select>
                  </div>

                  <div class="facebookpage-ads PAGE_LIKES_OBJ displayNone">
                    <ul id="tab_list" class="list-multiple jsList"></ul>
                    <div class="pos_relative">
                        <i class="fa fa-spinner fa-spin storyId_spin displayNone"></i>
                    </div>
                  </div>

                  <div class="select-wrap form-group LeadGenerationFormBox displayNone PAGE_LIKES_OBJ_Head2">
                  	<strong class="title with-bg" id="titleadformstep3">Step 3 - Select Your Lead Form</strong>
                    <select class="form-control call-to" name="creatives[lead_gen_form][]" id="leadGenForm">
                        <option value="">Please select Lead Gen form...</option>
                        
                    </select>
                    <button style="margin-top: 15px; margin-bottom: 15px;" class="btn btn-lg green m-icon ad_new_frm" id="ad_new_frm" disabled="" onclick="return lead_gen_frm();">Or Create a new form <i class="fa fa-file"></i></button>
                    <div class="alert alert-warning" id="jsNoTosAcceptedAlert" style="display: block;">
                        <div class="text">
                            <h4 class="alert-heading">
                                <i class="icon-check"></i>Warning                    </h4>
                            <p>It seems you have not accepted the TOS for the selected page.<br>
                                Make sure you <a href="javascript:;" id="jsAcceptTosBtn" onclick="return fb_termandcanditions();">read and accept</a> the TOS in order to proceed.</p>
                        </div>
                    </div>
                  </div>
                </div>
				
                <div class="images-option-holder uploadimg-ads ads-boximg POST_ENGAGEMENT_OBJ_Hide PAGE_LIKES_OBJ_Head POST_ENGAGEMENT_OBJ_Hide1">
                <strong class="title with-bg" id="titstep4" style="margin-left:-15px;"><?= ("PAGE_LIKES" == $Cmpdraft->adobjective) ? "Step 3" : "Step 4" ?> - Select/Upload Your Media</strong>
                <div class="wrap">
                    <div class="row">
                      <div class="col-xs-6">
                      <a href="javascript:;" style="text-decoration:none;" class="upld-lib-btn" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery');">
                        <div class="image-item image-library">
                          <i class="icon-image"></i>
                          <span class="text">IMAGE LIBRARY</span>
                        </div>
                      </a>  
                      </div>
                      <div class="col-xs-6">
                      <a href="javascript:;" style="text-decoration:none;" class="upld-video-btn" onclick="fngetallvideos('<?php echo $userId; ?>', 'gallery-video');">
                        <div class="image-item video-library">
                          <i class="icon-video"></i>
                          <span class="text">VIDEO LIBRARY</span>
                        </div>
                      </a>  
                      </div>
                    </div>
                  </div>
                
                
                 <!-- <div class="wrap">
                    <div class="row">
                      <div class="col-xs-4">
                      	<a href="javascript:;" class="upld-lib-btn" onclick="fngetallimages('<?php //echo $userId; ?>', 'gallery');">
                        <div class="image-item">
                          <span>
                            <i class="icon-library"></i>
                            <span class="text">Images</span>
                          </span>
                        </div>
                        </a>
                      </div>
                      <div class="col-xs-4" style="display:none;">
                        <a href="javascript:;" class="upld-img-btn">
                          <div class="image-item">
                            <span>
                              <i class="icon-image"></i>
                              <span class="text">Image</span>
                            </span>
                          </div>
                        </a>
                      </div>
                      <div class="col-xs-4">
                        <a href="javascript:;" class="upld-video-btn" onclick="fngetallvideos('<?php //echo $userId; ?>', 'gallery-video');">
                          <div class="image-item">
                            <span>
                              <i class="icon-library"></i>
                              <span class="text">Videos</span>
                            </span>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div> -->
                  <div class="wrap-new">
                    <div class="row upld-img">
                        <div id="uploadedImages">
                          <?php if($Cmpdraft->imgType == 'video'){?>
                            <?php if (!empty($Cmpdraft->adimage1)) { ?>
                                <input type="hidden" class="<?= $Cmpdraft->adimage1 ?>" id="viewUrl" name="creatives[viewUrl][]" value="<?= $Cmpdraft->adimage1 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->fbimghash1 ?>' class="<?= $Cmpdraft->fbimghash1 ?>" />
                            <?php } ?>
                            <?php if (!empty($Cmpdraft->adimage2)) { ?>
                                <input type="hidden" class="<?= $Cmpdraft->adimage2 ?>" id="viewUrl" name="creatives[viewUrl][]" value="<?= $Cmpdraft->adimage2 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->fbimghash2 ?>' class="<?= $Cmpdraft->fbimghash2 ?>" />
                            <?php } ?>
                            <?php if (!empty($Cmpdraft->adimage3)) { ?>
                                <input type="hidden" class="<?= $Cmpdraft->adimage3 ?>" id="viewUrl" name="creatives[viewUrl][]" value="<?= $Cmpdraft->adimage3 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->fbimghash3 ?>' class="<?= $Cmpdraft->fbimghash3 ?>" />
                            <?php } ?>
                            <?php if (!empty($Cmpdraft->adimage4)) { ?>
                                <input type="hidden" class="<?= $Cmpdraft->adimage4 ?>" id="viewUrl" name="creatives[viewUrl][]" value="<?= $Cmpdraft->adimage4 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->fbimghash4 ?>' class="<?= $Cmpdraft->fbimghash4 ?>" />
                            <?php } ?>
                            <?php if (!empty($Cmpdraft->adimage5)) { ?>
                                <input type="hidden" class="<?= $Cmpdraft->adimage5 ?>" id="viewUrl" name="creatives[viewUrl][]" value="<?= $Cmpdraft->adimage5 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->fbimghash5 ?>' class="<?= $Cmpdraft->fbimghash5 ?>" />
                            <?php } ?>
                          <?php }else{ ?>
                            <?php if (!empty($Cmpdraft->adimage1)) { $imgApp1 = explode('.', $Cmpdraft->adimage1);?>
                                <input type="hidden" class="<?= $imgApp1[0].'-gallery' ?>" id="viewUrl" name="creatives[viewUrl][]" value="https://scontent.xx.fbcdn.net/t45.1600-4/<?= $Cmpdraft->adimage1 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->adimagehash1 ?>' class="<?= $imgApp1[0].'-gallery' ?>" />
                            <?php } ?>
                            <?php if (!empty($Cmpdraft->adimage2)) { $imgApp2 = explode('.', $Cmpdraft->adimage2);?>
                                <input type="hidden" class="<?= $imgApp2[0].'-gallery' ?>" id="viewUrl" name="creatives[viewUrl][]" value="https://scontent.xx.fbcdn.net/t45.1600-4/<?= $Cmpdraft->adimage2 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->adimagehash2 ?>' class="<?= $imgApp2[0].'-gallery' ?>" />
                            <?php } ?>
                            <?php if (!empty($Cmpdraft->adimage3)) { $imgApp3 = explode('.', $Cmpdraft->adimage3);?>
                                <input type="hidden" class="<?= $imgApp3[0].'-gallery' ?>" id="viewUrl" name="creatives[viewUrl][]" value="https://scontent.xx.fbcdn.net/t45.1600-4/<?= $Cmpdraft->adimage3 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->adimagehash3 ?>' class="<?= $imgApp3[0].'-gallery' ?>" />
                            <?php } ?>
                            <?php if (!empty($Cmpdraft->adimage4)) { $imgApp4 = explode('.', $Cmpdraft->adimage4);?>
                                <input type="hidden" class="<?= $imgApp4[0].'-gallery' ?>" id="viewUrl" name="creatives[viewUrl][]" value="https://scontent.xx.fbcdn.net/t45.1600-4/<?= $Cmpdraft->adimage4 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->adimagehash4 ?>' class="<?= $imgApp4[0].'-gallery' ?>" />
                            <?php } ?>
                            <?php if (!empty($Cmpdraft->adimage5)) { $imgApp5 = explode('.', $Cmpdraft->adimage5);?>
                                <input type="hidden" class="<?= $imgApp5[0].'-gallery' ?>" id="viewUrl" name="creatives[viewUrl][]" value="https://scontent.xx.fbcdn.net/t45.1600-4/<?= $Cmpdraft->adimage5 ?>">
                                <input type='hidden' name='fbimghash[]' value='<?= $Cmpdraft->adimagehash5 ?>' class="<?= $imgApp5[0].'-gallery' ?>" />
                            <?php } ?>
                          <?php }?>
                        </div>
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-img">
                          <label class="col-md-12 control-label">Upload Image</label>
                          <div class="col-md-12">
                              <div action="<?php echo $this->config->item('assets'); ?>global/plugins/dropzone/upload.php?userId=<?php echo $userId; ?>" class="dropzone" id="myDropzone">
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="row upld-lib">
                      <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-lib">
                        <label class="col-md-12 control-label ">Choose From Library</label>
                        <div id="imglibgallery" class="feedloader" style="display: none;">
                    <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
                </div>
                <div class="row">
                        <div class="col-md-6"><a href="javascript:;" class="btn btn-save pull-left uploadbtn" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery')">Refresh</a></div>
                        <div class="col-md-6"><a href="javascript:void(0)" onclick="return openInNewTab('https://www.facebook.com/ads/manager/media/library/');" target="_blank" class="btn btn-save pull-right uploadbtn">Upload Image</a></div>
                </div>        
                        <div class="col-md-12">
                          <div class="gallery-box" id="gallery">
                                
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row upld-video">
                      <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-video">
                        <label class="col-md-12 control-label ">Choose From Library</label>
                        <div id="vidlib" class="feedloader" style="display: none;">
                    <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
                </div>
                <div class="row">
                <div class="col-md-6"><a href="javascript:;" class="btn btn-save pull-left uploadbtn" onclick="fngetallvideos('<?php echo $userId; ?>', 'gallery-video');">Refresh</a></div>
                        <div class="col-md-6"><a href="#" target="_blank" class="btn btn-save pull-right uploadbtn" id="uplaodpagevid">Upload Video</a></div>
                        </div>
                        <div class="col-md-12">
                          <div class="gallery-box" id="gallery-video">
                              
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="imgType" id="imgType" value="<?php echo !empty($Cmpdraft->imgType) ? $Cmpdraft->imgType : ''; ?>">
                <div class="inputs-holder" style="padding-top: 0px;">
                  <!--Product 1-->  
                  <div class="images-option-holder uploadimg-ads ads-boximg displaycontent-ads PAGE_LIKES_OBJ_Head1 displayNone" style="border-width: 0px;padding: 0px;">
                    <strong id="titp1" class="title with-bg">Product 1:</strong>
                    <div id="uploadedImages-product">
                        <?php 
                          if (!empty($Cmpdraft->product1image)) { 
                            $adimagehashArr = explode('_', $Cmpdraft->adimagehash1);
                            if($adimagehashArr[1] == 'video'){
                              ?>
                                <input class="<?= $adimagehashArr[0] ?> " type="hidden" id="viewUrl1" name="product1_img_url" value="<?= $Cmpdraft->product1image ?>">
                                <input class="<?= $adimagehashArr[0] ?>" id="fbimghash1" type='hidden' name='fbimghash1' value='<?= $adimagehashArr[0] ?>' />
                              <?php
                            }
                            else{
                            $imgApp1 = explode('.', $Cmpdraft->product1image);
                          ?>
                            <input class="<?= $imgApp1[0].'-gallery-p1' ?> p1" type="hidden" id="viewUrl1" name="product1_img_url" value="https://fbcdn-creative-a.akamaihd.net/hads-ak-xtf1/t45.1600-4/<?= $Cmpdraft->product1image ?>">
                            <input class="<?= $imgApp1[0].'-gallery-p1' ?> p1" id="fbimghash1" type='hidden' name='fbimghash1' value='<?= $Cmpdraft->adimagehash1 ?>' />
                        <?php } }?>
                        <?php 
                          if (!empty($Cmpdraft->product2image)) { 
                            $adimagehashArr2 = explode('_', $Cmpdraft->adimagehash2);
                            if($adimagehashArr2[1] == 'video'){
                              ?>
                                <input class="<?= $adimagehashArr2[0] ?> " type="hidden" id="viewUrl2" name="product2_img_url" value="<?= $Cmpdraft->product2image ?>">
                                <input class="<?= $adimagehashArr2[0] ?>" id="fbimghash2" type='hidden' name='fbimghash2' value='<?= $adimagehashArr2[0] ?>' />
                              <?php
                            }
                            else{
                          $imgApp2 = explode('.', $Cmpdraft->product2image);?>
                            <input class="<?= $imgApp2[0].'-gallery-p2' ?> p2" type="hidden" id="viewUrl2" name="product2_img_url" value="https://fbcdn-creative-a.akamaihd.net/hads-ak-xtf1/t45.1600-4/<?= $Cmpdraft->product2image ?>">
                            <input class="<?= $imgApp2[0].'-gallery-p2' ?> p2" id="fbimghash2" type='hidden' name='fbimghash2' value='<?= $Cmpdraft->adimagehash2 ?>' />
                        <?php } }?>
                        <?php 
                          if (!empty($Cmpdraft->product3image)) { 
                            $adimagehashArr3 = explode('_', $Cmpdraft->adimagehash3);
                            if($adimagehashArr3[1] == 'video'){
                              ?>
                                <input class="<?= $adimagehashArr3[0] ?> " type="hidden" id="viewUrl3" name="product3_img_url" value="<?= $Cmpdraft->product3image ?>">
                                <input class="<?= $adimagehashArr3[0] ?>" id="fbimghash3" type='hidden' name='fbimghash3' value='<?= $adimagehashArr3[0] ?>' />
                              <?php
                            }
                            else{
                            $imgApp3 = explode('.', $Cmpdraft->product3image);?>
                            <input class="<?= $imgApp3[0].'-gallery-p3' ?> p3" type="hidden" id="viewUrl3" name="product3_img_url" value="https://fbcdn-creative-a.akamaihd.net/hads-ak-xtf1/t45.1600-4/<?= $Cmpdraft->product3image ?>">
                            <input class="<?= $imgApp3[0].'-gallery-p3' ?> p3" id="fbimghash3" type='hidden' name='fbimghash3' value='<?= $Cmpdraft->adimagehash3 ?>' />
                        <?php }} ?>
                    </div>

                    <div class="input-wrap" id="headline_fileds">
                      <div class="form-group validation headline_del_div">
                        <input type="text" class="form-control" placeholder="Product 1 Headline" class="form-control addfield_textbox" name="headlines_pro1" id="headlines_pro1" value="<?= $Cmpdraft->product1headline ?>">
                        <span class="validate-info">*</span>
                      </div>
                    </div>
                    
                    <div class="input-wrap">
                      <div class="form-group validation url_del_div">
                        <input type="text" class="form-control addfield_textbox" name="url_pro1" id='url_pro1' placeholder="Product 1 URL" value="<?= $Cmpdraft->product1url ?>">
                      </div>
                    </div>

                    <div class="input-wrap" id="description_fileds">
                      <div class="form-group validation description_del_div">
                        <textarea type="text" class="form-control addfield_textbox" name="desc_pro1" id="desc_pro1" placeholder="Product 1 Description"><?= $Cmpdraft->product1description ?></textarea>
                        
                        <span class="validate-info">*</span>
                      </div>
                    </div>

                    <div class="wrap">
                    <div class="row">
                      <div class="col-xs-6">
                      <a href="javascript:;" style="text-decoration:none;" class="upld-lib-btn-p1" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p1')">
                        <div class="image-item image-library">
                          <i class="icon-image"></i>
                          <span class="text">IMAGE LIBRARY</span>
                        </div>
                      </a>  
                      </div>
                      <div class="col-xs-6">
                      
                      </div>
                    </div>
                  </div>
                  <!--
                    <div class="wrap">
                      <div class="row">
                        <div class="col-xs-4">
                          <a href="javascript:;" class="upld-lib-btn-p1" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p1')">
                            <div class="image-item">
                              <span>
                                <i class="icon-library"></i>
                                <span class="text">Images</span>
                              </span>
                            </div>
                          </a>
                        </div>
                        <div class="col-xs-4" style="display:none;">
                          <a href="javascript:;" class="upld-img-btn upld-img-btn-p1">
                            <div class="image-item">
                              <span>
                                <i class="icon-image"></i>
                                <span class="text">Image</span>
                              </span>
                            </div>
                          </a>
                        </div>
                        <div class="col-xs-4" style="display:none;">
                          <a href="javascript:;" class="upld-video-btn-p1" onclick="fngetallvideos('<?php echo $userId; ?>', 'gallery-video-p1')">
                            <div class="image-item">
                              <span>
                                <i class="icon-library"></i>
                                <span class="text">Videos</span>
                              </span>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div> -->
                    <div class="wrap-new">
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-img-p1" style="display: none">
                            <label class="col-md-12 control-label">Upload Image</label>
                            <div class="col-md-12">
                                <div action="<?php echo $this->config->item('assets'); ?>global/plugins/dropzone/upload.php?p=1&userId=<?php echo $userId; ?>" class="dropzone" id="myDropzone1">
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-lib-p1" style="display: none">
                        <label class="col-md-12 control-label ">Choose From Library</label>
                        <div id="imglibgallery-p1" class="feedloader" style="display: none;">
                    <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
                </div>
                <div class="row">
                        <div class="col-md-6"><a href="javascript:;" class="btn btn-save pull-left uploadbtn" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p1')">Refresh</a></div>
                        <div class="col-md-6"><a href="javascript:void(0)" onclick="return openInNewTab('https://www.facebook.com/ads/manager/media/library/');" target="_blank" class="btn btn-save pull-right uploadbtn">Upload Image</a></div>
                    </div>    
                        <div class="col-md-12">
                            <div class="gallery-box " id="gallery-p1">
                                
                             </div>
                        </div>
                        </div>
                      </div>
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-video-p1" style="display: none">
                        <label class="col-md-12 control-label ">Choose From Video Library</label>
                        <div class="col-md-12">
                            <div class="gallery-box " id="gallery-video-p1">
                                
                             </div>
                        </div>
                        </div>
                      </div>
                    </div>

                    <div class="select-wrap PAGE_LIKES_OBJ_Head PAGE_LIKES_OBJ_Head1">
                      <select class="form-control call-to" name="callToAction_pro1" id="callToAction_pro1">
                        <option value="SHOP_NOW"  <?= $Cmpdraft->callToAction_pro1 == 'SHOP_NOW' ? "selected" : "" ?>>Shop now</option>
                        <option value="BOOK_TRAVEL"<?= $Cmpdraft->callToAction_pro1 == 'BOOK_TRAVEL' ? "selected" : "" ?>>Book now</option>
                        <option value="LEARN_MORE"<?= $Cmpdraft->callToAction_pro1 == 'LEARN_MORE' ? "selected" : "" ?>>Learn more</option>
                        <option value="SIGN_UP"<?= $Cmpdraft->callToAction_pro1 == 'SIGN_UP' ? "selected" : "" ?>>Sign up</option>
                        <option value="DOWNLOAD"<?= $Cmpdraft->callToAction_pro1 == 'DOWNLOAD' ? "selected" : "" ?>>Download</option>
                        <option value="WATCH_MORE"<?= $Cmpdraft->callToAction_pro1 == 'WATCH_MORE' ? "selected" : "" ?>>Watch more</option>
                        <option value="WATCH_VIDEO"<?= $Cmpdraft->callToAction_pro1 == 'WATCH_VIDEO' ? "selected" : "" ?>>Watch video</option>
                        <option value="LISTEN_MUSIC"<?= $Cmpdraft->callToAction_pro1 == 'LISTEN_MUSIC' ? "selected" : "" ?>>Listen Now</option>
                        <option value="INSTALL_APP"<?= $Cmpdraft->callToAction_pro1 == 'INSTALL_APP' ? "selected" : "" ?>>Install Now</option>
                        <option value="USE_APP"<?= $Cmpdraft->callToAction_pro1 == 'USE_APP' ? "selected" : "" ?>>Use App</option>
                        <option value="NO_BUTTON"<?= $Cmpdraft->callToAction_pro1 == 'NO_BUTTON' ? "selected" : "" ?>>No Button</option>
                      </select>
                    </div>
                  </div>

                  <!--Product 2-->
                  <div class="images-option-holder uploadimg-ads ads-boximg displaycontent-ads PAGE_LIKES_OBJ_Head1 displayNone" style="border-width: 0px;padding: 0px;">
                    <strong id="titp1" class="title with-bg">Product 2:</strong>
                    <div class="input-wrap" id="headline_fileds">
                      <div class="form-group validation headline_del_div">
                        <input type="text" class="form-control" placeholder="Product 2 Headline" class="form-control addfield_textbox" name="headlines_pro2" id="headlines_pro2" value="<?= $Cmpdraft->product2headline ?>">
                        <span class="validate-info">*</span>
                      </div>
                    </div>
                    
                    <div class="input-wrap">
                      <div class="form-group validation url_del_div">
                        <input type="text" class="form-control addfield_textbox" name="url_pro2" id='url_pro2' placeholder="Product 2 URL" value="<?= $Cmpdraft->product2url ?>">
                      </div>
                    </div>

                    <div class="input-wrap" id="description_fileds">
                      <div class="form-group validation description_del_div">
                        <textarea type="text" class="form-control addfield_textbox" name="desc_pro2" id="desc_pro2" placeholder="Product 2 Description"><?= $Cmpdraft->product2description ?></textarea>
                        
                        <span class="validate-info">*</span>
                      </div>
                    </div>
                    <div class="wrap">
                    <div class="row">
                      <div class="col-xs-6">
                      <a href="javascript:;" style="text-decoration:none;" class="upld-lib-btn-p2" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p2')">
                        <div class="image-item image-library">
                          <i class="icon-image"></i>
                          <span class="text">IMAGE LIBRARY</span>
                        </div>
                      </a>  
                      </div>
                      <div class="col-xs-6">
                      
                      </div>
                    </div>
                  </div>
                   <!-- <div class="wrap">
                      <div class="row">
                        <div class="col-xs-4">
                          <a href="javascript:;" class="upld-lib-btn-p2" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p2')">
                          <div class="image-item">
                            <span>
                              <i class="icon-library"></i>
                              <span class="text">Images</span>
                            </span>
                          </div>
                          </a>
                        </div>
                        <div class="col-xs-4" style="display:none;">
                          <a href="javascript:;" class="upld-img-btn upld-img-btn-p2">
                            <div class="image-item">
                              <span>
                                <i class="icon-image"></i>
                                <span class="text">Image</span>
                              </span>
                            </div>
                          </a>
                        </div>
                        <div class="col-xs-4" style="display:none;">
                          <a href="javascript:;" class="upld-video-btn-p2" onclick="fngetallvideos('<?php echo $userId; ?>', 'gallery-video-p2')">
                          <div class="image-item">
                            <span>
                              <i class="icon-library"></i>
                              <span class="text">Videos</span>
                            </span>
                          </div>
                          </a>
                        </div>
                      </div>
                    </div>-->
                    <div class="wrap-new">  
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-img-p2" style="display: none">
                            <label class="col-md-12 control-label">Upload Image</label>
                            <div class="col-md-12">
                                <div action="<?php echo $this->config->item('assets'); ?>global/plugins/dropzone/upload.php?p=2&userId=<?php echo $userId; ?>" class="dropzone" id="myDropzone1">
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-lib-p2" style="display: none">
                        <label class="col-md-12 control-label ">Choose From Library</label>
                        <div id="imglibgallery-p2" class="feedloader" style="display: none;">
                    <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
                </div>
                <div class="row">
                        <div class="col-md-6"><a href="javascript:;" class="btn btn-save pull-left uploadbtn" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p2')">Refresh</a></div>
                        <div class="col-md-6"><a href="javascript:void(0)" onclick="return openInNewTab('https://www.facebook.com/ads/manager/media/library/');" target="_blank" class="btn btn-save pull-right uploadbtn">Upload Image</a></div>
                        </div>
                        <div class="col-md-12">
                            <div class="gallery-box " id="gallery-p2">
                                
                             </div>
                        </div>
                        </div>
                      </div>
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-video-p2" style="display: none">
                        <label class="col-md-12 control-label ">Choose From Video Library</label>
                        <div class="col-md-12">
                            <div class="gallery-box " id="gallery-video-p2">
                                
                             </div>
                        </div>
                        </div>
                      </div>
                    </div>

                    <div class="select-wrap PAGE_LIKES_OBJ_Head PAGE_LIKES_OBJ_Head1">
                      <select class="form-control call-to" name="callToAction_pro2" id="callToAction_pro2">
                        <option value="NO_BUTTON"<?= $Cmpdraft->callToAction_pro1 == 'NO_BUTTON' ? "selected" : "" ?>>No Button</option>
                        <option value="SHOP_NOW"  <?= $Cmpdraft->callToAction_pro1 == 'SHOP_NOW' ? "selected" : "" ?>>Shop now</option>
                        <option value="BOOK_TRAVEL"<?= $Cmpdraft->callToAction_pro1 == 'BOOK_TRAVEL' ? "selected" : "" ?>>Book now</option>
                        <option value="LEARN_MORE"<?= $Cmpdraft->callToAction_pro1 == 'LEARN_MORE' ? "selected" : "" ?>>Learn more</option>
                        <option value="SIGN_UP"<?= $Cmpdraft->callToAction_pro1 == 'SIGN_UP' ? "selected" : "" ?>>Sign up</option>
                        <option value="DOWNLOAD"<?= $Cmpdraft->callToAction_pro1 == 'DOWNLOAD' ? "selected" : "" ?>>Download</option>
                        <option value="WATCH_MORE"<?= $Cmpdraft->callToAction_pro1 == 'WATCH_MORE' ? "selected" : "" ?>>Watch more</option>
                        <option value="WATCH_VIDEO"<?= $Cmpdraft->callToAction_pro1 == 'WATCH_VIDEO' ? "selected" : "" ?>>Watch video</option>
                        <option value="LISTEN_MUSIC"<?= $Cmpdraft->callToAction_pro1 == 'LISTEN_MUSIC' ? "selected" : "" ?>>Listen Now</option>
                        <option value="INSTALL_APP"<?= $Cmpdraft->callToAction_pro1 == 'INSTALL_APP' ? "selected" : "" ?>>Install Now</option>
                        <option value="USE_APP"<?= $Cmpdraft->callToAction_pro1 == 'USE_APP' ? "selected" : "" ?>>Use App</option>
                      </select>
                    </div>
                  </div>

                  <!--Product 3-->
                  <div class="images-option-holder uploadimg-ads ads-boximg displaycontent-ads PAGE_LIKES_OBJ_Head1 displayNone" style="border-width: 0px;padding: 0px;">
                    <strong id="titp1" class="title with-bg">Product 3:</strong>
                    <div class="input-wrap" id="headline_fileds">
                      <div class="form-group validation headline_del_div">
                        <input type="text" class="form-control" placeholder="Product 3 Headline" class="form-control addfield_textbox" name="headlines_pro3" id="headlines_pro3" value="<?= $Cmpdraft->product3headline ?>">
                        <span class="validate-info">*</span>
                      </div>
                    </div>
                    
                    <div class="input-wrap">
                      <div class="form-group validation url_del_div">
                        <input type="text" class="form-control addfield_textbox" name="url_pro3" id='url_pro3' placeholder="Product 3 URL" value="<?= $Cmpdraft->product3url ?>">
                      </div>
                    </div>

                    <div class="input-wrap" id="description_fileds">
                      <div class="form-group validation description_del_div">
                        <textarea type="text" class="form-control addfield_textbox" name="desc_pro3" id="desc_pro3" placeholder="Product 3 Description"><?= $Cmpdraft->product3description ?></textarea>
                        
                        <span class="validate-info">*</span>
                      </div>
                    </div>
                    <div class="wrap">
                    <div class="row">
                      <div class="col-xs-6">
                      <a href="javascript:;" style="text-decoration:none;" class="upld-lib-btn-p3" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p3')">
                        <div class="image-item image-library">
                          <i class="icon-image"></i>
                          <span class="text">IMAGE LIBRARY</span>
                        </div>
                      </a>  
                      </div>
                      <div class="col-xs-6">
                      
                      </div>
                    </div>
                  </div>
                    <!-- <div class="wrap">
                      <div class="row">
                        <div class="col-xs-4">
                          <a href="javascript:;" class="upld-lib-btn-p3" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p3')">
                          <div class="image-item">
                            <span>
                              <i class="icon-library"></i>
                              <span class="text">Images</span>
                            </span>
                          </div>
                          </a>
                        </div>
                        <div class="col-xs-4" style="display:none;">
                          <a href="javascript:;" class="upld-img-btn upld-img-btn-p3">
                            <div class="image-item">
                              <span>
                                <i class="icon-image"></i>
                                <span class="text">Image</span>
                              </span>
                            </div>
                          </a>
                        </div>
                        <div class="col-xs-4" style="display:none;">
                          <a href="javascript:;" class="upld-video-btn-p3" onclick="fngetallvideos('<?php echo $userId; ?>', 'gallery-video-p3')">
                          <div class="image-item">
                            <span>
                              <i class="icon-library"></i>
                              <span class="text">Videos</span>
                            </span>
                          </div>
                          </a>
                        </div>
                      </div>
                    </div>-->
                    <div class="wrap-new">
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-img-p3" style="display: none">
                            <label class="col-md-12 control-label">Upload Image</label>
                            <div class="col-md-12">
                                <div action="<?php echo $this->config->item('assets'); ?>global/plugins/dropzone/upload.php?p=3&userId=<?php echo $userId; ?>" class="dropzone" id="myDropzone1">
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-lib-p3" style="display: none">
                        <label class="col-md-12 control-label ">Choose From Library</label>
                        <div id="imglibgallery-p3" class="feedloader" style="display: none;">
                    <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
                </div>
                <div class="row">
                        <div class="col-md-6"><a href="javascript:;" class="btn btn-save pull-left uploadbtn" onclick="fngetallimages('<?php echo $userId; ?>', 'gallery-p3')">Refresh</a></div>
                        <div class="col-md-6"><a href="javascript:void(0)" onclick="return openInNewTab('https://www.facebook.com/ads/manager/media/library/');" target="_blank" class="btn btn-save pull-right uploadbtn">Upload Image</a></div>
                </div>        
                        <div class="col-md-12">
                            <div class="gallery-box " id="gallery-p3">
                                
                             </div>
                        </div>
                        </div>
                      </div>
                      <div class="row">
                        <div align="center" class="form-group col-md-12 col-sm-12 p-rl-5 upld-video-p3" style="display: none">
                        <label class="col-md-12 control-label ">Choose From Video Library</label>
                        <div class="col-md-12">
                            <div class="gallery-box " id="gallery-video-p3">
                                
                             </div>
                        </div>
                        </div>
                      </div>
                    </div>

                    <div class="select-wrap PAGE_LIKES_OBJ_Head PAGE_LIKES_OBJ_Head1">
                      <select class="form-control call-to" name="callToAction_pro3" id="callToAction_pro3">
                        <option value="NO_BUTTON"<?= $Cmpdraft->callToAction_pro1 == 'NO_BUTTON' ? "selected" : "" ?>>No Button</option>
                        <option value="SHOP_NOW"  <?= $Cmpdraft->callToAction_pro1 == 'SHOP_NOW' ? "selected" : "" ?>>Shop now</option>
                        <option value="BOOK_TRAVEL"<?= $Cmpdraft->callToAction_pro1 == 'BOOK_TRAVEL' ? "selected" : "" ?>>Book now</option>
                        <option value="LEARN_MORE"<?= $Cmpdraft->callToAction_pro1 == 'LEARN_MORE' ? "selected" : "" ?>>Learn more</option>
                        <option value="SIGN_UP"<?= $Cmpdraft->callToAction_pro1 == 'SIGN_UP' ? "selected" : "" ?>>Sign up</option>
                        <option value="DOWNLOAD"<?= $Cmpdraft->callToAction_pro1 == 'DOWNLOAD' ? "selected" : "" ?>>Download</option>
                        <option value="WATCH_MORE"<?= $Cmpdraft->callToAction_pro1 == 'WATCH_MORE' ? "selected" : "" ?>>Watch more</option>
                        <option value="WATCH_VIDEO"<?= $Cmpdraft->callToAction_pro1 == 'WATCH_VIDEO' ? "selected" : "" ?>>Watch video</option>
                        <option value="LISTEN_MUSIC"<?= $Cmpdraft->callToAction_pro1 == 'LISTEN_MUSIC' ? "selected" : "" ?>>Listen Now</option>
                        <option value="INSTALL_APP"<?= $Cmpdraft->callToAction_pro1 == 'INSTALL_APP' ? "selected" : "" ?>>Install Now</option>
                        <option value="USE_APP"<?= $Cmpdraft->callToAction_pro1 == 'USE_APP' ? "selected" : "" ?>>Use App</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="tracking-ads-box">
                  <strong class="title with-bg" id="titstep5" style="margin-left:-15px;"><?php if("PAGE_LIKES" == $Cmpdraft->adobjective) { echo "Step 4"; } else if("POST_ENGAGEMENT" == $Cmpdraft->adobjective || "PRODUCT_CATALOG_SALES" == $Cmpdraft->adobjective){ echo "Step 3"; } else { echo "Step 5"; }  ?> - Tracking Your Ads</strong>
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" name="track_fb_pixel" id="checkbox3" value="Yes" <?php if($Cmpdraft->track_fb_pixel == 'Yes'){echo "checked";} ?>>
                    <label for="checkbox3">
                      Track using Facebook pixel
                    </label>
                  </div>
                  <div class="checkbox checkbox-success">
                    <input type="checkbox" name="track_google" id="checkbox4" value="Yes" <?php if($Cmpdraft->track_google == 'Yes'){echo "checked";} ?>>
                    <label for="checkbox4">
                      Track using Google analytics
                    </label>
                  </div>
                  <strong class="title with-bg PAGE_LIKES_OBJ_Head2 CONVERSIONS_OBJ_DUM" style="display: none; margin-left:-15px;">Step 6 - Choose Your Conversion Pixel</strong>
                  <div class="select-wrap CONVERSIONS_OBJ_DUM PAGE_LIKES_OBJ_Head2"  style="display: none">
                  	
                    <select id="pixel_id1" class="page1 form-control" name="pixel_id1" >
                     <!--  <option selected="">Conversions Tracking Pixel</option> -->
                      <option value="CONTENT_VIEW" <?= $Cmpdraft->conv_pixel == 'CONTENT_VIEW' ? "selected" : "" ?>>ViewContent</option>
                      <option value="SEARCH" <?= $Cmpdraft->conv_pixel == 'SEARCH' ? "selected" : "" ?>>Search</option>
                      <option value="ADD_TO_CART" <?= $Cmpdraft->conv_pixel == 'ADD_TO_CART' ? "selected" : "" ?>>AddToCart</option>
                      <option value="ADD_TO_WISHLIST" <?= $Cmpdraft->conv_pixel == 'ADD_TO_WISHLIST' ? "selected" : "" ?>>AddToWishlist</option>
                      <option value="INITIATED_CHECKOUT" <?= $Cmpdraft->conv_pixel == 'INITIATED_CHECKOUT' ? "selected" : "" ?>>InitiateCheckout</option>
                      <option value="ADD_PAYMENT_INFO" <?= $Cmpdraft->conv_pixel == 'ADD_PAYMENT_INFO' ? "selected" : "" ?>>AddPaymentInfo</option>
                      <option value="PURCHASE" <?= $Cmpdraft->conv_pixel == 'PURCHASE' ? "selected" : "" ?>>Purchase</option>
                      <option value="LEAD" <?= $Cmpdraft->conv_pixel == 'LEAD' ? "selected" : "" ?>>Lead</option>
                      <option value="COMPLETE_REGISTRATION" <?= $Cmpdraft->conv_pixel == 'COMPLETE_REGISTRATION' ? "selected" : "" ?>>CompleteRegistration</option>
                    </select>
                    
                    
                  </div>
                </div>
            </div>
            <div class="button-main">
              <?php 
                  if(!empty($Cmpdraft->product1image)){
                      $imgApp1 = explode('.', $Cmpdraft->product1image);
                      $tempName1 = $imgApp1[0].'-gallery-p1';
                  }
                  if(!empty($Cmpdraft->product2image)){
                      $imgApp2 = explode('.', $Cmpdraft->product2image);
                      $tempName2 = $imgApp1[0].'-gallery-p2';
                  }
                  if(!empty($Cmpdraft->product3image)){
                      $imgApp3 = explode('.', $Cmpdraft->product3image);
                      $tempName3 = $imgApp1[0].'-gallery-p3';
                  }
              ?>
              <input type="hidden" id="Product1ImgSet" name="Product1ImgSet" value="<?php echo !empty($tempName1) ? $tempName1 : ''?>">
              <input type="hidden" id="Product2ImgSet" name="Product2ImgSet" value="<?php echo !empty($tempName2) ? $tempName2 : ''?>">
              <input type="hidden" id="Product3ImgSet" name="Product3ImgSet" value="<?php echo !empty($tempName3) ? $tempName3 : ''?>">
              <input type="hidden" id="loadCampaing" name="loadCampaing" value="<?php echo !empty($Cmpdraft->ID) ? $Cmpdraft->ID : ''?>">
              <button class="btn btn-success pull-right" type="button" onclick="ad_design_darafts('loader_drft2', 'direct');CreateCreativeAd();">Go to targeting</button>
            </div>
          </div>
          <div class="col-md-6">
            <div class="ads-box">
              <input type="hidden" name="adsCounter" value="0" id="adsCounter"/>
              <input type="hidden" name="totalAdsCount" value="0" id="totalAdsCount"/>
              <?php if($Cmpdraft->adobjective != "PRODUCT_CATALOG_SALES") { ?>
              	<div class="ads-header displayNone showAdTextMess" >
                <div class="ads-count POST_ENGAGEMENT_OBJ_Hide " id="showAdTextMess"><span>Showing ad #1 of 20 ads</span></div>
                <div class="ads-control">
                  <a href="javascript:void(0)" id="PreviousAdLink" class="btn-prev" onclick="fnLoadPreviousAd()">Prev</a>
                  <a href="javascript:void(0)" id="NextAdLink" class="btn-next" onclick="fnLoadNextAd()"><span class="text">Next Ad</span></a>
                </div>
              </div>
              <?php } ?>
              <div class="preview-btn">
                <a href="javascript:void(0)" onclick="jQuery('#ads_preview_mobilefeed').hide();jQuery('#ads_preview_desktopfeed').show();jQuery('#ads_preview_mobilefeed').css('opacity', '0');" class="btn desktop-preview"><i class="icon-calendar"></i><span>Desktop Preview</span></a>
                <a href="javascript:void(0)" onclick="jQuery('#ads_preview_desktopfeed').hide();jQuery('#ads_preview_mobilefeed').show();jQuery('#ads_preview_mobilefeed').css('opacity', '1');" class="btn mobile-preview pull-right"><i class="icon-calendar"></i><span>Mobile Preview</span></a>
              </div>
              <div class="admock">
              	<div id="feedloader_1" class="feedloader displayNone">
                    <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
                </div>
              	<div id="ads_preview_desktopfeed" class="ads-preview-panel previews-desktopfeed" align="center">
					       <?php if (!empty($Cmpdraft->desktoppreview)) { ?>
                        <?php echo html_entity_decode(str_replace("and", "&", $Cmpdraft->desktoppreview)); ?>
                    <?php } else { ?>
                        <div class="preview-info-alert jsNoPreviewText">
                            <p class="tit">Preview not available</p>
                            <p>You must fill all required fields to see a preview of your ads.</p>
                        </div><!-- /.preview-info-alert -->
                        <div class="preview-info-alert preview-loading jsLoadingText hide">
                            <p class="tit">Loading preview...</p>
                        </div><!-- /.preview-info -->

                        <div id="js_preview_carousel_desktopfeed" class="carousel slide" data-interval="false">
                            <ul class="fb-ads-list fb-list-mobile fb-list-desktopfeed carousel-inner"></ul>
                        </div><!--/.carousel-->
                    <?php } ?>
                </div><!-- /.ads-preview-panel -->
                
                <div id="feedloader_2" class="feedloader displayNone">
                    <div class="feedloader_load"><i class="fa fa-spinner fa-spin"></i></div>
                </div>
                <div id="ads_preview_mobilefeed" class="ads-preview-panel previews-desktopfeed displayNone" align="center">
                    <?php if (!empty($Cmpdraft->mobilepreview)) { ?>
                        <?php echo html_entity_decode(str_replace("and", "&", $Cmpdraft->mobilepreview)); ?>
                    <?php } else { ?>
                        <div class="preview-info-alert jsNoPreviewText">
                            <p class="tit">Preview not available</p>
                            <p>You must fill all required fields to see a preview of your ads.</p>
                        </div><!-- /.preview-info-alert -->
                        <div class="preview-info-alert preview-loading jsLoadingText hide">
                            <p class="tit">Loading preview...</p>
                        </div><!-- /.preview-info -->

                        <div id="js_preview_carousel_mobilefeed" class="carousel slide" data-interval="false">
                            <ul class="fb-ads-list fb-list-mobile fb-list-desktopfeed carousel-inner"></ul>
                        </div><!--/.carousel-->
                    <?php } ?>
                </div><!-- /.ads-preview-panel -->
                <!--<img src="images/facebook.png" alt="image description">-->
              </div>
              <div class="ads-footer">
                <div class="drft_msg" style="display: none;">The draft has been  saved successfully.</div>
              </div>
              <div class="ads-footer">
                <!--<a href="#" class="btn btn-load"><i class="icon-pencil"></i> Load Template</a>-->
                <a class="btn btn-save pull-left" onclick="LoeadPreview();" href="javascript:;">
                <i class="fa fa-refresh"></i> Refresh 
            </a>
                <a href="#" class="btn btn-save pull-right" onclick="ad_design_darafts('loader_drft2')"><i class="icon-heart"></i> Save as template for later use</a>
              </div>
            </div>
            <!-- <div class="text-center save-btn-holder">
              <a href="#" class="btn btn-success btn-new-project">
                <i class="icon-pen"></i>
                <span>New project</span>
              </a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <script>
	(function($) {
    
    $.fn.bmdIframe = function( options ) {
        var self = this;
        var settings = $.extend({
            classBtn: '.bmd-modalButton',
            defaultW: 640,
            defaultH: 360
        }, options );
      
        $(settings.classBtn).on('click', function(e) {
          var allowFullscreen = $(this).attr('data-bmdVideoFullscreen') || false;
          
          var dataVideo = {
            'src': $(this).attr('data-bmdSrc'),
            'height': $(this).attr('data-bmdHeight') || settings.defaultH,
            'width': $(this).attr('data-bmdWidth') || settings.defaultW
          };
          
          if ( allowFullscreen ) dataVideo.allowfullscreen = "";
          
          // stampiamo i nostri dati nell'iframe
          $(self).find("iframe").attr(dataVideo);
        });
      
        // se si chiude la modale resettiamo i dati dell'iframe per impedire ad un video di continuare a riprodursi anche quando la modale è chiusa
        this.on('hidden.bs.modal', function(){
          $(this).find('iframe').html("").attr("src", "");
        });
      
        return this;
    };
  
})(jQuery);




jQuery(document).ready(function(){
  jQuery("#myModaliframe").bmdIframe();
  jQuery("#myModaliframe2").bmdIframe();
  /*
var enters = 0;
   jQuery('.addfield_textbox').keypress(function(event) {
        console.log(event.which);
        if (event.which == 13)
            enters++;
        else
            enters = 0;
        if (enters > 1) {
            //alert(jQuery(this).val());
            jQuery(this).val(jQuery(this).val()+"\n"+" \n"+" \n");
            //alert('You hit 2 new lines!');
            //alert(jQuery(this).val())
        }
    });*/
 

});
	</script>
  <style>

.bmd-modalButton {
  display: block;
 /* margin: 15px auto;
  padding: 5px 15px;*/
}

.close-button {
  overflow: hidden;
}

.bmd-modalContent {
  box-shadow: none;
  background-color: transparent;
  border: 0;
}
  
.bmd-modalContent .close {
  font-size: 30px;
  line-height: 30px;
  padding: 7px 4px 7px 13px;
  text-shadow: none;
  opacity: .7;
  color:#fff;
}

.bmd-modalContent .close span {
  display: block;
}

.bmd-modalContent .close:hover,
.bmd-modalContent .close:focus {
  opacity: 1;
  outline: none;
}

.bmd-modalContent iframe {
  display: block;
  margin: 0 auto;
}
  </style>