	<main id="main_1" class="bg-white">
				<div class="main-content sales-page">
					<div class="container">
						<div class="experienc-block">
							<div class="text-holder text-center">
								<h2><span class="highlight-text">“DONE FOR YOU”</span> FACEBOOK CAMPAIGNS</h2>
								<p>Want help with your Facebook ads... <br>
								But don’t want to pay an arm and a leg for good service?</p>
								<p>I understand that many marketers don't want full management, but could use a little assistance to get better results from their Facebook ad campaigns.</p>
							</div>
							<div class="img-holder text-center">
								<div class="img-wrap">
									<span class="water-mark">EXPERIENCED</span>
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/img-trophy.svg" alt="trophy">
								</div>
							</div>
						</div>
						<div class="about-section d-md-flex align-items-center">
							<div class="text-holder d-flex flex-wrap justify-content-end order-2">
								<div class="text-wrap">
									<h2 class="decorative-heding">WHO WE ARE</h2>
									<p>My name is Samy Zabarah, I’m the creator of The Campaign Maker.  I started using Facebook ads in 2011, and since then I have been using facebook ads as my sole source of marketing.</p>
									<p>In 2014, I founded FUBSZ LLC, a Facebook ad management service based in Florida, USA.   Since then I have managed over 350 accounts worldwide, which adds up to over 2,000 campaigns and over 20,000 ads!</p>
									<p>My team and I know every nook and cranny of FB ads inside and out. And to be perfectly blunt, we are really, REALLY good at FB advertising.</p>
									<p>That’s why I decided to create a NEW service!</p>
								</div>

							</div>
							<div class="img-holder text-center order-1">
								<div class="img-wrap">
									<span class="water-mark">ABOUT</span>
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/img-man.svg" alt="man">
								</div>
							</div>
						</div>
						<div class="offer-section">
							<div class="text-holder text-center">
								<h2>SO, WHAT DOES THIS SERVICE OFFER?</h2>
								<p>By combining the simplicity of optimizing custom campaigns through The Campaign Maker, and the expertise of our veteran Facebook marketing team, we could create a completely “done for you” Facebook advertising service... And we could do it at a fraction of the cost of what other management companies charge.</p>
							</div>
							<div class="img-holder text-center">
								<div class="img-wrap">
									<span class="water-mark">RESULTS BASED</span>
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/img-browser.svg" alt="browser">
								</div>
							</div>
						</div>
						<div class="sales-section d-md-flex align-items-center">
							<div class="text-holder">
								<div class="text-wrap">
									<h2 class="decorative-heding">HOW DOES THIS WORK EXACTLY?</h2>
									<p>You fill out a simple 5-question form and tell us which campaign  you want us to outperform.</p>
									<p>Our expert advertisers will then take care of researching your offer and learn about what your competition is doing.</p>
									<p>We will then send you a competitive quote guaranteeing an ad within our campaign will get a lower CPC within a specific time.</p>
									<p><strong class="highlight-text">You can then approve or reject the quote.</strong> <br>
									Once you approve the quote, our experts will within 3 business days setup your campaign in the most optimal way possible.</p>
									<p>Best of all, the new improved campaign will be saved right inside your TCM account so you can simply load the campaign, review it, edit it and launch it whenever you want!</p>
								</div>
							</div>
							<div class="img-holder text-center">
								<div class="img-wrap">
									<span class="water-mark">SIMPLE</span>
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/img-white-board.svg" alt="white-board">
								</div>
							</div>
						</div>
						<div class="guarantee-block">
							<div class="text-holder text-center">
								<h2>AND TO MAKE IT A “NO-BRAINER,” WE GIVE YOU <span class="highlight-text">A FULL MONEY BACK GUARANTEE!</span></h2>
								<p>If the campaign we created for you does not get a lower cost per click than the quote provided, we will give you a FULL Refund! <br> We won’t start working with you unless we are confident in getting better results.</p>
							</div>
							<div class="img-holder text-center">
								<div class="img-wrap">
									<span class="water-mark">GUARANTEE</span>
									<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/img-earth.svg" alt="earth">
								</div>
							</div>
						</div>
						<div class="feature-section">
							<h2 class="text-center">EXPERIENCE THE POWER OF TRULY RELIABLE <br><span class="highlight-text">EXPERT FACEBOOK ADVERTISERS</span></h2>
							<div class="content-holder d-md-flex align-items-center">
								<div class="text-holder left-text-holder">
									<div class="feature-item">
										<h3>ON TIME DELIVERY</h3>
										<p>All custom campaigns are guaranteed to be delivered within 3 business days (M-F) to your TCM account. We also offer rush deliviries for urgent campaigns.</p>
									</div>
									<div class="feature-item">
										<h3>NATIVE WRITERS</h3>
										<p>All our ads are written by native english writers who have at least 5+ years experience with Facebook ads and what harnesses the most engagement.</p>
									</div>
								</div>
								<div class="img-holder text-center">
									<div class="img-wrap">
										<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/img-target.svg" alt="target">
									</div>
								</div>
								<div class="text-holder right-text-holder">
									<div class="feature-item">
										<h3>WELL RESEARCHED</h3>
										<p>Your offer and direct competition will be researched in detail in order to provide the highest engagement and results for your campaign. </p>
									</div>
									<div class="feature-item">
										<h3>POLICY SAFE</h3>
										<p>ABefore final delivery your campaign will be checked in detail to make sure that your campaign is 100% on par with Facebook’s advertising policy and guidelines.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="plan-section">
							<div class="holder d-lg-flex flex-wrap align-items-center">
								<div class="text-holder">
									<div class="text-wrap">
										<h3>CHOOSE THE BEST PLAN FOR YOU</h3>
										<p>All plans include the full money back guarantee when applicable and include delivery within 3 business days.</p>
									</div>
								</div>
								<div class="plan-holder">
									<div class="two-columns d-sm-flex flex-wrap">
										<div class="plan-item d-sm-flex flex-wrap">
											<div class="plan-wrap text-center">
												<div class="plan-header">
													<div class="icon-holder">
														<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-megaphone-big.svg" alt="megaphone">
													</div>
													<strong class="title">Campaign Setup</strong>
													<span class="sub-title">Excludes retargeting</span>
												</div>
												<ul class="feature-list">
													<li>1 Complete Ad</li>
													<li>5 Images</li>
													<li>10 Audiences</li>
												</ul>
												<div class="price-holder">
													<div class="price-wrap">
														<strong class="price"><span class="currency">$</span> 200</strong>
														<span class="unit">/Campaign</span>
													</div>
													<a href="<?php echo base_url(); ?>service/contact" class="btn btn-get-started">GET YOUR FREE CPC QUOTE</a>
												</div>
											</div>
										</div>
										<div class="plan-item d-sm-flex flex-wrap">
											<div class="plan-wrap text-center">
												<div class="plan-header">
													<div class="icon-holder">
														<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-magnet-big.svg" alt="magnet">
													</div>
													<strong class="title">Campaign + Retarget</strong>
													<span class="sub-title">Get better results with retargeting</span>
												</div>
												<ul class="feature-list">
													<li>2 Complete Ads</li>
													<li>8 Images</li>
													<li>10 Audiences + Retarget</li>
												</ul>
												<div class="price-holder">
													<div class="price-wrap">
														<strong class="price"><span class="currency">$</span> 300</strong>
														<span class="unit">/Campaign</span>
													</div>
													<a href="<?php echo base_url(); ?>service/contact" class="btn btn-get-started">GET YOUR FREE CPC QUOTE</a>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="feedback-section">
							<header class="text-center">
								<span class="water-mark">Feedback</span>
								<h2>WHAT OTHERS SAY</h2>
							</header>
							<div class="testimonials">
								<div class="testimonial-holder d-md-flex flex-wrap">
									<div class="testimonial-item d-flex">
										<blockquote class="d-flex flex-wrap">
											<div class="quote-holder">
												<cite class="d-flex align-items-center">
													<span class="avatar">
														<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-boy-rounded.svg" alt="boy">
													</span>
													<span class="text">
														<span class="name">Brad G.</span>
														<span class="designation">Owner @ VectorToons.com</span>
													</span>
												</cite>
												<q>Samy was able to get me 52 new customers with his ads. He went above and beyond to find me the right target audience. Now I just have to scale this up.</q>
											</div>
										</blockquote>
									</div>
									<div class="testimonial-item d-flex">
										<blockquote class="d-flex flex-wrap">
											<div class="quote-holder">
												<cite class="d-flex align-items-center">
													<span class="avatar">
														<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-boy-rounded.svg" alt="boy">
													</span>
													<span class="text">
														<span class="name">Mark V.</span>
														<span class="designation">CMO @ FullyBookedFormula.com</span>
													</span>
												</cite>
												<q>After taking up this service we have reduced our lead cost by 60% and our funnel is converting 7% higher. Samy does wonders for your Facebook campaigns.  If in doubt try it, you will not be disappointed.</q>
											</div>
										</blockquote>
									</div>
									<div class="testimonial-item d-flex">
										<blockquote class="d-flex flex-wrap">
											<div class="quote-holder">
												<cite class="d-flex align-items-center">
													<span class="avatar">
														<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-boy-rounded.svg" alt="boy">
													</span>
													<span class="text">
														<span class="name">Eric I.</span>
														<span class="designation">Owner @ EICconsulting</span>
													</span>
												</cite>
												<q>I would not hesitate to use Samy. He really gets after it quick and has point blank made me money! So, if you don't have time to do your own fb ads or you don't know how to do them, just pull the trigger and let him do it. Good stuff!</q>
											</div>
										</blockquote>
									</div>
									<div class="testimonial-item d-flex">
										<blockquote class="d-flex flex-wrap">
											<div class="quote-holder">
												<cite class="d-flex align-items-center">
													<span class="avatar">
														<img src="<?php echo $this->config->item('assets'); ?>newdesign/images/icon-girl-rounded.svg" alt="girl">
													</span>
													<span class="text">
														<span class="name">Rene J.</span>
														<span class="designation">Owner @ GoZen.com</span>
													</span>
												</cite>
												<q>Samy is THE BEST. I've used several people to help with my Facebook ad campaigns and Samy is far and away the most professional and most competent person I've worked with. I absolutely LOVED my experience with him and would recommend him to any business owner!</q>
											</div>
										</blockquote>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>