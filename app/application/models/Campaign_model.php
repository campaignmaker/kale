<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Campaign_model
 *
 * @author Asif
 */
class Campaign_model extends CI_Model {

    public $adaccountid;

    public function __construct() {
        parent::__construct();
    }

    public function getAdaccountid() {
        return $this->adaccountid;
    }

    public function setAdaccountid($adaccountid) {
        $this->adaccountid = $adaccountid;
    }

    public function SaveCampaign($campaign) {
        $this->db->select('id, adaccountid');
        $this->db->from('campaigns');
        $this->db->where('adaccountid', $campaign['adaccountid']);
        $this->db->where('compaingname', $campaign['compaingname']);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return false;
        } else {
            $this->db->insert('campaigns', $campaign);
            return $this->db->insert_id();
        }
    }

    public function GetAllCampaignByAdAccount() {
        $this->db->select('id,adaccountid,compaingname,fbcampaignid');
        $this->db->from('campaigns');
        $this->db->where('adaccountid', $this->getAdaccountid());
        return $query = $this->db->get();
        //return $row    =   $query->result();

    }
	public function Saveautooptimization($autoopti) {
        $this->db->insert('automaticoptimization_new', $autoopti);
        redirect('automaticoptimization/'.$autoopti['addAccountId']);
	}
	
	public function Updateautooptimization($autoopti,$autoopti2) {
        
		$data = array('campaignsid' => $autoopti['campaignsid'],
					  'param1' => $autoopti['param1'],
					  'param2' => $autoopti['param2'],
                      'param3' => $autoopti['param3'],
                      'param4' => $autoopti['param4'],
                      'param5' => $autoopti['param5'],
                      'param6' => $autoopti['param6'],
					  'rulename' => $autoopti['rulename'],
					  'timeframe' => $autoopti['timeframe'],
					  'isactive' => $autoopti['isactive']
                      );
		$this->db->where('id', $autoopti2);
		$this->db->update('automaticoptimization_new', $data);
		$this->session->set_flashdata('message', 'Your data updated Successfully..');
		redirect('automaticoptimization/'.$autoopti['addAccountId']);
        //$this->db->insert('automaticoptimization', $autoopti);
        //return $this->db->insert_id();
        
    }
	
	public function Updateautooptimizationactivation($autoopti,$autoopti2) {
        
		$data = array('isactive' => $autoopti2);
		$this->db->where('id', $autoopti);
		$this->db->update('automaticoptimization_new', $data);
		$this->session->set_flashdata('message', 'Your data updated Successfully..');
		return true;
        
    }

    public function UpdateUserOptimizedData($autoopti,$autoopti2) {
        
        $data = array('optimized_adset_data' => $autoopti);
        $this->db->where('id', $autoopti2);
        $this->db->update('users', $data);
        $this->session->set_flashdata('message', 'Your data updated Successfully..');
        return true;
        
    }

    
	
	public function Getautomaticoptimization($campaign) {
        $query=$this->db->query("SELECT *
                                 FROM automaticoptimization_new 
                                 WHERE user_id = $campaign");
        return $query->result_array();
    }
    /*     $this->db->select('id,param1,param1val,param2,param2val');
        $this->db->from('automaticoptimization');
        $this->db->where('user_id', $campaign);
        return $query = $this->db->get();
        //return $row    =   $query->result();

    }*/
	
}

//end of class
