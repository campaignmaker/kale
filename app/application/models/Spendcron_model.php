<?php
Class Spendcron_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function getData($tablename, $whereArr='', $orderByArr='', $groupByArr=''){
        $this->db->select('*');
        $this->db->from($tablename);
        if(!empty($whereArr)){
            $this->db->where($whereArr);
        }
        if(!empty($groupByArr)){
            $this->db->group_by($groupByArr);
        }
        if(!empty($orderByArr)){
            $this->db->order_by($orderByArr);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
    
    function insertData($tablename, $insertArr){
        $this->db->insert($tablename, $insertArr);
        return $this->db->insert_id();
    }
    
    function getDataMaxDate($tablename, $whereArr=''){
        $this->db->select('MAX( date ) AS date');
        $this->db->from($tablename);
        if(!empty($whereArr)){
            $this->db->where($whereArr);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    public function deleteData($tablename) {
        $this->db->empty_table($tablename);
    }

    function getDemographicData($q){
        $res = $this->db->query("SELECT * FROM `demographic_data` WHERE `name` LIKE '%".$q."%'");
        return $res->result_array();
    }
}