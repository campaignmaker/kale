<?php

Class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function login($email, $password) {

        /*
		$this->db->select('id, first_name,email,picture,accesstoken,disable_user');
        $this->db->from('users');
        $this->db->where('email', $email);
        if ($password !== 'DQeVR6u7Jx%R2G36R2GxDg4Q6u7Jx%R2G') {
            $this->db->where('password', MD5($password));
        }
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
		*/
		/*$this->db->select('us.id, us.first_name,us.email,us.picture,us.accesstoken,us.disable_user,usub.billing_end_date',FALSE);
        $this->db->from('users as us, user_subscription as usub',FALSE);
		$this->db->where('us.id', 'usub.user_id', FALSE);
        $this->db->where('email', $email);
        if ($password !== 'DQeVR6u7Jx%R2G36R2GxDg4Q6u7Jx%R2G') {
            $this->db->where('password', MD5($password));
        }
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }*/
		
		$this->db->select('id, first_name,email,picture,accesstoken,disable_user');
        $this->db->from('users');
        $this->db->where('email', $email);
        if ($password !== 'DQeVR6u7Jx%R2G36R2GxDg4Q6u7Jx%R2G') {
            $this->db->where('password', MD5($password));
        }
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        
		
		
        if ($query->num_rows() == 1) {
			
			$querysubs = $this->db->query("select id,billing_end_date from user_subscription where user_id=".$query->row('id'));
			
			
			$rowsubs = $querysubs->row();

			if (isset($rowsubs) && $rowsubs->billing_end_date != '0000-00-00'){
				$todaydate = date('Y-m-d'); 
				$obj_date = date('Y-m-d', strtotime($rowsubs->billing_end_date));
				//echo $rowsubs->id .$todaydate . "sdsd" . $obj_date;
				if($todaydate >= $obj_date){
					//echo "junaid";
					$this->db->where('id',$query->row('id'));
            		$this->db->update('users', array('disable_user' => 0));
				}
			}
			
			
			
			$this->db->select('id, first_name,email,picture,accesstoken,disable_user');
			$this->db->from('users');
			$this->db->where('email', $email);
			$this->db->limit(1);
	
			$query = $this->db->get();
            return $query->row();
        } else {
            return false;
        }
		
    }
	
	
	public function checktrilaexpired($uid) {
		$this->db->select('id,first_name,email,stripe_customer_id');
        $this->db->from('users');
        $this->db->where('id', $uid);
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        
		
		
        if ($query->num_rows() == 1) {
			
			$querysubs = $this->db->query("select id,user_id,packages_id,billing_start_date, billing_end_date from user_subscription where user_id=".$query->row('id'));
			
			
			$rowsubs = $querysubs->row();
			//&& $rowsubs->packages_id == "1"
			if(isset($rowsubs) && $rowsubs->billing_end_date != '0000-00-00' && $rowsubs->billing_end_date != '0000-00-00'){
			    $rowsubs = array('exp' => "nottrial",'pkgid' => $rowsubs->packages_id,'stripestatus1' => $query->row("stripe_customer_id"),'bill_start_date' => $rowsubs->billing_start_date,'bill_end_date' => $rowsubs->billing_end_date);
			}
			elseif (isset($rowsubs) && $rowsubs->billing_end_date != '0000-00-00'){
				$todaydate = date('Y-m-d'); 
				$obj_date = date('Y-m-d', strtotime($rowsubs->billing_end_date));
				//echo $rowsubs->id .$todaydate . "sdsd" . $obj_date;
				if($todaydate >= $obj_date){
					$rowsubs = array('exp' => "expired",'pkgid' => $rowsubs->packages_id,'stripestatus1' => $query->row("stripe_customer_id"),'bill_start_date' => $rowsubs->billing_start_date,'bill_end_date' => $rowsubs->billing_end_date);
				}
				else{
					$rowsubs = array('exp' => "nottrial",'pkgid' => $rowsubs->packages_id,'stripestatus1' => $query->row("stripe_customer_id"),'bill_start_date' => $rowsubs->billing_start_date,'bill_end_date' => $rowsubs->billing_end_date);
				}
			}
			else{
				$rowsubs = array('exp' => "nottrial",'pkgid' => $rowsubs->packages_id,'stripestatus1' => $query->row("stripe_customer_id"),'bill_start_date' => $rowsubs->billing_start_date,'bill_end_date' => $rowsubs->billing_end_date);
			}

			
            return $rowsubs;
        } else {
            return false;
        }
		
    }
    
    public function getbillingstartenddate($uid) {
		$this->db->select('id,first_name,email,stripe_customer_id');
        $this->db->from('users');
        $this->db->where('id', $uid);
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        
		
		
        if ($query->num_rows() == 1) {
			
			$querysubs = $this->db->query("select id,user_id,packages_id,billing_start_date, billing_end_date from user_subscription where user_id=".$query->row('id'));
			
			
			$rowsubs = $querysubs->row();
			//&& $rowsubs->packages_id == "1"
			if(isset($rowsubs) && $rowsubs->billing_end_date == '0000-00-00' && $rowsubs->billing_end_date == '0000-00-00'){
			    $rowsubs = array('subscriptid' => $rowsubs->id);
			   
			}
			else{
				$rowsubs = "dontupdatethis";
			}

			
            return $rowsubs;
        } else {
            return "dontupdatethis";
        }
		
    }
	

    public function register($user) {
        $this->db->select('id, email');
        $this->db->from('users');
        $this->db->where('email', $user['email']);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('users', $user);
            return $this->db->insert_id();
        }
    }

    public function verifyEmailAddress($hash) {

        $this->db->select('id,email,first_name');
        $this->db->from('users');
        $this->db->where('hash', $hash);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $user = $query->row();
            $this->remove_user_hash($user->id);
            return $user;
        } else {
            return false;
        }
    }

    public function remove_user_hash($id) {
        $data = array(
            'hash' => '',
            'status' => 1
        );
        $this->db->where('id', $id);

        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function CheckEmailAddress($hash) {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('hash', $hash);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row('id');
        } else {
            return false;
        }
    }

    public function update_user_hash($id, $hash) {
        $data = array(
            'hash' => $hash
        );
        $this->db->where('id', $id);

        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }
	
	public function upgrade_downgrade_plan($id,$plan){
		if($plan == "free"){
			$data = array(
				'packages_id' => '8',
				'billing_end_date' => '0000-00-00',
				'status' => 'Active'
				
			);
			$this->db->where('user_id', $id);
	
			if ($this->db->update('user_subscription', $data)) {
				return true;
			} else {
				return false;
			}
		}
		elseif($plan == "monthly"){
			$data = array(
				'packages_id' => '4',
				'billing_end_date' => '0000-00-00',
				'status' => 'Active'
				
			);
			$this->db->where('user_id', $id);
	
			if ($this->db->update('user_subscription', $data)) {
				return true;
			} else {
				return false;
			}
		}
		else{
			$data = array(
				'packages_id' => '7',
				'billing_end_date' => '0000-00-00',
				'status' => 'Active'
				
			);
			$this->db->where('user_id', $id);
	
			if ($this->db->update('user_subscription', $data)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
    public function forgetpassword($email) {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row('id');
        } else {
            return false;
        }
    }

    public function update_password($id, $password) {
        $data = array(
            'hash' => '',
            'password' => $password,
            'status' => 1
        );
        $this->db->where('id', $id);

        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function UpdateOldPassword($id, $oldpassword, $newpassword) {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('password', $oldpassword);
        $this->db->where('status', 1);
        $this->db->where('id', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $this->db->where('id', $id);
            if ($this->db->update('users', array('password' => $newpassword))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function edit_profile($user, $uid) {

        if (isset($user['fbid']) && $user['fbid'] != NULL) {
            $this->db->select('id');
            $this->db->from('users');
            $this->db->where('fbid', $user['fbid']);
            $this->db->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return false;
            }
        }


        if ($this->db->update('users', $user, array('id' => $uid))) {
            return true;
        } else {
            return false;
        }
    }

    public function get_user_id_from_email($email) {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email);

        return $this->db->get()->row('id');
    }

    public function get_user($id) {

        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        //echo $this->db->last_query();
        return $this->db->get()->row();
    }
	
	public function get_user_subscription($id) {

        $this->db->select('*');
        $this->db->from('user_subscription');
        $this->db->where('user_id', $id);

        return $this->db->get()->row();
    }

    public function get_user_specific_detail($id, $data) {
        if (empty($data)) {
            return;
        }
        if (is_array($data)) {
            $string = rtrim(implode(',', $arr), ',');
            $this->db->select($string);
        } else {
            $this->db->select($data);
        }
        $this->db->from('users');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function getUserAddAccountId($userId) {

        $this->db->select('id,add_title,ad_account_id,status');
        $this->db->from('user_ad_account');
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return;
        }
    }

    public function getUserAddAccountName($ad_account_id) {
        $this->db->select('*');
        $this->db->from('user_ad_account');
        $this->db->where('ad_account_id', $ad_account_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return;
        }
    }

    public function addAdAccountId($adAcData) {
        
         $this->db->insert('user_ad_account', $adAcData);
        return true;
        
        /*
        $this->db->select('id');
        $this->db->from('user_ad_account');
        //$this->db->where('user_id', $adAcData['user_id']);
        $this->db->where('ad_account_id', $adAcData['ad_account_id']);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('user_ad_account', $adAcData);
            return true;
        }
        
        */
    }

    public function deleteAdAccount($uid) {

        if ($this->db->delete('user_ad_account', array('user_id' => $uid))) {
            return true;
        } else {
            return false;
        }
    }

    public function getCountryName($countryCode) {

        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('CountryCode', $countryCode);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return;
        }
    }
	public function update_stripe_customer($id, $cust_id) {
        $data = array(
            'stripe_customer_id' => $cust_id
        );
        $this->db->where('id', $id);

        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function update_billingdates($subid) {
        
		$billing_start_date = date('Y-m-d', strtotime("now"));
		$end_date = date("Y-m-d", strtotime($billing_start_date . "+4 days"));
        $data = array(
            'billing_start_date' => $billing_start_date,
            'billing_end_date' => $end_date
        );
        $this->db->where('id', $subid);

        if ($this->db->update('user_subscription', $data)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function update_user_subid($id) {
        $data = array(
            'stripe_subscription_id' => ''
        );
        $this->db->where('id', $id);

        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function insert_behaviour_tracking($id) {
        $data = array(
            'tracking_type_id'=>1,
            'user_id'=>$id,
            'tracking_date'=>date('Y-m-d H:i:s'),
            'tracking_time'=>time()
        );
        $this->db->insert('user_behaviour_tracking', $data);
    }
	public function get_all_user() {
       //if ($this->db->delete('users', array('id' => 7284))) {}
	   echo "<pre>";
	   $this->db->select('*');
       $this->db->from('user_daily_spend');
       $this->db->where('user_id','7352');
       $query = $this->db->get();
	  print_r($query->result());
	   	 
	   $this->db->select('*');
       $this->db->from('user_ad_account');
       $this->db->where('user_id','7352');
       $query = $this->db->get();
	   print_r($query->result());
        $this->db->select('*');
        $this->db->from('users');
		$this->db->where('id','7352');
       $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return;
        }
    }
	 public function get_user_pass_from_id($id) {
		
        $this->db->select('user_pass');
        $this->db->from('wp_users');
        $this->db->where('id', $id);
        return $this->db->get()->row('user_pass');
    }
	public function get_cancel_subscription($email) {
        $this->db->select('id');
        $this->db->from('wp_users');
        $this->db->where('user_email', $email);
        $id= $this->db->get()->row('id');
		$date=gmdate('Y-m-d H:i:s',time());	 
		$query=$this->db->query('SELECT expires_at as expires_at from  wp_mepr_transactions WHERE user_id = '.$id.' AND expires_at = "0000-00-00 00:00:00" and status = "complete"');
		$data1=$query->result();
		if(!empty($data1)){
			 return "Active";
		}else{
		//$querysubs=$this->db->query('SELECT sub.*,t.expires_at as expires_at from wp_mepr_subscriptions as sub Join wp_mepr_transactions as t  ON sub.id = t.subscription_id  WHERE t.user_id = '.$id.' AND t.status IN("complete","confirmed") AND sub.status <> "pending" ');
		$querysubs=$this->db->query('SELECT t.expires_at as expires_at from  wp_mepr_transactions as t WHERE t.user_id = '.$id.' AND t.status IN("complete","confirmed")');
		$data=$querysubs->result();
		//print_r($data);
		$n=sizeof($data)-1;
		if(empty($data)){
			return "No Subscribe";
		}if($data[$n]->status=="cancelled"){
			return "Cancelled";
		}elseif($data[$n]->expires_at <= $date){
			return "Expired";
		}else{
			return "Active";
		}
		}
    }
	public function get_subscription_id($email) {
        $this->db->select('id');
        $this->db->from('wp_users');
        $this->db->where('user_email', $email);
        $id= $this->db->get()->row('id');
		$date=gmdate('Y-m-d H:i:s',time());	 
		$query=$this->db->query('SELECT product_id from  wp_mepr_transactions WHERE user_id = '.$id.' AND expires_at = "0000-00-00 00:00:00" and status IN("complete","confirmed")');
		$data=$query->result();
		if(empty($data)){ return 0; }else{
		return $data[0]->product_id;  }
    }
	public function get_wp_user_id_subscription($tid) {
        $this->db->select('user_id');
        $this->db->from('wp_mepr_subscriptions');
        $this->db->where('id', $tid);
        return $this->db->get()->row('user_id');
	}
	public function get_wp_user_id_transaction($tid) {
        $this->db->select('user_id');
        $this->db->from('wp_mepr_transactions');
        $this->db->where('id', $tid);
        $id=$this->db->get()->row('user_id');
		$this->db->select('user_email');
        $this->db->from('wp_users');
        $this->db->where('id', $id);
        return $this->db->get()->row('user_email');
	}
	 public function update_user_wp_status($uid, $wp_status) {
        $data = array(
            'wp_status' => $wp_status
        );
        $this->db->where('id', $uid);
        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }
	public function get_user_email_from_username($username) {
        $this->db->select('user_email');
        $this->db->from('wp_users');
        $this->db->where('user_login', $username);
        return $this->db->get()->row('user_email');
    }
	public function reset_facebook($uid) {
		 $data = array(
            'fbid' => '',
            'accesstoken' => '',
            'fbemail' => '',
        );
		$this->db->where('id', $uid);
		if ($this->db->update('users', $data)) {
		if ($this->db->delete('user_ad_account', array('user_id' => $uid))) {
			 return true; } return true; 
        } else {
            return false;
        }
	}
}
