<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('loadView')) {

    function loadView($view, $data = null) {
        $CI = & get_instance();
        $vews[] = $CI->load->view('header');
        $vews[] = $CI->load->view('topbar');
        /*if ($CI->session->userdata('logged_in')) {
            $vews[] = $CI->load->view('menu');
        }*/
        $vews[] = $CI->load->view($view, $data);
        $vews[] = $CI->load->view('footer');
        return $vews;
    }

}

if (!function_exists('getUserAddAccountIdcount')) {

  function getUserAddAccountIdcount() {
      
        $CI = & get_instance();
        $userId = $CI->session->userdata['logged_in']['id'];
        $CI->load->model('Upgrade_model');

        $CI->db->select('id,add_title,ad_account_id,status');
        $CI->db->from('user_ad_account');
        $CI->db->where('user_id', $userId);
        $query = $CI->db->get();
        return $query->num_rows();
    }

}


if (!function_exists('SeoDetail')) {

    function SeoDetail($type) {
        $CI = & get_instance();

        if ($type == 'Title') {
            echo $CI->seo->GetValues('Title');
        }
        if ($type == 'Description') {
            echo $CI->seo->GetValues('Description');
        }
    }

}

if (!function_exists('sendEmail')) {

    function sendEmail($to, $subject, $user_data, $view = null) {

        $config = array('charset' => 'utf-8', 'wordwrap' => TRUE, 'mailtype' => 'html');

        $CI = & get_instance();
        $CI->load->library('email');
        $CI->email->initialize($config);
        $CI->email->set_newline("\r\n");
        $CI->email->from('samy@thecampaignmaker.com', 'The Campaign Maker');
        $CI->email->reply_to('samy@thecampaignmaker.com', 'Samy');
        $CI->email->to($to);
        $CI->email->subject($subject);

        if ($view && $view != NULL) {
            $message = $CI->load->view('email_templates/' . $view, $user_data, true);
        } else {
            if (is_array($user_data)) {
                $message = $user_data[0];
            } else {
                $message = $user_data;
            }
        }

        $CI->email->message($message);
        $CI->email->send();
    }

}

if (!function_exists('trialExpired')) {

    function trialExpired() {
        $CI = & get_instance();
        $CI->load->model('Upgrade_model');
        $date = $CI->Upgrade_model->checkTrialExpired($CI->session->userdata['logged_in']['id']);
        if ($date) {
            $today = time();
            $end = strtotime($date->billing_end_date);
            $day = floor(abs(strtotime(date('y-m-d')) - strtotime($date->billing_end_date)) / (60 * 60 * 24));

            if ($end > $today) {
                $days = 'Your trial will expire in '.$day." day(s)";
            } else {
                $days = 'Trial expired '.$day." day(s) ago!";
            }            
            return $days;
        } else {
            return false; // Trial completed. So Not need to show about trial expire , Now we check subcription
        }
    }

}

if (!function_exists('isTrialExpired')) {

    function isTrialExpired() {
        $CI = & get_instance();
        $CI->load->model('Upgrade_model');
        $date = $CI->Upgrade_model->checkTrialExpired($CI->session->userdata['logged_in']['id']);
        if ($date) {
            $today = time();
            $end = strtotime($date->billing_end_date);
            if ($end > $today || $date->billing_end_date != '0000-00-00') {
                return false; //Not expire
            } else {
                return true; //Expire
            } 
        } else {
            return true; //Expire becuase not found
        }
    }

}

if (!function_exists('userSubcriptionInvalid')) {

    function userSubcriptionInvalid() {
        $CI = & get_instance();
        $CI->load->model('Upgrade_model');
        if (isTrialExpired()) {
            $date = $CI->Upgrade_model->checkUserSubcription($CI->session->userdata['logged_in']['id']);
            if($date->packages_id ==10 || $date->packages_id == 8 || $date->packages_id == 7 || $date->packages_id == 1 || $date->packages_id == 4){
                return false; // Not expire 
            }
            else{
                if (isset($date->billing_end_date)) {
                    $datetime1 = date_create(date('Y-m-d'));
                    $datetime2 = date_create($date->billing_end_date);
                    $interval = date_diff($datetime1, $datetime2);
                    $diff = $interval->format('%R%a');               
                    if ($diff >= -2) { // Two extra days allow
                       return false; // Not expire 
                    } else {
                        return true; // also subcription expire  after giving two extra days
                    }
                } else {
                    return true; // No subscription found
                }
            }
            return true; // Yes trial expired, So subscriptin is invalid
        } else {
            return false;
        }
    }

}
if (!function_exists('userAccountSpendCheck')) {

    function userAccountSpendCheck() {
        $CI = & get_instance();
        $CI->load->model('Upgrade_model');
        if (isTrialExpired()) {
            $where = array('user_id' => $CI->session->userdata['logged_in']['id']);
            $userSubscriptionArr = $CI->Upgrade_model->getData('user_subscription', $where);
            
            $billing_start_date = $userSubscriptionArr['0']['billing_start_date'];
            $billing_end_date = $userSubscriptionArr['0']['billing_end_date'];
            $status = $userSubscriptionArr['0']['status'];
            $packages_id = $userSubscriptionArr['0']['packages_id'];
            
            $where = array('user_id' => $CI->session->userdata['logged_in']['id']);
            $userAdsAccountArr = $CI->Upgrade_model->getData('user_ad_account', $where);
            
            $where = array('id' => $packages_id);
            $packagesArr = $CI->Upgrade_model->getData('packages', $where);
            $monthly_limit = $packagesArr[0]['monthly_limit'];
            
            $userAdsStr = '';
            if(!empty($userAdsAccountArr)){
                foreach($userAdsAccountArr as $k=>$v){
                    $userAdsStr .= '"'.$v['ad_account_id'].'",';
                }
                $userAdsStr = rtrim($userAdsStr, ',');
            }
            
            $where = array('user_id' => $CI->session->userdata['logged_in']['id']);
            if(!empty($userAdsStr)){
                $whereCustom = 'ad_account_id in ('.$userAdsStr.')';
            }
            $userSpendArr = $CI->Upgrade_model->getDataTotalSpend('user_daily_spend', $where, $whereCustom);
            $totalAccountSpend = $userSpendArr[0]->spend;
            
            /*$where = array('user_id' => $CI->session->userdata['logged_in']['id']);
            if(!empty($userAdsStr)){
                $whereCustom = 'adaccountid in ('.$userAdsStr.') and campaignstatus = "ACTIVE"';
            }
            else{
                $whereCustom = 'campaignstatus = "ACTIVE"';
            }
            $userCampaignArr = $CI->Upgrade_model->getData('campaigns', $where, $whereCustom);
            
            $userCampaignStr = '';
            if(!empty($userCampaignArr)){
                foreach($userCampaignArr as $k1=>$v1){
                    $userCampaignStr .= '"'.$v1['id'].'",';
                }
                $userCampaignStr = rtrim($userCampaignStr, ',');
            }
            
            
            $where = array('uid' => $CI->session->userdata['logged_in']['id']);
            //$whereCustom = 'campaign_id in ('.$userCampaignStr.')';
            if(!empty($userCampaignStr)){
                $whereCustom = 'campaign_id in ('.$userCampaignStr.') and DATE_FORMAT(created_at, "%Y-%m-%d") BETWEEN "'.$billing_start_date.'" AND "'.$billing_end_date.'"';
            }
            else{
                $whereCustom = 'DATE_FORMAT(created_at, "%Y-%m-%d") BETWEEN "'.$billing_start_date.'" AND "'.$billing_end_date.'"';
            }
            $userBudgetArr = $CI->Upgrade_model->getData('user_budget', $where, $whereCustom);
            #echo $CI->db->last_query();
               
            $totalAccountSpend = 0;
            if(!empty($userBudgetArr)){
                $campaignWiseSpend = 0;
                foreach($userBudgetArr as $k2=>$v2){
                    $startDate = strtotime($v2['start_date']);
                    $endDate = strtotime(date('Y-m-d'));
                    
                    $days_between = ceil(abs($endDate - $startDate) / 86400);
                    $campaignWiseSpend = $days_between * $v2['budgetamount'];
                    $totalAccountSpend = $totalAccountSpend + $campaignWiseSpend;
                }
            }*/
            #echo $totalAccountSpend."<br>".$monthly_limit;exit;
            #echo $CI->session->userdata['cur_currency'];exit;
            if($CI->session->userdata['cur_currency'] == 'CO$'){
                $totalAccountSpend = $totalAccountSpend / 3000;
                /*$1 = 0.89 Euro
                $1 = 3,000 colombian pesos
                $1 = 1.30 Australian Dollar
                $1 = 1.30 Canadian Dollar
                $1 = 66.77 Indian Rupee
                $1 = 18.30 Mexican Pesos
                $1 = 0.77 GBP*/
            }
            else if($CI->session->userdata['cur_currency'] == '�'){
                $totalAccountSpend = $totalAccountSpend / 0.89;
            }
            else if($CI->session->userdata['cur_currency'] == 'AU$'){
                $totalAccountSpend = $totalAccountSpend / 1.30;
            }
            else if($CI->session->userdata['cur_currency'] == 'CA$'){
                $totalAccountSpend = $totalAccountSpend / 1.30;
            }
            else if($CI->session->userdata['cur_currency'] == 'INR'){
                $totalAccountSpend = $totalAccountSpend / 66.77;
            }
            else if($CI->session->userdata['cur_currency'] == 'MAX$'){
                $totalAccountSpend = $totalAccountSpend / 18.30;
            }
            else if($CI->session->userdata['cur_currency'] == 'Rp'){
                $totalAccountSpend = $totalAccountSpend / 13037;
            }
            #echo $totalAccountSpend;
            #echo "<br>".$monthly_limit;exit;
            if((int)$totalAccountSpend > (int)$monthly_limit){
                return true; // Not Spend limit reach
            }
            else{
                return false; // Spend limit reach
            }
        } 
        else {
            return false;
        }
    }

}

//BY ASIF CHUHAN RAJPOOT GUL-LAARI---------------------------------
if (!function_exists('msg_box')) {

    function msg_box($id) {
        echo '<div id="msg_box' . $id . '" class="alert alert-danger displayNone msg_box">
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              			<div id="msg_err' . $id . '"></div>
              		</div>';
    }

}
if (!function_exists('loader_box')) {

    function loader_box($id) {
        echo '<div id="loder_box' . $id . '" class="loaderBox text-center displayNone">
                    <i class="fa fa-spinner fa-spin"></i>
          </div>';
    }

}
if (!function_exists('success_message')) {

    function success_message($id) {
        echo '<div id="successmsgBx' . $id . '" class="alert alert-success displayNone">
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              			<div id="msg_suc' . $id . '"></div>
              		</div>';
    }

}

 
