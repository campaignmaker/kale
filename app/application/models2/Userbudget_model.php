<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Addesign_model
 *
 * @author Asif
 */
class Userbudget_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function saveuserbudget($data) {
        $this->db->insert('user_budget', $data);
        return $this->db->insert_id();
    }

//end of

    function updateuserbudget($data) {

        $this->db->where('adsetId', $data['adsetId']);
        $q = $this->db->get('user_budget');

        if ($q->num_rows() > 0) {
            $this->db->where('adsetId', $data['adsetId']);
            $this->db->update('user_budget', $data);
            return $this->db->affected_rows();
        } else {
           return $this->saveuserbudget($data);
        }
    }

//end of
}

//end of Addesign_model
