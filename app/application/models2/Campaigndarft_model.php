<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Addesign_model
 *
 * @author Asif
 */
class Campaigndarft_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function recordAlreadyExist($userId, $campaignname = NULL, $adobjective = NULL) {
        if ($campaignname == NULL || $adobjective == NULL) {
            return false;
        }
        $this->db->select('ID,userId,campaignname,adobjective');
        $this->db->from('campaign_darft');
        $this->db->where('campaignname', $campaignname);
        $this->db->where('adobjective', $adobjective);
        $this->db->where('userId', $userId);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function savecmpdraft($data) {
        $this->db->insert('campaign_darft', $data);
        return $this->db->insert_id();
    }

//end of

    public function getcmpdrfts($userId) {

        $this->db->select('ID,campaignname,adobjective,CreationTime,UpdateTime');
        $this->db->from('campaign_darft');
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getcmpdrfts1($userId, $searchKeyword) {

        $this->db->select('ID,campaignname,adobjective,CreationTime,UpdateTime');
        $this->db->from('campaign_darft');
        $this->db->where('userId', $userId);
        if(!empty($searchKeyword)){
            $this->db->where("campaignname LIKE '%$searchKeyword%'");
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function updatecmpdraft($id, $data) {
        $this->db->where('ID', $id);
        if ($this->db->update('campaign_darft', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteCmpDrft($id) {

        if ($this->db->delete('campaign_darft', array('ID' => $id))) {
            return true;
        } else {
            return false;
        }
    }

    public function getCmpDraft($userId, $dId) {

        $this->db->select('*');
        $this->db->from('campaign_darft');
        $this->db->where('userId', $userId);
        $this->db->where('ID', $dId);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

}

//end of Addesign_model
