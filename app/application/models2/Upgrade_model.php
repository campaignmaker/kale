<?php

Class Upgrade_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getPackage($id) {

        $this->db->from('packages');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    function userSubscription($subcription_data) {
        if ($this->db->insert('user_subscription', $subcription_data)) {
            return true;
        } else {
            return false;
        }
    }

    function checkTrialExpired($userId) {

        $this->db->select('id, billing_end_date');
        $this->db->from('user_subscription');
        $this->db->where('packages_id', 1);
        $this->db->where('user_id', $userId);
        $this->db->where('status', 'Active'); // When user subcribe then status will be complete 
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function checkUserSubcription($userId) {

        $this->db->select('*');
        $this->db->from('user_subscription');
        $this->db->where_not_in('packages_id', 1);
        $this->db->where('user_id', $userId);
        // $this->db->where('status', 'Active');
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function userCurrentSubscriptionDetail($userId) {
        $this->db->select('id,packages_id');
        $this->db->from('user_subscription');
        //$this->db->where_not_in('packages_id', 1);
        $this->db->where('user_id', $userId);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->db->from('packages');
            $this->db->where_in('id', $query->row('packages_id'));
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->row();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function updateTrialSubcription($userId) {

        $this->db->select('id');
        $this->db->from('user_subscription');
        $this->db->where('packages_id', 1);
        $this->db->where('user_id', $userId);
        $this->db->where('status', 'Active');
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->db->where('id', $query->row('id'));
            if ($this->db->update('user_subscription', array('status' => 'Complete'))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function packageLimit($userId) {
        $this->db->select('us.*,p.plan_cycle,p.daily_limit,monthly_limit', FALSE);
        $this->db->from('user_subscription as us, packages as p', FALSE);
        $this->db->where('p.id', 'us.packages_id', FALSE);
        $this->db->where('user_id', $userId);
        $this->db->where('status', 'Active');
        $this->db->order_by('us.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function dailyUseLimit($userId) {
        $this->db->select("sum((case when budget_type = 'lifetime_budget' THEN round(budgetamount/DATEDIFF(end_date,start_date)) ELSE budgetamount END)) as todayusedamount", FALSE);
        $this->db->from('user_budget');
        $this->db->where('DATE(created_at)', 'CURDATE()',FALSE);
        $this->db->where('uid', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    
    function monthlyUseLimit($userId,$billingStartDate, $billingEndDate) {
        $this->db->select("sum((case when budget_type = 'lifetime_budget' THEN round(budgetamount/DATEDIFF(end_date,start_date)) ELSE budgetamount END)) as monthlyusedamount", FALSE);
        $this->db->from('user_budget');
        $this->db->where("DATE(created_at) between ('$billingStartDate') and ('$billingEndDate')", '',FALSE);
        $this->db->where('uid', $userId);
        $query = $this->db->get();
        #echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    
    function userSubscriptionDetail($userId) {
        $this->db->select('id,packages_id,subscription_id');
        $this->db->from('user_subscription');
        $this->db->where_not_in('packages_id', 1);
        $this->db->where('user_id', $userId);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
           return $query->row();
        } else {
            return false;
        }
    }
    
    function getData($tn, $where = '', $whereCustom='') {
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($whereCustom)) {
            $this->db->where($whereCustom);
        }
        $re = $this->db->get($tn);
        return $re->result_array();
    }
    
    function getDataTotalSpend($tablename, $where = '', $whereCustom=''){
        $this->db->select('SUM( spend  ) AS spend ');
        $this->db->from($tablename);
        if(!empty($where)){
            $this->db->where($where);
        }
        if (!empty($whereCustom)) {
            $this->db->where($whereCustom);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
}
