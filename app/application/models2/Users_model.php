<?php

Class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function login($email, $password) {

        /*
		$this->db->select('id, first_name,email,picture,accesstoken,disable_user');
        $this->db->from('users');
        $this->db->where('email', $email);
        if ($password !== 'DQeVR6u7Jx%R2G36R2GxDg4Q6u7Jx%R2G') {
            $this->db->where('password', MD5($password));
        }
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
		*/
		/*$this->db->select('us.id, us.first_name,us.email,us.picture,us.accesstoken,us.disable_user,usub.billing_end_date',FALSE);
        $this->db->from('users as us, user_subscription as usub',FALSE);
		$this->db->where('us.id', 'usub.user_id', FALSE);
        $this->db->where('email', $email);
        if ($password !== 'DQeVR6u7Jx%R2G36R2GxDg4Q6u7Jx%R2G') {
            $this->db->where('password', MD5($password));
        }
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }*/
		
		$this->db->select('id, first_name,email,picture,accesstoken,disable_user');
        $this->db->from('users');
        $this->db->where('email', $email);
        if ($password !== 'DQeVR6u7Jx%R2G36R2GxDg4Q6u7Jx%R2G') {
            $this->db->where('password', MD5($password));
        }
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        
		
		
        if ($query->num_rows() == 1) {
			
			$querysubs = $this->db->query("select id,billing_end_date from user_subscription where user_id=".$query->row('id'));
			
			
			$rowsubs = $querysubs->row();

			if (isset($rowsubs) && $rowsubs->billing_end_date != '0000-00-00'){
				$todaydate = date('Y-m-d'); 
				$obj_date = date('Y-m-d', strtotime($rowsubs->billing_end_date));
				//echo $rowsubs->id .$todaydate . "sdsd" . $obj_date;
				if($todaydate >= $obj_date){
					//echo "junaid";
					$this->db->where('id',$query->row('id'));
            		$this->db->update('users', array('disable_user' => 0));
				}
			}
			
			
			
			$this->db->select('id, first_name,email,picture,accesstoken,disable_user');
			$this->db->from('users');
			$this->db->where('email', $email);
			$this->db->limit(1);
	
			$query = $this->db->get();
            return $query->row();
        } else {
            return false;
        }
		
    }

    public function register($user) {
        $this->db->select('id, email');
        $this->db->from('users');
        $this->db->where('email', $user['email']);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('users', $user);
            return $this->db->insert_id();
        }
    }

    public function verifyEmailAddress($hash) {

        $this->db->select('id,email,first_name');
        $this->db->from('users');
        $this->db->where('hash', $hash);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $user = $query->row();
            $this->remove_user_hash($user->id);
            return $user;
        } else {
            return false;
        }
    }

    public function remove_user_hash($id) {
        $data = array(
            'hash' => '',
            'status' => 1
        );
        $this->db->where('id', $id);

        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function CheckEmailAddress($hash) {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('hash', $hash);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row('id');
        } else {
            return false;
        }
    }

    public function update_user_hash($id, $hash) {
        $data = array(
            'hash' => $hash
        );
        $this->db->where('id', $id);

        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function forgetpassword($email) {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row('id');
        } else {
            return false;
        }
    }

    public function update_password($id, $password) {
        $data = array(
            'hash' => '',
            'password' => $password,
            'status' => 1
        );
        $this->db->where('id', $id);

        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function UpdateOldPassword($id, $oldpassword, $newpassword) {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('password', $oldpassword);
        $this->db->where('status', 1);
        $this->db->where('id', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $this->db->where('id', $id);
            if ($this->db->update('users', array('password' => $newpassword))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function edit_profile($user, $uid) {

        if (isset($user['fbid']) && $user['fbid'] != NULL) {
            $this->db->select('id');
            $this->db->from('users');
            $this->db->where('fbid', $user['fbid']);
            $this->db->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return false;
            }
        }


        if ($this->db->update('users', $user, array('id' => $uid))) {
            return true;
        } else {
            return false;
        }
    }

    public function get_user_id_from_email($email) {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email);

        return $this->db->get()->row('id');
    }

    public function get_user($id) {

        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);

        return $this->db->get()->row();
    }
	
	public function get_user_subscription($id) {

        $this->db->select('*');
        $this->db->from('user_subscription');
        $this->db->where('user_id', $id);

        return $this->db->get()->row();
    }

    public function get_user_specific_detail($id, $data) {
        if (empty($data)) {
            return;
        }
        if (is_array($data)) {
            $string = rtrim(implode(',', $arr), ',');
            $this->db->select($string);
        } else {
            $this->db->select($data);
        }
        $this->db->from('users');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function getUserAddAccountId($userId) {

        $this->db->select('id,add_title,ad_account_id,status');
        $this->db->from('user_ad_account');
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return;
        }
    }

    public function getUserAddAccountName($ad_account_id) {
        $this->db->select('*');
        $this->db->from('user_ad_account');
        $this->db->where('ad_account_id', $ad_account_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return;
        }
    }

    public function addAdAccountId($adAcData) {
        $this->db->select('id');
        $this->db->from('user_ad_account');
        //$this->db->where('user_id', $adAcData['user_id']);
        $this->db->where('ad_account_id', $adAcData['ad_account_id']);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            $this->db->insert('user_ad_account', $adAcData);
            return true;
        }
    }

    public function deleteAdAccount($uid) {

        if ($this->db->delete('user_ad_account', array('user_id' => $uid))) {
            return true;
        } else {
            return false;
        }
    }

    public function getCountryName($countryCode) {

        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('CountryCode', $countryCode);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return;
        }
    }
}
