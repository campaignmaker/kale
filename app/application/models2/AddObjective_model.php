<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Addesign_model
 *
 * @author Asif
 */
class AddObjective_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

     public function get_objective_id_byname($name) {
        $this->db->select('objective_id');
        $this->db->from('ad_objective');
        $this->db->where('objective_name',trim($name));
        return $this->db->get()->row('objective_id');
    }

}

//end of Addesign_model
