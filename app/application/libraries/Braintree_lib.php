<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/*
 *  Braintree_lib
 *  This is a codeigniter wrapper around the braintree sdk, any new sdk can be wrapped around here
 *  License: MIT to accomodate braintree open source sdk license (BSD)
 *  Author: Clint Canada
 *  Library tests (and parameters for lower Braintree functions) are found in:
 *  https://github.com/braintree/braintree_php/tree/master/tests/integration
 */


class Braintree_lib {

    function __construct() {
        $CI = &get_instance();
        $CI->config->load('braintree', TRUE);
        $braintree = $CI->config->item('braintree');
        // Let us load the configurations for the braintree library
        Braintree_Configuration::environment($braintree['braintree_environment']);
        Braintree_Configuration::merchantId($braintree['braintree_merchant_id']);
        Braintree_Configuration::publicKey($braintree['braintree_public_key']);
        Braintree_Configuration::privateKey($braintree['braintree_private_key']);
    }

    // This function simply creates a client token for the javascript sdk
    function create_client_token() {
        $clientToken = Braintree_ClientToken::generate();
        return $clientToken;
    }

}
