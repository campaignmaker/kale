<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Availability_limit {

    private $CI = NULL;
    private $user_id = NULL;

    function __construct() {
        $this->CI = & get_instance();
        $this->user_id = $this->CI->session->userdata['logged_in']['id'];
        $this->CI->load->model('Upgrade_model');
    }

    function getUsedLimit() {
        $dailyUseLimit = $this->CI->Upgrade_model->dailyUseLimit($this->user_id);
        $todayusedamount = $dailyUseLimit->todayusedamount ? $dailyUseLimit->todayusedamount : 0;

        $subscriptionDetail = $this->CI->Upgrade_model->packageLimit($this->user_id);
        $getDate = $this->createCurrentMonthFilter($subscriptionDetail->billing_start_date, $subscriptionDetail->billing_end_date);
        if ($getDate) {
            $monthlyUseLimit = $this->CI->Upgrade_model->monthlyUseLimit($this->user_id, $getDate->startDate, $getDate->endDate);
            $monthlyusedamount = $monthlyUseLimit->monthlyusedamount ? $monthlyUseLimit->monthlyusedamount : 0;
        } else {
            $monthlyusedamount = $subscriptionDetail->monthly_limit;// '100000000'; // Subcription end
        }

        return (object) array('monthlyUsed' => $monthlyusedamount, 'todayUsed' => $todayusedamount);
    }

    function dailyAvailabilityLimit($data = null) {

        if ($data['budget_type'] == "daily_budget") {
            $amount = $data['budgetAmount'];
        } else if ($data['budget_type'] == "lifetime_budget") {
            $datediff = strtotime($data['adset_end_date']) - strtotime($data['adset_start_date']);
            $amount = $data['budgetAmount'] / floor($datediff / (60 * 60 * 24));
        }


        if (!($amount)) {
            $amount = 0;
        }

        $subscriptionDetail = $this->CI->Upgrade_model->packageLimit($this->user_id);
        $dailyUseLimit = $this->CI->Upgrade_model->dailyUseLimit($this->user_id);

        $dailyLimit = $subscriptionDetail->daily_limit;
        $todayusedamount = $dailyUseLimit->todayusedamount ? $dailyUseLimit->todayusedamount : 0;
        $total = $todayusedamount + $amount;
        // echo "DL=".$dailyLimit."Tot=".$total;
        if ($dailyLimit <= $total) {
            return false; // Daily Limit exceed
        } else {
            return true; // Allow to create campaing
        }
    }

    function monthlyAvailabilityLimit($data = null) {

        if ($data['budget_type'] == "daily_budget") {
            $amount = $data['budgetAmount'];
        } else if ($data['budget_type'] == "lifetime_budget") {
            $datediff = strtotime($data['adset_end_date']) - strtotime($data['adset_start_date']);
            $amount = $data['budgetAmount'] / floor($datediff / (60 * 60 * 24));
        }

        if (!($amount)) {
            $amount = 0;
        }

        $subscriptionDetail = $this->CI->Upgrade_model->packageLimit($this->user_id);
        
        $getDate = $this->createCurrentMonthFilter($subscriptionDetail->billing_start_date, $subscriptionDetail->billing_end_date);
        if ($getDate) {
            $monthlyUseLimit = $this->CI->Upgrade_model->monthlyUseLimit($this->user_id, $getDate->startDate, $getDate->endDate);

            $monthlyLimit = $subscriptionDetail->monthly_limit;
            $monthlyusedamount = $monthlyUseLimit->monthlyusedamount ? $monthlyUseLimit->monthlyusedamount : 0;

            $total = $monthlyusedamount + $amount;
            if ($monthlyLimit <= $total) {
                return false; // Monthly Limit exceed
                // echo 1;
            } else {
                return true; // Allow to create campaing
                //echo 2;
            }
        } else {
            return false; // Subcription end
            //echo 3;
        }
    }

    function createCurrentMonthFilter($startDate, $endDate, $currentDate = NULL) {

        if ($currentDate) {
            $currentDate = strtotime($currentDate);
        } else {
            $currentDate = strtotime(Date('Y-m-d'));
        }

        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);
        
        if ($startDate <= $currentDate && $endDate >= $currentDate) {
            $months = 0;
            $stDate = $startDate;
            $edDate = strtotime('+1 day', $endDate);
            while (($stDate = strtotime('+1 MONTH', $stDate)) <= $edDate) {
                if (strtotime('+1 month', $startDate) < $currentDate && $endDate > $currentDate) {
                    $startDate = strtotime('+1 month', $startDate);
                    $finalStartDate = $startDate;
                } else {
                    $finalStartDate = $startDate;
                    $finalEndDate = strtotime('+1 month', $startDate);
                    break;
                }
                $months++;
            }
            
            if ($months == 0) {
                $finalStartDate = $startDate;
                $finalEndDate = $endDate;
            }
            
            if (strtotime('+1 month', $finalStartDate) < $endDate) {
                $finalEndDate = strtotime('+1 month', $finalStartDate);
            } else {
                $finalEndDate = $endDate;
            }
            return (object) array('startDate' => Date('Y-m-d', $finalStartDate), 'currentDate' => Date('Y-m-d', $currentDate), 'endDate' => Date('Y-m-d', $finalEndDate));
        } else {
            return false;
        }
    }

}
