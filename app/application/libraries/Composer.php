<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Composer {

    function __construct() {
        if (file_exists(APPPATH . 'vendor/autoload.php')) {
            require_once(APPPATH . 'vendor/autoload.php');
        } elseif (file_exists(APPPATH . '../vendor/autoload.php')) {
            require_once(APPPATH . '../vendor/autoload.php');
        }
    }

}
