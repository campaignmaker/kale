<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Seo {

    private $title = NULL;    // Contains the title
    private $description = NULL;     // Contains the description

    function __construct() {
        $this->set_defaults();
    }

    private function set_defaults() {
        $this->title = 'The Campaign Maker';
        $this->description = 'The Campaign Maker';
    }

    function SetValues($type, $data) {


        if ($type == 'Title') {
            $this->title = "The Campaign Maker - ".$data;
        }
        if ($type == 'Description') {
            $this->description = "The Campaign Maker - ".$data;
        }
    }

    function GetValues($type) {

        if ($type == 'Title') {
            return $this->title;
        }
        if ($type == 'Description') {
            return $this->description;
        }
    }

}
