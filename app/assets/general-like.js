var overLayDivHtml = '<div id="preloader"><div id="status"><i class="fa fa-spinner fa-spin"></i></div></div>';

$("#adAccountForm").submit(function (event) {

    var count_checked = $("[name='account_id_name[]']:checked").length; // count the checked rows
    if (count_checked == 0)
    {
        alert("Please select add account");
        return false;
    } else {
        return;
    }

    event.preventDefault();

});
function getCampaign(adAccountId, limit)
{
    getCampaignFeed();
}

function getAddSets(adAccountId, campaignId, limit)
{
    getAddSetFeed();
}
function getAdds(adAccountId, campaignId, addsetId, limit)
{
    getAdddsFeed();
}
function getAdd(adAccountId, campaignId, addsetId, addId, limit)
{    
    getAdFeed();
}
function ajaxloder(divclass, status)
{
    $("#overlay").remove();
    var over = '<div id="overlay"><img src="' + site_url + 'assets/images/ajax-loader.gif" id="img-load" /></div>';
    $(over).appendTo('body');
    if (status == 'show') {

        $t = $(divclass); // CHANGE it to the table's id you have

        $("#overlay").css({
            opacity: 0.8,
            top: $t.offset().top,
            width: $t.outerWidth(),
            height: $t.outerHeight(),
            left: $t.offset().left // the fix.
        });

        $("#img-load").css({
            top: ($t.height() / 2),
            left: ($t.width() / 2)
        });
        $("#overlay").fadeIn();
    } else if (status == 'hide') {

        $("#overlay").fadeOut();
    }
}
function checkradio(val)
{
    //alert(val);
    // var compaingname = $("#compaingname").val();
    //  var adaccountid = $("#adaccountid").val();
    //  $('#create_creativeads_frm')[0].reset();
    // $('#create_creativeads_frm input:not(.campaign-name)').val('');
    //  $("#compaingname").val(compaingname);
    //   $("#adaccountid").val(adaccountid);
    $("#" + val).trigger('click');

}

function getAddSetFeed()
{
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var page = $("#pageName").val();

    if (page == "addSetReport")
    {
        var url = site_url + 'likereports/getAddSetFeeds/';
        ajaxloder('.desktop-mobile1', 'show');
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        campaignId: campaignId,
                        addAccountId: addAccountId,
                        ajax: 1,
                        limit: $("#dayLimit").val()
                    },
            beforeSend: function ()
            {
                $(".desktop-mobile1 .alert").remove();
            },
            success: function (html)
            {
                ajaxloder('.desktop-mobile1', 'hide');
                if (html)
                {
                    try
                    {
                        var json = $.parseJSON(html);
                        if (json.code == 100)
                        {
                            //$(".slideUp").css("width", json.desktop + "%");
                            $(".desktop-value").html(json.desktop + "%");
                            $(".mobile-value").html(json.mobile + "%");
                            $(".progress-bar-info").css("width", json.desktop + "%");
                            getAddSetGender(addAccountId, campaignId);
                        }
                    } catch (ex)
                    {
                        getAddSetGender(addAccountId, campaignId);
                        $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                    }
                } else
                {
                    getAddSetGender(addAccountId, campaignId);
                    $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            },
            async: true
        });
    }
}

function getAddSetGender(addAccountId, campaignId)
{
    var url = site_url + 'likereports/getAddSetGendrs/';
    ajaxloder('.male-female-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".male-female-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    if (json.code == 100)
                    {
                        $(".male .male-value").html(json.male + "%");
                        $(".female .female-value").html(json.female + "%");
                        getAddSetChart(addAccountId, campaignId);
                    }
                } catch (ex)
                {
                    getAddSetChart(addAccountId, campaignId);
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }
            } else
            {
                getAddSetChart(addAccountId, campaignId);
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAddSetChart(addAccountId, campaignId)
{
    var url = site_url + 'likereports/getAddSetChart/';
    ajaxloder('.age-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".age-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.age-div', 'hide');
            if (html)
            {
                try
                {
                    generateAgeChart(html, addAccountId, campaignId);
                } catch (ex)
                {
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAddSetCountryChart(addAccountId, campaignId) {
    var url = site_url + 'likereports/getAddSetCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".country-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.country-div', 'hide');
            if (html)
            {
                try
                {
                    generateCountryChart(html, addAccountId, campaignId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAddSetTimeChart(addAccountId, campaignId) {
    var url = site_url + 'likereports/getAddSetTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".time-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.time-div', 'hide');
            if (html)
            {
                try
                {
                    generateTimeChart(html, addAccountId, campaignId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdddsFeed()
{
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var page = $("#pageName").val();

    if (page == "adddsReport")
    {
        var url = site_url + 'likereports/getAdddsFeeds/';
        ajaxloder('.desktop-mobile1', 'show');
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        campaignId: campaignId,
                        addAccountId: addAccountId,
                        addsetId: addsetId,
                        ajax: 1,
                        limit: $("#dayLimit").val()
                    },
            beforeSend: function ()
            {
                $(".desktop-mobile1 .alert").remove();
            },
            success: function (html)
            {
                ajaxloder('.desktop-mobile1', 'hide');
                if (html)
                {
                    try
                    {
                        var json = $.parseJSON(html);
                        if (json.code == 100)
                        {
                            //$(".slideUp").css("width", json.desktop + "%");
                            $(".desktop-value").html(json.desktop + "%");
                            $(".mobile-value").html(json.mobile + "%");
                            $(".progress-bar-info").css("width", json.desktop + "%");
                            getAdddsGender(addAccountId, campaignId, addsetId);
                        }
                    } catch (ex)
                    {
                        getAdddsGender(addAccountId, campaignId, addsetId);
                        $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                    }
                } else
                {
                    getAdddsGender(addAccountId, campaignId, addsetId);
                    $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            },
            async: true
        });
    }
}

function getAdddsGender(addAccountId, campaignId, addsetId)
{
    var url = site_url + 'likereports/getAdddsGendrs/';
    ajaxloder('.male-female-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".male-female-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    if (json.code == 100)
                    {
                        $(".male .male-value").html(json.male + "%");
                        $(".female .female-value").html(json.female + "%");
                        getAdddsChart(addAccountId, campaignId, addsetId);
                    }
                } catch (ex)
                {
                    getAdddsChart(addAccountId, campaignId, addsetId);
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }
            } else
            {
                getAdddsChart(addAccountId, campaignId, addsetId);
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdddsChart(addAccountId, campaignId, addsetId)
{

    var url = site_url + 'likereports/getAdddsChart/';
    ajaxloder('.age-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".age-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.age-div', 'hide');
            if (html)
            {
                try
                {
                    generateAgeChart(html, addAccountId, campaignId, addsetId);
                } catch (ex)
                {
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdddsCountryChart(addAccountId, campaignId, addsetId) {
    var url = site_url + 'likereports/getAdddsCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".country-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.country-div', 'hide');
            if (html)
            {
                try
                {
                    generateCountryChart(html, addAccountId, campaignId, addsetId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdddsTimeChart(addAccountId, campaignId, addsetId) {
    var url = site_url + 'likereports/getAdddsTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".time-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.time-div', 'hide');
            if (html)
            {
                try
                {
                    generateTimeChart(html, addAccountId, campaignId, addsetId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdFeed()
{
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var page = $("#pageName").val();

    if (page == "adReport")
    {
        var url = site_url + 'likereports/getAddFeeds/';
        ajaxloder('.desktop-mobile1', 'show');
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        campaignId: campaignId,
                        addAccountId: addAccountId,
                        addsetId: addsetId,
                        addId: addId,
                        ajax: 1,
                        limit: $("#dayLimit").val()
                    },
            beforeSend: function ()
            {
                $(".desktop-mobile1 .alert").remove();
            },
            success: function (html)
            {
                ajaxloder('.desktop-mobile1', 'hide');
                if (html)
                {
                    try
                    {
                        var json = $.parseJSON(html);
                        if (json.code == 100)
                        {
                            //$(".slideUp").css("width", json.desktop + "%");
                            $(".desktop-value").html(json.desktop + "%");
                            $(".mobile-value").html(json.mobile + "%");
                            $(".progress-bar-info").css("width", json.desktop + "%");
                            getAdGender(addAccountId, campaignId, addsetId, addId);
                        }
                    } catch (ex)
                    {
                        getAdGender(addAccountId, campaignId, addsetId, addId);
                        $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                    }
                } else
                {
                    getAdGender(addAccountId, campaignId, addsetId, addId);
                    $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            },
            async: true
        });
    }
}

function getAdGender(addAccountId, campaignId, addsetId, addId)
{
    var url = site_url + 'likereports/getAddGendrs/';
    ajaxloder('.male-female-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    addId: addId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".male-female-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    if (json.code == 100)
                    {
                        $(".male .male-value").html(json.male + "%");
                        $(".female .female-value").html(json.female + "%");
                        getAdChart(addAccountId, campaignId, addsetId, addId);
                    }
                } catch (ex)
                {
                    getAdChart(addAccountId, campaignId, addsetId, addId);
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }
            } else
            {
                getAdChart(addAccountId, campaignId, addsetId, addId);
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdChart(addAccountId, campaignId, addsetId, addId)
{
    var url = site_url + 'likereports/getAddChart/';
    ajaxloder('.age-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    addId: addId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".age-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.age-div', 'hide');
            if (html)
            {
                try
                {
                    generateAgeChart(html, addAccountId, campaignId, addsetId, addId);
                } catch (ex)
                {
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdCountryChart(addAccountId, campaignId, addsetId, addId) {
    var url = site_url + 'likereports/getAddCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    addId: addId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".country-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.country-div', 'hide');
            if (html)
            {
                try
                {
                    generateCountryChart(html, addAccountId, campaignId, addsetId, addId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdTimeChart(addAccountId, campaignId, addsetId, addId) {
    var url = site_url + 'likereports/getAddTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    addId: addId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".time-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.time-div', 'hide');
            if (html)
            {
                try
                {
                    generateTimeChart(html, addAccountId, campaignId, addsetId, addId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

$(document).ready(function ()
{
    getCampaignFeed();
    getAddSetFeed();
    getAdddsFeed();
    getAdFeed();
});

function getCampaignFeed(){
    var addAccountId = $("#addAccountId").val();
    var page = $("#pageName").val();
    if (page == "campaignsReport")
    {
        var url = site_url + 'likereports/getCampaignFeeds/';
        ajaxloder('.desktop-mobile1', 'show');
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        adAccountId: addAccountId,
                        ajax: 1,
                        limit: $("#dayLimit").val()
                    },
            beforeSend: function ()
            {
                $(".desktop-mobile .alert").remove();
            },
            success: function (html)
            {
                ajaxloder('.desktop-mobile', 'hide');
                if (html)
                {
                    try
                    {
                        var json = $.parseJSON(html);
                        if (json.code == 100)
                        {
                            //$(".slideUp").css("width", json.desktop + "%");
                            $(".desktop-value").html(json.desktop + "%");
                            $(".mobile-value").html(json.mobile + "%");
                            $(".progress-bar-info").css("width", json.desktop + "%");
                            getCampaignGender(addAccountId);
                        }
                    } catch (ex)
                    {
                        getCampaignGender(addAccountId);
                        $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                    }
                } else
                {
                    getCampaignGender(addAccountId);
                    $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            },
            async: true
        });
    }
}

function getCampaignGender(addAccountId) {
    var url = site_url + 'likereports/getCampaignGendrs/';
    ajaxloder('.male-female-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".male-female-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    if (json.code == 100)
                    {
                        $(".male .male-value").html(json.male + "%");
                        $(".female .female-value").html(json.female + "%");
                        getCampaignChart(addAccountId);
                    }
                } catch (ex)
                {
                    getCampaignChart(addAccountId);
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }
            } else
            {
                getCampaignChart(addAccountId);
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getCampaignChart(addAccountId) {
    var url = site_url + 'likereports/getCampaignChart/';
    ajaxloder('.age-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".age-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.age-div', 'hide');
            if (html)
            {
                try
                {
                    generateAgeChart(html, addAccountId, '');
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getCampaignCountryChart(addAccountId) {
    var url = site_url + 'likereports/getCampaignCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".country-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.country-div', 'hide');
            if (html)
            {
                try
                {
                    generateCountryChart(html, addAccountId, '');
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getCampaignTimeChart(addAccountId) {
    var url = site_url + 'likereports/getCampaignTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".time-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.time-div', 'hide');
            if (html)
            {
                try
                {
                    generateTimeChart(html, addAccountId, '');
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function generateAgeChart(resData, addAccountId, campaignId, addsetId, addId)
{
    google.load("visualization", "1", {packages: ["corechart"], callback: drawVisualization});
    function drawVisualization()
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Age');
        data.addColumn('number', 'Female');
        data.addColumn('number', 'Male');
        data.addColumn('number', 'Unknown');
        $.each(resData, function (i, resData1)
        {
            var value = resData1.value1;
            var value1 = resData1.value2;
            var value2 = resData1.value3;
            var name = resData1.name1;
            data.addRows([[name, value, value1, value2]]);
        });

        var options = {
            vAxis: {title: 'Impression'},
            width: '100%',
            height: '100%',
            seriesType: 'bars',
            colors: ['#ff625f', '#3fc6f3', '#42dd91'],
            series: {7: {type: 'line'}},
            bar: {groupWidth: "30%"},
            legend: {position: 'bottom'},
            chartArea: {'width': '90%'},
        };

        var chart;
        chart = new google.visualization.ComboChart(document.getElementById('age'));
        chart.draw(data, options);
    }
    var page = $("#pageName").val();
    if (page == "addSetReport") {
        getAddSetCountryChart(addAccountId, campaignId);
    } else if (page == "adddsReport") {
        getAdddsCountryChart(addAccountId, campaignId, addsetId);
    } else if (page == "adReport") {
        getAdCountryChart(addAccountId, campaignId, addsetId, addId);
    } else {
        getCampaignCountryChart(addAccountId);
    }
}

function generateCountryChart(resData, addAccountId, campaignId, addsetId, addId)
{
    google.load("visualization", "1", {packages: ["corechart"], callback: drawVisualization});
    function drawVisualization()
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'City');
        data.addColumn('number', 'Impression');
        $.each(resData, function (i, resData1)
        {
            var value = resData1.value1;
            var name = resData1.name1;
            data.addRows([[name, value]]);
        });

        var options = {
            xAxis: {title: 'Impression'},
            pieHole: 0.85,
            width: '100%',
            height: '100%',
            legend: {position: 'right'},
            chartArea: {'width': '100%'},
            colors: ['#3fc6f3', '#ff625f', '#42dd8f']
        };

        var chart;
        chart = new google.visualization.PieChart(document.getElementById('country'));
        chart.draw(data, options);
    }
    var page = $("#pageName").val();
    if (page == "addSetReport") {
        getAddSetTimeChart(addAccountId, campaignId);
    } else if (page == "adddsReport") {
        getAdddsTimeChart(addAccountId, campaignId, addsetId);
    } else if (page == "adReport") {
        getAdTimeChart(addAccountId, campaignId, addsetId, addId);
    } else {
        getCampaignTimeChart(addAccountId);
    }
}

function generateTimeChart(resData, addAccountId, campaignId, addsetId, addId)
{
    google.load("visualization", "1", {packages: ["corechart"], callback: drawVisualization});
    function drawVisualization()
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Time');
        data.addColumn('number', 'Impression');
        data.addColumn({type: 'string', role: 'style'});
        //data.addColumn('string', '{"role": "style}'); 

        var colors = ['#42dd8f', '#465866', '#ff625f', '#3fc6f3', '#42dd8f', '#465866'];
        var i = 0;
        data.addRows(6);
        $.each(resData, function (i, resData1)
        {
            var value = resData1.value1;
            var value1 = resData1.value2;
            var name = resData1.name1;
            //data.addRows([[name, value, '#42dd8f']]);
            data.setValue(i, 0, name);
            data.setValue(i, 1, value);
            data.setValue(i, 2, colors[i]);
            i++;
        });

        var options = {
            vAxis: {title: 'Impression'},
            seriesType: 'bars',
            series: {6: {type: 'line'}},
            bar: {groupWidth: "30%"},
            legend: {position: 'none'},
            isStacked: false,
            chartArea: {'width': '80%'},
        };

        var chart;
        chart = new google.visualization.ComboChart(document.getElementById('time'));
        chart.draw(data, options);
    }
}