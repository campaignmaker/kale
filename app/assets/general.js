var overLayDivHtml = '<div id="preloader"><div id="status"><i class="fa fa-spinner fa-spin"></i></div></div>';

$("#adAccountForm").submit(function (event) {

    var count_checked = $("[name='account_id_name[]']:checked").length; // count the checked rows
    if (count_checked == 0)
    {
        alert("Please select add account");
        return false;
    } else {
        return;
    }

    event.preventDefault();

});
function getCampaign(adAccountId, limit)
{

    var url = site_url + 'reports/index/';
    ajaxloder('.contant .container', 'show');
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.contant .container', 'hide');
            if (html)
            {
                getCampaignFeed();
                $(".campaigns").html(html);
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });

}

function getDatesRecords(startDate, endDate)
{
    var url = site_url + 'reports/index/';
    ajaxloder('.page-content', 'show');
    var addAccountId = $("#addAccountId").val();
    var limit = startDate + "#" + endDate;
    //console.log(limit);
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            if (html)
            {
                getCampaignFeed();
                $(".campaigns").html(html);
                var oTable = $('#sample_1').dataTable({"oLanguage": {
                        "sLengthMenu": "_MENU_ Campaigns",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ Campaigns",
                    }});
                oTable.fnDraw();
                var tableWrapper = $('#sample_1_wrapper');
                jQuery('.dataTables_filter input', tableWrapper).addClass("form-control input-medium input-inline"); // modify table search input
                jQuery('.dataTables_length select', tableWrapper).addClass("form-control input-small"); // modify table per page dropdown
                jQuery('.dataTables_length select', tableWrapper).select2(); // initialize select2 dropdown
                $('.lcs_check').lc_switch();
                $('#sample_1').on('draw.dt', function () {
                    $('.lcs_check').lc_switch();
                });
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getDatesRecords1(startDate, endDate)
{
    var url = site_url + 'addset/index/';

    ajaxloder('.page-content', 'show');
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var limit = startDate + "#" + endDate;
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    adAccountId: addAccountId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            if (html)
            {
                getAddSetFeed();
                $(".campaigns").html(html);
                var oTable = $('#sample_Adsets').dataTable({"oLanguage": {
                        "sLengthMenu": "_MENU_ Adsets",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ Adsets",
                    }});
                oTable.fnDraw();
                var tableWrapper = $('#sample_Adsets_wrapper');
                jQuery('.dataTables_filter input', tableWrapper).addClass("form-control input-medium input-inline"); // modify table search input
                jQuery('.dataTables_length select', tableWrapper).addClass("form-control input-small"); // modify table per page dropdown
                jQuery('.dataTables_length select', tableWrapper).select2(); // initialize select2 dropdown
                $('.lcs_check').lc_switch();
                $('#sample_Adsets').on('draw.dt', function () {
                    $('.lcs_check').lc_switch();
                });
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getDatesRecords2(startDate, endDate)
{
    var url = site_url + 'addds/index/';

    ajaxloder('.page-content', 'show');
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var limit = startDate + "#" + endDate;
    $("#dayLimit").val(limit);
    //alert("ss");
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    adAccountId: addAccountId,
                    addSetId: addsetId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            if (html)
            {
                getAdddsFeed();
                $(".campaigns").html(html);
                var oTable = $('#sample_Ads').dataTable({"oLanguage": {
                        "sLengthMenu": "_MENU_ Ads",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ Ads",
                    }});
                oTable.fnDraw();
                var tableWrapper = $('#sample_Ads_wrapper');
                jQuery('.dataTables_filter input', tableWrapper).addClass("form-control input-medium input-inline"); // modify table search input
                jQuery('.dataTables_length select', tableWrapper).addClass("form-control input-small"); // modify table per page dropdown
                jQuery('.dataTables_length select', tableWrapper).select2(); // initialize select2 dropdown
                $('.lcs_check').lc_switch();
                $('#sample_Ads').on('draw.dt', function () {
                    $('.lcs_check').lc_switch();
                });
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getDatesRecords3(startDate, endDate)
{
    var url = site_url + 'adone/index/';

    ajaxloder('.page-content', 'show');
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var limit = startDate + "#" + endDate;
    $("#dayLimit").val(limit);
    //alert("ss");
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    adAccountId: addAccountId,
                    addSetId: addsetId,
                    addId: addId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            if (html)
            {
                getAdFeed();
                $(".campaigns").html(html);
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAddSets(adAccountId, campaignId, limit)
{

    var url = site_url + 'addset/index/';
    ajaxloder('.page-content', 'show');
    //alert(limit);
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    campaignId: campaignId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {

            $(".alert").remove();

        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            if (html)
            {
                getAddSetFeed();

                $(".campaigns").html(html);
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });

}
function getAdds(adAccountId, campaignId, addsetId, limit)
{

    var url = site_url + 'addds/index/';
    ajaxloder('.page-content', 'show');
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    campaignId: campaignId,
                    addSetId: addsetId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {

            $(".alert").remove();

        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            if (html)
            {
                getAdddsFeed();
                $(".campaigns").html(html);
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });

}
function getAdd(adAccountId, campaignId, addsetId, addId, limit)
{

    var url = site_url + 'adone/index/';
    ajaxloder('.page-content', 'show');
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    campaignId: campaignId,
                    addSetId: addsetId,
                    addId: addId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {

            $(".alert").remove();

        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            if (html)
            {
                getAdFeed();
                $(".campaigns").html(html);
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });

}
function ajaxloder(divclass, status)
{
    $("#overlay").remove();
    var over = '<div id="overlay"><img src="' + site_url + 'assets/images/ajax-loader.gif" id="img-load" /></div>';
    $(over).appendTo('body');
    if (status == 'show') {

        $t = $(divclass); // CHANGE it to the table's id you have

        $("#overlay").css({
            opacity: 0.8,
            top: $t.offset().top,
            width: $t.outerWidth(),
            height: $t.outerHeight(),
            left: $t.offset().left // the fix.
        });

        $("#img-load").css({
            top: ($t.height() / 2),
            left: ($t.width() / 2)
        });
        $("#overlay").fadeIn();
    } else if (status == 'hide') {

        $("#overlay").fadeOut();
    }
}
function checkradio(val)
{
    //alert(val);
    // var compaingname = $("#compaingname").val();
    //  var adaccountid = $("#adaccountid").val();
    //  $('#create_creativeads_frm')[0].reset();
    // $('#create_creativeads_frm input:not(.campaign-name)').val('');
    //  $("#compaingname").val(compaingname);
    //   $("#adaccountid").val(adaccountid);
    $("#" + val).trigger('click');

}

function getCampaignFeed()
{
    var addAccountId = $("#addAccountId").val();
    var page = $("#pageName").val();
    if (page == "campaignsReport")
    {
        var url = site_url + 'reports/getCampaignFeeds/';
        ajaxloder('.desktop-mobile1', 'show');
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        adAccountId: addAccountId,
                        ajax: 1,
                        limit: $("#dayLimit").val()
                    },
            beforeSend: function ()
            {
                $(".desktop-mobile .alert").remove();
            },
            success: function (html)
            {
                ajaxloder('.desktop-mobile', 'hide');
                if (html)
                {
                    try
                    {
                        var json = $.parseJSON(html);
                        if (json.code == 100)
                        {
                            //$(".slideUp").css("width", json.desktop + "%");
                            $(".desktop-value").html(json.desktop + "%");
                            $(".mobile-value").html(json.mobile + "%");
                            $(".progress-bar-info").css("width", json.desktop + "%");
                            getCampaignGender(addAccountId);
                        }
                    } catch (ex)
                    {
                        getCampaignGender(addAccountId);
                        $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                    }
                } else
                {
                    getCampaignGender(addAccountId);
                    $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            },
            async: true
        });
    }
}

function getCampaignGender(addAccountId)
{
    var url = site_url + 'reports/getCampaignGendrs/';
    ajaxloder('.male-female-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".male-female-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    if (json.code == 100)
                    {
                        $(".male .male-value").html(json.male + "%");
                        $(".female .female-value").html(json.female + "%");
                        getCampaignChart(addAccountId);
                    }
                } catch (ex)
                {
                    getCampaignChart(addAccountId);
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }
            } else
            {
                getCampaignChart(addAccountId);
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getCampaignChart(addAccountId)
{
    var url = site_url + 'reports/getCampaignChart/';
    ajaxloder('.age-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".age-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.age-div', 'hide');
            if (html)
            {
                try
                {
                    generateAgeChart(html, addAccountId, '');
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getCampaignCountryChart(addAccountId) {
    var url = site_url + 'reports/getCampaignCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".country-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.country-div', 'hide');
            if (html)
            {
                try
                {
                    generateCountryChart(html, addAccountId, '');
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getCampaignTimeChart(addAccountId) {
    var url = site_url + 'reports/getCampaignTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    adAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".time-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.time-div', 'hide');
            if (html)
            {
                try
                {
                    generateTimeChart(html, addAccountId, '');
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAddSetFeed()
{
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var page = $("#pageName").val();

    if (page == "addSetReport")
    {
        var url = site_url + 'addset/getAddSetFeeds/';
        ajaxloder('.desktop-mobile1', 'show');
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        campaignId: campaignId,
                        addAccountId: addAccountId,
                        ajax: 1,
                        limit: $("#dayLimit").val()
                    },
            beforeSend: function ()
            {
                $(".desktop-mobile1 .alert").remove();
            },
            success: function (html)
            {
                ajaxloder('.desktop-mobile1', 'hide');
                if (html)
                {
                    try
                    {
                        var json = $.parseJSON(html);
                        if (json.code == 100)
                        {
                            //$(".slideUp").css("width", json.desktop + "%");
                            $(".desktop-value").html(json.desktop + "%");
                            $(".mobile-value").html(json.mobile + "%");
                            $(".progress-bar-info").css("width", json.desktop + "%");
                            getAddSetGender(addAccountId, campaignId);
                        }
                    } catch (ex)
                    {
                        getAddSetGender(addAccountId, campaignId);
                        $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                    }
                } else
                {
                    getAddSetGender(addAccountId, campaignId);
                    $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            },
            async: true
        });
    }


}

function getAddSetGender(addAccountId, campaignId)
{
    var url = site_url + 'addset/getAddSetGendrs/';
    ajaxloder('.male-female-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".male-female-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    if (json.code == 100)
                    {
                        $(".male .male-value").html(json.male + "%");
                        $(".female .female-value").html(json.female + "%");
                        getAddSetChart(addAccountId, campaignId);
                    }
                } catch (ex)
                {
                    getAddSetChart(addAccountId, campaignId);
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }
            } else
            {
                getAddSetChart(addAccountId, campaignId);
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAddSetChart(addAccountId, campaignId)
{
    var url = site_url + 'addset/getAddSetChart/';
    ajaxloder('.age-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".age-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.age-div', 'hide');
            if (html)
            {
                try
                {
                    generateAgeChart(html, addAccountId, campaignId);
                } catch (ex)
                {
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAddSetCountryChart(addAccountId, campaignId) {
    var url = site_url + 'addset/getCampaignCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".country-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.country-div', 'hide');
            if (html)
            {
                try
                {
                    generateCountryChart(html, addAccountId, campaignId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAddSetTimeChart(addAccountId, campaignId) {
    var url = site_url + 'addset/getCampaignTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".time-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.time-div', 'hide');
            if (html)
            {
                try
                {
                    generateTimeChart(html, addAccountId, campaignId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdddsFeed()
{
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var page = $("#pageName").val();

    if (page == "adddsReport")
    {
        var url = site_url + 'addds/getAdddsFeeds/';
        ajaxloder('.desktop-mobile1', 'show');
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        campaignId: campaignId,
                        addAccountId: addAccountId,
                        addsetId: addsetId,
                        ajax: 1,
                        limit: $("#dayLimit").val()
                    },
            beforeSend: function ()
            {
                $(".desktop-mobile1 .alert").remove();
            },
            success: function (html)
            {
                ajaxloder('.desktop-mobile1', 'hide');
                if (html)
                {
                    try
                    {
                        var json = $.parseJSON(html);
                        if (json.code == 100)
                        {
                            //$(".slideUp").css("width", json.desktop + "%");
                            $(".desktop-value").html(json.desktop + "%");
                            $(".mobile-value").html(json.mobile + "%");
                            $(".progress-bar-info").css("width", json.desktop + "%");
                            getAdddsGender(addAccountId, campaignId, addsetId);
                        }
                    } catch (ex)
                    {
                        getAdddsGender(addAccountId, campaignId, addsetId);
                        $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                    }
                } else
                {
                    getAdddsGender(addAccountId, campaignId, addsetId);
                    $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            },
            async: true
        });
    }
}

function getAdddsGender(addAccountId, campaignId, addsetId)
{
    var url = site_url + 'addds/getAdddsGendrs/';
    ajaxloder('.male-female-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".male-female-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    if (json.code == 100)
                    {
                        $(".male .male-value").html(json.male + "%");
                        $(".female .female-value").html(json.female + "%");
                        getAdddsChart(addAccountId, campaignId, addsetId);
                    }
                } catch (ex)
                {
                    getAdddsChart(addAccountId, campaignId, addsetId);
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }
            } else
            {
                getAdddsChart(addAccountId, campaignId, addsetId);
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdddsChart(addAccountId, campaignId, addsetId)
{

    var url = site_url + 'addds/getAdddsChart/';
    ajaxloder('.age-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".age-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.age-div', 'hide');
            if (html)
            {
                try
                {
                    generateAgeChart(html, addAccountId, campaignId, addsetId);
                } catch (ex)
                {
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdddsCountryChart(addAccountId, campaignId, addsetId) {
    var url = site_url + 'addds/getCampaignCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".country-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.country-div', 'hide');
            if (html)
            {
                try
                {
                    generateCountryChart(html, addAccountId, campaignId, addsetId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdddsTimeChart(addAccountId, campaignId, addsetId) {
    var url = site_url + 'addds/getCampaignTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".time-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.time-div', 'hide');
            if (html)
            {
                try
                {
                    generateTimeChart(html, addAccountId, campaignId, addsetId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdFeed()
{
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var page = $("#pageName").val();

    if (page == "adReport")
    {
        var url = site_url + 'adone/getAddFeeds/';
        ajaxloder('.desktop-mobile1', 'show');
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        campaignId: campaignId,
                        addAccountId: addAccountId,
                        addsetId: addsetId,
                        addId: addId,
                        ajax: 1,
                        limit: $("#dayLimit").val()
                    },
            beforeSend: function ()
            {
                $(".desktop-mobile1 .alert").remove();
            },
            success: function (html)
            {
                ajaxloder('.desktop-mobile1', 'hide');
                if (html)
                {
                    try
                    {
                        var json = $.parseJSON(html);
                        if (json.code == 100)
                        {
                            //$(".slideUp").css("width", json.desktop + "%");
                            $(".desktop-value").html(json.desktop + "%");
                            $(".mobile-value").html(json.mobile + "%");
                            $(".progress-bar-info").css("width", json.desktop + "%");
                            getAdGender(addAccountId, campaignId, addsetId, addId);
                        }
                    } catch (ex)
                    {
                        getAdGender(addAccountId, campaignId, addsetId, addId);
                        $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                    }
                } else
                {
                    getAdGender(addAccountId, campaignId, addsetId, addId);
                    $(".desktop-mobile .cntntBoxs").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            },
            async: true
        });
    }
}

function getAdGender(addAccountId, campaignId, addsetId, addId)
{
    var url = site_url + 'adone/getAddGendrs/';
    ajaxloder('.male-female-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    addId: addId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".male-female-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    if (json.code == 100)
                    {
                        $(".male .male-value").html(json.male + "%");
                        $(".female .female-value").html(json.female + "%");
                        getAdChart(addAccountId, campaignId, addsetId, addId);
                    }
                } catch (ex)
                {
                    getAdChart(addAccountId, campaignId, addsetId, addId);
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }
            } else
            {
                getAdChart(addAccountId, campaignId, addsetId, addId);
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdChart(addAccountId, campaignId, addsetId, addId)
{
    var url = site_url + 'adone/getAddChart/';
    ajaxloder('.age-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    addId: addId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".age-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.age-div', 'hide');
            if (html)
            {
                try
                {
                    generateAgeChart(html, addAccountId, campaignId, addsetId, addId);
                } catch (ex)
                {
                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdCountryChart(addAccountId, campaignId, addsetId, addId) {
    var url = site_url + 'adone/getCampaignCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    addId: addId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".country-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.country-div', 'hide');
            if (html)
            {
                try
                {
                    generateCountryChart(html, addAccountId, campaignId, addsetId, addId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

function getAdTimeChart(addAccountId, campaignId, addsetId, addId) {
    var url = site_url + 'adone/getCampaignTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:
                {
                    campaignId: campaignId,
                    addAccountId: addAccountId,
                    addsetId: addsetId,
                    addId: addId,
                    ajax: 1,
                    limit: $("#dayLimit").val()
                },
        beforeSend: function ()
        {
            $(".time-div .alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.time-div', 'hide');
            if (html)
            {
                try
                {
                    generateTimeChart(html, addAccountId, campaignId, addsetId, addId);
                } catch (ex)
                {

                    $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
                }

            } else
            {
                $(".male-female .cntntBoxs ").append('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });
}

$(document).ready(function ()
{
    getCampaignFeed();
    getAddSetFeed();
    getAdddsFeed();
    getAdFeed();
});

function generateAgeChart(resData, addAccountId, campaignId, addsetId, addId)
{
    google.load("visualization", "1", {packages: ["corechart"], callback: drawVisualization});
    function drawVisualization()
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Age');
        data.addColumn('number', 'Female');
        data.addColumn('number', 'Male');
        data.addColumn('number', 'Unknown');
        $.each(resData, function (i, resData1)
        {
            var value = resData1.value1;
            var value1 = resData1.value2;
            var value2 = resData1.value3;
            var name = resData1.name1;
            data.addRows([[name, value, value1, value2]]);
        });

        var options = {
            vAxis: {title: 'Impression'},
            width: '100%',
            height: '100%',
            seriesType: 'bars',
            colors: ['#ff625f', '#3fc6f3', '#42dd91'],
            series: {7: {type: 'line'}},
            bar: {groupWidth: "30%"},
            legend: {position: 'bottom'},
            chartArea: {'width': '90%'},
        };

        var chart;
        chart = new google.visualization.ComboChart(document.getElementById('age'));
        chart.draw(data, options);
    }
    var page = $("#pageName").val();
    if (page == "addSetReport") {
        getAddSetCountryChart(addAccountId, campaignId);
    } else if (page == "adddsReport") {
        getAdddsCountryChart(addAccountId, campaignId, addsetId);
    } else if (page == "adReport") {
        getAdCountryChart(addAccountId, campaignId, addsetId, addId);
    } else {
        getCampaignCountryChart(addAccountId);
    }
}

function generateCountryChart(resData, addAccountId, campaignId, addsetId, addId)
{
    google.load("visualization", "1", {packages: ["corechart"], callback: drawVisualization});
    function drawVisualization()
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'City');
        data.addColumn('number', 'Impression');
        $.each(resData, function (i, resData1)
        {
            var value = resData1.value1;
            var name = resData1.name1;
            data.addRows([[name, value]]);
        });

        var options = {
            xAxis: {title: 'Impression'},
            pieHole: 0.85,
            width: '100%',
            height: '100%',
            legend: {position: 'right'},
            chartArea: {'width': '100%'},
            colors: ['#3fc6f3', '#ff625f', '#42dd8f']
        };

        var chart;
        chart = new google.visualization.PieChart(document.getElementById('country'));
        chart.draw(data, options);
    }
    var page = $("#pageName").val();
    if (page == "addSetReport") {
        getAddSetTimeChart(addAccountId, campaignId);
    } else if (page == "adddsReport") {
        getAdddsTimeChart(addAccountId, campaignId, addsetId);
    } else if (page == "adReport") {
        getAdTimeChart(addAccountId, campaignId, addsetId, addId);
    } else {
        getCampaignTimeChart(addAccountId);
    }
}

function generateTimeChart(resData, addAccountId, campaignId, addsetId, addId)
{
    google.load("visualization", "1", {packages: ["corechart"], callback: drawVisualization});
    function drawVisualization()
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Time');
        data.addColumn('number', 'Impression');
        data.addColumn({type: 'string', role: 'style'});
        //data.addColumn('string', '{"role": "style}'); 

        var colors = ['#42dd8f', '#465866', '#ff625f', '#3fc6f3', '#42dd8f', '#465866'];
        var i = 0;
        data.addRows(6);
        $.each(resData, function (i, resData1)
        {
            var value = resData1.value1;
            var value1 = resData1.value2;
            var name = resData1.name1;
            //data.addRows([[name, value, '#42dd8f']]);
            data.setValue(i, 0, name);
            data.setValue(i, 1, value);
            data.setValue(i, 2, colors[i]);
            i++;
        });

        var options = {
            vAxis: {title: 'Impression'},
            seriesType: 'bars',
            series: {6: {type: 'line'}},
            bar: {groupWidth: "30%"},
            legend: {position: 'none'},
            isStacked: false,
            chartArea: {'width': '80%'},
        };

        var chart;
        chart = new google.visualization.ComboChart(document.getElementById('time'));
        chart.draw(data, options);
    }
}

function labelFormatter(label, series)
{
    //return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
    return "<div class='col-lg-8 percents'><p>AGE GROUPS FOLLOWING YOU</p><ul class='percent'>" +
            +"<li><span class='colr1'>" + label + series + "%</span></li>" +
            +"</ul>" +
            +"</div>";
}

function toolStartStop(action, type1, Id)
{
    //var page = $("#pageName").val();
    var addAccountId = $("#addAccountId").val();
    ajaxloder('.page-content-wrapper', 'show');

    var type = "";
    if (type1 == "campaignreport")
    {
        //Id = $("#campaignId").val();
        type = "addset";
        var url = site_url + 'addset/toolStartStop/';
    }
    if (type1 == "addSetReport")
    {
        //Id = $("#addsetId").val();
        type = "addset";
        var url = site_url + 'addds/toolStartStop/';
    }
    if (type1 == "adddsReport")
    {
        //Id = $("#addId").val();
        type = "add";
        var url = site_url + 'adone/toolStartStop/';
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data: {
            adAccountId: addAccountId,
            Id: Id,
            type: type,
            action: action,
            ajax: 1
        },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.page-content-wrapper', 'hide');
            if (html)
            {
                window.location.reload();
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }
        },
        async: true
    });
}

$(document).ready(function () {

    $('#relationship_statusesALL').click(function () {
        $('.relationship_status input:checkbox').not(this).prop('checked', this.checked);
        getAudiencesize();
    });

    $("[id*=relationship_statuses]").change(function () {
        if ($('input[id*=relationship_statuses][type=checkbox]:checked').length == $('input[id*=relationship_statuses][type=checkbox]').length) {
            $('#relationship_statusesALL').prop('checked', true);
        } else {
            $('#relationship_statusesALL').prop('checked', false);
        }

        getAudiencesize();
    });

    $('#interested_inALL').click(function () {
        $('.interested_in input:checkbox').not(this).prop('checked', this.checked);
        getAudiencesize();
    });

    $("[id*=interested_in]").change(function () {
        if ($('input[id*=interested_in][type=checkbox]:checked').length == $('input[id*=interested_in][type=checkbox]').length) {
            $('#interested_inALL').prop('checked', true);
        } else {
            $('#interested_inALL').prop('checked', false);
        }

        getAudiencesize();
    });

});

$(document).on('click', '#box5Li, #Savebudgetbtn', function () {

    $('#adcopy_details').html('');

    var i = 1;
    $('.headlines_fields_wrap').find('textarea').each(function () {
        var valus = $('textarea#' + $(this).attr('id')).val();
        if (valus) {
            var html = '<div class="header#" id="header1">Header # ' + i + ' : </div>' +
                    '<div class="adcopy-color-block' + i + '">' + valus + '</div>' +
                    '<div class="clearfix"></div>';
            $('#adcopy_details').append(html);
            // console.log(i);
            i++;
        }
    }
    );
    var i = 1;

    $('.creatives_body_fields_wrap').find('textarea').each(function () {
        var valus = $('textarea#' + $(this).attr('id')).val();
        if (valus) {
            var html = '<div class="header#" id="header1">Body text # ' + i + ' : </div>' +
                    '<div class="adcopy-color-block' + i + '">' + valus + '</div>' +
                    '<div class="clearfix"></div>';
            $('#adcopy_details').append(html);
            //console.log(i + '==' + i);
            i++;
        }
    });
    var i = 1;
    $('.creatives_link_desc_wrap').find('textarea').each(function () {
        var valus = $('textarea#' + $(this).attr('id')).val();
        if (valus) {
            var html = '<div class="header#" id="header1">Description # ' + i + ' : </div>' +
                    '<div class="adcopy-color-block' + i + '">' + valus + '</div>' +
                    '<div class="clearfix"></div>';
            $('#adcopy_details').append(html);
            i++;
        }
    });
    var cadsetval = countAdset();
    $(".numberOfAds").html(countAds()); // Count ads
    $(".numberOfAdset").html(cadsetval); // Count adsets


    if ($('#budgetTotal').is(":checked"))
    {
        $('.budget-details-bg p').html(' $' + ($('#exampleInputAmount').val() ? $('#exampleInputAmount').val() : '0') + ' / Total');
        $('.budget-details-bg2 p').html(' $' + ($('#exampleInputAmount').val() ? $('#exampleInputAmount').val() * cadsetval : '0') + ' / Total');
        // $('.budget-details-bg3 p').html(' $' + ($('#exampleInputAmount').val() ? $('#exampleInputAmount').val() : '0') + ' / Total');
    }

    if ($('#budgetDay').is(":checked"))
    {
        $('.budget-details-bg p').html(' $' + ($('#exampleInputAmount').val() ? $('#exampleInputAmount').val() : '0') + ' / Day');
        $('.budget-details-bg2 p').html(' $' + ($('#exampleInputAmount').val() ? $('#exampleInputAmount').val() * cadsetval : '0') + ' / Day');
        //$('.budget-details-bg3 p').html(' $' + ($('#exampleInputAmount').val() ? $('#exampleInputAmount').val() : '0') + ' / Day');
    }


});

$(document).on('click', '#box4Li, #SaveAudienceId', function () {
    $("#adsetCounts").html(countAdset());
});

$(document).on('change', '#exampleInputAmount', function () {
    $("#spendamount").html($('#exampleInputAmount').val());
});

function countAds() {
    var response;
    $.ajax({
        type: "POST",
        cache: false,
        url: site_url + 'createcampaign/countAds/',
        data: $("#create_creativeads_frm").serialize(),
        success: function (result)
        {
            response = result;
        },
        async: false
    });

    return response;
}

function countAdset() {
    var response;
    $.ajax({
        type: "POST",
        cache: false,
        url: site_url + 'createcampaign/countAdset/',
        data: $("#create_creativeads_frm").serialize(),
        success: function (result)
        {
            response = result;

        },
        async: false
    });
    return response;
}
$(document).on('change', '.mobile_devices,#more_behavious,#more_demographic', function () {
    getAudiencesize();
});

function getAudiencesize()
{
    //console.log('call');
    var addAccountId = $("#adaccountid").val();
    var new_geo_locations = $("#new_geo_locations").val();
    if (addAccountId && new_geo_locations) {
        var url = site_url + 'createcampaign/get_audience_size/' + addAccountId;

        var behaviors = $('#more_behavious').data('directoryTree').selectedItems('input:checkbox[name="behaviors[]"]').join(',');

        var life_events = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="life_events[]"]').join(',');
        var politics = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="politics[]"]').join(',');
        var industries = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="industries[]"]').join(',');
        var ethnic_affinity = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="ethnic_affinity[]"]').join(',');
        var generation = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="generation[]"]').join(',');
        var household_composition = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="household_composition[]"]').join(',');
        var family_statuses = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="family_statuses[]"]').join(',');

        var data = $("#create_creativeads_frm").serialize() + '&' + $.param({'behaviors': behaviors}) + '&' + $.param({'life_events': life_events}) + '&' + $.param({'politics': politics}) + '&' + $.param({'industries': industries}) + '&' + $.param({'ethnic_affinity': ethnic_affinity}) + '&' + $.param({'generation': generation}) + '&' + $.param({'household_composition': household_composition}) + '&' + $.param({'family_statuses': family_statuses});



        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result)
            {
                if (result) {
                    $(".audience_total_size").html(result);
                } else {
                    $(".audience_total_size").html('No response');
                }
            },
            async: true
        });
    } else {
        //$(".audience_total_size").html('Choose adaccount'); No change 
    }
}

function getSuggestions(val)
{
    var url = site_url + 'createcampaign/getSuggestions/';
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data: {typeList: val},
        success: function (result)
        {
            if (result) {
                $("#suggestion_data").fadeIn(2000);
                $("#suggestion_data ul").html(result);
            } else {
                $("#suggestion_data ul").html('No response');
            }
        },
        async: true
    });
}

function scaleCampaign(adAccountId, campaignId, adSetId) {
    $("#editCampaignScale").modal("hide");
    editAdsetScale(adAccountId, campaignId, adSetId);
}
var loaderHtml = '<div class="pos_relative"><div id="preloader" class="displayNone"><div id="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>';

function editAdsetScale(adAccountId, campaignId, adsetId) {
    $("#editAdSetScale").modal("show");
    $(".adset-modal").css("height", "50");
    var url = site_url + 'editadset/adsetscale/';

    $('#editAdSetScale .modal-body').append(loaderHtml);


    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    campaignId: campaignId,
                    adsetId: adsetId,
                    ajax: 1
                },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            $(".adset-modal").css("height", "auto");
            $('#editAdSetScale .modal-content').html(html);
            if (document.getElementById("adset_start_date")) {
                $('#adset_start_date').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    startDate: new Date()

                });
            }
            if (document.getElementById("adset_end_date")) {
                $('#adset_end_date').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    startDate: new Date()
                });
            }
        },
        async: true
    });

}

function saveAdsetScale(adsetId)
{
    $(".alert").remove();
    //$('#editAdSet .modal-body').append(loaderHtml);
    var url = site_url + 'editadset/saveaddSetScale/';

    var amount = $("#InputAmount").val();
    //var start_date = $("#adset_start_date").val();
    //var end_date = $("#adset_end_date").val();
    //var budget_type1 = $("input[name=budget]:checked").val();
    //console.log(budget_type1);

    //if (amount == "" || budget_type1 == ""){
    if (amount == "") {
        //if (budget_type1 == "daily_budget" && end_date == ""){
        $('#editAdSetScale .modal-body').prepend('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Required fields are empty.</div>');
        //}
    } else
    {
        $('#editAdSetScale .modal-body').append(loaderHtml);
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
                    {
                        adsetId: adsetId,
                        //start_date: start_date,
                        amount: amount,
                        //end_date: end_date,
                        //budget_type: budget_type1,
                        ajax: 1
                    },
            beforeSend: function ()
            {
                $(".alert").remove();
            },
            success: function (html)
            {
                $(".pos_relative").remove();
                $(".adset-modal").css("height", "auto");
                try {
                    var obj = $.parseJSON(html);
                    //console.log(obj);
                    if (obj.code == 200)
                    {
                        $('#editAdSetScale .modal-body').prepend('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> ' + obj.message + '</div>');
                    } else {
                        $('#editAdSetScale .modal-body').prepend('<div class="alert alert-success"><button class="close" data-dismiss="alert"></button> Updated Successfully</div>');
                        //window.location.reload();
                    }

                } catch (ex) {
                    $('#editAdSetScale .modal-body').prepend('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> ' + ex + '</div>');
                }
            },
            async: true
        });
    }


}

function getCampaign(adAccountId, limit) {
    var url = site_url + 'reports/index/';
    ajaxloder('.page-content', 'show');
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            if (html)
            {
                getCampaignFeed();
                $(".campaigns").html(html);
                var oTable = $('#sample_1').dataTable({"oLanguage": {
                        "sLengthMenu": "_MENU_ Campaigns",
                        "sInfo": "Showing _START_ to _END_ of _TOTAL_ Campaigns",
                    }});
                oTable.fnDraw();
                var tableWrapper = $('#sample_1_wrapper');
                jQuery('.dataTables_filter input', tableWrapper).addClass("form-control input-medium input-inline"); // modify table search input
                jQuery('.dataTables_length select', tableWrapper).addClass("form-control input-small"); // modify table per page dropdown
                jQuery('.dataTables_length select', tableWrapper).select2(); // initialize select2 dropdown
                $('.lcs_check').lc_switch();
                $('#sample_1').on('draw.dt', function () {
                    $('.lcs_check').lc_switch();
                });
            } else
            {
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }

        },
        async: true
    });

}
