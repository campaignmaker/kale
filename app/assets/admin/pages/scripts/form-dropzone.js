var FormDropzone = function () {
    return {
        //main function to initiate the module
        init: function () {
            Dropzone.options.myDropzone = {
                //maxFiles: 1,
                init: function () {
                    this.on("addedfile", function (file, responseText) {
                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;
                        // Listen to the click event
                        removeButton.addEventListener("click", function (e) {
                            // Make sure the button click doesn't submit the form:
                            console.log(e);
                            console.log(file.name);
                            console.log(responseText);
                            e.preventDefault();
                            e.stopPropagation();
                            var fname = file.name;
                            var fnameArr = fname.split('.');
                            var fname1 = fnameArr[0]+fnameArr[1]
                            jQuery('.' + fname1).remove();
                            stickyCounter('', '');
                            // Remove the file preview.
                            _this.removeFile(file);
                            
                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.
                        });
                        

                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });
                    
                    this.on("maxfilesexceeded", function(file) { this.removeFile(file); });
                },
                accept: function (file, done) {
                    var re = /(?:\.([^.]+))?$/;
                    var ext = re.exec(file.name)[1];
                    ext = ext.toUpperCase();
                    if (ext == "JPG" || ext == "JPEG" || ext == "PNG" || ext == "GIF" || ext == "BMP" || ext == "3GP" || ext == "AVI" || ext == "DAT" || ext == "F4V" || ext == "FLV" || ext == "M4V" || ext == "MKV" || ext == "MOV" || ext == "MP4" || ext == "WMV" || ext == "TOD" || ext == "QT" || ext == "VOB" || ext == "MPEG" || ext == "MPEG4")
                    {
                        done();
                    } else {
                        done("Please select only supported picture files.");
                    }
                },
                success: function (file, response) {
                    console.log(file);
                    obj = JSON.parse(response);
                    if(obj.p == 1){
                        var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="product1_img_url" id="product1_img_url">';
                        str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages-product').append(str);
                    }
                    else if(obj.p == 2){
                        var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="product2_img_url" id="product2_img_url">';
                        str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages-product').append(str);
                    }
                    else if(obj.p == 3){
                        var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="product3_img_url" id="product3_img_url">';
                        str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages-product').append(str);
                    }
                    else{
                        var str = '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="creatives[viewUrl][]" id="viewUrl1">';
                        str += '<input class="'+obj.oldfname+'" type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages').append(str);
                    }
                    stickyCounter(obj.filename, 'img');
                    LoeadPreview();
                    jQuery('.dz-message').css('opacity', 1);
                }
            }
        }
    };
}();

var FormDropzone1 = function () {
    return {
        //main function to initiate the module
        init: function () {
            Dropzone.options.myDropzone1 = {
                maxFiles: 1,
                init: function () {
                    this.on("addedfile", function (file, responseText) {
                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;
                        // Listen to the click event
                        removeButton.addEventListener("click", function (e) {
                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            e.stopPropagation();
                            stickyCounter();
                            // Remove the file preview.
                            _this.removeFile(file);
                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.
                        });

                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });
                    
                    this.on("maxfilesexceeded", function(file) { this.removeFile(file); });
                },
                accept: function (file, done) {
                    var re = /(?:\.([^.]+))?$/;
                    var ext = re.exec(file.name)[1];
                    ext = ext.toUpperCase();
                    if (ext == "JPG" || ext == "JPEG" || ext == "PNG" || ext == "GIF" || ext == "BMP" || ext == "3GP" || ext == "AVI" || ext == "DAT" || ext == "F4V" || ext == "FLV" || ext == "M4V" || ext == "MKV" || ext == "MOV" || ext == "MP4" || ext == "WMV" || ext == "TOD" || ext == "QT" || ext == "VOB" || ext == "MPEG" || ext == "MPEG4")
                    {
                        done();
                    } else {
                        done("Please select only supported picture files.");
                    }
                },
                success: function (file, response) {
                    console.log(file);
                    obj = JSON.parse(response);
                    if(obj.p == 1){
                        var str = '<input type="hidden" value="' + site_url + 'uplaods/campaign/'+obj.userId+'/' + obj.filename + '" name="product1_img_url" id="product1_img_url">';
                        str += '<input type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages-product').append(str);
                    }
                    else if(obj.p == 2){
                        var str = '<input type="hidden" value="' + site_url + 'uplaods/campaign/'+obj.userId+'/' + obj.filename + '" name="product2_img_url" id="product2_img_url">';
                        str += '<input type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages-product').append(str);
                    }
                    else if(obj.p == 3){
                        var str = '<input type="hidden" value="' + site_url + 'uplaods/campaign/'+obj.userId+'/' + obj.filename + '" name="product3_img_url" id="product3_img_url">';
                        str += '<input type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages-product').append(str);
                    }
                    else{
                        var str = '<input type="hidden" value="' + site_url + 'uplaods/campaign/'+obj.userId+'/' + obj.filename + '" name="creatives[viewUrl][]" id="viewUrl1">';
                        str += '<input type="hidden" value="' + site_url + 'uploads/campaign/'+obj.userId+'/' + obj.filename + '" name="fbimghash[]">';

                        jQuery('#uploadedImages').append(str);
                    }
                    $('#totalAdsCount1').val('4');
                    LoeadPreview();
                    jQuery('.dz-message').css('opacity', 1);
                }
            }
        }
    };
}();