jQuery(document).ready(function (){
    //getCampaignChart();
    //getCampaignClicksData();
    //getAddSetClicksData();
    //getAddsClicksData();
    //getAdoneClicksData();
});

function getCampaignClicksData(){
    var page = $("#pageName").val();
    if (page == "campaignsReport"){
        var addAccountId = $("#addAccountId").val();
        var url = site_url + 'reports/clickData/';
        ajaxloder('.click-div', 'show');
        jQuery.ajax({
            type: "POST",
            cache: false,
            dataType: "json",
            url: url,
            data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
            beforeSend: function (){
                $(".click-div .alert").remove();
            },
            success: function (html){
                console.log(html);
                ajaxloder('.click-div', 'hide');
                if (html){
                    try{
                        console.log(JSON.stringify(html));
                        new Morris.Line({
                            // ID of the element in which to draw the chart.
                            element: 'click-data-chart',
                            // Chart data records -- each entry in this array corresponds to a point on
                            // the chart.
                            data: eval(JSON.stringify(html)),
                            // The name of the data record attribute that contains x-values.
                            xkey: 'year',
                            // A list of names of data record attributes that contain y-values.
                            ykeys: ['value'],
                            // Labels for the ykeys -- will be displayed when you hover over the
                            // chart.
                            labels: ['Total Clicks'],
                            lineColors: ['#26c1c9'],
                            pointFillColors: ['#26c1c9'],
                            pointStrokeColors: ['#26c1c9'],
                            resize: true,
                            axes: false,
                            hideHover: true
                        });
                        getCampaignSpentData();
                    } 
                    catch (ex){
                        
                    }

                }
                else{
                    
                }

            },
            async: true
        });
    }
}

function getCampaignSpentData(){
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'reports/getCampaignSpentData/';
    ajaxloder('.spent-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".spent-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.spent-div', 'hide');
            console.log(html);
            if (html){
                try{   
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'spent-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Spent'],
                        lineColors: ['#ab7df6'],
                        pointFillColors: ['#ab7df6'],
                        pointStrokeColors: ['#ab7df6'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });       
                    getCampaignEngagementData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getCampaignEngagementData(){
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'reports/engagementData/';
    ajaxloder('.engagement-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".engagement-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.engagement-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'engagement-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Engagement'],
                        lineColors: ['#0079c4'],
                        pointFillColors: ['#0079c4'],
                        pointStrokeColors: ['#0079c4'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getCampaignPageLikeData();
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getCampaignPageLikeData(){
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'reports/pageLikeData/';
    ajaxloder('.pagelike-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".pagelike-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.pagelike-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'page-link-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'y',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['a'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Page Like'],
                        lineColors: ['#ff7b00'],
                        pointFillColors: ['#ff7b00'],
                        pointStrokeColors: ['#ff7b00'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getCampaignImpressionData();
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getCampaignImpressionData(){
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'reports/getCampaignImpressionData/';
    ajaxloder('.impression-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".impression-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.impression-div', 'hide');
            console.log(html);
            if (html){
                try{ 
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'impressions-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Impression'],
                        lineColors: ['#4990e2'],
                        pointFillColors: ['#4990e2'],
                        pointStrokeColors: ['#4990e2'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });   
                    getCampaignCtrData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getCampaignCtrData(){
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'reports/getCampaignCtrData/';
    ajaxloder('.ctr-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".ctr-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.ctr-div', 'hide');
            console.log(html);
            if (html){
                try{       
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'click-rate-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['CTR'],
                        lineColors: ['#faca00'],
                        pointFillColors: ['#faca00'],
                        pointStrokeColors: ['#faca00'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getCampaignConversionsData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getCampaignConversionsData(addAccountId){
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'reports/conversionData/';
    ajaxloder('.conversion-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".conversion-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.conversion-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'conversions-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Conversions'],
                        lineColors: ['#4ece3d'],
                        pointFillColors: ['#4ece3d'],
                        pointStrokeColors: ['#4ece3d'],
                        gridTextColor: ['#000'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getCampaignPlacementData();
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getCampaignPlacementData(){
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'reports/getCampaignFeeds/';
    ajaxloder('.placement-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1},
        beforeSend: function (){
            $(".placement-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.placement-div', 'hide');
            if (html){
                try{
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        jQuery('#desktop').html(json.desktop);
                        jQuery('#mobile').html(json.mobile);
                        jQuery('#right_hand').html(json.right_hand);
                        jQuery('#mobile_external_only').html(json.mobile_external_only);
                        jQuery('#instant_article').html(json.instant_article);

                        jQuery('#desktopPer').html(json.desktopPer);
                        jQuery('#mobilePer').html(json.mobilePer);
                        jQuery('#right_handPer').html(json.right_handPer);
                        jQuery('#mobile_external_onlyPer').html(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer').html(json.instant_articlePer);

                        jQuery('#desktopPer-1').width(json.desktopPer);
                        jQuery('#mobilePer-1').width(json.mobilePer);
                        jQuery('#right_handPer-1').width(json.right_handPer);
                        jQuery('#mobile_external_onlyPer-1').width(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer-1').width(json.instant_articlePer);
                        
                        getCampaignCountryChart();
                    }
                } catch (ex){
                    console.log(ex);
                    
                }

            } else
            {
                
            }

        },
        async: true
    });
}

function getCampaignCountryChart() {
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'reports/getCampaignCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".country-div .alert").remove();
        },
        success: function (html){
            console.log("locations"+html);
            ajaxloder('.country-div', 'hide');
            if (html){
                try{
                    Morris.Donut({
                        element: 'data-locations',
                        data: eval(JSON.stringify(html)),
                        colors: ['#ab7df6', '#faca00', '#81c926', '#26c2c9'],
                        resize: true
                    });
                    getCampaignChart();
                } 
                catch (ex){
                    
                }

            } else
            {
                
            }

        },
        async: true
    });
}

function GetBreakdown(el){
     var element = $(el);
     var loadingtest = element.attr('data-loading-text');
    
     element.html(loadingtest);
     
    var pagename = $(el).parent().parent().find(".ParamDD").val();
    getCampaignGendrsvarious(pagename,el);
    getCampaignChartvarious(pagename,el);
    getCampaignPlacementDatavarious(pagename,el);
    
    
    return false;
}

function GetBreakdownAdset(el){
    var pagename = $(el).parent().parent().find(".ParamDD").val();
    getAddSetGendervarious(pagename,el);
    getAddSetChartvarious(pagename,el);
    getAddSetPlacementDatavarious(pagename,el);
    return false;
}

function GetBreakdownAdds(el){
    var pagename = $(el).parent().parent().find(".ParamDD").val();
    getAddsGendervarious(pagename,el);
    getAddsChartvarious(pagename,el);
    getAddsPlacementDatavarious(pagename,el);
    return false;
}

function GetBreakdownAdone(el){
    var pagename = $(el).parent().parent().find(".ParamDD").val();
    getAdoneGendervarious(pagename,el);
    getAdoneChartvarious(pagename,el);
    getAdonePlacementDatavarious(pagename,el);
    return false;
}

function getDetailData(el){
    
    // alert($(el).val());
    // alert($('#hid'+$(el).val()).val());
    var actualstring = $(el).val();
    var actualvalue = $('#hid'+actualstring).val();
    
     
     if (typeof actualvalue != 'undefined'){
     
     }else{
         alert("Data not available for selected datapoints");
         
         $(el).prop('selectedIndex',0);
         return false;
     }
    var tempidstring = getlabel(actualstring);
    
    $(el).parent().parent().parent().find(".firstval").html($('#hid'+tempidstring).val());
    $(el).parent().parent().parent().find(".secondval").html($('#hid2'+tempidstring).val());
    $(el).parent().parent().parent().find(".thirdval").html($('#hid3'+tempidstring).val());
    
}


function getlabel(stringlabel){
  
        if (stringlabel.indexOf('.') > -1){
           if (stringlabel.indexOf('fb_pixel_') > -1){
                 temparray =  stringlabel.split("fb_pixel_");
          }else{
               temparray =  stringlabel.split("messaging_");
          }
           var templabel =  temparray[1];
          
        }else{
           var templabel =  stringlabel;
        }
  return   templabel;
}


function addnewanalytics(el){
    $('.data-row:last').clone().insertAfter(".data-row:last").openClose({
        activeClass: 'active',
        opener: '.opener',
        slider: '.slide',
        animSpeed: 400,
        hideOnClickOutside: true,
        effect: 'slide'
    });
    
    return false
}
function removenewanalytics(el){
    if($('.data-row').length > 2){
        $(el).parent().parent().parent().remove();
    }
    return false;
}

function getCampaignCountryChartvarious(pagename) {
	//alert(pagename);
    var addAccountId = $("#addAccountId").val();
    var pageName1 = pagename;//$("#pageName1").val();
    var url = site_url + 'reports/getCampaignCountryChartvarious/';
    
    ajaxloder('.country-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".country-divvar .alert").remove();
        },
        success: function (html){
            console.log("locations"+html);
            ajaxloder('.country-divvar', 'hide');
			if(html[0] == "0"){
				$("#locationmain").html("<li>"+html[0]+"</li>");
			}
			else{
				$("#locationmain").html("<li>"+html[0]+"</li>"+html[1]);
			}
			//alert(JSON.stringify(html));
           /* if (html){
                try{
                    Morris.Donut({
                        element: 'data-locations',
                        data: eval(JSON.stringify(html)),
                        colors: ['#ab7df6', '#faca00', '#81c926', '#26c2c9'],
                        resize: true
                    });
                    getCampaignChart();
                } 
                catch (ex){
                    
                }

            } else
            {
                
            }*/

        },
        async: true
    });
	return false;
}

function getAddSetCountryChartvarious(pagename) {
    //alert(pagename);
    var campaignId = $("#campaignId").val();
    var pageName1 = pagename;//$("#pageName1").val();
    var url = site_url + 'addset/getAddSetCountryChartvarious/';
    
    ajaxloder('.country-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate: $("#clickrate").text()},
        beforeSend: function (){
            //$(".country-divvar .alert").remove();
        },
        success: function (html){
            console.log("locations"+html);
            ajaxloder('.country-divvar', 'hide');
            if(html[0] == "0"){
                $("#locationmain").html("<li>"+html[0]+"</li>");
            }
            else{
                $("#locationmain").html("<li>"+html[0]+"</li>"+html[1]);
            }
            //alert(JSON.stringify(html));
           /* if (html){
                try{
                    Morris.Donut({
                        element: 'data-locations',
                        data: eval(JSON.stringify(html)),
                        colors: ['#ab7df6', '#faca00', '#81c926', '#26c2c9'],
                        resize: true
                    });
                    getCampaignChart();
                } 
                catch (ex){
                    
                }

            } else
            {
                
            }*/

        },
        async: true
    });
    return false;
}

function getAddSetGendervarious(pagename,evall){
    var campaignId = $("#campaignId").val();
    var pageName1 = pagename;
    var url = site_url + 'addset/getAddSetGendervarious/';
    //ajaxloder('.gender-divvar', 'show');
    
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".male-female-div .alert").remove();
        },
        success: function (html){
            console.log("age"+html);
           //alert(html);
           // ajaxloder('.gender-divvar', 'hide');
			var obj = JSON.parse(html); 
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".genderbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".genderbody").html(obj[1]);
			}
            /*if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        Morris.Donut({
                            element: 'data-gender',
                            data: [
                              {label: "MEN", value: json.male},
                              {label: "WOMEN", value: json.female},
                              {label: "Other", value: json.other}
                            ],
                            colors: ['#ab7df6', '#26c2c9','#faca00'],
                            resize: true
                        });
                    }
                } catch (ex)
                {
                    
                }
            } else
            {
                
            }*/

        },
        async: true
    });
	return false;
}

function getAddSetChartvarious(pagename,evall){
    var campaignId = $("#campaignId").val();
    var pageName1 = pagename;
    var url = site_url + 'addset/getAddSetChartvarious/';
    //ajaxloder('.agegroup-divvar', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".age-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            //ajaxloder('.agegroup-divvar', 'hide');
            var obj = html;//JSON.parse(html); 
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".agegroupbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".agegroupbody").html(obj[1]);
			}
			/*if (html){
                try{
                    Morris.Bar({
                        element: 'data-age-groups',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['value'],
                        labels: ['Age G'],
                        barColors:['#26c2c9', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                    });
                    getCampaignTimeChart();
                } 
                catch (ex){
                    
                }

            }
            else{
                
            }*/

        },
        async: true
    });
	return false;
}

function getAddsChartvarious(pagename,evall){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = pagename;
    var url = site_url + 'addds/getAddsChartvarious/';
    //ajaxloder('.agegroup-divvar', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".age-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            //ajaxloder('.agegroup-divvar', 'hide');
            var obj = html;//JSON.parse(html); 
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".agegroupbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".agegroupbody").html(obj[1]);
			}
            /*if (html){
                try{
                    Morris.Bar({
                        element: 'data-age-groups',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['value'],
                        labels: ['Age G'],
                        barColors:['#26c2c9', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                    });
                    getCampaignTimeChart();
                } 
                catch (ex){
                    
                }

            }
            else{
                
            }*/

        },
        async: true
    });
    return false;
}

function getAdoneChartvarious(pagename,evall){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdoneChartvarious/';
    var pageName1 = pagename;
    
    //ajaxloder('.agegroup-divvar', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId,addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1, clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".age-div .alert").remove();
        },
        success: function (html){
            var obj = html.toString().split("<span>");
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".agegroupbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".agegroupbody").html(obj[1]);
			}
            
          
            /*if (html){
                try{
                    Morris.Bar({
                        element: 'data-age-groups',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['value'],
                        labels: ['Age G'],
                        barColors:['#26c2c9', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                    });
                    getCampaignTimeChart();
                } 
                catch (ex){
                    
                }

            }
            else{
                
            }*/

        },
        async: true
    });
    return false;
}

function getAdonePlacementDatavarious(pagename,evall){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdonePlacementDatavarious/';
    var pageName1 = pagename;
    //ajaxloder('.placement-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId,addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1, clickrate:$("#clickrate").text()},
        beforeSend: function (){
            //$(".placement-div .alert").remove();
        },
        success: function (html){
			var obj = JSON.parse(html);
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".placementbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".placementbody").html(obj[1]);
			}
          
          /*  if (html){
                try{
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        jQuery('#desktop').html(json.desktop);
                        jQuery('#mobile').html(json.mobile);
                        jQuery('#right_hand').html(json.right_hand);
                        jQuery('#mobile_external_only').html(json.mobile_external_only);
                        jQuery('#instant_article').html(json.instant_article);

                        jQuery('#desktopPer').html(json.desktopPer);
                        jQuery('#mobilePer').html(json.mobilePer);
                        jQuery('#right_handPer').html(json.right_handPer);
                        jQuery('#mobile_external_onlyPer').html(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer').html(json.instant_articlePer);

                        jQuery('#desktopPer-1').width(json.desktopPer);
                        jQuery('#mobilePer-1').width(json.mobilePer);
                        jQuery('#right_handPer-1').width(json.right_handPer);
                        jQuery('#mobile_external_onlyPer-1').width(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer-1').width(json.instant_articlePer);
                        
                        getCampaignCountryChart();
                    }
                } catch (ex){
                    console.log(ex);
                    
                }

            } else
            {
                
            }*/

        },
        async: true
    });
    return true;
}
function getAddSetPlacementDatavarious(pagename,evall){
    var campaignId = $("#campaignId").val();
    var pageName1 = pagename;
    var url = site_url + 'addset/getAddSetPlacementDatavarious/';
    //ajaxloder('.placement-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text()},
        beforeSend: function (){
            //$(".placement-div .alert").remove();
        },
        success: function (html){
            //ajaxloder('.placement-divvar', 'hide');
			var obj = JSON.parse(html);
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".placementbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".placementbody").html(obj[1]);
			}
          /*  if (html){
                try{
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        jQuery('#desktop').html(json.desktop);
                        jQuery('#mobile').html(json.mobile);
                        jQuery('#right_hand').html(json.right_hand);
                        jQuery('#mobile_external_only').html(json.mobile_external_only);
                        jQuery('#instant_article').html(json.instant_article);

                        jQuery('#desktopPer').html(json.desktopPer);
                        jQuery('#mobilePer').html(json.mobilePer);
                        jQuery('#right_handPer').html(json.right_handPer);
                        jQuery('#mobile_external_onlyPer').html(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer').html(json.instant_articlePer);

                        jQuery('#desktopPer-1').width(json.desktopPer);
                        jQuery('#mobilePer-1').width(json.mobilePer);
                        jQuery('#right_handPer-1').width(json.right_handPer);
                        jQuery('#mobile_external_onlyPer-1').width(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer-1').width(json.instant_articlePer);
                        
                        getCampaignCountryChart();
                    }
                } catch (ex){
                    console.log(ex);
                    
                }

            } else
            {
                
            }*/

        },
        async: true
    });
	return true;
}

function getAddsPlacementDatavarious(pagename,evall){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = pagename;
    var url = site_url + 'addds/getAddsPlacementDatavarious/';
    //ajaxloder('.placement-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text()},
        beforeSend: function (){
            //$(".placement-div .alert").remove();
        },
        success: function (html){
            var obj = JSON.parse(html);
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".placementbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".placementbody").html(obj[1]);
			}
          /*  if (html){
                try{
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        jQuery('#desktop').html(json.desktop);
                        jQuery('#mobile').html(json.mobile);
                        jQuery('#right_hand').html(json.right_hand);
                        jQuery('#mobile_external_only').html(json.mobile_external_only);
                        jQuery('#instant_article').html(json.instant_article);

                        jQuery('#desktopPer').html(json.desktopPer);
                        jQuery('#mobilePer').html(json.mobilePer);
                        jQuery('#right_handPer').html(json.right_handPer);
                        jQuery('#mobile_external_onlyPer').html(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer').html(json.instant_articlePer);

                        jQuery('#desktopPer-1').width(json.desktopPer);
                        jQuery('#mobilePer-1').width(json.mobilePer);
                        jQuery('#right_handPer-1').width(json.right_handPer);
                        jQuery('#mobile_external_onlyPer-1').width(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer-1').width(json.instant_articlePer);
                        
                        getCampaignCountryChart();
                    }
                } catch (ex){
                    console.log(ex);
                    
                }

            } else
            {
                
            }*/

        },
        async: true
    });
    return true;
}

function getCampaignPlacementDatavarious(pagename,evall){
    var addAccountId = $("#addAccountId").val();
    var pageName1 = pagename;
    var url = site_url + 'reports/getCampaignFeedsvarious/';
    //ajaxloder('.placement-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1, clickrate:$("#clickrate").text()},
        beforeSend: function (){
            //$(".placement-div .alert").remove();
        },
        success: function (html){
            //ajaxloder('.placement-divvar', 'hide');
             $(evall).html('Un-Break');
			var obj = JSON.parse(html);
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".placementbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".placementbody").html(obj[1]);
			}
			
			
          /*  if (html){
                try{
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        jQuery('#desktop').html(json.desktop);
                        jQuery('#mobile').html(json.mobile);
                        jQuery('#right_hand').html(json.right_hand);
                        jQuery('#mobile_external_only').html(json.mobile_external_only);
                        jQuery('#instant_article').html(json.instant_article);

                        jQuery('#desktopPer').html(json.desktopPer);
                        jQuery('#mobilePer').html(json.mobilePer);
                        jQuery('#right_handPer').html(json.right_handPer);
                        jQuery('#mobile_external_onlyPer').html(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer').html(json.instant_articlePer);

                        jQuery('#desktopPer-1').width(json.desktopPer);
                        jQuery('#mobilePer-1').width(json.mobilePer);
                        jQuery('#right_handPer-1').width(json.right_handPer);
                        jQuery('#mobile_external_onlyPer-1').width(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer-1').width(json.instant_articlePer);
                        
                        getCampaignCountryChart();
                    }
                } catch (ex){
                    console.log(ex);
                    
                }

            } else
            {
                
            }*/

        },
        async: true
    });
	return true;
}

function getCampaignGendrsvarious(pagename,evall){
    var addAccountId = $("#addAccountId").val();
    var pageName1 = pagename;
    var url = site_url + 'reports/getCampaignGendrsvarious/';
    //ajaxloder('.gender-divvar', 'show');
    
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".male-female-div .alert").remove();
        },
        success: function (html){
            
            console.log("age"+html);
           //alert(html);
           // ajaxloder('.gender-divvar', 'hide');
			var obj = JSON.parse(html); 
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".genderbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".genderbody").html(obj[1]);
			}
            /*if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        Morris.Donut({
                            element: 'data-gender',
                            data: [
                              {label: "MEN", value: json.male},
                              {label: "WOMEN", value: json.female},
                              {label: "Other", value: json.other}
                            ],
                            colors: ['#ab7df6', '#26c2c9','#faca00'],
                            resize: true
                        });
                    }
                } catch (ex)
                {
                    
                }
            } else
            {
                
            }*/

        },
        async: true
    });
	return false;
}

function getCampaignChartvarious(pagename,evall){
    var addAccountId = $("#addAccountId").val();
    var pageName1 = pagename;
    var url = site_url + 'reports/getCampaignChartvarious/';
    //ajaxloder('.agegroup-divvar', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1, clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".age-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            //ajaxloder('.agegroup-divvar', 'hide');
            var obj = html;//JSON.parse(html); 
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".agegroupbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".agegroupbody").html(obj[1]);
			}
			/*if (html){
                try{
                    Morris.Bar({
                        element: 'data-age-groups',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['value'],
                        labels: ['Age G'],
                        barColors:['#26c2c9', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                    });
                    getCampaignTimeChart();
                } 
                catch (ex){
                    
                }

            }
            else{
                
            }*/

        },
        async: true
    });
	return false;
}


function getCampaignChart(){
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'reports/getCampaignChart/';
    ajaxloder('.age-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".age-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.age-div', 'hide');
            if (html){
                try{
                    Morris.Bar({
                        element: 'data-age-groups',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['value'],
                        labels: ['Age G'],
                        barColors:['#26c2c9', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                    });
                    getCampaignTimeChart();
                } 
                catch (ex){
                    
                }

            }
            else{
                
            }

        },
        async: true
    });
}

function getCampaignTimeChart() {
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'reports/getCampaignTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".time-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.time-div', 'hide');
            if (html){
                try{
                    Morris.Bar({
                        element: 'data-time-bar',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['Impressions'],
                        // yLabelFormat: function(x) { return ''; },
                        labels: ['Time'],
                        barColors:['#ab7df6', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                  });
                    getCampaignGender();
                } catch (ex){

                    
                }

            } else
            {
                
            }

        },
        async: true
    });
}

function getCampaignGender(){
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'reports/getCampaignGendrs/';
    ajaxloder('.male-female-div', 'show');
    
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".male-female-div .alert").remove();
        },
        success: function (html){
            console.log("age"+html);
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        Morris.Donut({
                            element: 'data-gender',
                            data: [
                              {label: "MEN", value: json.male},
                              {label: "WOMEN", value: json.female},
                              {label: "Other", value: json.other}
                            ],
                            colors: ['#ab7df6', '#26c2c9','#faca00'],
                            resize: true
                        });
                    }
                } catch (ex)
                {
                    
                }
            } else
            {
                
            }

        },
        async: true
    });
}

function ajaxloder(divclass, status){
    $("#overlay").remove();
    var over = '<div id="overlay"><img src="' + site_url + 'assets/images/ajax-loader.gif" id="img-load" /></div>';
    $(over).appendTo('body');
    if (status == 'show') {

        $t = $(divclass); // CHANGE it to the table's id you have

        $("#overlay").css({
            opacity: 0.8,
            top: $t.offset().top,
            width: $t.outerWidth(),
            height: $t.outerHeight(),
            left: $t.offset().left // the fix.
        });

        $("#img-load").css({
            top: ($t.height() / 2),
            left: ($t.width() / 2)
        });
        $("#overlay").fadeIn();
    } else if (status == 'hide') {

        $("#overlay").fadeOut();
    }
}

function getCampaign(adAccountId, limit) {
    var url = site_url + 'reports/setDateSessionGet/';
    //ajaxloder('.page-content', 'show');
     $('#preloaderMain').show();
    //ajaxloder('.w1', 'show');
    jQuery("#dayLimit").val(limit);
    jQuery.ajax({
        type: "GET",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function (){
            jQuery(".alert").remove();
        },
        success: function (html){
            //ajaxloder('.page-content', 'hide');
            // $('#preloaderMain').hide();
            window.location.reload(true);
        },
        async: true
    });
}
function getAICampaign(limit) {
    var url = site_url + 'reports/setcampaignSession/';
    //ajaxloder('.page-content', 'show');
    //ajaxloder('.w1', 'show');
    $('#preloaderMain').show();
    //jQuery("#dayLimit").val(limit);
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function (){
            jQuery(".alert").remove();
        },
        success: function (html){
            ajaxloder('.page-content', 'hide');
            window.location.reload(true);
        },
        async: true
    });
}

function getCampaignAjax(adAccountId, limit) {
    var url = site_url + 'reports/getCampaignAjax/';
    //ajaxloder('.page-content', 'show');
     $('#preloaderMain').show();
    //ajaxloder('.w1', 'show');
    jQuery("#dayLimit").val(limit);
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function (){
            jQuery(".alert").remove();
        },
        success: function (html){
			  jQuery("#campaign_ajaxdata").html(html);
             $('#preloaderMain').hide();
          
        },
        async: true
    });
}
//ADDSET FUNCION START
function getAddSets(adAccountId, campaignId, limit){

    var url = site_url + 'addset/setDateSession/';
   // ajaxloder('.page-content', 'show');
   $('#preloaderMain').show();
    //alert(limit);
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    campaignId: campaignId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {

            $(".alert").remove();

        },
        success: function (html)
        {
            //ajaxloder('.page-content', 'hide');
            
            window.location.reload(true);

        },
        async: true
    });
}

function getAddSetClicksData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var page = $("#pageName").val();
    if (page == "addSetReport"){
        var addAccountId = $("#addAccountId").val();
        var url = site_url + 'addset/clickData/';
        ajaxloder('.click-div', 'show');
        jQuery.ajax({
            type: "POST",
            cache: false,
            dataType: "json",
            url: url,
            data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
            beforeSend: function (){
                $(".click-div .alert").remove();
            },
            success: function (html){
                console.log(html);
                ajaxloder('.click-div', 'hide');
                if (html){
                    try{
                        console.log(JSON.stringify(html));
                        new Morris.Line({
                            // ID of the element in which to draw the chart.
                            element: 'click-data-chart',
                            // Chart data records -- each entry in this array corresponds to a point on
                            // the chart.
                            data: eval(JSON.stringify(html)),
                            // The name of the data record attribute that contains x-values.
                            xkey: 'year',
                            // A list of names of data record attributes that contain y-values.
                            ykeys: ['value'],
                            // Labels for the ykeys -- will be displayed when you hover over the
                            // chart.
                            labels: ['Total Clicks'],
                            lineColors: ['#26c1c9'],
                            pointFillColors: ['#26c1c9'],
                            pointStrokeColors: ['#26c1c9'],
                            resize: true,
                            axes: false,
                            hideHover: true
                        });
                        //getAddSetSpentData();
                    } 
                    catch (ex){
                        
                    }

                }
                else{
                    
                }

            },
            async: true
        });
    }
}

function getAddSetSpentData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'addset/getAddSetSpentData/';
    ajaxloder('.spent-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".spent-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.spent-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'spent-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Spent'],
                        lineColors: ['#ab7df6'],
                        pointFillColors: ['#ab7df6'],
                        pointStrokeColors: ['#ab7df6'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });  
                    getAddSetEngagementData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAddSetEngagementData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var page = $("#pageName").val();
    if (page == "addSetReport"){
        var addAccountId = $("#addAccountId").val();
        var url = site_url + 'addset/getAddSetEngagementData/';
        ajaxloder('.engagement-div', 'show');
        jQuery.ajax({
            type: "POST",
            cache: false,
            dataType: "json",
            url: url,
            data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
            beforeSend: function (){
                $(".engagement-div .alert").remove();
            },
            success: function (html){
                console.log(html);
                ajaxloder('.engagement-div', 'hide');
                if (html){
                    try{
                        console.log(JSON.stringify(html));
                        new Morris.Line({
                            // ID of the element in which to draw the chart.
                            element: 'engagement-data-chart',
                            // Chart data records -- each entry in this array corresponds to a point on
                            // the chart.
                            data: eval(JSON.stringify(html)),
                            // The name of the data record attribute that contains x-values.
                            xkey: 'year',
                            // A list of names of data record attributes that contain y-values.
                            ykeys: ['value'],
                            // Labels for the ykeys -- will be displayed when you hover over the
                            // chart.
                            labels: ['Total Engagement'],
                            lineColors: ['#0079c4'],
                            pointFillColors: ['#0079c4'],
                            pointStrokeColors: ['#0079c4'],
                            resize: true,
                            axes: false,
                            hideHover: true
                        });
                        getAddSetPageLikeData();
                    } 
                    catch (ex){
                        
                    }

                }
                else{
                    
                }

            },
            async: true
        });
    }
}

function getAddSetPageLikeData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'addset/getCampaignPageLikeData/';
    ajaxloder('.pagelike-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".pagelike-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.pagelike-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'page-link-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'y',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['a'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Page Like'],
                        lineColors: ['#ff7b00'],
                        pointFillColors: ['#ff7b00'],
                        pointStrokeColors: ['#ff7b00'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAddSetCtrData();
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getAddSetCtrData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'addset/getCampaignCtrData/';
    ajaxloder('.ctr-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".ctr-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.ctr-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'click-rate-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['CTR'],
                        lineColors: ['#faca00'],
                        pointFillColors: ['#faca00'],
                        pointStrokeColors: ['#faca00'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAddSetImpressionData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAddSetImpressionData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'addset/getCampaignImpressionData/';
    ajaxloder('.impression-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".impression-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.impression-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'impressions-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Impression'],
                        lineColors: ['#4990e2'],
                        pointFillColors: ['#4990e2'],
                        pointStrokeColors: ['#4990e2'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });  
                    getAddSetConversionsData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAddSetConversionsData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var url = site_url + 'addset/getCampaignConversionsData/';
    ajaxloder('.conversion-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".conversion-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.conversion-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'conversions-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Conversions'],
                        lineColors: ['#4ece3d'],
                        pointFillColors: ['#4ece3d'],
                        pointStrokeColors: ['#4ece3d'],
                        gridTextColor: ['#000'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAddSetPlacementData()
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getAddSetPlacementData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addset/getAddSetPlacementData/';
    ajaxloder('.placement-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".placement-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.placement-div', 'hide');
            if (html){
                try{
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        jQuery('#desktop').html(json.desktop);
                        jQuery('#mobile').html(json.mobile);
                        jQuery('#right_hand').html(json.right_hand);
                        jQuery('#mobile_external_only').html(json.mobile_external_only);
                        jQuery('#instant_article').html(json.instant_article);

                        jQuery('#desktopPer').html(json.desktopPer);
                        jQuery('#mobilePer').html(json.mobilePer);
                        jQuery('#right_handPer').html(json.right_handPer);
                        jQuery('#mobile_external_onlyPer').html(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer').html(json.instant_articlePer);

                        jQuery('#desktopPer-1').width(json.desktopPer);
                        jQuery('#mobilePer-1').width(json.mobilePer);
                        jQuery('#right_handPer-1').width(json.right_handPer);
                        jQuery('#mobile_external_onlyPer-1').width(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer-1').width(json.instant_articlePer);

                        getAddSetChart();
                    }
                } catch (ex){
                    console.log(ex);
                }
            }
        },
        async: true
    });
}

function getAddSetChart(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addset/getAddSetChart/';
    ajaxloder('.age-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".age-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.age-div', 'hide');
            if (html){
                try{
                    Morris.Bar({
                        element: 'data-age-groups',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['value'],
                        labels: ['Age G'],
                        barColors:['#26c2c9', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                    });
                    getAddSetGender();
                } 
                catch (ex){
                    
                }

            }
            else{
                
            }

        },
        async: true
    });
}

function getAddSetGender(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addset/getAddSetGender/';
    ajaxloder('.male-female-div', 'show');
    
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".male-female-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        Morris.Donut({
                            element: 'data-gender',
                            data: [
                              {label: "MEN", value: json.male},
                              {label: "WOMEN", value: json.female},
                              {label: "Other", value: json.other}
                            ],
                            colors: ['#ab7df6', '#26c2c9','#faca00'],
                            resize: true
                        });
                        getAddSetTimeChart();
                    }
                } catch (ex)
                {
                    
                }
            } else
            {
                
            }

        },
        async: true
    });
}

function getAddSetTimeChart() {
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addset/getAddSetTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".time-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.time-div', 'hide');
            if (html){
                try{
                    Morris.Bar({
                        element: 'data-time-bar',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['Impressions'],
                        // yLabelFormat: function(x) { return ''; },
                        labels: ['Impressions'],
                        barColors:['#ab7df6', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                  });
                    getAddSetCountryChart();
                } catch (ex){

                    
                }

            } else
            {
                
            }

        },
        async: true
    });
}

function getAddSetCountryChart() {
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addset/getAddSetCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".country-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.country-div', 'hide');
            if (html){
                try{
                    Morris.Donut({
                        element: 'data-locations',
                        data: eval(JSON.stringify(html)),
                        colors: ['#ab7df6', '#faca00', '#81c926', '#26c2c9'],
                        resize: true
                    });
                } 
                catch (ex){
                    
                }
            }
        },
        async: true
    });
}

//ADDS FUNCION START
function getAdds(adAccountId, campaignId, addsetId, limit){

    var url = site_url + 'addds/setDateSession/';
   // ajaxloder('.page-content', 'show');
   $('#preloaderMain').show();
    //alert(limit);
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    campaignId: campaignId,
                    addSetId: addsetId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {

            $(".alert").remove();

        },
        success: function (html)
        {
            ajaxloder('.page-content', 'hide');
            
            window.location.reload(true);

        },
        async: true
    });
}

function getAddsClicksData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var page = $("#pageName").val();
    if (page == "adddsReport"){
        var addAccountId = $("#addAccountId").val();
        var url = site_url + 'addds/clickData/';
        ajaxloder('.click-div', 'show');
        jQuery.ajax({
            type: "POST",
            cache: false,
            dataType: "json",
            url: url,
            data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val() },
            beforeSend: function (){
                $(".click-div .alert").remove();
            },
            success: function (html){
                console.log(html);
                ajaxloder('.click-div', 'hide');
                if (html){
                    try{
                        console.log(JSON.stringify(html));
                        new Morris.Line({
                            // ID of the element in which to draw the chart.
                            element: 'click-data-chart',
                            // Chart data records -- each entry in this array corresponds to a point on
                            // the chart.
                            data: eval(JSON.stringify(html)),
                            // The name of the data record attribute that contains x-values.
                            xkey: 'year',
                            // A list of names of data record attributes that contain y-values.
                            ykeys: ['value'],
                            // Labels for the ykeys -- will be displayed when you hover over the
                            // chart.
                            labels: ['Total Clicks'],
                            lineColors: ['#26c1c9'],
                            pointFillColors: ['#26c1c9'],
                            pointStrokeColors: ['#26c1c9'],
                            resize: true,
                            axes: false,
                            hideHover: true
                        });
                        getAddsSpentData();
                    } 
                    catch (ex){
                        
                    }

                }
                else{
                    
                }

            },
            async: true
        });
    }
}

function getAddsSpentData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var url = site_url + 'addds/getAddsSpentData/';
    ajaxloder('.spent-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".spent-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.spent-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'spent-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Spent'],
                        lineColors: ['#ab7df6'],
                        pointFillColors: ['#ab7df6'],
                        pointStrokeColors: ['#ab7df6'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });  
                    getAddsEngagementData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAddsEngagementData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var url = site_url + 'addds/getAddsEngagementData/';
    ajaxloder('.engagement-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".engagement-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.engagement-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                            // ID of the element in which to draw the chart.
                            element: 'engagement-data-chart',
                            // Chart data records -- each entry in this array corresponds to a point on
                            // the chart.
                            data: eval(JSON.stringify(html)),
                            // The name of the data record attribute that contains x-values.
                            xkey: 'year',
                            // A list of names of data record attributes that contain y-values.
                            ykeys: ['value'],
                            // Labels for the ykeys -- will be displayed when you hover over the
                            // chart.
                            labels: ['Total Engagement'],
                            lineColors: ['#0079c4'],
                            pointFillColors: ['#0079c4'],
                            pointStrokeColors: ['#0079c4'],
                            resize: true,
                            axes: false,
                            hideHover: true
                        });
                    getAddsPageLikeData();
                } 
                catch (ex){
                    
                }

            }
            else{
                
            }

        },
        async: true
    });
}

function getAddsPageLikeData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var url = site_url + 'addds/getAddsPageLikeData/';
    ajaxloder('.pagelike-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".pagelike-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.pagelike-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'page-link-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'y',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['a'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Page Like'],
                        lineColors: ['#ff7b00'],
                        pointFillColors: ['#ff7b00'],
                        pointStrokeColors: ['#ff7b00'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAddsCtrData();
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getAddsCtrData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var url = site_url + 'addds/getAddsCtrData/';
    ajaxloder('.ctr-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".ctr-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.ctr-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'click-rate-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['CTR'],
                        lineColors: ['#faca00'],
                        pointFillColors: ['#faca00'],
                        pointStrokeColors: ['#faca00'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAddsImpressionData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAddsImpressionData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var url = site_url + 'addds/getAddsImpressionData/';
    ajaxloder('.impression-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".impression-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.impression-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'impressions-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Impression'],
                        lineColors: ['#4990e2'],
                        pointFillColors: ['#4990e2'],
                        pointStrokeColors: ['#4990e2'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });  
                    getAddsConversionsData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAddsConversionsData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var url = site_url + 'addds/getAddsConversionsData/';
    ajaxloder('.conversion-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".conversion-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.conversion-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'conversions-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Conversions'],
                        lineColors: ['#4ece3d'],
                        pointFillColors: ['#4ece3d'],
                        pointStrokeColors: ['#4ece3d'],
                        gridTextColor: ['#000'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAddsPlacementData()
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getAddsPlacementData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addds/getAddsPlacementData/';
    ajaxloder('.placement-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".placement-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.placement-div', 'hide');
            if (html){
                try{
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        jQuery('#desktop').html(json.desktop);
                        jQuery('#mobile').html(json.mobile);
                        jQuery('#right_hand').html(json.right_hand);
                        jQuery('#mobile_external_only').html(json.mobile_external_only);
                        jQuery('#instant_article').html(json.instant_article);

                        jQuery('#desktopPer').html(json.desktopPer);
                        jQuery('#mobilePer').html(json.mobilePer);
                        jQuery('#right_handPer').html(json.right_handPer);
                        jQuery('#mobile_external_onlyPer').html(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer').html(json.instant_articlePer);

                        jQuery('#desktopPer-1').width(json.desktopPer);
                        jQuery('#mobilePer-1').width(json.mobilePer);
                        jQuery('#right_handPer-1').width(json.right_handPer);
                        jQuery('#mobile_external_onlyPer-1').width(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer-1').width(json.instant_articlePer);

                        getAddsChart();
                    }
                } catch (ex){
                    console.log(ex);
                    
                }

            } else
            {
                
            }

        },
        async: true
    });
}

function getAddsChart(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addds/getAddsChart/';
    ajaxloder('.age-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".age-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.age-div', 'hide');
            if (html){
                try{
                    Morris.Bar({
                        element: 'data-age-groups',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['value'],
                        labels: ['Age G'],
                        barColors:['#26c2c9', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                    });
                    getAddsGender();
                } 
                catch (ex){
                    
                }

            }
            else{
                
            }

        },
        async: true
    });
}

function getAddsGender(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addds/getAddsGender/';
    ajaxloder('.male-female-div', 'show');
    
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".male-female-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                         Morris.Donut({
                            element: 'data-gender',
                            data: [
                              {label: "MEN", value: json.male},
                              {label: "WOMEN", value: json.female},
                              {label: "Other", value: json.other}
                            ],
                            colors: ['#ab7df6', '#26c2c9','#faca00'],
                            resize: true
                        });
                        getAddsTimeChart();
                    }
                } catch (ex)
                {
                    
                }
            } else
            {
                
            }

        },
        async: true
    });
}

function getAddsGendervarious(pagename,evall){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = pagename;
    var url = site_url + 'addds/getAddsGendervarious/';
   // ajaxloder('.gender-divvar', 'show');
    
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text() },
        beforeSend: function (){
            $(".male-female-div .alert").remove();
        },
        success: function (html){
            console.log("age"+html);
           //alert(html);
           // ajaxloder('.gender-divvar', 'hide');
			var obj = JSON.parse(html); 
			
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".genderbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".genderbody").html(obj[1]);
			}
            /*if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        Morris.Donut({
                            element: 'data-gender',
                            data: [
                              {label: "MEN", value: json.male},
                              {label: "WOMEN", value: json.female},
                              {label: "Other", value: json.other}
                            ],
                            colors: ['#ab7df6', '#26c2c9','#faca00'],
                            resize: true
                        });
                    }
                } catch (ex)
                {
                    
                }
            } else
            {
                
            }*/

        },
        async: true
    });
}

function getAddsTimeChart() {
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addds/getAddsTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".time-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.time-div', 'hide');
            if (html){
                try{
                    Morris.Bar({
                        element: 'data-time-bar',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['Impressions'],
                        // yLabelFormat: function(x) { return ''; },
                        labels: ['Impressions'],
                        barColors:['#ab7df6', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                  });
                    getAddsCountryChart();
                } catch (ex){

                    
                }

            } else
            {
                
            }

        },
        async: true
    });
}


function getAddsCountryChartvarious(pagename) {
    //alert(pagename);
    var addAccountId = $("#addAccountId").val();
    var campaignId = $("#campaignId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = pagename;//$("#pageName1").val();
    var url = site_url + 'addds/getAddsCountryChartvarious/';
    ajaxloder('.country-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".country-divvar .alert").remove();
        },
        success: function (html){
            console.log("locations"+html);
            ajaxloder('.country-divvar', 'hide');
            if(html[0] == "0"){
                $("#locationmain").html("<li>"+html[0]+"</li>");
            }
            else{
                $("#locationmain").html("<li>"+html[0]+"</li>"+html[1]);
            }
            //alert(JSON.stringify(html));
           /* if (html){
                try{
                    Morris.Donut({
                        element: 'data-locations',
                        data: eval(JSON.stringify(html)),
                        colors: ['#ab7df6', '#faca00', '#81c926', '#26c2c9'],
                        resize: true
                    });
                    getCampaignChart();
                } 
                catch (ex){
                    
                }

            } else
            {
                
            }*/

        },
        async: true
    });
    return false;
}

function getAddsCountryChart() {
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'addds/getAddsCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".country-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.country-div', 'hide');
            if (html){
                try{
                     Morris.Donut({
                        element: 'data-locations',
                        data: eval(JSON.stringify(html)),
                        colors: ['#ab7df6', '#faca00', '#81c926', '#26c2c9'],
                        resize: true
                    });
                } 
                catch (ex){
                    
                }

            } else
            {
                
            }

        },
        async: true
    });
}

//ADD FUNCION START
function getAdd(adAccountId, campaignId, addsetId, addId, limit){

    var url = site_url + 'adone/setDateSession/';
     $('#preloaderMain').show();
    //ajaxloder('.page-content', 'show');
    //alert(limit);
    $("#dayLimit").val(limit);
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    campaignId: campaignId,
                    addSetId: addsetId,
                    addId: addId,
                    limit: limit,
                    ajax: 1
                },
        beforeSend: function ()
        {

            $(".alert").remove();

        },
        success: function (html)
        {
            //ajaxloder('.page-content', 'hide');
            
            window.location.reload(true);

        },
        async: true
    });
}

function getAdoneClicksData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var page = $("#pageName").val();
    if (page == "adReport"){
        var addAccountId = $("#addAccountId").val();
        var url = site_url + 'adone/clickData/';
        ajaxloder('.click-div', 'show');
        jQuery.ajax({
            type: "POST",
            cache: false,
            dataType: "json",
            url: url,
            data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val() },
            beforeSend: function (){
                $(".click-div .alert").remove();
            },
            success: function (html){
                console.log(html);
                ajaxloder('.click-div', 'hide');
                if (html){
                    try{
                        console.log(JSON.stringify(html));
                        new Morris.Line({
                            // ID of the element in which to draw the chart.
                            element: 'click-data-chart',
                            // Chart data records -- each entry in this array corresponds to a point on
                            // the chart.
                            data: eval(JSON.stringify(html)),
                            // The name of the data record attribute that contains x-values.
                            xkey: 'year',
                            // A list of names of data record attributes that contain y-values.
                            ykeys: ['value'],
                            // Labels for the ykeys -- will be displayed when you hover over the
                            // chart.
                            labels: ['Total Clicks'],
                            lineColors: ['#26c1c9'],
                            pointFillColors: ['#26c1c9'],
                            pointStrokeColors: ['#26c1c9'],
                            resize: true,
                            axes: false,
                            hideHover: true
                        });
                        getAdoneSpentData();
                    } 
                    catch (ex){
                        
                    }
                }
            },
            async: true
        });
    }
}

function getAdoneSpentData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdoneSpentData/';
    ajaxloder('.spent-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".spent-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.spent-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'spent-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Spent'],
                        lineColors: ['#ab7df6'],
                        pointFillColors: ['#ab7df6'],
                        pointStrokeColors: ['#ab7df6'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAdoneEngagementData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAdoneEngagementData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdoneEngagementData/';
    ajaxloder('.engagement-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".engagement-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.engagement-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                            // ID of the element in which to draw the chart.
                            element: 'engagement-data-chart',
                            // Chart data records -- each entry in this array corresponds to a point on
                            // the chart.
                            data: eval(JSON.stringify(html)),
                            // The name of the data record attribute that contains x-values.
                            xkey: 'year',
                            // A list of names of data record attributes that contain y-values.
                            ykeys: ['value'],
                            // Labels for the ykeys -- will be displayed when you hover over the
                            // chart.
                            labels: ['Total Engagement'],
                            lineColors: ['#0079c4'],
                            pointFillColors: ['#0079c4'],
                            pointStrokeColors: ['#0079c4'],
                            resize: true,
                            axes: false,
                            hideHover: true
                        });
                    getAdonePageLikeData();
                } 
                catch (ex){
                    
                }
            }
        },
        async: true
    });
}

function getAdonePageLikeData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdonePageLikeData/';
    ajaxloder('.pagelike-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".pagelike-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.pagelike-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'page-link-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'y',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['a'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Page Like'],
                        lineColors: ['#ff7b00'],
                        pointFillColors: ['#ff7b00'],
                        pointStrokeColors: ['#ff7b00'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAdoneCtrData();
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getAdoneCtrData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdoneCtrData/';
    ajaxloder('.ctr-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".ctr-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.ctr-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'click-rate-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['CTR'],
                        lineColors: ['#faca00'],
                        pointFillColors: ['#faca00'],
                        pointStrokeColors: ['#faca00'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAdoneImpressionData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAdoneImpressionData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdoneImpressionData/';
    ajaxloder('.impression-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".impression-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.impression-div', 'hide');
            console.log(html);
            if (html){
                try{           
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'impressions-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Impression'],
                        lineColors: ['#4990e2'],
                        pointFillColors: ['#4990e2'],
                        pointStrokeColors: ['#4990e2'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });  
                    getAdoneConversionsData();
                } 
                catch (ex){
                
                }
            } 
        },
        async: true
    });
}

function getAdoneConversionsData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdoneConversionsData/';
    ajaxloder('.conversion-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val() },
        beforeSend: function (){
            $(".conversion-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.conversion-div', 'hide');
            if (html){
                try{
                    console.log(JSON.stringify(html));
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'conversions-data-chart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: eval(JSON.stringify(html)),
                        // The name of the data record attribute that contains x-values.
                        xkey: 'year',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['value'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Total Conversions'],
                        lineColors: ['#4ece3d'],
                        pointFillColors: ['#4ece3d'],
                        pointStrokeColors: ['#4ece3d'],
                        gridTextColor: ['#000'],
                        resize: true,
                        axes: false,
                        hideHover: true
                    });
                    getAdonePlacementData()
                } 
                catch (ex){
                
                }
            }
        },
        async: true
    });
}

function getAdonePlacementData(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'adone/getAdonePlacementData/';
    ajaxloder('.placement-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".placement-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.placement-div', 'hide');
            if (html){
                try{
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        jQuery('#desktop').html(json.desktop);
                        jQuery('#mobile').html(json.mobile);
                        jQuery('#right_hand').html(json.right_hand);
                        jQuery('#mobile_external_only').html(json.mobile_external_only);
                        jQuery('#instant_article').html(json.instant_article);

                        jQuery('#desktopPer').html(json.desktopPer);
                        jQuery('#mobilePer').html(json.mobilePer);
                        jQuery('#right_handPer').html(json.right_handPer);
                        jQuery('#mobile_external_onlyPer').html(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer').html(json.instant_articlePer);

                        jQuery('#desktopPer-1').width(json.desktopPer);
                        jQuery('#mobilePer-1').width(json.mobilePer);
                        jQuery('#right_handPer-1').width(json.right_handPer);
                        jQuery('#mobile_external_onlyPer-1').width(json.mobile_external_onlyPer);
                        jQuery('#instant_articlePer-1').width(json.instant_articlePer);

                        getAdoneChart();
                    }
                } catch (ex){
                }
            } 
        },
        async: true
    });
}

function getAdoneChart(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'adone/getAdoneChart/';
    ajaxloder('.age-div', 'show');
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".age-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.age-div', 'hide');
            if (html){
                try{
                    Morris.Bar({
                        element: 'data-age-groups',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['value'],
                        labels: ['Age G'],
                        barColors:['#26c2c9', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                    });
                    getAdoneGender();
                } 
                catch (ex){
                    
                }
            }
        },
        async: true
    });
}

function getAdoneGender(){
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'adone/getAdoneGender/';
    ajaxloder('.male-female-div', 'show');
    
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".male-female-div .alert").remove();
        },
        success: function (html){
            console.log(html);
            ajaxloder('.male-female-div', 'hide');
            if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        Morris.Donut({
                            element: 'data-gender',
                            data: [
                              {label: "MEN", value: json.male},
                              {label: "WOMEN", value: json.female},
                              {label: "Other", value: json.other}
                            ],
                            colors: ['#ab7df6', '#26c2c9','#faca00'],
                            resize: true
                        });
                        getAdoneTimeChart();
                    }
                } catch (ex)
                {
                }
            }
        },
        async: true
    });
}

function getAdoneTimeChart() {
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'adone/getAdoneTimeChart/';
    ajaxloder('.time-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".time-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.time-div', 'hide');
            if (html){
                try{
                    Morris.Bar({
                        element: 'data-time-bar',
                        data: eval(JSON.stringify(html)),
                        xkey: 'y',
                        ykeys: ['Impressions'],
                        // yLabelFormat: function(x) { return ''; },
                        labels: ['Impressions'],
                        barColors:['#ab7df6', '#b4c1d7', '#fcc9ba'],
                        hideHover: true,
                        resize: true,
                        xLabelMargin: 5
                  });
                    getAdoneCountryChart();
                } catch (ex){    
                }
            } 
        },
        async: true
    });
}
function getAdoneCountryChartvarious(pagename) {
    //alert(pagename);
    var addAccountId = $("#addAccountId").val();
    var campaignId = $("#campaignId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdoneCountryChartvarious/';
    var pageName1 = pagename;//$("#pageName1").val();
    
    ajaxloder('.country-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1,clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".country-divvar .alert").remove();
        },
        success: function (html){
            console.log("locations"+html);
            ajaxloder('.country-divvar', 'hide');
            if(html[0] == "0"){
                $("#locationmain").html("<li>"+html[0]+"</li>");
            }
            else{
                $("#locationmain").html("<li>"+html[0]+"</li>"+html[1]);
            }
            //alert(JSON.stringify(html));
           /* if (html){
                try{
                    Morris.Donut({
                        element: 'data-locations',
                        data: eval(JSON.stringify(html)),
                        colors: ['#ab7df6', '#faca00', '#81c926', '#26c2c9'],
                        resize: true
                    });
                    getCampaignChart();
                } 
                catch (ex){
                    
                }

            } else
            {
                
            }*/

        },
        async: true
    });
    return false;
}
function getAdoneGendervarious(pagename,evall) {
    var addAccountId = $("#addAccountId").val();
    var campaignId = $("#campaignId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var url = site_url + 'adone/getAdoneGendervarious/';
    var pageName1 = pagename; 
    //ajaxloder('.gender-divvar', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1, clickrate:$("#clickrate").text() },
        beforeSend: function (){
            //$(".country-divvar .alert").remove();
        },
        success: function (html){
		
            var obj = html.toString().split("<span>");
			if(obj[0] == "0"){
				$(evall).parent().parent().parent().parent().find(".genderbody").html(obj[0]);
			}
			else{
				$(evall).parent().parent().parent().parent().find(".genderbody").html(obj[1]);
			}
			
            /*if (html)
            {
                try
                {
                    var json = $.parseJSON(html);
                    console.log(json);
                    if (json.code == 100){
                        Morris.Donut({
                            element: 'data-gender',
                            data: [
                              {label: "MEN", value: json.male},
                              {label: "WOMEN", value: json.female},
                              {label: "Other", value: json.other}
                            ],
                            colors: ['#ab7df6', '#26c2c9','#faca00'],
                            resize: true
                        });
                    }
                } catch (ex)
                {
                    
                }
            } else
            {
                
            }*/

        },
        async: true
    });
    return false;
}
function getAdoneCountryChart() {
    var campaignId = $("#campaignId").val();
    var addAccountId = $("#addAccountId").val();
    var addsetId = $("#addsetId").val();
    var addId = $("#addId").val();
    var pageName1 = $("#pageName1").val();
    var url = site_url + 'adone/getAdoneCountryChart/';
    ajaxloder('.country-div', 'show');
    $.ajax({
        type: "POST",
        cache: false,
        dataType: "json",
        url: url,
        data:{  campaignId: campaignId,adAccountId: addAccountId, addsetId: addsetId, addId:addId, ajax: 1, limit: $("#dayLimit").val(), pageName:pageName1 },
        beforeSend: function (){
            $(".country-div .alert").remove();
        },
        success: function (html){
            ajaxloder('.country-div', 'hide');
            if (html){
                try{
                    Morris.Donut({
                        element: 'data-locations',
                        data: eval(JSON.stringify(html)),
                        colors: ['#ab7df6', '#faca00', '#81c926', '#26c2c9'],
                        resize: true
                    });
                } 
                catch (ex){     
                }
            }
        },
        async: true
    });
}


function fnChangeCheck1(id){

}

function fnChangeCheck(id, val){
    var isChecked = $("#"+id).is(":checked");
    var page = $("#pageName").val();
    if (isChecked) {
        if (page == "addSetReport") {
            toolStartStop('ACTIVE', 'addSetReport', val);
        } else if (page == "adddsReport") {
            toolStartStop('ACTIVE', 'adddsReport', val);
        } else {
            toolStartStop('ACTIVE', 'campaignreport', val);
        }
    } else {
        if (page == "addSetReport") {
            toolStartStop('PAUSED', 'addSetReport', val);
        } else if (page == "adddsReport") {
            toolStartStop('PAUSED', 'adddsReport', val);
        } else {
            toolStartStop('PAUSED', 'campaignreport', val);
        }
    }
}

function toolStartStop(action, type1, Id){
    
    var addAccountId = $("#addAccountId").val();
   // ajaxloder('.page-content', 'show');

    var type = "";
    if (type1 == "campaignreport"){
        //Id = $("#campaignId").val();
        type = "addset";
        var url = site_url + 'addset/toolStartStop/';
    }
    if (type1 == "addSetReport"){
        //Id = $("#addsetId").val();
        type = "addset";
        var url = site_url + 'addds/toolStartStop/';
    }
    if (type1 == "adddsReport"){
        //Id = $("#addId").val();
        type = "add";
        var url = site_url + 'adone/toolStartStop/';
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data: {
            adAccountId: addAccountId,
            Id: Id,
            type: type,
            action: action,
            ajax: 1
        },
        beforeSend: function (){
            $(".alert").remove();
        },
        success: function (html){
            //ajaxloder('.page-content', 'hide');
            if (html){
                window.location.reload();
            } 
            else{
                $(".container .messages").html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Something wrong.Please try again.</div>');
            }
        },
        async: true
    });
}

function scaleCampaign(adAccountId, campaignId, adSetId) {
    $("#editCampaignScale").modal("hide");
    editAdsetScale(adAccountId, campaignId, adSetId);
}
var loaderHtml = '<div class="pos_relative"><div id="preloader" class="displayNone"><div id="status"><i class="fa fa-spinner fa-spin"></i></div></div></div>';

function editAdsetScale(adAccountId, campaignId, adsetId) {
    $("#editAdSetScale").modal("show");
    $(".adset-modal").css("height", "50");
    var url = site_url + 'editadset/adsetscale/';

    $('#editAdSetScale .modal-body').append(loaderHtml);

    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data:
                {
                    adAccountId: adAccountId,
                    campaignId: campaignId,
                    adsetId: adsetId,
                    ajax: 1
                },
        beforeSend: function ()
        {
            $(".alert").remove();
        },
        success: function (html)
        {
            $(".adset-modal").css("height", "auto");
            $('#editAdSetScale .modal-content').html(html);
            if (document.getElementById("adset_start_date")) {
                $('#adset_start_date').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    startDate: new Date()

                });
            }
            if (document.getElementById("adset_end_date")) {
                $('#adset_end_date').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    startDate: new Date()
                });
            }
        },
        async: true
    });
}

function saveAdsetScale(adsetId){
    $(".alert").remove();
    var url = site_url + 'editadset/saveaddSetScale/';

    var amount = $("#InputAmount").val();
    if (amount == "") {
        $('#editAdSetScale .modal-body').prepend('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> Required fields are empty.</div>');
    } 
    else{
        $('#editAdSetScale .modal-body').append(loaderHtml);
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data:
            {
                adsetId: adsetId,
                amount: amount,
                ajax: 1
            },
            beforeSend: function (){
                $(".alert").remove();
            },
            success: function (html){
                $(".pos_relative").remove();
                $(".adset-modal").css("height", "auto");
                try {
                    var obj = $.parseJSON(html);
                    //console.log(obj);
                    if (obj.code == 200)
                    {
                        $('#editAdSetScale .modal-body').prepend('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> ' + obj.message + '</div>');
                    } else {
                        $('#editAdSetScale .modal-body').prepend('<div class="alert alert-success"><button class="close" data-dismiss="alert"></button> Updated Successfully</div>');
                        //window.location.reload();
                    }

                } catch (ex) {
                    $('#editAdSetScale .modal-body').prepend('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button> ' + ex + '</div>');
                }
            },
            async: true
        });
    }
}