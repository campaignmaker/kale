
function getcomparedatapdf(comparedataservers){
    
   var comparedata = '';
  
   $.each(comparedataservers, function(indexkey, tempservers) {
   	var columncount = 0
    	$.each(tempservers, function(index, serverdata) {
         columncount++;
	});	
   comparedata += '<table class="table-s6 column'+columncount+'">';
   comparedata += '<colgroup>';
    	
    

	comparedata +=	'<col width="20%">';

	var colwidth = 80/columncount;
    
	$.each(tempservers, function(index, serverdata) {
	comparedata +=	'<col width="'+colwidth+'%">';
         
	});								
	comparedata +=	'</colgroup><thead><tr>';
   
										
						
							comparedata +='<th>'+indexkey+'</th>';
							var comparecol = 1;
							var finaldatakeyname = '';
							$.each(tempservers, function(datakey, datavalue) {
				
									 if(datakey.length > 30){
	                                 var datakeyname =   $.trim(datakey).substring(0, 30)+'...';
	                                }else{
	                                  var datakeyname = datakey;
	     
	                                    }
	                                  
                                  comparedata +=	'<th>'+datakeyname+comparecol+'</th>';	
	
							comparecol++;			
				 			});	
				 			
	comparedata += '</tr></thead><tbody>';
	comparedata += '<tr class="highlight-data">';
	comparedata +='<td>Total</td>';
						
				   $.each(tempservers, function(indextempserver, servers) {
					comparedata +='<td>'+number_formate_int(servers['total']['counts'])+'</td>';
                   });	
	                     
									
	comparedata += '</tr>';
	comparedata += '<tr class="highlight-data">';
									
					comparedata +='<td>Cost Per</td>';
					$.each(tempservers, function(indextempserver, servers) {
					comparedata +='<td>'+number_formate_currency(servers['cost_per']['counts'])+'</td>';
			     	});	
							     	
															
	comparedata += '</tr>';
	comparedata += '<tr class="highlight-data">';
	
					comparedata +='<td>Rate</td>';
			      	$.each(tempservers, function(indextempserver, servers) {
					comparedata +='<td>'+number_formate(servers['rate']['counts'])+'</td>';
				
					});
							
	comparedata += '</tr>';
	comparedata += '</tbody></table>';
	
   });
    
    
  return  comparedata; 
    
}


function spendtTablepdf(servers){
   
     
    var spendTable = '';
    
    spendTable += '<colgroup>';
    	
    	var columncount = 0
    	$.each(servers, function(index, serverdata) {
         columncount++;
	});	

	spendTable +=	'<col width="20%">';

	var colwidth = 80/columncount;
    
	$.each(servers, function(index, serverdata) {
	spendTable +=	'<col width="'+colwidth+'%">';
         
	});								
	spendTable +=	'</colgroup><thead><tr>';
    
	spendTable +=	'<th>SPEND</th>';
	
	var spendcol = 1;
	$.each(servers, function(index, value) {
	 
    	 if(index.length > 30){
    	   var indexname =   $.trim(index).substring(0, 30)+'...';
    	 }else{
    	     var indexname = index;
    	     
    	 }
    	
    	spendTable +=	'<th>'+indexname+spendcol+'</th>';
    spendcol++;
    });
     								
	spendTable +=	'</tr></thead>';
							
	   spendTable +=	'<tbody>';
       spendTable +=	'<tr class="highlight-data">';
	spendTable +=	'<td>Last 24 Hours</td>';
	
	$.each(servers, function(index, serverdata) {
	spendTable += '<td>'+number_formate_currency(serverdata['last24_hour']['spend'])+'</td>';
	});		
	spendTable +=	'</tr>';
	
	spendTable +=	'<tr class="highlight-data">';
	spendTable +=	'<td>Past 3 Days</td>';
	
	$.each(servers, function(index, serverdata) {
	
     	spendTable += '<td>'+number_formate_currency(serverdata['past3days']['spend'])+'</td>';
     
	});	
	
		
		spendTable += '</tr>';

    
    
    
    spendTable += '<tr class="highlight-data">';
  
    	spendTable +=	'<td>Past 30 Days</td>';
	 $.each(servers, function(index, serverdata) {
     	spendTable +=	'<td>'+number_formate_currency(serverdata['past_30days']['spend'])+'</td>';
	});	
	
	spendTable +=	'</tr>';
	
	
	spendTable += '<tr class="highlight-data">';
    	spendTable +=	'<td>Lifetime</td>';
	    $.each(servers, function(index, serverdata) {
     	spendTable +=	'<td>'+number_formate_currency(serverdata['lifetime']['spend']);+'</td>';
	});		
	
		spendTable += '</tr>';
														
    spendTable +=	'</tbody>';								
																
    return spendTable;

}

function gethitTablepdf(servers){
   
    var spendTable = '';
    
    
    	spendTable += '<colgroup>';
    	
    	var columncount = 0
    	$.each(servers, function(index, serverdata) {
         columncount++;
	});	

	spendTable +=	'<col width="20%">';

	var colwidth = 80/columncount;
    
	$.each(servers, function(index, serverdata) {
	spendTable +=	'<col width="'+colwidth+'%">';
         
	});								
	spendTable +=	'</colgroup><thead><tr>';

	
	spendTable +=	'<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>';
	
	var hitcolumn = 1;
	$.each(servers, function(index, value) {
	    
	if(index.length > 30){
	   var indexname =   $.trim(index).substring(0, 30)+'...';
	 }else{
	    var indexname = index;
	 }
	
	spendTable +=	'<th>'+indexname+hitcolumn+'</th>';

      hitcolumn++;      
            });
     								
											
	spendTable +=	'</tr></thead><tbody><tr class="highlight-data">';
    var col = 2;
	spendTable +=	'<td>Impressions</td>';
	
	$.each(servers, function(index, serverdata) {
	
	spendTable += '<td>'+number_formate_int(serverdata['impressions']['hits'])+'</td>';

	});		
				
	spendTable +=	'</tr>';
	
	spendTable +=	'<tr class="highlight-data">';
	spendTable +=	'<td >CPM</td>';
	
	$.each(servers, function(index, serverdata) {
     	spendTable += '<td >'+number_formate_currency(serverdata['cpm']['hits'])+'</td>';
	});	
	
								
    spendTable +=	'</tr>';
    
    spendTable += '<tr class="highlight-data">';
   									
    
    	spendTable +=	'<td>Frequency</td>';
	 $.each(servers, function(index, serverdata) {
 
     	spendTable +=	'<td>'+number_formate(serverdata['frequency']['hits'])+'</td>';
     	
	});	
	spendTable +=	'</tr>';
								
    spendTable +=	'</tbody>';								
																
    return spendTable;

}

/*
function number_formate(number){
    if (typeof number === "undefined") {
    return '0.00';
    }else{
        console.log(number);
       
        
         var num;
    if( number % 1 != 0){
        
    num = parseFloat(number);
    }else{
        
       num = parseInt(number); 
    }
    var formated_number = num.toFixed(2)
    return delimitNumbers(formated_number);
    }
}

function delimitNumbers(str) {
  return (str + "").replace(/\b(\d+)((\.\d+)*)\b/g, function(a, b, c) {
    return (b.charAt(0) > 0 && !(c || ".").lastIndexOf(".") ? b.replace(/(\d)(?=(\d{3})+$)/g, "$1,") : b) + c;
  });
}

*/