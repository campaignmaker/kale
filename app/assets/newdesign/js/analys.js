
function getcomparedata(comparedataservers){
    
   var comparedata = '';
  
   $.each(comparedataservers, function(indexkey, tempservers) {
       
       
       var columncount = 0
    	$.each(tempservers, function(index, serverdata) {
         columncount++;
	});	
   
       
   comparedata += '<div class="table-holder"><table class="table-report column'+columncount+'">';
													
	comparedata += '<thead><tr>';
										
						
							comparedata +='<th>'+datapointlabel(indexkey)+'</th>';
							
							//console.log(tempservers);
							
							$.each(tempservers, function(datakey, datavalue) {
							  
							   if(datakey.length > 30){
                                	   var datakeyname =   $.trim(datakey).substring(0, 30)+'...';
                            	      }else{
                            	          
                            	         var datakeyname = datakey;
                            	      }
							    
								
	                         comparedata +=	'<th title="'+datakey+'">'+datakeyname+'</th>';	
										
				 			});	
				 		
				 			comparedata +='<th>Breakdown</th>';
						
	comparedata += '</tr></thead><tbody>';
	
	
      // alert(servers);
	
	comparedata += '<tr class="tab-holder"><td colspan="5"><table>';
				
													
	comparedata += '<tbody><tr class="highlight-data">';
													
							comparedata +='<td title="Total">Total</td>';
						
						   $.each(tempservers, function(indextempserver, servers) {
							comparedata +='<td title="Total">'+number_formate_int(servers['total']['counts'])+'</td>';
	                       });	
	                       
	                    
							comparedata +=	'<td title="Breakdown">';
		                    comparedata +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
		
		                    $.each(tempservers, function(index, serverdata) {
        					     var incid = 1;
            					 $.each(serverdata['total']['breakdown'], function(breakdownkey, breakdowndata) {
            					 comparedata +=	'	<li><a href="#tab-3-'+incid+'" class="opener tag">'+breakdownkey+'</a></li>';
            					 incid++;
					            });
					            return false; 
				            });	
		
	                   comparedata += '</ul></td>';	
	                  
	                   comparedata += 	'<tr><td colspan="5" class="tab-content">';
        
		comparedata +=	'<div class="slide" id="tab-1-1" style="display:none">';
		
		comparedata +=	'<table>';
		
		comparedata +=	'<tr><td>MALE</td>';
			$.each(tempservers, function(index, serverdata) {
					
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['gender']['male'])+'</td>';
					     
					 
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		
		comparedata +=	'<tr><td>FEMALE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		
			comparedata +=	'<tr><td>UNKNOWN</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'</table></div>';
		
		comparedata +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		comparedata +=	'<tr>';
		
			
			
			comparedata +=	'<tr><td>18-24</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>25-34</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			comparedata +='<td>&nbsp;</td></tr>';	
				comparedata +=	'<tr><td>35-44</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['age']['35-44'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td></tr>';
		    	comparedata +=	'<tr><td>45-54</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['age']['45-54'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>55-64</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'<tr><td>65+</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['age']['65'])+'</td>';
				});	
			
				comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'</table></div>';	
				
				comparedata +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		comparedata +=	'<tr>';
		
			
			
			comparedata +=	'<tr><td>DESKTOP FEED</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  comparedata +='<td>&nbsp;</td>';
		  comparedata +='</tr>';
			comparedata +=	'<tr><td>MOBILE FEED</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			comparedata +='<td>&nbsp;</td>';
			comparedata +='</tr>';	
				comparedata +=	'<tr><td>INSTAGRAM</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td>';
		   comparedata +='</tr>';
			comparedata +=	'<tr><td>AUDIENCE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td>';
		comparedata +='</tr>';
		comparedata +=	'<tr><td>RIGHT SIDE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['total']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
		
	   comparedata +='<td>&nbsp;</td>';
	   comparedata +='</tr>';
		comparedata +=  '</table></div>';
		

		
		comparedata += '</tr>';
									
	comparedata += '</tr>';
	comparedata += '</tbody></table></td></tr><tr class="tab-holder"><td colspan="5"><table>';
				
				
														
	                           comparedata += '<tbody><tr class="highlight-data">';
														
								comparedata +='<td title="Cost Per">Cost Per</td>';
							
						
									$.each(tempservers, function(indextempserver, servers) {
									comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(servers['cost_per']['counts'])+'</td>';
							     	});	
							     	
							     	comparedata +=	'<td title="Breakdown">';
		                    comparedata +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
		
		                    $.each(tempservers, function(index, serverdata) {
        					     var incid = 1;
            					 $.each(serverdata['cost_per']['breakdown'], function(breakdownkey, breakdowndata) {
            					 comparedata +=	'	<li><a href="#tab-3-'+incid+'" class="opener tag">'+breakdownkey+'</a></li>';
            					 incid++;
					            });
					            return false; 
				            });	
		
	                   comparedata += '</ul></td>';
												
							comparedata += 	'<tr><td colspan="5" class="tab-content">';
        
		comparedata +=	'<div class="slide" id="tab-1-1" style="display:none">';
		
		comparedata +=	'<table>';
		
		comparedata +=	'<tr><td>MALE</td>';
			$.each(tempservers, function(index, serverdata) {
					
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['gender']['male'])+'</td>';
					     
					 
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		
		comparedata +=	'<tr><td>FEMALE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		
			comparedata +=	'<tr><td>UNKNOWN</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'</table></div>';
		
		comparedata +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		comparedata +=	'<tr>';
		
			
			
			comparedata +=	'<tr><td>18-24</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>25-34</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			comparedata +='<td>&nbsp;</td></tr>';	
				comparedata +=	'<tr><td>35-44</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['age']['35-44'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td></tr>';
		    comparedata +=	'<tr><td>45-54</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['age']['45-54'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>55-64</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'<tr><td>65+</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['age']['65'])+'</td>';
				});	
			
				comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'</table></div>';	
				
				comparedata +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		comparedata +=	'<tr>';
		
			
			
			comparedata +=	'<tr><td>DESKTOP FEED</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>MOBILE FEED</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			comparedata +='<td>&nbsp;</td></tr>';	
				comparedata +=	'<tr><td>INSTAGRAM</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>AUDIENCE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'<tr><td>RIGHT SIDE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cost_per']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
		
	   comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=  '</table></div>';
		

		
		comparedata += '</tr>';					
										
															
	comparedata += ' </tr></tbody></table></td></tr><tr class="tab-holder"><td colspan="5"><table>';
						
														
	comparedata += '<tbody><tr class="highlight-data">';
	
															
									comparedata +='<td title="Rate">Rate</td>';
							
							      	$.each(tempservers, function(indextempserver, servers) {
							      	    
							      	  
									comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(servers['rate']['counts'])+'</td>';
								
									});
									
										
										comparedata +=	'<td title="Breakdown">';
		                    comparedata +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
		
		                    $.each(tempservers, function(index, serverdata) {
        					     var incid = 1;
            					 $.each(serverdata['rate']['breakdown'], function(breakdownkey, breakdowndata) {
            					 comparedata +=	'	<li><a href="#tab-3-'+incid+'" class="opener tag">'+breakdownkey+'</a></li>';
            					 incid++;
					            });
					            return false; 
				            });	
		
	                   comparedata += '</ul></td>';
	                   
	                   comparedata += 	'<tr><td colspan="5" class="tab-content">';
        
		comparedata +=	'<div class="slide" id="tab-1-1" style="display:none">';
		
		comparedata +=	'<table>';
		
		comparedata +=	'<tr><td>MALE</td>';
			$.each(tempservers, function(index, serverdata) {
					
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['gender']['male'])+'</td>';
					     
					 
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		
		comparedata +=	'<tr><td>FEMALE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		
			comparedata +=	'<tr><td>UNKNOWN</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'</table></div>';
		
		comparedata +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		comparedata +=	'<tr>';
		
			
			
			comparedata +=	'<tr><td>18-24</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>25-34</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			comparedata +='<td>&nbsp;</td></tr>';	
				comparedata +=	'<tr><td>35-44</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['age']['35-44'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td></tr>';
		    	comparedata +=	'<tr><td>45-54</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['age']['45-54'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>55-64</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'<tr><td>65+</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['age']['65'])+'</td>';
				});	
			
				comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'</table></div>';	
				
				comparedata +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		comparedata +=	'<tr>';
		
			
			
			comparedata +=	'<tr><td>DESKTOP FEED</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>MOBILE FEED</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			comparedata +='<td>&nbsp;</td></tr>';	
				comparedata +=	'<tr><td>INSTAGRAM</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    comparedata +='<td>&nbsp;</td></tr>';
			comparedata +=	'<tr><td>AUDIENCE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=	'<tr><td>RIGHT SIDE</td>';
			$.each(tempservers, function(index, serverdata) {
				
					     comparedata +='<td title="ADSET NAME #1">'+number_formate_rate(serverdata['rate']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
		
	   comparedata +='<td>&nbsp;</td></tr>';
		comparedata +=  '</table></div>';
		

		
		comparedata += '</tr>';
												
	comparedata += '</tr>';
	comparedata += '</tbody></table></td></tr></tbody></table></div>';
	
   });
    
    
  return  comparedata; 
    
}

function spendtTable(servers){
   
     
    var spendTable = '';								
	spendTable +=	'<thead><tr>';
	
	
	
	spendTable +=	'<th>SPEND</th>';
	$.each(servers, function(index, value) {
	 
	 if(index.length > 30){
	   var indexname =   $.trim(index).substring(0, 30)+'...';
	 }else{
	     var indexname = index;
	 }
	
	spendTable +=	'<th title="'+index+'">'+indexname+'</th>';
            });
      spendTable +=	'<th>Breakdown</th>';      
            
										
											
	spendTable +=	'</tr></thead><tbody><tr class="tab-holder"><td colspan="5"><table>';
																
	   spendTable +=	'<tbody>';
       spendTable +=	'<tr class="highlight-data">';
    var col = 2;
	spendTable +=	'<td title="Last 24 Hours">Last 24 Hours</td>';
	
	$.each(servers, function(index, serverdata) {
	
	spendTable += '<td title="cpm'+col+'">'+number_formate_currency(serverdata['last24_hour']['spend'])+'</td>';

	});		
	
	 spendTable +=	'<td title="Breakdown">';
					spendTable +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
					
				$.each(servers, function(index, serverdata) {
					 
					 var incidfirst = 1;
					 $.each(serverdata['last24_hour']['breakdown'], function(breakdownkey, breakdowndata) {
					     
					 spendTable +=	'	<li><a href="#tab-3-'+incidfirst+'" class="opener tag">'+breakdownkey+'</a></li>';
					incidfirst++;
					 });
					 return false; 
				});	
				
					
				   spendTable +=	'</ul></td>';
											
															
	spendTable +=	'</tr>';
	
	
	
        spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['gender']['male'])+'</td>';
					     
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    	spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['age']['65'])+'</td>';
				});	
			
				spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';	
				
				spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['last24_hour']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		spendTable += '</tr>';
	
	
	
	
	spendTable +=	'</tbody></table></td></tr><tr class="tab-holder">';
	spendTable +=	'<td colspan="5"><table>';
																												
	spendTable +=	'<tbody><tr class="highlight-data">';
	
	  
	spendTable +=	'<td title="Past 3 Days">Past 3 Days</td>';
	
	$.each(servers, function(index, serverdata) {
	
     	spendTable += '<td title="cpm'+col+'">'+number_formate_currency(serverdata['past3days']['spend'])+'</td>';
     
	});	
	
	spendTable +=	'<td title="Breakdown">';
					spendTable +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
					
				$.each(servers, function(index, serverdata) {
					 
					 var incidfirst = 1;
					 $.each(serverdata['last24_hour']['breakdown'], function(breakdownkey, breakdowndata) {
					     
					 spendTable +=	'	<li><a href="#tab-3-'+incidfirst+'" class="opener tag">'+breakdownkey+'</a></li>';
					incidfirst++;
					 });
					 return false; 
				});	
				
					
				   spendTable +=	'</ul></td>';
															
    spendTable +=	'</tr>';
    
    spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					 
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['gender']['male'])+'</td>';
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    	spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['65'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		//spendTable += '</tr>';
		
		
		
		spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['rightside'])+'</td>';
				});	
				
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';		
				
			spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					 
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['gender']['male'])+'</td>';
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    	spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['age']['65'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		//spendTable += '</tr>';
		
		
		
		spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past3days']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
	  
		
	   spendTable +='<td>&nbsp;</td></tr>';
	   
		spendTable +=  '</table></div>';
		
		spendTable += '</tr>';

    
    
    
    spendTable += '</tbody></table></td></tr><tr class="tab-holder"><td colspan="5"><table>';
    

														
    spendTable +=	'<tbody><tr class="highlight-data">';
    
    	spendTable +=	'<td title="Past 30 Days">Past 30 Days</td>';
	 $.each(servers, function(index, serverdata) {
 
     	spendTable +=	'<td title="frequency'+col+'">'+number_formate_currency(serverdata['past_30days']['spend'])+'</td>';
     	
	});	
	
	  spendTable +=	'<td title="Breakdown">';
					spendTable +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
					
				$.each(servers, function(index, serverdata) {
					 
					 var incidfirst = 1;
					 $.each(serverdata['last24_hour']['breakdown'], function(breakdownkey, breakdowndata) {
					     
					 spendTable +=	'	<li><a href="#tab-3-'+incidfirst+'" class="opener tag">'+breakdownkey+'</a></li>';
					incidfirst++;
					 });
					 return false; 
				});	
				
					
				   spendTable +=	'</ul></td>';
	
	
	
	spendTable +=	'</tr>';
	
	spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					 
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['gender']['male'])+'</td>';
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    	spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['age']['65'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		//spendTable += '</tr>';
		
		
		
		spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['past_30days']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
	   
		spendTable +=  '</table></div>';
		
		spendTable += '</tr>';

	
	
	
	
	spendTable += '</tbody></table></td></tr><tr class="tab-holder"><td colspan="5"><table>';
    

	
														
    spendTable +=	'<tbody><tr class="highlight-data">';
    
    	spendTable +=	'<td title="Lifetime">Lifetime</td>';
	    $.each(servers, function(index, serverdata) {
 
     	spendTable +=	'<td title="frequency'+col+'">'+number_formate_currency(serverdata['lifetime']['spend']);+'</td>';
	});		
	
	
		spendTable +=	'<td title="Breakdown">';
		spendTable +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
		
		$.each(servers, function(index, serverdata) {
					  var incid = 1;
					 $.each(serverdata['lifetime']['breakdown'], function(breakdownkey, breakdowndata) {
					spendTable +=	'	<li><a href="#tab-3-'+incid+'" class="opener tag">'+breakdownkey+'</a></li>';
					incid++;
					 });
					 return false; 
				});	
		
		
	   spendTable +=	'</ul></td>';			
	
	
	
	
        spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					 
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['gender']['male'])+'</td>';
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    	spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['age']['65'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		//spendTable += '</tr>';
		
		
		
		spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['lifetime']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
	   
		spendTable +=  '</table></div>';
		
		spendTable += '</tr>';
		
		
		
		
	
														
															
    spendTable +=	'</tr></tr></tbody></table></td></tr></tbody>';								
																
    return spendTable;

}

function gethitTable(servers){
    
    // console.log('Hello');
    // console.log(servers);
    
    // return false;
     
    var spendTable = '';
								
	spendTable +=	'<thead><tr>';
	

	
	spendTable +=	'<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>';
	$.each(servers, function(index, value) {
	    
	if(index.length > 30){
	   var indexname =   $.trim(index).substring(0, 30)+'...';
	 }else{
	     var indexname = index;
	 }
	
	spendTable +=	'<th title="'+index+'">'+indexname+'</th>';
            
            });
      spendTable +=	'<th>Breakdown</th>';      
            
										
											
	spendTable +=	'</tr></thead><tbody><tr class="tab-holder"><td colspan="5"><table>';		
														
	   spendTable +=	'<tbody>';
       spendTable +=	'<tr class="highlight-data">';
    var col = 2;
	spendTable +=	'<td title="Impressions">Impressions</td>';
	
	$.each(servers, function(index, serverdata) {
	
	spendTable += '<td title="cpm'+col+'">'+number_formate_int(serverdata['impressions']['hits'])+'</td>';

	});		
	
	 spendTable +=	'<td title="Breakdown">';
					spendTable +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
					
				$.each(servers, function(index, serverdata) {
					 
					 var incidfirst = 1;
					 $.each(serverdata['impressions']['breakdown'], function(breakdownkey, breakdowndata) {
					     
					 spendTable +=	'	<li><a href="#tab-3-'+incidfirst+'" class="opener tag">'+breakdownkey+'</a></li>';
					incidfirst++;
					 });
					 return false; 
				});	
				
					
				   spendTable +=	'</ul></td>';
											
															
	spendTable +=	'</tr>';
	
	
	
        spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['gender']['male'])+'</td>';
					     
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['age']['65'])+'</td>';
				});	
			
				spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';	
				
				spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_int(serverdata['impressions']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		spendTable += '</tr>';
	
	
	
	
	spendTable +=	'</tbody></table></td></tr><tr class="tab-holder">';
	spendTable +=	'<td colspan="5"><table>';
															
														
	spendTable +=	'<tbody><tr class="highlight-data">';
	
	  
	spendTable +=	'<td title="CPM">CPM</td>';
	
	$.each(servers, function(index, serverdata) {
	
     	spendTable += '<td title="cpm'+col+'">'+number_formate_currency(serverdata['cpm']['hits'])+'</td>';
     
	});	
	
	spendTable +=	'<td title="Breakdown">';
		spendTable +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
		
		$.each(servers, function(index, serverdata) {
					  var incid = 1;
					 $.each(serverdata['impressions']['breakdown'], function(breakdownkey, breakdowndata) {
					spendTable +=	'	<li><a href="#tab-3-'+incid+'" class="opener tag">'+breakdownkey+'</a></li>';
					incid++;
					 });
					 return false; 
				});	
		
		
	   spendTable +=	'</ul></td>';
															
    spendTable +=	'</tr>';
    
    spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					 
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['gender']['male'])+'</td>';
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    	spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['65'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		//spendTable += '</tr>';
		
		
		
		spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['rightside'])+'</td>';
				});	
				
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';		
				
			spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					 
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['gender']['male'])+'</td>';
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    	spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['age']['65'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		//spendTable += '</tr>';
		
		
		
		spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate_currency(serverdata['cpm']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
	  
		
	   spendTable +='<td>&nbsp;</td></tr>';
	   
		spendTable +=  '</table></div>';
		
		spendTable += '</tr>';

    
    
    
    spendTable += '</tbody></table></td></tr><tr class="tab-holder"><td colspan="5"><table>';
    

														
    spendTable +=	'<tbody><tr class="highlight-data">';
    
    	spendTable +=	'<td title="Frequency">Frequency</td>';
	 $.each(servers, function(index, serverdata) {
 
     	spendTable +=	'<td title="frequency'+col+'">'+number_formate(serverdata['frequency']['hits'])+'</td>';
     	
	});	
	
	
	spendTable +=	'<td title="Breakdown">';
		spendTable +=	'	<ul class="tabset tags d-flex flex-wrap justify-content-md-center">';
		
		$.each(servers, function(index, serverdata) {
					  var incid = 1;
					 $.each(serverdata['impressions']['breakdown'], function(breakdownkey, breakdowndata) {
					spendTable +=	'	<li><a href="#tab-3-'+incid+'" class="opener tag">'+breakdownkey+'</a></li>';
					incid++;
					 });
					 return false; 
				});	
		
		
	   spendTable +=	'</ul></td>';
	
	
	
	spendTable +=	'</tr>';
	
	spendTable += 	'<tr><td colspan="5" class="tab-content">';
        
		spendTable +=	'<div class="slide" id="tab-1-1" style="display:none">';
		spendTable +=	'<table>';
		
		spendTable +=	'<tr><td>MALE</td>';
			$.each(servers, function(index, serverdata) {
					 
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['gender']['male'])+'</td>';
					 
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
		spendTable +=	'<tr><td>FEMALE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['gender']['female'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		
			spendTable +=	'<tr><td>UNKNOWN</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['gender']['unknown'])+'</td>';
				});	     
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'</table></div>';
		
		spendTable +=	'<div class="slide" id="tab-1-2" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>18-24</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['age']['18-24'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>25-34</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['age']['25-34'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>35-44</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['age']['35-44'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
		    spendTable +=	'<tr><td>45-54</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['age']['45-54'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>55-64</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['age']['55-64'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>65+</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['age']['65'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=  '</table></div>';
		

		
		//spendTable += '</tr>';
		
		
		
		spendTable +=	'<div class="slide" id="tab-1-3" style="display:none"><table>';
		spendTable +=	'<tr>';
		
			
			
			spendTable +=	'<tr><td>DESKTOP FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['placement']['dekstopfeed'])+'</td>';
				});	
		
		  spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>MOBILE FEED</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['placement']['mobilefeed'])+'</td>';
				});	
				
			spendTable +='<td>&nbsp;</td></tr>';	
				spendTable +=	'<tr><td>INSTAGRAM</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+(serverdata['frequency']['breakdown']['placement']['instagram'])+'</td>';
				});	
		    spendTable +='<td>&nbsp;</td></tr>';
			spendTable +=	'<tr><td>AUDIENCE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['placement']['audience'])+'</td>';
				});	
		
		spendTable +='<td>&nbsp;</td></tr>';
		spendTable +=	'<tr><td>RIGHT SIDE</td>';
			$.each(servers, function(index, serverdata) {
				
					     spendTable +='<td title="ADSET NAME #1">'+number_formate(serverdata['frequency']['breakdown']['placement']['rightside'])+'</td>';
				});	
		
	   spendTable +='<td>&nbsp;</td></tr>';
	   
		spendTable +=  '</table></div>';
		
		spendTable += '</tr>';
									
    spendTable +=	'</tr></tr></tbody></table></td></tr></tbody>';								
																
    return spendTable;

}

  
function number_formate_rate(number){
    
     if (typeof number === "undefined") {
    return '0.00';
    }else{
        console.log(number);
       
        
         var num;
    if( number % 1 != 0){
        
    num = parseFloat(number);
    }else{
        
       num = parseInt(number); 
    }
    //num = num*100;
    var formated_number = num.toFixed(2)+"%";
    return delimitNumbers(formated_number);
    }

var rate_count = servers['rate']['counts']*100;

}

function number_formate(number){
    if (typeof number === "undefined") {
    return '0.00';
    }else{
        console.log(number);
       
        
         var num;
    if( number % 1 != 0){
        
    num = parseFloat(number);
    }else{
        
       num = parseInt(number); 
    }
    var formated_number = num.toFixed(2)
    return delimitNumbers(formated_number);
    }
}

function number_formate_int(number){
    if (typeof number === "undefined") {
    return '0';
    }else{
       
         var num;
    if( number % 1 != 0){
        
    num = parseFloat(number);
    }else{
        
       num = parseInt(number); 
    }
   
    return delimitNumbers(num);
    }
}


function number_formate_currency(number){
    
    var current_curr = $('#current_curr').val();
    
    if (typeof number === "undefined") {
    return '0.00';
    }else{
        console.log(number);
       
        
         var num;
    if( number % 1 != 0){
        
    num = parseFloat(number);
    }else{
        
       num = parseInt(number); 
    }
    var formated_number = num.toFixed(2)
    return current_curr+delimitNumbers(formated_number);
    }
}



function delimitNumbers(str) {
  return (str + "").replace(/\b(\d+)((\.\d+)*)\b/g, function(a, b, c) {
    return (b.charAt(0) > 0 && !(c || ".").lastIndexOf(".") ? b.replace(/(\d)(?=(\d{3})+$)/g, "$1,") : b) + c;
  });
}


function datapointlabel(string_value){
    
				            var label = ''
							switch (string_value) { 
								case 'link_click': 
									label = 'Clicks to Website';
									break;
								case 'offsite_conversion.fb_pixel_lead': 
									label = 'Leads (FB pixel)';
									break;
								case 'post_engagement': 
									label = 'Post Engagement';
									break;
								case 'like': 
									label = 'Page Likes';
									break;
								case 'offsite_conversion.add_to_cart': 
									label = 'Adds to Cart (FB pixel)';
									break;
                                case 'offsite_conversion.checkout': 
									label = 'Checkouts (FB pixel)';
									break;	
								case 'offsite_conversion.fb_pixel_add_payment_info': 
									label = 'Adds Payment Info (FB pixel)';
									break;
								 case 'offsite_conversion.fb_pixel_add_to_cart': 
									label = 'Adds To Cart (FB pixel)';
									break;
								case 'offsite_conversion.fb_pixel_add_to_wishlist': 
									label = 'Adds To Wishlist (FB pixel)';
									break;
                                case 'offsite_conversion.fb_pixel_complete_registration': 
									label = 'Completed Registration (FB pixel)';
									break;
                                case 'offsite_conversion.fb_pixel_initiate_checkout': 
									label = 'Initiates Checkout (FB pixel)';
									break;	
                                 case 'offsite_conversion.fb_pixel_purchase': 
									label = 'Purchases (FB pixel)';
									break;
                                 case 'offsite_conversion.fb_pixel_search': 
									label = 'Search (FB pixel)';
									break;		
                                 case 'offsite_conversion.fb_pixel_view_content': 
									label = 'Views Content (FB pixel)';
									break;	
                                case 'offsite_conversion.key_page_view': 
									label = 'Key Page Views (FB pixel)';
									break;	
                                case 'onsite_conversion.messaging_first_reply': 
									label = 'New Messaging Conversations';
									break;	
                                case 'app_install': 
									label = 'App Installs';
									break;	
                                case 'leadgen.other': 
									label = 'Leads (Form)';
									break;	
                                case 'video_10_sec_watched_actions': 
									label = '10-Second Video Views';
									break;	
                                case 'video_p25_watched_actions': 
									label = '25% Video Watched';
									break;
                                case 'video_view': 
									label = '3-Second Video Views';
									break;	
                                case 'mobile_app_install': 
									label = 'Mobile App Installs';
									break;										
								default:
									label = 'No Label';
									break;
							}
				    return label;
				}	

