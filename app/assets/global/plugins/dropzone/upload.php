<?php 
$userId = $_GET['userId'];
$storeFolder = '../../../../uploads/campaign/';   //2
$uploadDirPath2 = $storeFolder.$userId."/";
if (!is_dir($uploadDirPath2)) {
    mkdir($uploadDirPath2);
    chmod($uploadDirPath2, 0777);
}
 
if (!empty($_FILES)) {
     
    $tempFile = $_FILES['file']['tmp_name'];          //3             
      
    $targetPath = $uploadDirPath2;  //4
    $temp = explode(".", $_FILES["file"]["name"]);
    $fileName = rand(0, 3000).".".$temp[1];
    $targetFile =  $targetPath. $fileName;  //5
 
    move_uploaded_file($tempFile,$targetFile); //6  
    
    //uploadedImages
   $success_message = array( 'success' => 200,
                        'filename' => $fileName,
                        'oldfname' => $temp[0].$temp[1],
                        'p' => !empty($_GET['p']) ? $_GET['p'] : "",
                        'userId' => $userId
                        );
   echo json_encode($success_message);
}
?>