jQuery(window).load(function () {

    "use strict";
    jQuery('#preloaderMain').delay(350).fadeOut(function () {
        jQuery('body').delay(350).css({'overflow': 'visible'});
    });
});
function ValueIsNull(id, msg, msgId) {
    var field_val = document.getElementById(id).value;
    if (field_val === "") {
        show_message_box(msgId, msg);
        AddErrorCls("#" + id);
        return true;
    } else {
        RemoveErrorCls("#" + id);
        return false;
    }
}
function invalid_url(id, msg, msgId) {
    var field_val = document.getElementById(id).value;
    var RegExp = (/^HTTP|HTTP|http(s)?:\/\/(www\.)?[A-Za-z0-9]+([\-\.]{1}[A-Za-z0-9]+)*\.[A-Za-z]{2,40}(:[0-9]{1,40})?(\/.*)?$/);
    if (RegExp.test(field_val) === false) {
        show_message_box(msgId, msg);
        AddErrorCls("#" + id);
        return true;
    } else {
        RemoveErrorCls("#" + id);
        return false;
    }
}
function show_message_box(id, text) {
    $("#msg_box" + id).fadeIn(3000);
    $("#msg_err" + id).html(text);
}
function autoHide(ele) {
    $(ele).delay(7000).fadeOut('slow');
}
function hide_message_box(id) {
    $("#msg_box" + id).hide();
}
function redirect_to(page) {
    window.location = page;
}
function hide_div(id) {
    $(id).hide();
}
function show_div(id) {
    $(id).fadeIn(3000);
}
function success_message(id, msg) {
    $("#successmsgBx" + id).fadeIn(3000);
    $("#msg_suc" + id).html(msg);
}
function autoHide(ele) {
    $(ele).delay(7000).fadeOut('slow');
}
function set_opcity_one(id) {
    $(id).fadeTo("slow", 1);
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function isNumberAndDot(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function setCampaignName() {
    $('#compaingname_1').val($('#compaingname').val());
}

function setCampaignAccountId() {
    $('#adaccountid_1').val($('#adaccountid').val());
    $('#adaccountid_2').val($('#adaccountid').val());
}
function CreateCampaign() {
    
    
    getCustomAudiences($('#adaccountid').val()); //For custom audience
    if (ValueIsNull("compaingname", "Please enter campaign name", 1)) {
        return false;
    }
    if (ValueIsNull("adaccountid", "Please select ad account", 1)) {
        return false;
    }
    $('#compaingname_1').val($('#compaingname').val());
    $('#adaccountid_1').val($('#adaccountid').val());
    $('#adaccountid_2').val($('#adaccountid').val());
    if ($('#objective5').is(':checked')) {
        get_fb_apps($('#adaccountid').val());
    }
    //if ($('#objective4').is(':checked')) {
    get_offsitepixels($('#adaccountid').val());
    // }
    hide_message_box(1);
    show_div("#preloader");
    setTimeout(function () {

        $("#home").removeClass("active in");
        $("#box1Li").removeClass("active")
        $("#box2Li").addClass("active")
        $("#menu1").addClass("active in");
//        $("#Create_Campaign").attr({
//            href: "#home",
//            "data-toggle": "tab"
//        });
        $("#Ads_Design").attr({
            href: "#menu1",
            "data-toggle": "tab"
        });
        hide_div("#preloader");
    }, 2000);
    return false;
    var data = $('#create_creativeads_frm').serialize();

    var url = site_url + 'createcampaign/create_campaign/';
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                //alert(result);
                if (result.indexOf('ValidationError') >= 0) {
                    hide_div("#loder_box1");
                    result = result.split("@");
                    show_message_box(1, result[1]);
                } else if (result.indexOf('Successfully') >= 0) {
                    hide_div("#loder_box1");
                    hide_message_box(1);
                    result = result.split("@");
                    success_message(1, result[1]);
                    autoHide("#successmsgBx1");
                    //Section Hide/Show
                    $("#home").removeClass("active in");
                    $("#box1Li").removeClass("active")
                    $("#box2Li").addClass("active")
                    $("#menu1").addClass("active in");
                    Create_Campaign
                    $("#Create_Campaign a").attr('href', '#home');
                    //console.log("aaaaaaaaa");
                    //----------------------------------------


                }

            },
            async: false
        });
    }, 2000);
    return false;
}
function ad_design_darafts() {
    set_opcity_one("#loader_drft");
    hide_message_box(22);
    $(".drft_msg").html('');
    var data = $('#create_creativeads_frm').serialize();
    var url = site_url + 'createcampaign/overllAllDrafts/';
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                //  if (result.indexOf('successfully') >= 0) {
                hide_div("#loader_drft");
                $(".drft_msg").show();
                $(".drft_msg").html(result);
                autoHide(".drft_msg");
                //  }
            },
            async: false
        });
    }, 2000);
}
function delete_darft(id) {
    //$("#del_"+id).confirmation('show');
    var url = site_url + 'createcampaign/delete_darft/';
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: {
                'darftId': id,
            },
            success: function (result) {
                console.log(result);
                if (result.indexOf('successfully') >= 0) {
                    $("#row_" + id).fadeOut("slow");
                }
            },
            async: false
        });
    }, 2000);
}
function CreateCreativeAd() {
    show_div("#preloader");
    var data = $('#create_creativeads_frm').serialize();
    var url = site_url + 'createcampaign/validate_ad_design/';
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                hide_div("#preloader");
                if (result.indexOf('successfully') >= 0) {
                    $("#menu1").removeClass("active in");
                    $("#box2Li").removeClass("active")
                    $("#box3Li").addClass("active")

                    $("#menu2").addClass("active in");
                    $("#Audi_ence").attr({
                        href: "#menu2",
                        "data-toggle": "tab"
                    });
                    scroll_to_top("#menu2");
                } else if (result.indexOf('ValidationError') >= 0) {
                    hide_div("#loader_drft");
                    result = result.split("@");
                    show_message_box(2, result[1]);
                }
            },
            async: false
        });
    }, 2000);
    return false;
}
function get_pages_tabs() {
    show_div('.storyId_spin');
    var data = $('#create_creativeads_frm').serialize();
    var url = site_url + 'createcampaign/get_pages_data/';
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                //hide_div("#preloader");
                //console.log(result);

                if (result.indexOf('NotAllowed') >= 0) {
                    //DO Nothing Yet
                } else if (result.indexOf('PAGE_LIKES_OBJ') >= 0) {
                    hide_div('.storyId_spin');
                    show_div(".PAGE_LIKES_OBJ");
                    result = result.split("@");
                    $("#tab_list").html(result[1]);
                } else if (result.indexOf('POST_ENGAGEMENT_OBJ') >= 0) {
                    $("#storyId").empty();
                    hide_div('.storyId_spin');
                    show_div(".POST_ENGAGEMENT_OBJ");
                    result = result.split("@");
                    $("#storyId").append(result[1]);
                } else if (result.indexOf('LEAD_GENERATION_OBJ') >= 0) {
                    $("#leadGenForm").empty();
                    hide_div('.storyId_spin');
                    show_div(".LEAD_GENERATION_OBJ");
                    result = result.split("@");
                    $("#leadGenForm").append(result[1]);
                    $('#leadGenForm').prop('disabled', false);
                    $('#ad_new_frm').prop('disabled', false);
                }
            },
            async: false
        });
    }, 2000);
    return false;
}
function lead_gen_frm() {
    var adaccountid = document.getElementById("adaccountid").value;
    var pageId = document.getElementById("objectIdpage").value;
    FB.ui({
        method: 'lead_gen',
        page_id: pageId,
        ad_account_id: adaccountid, // Note: This does NOT contain 'act_'
        display: 'popup'
    }, function (response) {

        if (response != 'undefined') {
            var formID = response.formID;
            var form_url = response.follow_up_action_url;
            var name = response.name;
            var name = name.replace("+", " ");
            var Option_val = form_url + "-$-" + formID;
            $('#leadGenForm').append('<option value="' + Option_val + '" selected="selected">' + name + '</option>')
        }

        // Handle response.
    });
    async: false
    return false;
}
function fb_termandcanditions() {
    //var adaccountid = document.getElementById("adaccountid").value;
    var pageId = document.getElementById("objectIdpage").value;
    FB.ui({
        method: 'lead_gen_tos',
        page_id: pageId,
        display: 'popup'
    }, function (response) {
        if (response != 'undefined') {
            hide_div("#jsNoTosAcceptedAlert");
        } else {

        }
    });
    async: false
    return false;
}
function SaveAudienceIntoDb() {
    show_div("#loader_drft2");
    var data = $('#create_creativeads_frm').serialize();
    var url = site_url + 'createcampaign/overllAllDrafts/';
    $(".drft_msg").html('');
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                // if (result.indexOf('successfully') >= 0) {
                hide_div("#loader_drft2");
                $(".drft_msg").show();
                $(".drft_msg").html(result);
                autoHide(".drft_msg");
                // }
            },
            async: false
        });
    }, 2000);
}
function SaveAudience() {
    var locations_autocomplete_include = document.getElementById("locations_autocomplete_include").value;
    if (locations_autocomplete_include === '') {
        show_message_box(3, "Insert at least one location!");
        return false
    }
    var data = $('#create_creativeads_frm').serialize();
    hide_message_box(3);
    show_div("#preloader");
    setTimeout(function () {
        
        $("#menu2").removeClass("active in");
        $("#box3Li").removeClass("active")
        $("#box4Li").addClass("active")
        $("#menu3").addClass("active in");
        $("#Budget_Bidding").attr({
            href: "#menu3",
            "data-toggle": "tab"
        });
        scroll_to_top("#menu3");
        hide_div("#preloader");
    }, 2000);
    return false;
}
function Savebudget() {
    show_div("#preloader");
    var data = $('#create_creativeads_frm').serialize();
    var url = site_url + 'createcampaign/Savebudget/';
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                //console.log(result);
                //return false;
                if (result.indexOf('ValidationError') >= 0) {
                    hide_div("#preloader");
                    result = result.split("@");
                    show_message_box(101, result[1]);
                } else if (result.indexOf('Successfully') >= 0) {
                    //show_div("#preloader");
                    $("#menu3").removeClass("active in");
                    $("#box4Li").removeClass("active")
                    $("#box5Li").addClass("active")
                    $("#menu4").addClass("active in");
                    $("#Pub_lish").attr({
                        href: "#menu4",
                        "data-toggle": "tab"
                    });
                    scroll_to_top("#menu4");
                    hide_div("#preloader");
                }

            },
            async: false
        });
    }, 2000);



    return false;
}
function LoeadPreview() {
    var data = $('#create_creativeads_frm').serialize();
    var url = site_url + 'createcampaign/getAdpreivewAv/';
    $("#ads_preview_desktopfeed").css("opacity", 0);
    $("#ads_preview_mobilefeed").css("opacity", 0);
    $("#ads_preview_rightfeed").css("opacity", 0);
    $("#feedloader_1").show();
    $("#feedloader_2").show();
    $("#feedloader_3").show();
    setTimeout(function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                $("#ads_preview_desktopfeed").fadeTo("slow", 1);
                if (result.indexOf('ValidationError') >= 0) {
                    $("#feedloader_1").hide();
                } else {
                    $("#ads_preview_desktopfeed").html(result);
                    $("#desktoppreview").val(result);
                    $("#feedloader_1").hide();
                }
            },
            async: false
        });
    }, 2000);
    setTimeout(function () {
        var url2 = site_url + 'createcampaign/getAdMobilepreivewAv/';
        $.ajax({
            type: "POST",
            cache: false,
            url: url2,
            data: data,
            success: function (result) {
                $("#ads_preview_mobilefeed").fadeTo("slow", 1);
                if (result.indexOf('ValidationError') >= 0) {
                    $("#feedloader_2").hide();
                } else {
                    $("#ads_preview_mobilefeed").html(result);
                    $("#mobilepreview").val(result);
                    $("#feedloader_2").hide();
                }
            },
            async: false
        });
    }, 2000);
    setTimeout(function () {
        var url3 = site_url + 'createcampaign/getAdRightColpreivewAv/';
        $.ajax({
            type: "POST",
            cache: false,
            url: url3,
            data: data,
            success: function (result) {
                $("#ads_preview_rightfeed").fadeTo("slow", 1);
                if (result.indexOf('ValidationError') >= 0) {
                    $("#feedloader_3").hide();
                } else {
                    $("#ads_preview_rightfeed").html(result);
                    $("#righthandpreview").val(result);
                    $("#feedloader_3").hide();
                }
            },
            async: false
        });
    }, 2000);
}
function setLinkURL(ele) {
    document.getElementById("creatives_link_url1").value = document.getElementById(ele).value;
}
function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}


function show_object_section(ele) {
    var index;
    var obj_section = new Array("LINK_CLICKS", "PAGE_LIKES", "POST_ENGAGEMENT", "CONVERSIONS", "LEAD_GENERATION", "CANVAS_APP_INSTALLS", "PRODUCT_CATALOG_SALES", "OFFER_CLAIMS");
    for (index = 0; index < obj_section.length; index++) {
        if (ele === obj_section[index]) {
            show_div("." + obj_section[index] + "_OBJ");
        } else {
            hide_div("." + obj_section[index] + "_OBJ");
        }
        //Do something
    }
}
function put_billing_option(option_val, option_text, select_val, select_text) {
    var select = $('#billing_event');
    if (option_val === "DEFAULT") {
        $("#optimization_goal_1").hide();
        $("#optimization_goal_1").siblings("a").hide();
        $("#billing_event_1").hide();
        $("#billing_event_1").siblings("a").hide();
        $('#optimization_goal_2').prop('checked', true);
        $('#billing_event_2').prop('checked', true);
    } else if (option_val === "LEAD_GENERATION") {
        $("#optimization_goal_1").siblings("a").show();
        $("#billing_event_1").siblings("a").show();
        $("#optimization_goal_1").val("LEAD_GENERATION");
        $("#optimization_goal_1").siblings("a").html("Lead Generation");
        //$("#billing_event_1").val("IMPRESSIONS");
        $("#billing_event_1").siblings("a").hide();
        $('#optimization_goal_2').prop('checked', false);
        $('#optimization_goal_2').siblings("a").hide();
        $('#optimization_goal_3').siblings("a").hide();
    } else {
        $("#optimization_goal_1").siblings("a").show();
        $("#billing_event_1").siblings("a").show();
        $("#optimization_goal_1").val(option_val);
        $("#optimization_goal_1").siblings("a").html(option_text);
        $("#billing_event_1").val(select_val);
        //$("#billing_event_1").siblings("a").html(select_text); // AT client request, dont show same name
        $('#optimization_goal_2').prop('checked', false);

    }

}
function get_fb_pages(adaccount) {
    setCampaignAccountId();
    var adaccount_val = (adaccount.options[adaccount.selectedIndex].value)
    if (adaccount_val === "") {
        return false;
    }
    //setTimeout(function () {
    var url = site_url + 'createcampaign/get_fb_advertisable_pages/' + adaccount_val;
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        success: function (result) {
            //console.log(result);
            //return false;
            $("#objectIdpage").empty();
            $("#objectIdpage").append(result);
        },
        async: true
    });
    // }, 2000);

    //getCustomAudiences(adaccount_val); //For custom audience

}
function getCustomAudiences(adaccountId) {
    var url = site_url + 'createcampaign/customAudiences/';
    $.ajax({
        type: "POST",
        cache: false,
        data: {adAccountId: adaccountId, },
        url: url,
        success: function (result) {
            $(".mobile_devices").html(result);
            setTimeout(function () {
                $(".mobile_devices").trigger("chosen:updated");
                $('.mobile_devices').chosen();
            }, 2000);

        },
        async: true
    });

}


function get_fb_apps(adaccount_val) {
    if (adaccount_val === "") {
        return false;
    }
    setTimeout(function () {
        var url = site_url + 'createcampaign/get_fb_canvas_app/' + adaccount_val;
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            success: function (result) {
                // console.log(result);
                // return false;
                $("#appId").empty();
                $("#appId").append(result);
            },
            async: false
        });
    }, 2000);

}
function get_offsitepixels(adaccount_val) {
    if (adaccount_val === "") {
        return false;
    }
    setTimeout(function () {
        var url = site_url + 'createcampaign/get_offsitepixels/' + adaccount_val;
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            success: function (result) {
               //console.log(result);
                // return false;
                $("#pixel_id").empty();
                $("#pixel_id").append(result);
            },
            async: false
        });
    }, 2000);

}
function Proceed_last() {


    $(".fa_spinner").removeAttr("style");
    $(".fa_spinner").show();


    hide_div("#successmsgBx303");
    hide_div("#msg_box303");

    var behaviors = $('#more_behavious').data('directoryTree').selectedItems('input:checkbox[name="behaviors[]"]').join(',');
    var life_events = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="life_events[]"]').join(',');
    var politics = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="politics[]"]').join(',');
    var industries = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="industries[]"]').join(',');
    var ethnic_affinity = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="ethnic_affinity[]"]').join(',');
    var generation = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="generation[]"]').join(',');
    var household_composition = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="household_composition[]"]').join(',');
    var family_statuses = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="family_statuses[]"]').join(',');
    var week_calendar = '';
    var weekcalendar = '';
    $('.selectable-area .item-hour.ui-selectee.ui-selected').each(function (index) {
        weekcalendar = $(this).parent('ul').parent('li').data('day') + "#" + $(this).data('hour');
        week_calendar += weekcalendar + ",";
    });
    var data = $("#create_creativeads_frm").serialize() + '&' + $.param({'behaviors': behaviors}) + '&' + $.param({'life_events': life_events}) + '&' + $.param({'politics': politics}) + '&' + $.param({'industries': industries}) + '&' + $.param({'ethnic_affinity': ethnic_affinity}) + '&' + $.param({'generation': generation}) + '&' + $.param({'household_composition': household_composition}) + '&' + $.param({'family_statuses': family_statuses}) + '&' + $.param({'week_calendar': week_calendar});

    //show_div(".last_spinner");
    //show_div("#cmp_created_box");
    //show_div("#finel_report");
    //return false;
    // setTimeout(function () {
    var url = site_url + 'createcampaign/last_proceed/';
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: true,
        beforeSend: function () {
            $(".fa_spinner").show();
        },
        success: function (result) {
            hide_div('.fa_spinner');
            console.log(result);
            var obj = $.parseJSON(result);
            if (typeof obj == 'object')
            {

                //return false;
                if (obj.status == 1) {
                    //success response

                    success_message(303, obj.message);
                    //$("#finel_report").html(obj.message);
                    //  show_div(".button_success");
                    $('#lastSavedBtn').prop('disabled', true);
                } else {
                    //failure response 
                    // $("#finel_report").html(obj.message);
                    show_message_box(303, obj.message);
                }
            }
        },
        error: function (data) {

            hide_div('.fa_spinner');
            console.log(data);
        },
        complete: function (data) {

            hide_div('.fa_spinner');
            console.log(data);
        }
    });
    // }, 5000);
}

function overllAllDrafts() {
    show_div("#loader_drft3");
    var behaviors = $('#more_behavious').data('directoryTree').selectedItems('input:checkbox[name="behaviors[]"]').join(',');
    var life_events = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="life_events[]"]').join(',');
    var politics = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="politics[]"]').join(',');
    var industries = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="industries[]"]').join(',');
    var ethnic_affinity = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="ethnic_affinity[]"]').join(',');
    var generation = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="generation[]"]').join(',');
    var household_composition = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="household_composition[]"]').join(',');
    var family_statuses = $('#more_demographic').data('directoryTree').selectedItems('input:checkbox[name="family_statuses[]"]').join(',');
    var week_calendar = '';
    var weekcalendar = '';
    $('.selectable-area .item-hour.ui-selectee.ui-selected').each(function (index) {
        weekcalendar = $(this).parent('ul').parent('li').data('day') + "#" + $(this).data('hour');
        week_calendar += weekcalendar + ",";
    });

    var data = $("#create_creativeads_frm").serialize() + '&' + $.param({'behaviors': behaviors}) + '&' + $.param({'life_events': life_events}) + '&' + $.param({'politics': politics}) + '&' + $.param({'industries': industries}) + '&' + $.param({'ethnic_affinity': ethnic_affinity}) + '&' + $.param({'generation': generation}) + '&' + $.param({'household_composition': household_composition}) + '&' + $.param({'family_statuses': family_statuses}) + '&' + $.param({'week_calendar': week_calendar});
    //show_div("#preloader");
    $(".drft_msg").html('');
    setTimeout(function () {
        var url = site_url + 'createcampaign/overllAllDrafts/';
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                //if (result.indexOf('successfully') >= 0) {
                hide_div("#loader_drft3");
                $(".drft_msg").show();
                $(".drft_msg").html(result);
                autoHide(".drft_msg");
                // }

            },
            async: false
        });
    }, 3000);
}
function selectInterset(val) {
    $("#interests").val(val);
    $("#suggesstion-box").hide();
}
function cs_countdown(field, countdowncls, text_max) {
    $(countdowncls).html(text_max);
    $(field).keyup(function () {
        var text_length = $(field).val().length;
        var text_remaining = text_max - text_length;
        $(countdowncls).html(text_remaining);
    });
}
function loadLocation(ele) {
    var include_exclude = ele.options[ele.selectedIndex].value;
    $(".locations_autocomplete").attr("id", "locations_autocomplete_" + include_exclude);
    $(".locations_autocomplete").attr("name", "locations_autocomplete_" + include_exclude);
    if (include_exclude == 'include') {
        $("#locations_autocomplete_include").show();
        $("#locations_autocomplete_exclude").hide();
    } else if (include_exclude == 'exclude') {
        $("#locations_autocomplete_exclude").show();
        $("#locations_autocomplete_include").hide();

    }
}

$.log = function (message) {
    var $logger = $("#logger");
    $logger.html($logger.html() + "\n * " + message);
}
function AddErrorCls(id) {
    $(id).addClass("error_boreder");
}
function RemoveErrorCls(id) {
    $(id).removeClass("error_boreder");
}
function CreateOffer() {
    show_div("#preloader");
    var data = $("#create_creativeads_frm").serialize();
    hide_div("#msg_box2");
    setTimeout(function () {
        var url = site_url + 'createcampaign/create_offer/';
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: data,
            success: function (result) {
                //console.log(result);
                hide_div("#preloader");
                if (result.indexOf('ValidationError') >= 0) {
                    hide_div("#preloader");
                    result = result.split("@");
                    show_message_box(2, result[1]);
                    $('#submit_ad_design').hide();
                } else if (result.indexOf('Successfully') >= 0) {
                    hide_div("#preloader");
                    $('#submit_ad_design').show();
                    result = result.split("@");
                    $("#offer_Id").val(result[1]);
                    LoeadPreview();
                }
            },
            async: false
        });
    }, 5000);
}
function apply_removeCls(removeCls) {
    $("." + removeCls).addClass(removeCls + "_DUM");
    $("div").removeClass(removeCls);
}
function apply_addcls(addcls) {
    $("." + addcls + "_DUM").addClass(addcls);
    $("." + addcls).removeClass(addcls + "_DUM");
}
//---------------------------------------------------------------------------
//For Image Uplaod
$("#input-id").fileinput({
    showCaption: false,
    multiple: true,
    showRemove: false,
    maxFileCount: 5,
    uploadUrl: site_url + 'createcampaign/fileUpload', // you must set a valid URL here else you will get an error
    allowedFileExtensions: ['jpg', 'png', 'jpeg'],
    overwriteInitial: false,
    maxFileSize: 1000,
    minFileCount: 1,
    dropZoneEnabled: false,
    maxImageWidth: 600,
    maxImageHeight: 500,
    resizeImage: true,
    showUpload: false,
    image: {width: "100px", height: "auto"},
    uploadAsync: true,
    browseLabel: 'Upload new images',
    browseIcon: '<span class="img"><img src="' + site_url + 'assets/images/objective2/3.png"></span>',
    //browseClass : 'up-images',
    //buttonLabelClass : ''
}).on("filebatchselected", function (event, files) {
    // trigger upload method immediately after files are selected
    $('#input-id').fileinput("upload");
});
$("#img_pro1").fileinput({
    showCaption: false,
    multiple: false,
    showRemove: false,
    maxFileCount: 1,
    validateInitialCount: true,
    uploadUrl: site_url + 'createcampaign/product1_img', // you must set a valid URL here else you will get an error
    allowedFileExtensions: ['jpg', 'png', 'jpeg'],
    overwriteInitial: false,
    maxFileSize: 1000,
    minFileCount: 1,
    dropZoneEnabled: false,
    maxImageWidth: 600,
    maxImageHeight: 500,
    resizeImage: true,
    showUpload: false,
    image: {width: "100px", height: "auto"},
    uploadAsync: true,
    browseLabel: 'Upload new images',
    browseIcon: '<span class="img"><img src="' + site_url + 'assets/images/objective2/3.png"></span>',
    //browseClass : 'up-images',
    //buttonLabelClass : ''
}).on("filebatchselected", function (event, files) {
    // trigger upload method immediately after files are selected
    $('#img_pro1').fileinput("upload");
});
$("#img_pro2").fileinput({
    showCaption: false,
    multiple: false,
    showRemove: false,
    maxFileCount: 1,
    validateInitialCount: true,
    uploadUrl: site_url + 'createcampaign/product2_img', // you must set a valid URL here else you will get an error
    allowedFileExtensions: ['jpg', 'png', 'jpeg'],
    overwriteInitial: false,
    maxFileSize: 1000,
    minFileCount: 1,
    dropZoneEnabled: false,
    maxImageWidth: 600,
    maxImageHeight: 500,
    resizeImage: true,
    showUpload: false,
    image: {width: "100px", height: "auto"},
    uploadAsync: true,
    browseLabel: 'Upload new images',
    browseIcon: '<span class="img"><img src="' + site_url + 'assets/images/objective2/3.png"></span>',
    //browseClass : 'up-images',
    //buttonLabelClass : ''
}).on("filebatchselected", function (event, files) {
    // trigger upload method immediately after files are selected
    $('#img_pro2').fileinput("upload");
});
$("#img_pro3").fileinput({
    showCaption: false,
    multiple: false,
    showRemove: false,
    maxFileCount: 1,
    validateInitialCount: true,
    uploadUrl: site_url + 'createcampaign/product3_img', // you must set a valid URL here else you will get an error
    allowedFileExtensions: ['jpg', 'png', 'jpeg'],
    overwriteInitial: false,
    maxFileSize: 1000,
    minFileCount: 1,
    dropZoneEnabled: false,
    maxImageWidth: 600,
    maxImageHeight: 500,
    resizeImage: true,
    showUpload: false,
    image: {width: "100px", height: "auto"},
    uploadAsync: true,
    browseLabel: 'Upload new images',
    browseIcon: '<span class="img"><img src="' + site_url + 'assets/images/objective2/3.png"></span>',
    //browseClass : 'up-images',
    //buttonLabelClass : ''
}).on("filebatchselected", function (event, files) {
    // trigger upload method immediately after files are selected
    $('#img_pro3').fileinput("upload");
});
//-----------------------------------------------------------
$("#offer_img").fileinput({
    showCaption: false,
    multiple: false,
    showRemove: false,
    maxFileCount: 1,
    uploadUrl: site_url + 'createcampaign/offer_img_uplaod', // you must set a valid URL here else you will get an error
    allowedFileExtensions: ['jpg', 'png', 'jpeg'],
    overwriteInitial: false,
    maxFileSize: 1000,
    minFileCount: 1,
    dropZoneEnabled: false,
    maxImageWidth: 600,
    maxImageHeight: 500,
    resizeImage: true,
    showUpload: false,
    image: {width: "100px", height: "auto"},
    uploadAsync: true,
    browseLabel: 'Upload new images',
    browseIcon: '<span class="img"><img src="' + site_url + 'assets/images/objective2/3.png"></span>',
    //browseClass : 'up-images',
    //buttonLabelClass : ''
});
//        .on("filebatchselected", function(event, files) {
//    // trigger upload method immediately after files are selected
//    $('#offer_img').fileinput("upload");
//});
function scroll_to_top(id) {
    $('html, body').animate({
        'scrollTop': $(id).position().top
    });
}